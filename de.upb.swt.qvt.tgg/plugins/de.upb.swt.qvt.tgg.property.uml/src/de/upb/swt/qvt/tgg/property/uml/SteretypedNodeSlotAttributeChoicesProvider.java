package de.upb.swt.qvt.tgg.property.uml;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.provider.extension.IEAttributeProvider;

public class SteretypedNodeSlotAttributeChoicesProvider implements IEAttributeProvider {

	public EList<EAttribute> getValidSlotAttributes(TGGConstraint tggConstraint) {

		EList<EAttribute> returnEAttributeList = new UniqueEList<EAttribute>();

		for (EObject valueObject : Helper.getNodesAppliedStereotypes(tggConstraint.getSlotNode())) {
			returnEAttributeList.addAll(((Stereotype)valueObject).getDefinition().getEAllAttributes());		
		}

		return returnEAttributeList;
	}

}
