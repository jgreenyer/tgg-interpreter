/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Jacques Lescot (Anyware Technologies) - initial API and
 * implementation
 *     - Benoit MAGGI (Atos Origin) feature 2222 : using seteotype section on many element
 * 	
 ******************************************************************************/
package de.upb.swt.qvt.tgg.property.page;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ItemProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.diagram.ui.properties.sections.AbstractModelerPropertySection;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.common.edit.provider.IItemQualifiedTextProvider;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UMLPlugin;
import org.eclipse.uml2.uml.edit.providers.UMLItemProviderAdapterFactory;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.qvtbase.QvtbaseFactory;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.property.uml.Helper;

/**
 * The section used to Assign/Unassign a Stereotype to a TGG node
 */
public class StereotypesSection extends AbstractModelerPropertySection {
	
	private static Map<String, Profile> profileCache;
	
	static {
		profileCache = new LinkedHashMap<String, Profile>(25, 0.75f, true) {
			
			private static final long serialVersionUID = -8442374149358161358L;
			private static final int CAPACITY = 15;

			@Override
			protected boolean removeEldestEntry(
					java.util.Map.Entry<String, Profile> eldest) {
				return size() > CAPACITY;
			}
		};
	}
	
	private List<Stereotype> assignableStereotypesList;

	private List<Stereotype> assignedStereotypesList;
	
	private List<Stereotype> assignedNegativeStereotypesList;

	private ILabelProvider labelProvider;

	private TableViewer availableElementsTableViewer;

	private TableViewer selectedElementsTableViewer;
	
	private TableViewer selectedNegativeElementsTableViewer;

	private Button addButton;

	private Button removeButton;

	private Button addToNegativeButton;

	private Button removeFromNegativeButton;

	private Node selectedTGGNode;

	private List<Stereotype> getSelectedStereotypes() {
		return assignedStereotypesList;
	}

	private List<Stereotype> getSelectedNegativeStereotypes() {
		return assignedNegativeStereotypesList;
	}

	private List<Stereotype> getSelectableStereotypes() {
		ArrayList<Stereotype> selectableStereotypesList = new ArrayList<Stereotype>(assignableStereotypesList);
		selectableStereotypesList.removeAll(getSelectedStereotypes());
		selectableStereotypesList.removeAll(getSelectedNegativeStereotypes());
		return selectableStereotypesList;
	}
	
	/**
	 * Checks whether the given profile or one of its 
	 * packaged elements is the profile with the given profile URI.
	 * 
	 * @param profile The profile.
	 * @param profileUri The profile URI.
	 * @return The profile if found, otherwise null.
	 */
	private Profile findProfile(Profile profile, String profileUri) {
		if (profile == null || profileUri == null)
			return null;
		
		if (profile.getURI() != null && profile.getURI().equals(profileUri)) {
			return profile;
		}
		
		if (profile.getPackagedElements() != null) {
			// look for the profile in the current profile's packaged elements
			for (EObject packagedElement: profile.getPackagedElements()) {
				if (!(packagedElement instanceof Profile))
					continue;
				
				Profile subProfile = findProfile((Profile) packagedElement, profileUri);
				if (subProfile != null)
					return subProfile;
			}
		}
		
		return null;
	}

	private IDoubleClickListener availableElementsTableDoubleClickListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			if (addButton.isEnabled()) {
				addButton.notifyListeners(SWT.Selection, null);
			}
		}
	};

	private IDoubleClickListener selectedElementsTableDoubleClickListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			if (removeButton.isEnabled()) {
				removeButton.notifyListeners(SWT.Selection, null);
			}
		}
	};

	private IDoubleClickListener selectedNegativeElementsTableDoubleClickListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			if (removeFromNegativeButton.isEnabled()) {
				removeFromNegativeButton.notifyListeners(SWT.Selection, null);
			}
		}
	};

	private SelectionAdapter addButtonSelectionAdapter = new SelectionAdapter() {
		// event is null when availableElementsTableViewer is double clicked
		public void widgetSelected(SelectionEvent event) {

			final List<Stereotype> stereotypesToAdd = new ArrayList<Stereotype>();
			IStructuredSelection selection = (IStructuredSelection) availableElementsTableViewer.getSelection();
			for (Iterator<?> i = selection.iterator(); i.hasNext();) {
				Stereotype stereotypeToAdd = (Stereotype) i.next();

				EList<Object> children = ((ItemProvider) selectedElementsTableViewer.getInput()).getChildren();
				if (!children.contains(stereotypeToAdd)) {
					children.add(stereotypeToAdd);
				}
				((ItemProvider) availableElementsTableViewer.getInput()).getChildren().remove(stereotypeToAdd);

				stereotypesToAdd.add(stereotypeToAdd);
			}
			
			if (stereotypesToAdd.isEmpty())
				return;

			getEditingDomain().getCommandStack().execute( new RecordingCommand(getEditingDomain()) {

				@Override
				protected void doExecute() {
					Annotation stereoTypesAnnotation;
					//check whether an annotation for UML stereotypes already exists or has to be created anew:
					if (selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()){
						stereoTypesAnnotation = QvtbaseFactory.eINSTANCE.createAnnotation();
						stereoTypesAnnotation.setKey(Helper.umlNsURI);
						selectedTGGNode.getAnnotations().add(stereoTypesAnnotation);
					}else{
						stereoTypesAnnotation = selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0);
					}
					// for all selected stereotypes...
					for (Stereotype stereotypeToAdd : stereotypesToAdd) {
						// check whether an annotation for that stereotype already exists
						boolean stereotypeAnnotationAlreadyExists = false;
						for (Annotation stereoTypeAnnotation : stereoTypesAnnotation.getAnnotations()) {
							if (stereoTypeAnnotation.getValue().contains(stereotypeToAdd)) {
								stereotypeAnnotationAlreadyExists = true;
								stereoTypeAnnotation.setKey(Helper.positiveStereotypeString);
								break;
							}
						}
						if (!stereotypeAnnotationAlreadyExists){
							Annotation stereoTypeAnnotation = QvtbaseFactory.eINSTANCE.createAnnotation();
							stereoTypeAnnotation.setKey(Helper.positiveStereotypeString);
							stereoTypeAnnotation.getValue().add(stereotypeToAdd);
							stereoTypesAnnotation.getAnnotations().add(stereoTypeAnnotation);
						} 
					}
					
					
				}
			});
			
		}
	};

	private SelectionAdapter removeButtonSelectionAdapter = new SelectionAdapter() {
		// event is null when selectedElementsTableViewer is double clicked
		public void widgetSelected(SelectionEvent event) {

			// get selected stereotype(s)
			IStructuredSelection selection = (IStructuredSelection) selectedElementsTableViewer.getSelection();

			// remove those elements from the availableElementsTableViewer (just
			// UI)
			final List<Stereotype> stereotypesToRemove = new ArrayList<Stereotype>();
			for (Iterator<?> i = selection.iterator(); i.hasNext();) {
				Stereotype selectedStereotype = (Stereotype) i.next();

				EList<Object> children = ((ItemProvider) availableElementsTableViewer.getInput()).getChildren();

				if (!children.contains(selectedStereotype)) {
					children.add(selectedStereotype);
				}

				((ItemProvider) selectedElementsTableViewer.getInput()).getChildren().remove(selectedStereotype);

				stereotypesToRemove.add(selectedStereotype);
			}
			
			if (stereotypesToRemove.isEmpty())
				return;
			
			getEditingDomain().getCommandStack().execute( new RecordingCommand(getEditingDomain()) {

				@Override
				protected void doExecute() {
					Annotation stereotypesAnnotation = selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0);
					EList<Annotation> annotationsToRemove = new BasicEList<Annotation>();
					for (Annotation stereoTypeAnnotation : stereotypesAnnotation.getAnnotations()) {
						for (Stereotype stereotypeToRemove : stereotypesToRemove) {
							if (stereoTypeAnnotation.getValue().contains(stereotypeToRemove)) {
								annotationsToRemove.add(stereoTypeAnnotation);
							}
						}
					}
					stereotypesAnnotation.getAnnotations().removeAll(annotationsToRemove);
					// check whether the annotation will be emptied and can be removed:
					if (stereotypesAnnotation.getAnnotations().isEmpty()) {
						selectedTGGNode.getAnnotations().remove(stereotypesAnnotation);
					}
				}
			});
		}
	};
	
	private SelectionAdapter addToNegativeButtonSelectionAdapter = new SelectionAdapter() {
		// event is null when availableElementsTableViewer is double clicked
		public void widgetSelected(SelectionEvent event) {

			final List<Stereotype> stereotypesToAdd = new ArrayList<Stereotype>();
			IStructuredSelection selection = (IStructuredSelection) availableElementsTableViewer.getSelection();
			for (Iterator<?> i = selection.iterator(); i.hasNext();) {
				Stereotype stereotypeToAdd = (Stereotype) i.next();

				EList<Object> children = ((ItemProvider) selectedNegativeElementsTableViewer.getInput()).getChildren();
				if (!children.contains(stereotypeToAdd)) {
					children.add(stereotypeToAdd);
				}
				((ItemProvider) availableElementsTableViewer.getInput()).getChildren().remove(stereotypeToAdd);

				stereotypesToAdd.add(stereotypeToAdd);
			}
			
			if (stereotypesToAdd.isEmpty())
				return;
			
			getEditingDomain().getCommandStack().execute( new RecordingCommand(getEditingDomain()) {

				@Override
				protected void doExecute() {
					Annotation stereoTypesAnnotation;
					//check whether an annotation for UML stereotypes already exists or has to be created anew:
					if (selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()){
						stereoTypesAnnotation = QvtbaseFactory.eINSTANCE.createAnnotation();
						stereoTypesAnnotation.setKey(Helper.umlNsURI);
						selectedTGGNode.getAnnotations().add(stereoTypesAnnotation);
					}else{
						stereoTypesAnnotation = selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0);
					}
					// for all selected stereotypes...
					for (Stereotype stereotypeToAdd : stereotypesToAdd) {
						// check whether an annotation for that stereotype already exists
						boolean stereotypeAnnotationAlreadyExists = false;
						for (Annotation stereoTypeAnnotation : stereoTypesAnnotation.getAnnotations()) {
							if (stereoTypeAnnotation.getValue().contains(stereotypeToAdd)) {
								stereotypeAnnotationAlreadyExists = true;
								stereoTypeAnnotation.setKey(Helper.negativeStereotypeString);
								break;
							}
						}
						if (!stereotypeAnnotationAlreadyExists){
							Annotation stereoTypeAnnotation = QvtbaseFactory.eINSTANCE.createAnnotation();
							stereoTypeAnnotation.setKey(Helper.negativeStereotypeString);
							stereoTypeAnnotation.getValue().add(stereotypeToAdd);
							stereoTypesAnnotation.getAnnotations().add(stereoTypeAnnotation);
						} 
					}
					
					
				}
			});			
		}
	};

	private SelectionAdapter removeFromNegativeButtonSelectionAdapter = new SelectionAdapter() {
		// event is null when selectedElementsTableViewer is double clicked
		public void widgetSelected(SelectionEvent event) {

			// get selected stereotype(s)
			IStructuredSelection selection = (IStructuredSelection) selectedNegativeElementsTableViewer.getSelection();

			// remove those elements from the availableElementsTableViewer (just
			// UI)
			final List<Stereotype> stereotypesToRemove = new ArrayList<Stereotype>();
			for (Iterator<?> i = selection.iterator(); i.hasNext();) {
				Stereotype selectedStereotype = (Stereotype) i.next();

				EList<Object> children = ((ItemProvider) availableElementsTableViewer.getInput()).getChildren();

				if (!children.contains(selectedStereotype)) {
					children.add(selectedStereotype);
				}

				((ItemProvider) selectedNegativeElementsTableViewer.getInput()).getChildren().remove(selectedStereotype);

				stereotypesToRemove.add(selectedStereotype);
			}

			if (stereotypesToRemove.isEmpty())
				return;

			getEditingDomain().getCommandStack().execute( new RecordingCommand(getEditingDomain()) {

				@Override
				protected void doExecute() {
					Annotation stereotypesAnnotation = selectedTGGNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0);
					EList<Annotation> annotationsToRemove = new BasicEList<Annotation>();
					for (Annotation stereoTypeAnnotation : stereotypesAnnotation.getAnnotations()) {
						for (Stereotype stereotypeToRemove : stereotypesToRemove) {
							if (stereoTypeAnnotation.getValue().contains(stereotypeToRemove)) {
								annotationsToRemove.add(stereoTypeAnnotation);
							}
						}
					}
					stereotypesAnnotation.getAnnotations().removeAll(annotationsToRemove);
					// check whether the annotation will be emptied and can be removed:
					if (stereotypesAnnotation.getAnnotations().isEmpty()) {
						selectedTGGNode.getAnnotations().remove(stereotypesAnnotation);
					}
				}
			});

		}
	};
	
	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {

		super.createControls(parent, aTabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		composite.setLayout(new GridLayout(3, false));

		Composite choiceComposite = getWidgetFactory().createComposite(composite, SWT.NONE);
		choiceComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		choiceComposite.setLayout(new GridLayout());

		Label choiceLabel = getWidgetFactory().createLabel(choiceComposite, "Available Stereotypes", SWT.NONE);
		choiceLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		final Table choiceTable = getWidgetFactory().createTable(choiceComposite, SWT.MULTI | SWT.BORDER);
		choiceTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		availableElementsTableViewer = new TableViewer(choiceTable);

		Composite controlButtons = getWidgetFactory().createComposite(composite, SWT.NONE);
		controlButtons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		GridLayout controlsButtonGridLayout = new GridLayout();
		controlButtons.setLayout(controlsButtonGridLayout);

		new Label(controlButtons, SWT.NONE);

		addButton = getWidgetFactory().createButton(controlButtons, "Add >", SWT.PUSH);
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		removeButton = getWidgetFactory().createButton(controlButtons, "< Remove", SWT.PUSH);
		removeButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		new Label(controlButtons, SWT.NONE);

		addToNegativeButton = getWidgetFactory().createButton(controlButtons, "Add Negative >", SWT.PUSH);
		addToNegativeButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		removeFromNegativeButton = getWidgetFactory().createButton(controlButtons, "< Remove Negative", SWT.PUSH);
		removeFromNegativeButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		/*Label spaceLabel = new Label(controlButtons, SWT.NONE);
		GridData spaceLabelGridData = new GridData();
		spaceLabelGridData.verticalSpan = 2;
		spaceLabel.setLayoutData(spaceLabelGridData);*/

		Composite featureComposite = getWidgetFactory().createComposite(composite, SWT.NONE);
		featureComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		featureComposite.setLayout(new GridLayout());

		Label featureLabel = getWidgetFactory().createLabel(featureComposite, "Applied Stereotypes", SWT.NONE);
		featureLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false));

		final Table featureTable = getWidgetFactory().createTable(featureComposite, SWT.MULTI | SWT.BORDER);
		featureTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		selectedElementsTableViewer = new TableViewer(featureTable);

		Label negativeFeatureLabel = getWidgetFactory().createLabel(featureComposite, "Applied Negative Stereotypes", SWT.NONE);
		negativeFeatureLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false));
		
		final Table negativeStereotypesTable = getWidgetFactory().createTable(featureComposite, SWT.MULTI | SWT.BORDER);
		negativeStereotypesTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		selectedNegativeElementsTableViewer = new TableViewer(negativeStereotypesTable);
	}
	
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}

		IStructuredSelection structuredSelection = (IStructuredSelection) selection;
		// only show contents when only one node was selected
		if (structuredSelection.toArray().length != 1) {
			return;
		}
		// and if it is really a node...
		Object selectedObject = structuredSelection.getFirstElement();
		if (!(adapt(selectedObject) instanceof Node)) {
			return;
		}
		selectedTGGNode = (Node) adapt(selectedObject);

		// 1a. Determine stereotypes already assigned to the node
		assignedStereotypesList = Helper.getNodesAppliedStereotypes(selectedTGGNode);
		// 1b. Determine negative stereotypes already assigned to the node
		assignedNegativeStereotypesList = Helper.getNodesAppliedNegativeStereotypes(selectedTGGNode);

		// 2. Determine stereotypes assignable to the node
		assignableStereotypesList = new ArrayList<Stereotype>();
		
		EList<EObject> profileList = new BasicEList<EObject>();
		
		if (((TripleGraphGrammarRule) selectedTGGNode.getGraph()).getNodesDomainGraphPattern(selectedTGGNode) != null){
		TypedModel typedModel = ((TripleGraphGrammarRule) selectedTGGNode.getGraph()).getNodesDomainGraphPattern(selectedTGGNode).getTypedModel();
			for (EPackage pckg : typedModel.getUsedPackage()) {
				
				if (pckg.eContainer() instanceof EAnnotation
						&& (Helper.umlNsURI.equals(((EAnnotation)pckg.eContainer()).getSource())
						   || Helper.umlNsURIforProfiles.equals(((EAnnotation)pckg.eContainer()).getSource()))
					&& UMLPackage.Literals.PROFILE.isInstance(((EAnnotation)pckg.eContainer()).eContainer())) {
					
					profileList.add(((EAnnotation)pckg.eContainer()).eContainer());
				} else {
					// check if a profile is registered in the UMLPlugin
					String namespaceUri = pckg.getNsURI();
					
					if (namespaceUri != null) {
						Profile profile = null;
						
						if (!profileCache.containsKey(namespaceUri)) {
							// the URI has not been checked (and added to the cache) yet
							URI profileUri = UMLPlugin.getEPackageNsURIToProfileLocationMap().get(namespaceUri);
							
							if (profileUri != null) {
								// a profile has been defined
								ResourceSet resourceSet = new ResourceSetImpl();
								Profile tmpProfile = UML2Util.load(resourceSet, profileUri, UMLPackage.Literals.PROFILE);
								
								if (tmpProfile != null)
									profile = findProfile(tmpProfile, namespaceUri);
							}
							
							// add the profile (or null) to the cache
							profileCache.put(namespaceUri, profile);
						} else {
							profile = profileCache.get(namespaceUri);
						}
						
						// if a profile has been found add it to the profile list
						if (profile != null)
							profileList.add(profile);
					}
				}
			}
		}
		
		EList<Annotation> profileAnnotationList = null;
		if (((TripleGraphGrammarRule) selectedTGGNode.getGraph())
				.getNodesDomainGraphPattern(selectedTGGNode) != null) {
			profileAnnotationList = ((TripleGraphGrammarRule) selectedTGGNode
					.getGraph()).getNodesDomainGraphPattern(selectedTGGNode)
					.getTypedModel().getAnnotationsByKeyString(Helper.umlNsURI);
		}
		if (profileAnnotationList != null && !profileAnnotationList.isEmpty())
			profileList.addAll(profileAnnotationList.get(0).getValue());
		for (EObject referencedElement : profileList) {
			if (referencedElement instanceof Profile) {
				Profile profile = (Profile) referencedElement;

				EClass nodeEClass = (EClass) UMLFactory.eINSTANCE.getUMLPackage().getEClassifier(
						selectedTGGNode.getEType().getName());

				for (PackageableElement packageableElement : profile.getPackagedElements()) {
					if (packageableElement instanceof Stereotype) {
						Stereotype candidateStereotype = (Stereotype) packageableElement;
						// System.out.println(" tggNode              " +
						// tggNode);
						// System.out.println(" tggNode.getEType()   " +
						// tggNode.getEType());
						// System.out.println(" nodeEClass           " +
						// nodeEClass);
						// System.out.println(" candidateStereotype  " +
						// candidateStereotype);

						/*
						 * When is a stereotye assignable to a node?
						 * 
						 * 1. when the node's type class equals the extended
						 * meta-class of the stereotype 2. when the type
						 * class is a subclass of the extended meta-class of
						 * the stereotype <=> the extended meta-class is in
						 * the set of superclasses of the node's type class
						 */
						for (Class extendedMetaClass : candidateStereotype.getAllExtendedMetaclasses()) {
							EClassifier eExtendedMetaClass = UMLFactory.eINSTANCE.getEPackage().getEClassifier(
									extendedMetaClass.getName());
							if (eExtendedMetaClass == nodeEClass || nodeEClass.getEAllSuperTypes().contains(eExtendedMetaClass)) {
								assignableStereotypesList.add(candidateStereotype);
								break;
							}
						}

					}
				}
			}
		}


		AdapterFactory adapterFactory = new ComposedAdapterFactory();
		availableElementsTableViewer.setLabelProvider(getLabelProvider());
		availableElementsTableViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		availableElementsTableViewer.setInput(new ItemProvider(adapterFactory, getSelectableStereotypes()));

		AdapterFactory adapterFactory2 = new ComposedAdapterFactory();
		selectedElementsTableViewer.setLabelProvider(getLabelProvider());
		selectedElementsTableViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory2));
		selectedElementsTableViewer.setInput(new ItemProvider(adapterFactory2, getSelectedStereotypes()));

		AdapterFactory adapterFactory3 = new ComposedAdapterFactory();
		selectedNegativeElementsTableViewer.setLabelProvider(getLabelProvider());
		selectedNegativeElementsTableViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory3));
		selectedNegativeElementsTableViewer.setInput(new ItemProvider(adapterFactory3, getSelectedNegativeStereotypes()));
	}
	
	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		super.refresh();
	}

	/**
	 * Get the LabelProvider to use to display the Object
	 * 
	 * @return ILabelProvider
	 */
	protected ILabelProvider getLabelProvider() {
		if (labelProvider == null) {

			labelProvider = new AdapterFactoryLabelProvider(new UMLItemProviderAdapterFactory()) {
				public String getColumnText(Object object, int columnIndex) {
					IItemQualifiedTextProvider itemQualifiedTextProvider = (IItemQualifiedTextProvider) adapterFactory
							.adapt(object, IItemQualifiedTextProvider.class);

					return itemQualifiedTextProvider != null ? itemQualifiedTextProvider.getQualifiedText(object)
							: super.getColumnText(object, columnIndex);
				}

				public String getText(Object object) {
					IItemQualifiedTextProvider itemQualifiedTextProvider = (IItemQualifiedTextProvider) adapterFactory
							.adapt(object, IItemQualifiedTextProvider.class);

					return itemQualifiedTextProvider != null ? itemQualifiedTextProvider.getQualifiedText(object)
							: super.getText(object);
				}
			};
		}

		return labelProvider;
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#aboutToBeShown()
	 */
	public void aboutToBeShown() {
		availableElementsTableViewer.addDoubleClickListener(availableElementsTableDoubleClickListener);
		selectedElementsTableViewer.addDoubleClickListener(selectedElementsTableDoubleClickListener);
		selectedNegativeElementsTableViewer.addDoubleClickListener(selectedNegativeElementsTableDoubleClickListener);
		
		addButton.addSelectionListener(addButtonSelectionAdapter);
		removeButton.addSelectionListener(removeButtonSelectionAdapter);
		addToNegativeButton.addSelectionListener(addToNegativeButtonSelectionAdapter);
		removeFromNegativeButton.addSelectionListener(removeFromNegativeButtonSelectionAdapter);
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#aboutToBeHidden()
	 */
	public void aboutToBeHidden() {
		availableElementsTableViewer.removeDoubleClickListener(availableElementsTableDoubleClickListener);
		selectedElementsTableViewer.removeDoubleClickListener(selectedElementsTableDoubleClickListener);
		selectedNegativeElementsTableViewer.removeDoubleClickListener(selectedNegativeElementsTableDoubleClickListener);

		if (!addButton.isDisposed()) {
			addButton.removeSelectionListener(addButtonSelectionAdapter);
			removeButton.removeSelectionListener(removeButtonSelectionAdapter);
			addToNegativeButton.removeSelectionListener(addToNegativeButtonSelectionAdapter);
			removeFromNegativeButton.removeSelectionListener(removeFromNegativeButtonSelectionAdapter);
		}
	}

	/**
	 * @generated
	 */
	protected AdapterFactory getAdapterFactory(Object object) {
		if (getEditingDomain() instanceof AdapterFactoryEditingDomain) {
			return ((AdapterFactoryEditingDomain) getEditingDomain()).getAdapterFactory();
		}
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(object);
		if (editingDomain != null) {
			return ((AdapterFactoryEditingDomain) editingDomain).getAdapterFactory();
		}
		return null;
	}

}