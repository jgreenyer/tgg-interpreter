package de.upb.swt.qvt.tgg.property.uml;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.provider.extension.IEReferenceProvider;

public class StereotypedNodeOutgoingEdgeTypeEReferenceChoicesProvider implements IEReferenceProvider{

	public EList<EReference> getValidEdgeTypeEReferences(Edge edge){
		EList<EReference> returnEReferenceList = new UniqueEList<EReference>();

		for (EObject valueObject : Helper.getNodesAppliedStereotypes(edge.getSourceNode())) {
			returnEReferenceList.addAll(((Stereotype)valueObject).getDefinition().getEAllReferences());		
		}
		return returnEReferenceList;
	}
	
}
