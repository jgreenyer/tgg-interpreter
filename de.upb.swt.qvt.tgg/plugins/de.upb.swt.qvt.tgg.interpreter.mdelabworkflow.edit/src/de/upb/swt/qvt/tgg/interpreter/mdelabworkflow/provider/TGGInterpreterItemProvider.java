/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.provider;


import de.mdelab.workflow.components.provider.WorkflowComponentItemProvider;

import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage;
import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TGGInterpreterItemProvider
	extends WorkflowComponentItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGInterpreterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourceModelSlotPropertyDescriptor(object);
			addTargetModelSlotPropertyDescriptor(object);
			addDirectionPropertyDescriptor(object);
			addRulesPropertyDescriptor(object);
			addCorrPackageSlotPropertyDescriptor(object);
			addCorrModelSlotPropertyDescriptor(object);
			addProtocolSlotPropertyDescriptor(object);
			addSynchronizePropertyDescriptor(object);
			addInterpreterSlotPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_sourceModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_sourceModelSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__SOURCE_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_targetModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_targetModelSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__TARGET_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_direction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_direction_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__DIRECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rules feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRulesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_rules_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_rules_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__RULES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Corr Package Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrPackageSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_corrPackageSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_corrPackageSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__CORR_PACKAGE_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Corr Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_corrModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_corrModelSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__CORR_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Protocol Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProtocolSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_protocolSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_protocolSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__PROTOCOL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synchronize feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSynchronizePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_synchronize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_synchronize_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__SYNCHRONIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interpreter Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInterpreterSlotPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGInterpreter_interpreterSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGInterpreter_interpreterSlot_feature", "_UI_TGGInterpreter_type"),
				 MdelabworkflowPackage.Literals.TGG_INTERPRETER__INTERPRETER_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns TGGInterpreter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TGGInterpreter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TGGInterpreter)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_TGGInterpreter_type") :
			getString("_UI_TGGInterpreter_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TGGInterpreter.class)) {
			case MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT:
			case MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT:
			case MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION:
			case MdelabworkflowPackage.TGG_INTERPRETER__RULES:
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT:
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT:
			case MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT:
			case MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE:
			case MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
