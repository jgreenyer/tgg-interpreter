package de.upb.swt.qvt.tgg.edit.command;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.upb.swt.qvt.tgg.Node;

public class SetNodeLeftCommand extends SetCommand {

	public SetNodeLeftCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		super(domain, owner, feature, value);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean doCanExecute() {
		
		Node node = (Node) owner;

		if (node.getRefinedNode().isLeft() != (Boolean) value){
			return false;
		}

		return super.doCanExecute();
	}

	
}
