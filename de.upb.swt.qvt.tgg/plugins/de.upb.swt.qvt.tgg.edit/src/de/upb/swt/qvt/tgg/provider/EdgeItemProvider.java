/**
 * <copyright>
 * </copyright>
 *
 * $Id: EdgeItemProvider.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.provider;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.provider.extension.IEReferenceProvider;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.Edge} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EdgeItemProvider
	extends GraphElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EdgeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTypeReferencePropertyDescriptor(object);
			addTargetNodePropertyDescriptor(object);
			addGraphPatternPropertyDescriptor(object);
			addTypedNamePropertyDescriptor(object);
			addSetModifierPropertyDescriptor(object);
			addLinkConstraintTypePropertyDescriptor(object);
			addDirectSuccessorPropertyDescriptor(object);
			addIndirectSuccessorPropertyDescriptor(object);
			addIndexExpressionPropertyDescriptor(object);
			addDirectPredecessorPropertyDescriptor(object);
			addIndirectPredecessorPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Type Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addTypeReferencePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
//			(createItemPropertyDescriptor
//				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
//				 getResourceLocator(),
//				 getString("_UI_Edge_typeReference_feature"),
//				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_typeReference_feature", "_UI_Edge_type"),
//				 TggPackage.Literals.EDGE__TYPE_REFERENCE,
//				 true,
//				 null,
//				 null,
//				 null));
		
		(
		 new ItemPropertyDescriptor(
				 ((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_typeReference_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_typeReference_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__TYPE_REFERENCE,
				 true,
				 null,
				 null,
				 null)
				 {
			 		public Collection<?> getChoiceOfValues(Object object){

			 			try{
				 			EList<EReference> choiceValueList = new BasicEList<EReference>();
				 			
				 			Edge edge = (Edge)object;
				 			
				 			Node sourceNode = edge.getSourceNode();
				 			Node targetNode = edge.getTargetNode();
				 			
//							System.out.println("sourceNode.getEType:    " + sourceNode.getEType());
//							System.out.println("sourceNode.getEType.getEPackage:    " + ((EClass)sourceNode.getEType()).getEPackage());
//							System.out.println("---");

							Iterator<EReference> sourceNodesClassOutgoingReferencesIterator = ((EClass)sourceNode.getEType()).getEAllReferences().iterator();
				 			while (sourceNodesClassOutgoingReferencesIterator.hasNext()) {
				 				EReference reference = sourceNodesClassOutgoingReferencesIterator.next();

//								System.out.println("reference:                                 " + reference);
//								System.out.println("reference.getEType():                      " + reference.getEType());
//								System.out.println("reference.getEType().getEPackage():        " + reference.getEType().getEPackage());
//								//System.out.println("---");
//								System.out.println("");

				 				
/*								if (sourceNode.getEType().getName().equals("PropertyCallExp") 
										&& reference.getName().equals("referredProperty")){
//									System.out.println("reference.getEType():    " + reference.getEType());
//									System.out.println("reference.getEType():        " + reference.getEType().getEPackage());
//									System.out.println("targetNode.getEType():    " + targetNode.getEType());
//									System.out.println("targetNode.getEType():        " + targetNode.getEType().getEPackage());
									for (Iterator<EClass> iterator = ((EClass)targetNode.getEType()).getEAllSuperTypes()
											.iterator(); iterator.hasNext();) {
										EClass object2 = iterator.next();
//										System.out.println("getEAllSuperTypes().:    " + object2);
//										System.out.println("getEAllSuperTypes().:        " + object2.getEPackage());
									}
									
								}
*/
				 				if (reference.getEReferenceType().isSuperTypeOf((EClass) targetNode.getEType())){
				 					choiceValueList.add(reference);
				 				}

//				 				if (reference.getEType().equals(targetNode.getEType()) || ((EClass)targetNode.getEType()).getEAllSuperTypes().contains(reference.getEType())){
//				 					choiceValueList.add(reference);
//								}
				 			}
				 			
				 			choiceValueList.addAll(getAdditionalChoiceEReferences(edge));
				 			
				 			return choiceValueList;
						} catch (NullPointerException e) {
							//nothing to worry about there. May occur if the necessary references have not been set up properly.
						} catch (ClassCastException e) {
							//nothing to worry about there. May occur if the necessary references have not been set up properly.
						}
						return super.getChoiceOfValues(object);
			 		}

				 }
		);

	}

	private EList<IEReferenceProvider> eReferenceProviders;

	private Collection<EReference> getAdditionalChoiceEReferences(Edge edge) {
		if (eReferenceProviders == null)
			loadEdgeTypeReferenceChoicesProvider();
		EList<EReference> choiceReferences = new UniqueEList<EReference>();
		for (IEReferenceProvider eReferenceProvider : eReferenceProviders) {
			choiceReferences.addAll(eReferenceProvider.getValidEdgeTypeEReferences(edge));
		}
		return choiceReferences;
	}
	
	private void loadEdgeTypeReferenceChoicesProvider() {
		eReferenceProviders = new BasicEList<IEReferenceProvider>();
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
		"de.upb.swt.qvt.tgg.edit.EdgeTypeReferenceChoicesProvider");
		for (IConfigurationElement configurationElement : config) {
			try {
				Object obj;
				obj = configurationElement.createExecutableExtension("EReferenceProvider");
				if (obj instanceof IEReferenceProvider) {
					eReferenceProviders.add((IEReferenceProvider) obj);
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	/**
	 * This adds a property descriptor for the Target Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_targetNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_targetNode_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__TARGET_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Graph Pattern feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGraphPatternPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_graphPattern_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_graphPattern_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__GRAPH_PATTERN,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Typed Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_typedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_typedName_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__TYPED_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Set Modifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSetModifierPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_setModifier_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_setModifier_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__SET_MODIFIER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Link Constraint Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLinkConstraintTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_linkConstraintType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_linkConstraintType_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__LINK_CONSTRAINT_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direct Successor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectSuccessorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_directSuccessor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_directSuccessor_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__DIRECT_SUCCESSOR,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Indirect Successor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndirectSuccessorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_indirectSuccessor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_indirectSuccessor_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__INDIRECT_SUCCESSOR,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Index Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndexExpressionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_indexExpression_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_indexExpression_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__INDEX_EXPRESSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direct Predecessor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectPredecessorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_directPredecessor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_directPredecessor_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__DIRECT_PREDECESSOR,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Indirect Predecessor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndirectPredecessorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Edge_indirectPredecessor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Edge_indirectPredecessor_feature", "_UI_Edge_type"),
				 TggPackage.Literals.EDGE__INDIRECT_PREDECESSOR,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns Edge.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Edge"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Edge)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Edge_type") :
			getString("_UI_Edge_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Edge.class)) {
			case TggPackage.EDGE__TYPED_NAME:
			case TggPackage.EDGE__SET_MODIFIER:
			case TggPackage.EDGE__LINK_CONSTRAINT_TYPE:
			case TggPackage.EDGE__INDEX_EXPRESSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
