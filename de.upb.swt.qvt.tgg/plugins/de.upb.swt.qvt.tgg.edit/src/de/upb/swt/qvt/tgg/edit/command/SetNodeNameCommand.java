package de.upb.swt.qvt.tgg.edit.command;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.upb.swt.qvt.tgg.Node;

public class SetNodeNameCommand extends SetCommand {

	public SetNodeNameCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		super(domain, owner, feature, value);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean doCanExecute() {
		
		Node node = (Node) owner;

		if (!node.getRefinedNode().getName().equals(value)){
			return false;
		}

		return super.doCanExecute();
	}
	
}
