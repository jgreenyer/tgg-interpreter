package de.upb.swt.qvt.tgg.edit.command;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class SetRefinedNodeCommand extends CompoundCommand {

	Node refiningNode;
	Node refinedNode;
	
	
	public SetRefinedNodeCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		refiningNode = (Node) owner;
		refinedNode = (Node) value;
		

		// copy name:
		append(new SetCommand(domain, owner, QvtbasePackage.Literals.NAMED_ELEMENT__NAME, refinedNode.getName()));

		// copy etype;
		if (refinedNode.getEType() != null){
			if(refiningNode.getEType() == null ||
					!(refinedNode.getEType().isSuperTypeOf(refiningNode.getEType())
							|| refinedNode.getEType().equals(refiningNode.getEType())  )){
				append(new SetCommand(domain, owner, TggPackage.Literals.NODE__ETYPE, refinedNode.getEType()));
			}
		}

		//left and right
		if (refiningNode.isRight() != refinedNode.isRight() ){
			append(new SetCommand(domain, owner, TggPackage.Literals.GRAPH_ELEMENT__RIGHT, refinedNode.isRight()));
		}
		if (refiningNode.isLeft() != refinedNode.isLeft() ){
			append(new SetCommand(domain, owner, TggPackage.Literals.GRAPH_ELEMENT__LEFT, refinedNode.isLeft()));
		}

		// copy domain graph pattern
		if (refiningNode.getDomainGraphPattern() == null ||
				refiningNode.getDomainGraphPattern().getTypedModel() != refinedNode.getDomainGraphPattern().getTypedModel()){
			
			if (refiningNode.getDomainGraphPattern() != null){
				append(new RemoveCommand(domain, owner, TggPackage.Literals.NODE__GRAPH_PATTERN, refiningNode.getDomainGraphPattern()));
			}
			append(new AddCommand(domain, owner, TggPackage.Literals.NODE__GRAPH_PATTERN, ((TripleGraphGrammarRule)refiningNode.getGraph()).getDomainGraphPattern(refinedNode.getDomainGraphPattern().getTypedModel())));
			
		}
		
		// finally the original SetCommand:
		append(new SetCommand(domain, owner, feature, value));
	}

}
