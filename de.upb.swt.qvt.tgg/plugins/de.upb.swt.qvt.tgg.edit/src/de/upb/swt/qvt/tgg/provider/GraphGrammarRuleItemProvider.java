/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.GraphGrammarRule} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class GraphGrammarRuleItemProvider
	extends GraphItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphGrammarRuleItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContextNodesPropertyDescriptor(object);
			addProducedNodesPropertyDescriptor(object);
			addNonproducedNodesPropertyDescriptor(object);
			addReusableNodesPropertyDescriptor(object);
			addRefinedRulePropertyDescriptor(object);
			addAllNodesPropertyDescriptor(object);
			addAllContextNodesPropertyDescriptor(object);
			addAllProducedNodesPropertyDescriptor(object);
			addAllNonproducedNodesPropertyDescriptor(object);
			addAllReusableNodesPropertyDescriptor(object);
			addAllRefinedRulesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Context Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContextNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_contextNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_contextNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__CONTEXT_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Produced Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProducedNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_producedNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_producedNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__PRODUCED_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Nonproduced Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNonproducedNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_nonproducedNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_nonproducedNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reusable Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReusableNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_reusableNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_reusableNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__REUSABLE_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Refined Rule feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRefinedRulePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_refinedRule_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_refinedRule_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__REFINED_RULE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Context Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllContextNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allContextNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allContextNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Produced Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllProducedNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allProducedNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allProducedNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Nonproduced Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllNonproducedNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allNonproducedNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allNonproducedNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Reusable Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllReusableNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allReusableNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allReusableNodes_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Refined Rules feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllRefinedRulesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_GraphGrammarRule_allRefinedRules_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_GraphGrammarRule_allRefinedRules_feature", "_UI_GraphGrammarRule_type"),
				 TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TggPackage.Literals.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN);
			childrenFeatures.add(TggPackage.Literals.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN);
			childrenFeatures.add(TggPackage.Literals.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns GraphGrammarRule.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/GraphGrammarRule"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((GraphGrammarRule)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_GraphGrammarRule_type") :
			getString("_UI_GraphGrammarRule_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(GraphGrammarRule.class)) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TggPackage.Literals.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN,
				 TggFactory.eINSTANCE.createLeftGraphPattern()));

		newChildDescriptors.add
			(createChildParameter
				(TggPackage.Literals.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN,
				 TggFactory.eINSTANCE.createRightGraphPattern()));

		newChildDescriptors.add
			(createChildParameter
				(TggPackage.Literals.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN,
				 TggFactory.eINSTANCE.createReusablePattern()));
	}

}
