package de.upb.swt.qvt.tgg.edit.command;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.upb.swt.qvt.tgg.Node;

public class SetNodeETypeCommand extends SetCommand {

	public SetNodeETypeCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		super(domain, owner, feature, value);
	}
	
	@Override
	public boolean doCanExecute() {
		
		if (owner instanceof Node) {
			Node node = (Node) owner;
			if (node.getRefinedNode() != null
					&& !node.getRefinedNode().getEType().isSuperTypeOf((EClass) value)){
				
//		        Status status = new Status(IStatus.ERROR, "My Plug-in ID", 0,
//		                "Status Error Message", null);
//
//				ErrorDialog.openError(null, 
//						"Invalid type", 
//						value + " is not equal to or a subclass of " + value, status);
				
				return false;
			}
		}
		return super.doCanExecute();
	}

}
