/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeItemProvider.java 1839 2007-03-17 14:28:39 +0000 (Sa, 17 Mrz 2007) jgreen $
 */
package de.upb.swt.qvt.tgg.provider;


import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.edit.command.SetNodeETypeCommand;
import de.upb.swt.qvt.tgg.edit.command.SetNodeLeftCommand;
import de.upb.swt.qvt.tgg.edit.command.SetNodeNameCommand;
import de.upb.swt.qvt.tgg.edit.command.SetNodeRightCommand;
import de.upb.swt.qvt.tgg.edit.command.SetRefinedNodeCommand;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.Node} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NodeItemProvider
	extends GraphElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	public Command createCommand(Object object, EditingDomain domain, Class<? extends Command> commandClass,
			CommandParameter commandParameter) {
		
		System.out.println(" ### object: " + object );
		System.out.println(" ### domain: " + domain );
		System.out.println(" ### commandClass: " + commandClass );
		System.out.println(" ### commandParameter: " + commandParameter );

		if (commandClass == SetCommand.class) {
			
		}
		
		// TODO Auto-generated method stub
		return super.createCommand(object, domain, commandClass, commandParameter);
	}
	
//	@Override
//	protected Command createSetCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
//		// TODO Auto-generated method stub
//		return super.createSetCommand(domain, owner, feature, value);
//	}
//	
	@Override
	protected Command createSetCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value,
			int index) {

		if (feature == TggPackage.Literals.NODE__REFINED_NODE  && value != null){
			return new SetRefinedNodeCommand(domain, owner, feature, value);
		} else if (((Node) owner).getRefinedNode() != null) {
			if (feature == TggPackage.Literals.NODE__ETYPE) {
				return new SetNodeETypeCommand(domain, owner, feature, value);
			} else if (feature == TggPackage.Literals.GRAPH_ELEMENT__LEFT) {
				return new SetNodeLeftCommand(domain, owner, feature, value);
			} else if (feature == TggPackage.Literals.GRAPH_ELEMENT__RIGHT) {
				return new SetNodeRightCommand(domain, owner, feature, value);
			} else if (feature == QvtbasePackage.Literals.NAMED_ELEMENT__NAME) {
				return new SetNodeNameCommand(domain, owner, feature, value);
			}
		}
		
		return super.createSetCommand(domain, owner, feature, value, index);
	}

	
	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIncomingEdgePropertyDescriptor(object);
			addGraphPatternPropertyDescriptor(object);
			addTypedNamePropertyDescriptor(object);
			addMatchSubtypePropertyDescriptor(object);
			addETypePropertyDescriptor(object);
			addRefinedNodePropertyDescriptor(object);
			addAllOutgoingEdgesPropertyDescriptor(object);
			addAllIncomingEdgesPropertyDescriptor(object);
			addAllRefinedNodesPropertyDescriptor(object);
			addAllGraphPatternsPropertyDescriptor(object);
			addMatchingPriorityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the EType feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addETypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
		(
				
//				createItemPropertyDescriptor
//			(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
//			 getResourceLocator(),
//			 getString("_UI_ETypedElement_eType_feature"),
//			 getString("_UI_PropertyDescriptor_description", "_UI_ETypedElement_eType_feature", "_UI_ETypedElement_type"),
//			 EcorePackage.Literals.ETYPED_ELEMENT__ETYPE,
//			 true,
//			 false,
//			 true,
//			 null,
//			 null,
//			 null));
				
				new ItemPropertyDescriptor(
						((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ETypedElement_eType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ETypedElement_eType_feature", "_UI_ETypedElement_type"),
						TggPackage.Literals.NODE__ETYPE,
				        true,
				        false,
				        true,
				        null,
				        null,
				        null){

					
					public Collection<?> getChoiceOfValues(Object object) {
						
						Node node = (Node) object;
						try {
							for (GraphPattern graphPattern : node.getGraphPattern()) {

								if (graphPattern instanceof DomainGraphPattern){
									TypedModel typedModel = ((DomainGraphPattern) graphPattern).getTypedModel();
									EList<EClass> eClassList = new UniqueEList<EClass>();
									// important to have an "empty" item, i.e. the value can be set to null
									eClassList.add(null);
									for (EPackage usedPackage : typedModel.getUsedPackage()) {
										for (EClassifier eClassifier : usedPackage.getEClassifiers()) {
											if (eClassifier instanceof EClass){
//												if (node.getRefinedNode() != null && node.getRefinedNode().getEType() != null){
//													if (((EClass) eClassifier).getEAllSuperTypes().contains(node.getRefinedNode().getEType())
//															|| eClassifier.equals(node.getRefinedNode().getEType()))
//														eClassList.add((EClass) eClassifier);														
//												}else
													eClassList.add((EClass) eClassifier);
											}
										}
									}
									return eClassList;
								}
							}
							
						} catch (NullPointerException e) {
							//nothing to worry about there. May occur if the necessary references have not been set up properly.
						}
						return Collections.EMPTY_LIST;
					}

				}
		);
	}

	/**
	 * This adds a property descriptor for the Refined Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addRefinedNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(
//			createItemPropertyDescriptor
//				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
//				 getResourceLocator(),
//				 getString("_UI_Node_refinedNode_feature"),
//				 getString("_UI_PropertyDescriptor_description", "_UI_Node_refinedNode_feature", "_UI_Node_type"),
//				 TggPackage.Literals.NODE__REFINED_NODE,
//				 true,
//				 false,
//				 true,
//				 null,
//				 null,
//				 null)

				new ItemPropertyDescriptor(
						((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Node_refinedNode_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Node_refinedNode_feature", "_UI_Node_type"),
						TggPackage.Literals.NODE__REFINED_NODE,
				         true,
				         false,
				         true,
				         null,
				         null,
				         null){
					
					public Collection<?> getChoiceOfValues(Object object) {
						
						Node node = (Node) object;
						TripleGraphGrammarRule tggRule =(TripleGraphGrammarRule)node.getGraph(); 
						if (tggRule.getRefinedRule() != null){
							if (node.getEType() != null){
								EList<Node> returnList = new UniqueEList<Node>();
								
								// important to have an "empty" item, i.e. the value can be set to null
								returnList.add(null);

								for (Node nodeOfRefinedRule : tggRule.getRefinedRule().getAllNodes()) {
//									if (node.getDomainGraphPattern() != null){
//										if (node.getDomainGraphPattern().getTypedModel() == nodeOfRefinedRule.getDomainGraphPattern().getTypedModel()){
//											returnList.add(nodeOfRefinedRule);
//										}
//									}else{
										returnList.add(nodeOfRefinedRule);
//									}
										
										
									
//									if (nodeOfRefinedRule.isContext() && nodeOfRefinedRule.isContext()
//											|| nodeOfRefinedRule.isProduced() && nodeOfRefinedRule.isProduced()
//											|| nodeOfRefinedRule.isReusable() && nodeOfRefinedRule.isReusable()){
//										if (nodeOfRefinedRule.getEType() != null 
//												&& (node.getEType().getEAllSuperTypes().contains(nodeOfRefinedRule.getEType())
//														|| node.getEType() == nodeOfRefinedRule.getEType()))
//											returnList.add(nodeOfRefinedRule);
//									}
								}
								return returnList;
							}else
								return tggRule.getRefinedRule().getAllNodes();
						}else
							return Collections.EMPTY_LIST;
					}

				}
			);
	}

	/**
	 * This adds a property descriptor for the All Outgoing Edges feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllOutgoingEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_allOutgoingEdges_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_allOutgoingEdges_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__ALL_OUTGOING_EDGES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Incoming Edges feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllIncomingEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_allIncomingEdges_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_allIncomingEdges_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__ALL_INCOMING_EDGES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Refined Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllRefinedNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_allRefinedNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_allRefinedNodes_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__ALL_REFINED_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the All Graph Patterns feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllGraphPatternsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_allGraphPatterns_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_allGraphPatterns_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__ALL_GRAPH_PATTERNS,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Matching Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMatchingPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_matchingPriority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_matchingPriority_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__MATCHING_PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Incoming Edge feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncomingEdgePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_incomingEdge_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_incomingEdge_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__INCOMING_EDGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Graph Pattern feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGraphPatternPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_graphPattern_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_graphPattern_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__GRAPH_PATTERN,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Match Subtype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMatchSubtypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_matchSubtype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_matchSubtype_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__MATCH_SUBTYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Typed Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Node_typedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Node_typedName_feature", "_UI_Node_type"),
				 TggPackage.Literals.NODE__TYPED_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}


	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TggPackage.Literals.NODE__OUTGOING_EDGE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Node.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Node"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Node)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Node_type") :
			getString("_UI_Node_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Node.class)) {
			case TggPackage.NODE__TYPED_NAME:
			case TggPackage.NODE__MATCH_SUBTYPE:
			case TggPackage.NODE__MATCHING_PRIORITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case TggPackage.NODE__OUTGOING_EDGE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TggPackage.Literals.NODE__OUTGOING_EDGE,
				 TggFactory.eINSTANCE.createEdge()));
	}

}
