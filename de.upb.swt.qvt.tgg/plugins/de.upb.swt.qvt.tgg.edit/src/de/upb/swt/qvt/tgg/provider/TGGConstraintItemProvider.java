/**
 * <copyright>
 * </copyright>
 *
 * $Id: TGGConstraintItemProvider.java 1632 2006-07-01 16:20:30 +0000 (Sa, 01 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.provider.extension.IEAttributeProvider;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.TGGConstraint} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TGGConstraintItemProvider
	extends ConstraintItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGConstraintItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSlotNodePropertyDescriptor(object);
			addSlotAttributePropertyDescriptor(object);
			addSlotAttributeNamePropertyDescriptor(object);
			addCheckonlyPropertyDescriptor(object);
			addEvaluateAfterMatchingPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Slot Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSlotNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGConstraint_slotNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_slotNode_feature", "_UI_TGGConstraint_type"),
				 TggPackage.Literals.TGG_CONSTRAINT__SLOT_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Slot Attribute feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addSlotAttributePropertyDescriptor(Object object) {
//		itemPropertyDescriptors.add
//			(createItemPropertyDescriptor
//				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
//				 getResourceLocator(),
//				 getString("_UI_TGGConstraint_slotAttribute_feature"),
//				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_slotAttribute_feature", "_UI_TGGConstraint_type"),
//				 TggPackage.Literals.TGG_CONSTRAINT__SLOT_ATTRIBUTE,
//				 true,
//				 null,
//				 null,
//				 null));
		itemPropertyDescriptors.add
		(
				new ItemPropertyDescriptor(
				 ((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGConstraint_slotAttribute_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_slotAttribute_feature", "_UI_TGGConstraint_type"),
				 TggPackage.Literals.TGG_CONSTRAINT__SLOT_ATTRIBUTE,
				 true,
				 null,
				 null,
				 null){

					public Collection<?> getChoiceOfValues(Object object) {
						TGGConstraint tggConstraint = (TGGConstraint) object;
						if (tggConstraint.getSlotNode() == null || tggConstraint.getSlotNode().getEType() == null) {
							return super.getChoiceOfValues(object);
						} else {
					        Collection<Object> result = new UniqueEList<Object>();
					        result.addAll(((EClass)tggConstraint.getSlotNode().getEType()).getEAllAttributes());
					        result.addAll(getAdditionalChoiceEAttributes(tggConstraint));
					        result.add(null);     
					        return result;
						}
					}

				}
			);
	}
	
	private EList<IEAttributeProvider> attributeProvider;

	private EList<EAttribute> getAdditionalChoiceEAttributes(TGGConstraint tggConstraint){
		if (attributeProvider == null)
			loadTGGConstraintSlotAttributeChoicesProvider();
		EList<EAttribute> choiceAttributes = new UniqueEList<EAttribute>();
		for (IEAttributeProvider eAttributeProvider : attributeProvider) {
			choiceAttributes.addAll(eAttributeProvider.getValidSlotAttributes(tggConstraint));
		}
		return choiceAttributes;
	}
	
	
	private void loadTGGConstraintSlotAttributeChoicesProvider(){
		attributeProvider = new BasicEList<IEAttributeProvider>();
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
		"de.upb.swt.qvt.tgg.edit.TGGConstraintSlotAttributeChoicesProvider");
		for (IConfigurationElement configurationElement : config) {
			try {
				Object obj;
				obj = configurationElement.createExecutableExtension("EAttributeProvider");
				if (obj instanceof IEAttributeProvider) {
					attributeProvider.add((IEAttributeProvider) obj);
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	


	/**
	 * This adds a property descriptor for the Slot Attribute Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSlotAttributeNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGConstraint_slotAttributeName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_slotAttributeName_feature", "_UI_TGGConstraint_type"),
				 TggPackage.Literals.TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Checkonly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCheckonlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGConstraint_checkonly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_checkonly_feature", "_UI_TGGConstraint_type"),
				 TggPackage.Literals.TGG_CONSTRAINT__CHECKONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Evaluate After Matching feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEvaluateAfterMatchingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGConstraint_evaluateAfterMatching_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGConstraint_evaluateAfterMatching_feature", "_UI_TGGConstraint_type"),
				 TggPackage.Literals.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TGGConstraint)object).getSlotAttributeName();
		return label == null || label.length() == 0 ?
			getString("_UI_TGGConstraint_type") :
			getString("_UI_TGGConstraint_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TGGConstraint.class)) {
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME:
			case TggPackage.TGG_CONSTRAINT__CHECKONLY:
			case TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
