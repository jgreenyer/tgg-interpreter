package de.upb.swt.qvt.tgg.provider.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;

import de.upb.swt.qvt.tgg.Edge;

public interface IEReferenceProvider {

	public EList<EReference> getValidEdgeTypeEReferences(Edge edge);
	
}
