package de.upb.swt.qvt.tgg.provider.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;

import de.upb.swt.qvt.tgg.TGGConstraint;

public interface IEAttributeProvider {

	public EList<EAttribute> getValidSlotAttributes(TGGConstraint tggConstraint);

}
