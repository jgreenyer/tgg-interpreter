/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage
 * @generated
 */
public interface MdelabworkflowFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MdelabworkflowFactory eINSTANCE = de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.MdelabworkflowFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TGG Interpreter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TGG Interpreter</em>'.
	 * @generated
	 */
	TGGInterpreter createTGGInterpreter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MdelabworkflowPackage getMdelabworkflowPackage();

} //MdelabworkflowFactory
