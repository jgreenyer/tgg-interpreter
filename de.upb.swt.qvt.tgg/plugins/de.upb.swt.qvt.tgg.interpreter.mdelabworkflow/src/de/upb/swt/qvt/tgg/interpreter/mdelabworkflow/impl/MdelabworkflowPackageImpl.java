/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowFactory;
import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage;
import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MdelabworkflowPackageImpl extends EPackageImpl implements MdelabworkflowPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggInterpreterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MdelabworkflowPackageImpl() {
		super(eNS_URI, MdelabworkflowFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MdelabworkflowPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MdelabworkflowPackage init() {
		if (isInited) return (MdelabworkflowPackage)EPackage.Registry.INSTANCE.getEPackage(MdelabworkflowPackage.eNS_URI);

		// Obtain or create and register package
		MdelabworkflowPackageImpl theMdelabworkflowPackage = (MdelabworkflowPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MdelabworkflowPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MdelabworkflowPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		WorkflowPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMdelabworkflowPackage.createPackageContents();

		// Initialize created meta-data
		theMdelabworkflowPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMdelabworkflowPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MdelabworkflowPackage.eNS_URI, theMdelabworkflowPackage);
		return theMdelabworkflowPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGInterpreter() {
		return tggInterpreterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_SourceModelSlot() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_TargetModelSlot() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_Direction() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_Rules() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_CorrPackageSlot() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_CorrModelSlot() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_ProtocolSlot() {
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_Synchronize()
	{
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGInterpreter_InterpreterSlot()
	{
		return (EAttribute)tggInterpreterEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MdelabworkflowFactory getMdelabworkflowFactory() {
		return (MdelabworkflowFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tggInterpreterEClass = createEClass(TGG_INTERPRETER);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__SOURCE_MODEL_SLOT);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__TARGET_MODEL_SLOT);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__DIRECTION);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__RULES);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__CORR_PACKAGE_SLOT);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__CORR_MODEL_SLOT);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__PROTOCOL_SLOT);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__SYNCHRONIZE);
		createEAttribute(tggInterpreterEClass, TGG_INTERPRETER__INTERPRETER_SLOT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tggInterpreterEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());

		// Initialize classes and features; add operations and parameters
		initEClass(tggInterpreterEClass, TGGInterpreter.class, "TGGInterpreter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTGGInterpreter_SourceModelSlot(), ecorePackage.getEString(), "sourceModelSlot", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_TargetModelSlot(), ecorePackage.getEString(), "targetModelSlot", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_Direction(), ecorePackage.getEString(), "direction", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_Rules(), ecorePackage.getEString(), "rules", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_CorrPackageSlot(), ecorePackage.getEString(), "corrPackageSlot", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_CorrModelSlot(), ecorePackage.getEString(), "corrModelSlot", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_ProtocolSlot(), ecorePackage.getEString(), "protocolSlot", null, 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_Synchronize(), theEcorePackage.getEBoolean(), "synchronize", "false", 1, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGInterpreter_InterpreterSlot(), theEcorePackage.getEString(), "interpreterSlot", "ruleBindings", 0, 1, TGGInterpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //MdelabworkflowPackageImpl
