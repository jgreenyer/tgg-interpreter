/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TGG Interpreter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getSourceModelSlot <em>Source Model Slot</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getTargetModelSlot <em>Target Model Slot</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getDirection <em>Direction</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getRules <em>Rules</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrPackageSlot <em>Corr Package Slot</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrModelSlot <em>Corr Model Slot</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getProtocolSlot <em>Protocol Slot</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#isSynchronize <em>Synchronize</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getInterpreterSlot <em>Interpreter Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter()
 * @model
 * @generated
 */
public interface TGGInterpreter extends WorkflowComponent {

	/**
	 * Returns the value of the '<em><b>Source Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model Slot</em>' attribute.
	 * @see #setSourceModelSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_SourceModelSlot()
	 * @model
	 * @generated
	 */
	String getSourceModelSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getSourceModelSlot <em>Source Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Model Slot</em>' attribute.
	 * @see #getSourceModelSlot()
	 * @generated
	 */
	void setSourceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Target Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Model Slot</em>' attribute.
	 * @see #setTargetModelSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_TargetModelSlot()
	 * @model
	 * @generated
	 */
	String getTargetModelSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getTargetModelSlot <em>Target Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Model Slot</em>' attribute.
	 * @see #getTargetModelSlot()
	 * @generated
	 */
	void setTargetModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see #setDirection(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_Direction()
	 * @model
	 * @generated
	 */
	String getDirection();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(String value);

	/**
	 * Returns the value of the '<em><b>Rules</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' attribute.
	 * @see #setRules(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_Rules()
	 * @model
	 * @generated
	 */
	String getRules();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getRules <em>Rules</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rules</em>' attribute.
	 * @see #getRules()
	 * @generated
	 */
	void setRules(String value);

	/**
	 * Returns the value of the '<em><b>Corr Package Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Package Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corr Package Slot</em>' attribute.
	 * @see #setCorrPackageSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_CorrPackageSlot()
	 * @model
	 * @generated
	 */
	String getCorrPackageSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrPackageSlot <em>Corr Package Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corr Package Slot</em>' attribute.
	 * @see #getCorrPackageSlot()
	 * @generated
	 */
	void setCorrPackageSlot(String value);

	/**
	 * Returns the value of the '<em><b>Corr Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corr Model Slot</em>' attribute.
	 * @see #setCorrModelSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_CorrModelSlot()
	 * @model
	 * @generated
	 */
	String getCorrModelSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrModelSlot <em>Corr Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corr Model Slot</em>' attribute.
	 * @see #getCorrModelSlot()
	 * @generated
	 */
	void setCorrModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Protocol Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protocol Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Slot</em>' attribute.
	 * @see #setProtocolSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_ProtocolSlot()
	 * @model
	 * @generated
	 */
	String getProtocolSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getProtocolSlot <em>Protocol Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Slot</em>' attribute.
	 * @see #getProtocolSlot()
	 * @generated
	 */
	void setProtocolSlot(String value);

	/**
	 * Returns the value of the '<em><b>Synchronize</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronize</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronize</em>' attribute.
	 * @see #setSynchronize(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_Synchronize()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isSynchronize();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#isSynchronize <em>Synchronize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronize</em>' attribute.
	 * @see #isSynchronize()
	 * @generated
	 */
	void setSynchronize(boolean value);

	/**
	 * Returns the value of the '<em><b>Interpreter Slot</b></em>' attribute.
	 * The default value is <code>"ruleBindings"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interpreter Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interpreter Slot</em>' attribute.
	 * @see #setInterpreterSlot(String)
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage#getTGGInterpreter_InterpreterSlot()
	 * @model default="ruleBindings"
	 * @generated
	 */
	String getInterpreterSlot();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getInterpreterSlot <em>Interpreter Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpreter Slot</em>' attribute.
	 * @see #getInterpreterSlot()
	 * @generated
	 */
	void setInterpreterSlot(String value);
} // TGGInterpreter
