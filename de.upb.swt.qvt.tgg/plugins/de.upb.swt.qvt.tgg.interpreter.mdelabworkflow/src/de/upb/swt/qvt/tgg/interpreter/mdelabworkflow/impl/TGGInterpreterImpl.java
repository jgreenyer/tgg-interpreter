/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl;

import java.io.IOException;
import java.util.AbstractList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowPackage;
import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Interpreter</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getSourceModelSlot
 * <em>Source Model Slot</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getTargetModelSlot
 * <em>Target Model Slot</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getDirection
 * <em>Direction</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getRules
 * <em>Rules</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getCorrPackageSlot
 * <em>Corr Package Slot</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getCorrModelSlot
 * <em>Corr Model Slot</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getProtocolSlot
 * <em>Protocol Slot</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#isSynchronize
 * <em>Synchronize</em>}</li>
 * <li>
 * {@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl#getInterpreterSlot
 * <em>Interpreter Slot</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TGGInterpreterImpl extends WorkflowComponentImpl implements TGGInterpreter
{
	/**
	 * The default value of the '{@link #getSourceModelSlot()
	 * <em>Source Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	SOURCE_MODEL_SLOT_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getSourceModelSlot()
	 * <em>Source Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				sourceModelSlot				= TGGInterpreterImpl.SOURCE_MODEL_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #getTargetModelSlot()
	 * <em>Target Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	TARGET_MODEL_SLOT_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getTargetModelSlot()
	 * <em>Target Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				targetModelSlot				= TGGInterpreterImpl.TARGET_MODEL_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final String	DIRECTION_EDEFAULT			= null;
	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected String				direction					= TGGInterpreterImpl.DIRECTION_EDEFAULT;
	/**
	 * The default value of the '{@link #getRules() <em>Rules</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected static final String	RULES_EDEFAULT				= null;
	/**
	 * The cached value of the '{@link #getRules() <em>Rules</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected String				rules						= TGGInterpreterImpl.RULES_EDEFAULT;
	/**
	 * The default value of the '{@link #getCorrPackageSlot()
	 * <em>Corr Package Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCorrPackageSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	CORR_PACKAGE_SLOT_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getCorrPackageSlot()
	 * <em>Corr Package Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCorrPackageSlot()
	 * @generated
	 * @ordered
	 */
	protected String				corrPackageSlot				= TGGInterpreterImpl.CORR_PACKAGE_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #getCorrModelSlot()
	 * <em>Corr Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCorrModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	CORR_MODEL_SLOT_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getCorrModelSlot()
	 * <em>Corr Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCorrModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				corrModelSlot				= TGGInterpreterImpl.CORR_MODEL_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #getProtocolSlot()
	 * <em>Protocol Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getProtocolSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	PROTOCOL_SLOT_EDEFAULT		= null;
	/**
	 * The cached value of the '{@link #getProtocolSlot()
	 * <em>Protocol Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getProtocolSlot()
	 * @generated
	 * @ordered
	 */
	protected String				protocolSlot				= TGGInterpreterImpl.PROTOCOL_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #isSynchronize() <em>Synchronize</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isSynchronize()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	SYNCHRONIZE_EDEFAULT		= false;
	/**
	 * The cached value of the '{@link #isSynchronize() <em>Synchronize</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isSynchronize()
	 * @generated
	 * @ordered
	 */
	protected boolean				synchronize					= TGGInterpreterImpl.SYNCHRONIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInterpreterSlot()
	 * <em>Interpreter Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInterpreterSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	INTERPRETER_SLOT_EDEFAULT	= "ruleBindings";
	/**
	 * The cached value of the '{@link #getInterpreterSlot()
	 * <em>Interpreter Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInterpreterSlot()
	 * @generated
	 * @ordered
	 */
	protected String				interpreterSlot				= TGGInterpreterImpl.INTERPRETER_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TGGInterpreterImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return MdelabworkflowPackage.Literals.TGG_INTERPRETER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getSourceModelSlot()
	{
		return this.sourceModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSourceModelSlot(final String newSourceModelSlot)
	{
		final String oldSourceModelSlot = this.sourceModelSlot;
		this.sourceModelSlot = newSourceModelSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT,
					oldSourceModelSlot, this.sourceModelSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getTargetModelSlot()
	{
		return this.targetModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setTargetModelSlot(final String newTargetModelSlot)
	{
		final String oldTargetModelSlot = this.targetModelSlot;
		this.targetModelSlot = newTargetModelSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT,
					oldTargetModelSlot, this.targetModelSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getDirection()
	{
		return this.direction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setDirection(final String newDirection)
	{
		final String oldDirection = this.direction;
		this.direction = newDirection;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION, oldDirection,
					this.direction));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getRules()
	{
		return this.rules;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setRules(final String newRules)
	{
		final String oldRules = this.rules;
		this.rules = newRules;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__RULES, oldRules, this.rules));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getCorrPackageSlot()
	{
		return this.corrPackageSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setCorrPackageSlot(final String newCorrPackageSlot)
	{
		final String oldCorrPackageSlot = this.corrPackageSlot;
		this.corrPackageSlot = newCorrPackageSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT,
					oldCorrPackageSlot, this.corrPackageSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getCorrModelSlot()
	{
		return this.corrModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setCorrModelSlot(final String newCorrModelSlot)
	{
		final String oldCorrModelSlot = this.corrModelSlot;
		this.corrModelSlot = newCorrModelSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT,
					oldCorrModelSlot, this.corrModelSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getProtocolSlot()
	{
		return this.protocolSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setProtocolSlot(final String newProtocolSlot)
	{
		final String oldProtocolSlot = this.protocolSlot;
		this.protocolSlot = newProtocolSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT,
					oldProtocolSlot, this.protocolSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean isSynchronize()
	{
		return this.synchronize;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSynchronize(final boolean newSynchronize)
	{
		final boolean oldSynchronize = this.synchronize;
		this.synchronize = newSynchronize;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE, oldSynchronize,
					this.synchronize));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getInterpreterSlot()
	{
		return this.interpreterSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setInterpreterSlot(final String newInterpreterSlot)
	{
		final String oldInterpreterSlot = this.interpreterSlot;
		this.interpreterSlot = newInterpreterSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT,
					oldInterpreterSlot, this.interpreterSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType)
	{
		switch (featureID)
		{
			case MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT:
				return this.getSourceModelSlot();
			case MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT:
				return this.getTargetModelSlot();
			case MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION:
				return this.getDirection();
			case MdelabworkflowPackage.TGG_INTERPRETER__RULES:
				return this.getRules();
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT:
				return this.getCorrPackageSlot();
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT:
				return this.getCorrModelSlot();
			case MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT:
				return this.getProtocolSlot();
			case MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE:
				return this.isSynchronize();
			case MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT:
				return this.getInterpreterSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(final int featureID, final Object newValue)
	{
		switch (featureID)
		{
			case MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT:
				this.setSourceModelSlot((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT:
				this.setTargetModelSlot((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION:
				this.setDirection((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__RULES:
				this.setRules((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT:
				this.setCorrPackageSlot((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT:
				this.setCorrModelSlot((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT:
				this.setProtocolSlot((String) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE:
				this.setSynchronize((Boolean) newValue);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT:
				this.setInterpreterSlot((String) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID)
	{
		switch (featureID)
		{
			case MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT:
				this.setSourceModelSlot(TGGInterpreterImpl.SOURCE_MODEL_SLOT_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT:
				this.setTargetModelSlot(TGGInterpreterImpl.TARGET_MODEL_SLOT_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION:
				this.setDirection(TGGInterpreterImpl.DIRECTION_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__RULES:
				this.setRules(TGGInterpreterImpl.RULES_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT:
				this.setCorrPackageSlot(TGGInterpreterImpl.CORR_PACKAGE_SLOT_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT:
				this.setCorrModelSlot(TGGInterpreterImpl.CORR_MODEL_SLOT_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT:
				this.setProtocolSlot(TGGInterpreterImpl.PROTOCOL_SLOT_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE:
				this.setSynchronize(TGGInterpreterImpl.SYNCHRONIZE_EDEFAULT);
				return;
			case MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT:
				this.setInterpreterSlot(TGGInterpreterImpl.INTERPRETER_SLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID)
	{
		switch (featureID)
		{
			case MdelabworkflowPackage.TGG_INTERPRETER__SOURCE_MODEL_SLOT:
				return TGGInterpreterImpl.SOURCE_MODEL_SLOT_EDEFAULT == null ? this.sourceModelSlot != null
						: !TGGInterpreterImpl.SOURCE_MODEL_SLOT_EDEFAULT.equals(this.sourceModelSlot);
			case MdelabworkflowPackage.TGG_INTERPRETER__TARGET_MODEL_SLOT:
				return TGGInterpreterImpl.TARGET_MODEL_SLOT_EDEFAULT == null ? this.targetModelSlot != null
						: !TGGInterpreterImpl.TARGET_MODEL_SLOT_EDEFAULT.equals(this.targetModelSlot);
			case MdelabworkflowPackage.TGG_INTERPRETER__DIRECTION:
				return TGGInterpreterImpl.DIRECTION_EDEFAULT == null ? this.direction != null : !TGGInterpreterImpl.DIRECTION_EDEFAULT
						.equals(this.direction);
			case MdelabworkflowPackage.TGG_INTERPRETER__RULES:
				return TGGInterpreterImpl.RULES_EDEFAULT == null ? this.rules != null : !TGGInterpreterImpl.RULES_EDEFAULT
						.equals(this.rules);
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_PACKAGE_SLOT:
				return TGGInterpreterImpl.CORR_PACKAGE_SLOT_EDEFAULT == null ? this.corrPackageSlot != null
						: !TGGInterpreterImpl.CORR_PACKAGE_SLOT_EDEFAULT.equals(this.corrPackageSlot);
			case MdelabworkflowPackage.TGG_INTERPRETER__CORR_MODEL_SLOT:
				return TGGInterpreterImpl.CORR_MODEL_SLOT_EDEFAULT == null ? this.corrModelSlot != null
						: !TGGInterpreterImpl.CORR_MODEL_SLOT_EDEFAULT.equals(this.corrModelSlot);
			case MdelabworkflowPackage.TGG_INTERPRETER__PROTOCOL_SLOT:
				return TGGInterpreterImpl.PROTOCOL_SLOT_EDEFAULT == null ? this.protocolSlot != null
						: !TGGInterpreterImpl.PROTOCOL_SLOT_EDEFAULT.equals(this.protocolSlot);
			case MdelabworkflowPackage.TGG_INTERPRETER__SYNCHRONIZE:
				return this.synchronize != TGGInterpreterImpl.SYNCHRONIZE_EDEFAULT;
			case MdelabworkflowPackage.TGG_INTERPRETER__INTERPRETER_SLOT:
				return TGGInterpreterImpl.INTERPRETER_SLOT_EDEFAULT == null ? this.interpreterSlot != null
						: !TGGInterpreterImpl.INTERPRETER_SLOT_EDEFAULT.equals(this.interpreterSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (this.eIsProxy())
		{
			return super.toString();
		}

		final StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourceModelSlot: ");
		result.append(this.sourceModelSlot);
		result.append(", targetModelSlot: ");
		result.append(this.targetModelSlot);
		result.append(", direction: ");
		result.append(this.direction);
		result.append(", rules: ");
		result.append(this.rules);
		result.append(", corrPackageSlot: ");
		result.append(this.corrPackageSlot);
		result.append(", corrModelSlot: ");
		result.append(this.corrModelSlot);
		result.append(", protocolSlot: ");
		result.append(this.protocolSlot);
		result.append(", synchronize: ");
		result.append(this.synchronize);
		result.append(", interpreterSlot: ");
		result.append(this.interpreterSlot);
		result.append(')');
		return result.toString();
	}

	@Override
	public void execute(final WorkflowExecutionContext context, final IProgressMonitor monitor) throws WorkflowExecutionException, IOException
	{
		Interpreter interpreter = null;

		if (this.isSynchronize())
		{
			interpreter = (Interpreter) context.getModelSlots().get(this.getInterpreterSlot());
		}
		else
		{

			final TripleGraphGrammar tgg = (TripleGraphGrammar) context.getModelSlots().get(this.getRules());

			// Iterator<TypedModel> typedModelIterator =
			// tgg.getAllModelParameters().iterator();
			// while (typedModelIterator.hasNext()) {
			// TypedModel typedModel = (TypedModel) typedModelIterator.next();
			// if (logger.isDebugEnabled()){
			// logger.debug("### " + typedModel.getUsedPackage());
			// }
			// Iterator<EPackage> usedPackageIterator =
			// typedModel.getUsedPackage().iterator();
			// while (usedPackageIterator.hasNext()) {
			// EPackage usedPackage = (EPackage) usedPackageIterator.next();
			// redirectPackageURI(usedPackage, context.getGlobalResourceSet());
			// }
			// }

			// EObject targetRootObject;
			EObject sourceRootObject;
			if ("BACKWARD".equals(this.getDirection()))
			{
				// targetRootObject = (EObject)
				// context.getModelSlots().get(getSourceModelSlot());
				final Object sourceRootJObject = context.getModelSlots().get(this.getTargetModelSlot());
				if (sourceRootJObject instanceof AbstractList<?>)
				{
					sourceRootObject = (EObject) ((AbstractList<?>) sourceRootJObject).get(0);
				}
				else
				{
					sourceRootObject = (EObject) sourceRootJObject;
				}
			}
			else
			{
				sourceRootObject = (EObject) context.getModelSlots().get(this.getSourceModelSlot());
				// targetRootObject = (EObject)
				// context.getModelSlots().get(getTargetModelSlot());
			}
			final EPackage corrPackage = (EPackage) context.getModelSlots().get(this.getCorrPackageSlot());

			// 1. Create configuration and set TGG
			final Configuration configuration = InterpreterconfigurationFactory.eINSTANCE.createConfiguration();
			configuration.setTripleGraphGrammar(tgg);
			// ResourceSet resourceSet =
			// sourceRootObject.eResource().getResourceSet();
			// Resource resource =
			// resourceSet.createResource(URI.createFileURI("config.interpreterconfig"));
			// resource.getContents().add(configuration);

			// 2. Create source domain model
			final DomainModel sourceDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
			// find correct typed model from the tgg
			for (final TypedModel typedModel : tgg.getAllModelParameters())
			{
				for (final EPackage ePackage : typedModel.getUsedPackage())
				{
					if (ePackage.equals(sourceRootObject.eClass().getEPackage()))
					{
						sourceDomainModel.setTypedModel(typedModel);
					}
				}
			}
			if (sourceDomainModel.getTypedModel() == null)
			{
				throw new IllegalArgumentException(
						"The TGG does not contain any typed model that fits the root object of the selected source model");
			}

			// add typed model
			configuration.getDomainModel().add(sourceDomainModel);

			// add root object
			sourceDomainModel.getRootObject().add(sourceRootObject);

			// 3. Create correspondence domain model
			final DomainModel correspondenceDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
			// find correct typed model from the tgg
			for (final TypedModel typedModel : tgg.getAllModelParameters())
			{
				for (final EPackage ePackage : typedModel.getUsedPackage())
				{
					if (ePackage.equals(corrPackage))
					{
						correspondenceDomainModel.setTypedModel(typedModel);
					}
				}
			}
			configuration.getDomainModel().add(correspondenceDomainModel);
			correspondenceDomainModel.getResourceURI().add("corr.xmi");

			// 4. Create target domain model
			final DomainModel targetDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
			// find correct typed model from the tgg
			for (final TypedModel typedModel : tgg.getAllModelParameters())
			{
				if (!typedModel.equals(sourceDomainModel.getTypedModel()) && !typedModel.equals(correspondenceDomainModel.getTypedModel()))
				{
					targetDomainModel.setTypedModel(typedModel);
				}
			}
			configuration.getDomainModel().add(targetDomainModel);
			configuration.setRecordRuleBindings(true);
			configuration.setRuleBindingContainer(InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer());

			targetDomainModel.getResourceURI().add("target.xmi");

			final ApplicationScenario scenario = InterpreterconfigurationFactory.eINSTANCE.createApplicationScenario();
			scenario.getSourceDomainModel().add(sourceDomainModel);
			scenario.getTargetDomainModel().add(targetDomainModel);
			scenario.getCorrespondenceModel().add(correspondenceDomainModel);
			scenario.setName("fwd");

			scenario.setMode(ApplicationMode.UPDATE);

			// scenario.setDeactivateFront(true);
			configuration.getApplicationScenario().add(scenario);
			configuration.setActiveApplicationScenario(scenario);

			// run interpreter
			interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
			interpreter.setConfiguration(configuration);
			interpreter.initializeConfiguration();

			context.getModelSlots().put(this.getInterpreterSlot(), interpreter);
		}

		// IStatus returnStatus =
		interpreter.performTransformation(monitor);

		if ("BACKWARD".equals(this.getDirection()))
		{
			context.getModelSlots().put(this.getSourceModelSlot(),
					interpreter.getConfiguration().getActiveApplicationScenario().getTargetDomainModel().get(0).getRootObject());
		}
		else
		{
			context.getModelSlots().put(this.getTargetModelSlot(),
					interpreter.getConfiguration().getActiveApplicationScenario().getTargetDomainModel().get(0).getRootObject());
		}
		context.getModelSlots().put(this.getCorrModelSlot(),
				interpreter.getConfiguration().getActiveApplicationScenario().getCorrespondenceModel().get(0).getRootObject());
	}

} // TGGInterpreterImpl
