/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl;

import de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MdelabworkflowFactoryImpl extends EFactoryImpl implements MdelabworkflowFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MdelabworkflowFactory init() {
		try
		{
			MdelabworkflowFactory theMdelabworkflowFactory = (MdelabworkflowFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.tgg.interpreter.mdelabworkflow"); 
			if (theMdelabworkflowFactory != null)
			{
				return theMdelabworkflowFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MdelabworkflowFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MdelabworkflowFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID())
		{
			case MdelabworkflowPackage.TGG_INTERPRETER: return createTGGInterpreter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGInterpreter createTGGInterpreter() {
		TGGInterpreterImpl tggInterpreter = new TGGInterpreterImpl();
		return tggInterpreter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MdelabworkflowPackage getMdelabworkflowPackage() {
		return (MdelabworkflowPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MdelabworkflowPackage getPackage() {
		return MdelabworkflowPackage.eINSTANCE;
	}

} //MdelabworkflowFactoryImpl
