/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.mdelabworkflow;

import de.mdelab.workflow.components.ComponentsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.MdelabworkflowFactory
 * @model kind="package"
 * @generated
 */
public interface MdelabworkflowPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mdelabworkflow";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.upb.swt.qvt.tgg.interpreter.mdelabworkflow";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.upb.swt.qvt.tgg.interpreter.mdelabworkflow";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MdelabworkflowPackage eINSTANCE = de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.MdelabworkflowPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl <em>TGG Interpreter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.MdelabworkflowPackageImpl#getTGGInterpreter()
	 * @generated
	 */
	int TGG_INTERPRETER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__SOURCE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__TARGET_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__DIRECTION = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__RULES = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Corr Package Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__CORR_PACKAGE_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Corr Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__CORR_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Protocol Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__PROTOCOL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Synchronize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__SYNCHRONIZE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Interpreter Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER__INTERPRETER_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>TGG Interpreter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_INTERPRETER_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 9;


	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter <em>TGG Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Interpreter</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter
	 * @generated
	 */
	EClass getTGGInterpreter();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getSourceModelSlot <em>Source Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Model Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getSourceModelSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_SourceModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getTargetModelSlot <em>Target Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Model Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getTargetModelSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_TargetModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getDirection()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_Direction();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rules</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getRules()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_Rules();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrPackageSlot <em>Corr Package Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Corr Package Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrPackageSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_CorrPackageSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrModelSlot <em>Corr Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Corr Model Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getCorrModelSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_CorrModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getProtocolSlot <em>Protocol Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getProtocolSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_ProtocolSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#isSynchronize <em>Synchronize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronize</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#isSynchronize()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_Synchronize();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getInterpreterSlot <em>Interpreter Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpreter Slot</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.TGGInterpreter#getInterpreterSlot()
	 * @see #getTGGInterpreter()
	 * @generated
	 */
	EAttribute getTGGInterpreter_InterpreterSlot();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MdelabworkflowFactory getMdelabworkflowFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl <em>TGG Interpreter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.TGGInterpreterImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.mdelabworkflow.impl.MdelabworkflowPackageImpl#getTGGInterpreter()
		 * @generated
		 */
		EClass TGG_INTERPRETER = eINSTANCE.getTGGInterpreter();
		/**
		 * The meta object literal for the '<em><b>Source Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__SOURCE_MODEL_SLOT = eINSTANCE.getTGGInterpreter_SourceModelSlot();
		/**
		 * The meta object literal for the '<em><b>Target Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__TARGET_MODEL_SLOT = eINSTANCE.getTGGInterpreter_TargetModelSlot();
		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__DIRECTION = eINSTANCE.getTGGInterpreter_Direction();
		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__RULES = eINSTANCE.getTGGInterpreter_Rules();
		/**
		 * The meta object literal for the '<em><b>Corr Package Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__CORR_PACKAGE_SLOT = eINSTANCE.getTGGInterpreter_CorrPackageSlot();
		/**
		 * The meta object literal for the '<em><b>Corr Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__CORR_MODEL_SLOT = eINSTANCE.getTGGInterpreter_CorrModelSlot();
		/**
		 * The meta object literal for the '<em><b>Protocol Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__PROTOCOL_SLOT = eINSTANCE.getTGGInterpreter_ProtocolSlot();
		/**
		 * The meta object literal for the '<em><b>Synchronize</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__SYNCHRONIZE = eINSTANCE.getTGGInterpreter_Synchronize();
		/**
		 * The meta object literal for the '<em><b>Interpreter Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_INTERPRETER__INTERPRETER_SLOT = eINSTANCE.getTGGInterpreter_InterpreterSlot();

	}

} //MdelabworkflowPackage
