/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.upb.swt.qvt.qvtbase.OCLVariable;
import de.upb.swt.qvt.qvtbase.QvtbasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OCL Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class OCLVariableImpl extends EObjectImpl implements OCLVariable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OCLVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QvtbasePackage.Literals.OCL_VARIABLE;
	}

} //OCLVariableImpl
