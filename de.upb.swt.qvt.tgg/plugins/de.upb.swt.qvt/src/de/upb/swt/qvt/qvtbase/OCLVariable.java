/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OCL Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Since thus far, it is not required for this model to have a
 * dependency to OCL, we simply use these dummy classes
 * here. The objective behind this is to keep the QVT-Base
 * package according to the QVT specification
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getOCLVariable()
 * @model
 * @generated
 */
public interface OCLVariable extends EObject {
} // OCLVariable
