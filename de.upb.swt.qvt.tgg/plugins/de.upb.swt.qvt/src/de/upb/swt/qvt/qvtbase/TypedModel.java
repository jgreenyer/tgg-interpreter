/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Typed Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.TypedModel#getTransformation <em>Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.TypedModel#getUsedPackage <em>Used Package</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.TypedModel#getDependsOn <em>Depends On</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.TypedModel#getDependent <em>Dependent</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTypedModel()
 * @model
 * @generated
 */
public interface TypedModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Transformation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getModelParameter <em>Model Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation</em>' container reference.
	 * @see #setTransformation(Transformation)
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTypedModel_Transformation()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getModelParameter
	 * @model opposite="modelParameter" transient="false"
	 * @generated
	 */
	Transformation getTransformation();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.qvtbase.TypedModel#getTransformation <em>Transformation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation</em>' container reference.
	 * @see #getTransformation()
	 * @generated
	 */
	void setTransformation(Transformation value);

	/**
	 * Returns the value of the '<em><b>Used Package</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Package</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Package</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTypedModel_UsedPackage()
	 * @model required="true"
	 * @generated
	 */
	EList<EPackage> getUsedPackage();

	/**
	 * Returns the value of the '<em><b>Depends On</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.TypedModel}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.TypedModel#getDependent <em>Dependent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depends On</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depends On</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTypedModel_DependsOn()
	 * @see de.upb.swt.qvt.qvtbase.TypedModel#getDependent
	 * @model opposite="dependent"
	 * @generated
	 */
	EList<TypedModel> getDependsOn();

	/**
	 * Returns the value of the '<em><b>Dependent</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.TypedModel}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.TypedModel#getDependsOn <em>Depends On</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependent</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTypedModel_Dependent()
	 * @see de.upb.swt.qvt.qvtbase.TypedModel#getDependsOn
	 * @model opposite="dependsOn"
	 * @generated
	 */
	EList<TypedModel> getDependent();

} // TypedModel
