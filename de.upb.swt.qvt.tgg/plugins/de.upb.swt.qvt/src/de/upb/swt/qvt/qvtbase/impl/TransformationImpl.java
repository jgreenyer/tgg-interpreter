/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EcoreFactoryImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.qvtbase.TypedModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getExtendedBy <em>Extended By</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getExtendsTransformation <em>Extends Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getModelParameter <em>Model Parameter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getRule <em>Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getChildTransformation <em>Child Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getParentTransformation <em>Parent Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getAllRules <em>All Rules</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.TransformationImpl#getAllModelParameters <em>All Model Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransformationImpl extends NamedElementImpl implements Transformation {
	/**
	 * The cached value of the '{@link #getExtendedBy() <em>Extended By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<Transformation> extendedBy;

	/**
	 * The cached value of the '{@link #getExtendsTransformation() <em>Extends Transformation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendsTransformation()
	 * @generated
	 * @ordered
	 */
	protected Transformation extendsTransformation;

	/**
	 * The cached value of the '{@link #getModelParameter() <em>Model Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedModel> modelParameter;

	/**
	 * The cached value of the '{@link #getRule() <em>Rule</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRule()
	 * @generated
	 * @ordered
	 */
	protected EList<Rule> rule;

	/**
	 * The cached value of the '{@link #getChildTransformation() <em>Child Transformation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildTransformation()
	 * @generated
	 * @ordered
	 */
	protected EList<Transformation> childTransformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QvtbasePackage.Literals.TRANSFORMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EFactory getEFactoryInstance() {
		//create dummy instance...
		return EcoreFactoryImpl.eINSTANCE.createEFactory();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transformation> getExtendedBy() {
		if (extendedBy == null) {
			extendedBy = new EObjectWithInverseResolvingEList<Transformation>(Transformation.class, this, QvtbasePackage.TRANSFORMATION__EXTENDED_BY, QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION);
		}
		return extendedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation getExtendsTransformation() {
		if (extendsTransformation != null && extendsTransformation.eIsProxy()) {
			InternalEObject oldExtendsTransformation = (InternalEObject)extendsTransformation;
			extendsTransformation = (Transformation)eResolveProxy(oldExtendsTransformation);
			if (extendsTransformation != oldExtendsTransformation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION, oldExtendsTransformation, extendsTransformation));
			}
		}
		return extendsTransformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation basicGetExtendsTransformation() {
		return extendsTransformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtendsTransformation(Transformation newExtendsTransformation, NotificationChain msgs) {
		Transformation oldExtendsTransformation = extendsTransformation;
		extendsTransformation = newExtendsTransformation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION, oldExtendsTransformation, newExtendsTransformation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendsTransformation(Transformation newExtendsTransformation) {
		if (newExtendsTransformation != extendsTransformation) {
			NotificationChain msgs = null;
			if (extendsTransformation != null)
				msgs = ((InternalEObject)extendsTransformation).eInverseRemove(this, QvtbasePackage.TRANSFORMATION__EXTENDED_BY, Transformation.class, msgs);
			if (newExtendsTransformation != null)
				msgs = ((InternalEObject)newExtendsTransformation).eInverseAdd(this, QvtbasePackage.TRANSFORMATION__EXTENDED_BY, Transformation.class, msgs);
			msgs = basicSetExtendsTransformation(newExtendsTransformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION, newExtendsTransformation, newExtendsTransformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypedModel> getModelParameter() {
		if (modelParameter == null) {
			modelParameter = new EObjectContainmentWithInverseEList.Resolving<TypedModel>(TypedModel.class, this, QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER, QvtbasePackage.TYPED_MODEL__TRANSFORMATION);
		}
		return modelParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Rule> getRule() {
		if (rule == null) {
			rule = new EObjectContainmentWithInverseEList.Resolving<Rule>(Rule.class, this, QvtbasePackage.TRANSFORMATION__RULE, QvtbasePackage.RULE__TRANSFORMATION);
		}
		return rule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transformation> getChildTransformation() {
		if (childTransformation == null) {
			childTransformation = new EObjectContainmentWithInverseEList.Resolving<Transformation>(Transformation.class, this, QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION, QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION);
		}
		return childTransformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation getParentTransformation() {
		if (eContainerFeatureID() != QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION) return null;
		return (Transformation)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation basicGetParentTransformation() {
		if (eContainerFeatureID() != QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION) return null;
		return (Transformation)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentTransformation(Transformation newParentTransformation, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentTransformation, QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentTransformation(Transformation newParentTransformation) {
		if (newParentTransformation != eInternalContainer() || (eContainerFeatureID() != QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION && newParentTransformation != null)) {
			if (EcoreUtil.isAncestor(this, newParentTransformation))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentTransformation != null)
				msgs = ((InternalEObject)newParentTransformation).eInverseAdd(this, QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION, Transformation.class, msgs);
			msgs = basicSetParentTransformation(newParentTransformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION, newParentTransformation, newParentTransformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Rule> getAllRules() {
		//TODO: Cache the list?
		BasicEList<Rule> rules = new BasicEList<Rule>();
		rules.addAll(getRule());
		for (Transformation childTrafo : getChildTransformation()) {
			rules.addAll(childTrafo.getAllRules());
		}

		return new EcoreEList.UnmodifiableEList<Rule>(this,
				QvtbasePackage.Literals.TRANSFORMATION__CHILD_TRANSFORMATION,
				rules.size(), rules.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TypedModel> getAllModelParameters() {
		//TODO: Cache the list?
		BasicEList<TypedModel> params = new BasicEList<TypedModel>();
		params.addAll(getModelParameter());
		if (getParentTransformation() != null)
			params.addAll(getParentTransformation().getAllModelParameters());

		return new EcoreEList.UnmodifiableEList<TypedModel>(this,
				QvtbasePackage.Literals.TRANSFORMATION__ALL_MODEL_PARAMETERS,
				params.size(), params.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendedBy()).basicAdd(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				if (extendsTransformation != null)
					msgs = ((InternalEObject)extendsTransformation).eInverseRemove(this, QvtbasePackage.TRANSFORMATION__EXTENDED_BY, Transformation.class, msgs);
				return basicSetExtendsTransformation((Transformation)otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getModelParameter()).basicAdd(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__RULE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRule()).basicAdd(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildTransformation()).basicAdd(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentTransformation((Transformation)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				return ((InternalEList<?>)getExtendedBy()).basicRemove(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				return basicSetExtendsTransformation(null, msgs);
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				return ((InternalEList<?>)getModelParameter()).basicRemove(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__RULE:
				return ((InternalEList<?>)getRule()).basicRemove(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				return ((InternalEList<?>)getChildTransformation()).basicRemove(otherEnd, msgs);
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				return basicSetParentTransformation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				return eInternalContainer().eInverseRemove(this, QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION, Transformation.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				return getExtendedBy();
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				if (resolve) return getExtendsTransformation();
				return basicGetExtendsTransformation();
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				return getModelParameter();
			case QvtbasePackage.TRANSFORMATION__RULE:
				return getRule();
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				return getChildTransformation();
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				if (resolve) return getParentTransformation();
				return basicGetParentTransformation();
			case QvtbasePackage.TRANSFORMATION__ALL_RULES:
				return getAllRules();
			case QvtbasePackage.TRANSFORMATION__ALL_MODEL_PARAMETERS:
				return getAllModelParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				getExtendedBy().clear();
				getExtendedBy().addAll((Collection<? extends Transformation>)newValue);
				return;
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				setExtendsTransformation((Transformation)newValue);
				return;
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				getModelParameter().clear();
				getModelParameter().addAll((Collection<? extends TypedModel>)newValue);
				return;
			case QvtbasePackage.TRANSFORMATION__RULE:
				getRule().clear();
				getRule().addAll((Collection<? extends Rule>)newValue);
				return;
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				getChildTransformation().clear();
				getChildTransformation().addAll((Collection<? extends Transformation>)newValue);
				return;
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				setParentTransformation((Transformation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				getExtendedBy().clear();
				return;
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				setExtendsTransformation((Transformation)null);
				return;
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				getModelParameter().clear();
				return;
			case QvtbasePackage.TRANSFORMATION__RULE:
				getRule().clear();
				return;
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				getChildTransformation().clear();
				return;
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				setParentTransformation((Transformation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QvtbasePackage.TRANSFORMATION__EXTENDED_BY:
				return extendedBy != null && !extendedBy.isEmpty();
			case QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION:
				return extendsTransformation != null;
			case QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER:
				return modelParameter != null && !modelParameter.isEmpty();
			case QvtbasePackage.TRANSFORMATION__RULE:
				return rule != null && !rule.isEmpty();
			case QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION:
				return childTransformation != null && !childTransformation.isEmpty();
			case QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION:
				return basicGetParentTransformation() != null;
			case QvtbasePackage.TRANSFORMATION__ALL_RULES:
				return !getAllRules().isEmpty();
			case QvtbasePackage.TRANSFORMATION__ALL_MODEL_PARAMETERS:
				return !getAllModelParameters().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransformationImpl
