/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Rule#getTransformation <em>Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Rule#getDomain <em>Domain</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Rule#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Rule#getOverridden <em>Overridden</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getRule()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniqueName'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL uniqueName='transformation.rule->select(r | r.name=self.name)->size() < 2'"
 * @generated
 */
public interface Rule extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Transformation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation</em>' container reference.
	 * @see #setTransformation(Transformation)
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getRule_Transformation()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getRule
	 * @model opposite="rule" transient="false"
	 * @generated
	 */
	Transformation getTransformation();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.qvtbase.Rule#getTransformation <em>Transformation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation</em>' container reference.
	 * @see #getTransformation()
	 * @generated
	 */
	void setTransformation(Transformation value);

	/**
	 * Returns the value of the '<em><b>Domain</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Domain}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Domain#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getRule_Domain()
	 * @see de.upb.swt.qvt.qvtbase.Domain#getRule
	 * @model opposite="rule" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Domain> getDomain();

	/**
	 * Returns the value of the '<em><b>Overrides</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Rule#getOverridden <em>Overridden</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overrides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overrides</em>' reference.
	 * @see #setOverrides(Rule)
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getRule_Overrides()
	 * @see de.upb.swt.qvt.qvtbase.Rule#getOverridden
	 * @model opposite="overridden"
	 * @generated
	 */
	Rule getOverrides();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.qvtbase.Rule#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overrides</em>' reference.
	 * @see #getOverrides()
	 * @generated
	 */
	void setOverrides(Rule value);

	/**
	 * Returns the value of the '<em><b>Overridden</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Rule}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Rule#getOverrides <em>Overrides</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getRule_Overridden()
	 * @see de.upb.swt.qvt.qvtbase.Rule#getOverrides
	 * @model opposite="overrides"
	 * @generated
	 */
	EList<Rule> getOverridden();

} // Rule
