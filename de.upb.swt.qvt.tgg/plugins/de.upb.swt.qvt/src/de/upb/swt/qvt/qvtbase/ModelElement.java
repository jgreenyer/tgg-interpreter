/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getModelElement()
 * @model
 * @generated
 */
public interface ModelElement extends AnnotatedElement {

} // ModelElement
