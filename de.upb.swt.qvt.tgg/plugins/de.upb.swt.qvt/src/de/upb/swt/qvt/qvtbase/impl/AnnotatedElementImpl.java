/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.AnnotatedElement;
import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.qvtbase.QvtbasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotated Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.AnnotatedElementImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.impl.AnnotatedElementImpl#getAnnotationString <em>Annotation String</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AnnotatedElementImpl extends EObjectImpl implements AnnotatedElement {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The default value of the '{@link #getAnnotationString() <em>Annotation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationString()
	 * @generated
	 * @ordered
	 */
	protected static final String ANNOTATION_STRING_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotatedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QvtbasePackage.Literals.ANNOTATED_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList.Resolving<Annotation>(Annotation.class, this, QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getAnnotationString() {
		String returnString = "";
		for (Annotation annotation : getAnnotations()) {
			for (Annotation subAnnotation : annotation.getAnnotations()) {
				for (EObject eObj : subAnnotation.getValue()) {
					for (EAttribute eAttribute : eObj.eClass().getEAllAttributes()) {
						if (eAttribute.getName().equals("name")){
							if ("doNotMatch".equals(subAnnotation.getKey()))
								returnString += "not ";
							returnString += "<<" + (String)eObj.eGet(eAttribute) + ">>\n";
						}
					}
				}
			}
		}
		return returnString;
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Annotation> getAnnotationsByKeyString(String keyString) {
		EList<Annotation> annotationsByKeyStringList = new BasicEList<Annotation>();

		for (Annotation annotation : getAnnotations()) {
			if (annotation.getKey().equals(keyString)){
				annotationsByKeyStringList.add(annotation);
			}
		}

		return annotationsByKeyStringList;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS:
				return getAnnotations();
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATION_STRING:
				return getAnnotationString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS:
				getAnnotations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATION_STRING:
				return ANNOTATION_STRING_EDEFAULT == null ? getAnnotationString() != null : !ANNOTATION_STRING_EDEFAULT.equals(getAnnotationString());
		}
		return super.eIsSet(featureID);
	}

} //AnnotatedElementImpl
