/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getExtendedBy <em>Extended By</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getExtendsTransformation <em>Extends Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getModelParameter <em>Model Parameter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getRule <em>Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getChildTransformation <em>Child Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getParentTransformation <em>Parent Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getAllRules <em>All Rules</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Transformation#getAllModelParameters <em>All Model Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation()
 * @model
 * @generated
 */
public interface Transformation extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Extended By</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Transformation}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getExtendsTransformation <em>Extends Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended By</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_ExtendedBy()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getExtendsTransformation
	 * @model opposite="extendsTransformation"
	 * @generated
	 */
	EList<Transformation> getExtendedBy();

	/**
	 * Returns the value of the '<em><b>Extends Transformation</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getExtendedBy <em>Extended By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends Transformation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends Transformation</em>' reference.
	 * @see #setExtendsTransformation(Transformation)
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_ExtendsTransformation()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getExtendedBy
	 * @model opposite="extendedBy"
	 * @generated
	 */
	Transformation getExtendsTransformation();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.qvtbase.Transformation#getExtendsTransformation <em>Extends Transformation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extends Transformation</em>' reference.
	 * @see #getExtendsTransformation()
	 * @generated
	 */
	void setExtendsTransformation(Transformation value);

	/**
	 * Returns the value of the '<em><b>Model Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.TypedModel}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.TypedModel#getTransformation <em>Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Parameter</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_ModelParameter()
	 * @see de.upb.swt.qvt.qvtbase.TypedModel#getTransformation
	 * @model opposite="transformation" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<TypedModel> getModelParameter();

	/**
	 * Returns the value of the '<em><b>Rule</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Rule}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Rule#getTransformation <em>Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_Rule()
	 * @see de.upb.swt.qvt.qvtbase.Rule#getTransformation
	 * @model opposite="transformation" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Rule> getRule();

	/**
	 * Returns the value of the '<em><b>Child Transformation</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Transformation}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getParentTransformation <em>Parent Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Transformation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Transformation</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_ChildTransformation()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getParentTransformation
	 * @model opposite="parentTransformation" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Transformation> getChildTransformation();

	/**
	 * Returns the value of the '<em><b>Parent Transformation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Transformation#getChildTransformation <em>Child Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Transformation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Transformation</em>' container reference.
	 * @see #setParentTransformation(Transformation)
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_ParentTransformation()
	 * @see de.upb.swt.qvt.qvtbase.Transformation#getChildTransformation
	 * @model opposite="childTransformation" transient="false"
	 * @generated
	 */
	Transformation getParentTransformation();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.qvtbase.Transformation#getParentTransformation <em>Parent Transformation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Transformation</em>' container reference.
	 * @see #getParentTransformation()
	 * @generated
	 */
	void setParentTransformation(Transformation value);

	/**
	 * Returns the value of the '<em><b>All Rules</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Rule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Rules</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Rules</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_AllRules()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Rule> getAllRules();

	/**
	 * Returns the value of the '<em><b>All Model Parameters</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.TypedModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Model Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Model Parameters</em>' reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getTransformation_AllModelParameters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<TypedModel> getAllModelParameters();

} // Transformation
