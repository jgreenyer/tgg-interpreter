/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Pattern#getBindsTo <em>Binds To</em>}</li>
 *   <li>{@link de.upb.swt.qvt.qvtbase.Pattern#getPredicate <em>Predicate</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getPattern()
 * @model
 * @generated
 */
public interface Pattern extends EObject {
	/**
	 * Returns the value of the '<em><b>Binds To</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.OCLVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binds To</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binds To</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getPattern_BindsTo()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<OCLVariable> getBindsTo();

	/**
	 * Returns the value of the '<em><b>Predicate</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.qvtbase.Predicate}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.qvtbase.Predicate#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate</em>' containment reference list.
	 * @see de.upb.swt.qvt.qvtbase.QvtbasePackage#getPattern_Predicate()
	 * @see de.upb.swt.qvt.qvtbase.Predicate#getPattern
	 * @model opposite="pattern" containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<Predicate> getPredicate();

} // Pattern
