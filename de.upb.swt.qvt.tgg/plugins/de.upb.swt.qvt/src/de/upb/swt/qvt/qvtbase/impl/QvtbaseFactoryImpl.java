/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.qvtbase.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.swt.qvt.qvtbase.AnnotatedElement;
import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.qvtbase.ModelElement;
import de.upb.swt.qvt.qvtbase.NamedElement;
import de.upb.swt.qvt.qvtbase.OCLExpression;
import de.upb.swt.qvt.qvtbase.OCLVariable;
import de.upb.swt.qvt.qvtbase.Pattern;
import de.upb.swt.qvt.qvtbase.Predicate;
import de.upb.swt.qvt.qvtbase.QvtbaseFactory;
import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.qvtbase.TypedModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class QvtbaseFactoryImpl extends EFactoryImpl implements QvtbaseFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static QvtbaseFactory init() {
		try {
			QvtbaseFactory theQvtbaseFactory = (QvtbaseFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.qvtbase"); 
			if (theQvtbaseFactory != null) {
				return theQvtbaseFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new QvtbaseFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QvtbaseFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case QvtbasePackage.TRANSFORMATION: return createTransformation();
			case QvtbasePackage.TYPED_MODEL: return createTypedModel();
			case QvtbasePackage.PATTERN: return createPattern();
			case QvtbasePackage.PREDICATE: return createPredicate();
			case QvtbasePackage.OCL_VARIABLE: return createOCLVariable();
			case QvtbasePackage.OCL_EXPRESSION: return createOCLExpression();
			case QvtbasePackage.NAMED_ELEMENT: return createNamedElement();
			case QvtbasePackage.MODEL_ELEMENT: return createModelElement();
			case QvtbasePackage.ANNOTATION: return createAnnotation();
			case QvtbasePackage.ANNOTATED_ELEMENT: return createAnnotatedElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation createTransformation() {
		TransformationImpl transformation = new TransformationImpl();
		return transformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedModel createTypedModel() {
		TypedModelImpl typedModel = new TypedModelImpl();
		return typedModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Pattern createPattern() {
		PatternImpl pattern = new PatternImpl();
		return pattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Predicate createPredicate() {
		PredicateImpl predicate = new PredicateImpl();
		return predicate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLVariable createOCLVariable() {
		OCLVariableImpl oclVariable = new OCLVariableImpl();
		return oclVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLExpression createOCLExpression() {
		OCLExpressionImpl oclExpression = new OCLExpressionImpl();
		return oclExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement createNamedElement() {
		NamedElementImpl namedElement = new NamedElementImpl();
		return namedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement createModelElement() {
		ModelElementImpl modelElement = new ModelElementImpl();
		return modelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement createAnnotatedElement() {
		AnnotatedElementImpl annotatedElement = new AnnotatedElementImpl();
		return annotatedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QvtbasePackage getQvtbasePackage() {
		return (QvtbasePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static QvtbasePackage getPackage() {
		return QvtbasePackage.eINSTANCE;
	}

} //QvtbaseFactoryImpl
