package de.upb.swt.qvt.tgg.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintExpressionEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintSlotAttributeNameEditPart;
import de.upb.swt.qvt.tgg.diagram.parsers.MessageFormatParser;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser nodeAnnotationStringNameTypedName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getNodeAnnotationStringNameTypedName_5001Parser() {
		if (nodeAnnotationStringNameTypedName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE
							.getAnnotatedElement_AnnotationString(),
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getNode_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}{1}:{2}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			nodeAnnotationStringNameTypedName_5001Parser = parser;
		}
		return nodeAnnotationStringNameTypedName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser nodeAnnotationStringNameTypedName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getNodeAnnotationStringNameTypedName_5002Parser() {
		if (nodeAnnotationStringNameTypedName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE
							.getAnnotatedElement_AnnotationString(),
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getNode_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}{1}:{2}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			nodeAnnotationStringNameTypedName_5002Parser = parser;
		}
		return nodeAnnotationStringNameTypedName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser nodeAnnotationStringNameTypedName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getNodeAnnotationStringNameTypedName_5003Parser() {
		if (nodeAnnotationStringNameTypedName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE
							.getAnnotatedElement_AnnotationString(),
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getNode_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}{1}:{2}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			nodeAnnotationStringNameTypedName_5003Parser = parser;
		}
		return nodeAnnotationStringNameTypedName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser domainGraphPatternName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getDomainGraphPatternName_5004Parser() {
		if (domainGraphPatternName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			domainGraphPatternName_5004Parser = parser;
		}
		return domainGraphPatternName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser oCLConstraintSlotAttributeName_5013Parser;

	/**
	 * @generated
	 */
	private IParser getOCLConstraintSlotAttributeName_5013Parser() {
		if (oCLConstraintSlotAttributeName_5013Parser == null) {
			EAttribute[] features = new EAttribute[] { TggPackage.eINSTANCE
					.getTGGConstraint_SlotAttributeName() };
			MessageFormatParser parser = new MessageFormatParser(features);
			oCLConstraintSlotAttributeName_5013Parser = parser;
		}
		return oCLConstraintSlotAttributeName_5013Parser;
	}

	/**
	 * @generated
	 */
	private IParser oCLConstraintExpression_5014Parser;

	/**
	 * @generated
	 */
	private IParser getOCLConstraintExpression_5014Parser() {
		if (oCLConstraintExpression_5014Parser == null) {
			EAttribute[] features = new EAttribute[] { TggPackage.eINSTANCE
					.getOCLConstraint_Expression() };
			MessageFormatParser parser = new MessageFormatParser(features);
			oCLConstraintExpression_5014Parser = parser;
		}
		return oCLConstraintExpression_5014Parser;
	}

	/**
	 * @generated
	 */
	private IParser edgeNameTypedName_6001Parser;

	/**
	 * @generated
	 */
	private IParser getEdgeNameTypedName_6001Parser() {
		if (edgeNameTypedName_6001Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getEdge_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			edgeNameTypedName_6001Parser = parser;
		}
		return edgeNameTypedName_6001Parser;
	}

	/**
	 * @generated
	 */
	private IParser edgeNameTypedName_6003Parser;

	/**
	 * @generated
	 */
	private IParser getEdgeNameTypedName_6003Parser() {
		if (edgeNameTypedName_6003Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getEdge_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			edgeNameTypedName_6003Parser = parser;
		}
		return edgeNameTypedName_6003Parser;
	}

	/**
	 * @generated
	 */
	private IParser edgeNameTypedName_6005Parser;

	/**
	 * @generated
	 */
	private IParser getEdgeNameTypedName_6005Parser() {
		if (edgeNameTypedName_6005Parser == null) {
			EAttribute[] features = new EAttribute[] {
					QvtbasePackage.eINSTANCE.getNamedElement_Name(),
					TggPackage.eINSTANCE.getEdge_TypedName() };
			EAttribute[] editableFeatures = new EAttribute[] { QvtbasePackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			edgeNameTypedName_6005Parser = parser;
		}
		return edgeNameTypedName_6005Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case NodeNameTypedNameEditPart.VISUAL_ID:
			return getNodeAnnotationStringNameTypedName_5001Parser();
		case NodeNameTypedName2EditPart.VISUAL_ID:
			return getNodeAnnotationStringNameTypedName_5002Parser();
		case NodeNameTypedName3EditPart.VISUAL_ID:
			return getNodeAnnotationStringNameTypedName_5003Parser();
		case DomainGraphPatternNameEditPart.VISUAL_ID:
			return getDomainGraphPatternName_5004Parser();
		case OCLConstraintSlotAttributeNameEditPart.VISUAL_ID:
			return getOCLConstraintSlotAttributeName_5013Parser();
		case OCLConstraintExpressionEditPart.VISUAL_ID:
			return getOCLConstraintExpression_5014Parser();
		case EdgeNameTypedNameEditPart.VISUAL_ID:
			return getEdgeNameTypedName_6001Parser();
		case EdgeNameTypedName2EditPart.VISUAL_ID:
			return getEdgeNameTypedName_6003Parser();
		case EdgeNameTypedName3EditPart.VISUAL_ID:
			return getEdgeNameTypedName_6005Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * 
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(TggVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(TggVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (TggElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}