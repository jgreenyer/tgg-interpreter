package de.upb.swt.qvt.tgg.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TggCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TggCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TggCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TggCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TggCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TggCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TggCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TggCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TggDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TggDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TggDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TggDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TggDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TggNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TggDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TggDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TggDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TggDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TggDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TggElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String GraphElements1Group_title;

	/**
	 * @generated
	 */
	public static String Constraints2Group_title;

	/**
	 * @generated
	 */
	public static String ContextNode1CreationTool_title;

	/**
	 * @generated
	 */
	public static String ContextNode1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ProducedNode2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ProducedNode2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ReusableNode3CreationTool_title;

	/**
	 * @generated
	 */
	public static String ReusableNode3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ReusablePattern4CreationTool_title;

	/**
	 * @generated
	 */
	public static String ReusablePattern4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ContextEdge6CreationTool_title;

	/**
	 * @generated
	 */
	public static String ContextEdge6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ProducedEdge7CreationTool_title;

	/**
	 * @generated
	 */
	public static String ProducedEdge7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ReusableEdge8CreationTool_title;

	/**
	 * @generated
	 */
	public static String ReusableEdge8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String DomainGraphPattern10CreationTool_title;

	/**
	 * @generated
	 */
	public static String DomainGraphPattern10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String GraphPatternToNodeLink11CreationTool_title;

	/**
	 * @generated
	 */
	public static String GraphPatternToNodeLink11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String OCLAttributeConstraint1CreationTool_title;

	/**
	 * @generated
	 */
	public static String OCLAttributeConstraint1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ConstraintToSlotNodeLink2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ConstraintToSlotNodeLink2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TripleGraphGrammarRule_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ReusablePattern_2011_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Node_2003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_DomainGraphPattern_2005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_OCLConstraint_2010_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4007_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4007_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4008_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4008_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_NodeGraphPattern_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_NodeGraphPattern_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TGGConstraintSlotNode_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TGGConstraintSlotNode_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TggModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TggModelingAssistantProviderMessage;

	// TODO: put accessor fields manually
}
