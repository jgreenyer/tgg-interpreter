package de.upb.swt.qvt.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class NodeGraphPatternItemSemanticEditPolicy extends
		TggBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public NodeGraphPatternItemSemanticEditPolicy() {
		super(TggElementTypes.NodeGraphPattern_4004);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
