package de.upb.swt.qvt.tgg.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;

public interface ITGGNodeEditPart {
	
	public void unregisterVisuals();
	
	public void registerVisuals();	
	
	public void refreshVisuals();

	public IFigure getFigure();	
}
