package de.upb.swt.qvt.tgg.diagram.edit.parts;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.diagram.edit.policies.Edge3ItemSemanticEditPolicy;

/**
 * @generated
 */
public class Edge3EditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4008;

	/**
	 * @generated
	 */
	public Edge3EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Edge3ItemSemanticEditPolicy());
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof EdgeNameTypedName3EditPart) {
			((EdgeNameTypedName3EditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureEdgeTypedNameLabelFigure());
			return true;
		}
		if (childEditPart instanceof WrappingLabel3EditPart) {
			((WrappingLabel3EditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureEdgeGraphGrammarSideLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof EdgeNameTypedName3EditPart) {
			return true;
		}
		if (childEditPart instanceof WrappingLabel3EditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new ReusedTGGEdgeFigure();
	}

	/**
	 * @generated
	 */
	public ReusedTGGEdgeFigure getPrimaryShape() {
		return (ReusedTGGEdgeFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class ReusedTGGEdgeFigure extends PolylineConnectionEx {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureEdgeGraphGrammarSideLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureEdgeTypedNameLabelFigure;

		/**
		 * @generated
		 */
		public ReusedTGGEdgeFigure() {
			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);

			this.setFont(THIS_FONT);

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureEdgeGraphGrammarSideLabelFigure = new WrappingLabel();
			fFigureEdgeGraphGrammarSideLabelFigure.setText("##");
			fFigureEdgeGraphGrammarSideLabelFigure
					.setForegroundColor(FFIGUREEDGEGRAPHGRAMMARSIDELABELFIGURE_FORE);

			this.add(fFigureEdgeGraphGrammarSideLabelFigure);

			fFigureEdgeTypedNameLabelFigure = new WrappingLabel();
			fFigureEdgeTypedNameLabelFigure.setText("typedName");
			fFigureEdgeTypedNameLabelFigure
					.setForegroundColor(FFIGUREEDGETYPEDNAMELABELFIGURE_FORE);

			this.add(fFigureEdgeTypedNameLabelFigure);

		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			PointList pl = new PointList();
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(2));
			pl.addPoint(getMapMode().DPtoLP(0), getMapMode().DPtoLP(0));
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(-2));
			df.setTemplate(pl);
			df.setScale(getMapMode().DPtoLP(7), getMapMode().DPtoLP(3));
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureEdgeGraphGrammarSideLabelFigure() {
			return fFigureEdgeGraphGrammarSideLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureEdgeTypedNameLabelFigure() {
			return fFigureEdgeTypedNameLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 100, 100, 100);

	/**
	 * @generated
	 */
	static final Font THIS_FONT = new Font(Display.getCurrent(), Display
			.getDefault().getSystemFont().getFontData()[0].getName(), 8,
			SWT.NORMAL);

	/**
	 * @generated
	 */
	static final Color FFIGUREEDGEGRAPHGRAMMARSIDELABELFIGURE_FORE = new Color(
			null, 100, 100, 100);

	/**
	 * @generated
	 */
	static final Color FFIGUREEDGETYPEDNAMELABELFIGURE_FORE = new Color(null,
			50, 50, 50);

	/**
	 * @generated NOT
	 */
	@Override
	protected void handleNotificationEvent(Notification notification) {

		if (notification.getFeature() instanceof EReference
				&& ((EReference) notification.getFeature()).getFeatureID() == TggPackage.EDGE__GRAPH_PATTERN) {
			// ((CanonicalEditPolicy)getParent().getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
			List<CanonicalEditPolicy> editPolicies = CanonicalEditPolicy
					.getRegisteredEditPolicies(this.getDiagramView()
							.getElement());
			for (Iterator<CanonicalEditPolicy> it = editPolicies.iterator(); it
					.hasNext();) {
				CanonicalEditPolicy nextEditPolicy = it.next();
				nextEditPolicy.refresh();
			}
		}

		super.handleNotificationEvent(notification);
	}

}
