package de.upb.swt.qvt.tgg.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.upb.swt.qvt.tgg.diagram.edit.policies.OCLConstraintItemSemanticEditPolicy;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class OCLConstraintEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2010;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public OCLConstraintEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new OCLConstraintItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new AttributeConstraintFigure();
	}

	/**
	 * @generated
	 */
	public AttributeConstraintFigure getPrimaryShape() {
		return (AttributeConstraintFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof OCLConstraintSlotAttributeNameEditPart) {
			((OCLConstraintSlotAttributeNameEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureAttributeConstraintSlotAttributeNameLabelFigure());
			return true;
		}
		if (childEditPart instanceof OCLConstraintExpressionEditPart) {
			((OCLConstraintExpressionEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureAttributeConstraintValueAttributeNameLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof OCLConstraintSlotAttributeNameEditPart) {
			return true;
		}
		if (childEditPart instanceof OCLConstraintExpressionEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(TggVisualIDRegistry
				.getType(OCLConstraintSlotAttributeNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(TggElementTypes.TGGConstraintSlotNode_4005);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof NodeEditPart) {
			types.add(TggElementTypes.TGGConstraintSlotNode_4005);
		}
		if (targetEditPart instanceof Node2EditPart) {
			types.add(TggElementTypes.TGGConstraintSlotNode_4005);
		}
		if (targetEditPart instanceof Node3EditPart) {
			types.add(TggElementTypes.TGGConstraintSlotNode_4005);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == TggElementTypes.TGGConstraintSlotNode_4005) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public class AttributeConstraintFigure extends RoundedRectangle {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureAttributeConstraintSlotAttributeNameLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureAttributeConstraintValueAttributeNameLabelFigure;

		/**
		 * @generated
		 */
		public AttributeConstraintFigure() {
			this.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8),
					getMapMode().DPtoLP(8)));
			this.setForegroundColor(THIS_FORE);
			this.setBackgroundColor(THIS_BACK);
			this.setBorder(new MarginBorder(getMapMode().DPtoLP(2),
					getMapMode().DPtoLP(2), getMapMode().DPtoLP(2),
					getMapMode().DPtoLP(2)));
			createContents();
		}

		/**
		 * @generated NOT
		 */
		private void createContents() {

			RoundedRectangle slotContainerFigure0 = new RoundedRectangle();
			slotContainerFigure0.setCornerDimensions(new Dimension(getMapMode()
					.DPtoLP(4), getMapMode().DPtoLP(4)));
			slotContainerFigure0.setLineWidth(1);
			slotContainerFigure0.setForegroundColor(SLOTCONTAINERFIGURE0_FORE);
			slotContainerFigure0.setBorder(new MarginBorder(getMapMode()
					.DPtoLP(1), getMapMode().DPtoLP(1), getMapMode().DPtoLP(1),
					getMapMode().DPtoLP(1)));

			this.add(slotContainerFigure0);

			BorderLayout layoutSlotContainerFigure0 = new BorderLayout();
			slotContainerFigure0.setLayoutManager(layoutSlotContainerFigure0);

			fFigureAttributeConstraintSlotAttributeNameLabelFigure = new WrappingLabel();
			fFigureAttributeConstraintSlotAttributeNameLabelFigure
					.setText("slotattrib");

			slotContainerFigure0.add(
					fFigureAttributeConstraintSlotAttributeNameLabelFigure,
					BorderLayout.CENTER);

			RoundedRectangle valueContainerFigure0 = new RoundedRectangle();
			valueContainerFigure0.setCornerDimensions(new Dimension(
					getMapMode().DPtoLP(4), getMapMode().DPtoLP(4)));
			valueContainerFigure0.setLineWidth(1);
			valueContainerFigure0
					.setForegroundColor(VALUECONTAINERFIGURE0_FORE);
			valueContainerFigure0.setBorder(new MarginBorder(getMapMode()
					.DPtoLP(1), getMapMode().DPtoLP(1), getMapMode().DPtoLP(1),
					getMapMode().DPtoLP(1)));

			this.add(valueContainerFigure0);

			BorderLayout layoutValueContainerFigure0 = new BorderLayout();
			valueContainerFigure0.setLayoutManager(layoutValueContainerFigure0);

			fFigureAttributeConstraintValueAttributeNameLabelFigure = new WrappingLabel();
			fFigureAttributeConstraintValueAttributeNameLabelFigure
					.setText("valueexpr");

			// jgreen: For having a wrapping text.
			fFigureAttributeConstraintValueAttributeNameLabelFigure
					.setTextWrap(true);

			valueContainerFigure0.add(
					fFigureAttributeConstraintValueAttributeNameLabelFigure,
					BorderLayout.CENTER);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureAttributeConstraintSlotAttributeNameLabelFigure() {
			return fFigureAttributeConstraintSlotAttributeNameLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureAttributeConstraintValueAttributeNameLabelFigure() {
			return fFigureAttributeConstraintValueAttributeNameLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 100, 100, 100);

	/**
	 * @generated
	 */
	static final Color THIS_BACK = new Color(null, 255, 255, 220);

	/**
	 * @generated
	 */
	static final Color SLOTCONTAINERFIGURE0_FORE = new Color(null, 180, 180,
			180);

	/**
	 * @generated
	 */
	static final Color VALUECONTAINERFIGURE0_FORE = new Color(null, 180, 180,
			180);

}
