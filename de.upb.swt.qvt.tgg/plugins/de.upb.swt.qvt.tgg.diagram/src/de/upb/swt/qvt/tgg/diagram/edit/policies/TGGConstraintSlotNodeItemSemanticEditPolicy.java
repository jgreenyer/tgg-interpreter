package de.upb.swt.qvt.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TGGConstraintSlotNodeItemSemanticEditPolicy extends
		TggBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public TGGConstraintSlotNodeItemSemanticEditPolicy() {
		super(TggElementTypes.TGGConstraintSlotNode_4005);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
