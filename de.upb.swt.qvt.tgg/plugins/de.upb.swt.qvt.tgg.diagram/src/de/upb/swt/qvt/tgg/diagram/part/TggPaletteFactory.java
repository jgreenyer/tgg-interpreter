package de.upb.swt.qvt.tgg.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TggPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createGraphElements1Group());
		paletteRoot.add(createConstraints2Group());
	}

	/**
	 * Creates "Graph Elements" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createGraphElements1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.GraphElements1Group_title);
		paletteContainer.setId("createGraphElements1Group"); //$NON-NLS-1$
		paletteContainer.add(createContextNode1CreationTool());
		paletteContainer.add(createProducedNode2CreationTool());
		paletteContainer.add(createReusableNode3CreationTool());
		paletteContainer.add(createReusablePattern4CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createContextEdge6CreationTool());
		paletteContainer.add(createProducedEdge7CreationTool());
		paletteContainer.add(createReusableEdge8CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createDomainGraphPattern10CreationTool());
		paletteContainer.add(createGraphPatternToNodeLink11CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Constraints" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createConstraints2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Constraints2Group_title);
		paletteContainer.setId("createConstraints2Group"); //$NON-NLS-1$
		paletteContainer.add(createOCLAttributeConstraint1CreationTool());
		paletteContainer.add(createConstraintToSlotNodeLink2CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createContextNode1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ContextNode1CreationTool_title,
				Messages.ContextNode1CreationTool_desc,
				Collections.singletonList(TggElementTypes.Node_2001));
		entry.setId("createContextNode1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/node-context.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProducedNode2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ProducedNode2CreationTool_title,
				Messages.ProducedNode2CreationTool_desc,
				Collections.singletonList(TggElementTypes.Node_2002));
		entry.setId("createProducedNode2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/node-produced.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReusableNode3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ReusableNode3CreationTool_title,
				Messages.ReusableNode3CreationTool_desc,
				Collections.singletonList(TggElementTypes.Node_2003));
		entry.setId("createReusableNode3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/node-reusable.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReusablePattern4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ReusablePattern4CreationTool_title,
				Messages.ReusablePattern4CreationTool_desc,
				Collections.singletonList(TggElementTypes.ReusablePattern_2011));
		entry.setId("createReusablePattern4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/node-reusable.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createContextEdge6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ContextEdge6CreationTool_title,
				Messages.ContextEdge6CreationTool_desc,
				Collections.singletonList(TggElementTypes.Edge_4001));
		entry.setId("createContextEdge6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/edge-context.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProducedEdge7CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ProducedEdge7CreationTool_title,
				Messages.ProducedEdge7CreationTool_desc,
				Collections.singletonList(TggElementTypes.Edge_4007));
		entry.setId("createProducedEdge7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/edge-produced.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReusableEdge8CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ReusableEdge8CreationTool_title,
				Messages.ReusableEdge8CreationTool_desc,
				Collections.singletonList(TggElementTypes.Edge_4008));
		entry.setId("createReusableEdge8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/edge-reusable.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createDomainGraphPattern10CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.DomainGraphPattern10CreationTool_title,
				Messages.DomainGraphPattern10CreationTool_desc,
				Collections
						.singletonList(TggElementTypes.DomainGraphPattern_2005));
		entry.setId("createDomainGraphPattern10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/domaingraphpattern.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGraphPatternToNodeLink11CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.GraphPatternToNodeLink11CreationTool_title,
				Messages.GraphPatternToNodeLink11CreationTool_desc,
				Collections
						.singletonList(TggElementTypes.NodeGraphPattern_4004));
		entry.setId("createGraphPatternToNodeLink11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/domaingraphpatternlink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createOCLAttributeConstraint1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.OCLAttributeConstraint1CreationTool_title,
				Messages.OCLAttributeConstraint1CreationTool_desc,
				Collections.singletonList(TggElementTypes.OCLConstraint_2010));
		entry.setId("createOCLAttributeConstraint1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/constraint.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createConstraintToSlotNodeLink2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ConstraintToSlotNodeLink2CreationTool_title,
				Messages.ConstraintToSlotNodeLink2CreationTool_desc,
				Collections
						.singletonList(TggElementTypes.TGGConstraintSlotNode_4005));
		entry.setId("createConstraintToSlotNodeLink2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.upb.swt.qvt.tgg/icons/constraintslotnodelink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
