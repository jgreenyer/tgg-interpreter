package de.upb.swt.qvt.tgg.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintExpressionEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintSlotAttributeNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ReusablePatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.WrappingLabel2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.WrappingLabel3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.WrappingLabel4EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.WrappingLabelEditPart;
import de.upb.swt.qvt.tgg.diagram.expressions.TggOCLFactory;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented by
 * a domain model object.
 * 
 * @generated
 */
public class TggVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "de.upb.swt.qvt.tgg.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (TripleGraphGrammarRuleEditPart.MODEL_ID.equals(view.getType())) {
				return TripleGraphGrammarRuleEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				TggDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TggPackage.eINSTANCE.getTripleGraphGrammarRule().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((TripleGraphGrammarRule) domainElement)) {
			return TripleGraphGrammarRuleEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry
				.getModelID(containerView);
		if (!TripleGraphGrammarRuleEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (TripleGraphGrammarRuleEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = TripleGraphGrammarRuleEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			if (TggPackage.eINSTANCE.getNode().isSuperTypeOf(
					domainElement.eClass())
					&& isNode_2001((Node) domainElement)) {
				return NodeEditPart.VISUAL_ID;
			}
			if (TggPackage.eINSTANCE.getNode().isSuperTypeOf(
					domainElement.eClass())
					&& isNode_2002((Node) domainElement)) {
				return Node2EditPart.VISUAL_ID;
			}
			if (TggPackage.eINSTANCE.getNode().isSuperTypeOf(
					domainElement.eClass())
					&& isNode_2003((Node) domainElement)) {
				return Node3EditPart.VISUAL_ID;
			}
			if (TggPackage.eINSTANCE.getDomainGraphPattern().isSuperTypeOf(
					domainElement.eClass())) {
				return DomainGraphPatternEditPart.VISUAL_ID;
			}
			if (TggPackage.eINSTANCE.getOCLConstraint().isSuperTypeOf(
					domainElement.eClass())) {
				return OCLConstraintEditPart.VISUAL_ID;
			}
			if (TggPackage.eINSTANCE.getReusablePattern().isSuperTypeOf(
					domainElement.eClass())) {
				return ReusablePatternEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry
				.getModelID(containerView);
		if (!TripleGraphGrammarRuleEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (TripleGraphGrammarRuleEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = TripleGraphGrammarRuleEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			if (NodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Node2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Node3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DomainGraphPatternEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (OCLConstraintEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ReusablePatternEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case NodeEditPart.VISUAL_ID:
			if (NodeNameTypedNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Node2EditPart.VISUAL_ID:
			if (NodeNameTypedName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Node3EditPart.VISUAL_ID:
			if (NodeNameTypedName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DomainGraphPatternEditPart.VISUAL_ID:
			if (DomainGraphPatternNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case OCLConstraintEditPart.VISUAL_ID:
			if (OCLConstraintSlotAttributeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (OCLConstraintExpressionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ReusablePatternEditPart.VISUAL_ID:
			if (WrappingLabel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EdgeEditPart.VISUAL_ID:
			if (EdgeNameTypedNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (WrappingLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Edge2EditPart.VISUAL_ID:
			if (EdgeNameTypedName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (WrappingLabel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Edge3EditPart.VISUAL_ID:
			if (EdgeNameTypedName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (WrappingLabel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TggPackage.eINSTANCE.getEdge()
				.isSuperTypeOf(domainElement.eClass())
				&& isEdge_4001((Edge) domainElement)) {
			return EdgeEditPart.VISUAL_ID;
		}
		if (TggPackage.eINSTANCE.getEdge()
				.isSuperTypeOf(domainElement.eClass())
				&& isEdge_4007((Edge) domainElement)) {
			return Edge2EditPart.VISUAL_ID;
		}
		if (TggPackage.eINSTANCE.getEdge()
				.isSuperTypeOf(domainElement.eClass())
				&& isEdge_4008((Edge) domainElement)) {
			return Edge3EditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(TripleGraphGrammarRule element) {
		return true;
	}

	/**
	 * @generated
	 */
	private static boolean isNode_2001(Node domainElement) {
		Object result = TggOCLFactory.getExpression(0,
				TggPackage.eINSTANCE.getNode(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	private static boolean isNode_2002(Node domainElement) {
		Object result = TggOCLFactory.getExpression(3,
				TggPackage.eINSTANCE.getNode(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	private static boolean isNode_2003(Node domainElement) {
		Object result = TggOCLFactory.getExpression(5,
				TggPackage.eINSTANCE.getNode(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	private static boolean isEdge_4001(Edge domainElement) {
		Object result = TggOCLFactory.getExpression(6,
				TggPackage.eINSTANCE.getEdge(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	private static boolean isEdge_4007(Edge domainElement) {
		Object result = TggOCLFactory.getExpression(10,
				TggPackage.eINSTANCE.getEdge(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	private static boolean isEdge_4008(Edge domainElement) {
		Object result = TggOCLFactory.getExpression(12,
				TggPackage.eINSTANCE.getEdge(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

}
