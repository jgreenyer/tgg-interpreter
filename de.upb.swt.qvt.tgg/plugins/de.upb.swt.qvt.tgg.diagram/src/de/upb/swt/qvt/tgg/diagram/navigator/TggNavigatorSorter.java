package de.upb.swt.qvt.tgg.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 4010;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof TggNavigatorItem) {
			TggNavigatorItem item = (TggNavigatorItem) element;
			return TggVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
