package de.upb.swt.qvt.tgg.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ReusablePatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TGGConstraintSlotNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TggDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<TggNodeDescriptor> getSemanticChildren(View view) {
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			return getTripleGraphGrammarRule_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggNodeDescriptor> getTripleGraphGrammarRule_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		TripleGraphGrammarRule modelElement = (TripleGraphGrammarRule) view
				.getElement();
		LinkedList<TggNodeDescriptor> result = new LinkedList<TggNodeDescriptor>();
		for (Iterator<?> it = modelElement.getNode().iterator(); it.hasNext();) {
			Node childElement = (Node) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == NodeEditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Node2EditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Node3EditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getDomain().iterator(); it.hasNext();) {
			Domain childElement = (Domain) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == DomainGraphPatternEditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getConstraint().iterator(); it
				.hasNext();) {
			Constraint childElement = (Constraint) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == OCLConstraintEditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getReusablePattern().iterator(); it
				.hasNext();) {
			ReusablePattern childElement = (ReusablePattern) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ReusablePatternEditPart.VISUAL_ID) {
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getContainedLinks(View view) {
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			return getTripleGraphGrammarRule_1000ContainedLinks(view);
		case NodeEditPart.VISUAL_ID:
			return getNode_2001ContainedLinks(view);
		case Node2EditPart.VISUAL_ID:
			return getNode_2002ContainedLinks(view);
		case Node3EditPart.VISUAL_ID:
			return getNode_2003ContainedLinks(view);
		case DomainGraphPatternEditPart.VISUAL_ID:
			return getDomainGraphPattern_2005ContainedLinks(view);
		case OCLConstraintEditPart.VISUAL_ID:
			return getOCLConstraint_2010ContainedLinks(view);
		case ReusablePatternEditPart.VISUAL_ID:
			return getReusablePattern_2011ContainedLinks(view);
		case EdgeEditPart.VISUAL_ID:
			return getEdge_4001ContainedLinks(view);
		case Edge2EditPart.VISUAL_ID:
			return getEdge_4007ContainedLinks(view);
		case Edge3EditPart.VISUAL_ID:
			return getEdge_4008ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getIncomingLinks(View view) {
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case NodeEditPart.VISUAL_ID:
			return getNode_2001IncomingLinks(view);
		case Node2EditPart.VISUAL_ID:
			return getNode_2002IncomingLinks(view);
		case Node3EditPart.VISUAL_ID:
			return getNode_2003IncomingLinks(view);
		case DomainGraphPatternEditPart.VISUAL_ID:
			return getDomainGraphPattern_2005IncomingLinks(view);
		case OCLConstraintEditPart.VISUAL_ID:
			return getOCLConstraint_2010IncomingLinks(view);
		case ReusablePatternEditPart.VISUAL_ID:
			return getReusablePattern_2011IncomingLinks(view);
		case EdgeEditPart.VISUAL_ID:
			return getEdge_4001IncomingLinks(view);
		case Edge2EditPart.VISUAL_ID:
			return getEdge_4007IncomingLinks(view);
		case Edge3EditPart.VISUAL_ID:
			return getEdge_4008IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getOutgoingLinks(View view) {
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case NodeEditPart.VISUAL_ID:
			return getNode_2001OutgoingLinks(view);
		case Node2EditPart.VISUAL_ID:
			return getNode_2002OutgoingLinks(view);
		case Node3EditPart.VISUAL_ID:
			return getNode_2003OutgoingLinks(view);
		case DomainGraphPatternEditPart.VISUAL_ID:
			return getDomainGraphPattern_2005OutgoingLinks(view);
		case OCLConstraintEditPart.VISUAL_ID:
			return getOCLConstraint_2010OutgoingLinks(view);
		case ReusablePatternEditPart.VISUAL_ID:
			return getReusablePattern_2011OutgoingLinks(view);
		case EdgeEditPart.VISUAL_ID:
			return getEdge_4001OutgoingLinks(view);
		case Edge2EditPart.VISUAL_ID:
			return getEdge_4007OutgoingLinks(view);
		case Edge3EditPart.VISUAL_ID:
			return getEdge_4008OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getTripleGraphGrammarRule_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2001ContainedLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2002ContainedLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2003ContainedLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getDomainGraphPattern_2005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getOCLConstraint_2010ContainedLinks(
			View view) {
		OCLConstraint modelElement = (OCLConstraint) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getReusablePattern_2011ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4007ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4008ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2001IncomingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4007(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4008(modelElement,
				crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2002IncomingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4007(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4008(modelElement,
				crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2003IncomingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4007(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4008(modelElement,
				crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getDomainGraphPattern_2005IncomingLinks(
			View view) {
		DomainGraphPattern modelElement = (DomainGraphPattern) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Node_GraphPattern_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getOCLConstraint_2010IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getReusablePattern_2011IncomingLinks(
			View view) {
		ReusablePattern modelElement = (ReusablePattern) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Node_GraphPattern_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4007IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4008IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2001OutgoingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2002OutgoingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getNode_2003OutgoingLinks(View view) {
		Node modelElement = (Node) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4007(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4008(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getDomainGraphPattern_2005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getOCLConstraint_2010OutgoingLinks(
			View view) {
		OCLConstraint modelElement = (OCLConstraint) view.getElement();
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getReusablePattern_2011OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4007OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TggLinkDescriptor> getEdge_4008OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getContainedTypeModelFacetLinks_Edge_4001(
			Node container) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (EdgeEditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4001, EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getContainedTypeModelFacetLinks_Edge_4007(
			Node container) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (Edge2EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4007, Edge2EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getContainedTypeModelFacetLinks_Edge_4008(
			Node container) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (Edge3EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4008, Edge3EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getIncomingTypeModelFacetLinks_Edge_4001(
			Node target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TggPackage.eINSTANCE
					.getEdge_TargetNode()
					|| false == setting.getEObject() instanceof Edge) {
				continue;
			}
			Edge link = (Edge) setting.getEObject();
			if (EdgeEditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, target, link,
					TggElementTypes.Edge_4001, EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getIncomingTypeModelFacetLinks_Edge_4007(
			Node target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TggPackage.eINSTANCE
					.getEdge_TargetNode()
					|| false == setting.getEObject() instanceof Edge) {
				continue;
			}
			Edge link = (Edge) setting.getEObject();
			if (Edge2EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, target, link,
					TggElementTypes.Edge_4007, Edge2EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getIncomingTypeModelFacetLinks_Edge_4008(
			Node target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TggPackage.eINSTANCE
					.getEdge_TargetNode()
					|| false == setting.getEObject() instanceof Edge) {
				continue;
			}
			Edge link = (Edge) setting.getEObject();
			if (Edge3EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node src = link.getSourceNode();
			result.add(new TggLinkDescriptor(src, target, link,
					TggElementTypes.Edge_4008, Edge3EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getIncomingFeatureModelFacetLinks_Node_GraphPattern_4004(
			GraphPattern target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TggPackage.eINSTANCE
					.getNode_GraphPattern()) {
				result.add(new TggLinkDescriptor(setting.getEObject(), target,
						TggElementTypes.NodeGraphPattern_4004,
						NodeGraphPatternEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getIncomingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(
			Node target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TggPackage.eINSTANCE
					.getTGGConstraint_SlotNode()) {
				result.add(new TggLinkDescriptor(setting.getEObject(), target,
						TggElementTypes.TGGConstraintSlotNode_4005,
						TGGConstraintSlotNodeEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getOutgoingTypeModelFacetLinks_Edge_4001(
			Node source) {
		Node container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Node) {
				container = (Node) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (EdgeEditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			if (src != source) {
				continue;
			}
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4001, EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getOutgoingTypeModelFacetLinks_Edge_4007(
			Node source) {
		Node container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Node) {
				container = (Node) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (Edge2EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			if (src != source) {
				continue;
			}
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4007, Edge2EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getOutgoingTypeModelFacetLinks_Edge_4008(
			Node source) {
		Node container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Node) {
				container = (Node) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingEdge().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Edge) {
				continue;
			}
			Edge link = (Edge) linkObject;
			if (Edge3EditPart.VISUAL_ID != TggVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Node dst = link.getTargetNode();
			Node src = link.getSourceNode();
			if (src != source) {
				continue;
			}
			result.add(new TggLinkDescriptor(src, dst, link,
					TggElementTypes.Edge_4008, Edge3EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getOutgoingFeatureModelFacetLinks_Node_GraphPattern_4004(
			Node source) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		for (Iterator<?> destinations = source.getGraphPattern().iterator(); destinations
				.hasNext();) {
			GraphPattern destination = (GraphPattern) destinations.next();
			result.add(new TggLinkDescriptor(source, destination,
					TggElementTypes.NodeGraphPattern_4004,
					NodeGraphPatternEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TggLinkDescriptor> getOutgoingFeatureModelFacetLinks_TGGConstraint_SlotNode_4005(
			TGGConstraint source) {
		LinkedList<TggLinkDescriptor> result = new LinkedList<TggLinkDescriptor>();
		Node destination = source.getSlotNode();
		if (destination == null) {
			return result;
		}
		result.add(new TggLinkDescriptor(source, destination,
				TggElementTypes.TGGConstraintSlotNode_4005,
				TGGConstraintSlotNodeEditPart.VISUAL_ID));
		return result;
	}

}
