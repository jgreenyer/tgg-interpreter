package de.upb.swt.qvt.tgg.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.swt.qvt.tgg.diagram.edit.commands.Edge2CreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.Edge2ReorientCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.Edge3CreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.Edge3ReorientCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.EdgeCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.EdgeReorientCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.NodeGraphPatternCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.NodeGraphPatternReorientCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.TGGConstraintSlotNodeCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.TGGConstraintSlotNodeReorientCommand;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TGGConstraintSlotNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class Node2ItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public Node2ItemSemanticEditPolicy() {
		super(TggElementTypes.Node_2002);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (TggVisualIDRegistry.getVisualID(incomingLink) == EdgeEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(incomingLink) == Edge2EditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(incomingLink) == Edge3EditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(incomingLink) == TGGConstraintSlotNodeEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator<?> it = view.getSourceEdges().iterator(); it.hasNext();) {
			Edge outgoingLink = (Edge) it.next();
			if (TggVisualIDRegistry.getVisualID(outgoingLink) == EdgeEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(outgoingLink) == Edge2EditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(outgoingLink) == Edge3EditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(outgoingLink) == NodeGraphPatternEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TggElementTypes.Edge_4001 == req.getElementType()) {
			return getGEFWrapper(new EdgeCreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.Edge_4007 == req.getElementType()) {
			return getGEFWrapper(new Edge2CreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.Edge_4008 == req.getElementType()) {
			return getGEFWrapper(new Edge3CreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.NodeGraphPattern_4004 == req.getElementType()) {
			return getGEFWrapper(new NodeGraphPatternCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (TggElementTypes.TGGConstraintSlotNode_4005 == req.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (TggElementTypes.Edge_4001 == req.getElementType()) {
			return getGEFWrapper(new EdgeCreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.Edge_4007 == req.getElementType()) {
			return getGEFWrapper(new Edge2CreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.Edge_4008 == req.getElementType()) {
			return getGEFWrapper(new Edge3CreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (TggElementTypes.NodeGraphPattern_4004 == req.getElementType()) {
			return null;
		}
		if (TggElementTypes.TGGConstraintSlotNode_4005 == req.getElementType()) {
			return getGEFWrapper(new TGGConstraintSlotNodeCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case EdgeEditPart.VISUAL_ID:
			return getGEFWrapper(new EdgeReorientCommand(req));
		case Edge2EditPart.VISUAL_ID:
			return getGEFWrapper(new Edge2ReorientCommand(req));
		case Edge3EditPart.VISUAL_ID:
			return getGEFWrapper(new Edge3ReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

	/**
	 * Returns command to reorient EReference based link. New link target or
	 * source should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case NodeGraphPatternEditPart.VISUAL_ID:
			return getGEFWrapper(new NodeGraphPatternReorientCommand(req));
		case TGGConstraintSlotNodeEditPart.VISUAL_ID:
			return getGEFWrapper(new TGGConstraintSlotNodeReorientCommand(req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
