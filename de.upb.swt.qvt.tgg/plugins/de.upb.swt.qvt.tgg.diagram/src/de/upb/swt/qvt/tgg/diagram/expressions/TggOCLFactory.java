package de.upb.swt.qvt.tgg.diagram.expressions;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;

import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class TggOCLFactory {

	/**
	 * @generated
	 */
	private final TggAbstractExpression[] expressions;

	/**
	 * @generated
	 */
	protected TggOCLFactory() {
		this.expressions = new TggAbstractExpression[18];
	}

	/**
	 * @generated
	 */
	public static TggAbstractExpression getExpression(int index,
			EClassifier context, Map<String, EClassifier> environment) {
		TggOCLFactory cached = TggDiagramEditorPlugin.getInstance()
				.getTggOCLFactory();
		if (cached == null) {
			TggDiagramEditorPlugin.getInstance().setTggOCLFactory(
					cached = new TggOCLFactory());
		}
		if (index < 0 || index >= cached.expressions.length) {
			throw new IllegalArgumentException();
		}
		if (cached.expressions[index] == null) {
			final String[] exprBodies = new String[] {
					"self.oclIsTypeOf(Node) and self.right and self.left", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"self.oclIsTypeOf(Node) and self.right and not self.left", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"self.oclIsTypeOf(Node) and not self.right and not self.left", //$NON-NLS-1$
					"self.left and self.right", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"self.left and self.right", //$NON-NLS-1$
					"not self.left and self.right", //$NON-NLS-1$
					"true", //$NON-NLS-1$
					"not self.left and not self.right", //$NON-NLS-1$
					"self.typeReference<>null and self.typeReference.eOpposite<>null implies self.targetNode.outgoingEdge->exists(edge|edge.typeReference=self.typeReference.eOpposite)", //$NON-NLS-1$
					"self.eType<>null", //$NON-NLS-1$
					"self.graphPattern->exists(gp|gp.oclIsTypeOf(DomainGraphPattern))", //$NON-NLS-1$
					"not self.graphPattern->exists(gp1, gp2 | gp1.oclIsTypeOf(DomainGraphPattern) and gp2.oclIsTypeOf(DomainGraphPattern) and gp1<>gp2)", //$NON-NLS-1$
					"self.outgoingEdge<>null or self.incomingEdge<>null", //$NON-NLS-1$
			};
			cached.expressions[index] = getExpression(
					exprBodies[index],
					context,
					environment == null ? Collections
							.<String, EClassifier> emptyMap() : environment);
		}
		return cached.expressions[index];
	}

	/**
	 * @generated
	 */
	public static TggAbstractExpression getExpression(String body,
			EClassifier context, Map<String, EClassifier> environment) {
		return new Expression(body, context, environment);
	}

	/**
	 * @generated
	 */
	public static TggAbstractExpression getExpression(String body,
			EClassifier context) {
		return getExpression(body, context,
				Collections.<String, EClassifier> emptyMap());
	}

	/**
	 * @generated
	 */
	private static class Expression extends TggAbstractExpression {

		/**
		 * @generated
		 */
		private final org.eclipse.ocl.ecore.OCL oclInstance;

		/**
		 * @generated
		 */
		private OCLExpression oclExpression;

		/**
		 * @generated
		 */
		public Expression(String body, EClassifier context,
				Map<String, EClassifier> environment) {
			super(body, context);
			oclInstance = org.eclipse.ocl.ecore.OCL.newInstance();
			initCustomEnv(oclInstance.getEnvironment(), environment);
			Helper oclHelper = oclInstance.createOCLHelper();
			oclHelper.setContext(context());
			try {
				oclExpression = oclHelper.createQuery(body());
				setStatus(IStatus.OK, null, null);
			} catch (ParserException e) {
				setStatus(IStatus.ERROR, e.getMessage(), e);
			}
		}

		/**
		 * @generated
		 */
		@SuppressWarnings("rawtypes")
		protected Object doEvaluate(Object context, Map env) {
			if (oclExpression == null) {
				return null;
			}
			// on the first call, both evalEnvironment and extentMap are clear, for later we have finally, below.
			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = oclInstance
					.getEvaluationEnvironment();
			// initialize environment
			for (Object nextKey : env.keySet()) {
				evalEnv.replace((String) nextKey, env.get(nextKey));
			}
			try {
				Object result = oclInstance.evaluate(context, oclExpression);
				return oclInstance.isInvalid(result) ? null : result;
			} finally {
				evalEnv.clear();
				oclInstance.setExtentMap(null); // clear allInstances cache, and get the oclInstance ready for the next call
			}
		}

		/**
		 * @generated
		 */
		private static void initCustomEnv(
				Environment<?, EClassifier, ?, ?, ?, EParameter, ?, ?, ?, ?, ?, ?> ecoreEnv,
				Map<String, EClassifier> environment) {
			// Use EObject as implicit root class for any object, to allow eContainer() and other EObject operations from OCL expressions
			ParsingOptions.setOption(ecoreEnv,
					ParsingOptions.implicitRootClass(ecoreEnv),
					EcorePackage.eINSTANCE.getEObject());
			for (String varName : environment.keySet()) {
				EClassifier varType = environment.get(varName);
				ecoreEnv.addElement(varName,
						createVar(ecoreEnv, varName, varType), false);
			}
		}

		/**
		 * @generated
		 */
		private static Variable createVar(
				Environment<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> ecoreEnv,
				String name, EClassifier type) {
			Variable var = EcoreFactory.eINSTANCE.createVariable();
			var.setName(name);
			var.setType(ecoreEnv.getUMLReflection().getOCLType(type));
			return var;
		}
	}
}
