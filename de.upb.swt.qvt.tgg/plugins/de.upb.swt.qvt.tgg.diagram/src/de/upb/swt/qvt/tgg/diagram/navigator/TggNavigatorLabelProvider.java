package de.upb.swt.qvt.tgg.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedName3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeNameTypedNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintSlotAttributeNameEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ReusablePatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TGGConstraintSlotNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.WrappingLabel4EditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;
import de.upb.swt.qvt.tgg.diagram.providers.TggParserProvider;

/**
 * @generated
 */
public class TggNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		TggDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		TggDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof TggNavigatorItem
				&& !isOwnView(((TggNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof TggNavigatorGroup) {
			TggNavigatorGroup group = (TggNavigatorGroup) element;
			return TggDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof TggNavigatorItem) {
			TggNavigatorItem navigatorItem = (TggNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case OCLConstraintEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?OCLConstraint", TggElementTypes.OCLConstraint_2010); //$NON-NLS-1$
		case NodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?Node", TggElementTypes.Node_2001); //$NON-NLS-1$
		case TGGConstraintSlotNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de.upb.swt.qvt.tgg?TGGConstraint?slotNode", TggElementTypes.TGGConstraintSlotNode_4005); //$NON-NLS-1$
		case Node3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?Node", TggElementTypes.Node_2003); //$NON-NLS-1$
		case Node2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?Node", TggElementTypes.Node_2002); //$NON-NLS-1$
		case Edge3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de.upb.swt.qvt.tgg?Edge", TggElementTypes.Edge_4008); //$NON-NLS-1$
		case ReusablePatternEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?ReusablePattern", TggElementTypes.ReusablePattern_2011); //$NON-NLS-1$
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://de.upb.swt.qvt.tgg?TripleGraphGrammarRule", TggElementTypes.TripleGraphGrammarRule_1000); //$NON-NLS-1$
		case NodeGraphPatternEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de.upb.swt.qvt.tgg?Node?graphPattern", TggElementTypes.NodeGraphPattern_4004); //$NON-NLS-1$
		case Edge2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de.upb.swt.qvt.tgg?Edge", TggElementTypes.Edge_4007); //$NON-NLS-1$
		case EdgeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de.upb.swt.qvt.tgg?Edge", TggElementTypes.Edge_4001); //$NON-NLS-1$
		case DomainGraphPatternEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de.upb.swt.qvt.tgg?DomainGraphPattern", TggElementTypes.DomainGraphPattern_2005); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = TggDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& TggElementTypes.isKnownElementType(elementType)) {
			image = TggElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof TggNavigatorGroup) {
			TggNavigatorGroup group = (TggNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof TggNavigatorItem) {
			TggNavigatorItem navigatorItem = (TggNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (TggVisualIDRegistry.getVisualID(view)) {
		case OCLConstraintEditPart.VISUAL_ID:
			return getOCLConstraint_2010Text(view);
		case NodeEditPart.VISUAL_ID:
			return getNode_2001Text(view);
		case TGGConstraintSlotNodeEditPart.VISUAL_ID:
			return getTGGConstraintSlotNode_4005Text(view);
		case Node3EditPart.VISUAL_ID:
			return getNode_2003Text(view);
		case Node2EditPart.VISUAL_ID:
			return getNode_2002Text(view);
		case Edge3EditPart.VISUAL_ID:
			return getEdge_4008Text(view);
		case ReusablePatternEditPart.VISUAL_ID:
			return getReusablePattern_2011Text(view);
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			return getTripleGraphGrammarRule_1000Text(view);
		case NodeGraphPatternEditPart.VISUAL_ID:
			return getNodeGraphPattern_4004Text(view);
		case Edge2EditPart.VISUAL_ID:
			return getEdge_4007Text(view);
		case EdgeEditPart.VISUAL_ID:
			return getEdge_4001Text(view);
		case DomainGraphPatternEditPart.VISUAL_ID:
			return getDomainGraphPattern_2005Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getTripleGraphGrammarRule_1000Text(View view) {
		TripleGraphGrammarRule domainModelElement = (TripleGraphGrammarRule) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getNode_2001Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Node_2001,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(NodeNameTypedNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getNode_2002Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Node_2002,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(NodeNameTypedName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getReusablePattern_2011Text(View view) {
		IParser parser = TggParserProvider.getParser(
				TggElementTypes.ReusablePattern_2011,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry.getType(WrappingLabel4EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5015); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getNode_2003Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Node_2003,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(NodeNameTypedName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getDomainGraphPattern_2005Text(View view) {
		IParser parser = TggParserProvider.getParser(
				TggElementTypes.DomainGraphPattern_2005,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(DomainGraphPatternNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getOCLConstraint_2010Text(View view) {
		IParser parser = TggParserProvider
				.getParser(
						TggElementTypes.OCLConstraint_2010,
						view.getElement() != null ? view.getElement() : view,
						TggVisualIDRegistry
								.getType(OCLConstraintSlotAttributeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEdge_4001Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Edge_4001,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(EdgeNameTypedNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEdge_4007Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Edge_4007,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(EdgeNameTypedName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEdge_4008Text(View view) {
		IParser parser = TggParserProvider.getParser(TggElementTypes.Edge_4008,
				view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry
						.getType(EdgeNameTypedName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TggDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getNodeGraphPattern_4004Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getTGGConstraintSlotNode_4005Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return TripleGraphGrammarRuleEditPart.MODEL_ID
				.equals(TggVisualIDRegistry.getModelID(view));
	}

}
