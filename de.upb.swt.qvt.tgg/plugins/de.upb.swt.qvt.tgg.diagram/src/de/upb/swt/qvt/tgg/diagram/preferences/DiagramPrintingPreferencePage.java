package de.upb.swt.qvt.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(TggDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
