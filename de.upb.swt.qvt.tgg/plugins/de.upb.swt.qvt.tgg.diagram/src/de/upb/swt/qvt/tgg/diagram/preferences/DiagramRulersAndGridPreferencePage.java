package de.upb.swt.qvt.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(TggDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
