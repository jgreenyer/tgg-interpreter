package de.upb.swt.qvt.tgg.diagram.part;

import org.eclipse.emf.ecore.EObject;

/**
 * @generated
 */
public class TggNodeDescriptor {

	/**
	 * @generated
	 */
	private final EObject myModelElement;

	/**
	 * @generated
	 */
	private final int myVisualID;

	/**
	 * @generated
	 */
	public TggNodeDescriptor(EObject modelElement, int visualID) {
		myModelElement = modelElement;
		myVisualID = visualID;
	}

	/**
	 * @generated
	 */
	public EObject getModelElement() {
		return myModelElement;
	}

	/**
	 * @generated
	 */
	public int getVisualID() {
		return myVisualID;
	}

}
