package de.upb.swt.qvt.tgg.diagram.providers;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.diagram.expressions.TggOCLFactory;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {
	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public void init_Node_2001(Node instance) {
		try {
			Object value_0 = TggOCLFactory.getExpression(1,
					TggPackage.eINSTANCE.getNode(), null).evaluate(instance);
			instance.setRight(((Boolean) value_0).booleanValue());
			Object value_1 = TggOCLFactory.getExpression(2,
					TggPackage.eINSTANCE.getNode(), null).evaluate(instance);
			instance.setLeft(((Boolean) value_1).booleanValue());
		} catch (RuntimeException e) {
			TggDiagramEditorPlugin.getInstance().logError(
					"Element initialization failed", e); //$NON-NLS-1$						
		}
	}

	/**
	 * @generated
	 */
	public void init_Node_2002(Node instance) {
		try {
			Object value_0 = TggOCLFactory.getExpression(4,
					TggPackage.eINSTANCE.getNode(), null).evaluate(instance);
			instance.setRight(((Boolean) value_0).booleanValue());
		} catch (RuntimeException e) {
			TggDiagramEditorPlugin.getInstance().logError(
					"Element initialization failed", e); //$NON-NLS-1$						
		}
	}

	/**
	 * @generated
	 */
	public void init_Edge_4001(Edge instance) {
		try {
			Object value_0 = TggOCLFactory.getExpression(7,
					TggPackage.eINSTANCE.getEdge(), null).evaluate(instance);
			instance.setLeft(((Boolean) value_0).booleanValue());
			Object value_1 = TggOCLFactory.getExpression(8,
					TggPackage.eINSTANCE.getEdge(), null).evaluate(instance);
			instance.setRight(((Boolean) value_1).booleanValue());
		} catch (RuntimeException e) {
			TggDiagramEditorPlugin.getInstance().logError(
					"Element initialization failed", e); //$NON-NLS-1$						
		}
	}

	/**
	 * @generated
	 */
	public void init_Edge_4007(Edge instance) {
		try {
			Object value_0 = TggOCLFactory.getExpression(11,
					TggPackage.eINSTANCE.getEdge(), null).evaluate(instance);
			instance.setRight(((Boolean) value_0).booleanValue());
		} catch (RuntimeException e) {
			TggDiagramEditorPlugin.getInstance().logError(
					"Element initialization failed", e); //$NON-NLS-1$						
		}
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = TggDiagramEditorPlugin.getInstance()
				.getElementInitializers();
		if (cached == null) {
			TggDiagramEditorPlugin.getInstance().setElementInitializers(
					cached = new ElementInitializers());
		}
		return cached;
	}

}
