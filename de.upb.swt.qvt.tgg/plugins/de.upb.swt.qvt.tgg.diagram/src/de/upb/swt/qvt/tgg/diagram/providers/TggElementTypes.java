package de.upb.swt.qvt.tgg.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Edge3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.EdgeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.OCLConstraintEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ReusablePatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TGGConstraintSlotNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class TggElementTypes {

	/**
	 * @generated
	 */
	private TggElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType TripleGraphGrammarRule_1000 = getElementType("de.upb.swt.qvt.tgg.diagram.TripleGraphGrammarRule_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Node_2001 = getElementType("de.upb.swt.qvt.tgg.diagram.Node_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Node_2002 = getElementType("de.upb.swt.qvt.tgg.diagram.Node_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Node_2003 = getElementType("de.upb.swt.qvt.tgg.diagram.Node_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType DomainGraphPattern_2005 = getElementType("de.upb.swt.qvt.tgg.diagram.DomainGraphPattern_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType OCLConstraint_2010 = getElementType("de.upb.swt.qvt.tgg.diagram.OCLConstraint_2010"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ReusablePattern_2011 = getElementType("de.upb.swt.qvt.tgg.diagram.ReusablePattern_2011"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Edge_4001 = getElementType("de.upb.swt.qvt.tgg.diagram.Edge_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Edge_4007 = getElementType("de.upb.swt.qvt.tgg.diagram.Edge_4007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Edge_4008 = getElementType("de.upb.swt.qvt.tgg.diagram.Edge_4008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType NodeGraphPattern_4004 = getElementType("de.upb.swt.qvt.tgg.diagram.NodeGraphPattern_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TGGConstraintSlotNode_4005 = getElementType("de.upb.swt.qvt.tgg.diagram.TGGConstraintSlotNode_4005"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return TggDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(TripleGraphGrammarRule_1000,
					TggPackage.eINSTANCE.getTripleGraphGrammarRule());

			elements.put(Node_2001, TggPackage.eINSTANCE.getNode());

			elements.put(Node_2002, TggPackage.eINSTANCE.getNode());

			elements.put(Node_2003, TggPackage.eINSTANCE.getNode());

			elements.put(DomainGraphPattern_2005,
					TggPackage.eINSTANCE.getDomainGraphPattern());

			elements.put(OCLConstraint_2010,
					TggPackage.eINSTANCE.getOCLConstraint());

			elements.put(ReusablePattern_2011,
					TggPackage.eINSTANCE.getReusablePattern());

			elements.put(Edge_4001, TggPackage.eINSTANCE.getEdge());

			elements.put(Edge_4007, TggPackage.eINSTANCE.getEdge());

			elements.put(Edge_4008, TggPackage.eINSTANCE.getEdge());

			elements.put(NodeGraphPattern_4004,
					TggPackage.eINSTANCE.getNode_GraphPattern());

			elements.put(TGGConstraintSlotNode_4005,
					TggPackage.eINSTANCE.getTGGConstraint_SlotNode());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(TripleGraphGrammarRule_1000);
			KNOWN_ELEMENT_TYPES.add(Node_2001);
			KNOWN_ELEMENT_TYPES.add(Node_2002);
			KNOWN_ELEMENT_TYPES.add(Node_2003);
			KNOWN_ELEMENT_TYPES.add(DomainGraphPattern_2005);
			KNOWN_ELEMENT_TYPES.add(OCLConstraint_2010);
			KNOWN_ELEMENT_TYPES.add(ReusablePattern_2011);
			KNOWN_ELEMENT_TYPES.add(Edge_4001);
			KNOWN_ELEMENT_TYPES.add(Edge_4007);
			KNOWN_ELEMENT_TYPES.add(Edge_4008);
			KNOWN_ELEMENT_TYPES.add(NodeGraphPattern_4004);
			KNOWN_ELEMENT_TYPES.add(TGGConstraintSlotNode_4005);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case TripleGraphGrammarRuleEditPart.VISUAL_ID:
			return TripleGraphGrammarRule_1000;
		case NodeEditPart.VISUAL_ID:
			return Node_2001;
		case Node2EditPart.VISUAL_ID:
			return Node_2002;
		case Node3EditPart.VISUAL_ID:
			return Node_2003;
		case DomainGraphPatternEditPart.VISUAL_ID:
			return DomainGraphPattern_2005;
		case OCLConstraintEditPart.VISUAL_ID:
			return OCLConstraint_2010;
		case ReusablePatternEditPart.VISUAL_ID:
			return ReusablePattern_2011;
		case EdgeEditPart.VISUAL_ID:
			return Edge_4001;
		case Edge2EditPart.VISUAL_ID:
			return Edge_4007;
		case Edge3EditPart.VISUAL_ID:
			return Edge_4008;
		case NodeGraphPatternEditPart.VISUAL_ID:
			return NodeGraphPattern_4004;
		case TGGConstraintSlotNodeEditPart.VISUAL_ID:
			return TGGConstraintSlotNode_4005;
		}
		return null;
	}

}
