package de.upb.swt.qvt.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;

import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class Edge2ItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public Edge2ItemSemanticEditPolicy() {
		super(TggElementTypes.Edge_4007);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

}
