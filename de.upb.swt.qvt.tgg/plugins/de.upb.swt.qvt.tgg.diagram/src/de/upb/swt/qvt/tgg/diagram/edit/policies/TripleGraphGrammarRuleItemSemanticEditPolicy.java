package de.upb.swt.qvt.tgg.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import de.upb.swt.qvt.tgg.diagram.edit.commands.DomainGraphPatternCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.Node2CreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.Node3CreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.NodeCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.OCLConstraintCreateCommand;
import de.upb.swt.qvt.tgg.diagram.edit.commands.ReusablePatternCreateCommand;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TripleGraphGrammarRuleItemSemanticEditPolicy extends
		TggBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public TripleGraphGrammarRuleItemSemanticEditPolicy() {
		super(TggElementTypes.TripleGraphGrammarRule_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TggElementTypes.Node_2001 == req.getElementType()) {
			return getGEFWrapper(new NodeCreateCommand(req));
		}
		if (TggElementTypes.Node_2002 == req.getElementType()) {
			return getGEFWrapper(new Node2CreateCommand(req));
		}
		if (TggElementTypes.Node_2003 == req.getElementType()) {
			return getGEFWrapper(new Node3CreateCommand(req));
		}
		if (TggElementTypes.DomainGraphPattern_2005 == req.getElementType()) {
			return getGEFWrapper(new DomainGraphPatternCreateCommand(req));
		}
		if (TggElementTypes.OCLConstraint_2010 == req.getElementType()) {
			return getGEFWrapper(new OCLConstraintCreateCommand(req));
		}
		if (TggElementTypes.ReusablePattern_2011 == req.getElementType()) {
			return getGEFWrapper(new ReusablePatternCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
