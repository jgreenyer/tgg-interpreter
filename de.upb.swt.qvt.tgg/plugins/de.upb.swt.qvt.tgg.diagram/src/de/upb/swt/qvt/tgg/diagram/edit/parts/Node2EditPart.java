package de.upb.swt.qvt.tgg.diagram.edit.parts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.diagram.edit.policies.Node2ItemSemanticEditPolicy;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class Node2EditPart extends ShapeNodeEditPart implements ITGGNodeEditPart{

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2002;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public Node2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Node2ItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new ProducedTGGNodeFigure();
	}

	/**
	 * @generated
	 */
	public ProducedTGGNodeFigure getPrimaryShape() {
		return (ProducedTGGNodeFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof NodeNameTypedName2EditPart) {
			((NodeNameTypedName2EditPart) childEditPart)
					.setLabel(getPrimaryShape().getFigureNodeNameLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof NodeNameTypedName2EditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated NOT
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);

		if (((de.upb.swt.qvt.tgg.Node) ((Node) getModel()).getElement())
				.getRefinedNode() != null) {
			setLineType(SWT.LINE_DASH);
		}
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(TggVisualIDRegistry
				.getType(NodeNameTypedName2EditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(4);
		types.add(TggElementTypes.Edge_4001);
		types.add(TggElementTypes.Edge_4007);
		types.add(TggElementTypes.Edge_4008);
		types.add(TggElementTypes.NodeGraphPattern_4004);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof NodeEditPart) {
			types.add(TggElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart) {
			types.add(TggElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof Node3EditPart) {
			types.add(TggElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof NodeEditPart) {
			types.add(TggElementTypes.Edge_4007);
		}
		if (targetEditPart instanceof de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart) {
			types.add(TggElementTypes.Edge_4007);
		}
		if (targetEditPart instanceof Node3EditPart) {
			types.add(TggElementTypes.Edge_4007);
		}
		if (targetEditPart instanceof NodeEditPart) {
			types.add(TggElementTypes.Edge_4008);
		}
		if (targetEditPart instanceof de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart) {
			types.add(TggElementTypes.Edge_4008);
		}
		if (targetEditPart instanceof Node3EditPart) {
			types.add(TggElementTypes.Edge_4008);
		}
		if (targetEditPart instanceof DomainGraphPatternEditPart) {
			types.add(TggElementTypes.NodeGraphPattern_4004);
		}
		if (targetEditPart instanceof ReusablePatternEditPart) {
			types.add(TggElementTypes.NodeGraphPattern_4004);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == TggElementTypes.Edge_4001) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.Edge_4007) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.Edge_4008) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.NodeGraphPattern_4004) {
			types.add(TggElementTypes.DomainGraphPattern_2005);
			types.add(TggElementTypes.ReusablePattern_2011);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnTarget() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(4);
		types.add(TggElementTypes.Edge_4001);
		types.add(TggElementTypes.Edge_4007);
		types.add(TggElementTypes.Edge_4008);
		types.add(TggElementTypes.TGGConstraintSlotNode_4005);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForSource(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == TggElementTypes.Edge_4001) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.Edge_4007) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.Edge_4008) {
			types.add(TggElementTypes.Node_2001);
			types.add(TggElementTypes.Node_2002);
			types.add(TggElementTypes.Node_2003);
		} else if (relationshipType == TggElementTypes.TGGConstraintSlotNode_4005) {
			types.add(TggElementTypes.OCLConstraint_2010);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public class ProducedTGGNodeFigure extends RectangleFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureGrammarSideLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureNodeNameLabelFigure;

		/**
		 * @generated
		 */
		public ProducedTGGNodeFigure() {
			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);
			this.setBackgroundColor(THIS_BACK);
			this.setBorder(new MarginBorder(getMapMode().DPtoLP(2),
					getMapMode().DPtoLP(5), getMapMode().DPtoLP(2),
					getMapMode().DPtoLP(5)));
			createContents();
		}

		/**
		 * @generated NOT
		 */
		private void createContents() {

			fFigureGrammarSideLabelFigure = new WrappingLabel();
			fFigureGrammarSideLabelFigure.setText("++");
			fFigureGrammarSideLabelFigure
					.setForegroundColor(FFIGUREGRAMMARSIDELABELFIGURE_FORE);
			// jgreen: center alignment
			fFigureGrammarSideLabelFigure
					.setAlignment(PositionConstants.CENTER);

			this.add(fFigureGrammarSideLabelFigure);

			fFigureNodeNameLabelFigure = new WrappingLabel();
			fFigureNodeNameLabelFigure.setText("Node");
			// jgreen: center alignment
			fFigureNodeNameLabelFigure.setAlignment(PositionConstants.CENTER);
			fFigureNodeNameLabelFigure
					.setTextJustification(PositionConstants.CENTER);

			// jgreen: For having a wrapping text.
			fFigureNodeNameLabelFigure.setTextWrap(true);

			this.add(fFigureNodeNameLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureGrammarSideLabelFigure() {
			return fFigureGrammarSideLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureNodeNameLabelFigure() {
			return fFigureNodeNameLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 0, 150, 0);

	/**
	 * @generated
	 */
	static final Color THIS_BACK = new Color(null, 200, 255, 200);

	/**
	 * @generated
	 */
	static final Color FFIGUREGRAMMARSIDELABELFIGURE_FORE = new Color(null, 20,
			170, 20);

	/**
	 * @generated
	 */
	static final Color FFIGURENODENAMELABELFIGURE_FORE = new Color(null, 0, 75,
			0);

	/**
	 * @generated NOT
	 */
	@Override
	protected void handleNotificationEvent(Notification notification) {

		((CanonicalEditPolicy) getParent().getEditPolicy(
				EditPolicyRoles.CANONICAL_ROLE)).refresh();
		List<CanonicalEditPolicy> editPolicies = CanonicalEditPolicy
				.getRegisteredEditPolicies(this.getDiagramView().getElement());
		for (Iterator<CanonicalEditPolicy> it = editPolicies.iterator(); it
				.hasNext();) {
			CanonicalEditPolicy nextEditPolicy = it.next();
			nextEditPolicy.refresh();
		}

		if (notification.getFeature().equals(
				TggPackage.eINSTANCE.getNode_RefinedNode())) {
			if (notification.getNewValue() != null)
				setLineType(SWT.LINE_DASH);
			else
				setLineType(SWT.LINE_SOLID);
			refreshVisuals();
		}

		// }

		// TODO Auto-generated method stub
		super.handleNotificationEvent(notification);
	}
	
	public void unregisterVisuals(){
		super.unregisterVisuals();
	}
	
	public void registerVisuals() {
		super.registerVisuals();
	}
	
	public void refreshVisuals() {
		super.refreshVisuals();
	}
}
