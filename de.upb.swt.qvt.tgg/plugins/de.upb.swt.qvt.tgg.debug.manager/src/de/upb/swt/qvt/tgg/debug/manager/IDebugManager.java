package de.upb.swt.qvt.tgg.debug.manager;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;


public interface IDebugManager extends ITGGExecutor {

	public IDebugTarget getDebugTarget(ILaunch launch, IProcess process);

}
