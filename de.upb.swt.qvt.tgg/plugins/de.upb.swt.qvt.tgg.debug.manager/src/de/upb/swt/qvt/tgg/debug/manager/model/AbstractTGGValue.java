package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;


public abstract class AbstractTGGValue  extends TGGDebugElement implements IValue {

	public AbstractTGGValue(IDebugTarget target) {
		super(target);
	}

	public abstract void addCandiateNodeToValue(Node node, EObject eObject);
	
}
