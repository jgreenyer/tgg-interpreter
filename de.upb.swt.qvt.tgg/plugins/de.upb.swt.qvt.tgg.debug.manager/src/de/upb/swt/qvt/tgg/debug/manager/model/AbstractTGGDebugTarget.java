package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;

public abstract class AbstractTGGDebugTarget extends TGGDebugElement implements IDebugTarget {

	protected IProcess fProcess;
	protected ILaunch launch;
	protected AbstractTGGThread thread;

	public AbstractTGGDebugTarget(ILaunch launch, IProcess process) {
		super(null);
		this.fProcess = process;
		this.launch = launch;
		try {
			DebugEvent launchChanged = new DebugEvent(launch, DebugEvent.CHANGE);
			DebugEvent processChanged = new DebugEvent(launch.getProcesses()[0], DebugEvent.CHANGE);
			DebugEvent debugTargetChanged = new DebugEvent(launch.getDebugTarget(), DebugEvent.CREATE);
			DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] {launchChanged, processChanged, debugTargetChanged});
		} catch (Exception e) {
		}
		DebugPlugin.getDefault().getBreakpointManager().addBreakpointListener(this);
	}

	@Override
	public String getName() throws DebugException {
		return DebugConstants.TARGET_NAME;
	}
	
	@Override
	public ILaunch getLaunch() {
        return launch;
    }
	
	@Override
	public IDebugTarget getDebugTarget() {
		return this;
	}

	@Override
	public IProcess getProcess() {
		return fProcess;
	}

	@Override
	public abstract IThread[] getThreads() throws DebugException;

	@Override
	public boolean hasThreads() throws DebugException {
		return true;
	}

	@Override
	public boolean supportsBreakpoint(IBreakpoint breakpoint) {
		return true;
	}

	@Override
	public String getModelIdentifier() {
		return DebugConstants.TARGET_NAME;
	}

	@Override
	public boolean canTerminate() {
		return fProcess.canTerminate();
	}

	@Override
	public boolean isTerminated() {
		return fProcess.isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
		DebugPlugin.getDefault().getBreakpointManager().removeBreakpointListener(this);
		fireTerminateEvent();
		fProcess.terminate();
	}

	@Override
	public boolean canResume() {
		return false;
	}

	@Override
	public boolean canSuspend() {
		return false;
	}

	@Override
	public boolean isSuspended() {
		return false;
	}

	@Override
	public void resume() throws DebugException {
	}

	@Override
	public void suspend() throws DebugException {
	}

	@Override
	public void breakpointAdded(IBreakpoint breakpoint) {
	}

	@Override
	public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {
	}

	@Override
	public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {
	}

	@Override
	public boolean canDisconnect() {
		return false;
	}

	@Override
	public void disconnect() throws DebugException {
	}

	@Override
	public boolean isDisconnected() {
		return isTerminated();
	}

	@Override
	public IMemoryBlock getMemoryBlock(long startAddress, long length) throws DebugException {
		return null;
	}

	@Override
	public boolean supportsStorageRetrieval() {
		return false;
	}
	
	public AbstractTGGThread getThread(){
		return thread;
	}
}
