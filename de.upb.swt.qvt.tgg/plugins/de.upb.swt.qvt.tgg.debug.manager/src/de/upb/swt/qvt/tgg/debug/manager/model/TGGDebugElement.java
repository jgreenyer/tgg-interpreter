package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.debug.core.model.DebugElement;
import org.eclipse.debug.core.model.IDebugElement;
import org.eclipse.debug.core.model.IDebugTarget;

public class TGGDebugElement extends DebugElement implements IDebugElement {
	
	public TGGDebugElement(IDebugTarget target){
		super(target);
	}
	
	@Override
	public String getModelIdentifier() {
		return "TGGDebugElement";
	}
}
