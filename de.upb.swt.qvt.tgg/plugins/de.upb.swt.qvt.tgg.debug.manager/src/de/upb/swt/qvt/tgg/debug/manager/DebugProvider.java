package de.upb.swt.qvt.tgg.debug.manager;

import java.util.ArrayList;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGDebugTarget;
import de.upb.swt.qvt.tgg.debug.manager.model.TGGNodeBreakpoint;
import de.upb.swt.qvt.tgg.debug.manager.model.TGGRuleBreakpoint;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.internal.core.BreakpointManager;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class DebugProvider extends AbstractUIPlugin implements BundleActivator {
	
	public static final String PLUGIN_ID = "de.upb.swt.qvt.tgg.interpreter.debug.manager";	
	private static DebugProvider plugin;
	private static BundleContext context;
	private AbstractTGGDebugTarget debugTarget;
	private ArrayList<ITGGDebugEventListener> diagramListeners;
	
	public static BundleContext getContext() {
		return context;
	}

	public static DebugProvider getDefault() {
		return plugin;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		diagramListeners = new ArrayList<ITGGDebugEventListener>();
		DebugProvider.context = bundleContext;
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		DebugProvider.context = null;
		plugin = null;
	}

	public void addBreakpoint(IResource resource, Object object, String ruleName) {
		try {
			IBreakpoint tggbreakpoint = null;
			if(object instanceof TripleGraphGrammarRule){
				tggbreakpoint = new TGGRuleBreakpoint(resource, (TripleGraphGrammarRule)object);
			} else if(object instanceof Node){
				tggbreakpoint = new TGGNodeBreakpoint(resource, (Node)object, ruleName);
			}
			if(tggbreakpoint != null){
				BreakpointManager manager = (BreakpointManager)DebugPlugin.getDefault().getBreakpointManager();
				manager.addBreakpoint(tggbreakpoint);
			}
		} catch (CoreException e) {
		}
	}
	
	public boolean ruleContainsBreakpoint(TripleGraphGrammarRule rule){
		IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
		boolean isBreakpointFound = false;
			if(breakpoints.length > 0){				
				for(int i=0; i < breakpoints.length; i++){
					IMarker marker = breakpoints[i].getMarker();
					try {
						if(marker.getAttribute("nodename") == null){
							if(marker.getAttribute("rulename").equals(rule.getName()) &&
									breakpoints[i].isEnabled()){
								isBreakpointFound = true;
								break;
							} 
						}
					} catch (Exception e) {	}
				}				
			}
		return isBreakpointFound;
	}
		
	public boolean nodeContainsBreakpoint(Node neighborNode,TripleGraphGrammarRule rule) {

		IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
		boolean isBreakpoint = false;
		if (breakpoints.length > 0) {

			for (int i = 0; i < breakpoints.length; i++) {
				IMarker marker = breakpoints[i].getMarker();
				try {
					if (marker.getAttribute("nodetype") != null	&& neighborNode.getTypedName() != null) {
						if (marker.getAttribute("nodetype").equals(neighborNode.getTypedName())) {
							if (marker.getAttribute("rulename").equals(rule.getName())) {
								if (neighborNode.getName() != null) {
									if (neighborNode.getName().equals(marker.getAttribute("nodename")) &&
											breakpoints[i].isEnabled()) {										
										isBreakpoint = true;
										break;
									}
								} else {
									isBreakpoint = true;
									break;
								}
							}
						}
					}
				} catch (Exception e) {
				}
			}
		}
		return isBreakpoint;
	}
	
	public IBreakpointManager getBreakpointManager(){
		return DebugPlugin.getDefault().getBreakpointManager();
	}
	
	public void setDebugTarget(AbstractTGGDebugTarget debugTarget){
		this.debugTarget = debugTarget;
	}
	
	public AbstractTGGDebugTarget getDebugTarget(){
		return debugTarget;
	}
	
	public void changeFigure(Object editPart, boolean isCurrentDebugNode, int indexOfNodeMatching){
		for (ITGGDebugEventListener listener : diagramListeners){
			listener.changeFigure(editPart, isCurrentDebugNode, indexOfNodeMatching);
		}
	}
	
	public void clearDiagram(Object editPart){
		for (ITGGDebugEventListener listener : diagramListeners){
			listener.clearDiagram(editPart);
		}
	}
	
	public void clearDiagram(){
		for (ITGGDebugEventListener listener : diagramListeners){
			listener.clearDiagram();
		}
	}
	
	public ArrayList getDiagramListeners(){
		return diagramListeners;
	}
}
