package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.debug.core.model.IThread;

import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;

public abstract class AbstractTGGProcess implements IProcess {

	public static final String ATTR_CONFIG_FILE = DebugProvider.getContext().getBundle().getSymbolicName() + ".ATTR_CONFIG_FILE"; 
	private String fConfigFile;
	private ILaunch fLaunch;

	public AbstractTGGProcess(ILaunch launch) {
		this.fLaunch = launch;
	}

	public abstract void run();
	
	protected void clearDiagram(){
		//DiagramElements.clearDiagram();
	}
	
	@Override
	public String getAttribute(String key) {
		if (ATTR_CONFIG_FILE.equals(key))
			return fConfigFile;
		else
			return null;
	}

	@Override
	public abstract int getExitValue() throws DebugException;	
	
	public IDebugTarget getDebugTarget() {
		return fLaunch.getDebugTarget();
	}

	@Override
	public String getLabel() {
		return "TGG Tool";
	}

	@Override
	public ILaunch getLaunch() {
		return fLaunch;
	}

	@Override
	public IStreamsProxy getStreamsProxy() {
		return null;
	}

	@Override
	public void setAttribute(String key, String value) {
		if (ATTR_CONFIG_FILE.equals(key))
			fConfigFile = value;
		else
			return;
	}
	
	public void setDebugTarget(IDebugTarget debugTarget){	
		//do not allow overwriting debug target
		assert(false);
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return null;
	}

	@Override
	public boolean canTerminate() {
		return false;
	}

	@Override
	public abstract boolean isTerminated();

	@Override
	public abstract void terminate() throws DebugException;
	
	public IThread getMainThread() {
		try {
			return fLaunch.getDebugTarget().getThreads()[0];
		} catch (DebugException e) {
		}
		return null;
	}
}