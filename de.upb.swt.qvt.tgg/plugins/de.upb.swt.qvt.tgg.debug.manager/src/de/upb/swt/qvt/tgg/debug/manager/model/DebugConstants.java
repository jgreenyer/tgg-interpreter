package de.upb.swt.qvt.tgg.debug.manager.model;

public final class DebugConstants  {

	  public static final String EXECUTE_TRANSFORMATION = "Transformation Execution";
	  
	  public static final String TRANSFORMATION_PROPERTIES = "Transformation Properties";
	  
	  public static final String MATCH_RULE_AND_EXECUTE = "Rule Matching and Execution";
	  
	  public static final String PATTERMATCHING = "Pattern Matching";
	  
	  public static final String APPLY_RULE = "Rule Application";
	  
	  public static final String CONFIGURATION_VARIABLE = "Configuration Variable";
	  
	  public static final String CANDIDATE_RULE = "Candidate Rule";
	  
	  public static final String APPLIED_RULES = "Applied Rules";
	  
	  public static final String CANDIDATE_NODE_BINDING = "Candidate Node Binding";
	  
	  public static final String ENFORCED_NODES = "Enforced Nodes";
	  
	  public static final String MATCHED_NODES = "Matched Nodes";
	  
	  public static final String NODE = "Node";
	  
	  public static final String RULEBINDINGNODE = "rulebindingnode";
	  
	  public static final String THREAD_NAME = "TGG Debug Thread";
	  
	  public static final String TARGET_NAME = "TGG Debug Target";
	  
	  public static final String NODETYPE = "nodetype";
	  
	  public static final String NODENAME = "nodename";
	  
	  public static final String RULENAME = "rulename";
	  
	  public static final String ADDNODE = "addnode";
	  
	  public static final String NULL = "null";
	  
	  public static final String NOTHING = "";
	  
	  public static final String ADD_APPLIED_RULE_TO_VALUE = "add applied rule to value";
	 
	  private DebugConstants(){
	    throw new AssertionError();
	  }
} 
