package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.model.Breakpoint;
import org.eclipse.debug.core.model.IBreakpoint;

import de.upb.swt.qvt.tgg.Node;


public class TGGNodeBreakpoint extends Breakpoint { 
	
	public TGGNodeBreakpoint(){
	}
	
	public TGGNodeBreakpoint(final IResource resource, final Node node, final String ruleName) throws CoreException {
			IMarker marker = resource.createMarker("de.upb.swt.qvt.tgg.debug.manager.model.TGGBreakpointNodeMarker");
			setMarker(marker);
			marker.setAttribute(IBreakpoint.ENABLED, Boolean.TRUE);
			marker.setAttribute(IBreakpoint.ID, getModelIdentifier());
			marker.setAttribute(IBreakpoint.REGISTERED, Boolean.TRUE);
			marker.setAttribute(DebugConstants.NODETYPE, node.getTypedName());
			if(node.getName() != null){
				marker.setAttribute(DebugConstants.NODENAME, node.getName());
			} else {
				marker.setAttribute(DebugConstants.NODENAME, DebugConstants.NULL);
			}
			marker.setAttribute(DebugConstants.RULENAME, ruleName);
			marker.setAttribute(IMarker.MESSAGE, "TGG Breakpoint: ");
	}
	

	@Override
	public String getModelIdentifier() {
		return "de.upb.swt.qvt.tgg.debug.manager.model.TGGBreakpointNodeMarker";
	}
	
	@Override
	public String toString() {
		return super.toString();
	}	
}
