package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IRegisterGroup;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;



public abstract class AbstractTGGStackFrame extends TGGDebugElement implements IStackFrame {

	private AbstractTGGThread fThread;
	private Object object;


	public AbstractTGGStackFrame(AbstractTGGThread thread, String entry, Object object){
		super(thread.getDebugTarget());
		this.fThread = thread;
		this.object = object;
	}

	public Object getObject() throws DebugException {
		return object;
	}

	@Override
	public int getCharEnd() throws DebugException {
		return -1;
	}

	@Override
	public int getCharStart() throws DebugException {
		return -1;
	}

	@Override
	public int getLineNumber() throws DebugException {
		return -1;
	}

	@Override
	public IRegisterGroup[] getRegisterGroups() throws DebugException {
		return null;
	}

	@Override
	public IThread getThread() {
		return fThread;
	}

	@Override
	public boolean hasRegisterGroups() throws DebugException {
		return false;
	}

	@Override
	public String getModelIdentifier() {
		return "AbstractTGGInterpreterStackFrame";
	}
	
	public abstract AbstractTGGVariable getSpecificNodesVariable(String searchConstant);
	
	public abstract void setStepInto(boolean isStepInto);	

	public abstract void setStepOver(boolean isStepOver);
	
	public abstract boolean isStepInto();

	public abstract boolean isStepOver();	
}
