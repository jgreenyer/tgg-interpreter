package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.model.Breakpoint;
import org.eclipse.debug.core.model.IBreakpoint;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;


public class TGGRuleBreakpoint extends Breakpoint { 
	
	public TGGRuleBreakpoint(){
	}

	public TGGRuleBreakpoint(final IResource resource, final TripleGraphGrammarRule tgg) throws CoreException {
		 IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
	            public void run(IProgressMonitor monitor) throws CoreException {
	            	IMarker marker = resource.createMarker("de.upb.swt.qvt.tgg.debug.manager.model.TGGBreakpointRuleMarker");
	            	setMarker(marker);
	            	setAttribute(IBreakpoint.ENABLED, Boolean.TRUE);
	            	setAttribute(IBreakpoint.ID, getModelIdentifier());
	            	setAttribute(DebugConstants.RULENAME, tgg.getName());
	            	setAttribute(IMarker.MESSAGE, "TGG Breakpoint");
	            }
	        };
	        run(getMarkerRule(resource), runnable);
	}

	@Override
	public String getModelIdentifier() {
		return "de.upb.swt.qvt.tgg.debug.manager.model.TGGBreakpointRuleMarker";
	}
}
