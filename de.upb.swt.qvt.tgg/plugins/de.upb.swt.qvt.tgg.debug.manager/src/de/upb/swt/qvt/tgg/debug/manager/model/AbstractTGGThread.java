package de.upb.swt.qvt.tgg.debug.manager.model;

import java.util.ArrayList;

import org.eclipse.core.resources.IMarker;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public abstract class AbstractTGGThread extends TGGDebugElement implements IThread {
	
	protected ArrayList<AbstractTGGStackFrame> stackList = new ArrayList<AbstractTGGStackFrame>();
	protected ArrayList<IBreakpoint> breakpointList = new ArrayList<IBreakpoint>();
//	private ArrayList<RuleBindingWrapper> appliedRulebindings = new ArrayList<RuleBindingWrapper>();
	
	public  static Object classInstanceWhichWaitsCurrently;
	public  static Object classInstanceForFrontTransformationNodeToWAIT;
//	private static ShapeNodeEditPart lastNodePart;
//	private static ShapeNodeEditPart currentNodePart;
	
	protected boolean resume = false;
	protected boolean threadIsRunning = true;
	protected boolean isTransformationRunning = false;
	protected boolean isStepping;
	protected boolean hasToStepOverPatternMatching = false;
	
	protected int indexOfNodes = 0;
	
	protected String waitingFor;
//	private FrontTransformationNode frontTransformationNode= null;
//	private RuleBindingWrapper rulebindingwrapper;
	protected TripleGraphGrammarRule tripleGraphGrammarRule;
	
	public AbstractTGGThread(IDebugTarget target) {
		super(target);
		this.waitingFor = DebugConstants.NOTHING;
		try {
			DebugEvent launchChanged = new DebugEvent(target.getLaunch(), DebugEvent.CHANGE);
			DebugEvent processChanged = new DebugEvent(target.getLaunch().getProcesses()[0], DebugEvent.CHANGE);
			DebugEvent debugTargetChanged = new DebugEvent(target.getLaunch().getDebugTarget(), DebugEvent.CHANGE);
			DebugEvent threadChanged = new DebugEvent(this, DebugEvent.CREATE);
			DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] {launchChanged, processChanged, debugTargetChanged, threadChanged});
		} catch (Exception e) {
		}
	}

//	public ArrayList<RuleBindingWrapper> getAppliedRulebindings() {
//		return appliedRulebindings;
//	}
	
	public boolean isHasToStepOverPatternMatching() {
		return hasToStepOverPatternMatching;
	}

	public void setHasToStepOverPatternMatching(boolean hasToStepOverPatternMatching) {
		this.hasToStepOverPatternMatching = hasToStepOverPatternMatching;
	}

//	@Override
//	public IBreakpoint[] getBreakpoints() {
//		IBreakpoint[] breakpoints = DebugProvider.getDefault().getBreakpointManager().getBreakpoints();
//		for(int i = 0; i < breakpoints.length; i++){
//			if(breakpoints[i] instanceof TGGNodeBreakpoint || breakpoints[i] instanceof TGGRuleBreakpoint){
//				breakpointList.add(breakpoints[i]);
//			}
//		}
//		IBreakpoint[] breakpointscopy = new IBreakpoint[breakpointList.size()];
//		for(int i = 0; i < breakpointList.toArray().length; i++){
//			breakpointscopy[i] = (IBreakpoint)breakpointList.toArray()[breakpointList.size()-1-i];
//		}
//		return breakpointscopy;	
//	}

	@Override
	public String getName() throws DebugException {
		return DebugConstants.THREAD_NAME;
	}

	@Override
	public int getPriority() throws DebugException {
		return 0;
	}

	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		IStackFrame[] stackFrame = new IStackFrame[stackList.size()];
		for(int i = 0; i < stackList.toArray().length; i++){
			stackFrame[i] = (IStackFrame)stackList.get(stackList.size()-1-i);
		}
		return stackFrame;
	}

	@Override
	public IStackFrame getTopStackFrame() throws DebugException {
		if (stackList.size() > 0) {
 			return stackList.get(0);
 		}	
 		return null;
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return !stackList.isEmpty();
	}

	@Override
	public String getModelIdentifier() {
		return DebugConstants.THREAD_NAME;
	}

	@Override
	public boolean canResume() {
		return isTransformationRunning;
	}

	@Override
	public boolean canSuspend() {
		return false;
	}

	@Override
	public boolean isSuspended() {
		return false;
	}
	
//	public void changePaintedEditpart(){
//		if(currentNodePart != null){
//			if(currentNodePart instanceof NodeEditPart){
//				((NodeEditPart)currentNodePart).changeFigure(false, -1);
//			} else if(currentNodePart instanceof Node2EditPart){
//				((Node2EditPart)currentNodePart).changeFigure(false, -1);
//			}
//		}
//	}
	
	@Override
	public void resume() throws DebugException {
		resume = true;
//		changePaintedEditpart();
		setWaitingFor(DebugConstants.NOTHING);
		setClassInstanceNotify(null);
	}

	@Override
	public void suspend() throws DebugException {
	}

	@Override
	public boolean canStepInto() {
		return false;
	}

	@Override
	public boolean canStepOver() {
		return false;
	}

	@Override
	public boolean canStepReturn() {
		return false;
	}

	@Override
	public boolean isStepping() {
		return isStepping;
	}

	@Override
	public void stepInto() throws DebugException {
//		changePaintedEditpart();
	}

	@Override
	public void stepOver() throws DebugException {
//		changePaintedEditpart();
	}

	@Override
	public void stepReturn() throws DebugException {
	}

	@Override
	public boolean canTerminate() {
		return isTransformationRunning;
	}

	@Override
	public boolean isTerminated() {
		return getDebugTarget().isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
//		changePaintedEditpart();
		setStepping(false);
		setClassInstanceNotify(null);
		getDebugTarget().terminate();
	}
	
	public void setClassInstanceWait(Object classInstance) {
		threadIsRunning = false;
		if(classInstance != null){
			AbstractTGGThread.classInstanceWhichWaitsCurrently = classInstance;
			try {
				synchronized( classInstance ) 
				{ 
					classInstance.wait(); 
				}
			} catch(Exception e){
			}
		} else {
			try {
				synchronized(AbstractTGGThread.classInstanceWhichWaitsCurrently ) 
				{ 
					AbstractTGGThread.classInstanceWhichWaitsCurrently.wait(); 
				}
			} catch(Exception e){
			}
		}
	}
	
	public void setClassInstanceNotify(Object classInstance) {
		threadIsRunning = true;
		if(classInstance != null){
			synchronized( classInstance ) 
			{ 
				classInstance.notifyAll(); 
			}
		} else {
			synchronized( AbstractTGGThread.classInstanceWhichWaitsCurrently ) 
			{ 
				AbstractTGGThread.classInstanceWhichWaitsCurrently.notifyAll(); 
			}
		}
	}
	
	protected void changeDebugEvent(AbstractTGGStackFrame frame){
		try {
			DebugEvent launchChanged = new DebugEvent(getLaunch(), DebugEvent.CHANGE);
			DebugEvent processChanged = new DebugEvent(getLaunch().getProcesses()[0], DebugEvent.CHANGE);
			DebugEvent debugTargetChanged = new DebugEvent(getDebugTarget(), DebugEvent.CHANGE);
			DebugEvent stackChanged = new DebugEvent(frame, DebugEvent.CHANGE);
			DebugEvent threadChanged= new DebugEvent(getLaunch().getDebugTarget().getThreads()[0], DebugEvent.CHANGE);
			if(frame.getSpecificNodesVariable(DebugConstants.MATCHED_NODES) != null){
				DebugEvent variableChanged = new DebugEvent(frame.getSpecificNodesVariable(DebugConstants.MATCHED_NODES), DebugEvent.CHANGE);
				DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] {launchChanged, processChanged, debugTargetChanged, threadChanged, stackChanged, variableChanged});
			} else if(frame.getSpecificNodesVariable(DebugConstants.ENFORCED_NODES) != null){
				DebugEvent variableChanged = new DebugEvent(frame.getSpecificNodesVariable(DebugConstants.ENFORCED_NODES), DebugEvent.CHANGE);
				DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] {launchChanged, processChanged, debugTargetChanged, threadChanged, stackChanged, variableChanged});
			} else {
				DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] {launchChanged, processChanged, debugTargetChanged, threadChanged, stackChanged});
			}
		} catch (DebugException e) {
		}
	}
	
	protected void showErrorInConsole(){
		System.out.println("Please open the concrete editor window.");
	}
	
	protected void showOpenTheRightDiagram(String diagramName){		
		System.out.println("Please open the right TGG-Diagram: "+diagramName);		
	}
	
	protected boolean isNodeABreakpoint(Node node){
		try{
			IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
			if(breakpoints.length > 0){
				for(int i=0; i < breakpoints.length; i++){
					IMarker marker = breakpoints[i].getMarker();
					if( marker.getAttribute("nodetype") != null && node.getTypedName() != null){
						if(marker.getAttribute("nodetype").equals(node.getTypedName())){
							if(marker.getAttribute("rulename").equals(this.tripleGraphGrammarRule.getName())){
								if(node.getName() != null){
									if(node.getName().equals(marker.getAttribute("nodename"))){
										return true&breakpoints[i].isEnabled();											
									}
								} else {
									return true&breakpoints[i].isEnabled();
								}											
							}								
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			} else {
				return false;
			}
		} catch(Exception e){	
		}
		return false;
	}
	
	
//	public void addEnforceNode(Node node, EObject eObject, RuleBindingWrapper rulebinding, Object classInstance, boolean isRunning, boolean isBreakpointFound) {
//		try {
//			boolean goOut = false;
//			boolean isTGGDiagrammEditorFound = false;
//			if(rulebindingwrapper != null){
//				if(rulebindingwrapper.getUID().equals(rulebinding.getUID())){
//					indexOfNodes++;
//				} else {
//					indexOfNodes = 0;
//				}
//			} 
//			TripleGraphGrammarRule ruleOfOpenEditor;
//			String nameOfRuleOfOpenEditor;
//			for(int i = 0 ;i < stackList.size() ;i++) {
//					if(stackList.get(i).getName().equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
//						((TGGValue)stackList.get(i).getSpecificNodesVariable(DebugConstants.ENFORCED_NODES).getValue()).addNodeToValue(node, eObject);
//						changeDebugEvent(stackList.get(i));
//						if(isStepping || isBreakpointFound){
//							IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
//							if(windows.length < 1){
//								showErrorInConsole();
//							}
//							for(int u=0; u < windows.length; u++){
//								if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
//									isTGGDiagrammEditorFound = true;
//									TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
//									target.getRootEditpart();
//									for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
//										EditPart part = (EditPart)j.next();
//										if(part instanceof TripleGraphGrammarRuleEditPart){
//											DiagramImpl diagramImpl = (DiagramImpl)((TripleGraphGrammarRuleEditPart)part).getModel();
//											ruleOfOpenEditor = (TripleGraphGrammarRuleImpl)diagramImpl.getElement();
//											nameOfRuleOfOpenEditor = ruleOfOpenEditor.getName();
//											TGGStackFrame tggApplyRuleStackframe = null;
//											for(int a = 0; a < stackList.size(); a++){
//												if(stackList.get(a).getName().equals(DebugConstants.APPLY_RULE)){
//													tggApplyRuleStackframe = stackList.get(a);
//													break;
//												}
//											}
//											String currentRuleOfDebugger = ((TripleGraphGrammarRule)tggApplyRuleStackframe.getObject()).getName();
//											if(currentRuleOfDebugger.equals(nameOfRuleOfOpenEditor)){
//												TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
//												Iterator k =rulepart.getChildren().iterator();
//												while(k.hasNext() && !goOut){
//													EditPart partOfRule = (EditPart)k.next();
//													if(partOfRule instanceof NodeEditPart){
//														NodeEditPart nodepart = (NodeEditPart)partOfRule;
//														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
//														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
//														if(tggNodeOfGraph.getTypedName().equals(node.getTypedName())){
//															if(tggNodeOfGraph.getName() != null && node.getName() != null){
//																if(tggNodeOfGraph.getName().equals(node.getName())){
//																	if(lastNodePart != null){
//																		if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																			((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																			((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		}
//																	}
//																	currentNodePart = nodepart;
//																	nodepart.changeFigure(true, indexOfNodes);
//																	AbstractTGGThread.lastNodePart = nodepart;
//																	goOut = true;
//																}
//															} else {
//																if(lastNodePart != null){
//																	if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																		((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																		((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	}
//																}
//																currentNodePart = nodepart;
//																nodepart.changeFigure(true, indexOfNodes);
//																AbstractTGGThread.lastNodePart = nodepart;
//																goOut = true;
//															}
//														}
//													} else if(partOfRule instanceof Node2EditPart){
//														Node2EditPart nodepart = (Node2EditPart)partOfRule;
//														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
//														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
//						
//														if(tggNodeOfGraph.getTypedName().equals(node.getTypedName())){
//															if(tggNodeOfGraph.getName() != null && node.getName() != null){
//																if(tggNodeOfGraph.getName().equals(node.getName())){
//																	if(lastNodePart != null){
//																		if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																			((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																			((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		}
//																	}
//																	currentNodePart = nodepart;
//																	nodepart.changeFigure(true, indexOfNodes);
//																	AbstractTGGThread.lastNodePart = nodepart;
//																	goOut = true;
//																}
//															} else {
//																if(lastNodePart != null){
//																	if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																		((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																		((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	}
//																}
//																currentNodePart = nodepart;
//																nodepart.changeFigure(true, indexOfNodes);
//																AbstractTGGThread.lastNodePart = nodepart;
//																goOut = true;
//															}
//														}
//													} else if(partOfRule instanceof DomainGraphPatternEditPart){
//													}
//												}
//											} else {
//												System.out.println("Please open the right TGG-Diagram: "+ currentRuleOfDebugger);
//											}
//										}
//									}
//								} 
//								if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
//									setClassInstanceWait(classInstance);
//								} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
//									if(DebugConstants.NODE.equals(getWaitingFor()) && classInstance != null){
//										setClassInstanceWait(classInstance);
//										refreshStackFrames(); 
//									} 
//								} 
//							}
//							if(!isTGGDiagrammEditorFound){					
//								showErrorInConsole();
//							}
//						}
//					}
//			}
//		} catch (DebugException e) {
//		}
//		if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
//			setClassInstanceWait(classInstance);
//		} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
//			if(DebugConstants.MATCH_RULE_AND_EXECUTE.equals(getWaitingFor()) && classInstance != null && !hasToStepOverPatternMatching){
//				setClassInstanceWait(classInstance);
//				refreshStackFrames(); 
//			}
//		} 
//	}
	
	public void addCandidateNodeForPatternMatching(Node node, EObject eObject, Object classInstance) {
		for(int i = 0 ;i < stackList.size() ;i++) {
			try {
				if(stackList.get(i).getName().equals(DebugConstants.PATTERMATCHING)){
					((AbstractTGGValue)stackList.get(i).getSpecificNodesVariable(DebugConstants.CANDIDATE_NODE_BINDING).getValue()).addCandiateNodeToValue(node, eObject);
				}
			} catch (DebugException e) {
			}
		}
	}

	
//	public void addMatchedNodeForPatternMatching(Node node, EObject eObject, RuleBindingWrapper rulebinding, boolean isFrontTransformationNode, Object classInstance, boolean isRunning, boolean isBreakpointFound) {
//		boolean goOut = false;
//		boolean isTGGDiagrammEditorFound = false;
//		AbstractTGGThread.classInstanceForFrontTransformationNodeToWAIT = classInstance;
//		if(isFrontTransformationNode){
//			frontTransformationNode = new FrontTransformationNode();
//			frontTransformationNode.eObject = eObject;
//			frontTransformationNode.isBreakpointFound = isBreakpointFound;
//			frontTransformationNode.isRunning = isRunning;
//			frontTransformationNode.node = node;
//			frontTransformationNode.rulebinding = rulebinding;
//		} else {
//			if(frontTransformationNode != null){
//				Node copynode = frontTransformationNode.node;
//				EObject copyeObject= frontTransformationNode.eObject; 
//				RuleBindingWrapper copyrulebinding = frontTransformationNode.rulebinding; 
//				Object copyclassInstance = classInstance; 
//				boolean copyisRunning = frontTransformationNode.isRunning;
//				boolean copyisBreakpointFound = isNodeABreakpoint(copynode);
//				frontTransformationNode = null;
//				addMatchedNodeForPatternMatching(copynode, copyeObject, copyrulebinding, false, copyclassInstance, copyisRunning, copyisBreakpointFound);
//			}
//			if(rulebindingwrapper != null){
//				if(rulebindingwrapper.getUID().equals(rulebinding.getUID())){
//					indexOfNodes++;
//				}
//			} else {
//				indexOfNodes = 0;
//			}
//			TripleGraphGrammarRule ruleOfOpenEditor;
//			String nameOfRuleOfOpenEditor;
//			for(int i = 0 ;i < stackList.size() ;i++) {
//				try {
//					if(stackList.get(i).getName().equals(DebugConstants.PATTERMATCHING)){
//						((AbstractTGGValue)stackList.get(i).getSpecificNodesVariable(DebugConstants.MATCHED_NODES).getValue()).addNodeToValue(node, eObject);
//						changeDebugEvent(stackList.get(i));
//						if(isStepping || isBreakpointFound){
//							IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
//							if(windows.length < 1){
//								showErrorInConsole();
//							}
//							for(int u=0; u < windows.length; u++){
//								if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
//									isTGGDiagrammEditorFound = true;
//									TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
//									target.getRootEditpart();
//									for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
//										EditPart part = (EditPart)j.next();
//										if(part instanceof TripleGraphGrammarRuleEditPart){
//											DiagramImpl diagramImpl = (DiagramImpl)((TripleGraphGrammarRuleEditPart)part).getModel();
//											ruleOfOpenEditor = (TripleGraphGrammarRuleImpl)diagramImpl.getElement();
//											nameOfRuleOfOpenEditor = ruleOfOpenEditor.getName();
//											TGGStackFrame tggApplyRuleStackframe = null;
//											for(int a = 0; a < stackList.size(); a++){
//												if(stackList.get(a).getName().equals(DebugConstants.APPLY_RULE)){
//													tggApplyRuleStackframe = stackList.get(a);
//													break;
//												}
//											}
//											String currentRuleOfDebugger = ((TripleGraphGrammarRule)tggApplyRuleStackframe.getObject()).getName();
//											if(currentRuleOfDebugger.equals(nameOfRuleOfOpenEditor)){
//												TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
//												Iterator k =rulepart.getChildren().iterator();
//												while(k.hasNext() && !goOut){
//													EditPart partOfRule = (EditPart)k.next();
//													if(partOfRule instanceof NodeEditPart){
//														NodeEditPart nodepart = (NodeEditPart)partOfRule;
//														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
//														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
//														if(tggNodeOfGraph.getTypedName().equals(node.getTypedName())){
//															if(tggNodeOfGraph.getName() != null && node.getName() != null){
//																if(tggNodeOfGraph.getName().equals(node.getName())){
//																	if(lastNodePart != null){
//																		if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																			((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																			((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		}
//																	}
//																	currentNodePart = nodepart;
//																	nodepart.changeFigure(true, indexOfNodes);
//																	AbstractTGGThread.lastNodePart = nodepart;
//																	goOut = true;
//																}
//															} else {
//																if(lastNodePart != null){
//																	if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																		((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																		((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	}
//																}
//																currentNodePart = nodepart;
//																nodepart.changeFigure(true, indexOfNodes);
//																AbstractTGGThread.lastNodePart = nodepart;
//																goOut = true;
//															}
//														}
//													} else if(partOfRule instanceof Node2EditPart){
//														Node2EditPart nodepart = (Node2EditPart)partOfRule;
//														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
//														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
//						
//														if(tggNodeOfGraph.getTypedName().equals(node.getTypedName())){
//															if(tggNodeOfGraph.getName() != null && node.getName() != null){
//																if(tggNodeOfGraph.getName().equals(node.getName())){
//																	if(lastNodePart != null){
//																		if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																			((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																			((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																		}
//																	}
//																	currentNodePart = nodepart;
//																	nodepart.changeFigure(true, indexOfNodes);
//																	AbstractTGGThread.lastNodePart = nodepart;
//																	goOut = true;
//																}
//															} else {
//																if(lastNodePart != null){
//																	if(AbstractTGGThread.lastNodePart instanceof NodeEditPart){
//																		((NodeEditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	} else if(AbstractTGGThread.lastNodePart instanceof Node2EditPart){
//																		((Node2EditPart)AbstractTGGThread.lastNodePart).changeFigure(false, -1);
//																	}
//																}
//																currentNodePart = nodepart;
//																nodepart.changeFigure(true, indexOfNodes);
//																AbstractTGGThread.lastNodePart = nodepart;
//																goOut = true;
//															}
//														}
//													} else if(partOfRule instanceof DomainGraphPatternEditPart){
//													}
//												}
//											} else {
//												System.out.println("Please open the right TGG-Diagram: "+ currentRuleOfDebugger);
//											}
//										}
//									}
//								} 
//								if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING)){// && !resume){
//									setClassInstanceWait(classInstance);
//								} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
//									if(DebugConstants.NODE.equals(getWaitingFor()) && classInstance != null){
//										setClassInstanceWait(classInstance);
//										refreshStackFrames(); 
//									} 
//								} 
//							}
//							if(!isTGGDiagrammEditorFound){					
//								showErrorInConsole();
//							}
//						}
//					}
//				} catch (DebugException e) {
//				}
//			}
//			if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
//				setClassInstanceWait(classInstance);
//			} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
//				if(DebugConstants.PATTERMATCHING.equals(getWaitingFor()) && classInstance != null && !hasToStepOverPatternMatching){
//					setClassInstanceWait(classInstance);
//					refreshStackFrames(); 
//				}
//			} 
//		}
//		this.rulebindingwrapper = rulebinding;
//	}
	
	protected void refreshStackFrames(){
		for(int i = 0 ;i < stackList.size() ;i++) {
			try {
				if(stackList.get(i).isStepInto() || stackList.get(i).isStepOver()){
					if(stackList.get(i) instanceof AbstractTGGStackFrame){
						AbstractTGGStackFrame frame = stackList.get(i);
						frame.setStepInto(false);
						frame.setStepOver(false);
					}
				}
			} catch (Exception e) {
			}
		}
	}
	
//	private void clearDiagram(){
//		try {
//			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
//			for(int u=0; u < windows.length; u++){
//				if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
//					TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
//					target.getRootEditpart();
//					for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
//						EditPart part = (EditPart)j.next();
//						if(part instanceof TripleGraphGrammarRuleEditPart){
//							TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
//							Iterator k =rulepart.getChildren().iterator();
//							while(k.hasNext()){
//								EditPart partOfRule = (EditPart)k.next();
//								if(partOfRule instanceof NodeEditPart){
//									NodeEditPart nodepart = (NodeEditPart)partOfRule;
//									nodepart.clearDiagram();
//								} else if(partOfRule instanceof Node2EditPart){
//									Node2EditPart nodepart = (Node2EditPart)partOfRule;
//									nodepart.clearDiagram();
//								}
//							}
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//		}
//	}
	
//	public void addOrDeleteTGGInterpreterStackFrame(String entry, Object object, boolean isRunning, Object classInstance, boolean isBreakpointFound) {
//		changePaintedEditpart();
//		if(isRunning){
//			clearDiagram();
//			if(object instanceof TripleGraphGrammarRule){
//				this.tripleGraphGrammarRule = (TripleGraphGrammarRule)object;
//				indexOfNodes = 0;
//			}
//			if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
//				isTransformationRunning = true;
//			}
//			TGGStackFrame newStackFrame = new TGGStackFrame(this, entry, object);
//			stackList.add(newStackFrame);
//			if(entry.equals(DebugConstants.APPLY_RULE)){
//				for(int i = 0 ;i < appliedRulebindings.size() ;i++) {
//					try {
//						((AbstractTGGValue)newStackFrame.getSpecificNodesVariable(DebugConstants.APPLIED_RULES).getValue()).addAppliedRuleBinding(appliedRulebindings.get(i));
//					} catch (DebugException e) {
//					}
//				}
//			}
//			changeDebugEvent(newStackFrame);
//			if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING)){
//				setClassInstanceWait(classInstance);
//			} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
//				if(entry.equals(getWaitingFor()) && classInstance != null){
//					setClassInstanceWait(classInstance);
//					refreshStackFrames(); 
//				}
//			} 
//		} else if(deleteStackFrame(entry, object) != null) {
//			if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
//				isTransformationRunning = false;
//			}
//			if(entry.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
//				if(getWaitingFor().equals(DebugConstants.NODE)){
//					setWaitingFor(DebugConstants.PATTERMATCHING);
//				}
//			}
//			if(hasToStepOverPatternMatching){
//				if(entry.equals(DebugConstants.PATTERMATCHING)){
//					isTransformationRunning = false;
//				}
//			}
//			if(entry.equals(DebugConstants.PATTERMATCHING) && frontTransformationNode != null){
//				Node copynode = frontTransformationNode.node;
//				EObject copyeObject= frontTransformationNode.eObject; 
//				RuleBindingWrapper wrapper = frontTransformationNode.rulebinding;
//				Object copyclassInstance = classInstance; 
//				boolean copyisRunning = frontTransformationNode.isRunning;
//				boolean copyisBreakpointFound = isNodeABreakpoint(copynode);
//				frontTransformationNode = null;
//				addMatchedNodeForPatternMatching(copynode, copyeObject, wrapper, false, copyclassInstance, copyisRunning, copyisBreakpointFound);
//			}
//			TGGStackFrame deleteStack = deleteStackFrame(entry, object);
//			stackList.remove(deleteStack);
//			changeDebugEvent(deleteStack);
//		}
//	}
	
	protected AbstractTGGStackFrame deleteStackFrame(String entry, Object object){
			for(int i = 0 ;i < stackList.size() ;i++) {
				try {
					if(stackList.get(i).getName().equals(entry)){
						return stackList.get(i);
					}
				} catch (DebugException e) {
				}
			}
		return null;
	}

	public boolean isThreadIsRunning() {
		return threadIsRunning;
	}

	public void setThreadIsRunning(boolean threadIsRunning) {
		this.threadIsRunning = threadIsRunning;
	}

	public String getWaitingFor() {
		return waitingFor;
	}

	public void setWaitingFor(String waitingFor) {
		this.waitingFor = waitingFor;
	}
	
//	public void addAppliedRuleBinding(RuleBindingWrapper ruleBinding){
//		appliedRulebindings.add(ruleBinding);
//	}

	public void setStepping(boolean isStepping) {
		this.isStepping = isStepping;
	}

	public void decrementIndexOfNodes() {
		this.indexOfNodes = indexOfNodes -1;
	}

}
