	package de.upb.swt.qvt.tgg.debug.manager;

import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.debug.manager.model.IRuleMatchingState;
import de.upb.swt.qvt.tgg.debug.manager.model.ITransformationConfiguration;

public interface ITGGDebugEventListener {
	
	public void changeFigure(Object editPart, boolean isCurrentDebugNode, int indexOfNodeMatching);
	
	public void clearDiagram(Object editPart);
	
	public void clearDiagram();
//	public void setDebugManager(ITGGExecutor debugManager);
	
	//public void isApplyRuleRunning(TripleGraphGrammarRule tripleGraphGrammarRule, boolean isRunning, Object classInstance, boolean isBreakpointFound);
	
//	public void matchedNode(Node node, EObject eObject, /*RuleBinding ruleBinding, */boolean isFrontTransformationNode, boolean isRunning, Object classInstance, boolean isBreakpointFound);
//	
//	public void candidateNode(Node node, EObject eObject, Object classInstance);
//	
//	public void enforcedNode(Node node, EObject eObject, /*RuleBinding ruleBinding,*/ Object classInstance, boolean isRunning, boolean isBreakpointFound);
//	
//	public void currentRuleBinding(/*RuleBinding ruleBinding*/);
//	
//	public void matchingCandidateObjectToNode(Node node, EObject object,
//			boolean successful);
//
//	public void finishedMatchingRule(IRuleMatchingState ruleBinding, boolean successful);
//
//	public void startMatchingRule(IRuleMatchingState ruleBinding);
//
//	public void startTransformation(ITransformationConfiguration configuration);
//
//	public void finishedTransformation(ITransformationConfiguration configuration,
//			boolean successful);
	
	//public IDebugTarget getDebugTarget(ILaunch launch, IProcess process);
	
	//public void isTransformationRunning(Configuration configuration,boolean isRunning);
	
	//public void isMatchingRuleRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance);
	
	//public void isPatternMatchingRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance);

}
