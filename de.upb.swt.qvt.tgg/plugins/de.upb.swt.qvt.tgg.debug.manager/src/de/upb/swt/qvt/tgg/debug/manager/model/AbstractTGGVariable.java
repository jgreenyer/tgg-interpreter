package de.upb.swt.qvt.tgg.debug.manager.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

public abstract class AbstractTGGVariable extends TGGDebugElement implements IVariable {
	
	public AbstractTGGVariable(AbstractTGGThread thread){
		super(thread.getDebugTarget());
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return null;
	}


	@Override
	public boolean hasValueChanged() throws DebugException {
		return false;
	}

	@Override
	public String getModelIdentifier() {
		return null;
	}

	@Override
	public void setValue(String expression) throws DebugException {
	}

	@Override
	public void setValue(IValue value) throws DebugException {
	}

	@Override
	public boolean supportsValueModification() {
		return false;
	}

	@Override
	public boolean verifyValue(String expression) throws DebugException {
		return false;
	}

	@Override
	public boolean verifyValue(IValue value) throws DebugException {
		return false;
	}
}
