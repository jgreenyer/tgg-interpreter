package de.upb.swt.qvt.tgg.debug.manager;


//import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
//import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

public interface ITGGExecutor {
	public boolean isTransformationRunning();
	public void isMatchingRuleRunning(/*RuleBinding ruleBinding,*/ boolean isRunning, Object classInstance);
	public void isPatternMatchingRunning(/*RuleBinding ruleBinding,*/ boolean isRunning, Object classInstance);
}
