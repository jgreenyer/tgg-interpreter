/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Transformation Processor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#performTransformation(org.eclipse.core.runtime.IProgressMonitor) <em>Perform Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#ruleApplicationPostProcessing(de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding) <em>Rule Application Post Processing</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#rulesApplicable() <em>Rules Applicable</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#createInitialRuleBinding() <em>Create Initial Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#transformationSuccessful() <em>Transformation Successful</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class TransformationProcessorTest extends TestCase {

	/**
	 * The fixture for this Transformation Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationProcessor fixture = null;

	/**
	 * Constructs a new Transformation Processor test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Transformation Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TransformationProcessor fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Transformation Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationProcessor getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#performTransformation(org.eclipse.core.runtime.IProgressMonitor) <em>Perform Transformation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#performTransformation(org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	public void testPerformTransformation__IProgressMonitor() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#ruleApplicationPostProcessing(de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding) <em>Rule Application Post Processing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#ruleApplicationPostProcessing(de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding)
	 * @generated
	 */
	public void testRuleApplicationPostProcessing__RuleBinding() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#rulesApplicable() <em>Rules Applicable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#rulesApplicable()
	 * @generated
	 */
	public void testRulesApplicable() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#createInitialRuleBinding() <em>Create Initial Rule Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#createInitialRuleBinding()
	 * @generated
	 */
	public void testCreateInitialRuleBinding() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#transformationSuccessful() <em>Transformation Successful</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#transformationSuccessful()
	 * @generated
	 */
	public void testTransformationSuccessful() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //TransformationProcessorTest
