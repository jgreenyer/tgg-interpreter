/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Default Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DefaultEdgeProcessingPolicyTest extends BaseEdgeProcessingPolicyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DefaultEdgeProcessingPolicyTest.class);
	}

	/**
	 * Constructs a new Default Edge Processing Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultEdgeProcessingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Default Edge Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DefaultEdgeProcessingPolicy getFixture() {
		return (DefaultEdgeProcessingPolicy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(InterpreterFactory.eINSTANCE.createDefaultEdgeProcessingPolicy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DefaultEdgeProcessingPolicyTest
