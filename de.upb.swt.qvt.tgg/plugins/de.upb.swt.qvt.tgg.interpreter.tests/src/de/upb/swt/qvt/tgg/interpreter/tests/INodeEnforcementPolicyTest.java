/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>INode Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy#enforceNode(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Node</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class INodeEnforcementPolicyTest extends TestCase {

	/**
	 * The fixture for this INode Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INodeEnforcementPolicy fixture = null;

	/**
	 * Constructs a new INode Enforcement Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INodeEnforcementPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this INode Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(INodeEnforcementPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this INode Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INodeEnforcementPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy#enforceNode(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy#enforceNode(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEnforceNode__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //INodeEnforcementPolicyTest
