/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>interpreter</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new InterpreterTests("interpreter Tests");
		suite.addTestSuite(InterpreterTest.class);
		suite.addTestSuite(FrontManagerTest.class);
		suite.addTestSuite(RuleProcessorTest.class);
		suite.addTestSuite(FrontTransformationProcessorTest.class);
		suite.addTestSuite(GraphMatcherTest.class);
		suite.addTestSuite(DefaultNodeProcessingPolicyTest.class);
		suite.addTestSuite(DefaultEdgeProcessingPolicyTest.class);
		suite.addTestSuite(DefaultOCLConstraintProcessingPolicyTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterTests(String name) {
		super(name);
	}

} //InterpreterTests
