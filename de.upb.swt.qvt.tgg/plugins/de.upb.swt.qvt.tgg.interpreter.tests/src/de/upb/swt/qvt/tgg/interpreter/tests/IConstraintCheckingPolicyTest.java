/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IConstraint Checking Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IConstraintCheckingPolicyTest extends TestCase {

	/**
	 * The fixture for this IConstraint Checking Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConstraintCheckingPolicy fixture = null;

	/**
	 * Constructs a new IConstraint Checking Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConstraintCheckingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IConstraint Checking Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IConstraintCheckingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IConstraint Checking Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConstraintCheckingPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEvaluateConstraints__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft()
	 * @generated
	 */
	public void testHasUnprocessedConstraintsLeft() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose()
	 * @generated
	 */
	public void testDispose() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IConstraintCheckingPolicyTest
