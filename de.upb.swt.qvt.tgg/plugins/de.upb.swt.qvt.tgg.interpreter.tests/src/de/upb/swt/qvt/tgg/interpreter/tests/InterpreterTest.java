/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interpreter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#initializeConfiguration() <em>Initialize Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#storeConfiguration() <em>Store Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#performTransformation(org.eclipse.core.runtime.IProgressMonitor) <em>Perform Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#storeResult() <em>Store Result</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class InterpreterTest extends TestCase {

	/**
	 * The fixture for this Interpreter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Interpreter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InterpreterTest.class);
	}

	/**
	 * Constructs a new Interpreter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interpreter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Interpreter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interpreter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Interpreter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(InterpreterFactory.eINSTANCE.createInterpreter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#initializeConfiguration() <em>Initialize Configuration</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#initializeConfiguration()
	 * @generated
	 */
	public void testInitializeConfiguration() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#storeConfiguration() <em>Store Configuration</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#storeConfiguration()
	 * @generated
	 */
	public void testStoreConfiguration() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#performTransformation(org.eclipse.core.runtime.IProgressMonitor) <em>Perform Transformation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#performTransformation(org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	public void testPerformTransformation__IProgressMonitor() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#storeResult() <em>Store Result</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#storeResult()
	 * @generated
	 */
	public void testStoreResult() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //InterpreterTest
