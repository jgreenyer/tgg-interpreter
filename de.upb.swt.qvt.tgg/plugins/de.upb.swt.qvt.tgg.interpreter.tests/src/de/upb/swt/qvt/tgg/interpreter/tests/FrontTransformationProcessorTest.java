/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Front Transformation Processor</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FrontTransformationProcessorTest extends TransformationProcessorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FrontTransformationProcessorTest.class);
	}

	/**
	 * Constructs a new Front Transformation Processor test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrontTransformationProcessorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Front Transformation Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FrontTransformationProcessor getFixture() {
		return (FrontTransformationProcessor)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(InterpreterFactory.eINSTANCE.createFrontTransformationProcessor());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FrontTransformationProcessorTest
