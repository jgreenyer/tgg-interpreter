/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;

import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Node To Constraint Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class NodeToConstraintMapEntryTest extends TestCase {

	/**
	 * The fixture for this Node To Constraint Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<Node, EList<Constraint>> fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NodeToConstraintMapEntryTest.class);
	}

	/**
	 * Constructs a new Node To Constraint Map Entry test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeToConstraintMapEntryTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Node To Constraint Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Map.Entry<Node, EList<Constraint>> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Node To Constraint Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<Node, EList<Constraint>> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void setUp() throws Exception {
		setFixture((Map.Entry<Node, EList<Constraint>>)InterpreterFactory.eINSTANCE.create(InterpreterPackage.Literals.NODE_TO_CONSTRAINT_MAP_ENTRY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //NodeToConstraintMapEntryTest
