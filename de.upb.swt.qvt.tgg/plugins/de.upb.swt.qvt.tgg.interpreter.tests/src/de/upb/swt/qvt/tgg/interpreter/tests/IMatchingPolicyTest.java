/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IMatching Policy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IMatchingPolicyTest extends TestCase {

	/**
	 * The fixture for this IMatching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IMatchingPolicy fixture = null;

	/**
	 * Constructs a new IMatching Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IMatchingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IMatching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IMatchingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IMatching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IMatchingPolicy getFixture() {
		return fixture;
	}

} //IMatchingPolicyTest
