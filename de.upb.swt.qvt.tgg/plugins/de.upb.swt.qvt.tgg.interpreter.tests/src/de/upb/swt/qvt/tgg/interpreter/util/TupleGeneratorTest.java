/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.interpreter.util.TupleGenerator;

import junit.framework.TestCase;

import junit.textui.TestRunner;

public class TupleGeneratorTest extends TestCase {

	protected TupleGenerator fixture = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TupleGeneratorTest.class);
	}

	/**
	 * Constructs a new Tuple Generator test case with the given name.
	 */
	public TupleGeneratorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Tuple Generator test case.
	 */
	protected void setFixture(TupleGenerator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Tuple Generator test case.
	 */
	protected TupleGenerator getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated NOT
	 */
	@Override
	protected void setUp() throws Exception {

		Node node11 = TggFactory.eINSTANCE.createNode();
		node11.setName("1");
		Node node12 = TggFactory.eINSTANCE.createNode();
		node12.setName("2");
		Node node13 = TggFactory.eINSTANCE.createNode();
		node13.setName("3");
		Object[][] inputArrays = { { node11, node12, node13 }, { node11, node12, node13 }, { node11, node12, node13 } };

		setFixture(new TupleGenerator(inputArrays));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	private Object[] tuple;

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getNextTupel()
	 * <em>Get Next Tupel</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getNextTupel()
	 * @generated NOT
	 */
	public void testGetNextTupel() {
		tuple = getFixture().getNextTuple();
		int tupelValue = 0;
		while (tuple.length != 0) {
			int newTupelValue = parseTupelValue(tuple);
			assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}
		assertEquals(tupelValue, 333);
	}

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#replaceInputList(int, org.eclipse.emf.common.util.EList)
	 * <em>Replace Input List</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#replaceInputList(int,
	 *      org.eclipse.emf.common.util.EList)
	 * @generated NOT
	 */
	public void testReplaceInputList__int_EList() {
		tuple = getFixture().getNextTuple();
		int tupelValue = 0;
		while (tuple.length != 0 && tupelValue != 222) {
			int newTupelValue = parseTupelValue(tuple);
			System.out.println(newTupelValue);
			assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}

		Node node1 = TggFactory.eINSTANCE.createNode();
		node1.setName("1");
		Node node2 = TggFactory.eINSTANCE.createNode();
		node2.setName("2");
		Node node3 = TggFactory.eINSTANCE.createNode();
		node3.setName("3");
		Node node4 = TggFactory.eINSTANCE.createNode();
		node4.setName("4");

		EObject[] replaceArray = { node1, node2, node3, node4 };

		System.out.println("Replacing...");

		getFixture().replaceInputArray(1, replaceArray);

		tuple = getFixture().getNextTuple();

		assertEquals(211, parseTupelValue(tuple));

		System.out.println();

		tupelValue = 211;

		while (tuple.length != 0) {
			int newTupelValue = parseTupelValue(tuple);
			System.out.println(newTupelValue);
			assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}
		assertEquals(tupelValue, 343);
	}

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#hasNext()
	 * <em>Has Next</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#hasNext()
	 * @generated NOT
	 */
	public void testHasNext() {
		int tupelValue = 0;
		while (getFixture().hasNext()) {
			tuple = getFixture().getNextTuple();
			assertTrue(tuple.length != 0);
			int newTupelValue = parseTupelValue(tuple);
			assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
		}
		assertEquals(333, tupelValue);
	}

	private int parseTupelValue(Object[] tuple) {

		String tupelString = "";

		for (Object obj : tuple) {
			assert (obj != null);

			tupelString = tupelString.concat(((Node) obj).getName());
		}

		return Integer.parseInt(tupelString);

	}

} // TupelGeneratorTest
