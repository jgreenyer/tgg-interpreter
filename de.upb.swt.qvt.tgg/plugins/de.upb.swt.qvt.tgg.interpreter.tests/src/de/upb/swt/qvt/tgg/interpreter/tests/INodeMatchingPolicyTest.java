/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>INode Matching Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#nodeMatches(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Node Matches</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getUnboundEdgeToMatchNext(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Get Unbound Edge To Match Next</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getCandidateNeighborObjectsForEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge) <em>Get Candidate Neighbor Objects For Edge</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class INodeMatchingPolicyTest extends TestCase {

	/**
	 * The fixture for this INode Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INodeMatchingPolicy fixture = null;

	/**
	 * Constructs a new INode Matching Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INodeMatchingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this INode Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(INodeMatchingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this INode Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INodeMatchingPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#nodeMatches(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Node Matches</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#nodeMatches(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testNodeMatches__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getUnboundEdgeToMatchNext(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Get Unbound Edge To Match Next</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getUnboundEdgeToMatchNext(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testGetUnboundEdgeToMatchNext__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getCandidateNeighborObjectsForEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge) <em>Get Candidate Neighbor Objects For Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getCandidateNeighborObjectsForEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge)
	 * @generated
	 */
	public void testGetCandidateNeighborObjectsForEdge__Node_EObject_Edge() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //INodeMatchingPolicyTest
