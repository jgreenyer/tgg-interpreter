/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IConstraint Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IConstraintEnforcementPolicyTest extends TestCase {

	/**
	 * The fixture for this IConstraint Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConstraintEnforcementPolicy fixture = null;

	/**
	 * Constructs a new IConstraint Enforcement Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConstraintEnforcementPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IConstraint Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IConstraintEnforcementPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IConstraint Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConstraintEnforcementPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Constraints</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEnforceConstraints__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEvaluateConstraints__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft()
	 * @generated
	 */
	public void testHasUnprocessedConstraintsLeft() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose()
	 * @generated
	 */
	public void testDispose() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IConstraintEnforcementPolicyTest
