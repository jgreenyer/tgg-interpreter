/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Default Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DefaultNodeProcessingPolicyTest extends BaseNodeProcessingPolicyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DefaultNodeProcessingPolicyTest.class);
	}

	/**
	 * Constructs a new Default Node Processing Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultNodeProcessingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Default Node Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DefaultNodeProcessingPolicy getFixture() {
		return (DefaultNodeProcessingPolicy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(InterpreterFactory.eINSTANCE.createDefaultNodeProcessingPolicy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DefaultNodeProcessingPolicyTest
