/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Base Constraint Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#checkConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint) <em>Check Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#enforceConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint) <em>Enforce Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingRemovedFromRuleBinding(de.upb.swt.qvt.tgg.Node) <em>Node Binding Removed From Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingAddedToRuleBinding(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Node Binding Added To Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class BaseConstraintProcessingPolicyTest extends TestCase {

	/**
	 * The fixture for this Base Constraint Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseConstraintProcessingPolicy fixture = null;

	/**
	 * Constructs a new Base Constraint Processing Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseConstraintProcessingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Base Constraint Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(BaseConstraintProcessingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Base Constraint Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseConstraintProcessingPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#checkConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint) <em>Check Constraint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#checkConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint)
	 * @generated
	 */
	public void testCheckConstraint__Node_EObject_Constraint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#enforceConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint) <em>Enforce Constraint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#enforceConstraint(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Constraint)
	 * @generated
	 */
	public void testEnforceConstraint__Node_EObject_Constraint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingRemovedFromRuleBinding(de.upb.swt.qvt.tgg.Node) <em>Node Binding Removed From Rule Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingRemovedFromRuleBinding(de.upb.swt.qvt.tgg.Node)
	 * @generated
	 */
	public void testNodeBindingRemovedFromRuleBinding__Node() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingAddedToRuleBinding(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Node Binding Added To Rule Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy#nodeBindingAddedToRuleBinding(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testNodeBindingAddedToRuleBinding__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Enforce Constraints</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy#enforceConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEnforceConstraints__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject) <em>Evaluate Constraints</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#evaluateConstraints(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public void testEvaluateConstraints__Node_EObject() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft() <em>Has Unprocessed Constraints Left</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#hasUnprocessedConstraintsLeft()
	 * @generated
	 */
	public void testHasUnprocessedConstraintsLeft() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose() <em>Dispose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#dispose()
	 * @generated
	 */
	public void testDispose() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //BaseConstraintProcessingPolicyTest
