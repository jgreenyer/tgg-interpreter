/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IEdge Matching Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Edge Matches</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IEdgeMatchingPolicyTest extends TestCase {

	/**
	 * The fixture for this IEdge Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IEdgeMatchingPolicy fixture = null;

	/**
	 * Constructs a new IEdge Matching Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IEdgeMatchingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IEdge Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IEdgeMatchingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IEdge Matching Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IEdgeMatchingPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Edge Matches</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	public void testEdgeMatches__Edge_EList() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IEdgeMatchingPolicyTest
