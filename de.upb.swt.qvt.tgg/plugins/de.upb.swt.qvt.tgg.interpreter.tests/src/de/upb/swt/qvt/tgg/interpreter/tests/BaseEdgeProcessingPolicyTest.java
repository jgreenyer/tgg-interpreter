/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Base Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Edge Matches</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Enforce Edge</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class BaseEdgeProcessingPolicyTest extends TestCase {

	/**
	 * The fixture for this Base Edge Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseEdgeProcessingPolicy fixture = null;

	/**
	 * Constructs a new Base Edge Processing Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseEdgeProcessingPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Base Edge Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(BaseEdgeProcessingPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Base Edge Processing Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseEdgeProcessingPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Edge Matches</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#edgeMatches(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	public void testEdgeMatches__Edge_EList() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Enforce Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	public void testEnforceEdge__Edge_EList() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //BaseEdgeProcessingPolicyTest
