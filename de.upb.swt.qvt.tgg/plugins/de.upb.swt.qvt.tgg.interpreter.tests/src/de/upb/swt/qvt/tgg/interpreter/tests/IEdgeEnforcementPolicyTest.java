/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IEdge Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Enforce Edge</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IEdgeEnforcementPolicyTest extends TestCase {

	/**
	 * The fixture for this IEdge Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IEdgeEnforcementPolicy fixture = null;

	/**
	 * Constructs a new IEdge Enforcement Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IEdgeEnforcementPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IEdge Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IEdgeEnforcementPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IEdge Enforcement Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IEdgeEnforcementPolicy getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList) <em>Enforce Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy#enforceEdge(de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	public void testEnforceEdge__Edge_EList() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IEdgeEnforcementPolicyTest
