/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.tests;

import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Graph Matcher</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdges(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, int) <em>Explore Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.ecore.EObject, int) <em>Explore Edge</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#match() <em>Match</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class GraphMatcherTest extends TestCase {

	/**
	 * The fixture for this Graph Matcher test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphMatcher fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(GraphMatcherTest.class);
	}

	/**
	 * Constructs a new Graph Matcher test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcherTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Graph Matcher test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GraphMatcher fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Graph Matcher test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphMatcher getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(InterpreterFactory.eINSTANCE.createGraphMatcher());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdges(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, int) <em>Explore Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdges(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, int)
	 * @generated
	 */
	public void testExploreEdges__Node_EObject_int() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.ecore.EObject, int) <em>Explore Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#exploreEdge(de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.ecore.EObject, int)
	 * @generated
	 */
	public void testExploreEdge__Node_EObject_Edge_EObject_int() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#match() <em>Match</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#match()
	 * @generated
	 */
	public void testMatch() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //GraphMatcherTest
