package com.tools.logging;

import org.apache.log4j.spi.LoggingEvent;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class TGGDebugLoggerAppender extends PluginLogAppender {

	MessageConsole myConsole;
	MessageConsoleStream out;
	
	public TGGDebugLoggerAppender(){
		super();
		myConsole = createConsole();
		out = myConsole.newMessageStream();
	}
	
	@Override
	public void append(LoggingEvent event) {		
		out.println(event.getMessage().toString());
	}

	private MessageConsole createConsole() {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		// search for an existing console
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if ("loggerConsole".equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole("loggerConsole", null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}
}
