/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TGG Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGAxiom()
 * @model
 * @generated
 */
public interface TGGAxiom extends TripleGraphGrammarRule {
} // TGGAxiom
