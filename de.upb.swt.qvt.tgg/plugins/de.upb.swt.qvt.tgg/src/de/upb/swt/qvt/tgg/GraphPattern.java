/**
 * <copyright>
 * </copyright>
 *
 * $Id: GraphPattern.java 1600 2006-06-13 10:33:21 +0000 (Di, 13 Jun 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphPattern#getNode <em>Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphPattern#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphPattern#getEdge <em>Edge</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphPattern()
 * @model
 * @generated
 */
public interface GraphPattern extends EObject {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Node#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphPattern_Node()
	 * @see de.upb.swt.qvt.tgg.Node#getGraphPattern
	 * @model opposite="graphPattern"
	 * @generated
	 */
	EList<Node> getNode();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Constraint}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Constraint#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphPattern_Constraint()
	 * @see de.upb.swt.qvt.tgg.Constraint#getGraphPattern
	 * @model opposite="graphPattern"
	 * @generated
	 */
	EList<Constraint> getConstraint();

	/**
	 * Returns the value of the '<em><b>Edge</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphPattern_Edge()
	 * @see de.upb.swt.qvt.tgg.Edge#getGraphPattern
	 * @model opposite="graphPattern"
	 * @generated
	 */
	EList<Edge> getEdge();

} // GraphPattern