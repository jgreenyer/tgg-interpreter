/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TGG Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl#getSlotNode <em>Slot Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl#getSlotAttribute <em>Slot Attribute</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl#getSlotAttributeName <em>Slot Attribute Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl#isCheckonly <em>Checkonly</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl#isEvaluateAfterMatching <em>Evaluate After Matching</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class TGGConstraintImpl extends ConstraintImpl implements TGGConstraint {
	/**
	 * The cached value of the '{@link #getSlotNode() <em>Slot Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlotNode()
	 * @generated
	 * @ordered
	 */
	protected Node slotNode;

	/**
	 * The cached value of the '{@link #getSlotAttribute() <em>Slot Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlotAttribute()
	 * @generated
	 * @ordered
	 */
	protected EAttribute slotAttribute;

	/**
	 * The default value of the '{@link #getSlotAttributeName() <em>Slot Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlotAttributeName()
	 * @generated
	 * @ordered
	 */
	protected static final String SLOT_ATTRIBUTE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isCheckonly() <em>Checkonly</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckonly()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CHECKONLY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCheckonly() <em>Checkonly</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckonly()
	 * @generated
	 * @ordered
	 */
	protected boolean checkonly = CHECKONLY_EDEFAULT;

	/**
	 * The default value of the '{@link #isEvaluateAfterMatching() <em>Evaluate After Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEvaluateAfterMatching()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EVALUATE_AFTER_MATCHING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEvaluateAfterMatching() <em>Evaluate After Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEvaluateAfterMatching()
	 * @generated
	 * @ordered
	 */
	protected boolean evaluateAfterMatching = EVALUATE_AFTER_MATCHING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TGG_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getSlotNode() {
		if (slotNode != null && slotNode.eIsProxy()) {
			InternalEObject oldSlotNode = (InternalEObject)slotNode;
			slotNode = (Node)eResolveProxy(oldSlotNode);
			if (slotNode != oldSlotNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.TGG_CONSTRAINT__SLOT_NODE, oldSlotNode, slotNode));
			}
		}
		return slotNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetSlotNode() {
		return slotNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlotNode(Node newSlotNode) {
		Node oldSlotNode = slotNode;
		slotNode = newSlotNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TGG_CONSTRAINT__SLOT_NODE, oldSlotNode, slotNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSlotAttribute() {
		if (slotAttribute != null && slotAttribute.eIsProxy()) {
			InternalEObject oldSlotAttribute = (InternalEObject)slotAttribute;
			slotAttribute = (EAttribute)eResolveProxy(oldSlotAttribute);
			if (slotAttribute != oldSlotAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE, oldSlotAttribute, slotAttribute));
			}
		}
		return slotAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute basicGetSlotAttribute() {
		return slotAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setSlotAttribute(EAttribute newSlotAttribute) {
		String oldSlotAttributeName = getSlotAttributeName();
		EAttribute oldSlotAttribute = slotAttribute;
		slotAttribute = newSlotAttribute;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE,
					oldSlotAttribute, slotAttribute));
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME,
					oldSlotAttributeName, getSlotAttributeName()));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getSlotAttributeName() {
		if (getSlotAttribute() == null) {
			return "[unset]";
		} else {
			return getSlotAttribute().getName();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCheckonly() {
		return checkonly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckonly(boolean newCheckonly) {
		boolean oldCheckonly = checkonly;
		checkonly = newCheckonly;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TGG_CONSTRAINT__CHECKONLY, oldCheckonly, checkonly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEvaluateAfterMatching() {
		return evaluateAfterMatching;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluateAfterMatching(boolean newEvaluateAfterMatching) {
		boolean oldEvaluateAfterMatching = evaluateAfterMatching;
		evaluateAfterMatching = newEvaluateAfterMatching;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING, oldEvaluateAfterMatching, evaluateAfterMatching));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.TGG_CONSTRAINT__SLOT_NODE:
				if (resolve) return getSlotNode();
				return basicGetSlotNode();
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE:
				if (resolve) return getSlotAttribute();
				return basicGetSlotAttribute();
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME:
				return getSlotAttributeName();
			case TggPackage.TGG_CONSTRAINT__CHECKONLY:
				return isCheckonly();
			case TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING:
				return isEvaluateAfterMatching();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.TGG_CONSTRAINT__SLOT_NODE:
				setSlotNode((Node)newValue);
				return;
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE:
				setSlotAttribute((EAttribute)newValue);
				return;
			case TggPackage.TGG_CONSTRAINT__CHECKONLY:
				setCheckonly((Boolean)newValue);
				return;
			case TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING:
				setEvaluateAfterMatching((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.TGG_CONSTRAINT__SLOT_NODE:
				setSlotNode((Node)null);
				return;
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE:
				setSlotAttribute((EAttribute)null);
				return;
			case TggPackage.TGG_CONSTRAINT__CHECKONLY:
				setCheckonly(CHECKONLY_EDEFAULT);
				return;
			case TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING:
				setEvaluateAfterMatching(EVALUATE_AFTER_MATCHING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.TGG_CONSTRAINT__SLOT_NODE:
				return slotNode != null;
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE:
				return slotAttribute != null;
			case TggPackage.TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME:
				return SLOT_ATTRIBUTE_NAME_EDEFAULT == null ? getSlotAttributeName() != null : !SLOT_ATTRIBUTE_NAME_EDEFAULT.equals(getSlotAttributeName());
			case TggPackage.TGG_CONSTRAINT__CHECKONLY:
				return checkonly != CHECKONLY_EDEFAULT;
			case TggPackage.TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING:
				return evaluateAfterMatching != EVALUATE_AFTER_MATCHING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checkonly: ");
		result.append(checkonly);
		result.append(", evaluateAfterMatching: ");
		result.append(evaluateAfterMatching);
		result.append(')');
		return result.toString();
	}

} //TGGConstraintImpl
