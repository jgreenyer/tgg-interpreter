/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.ecore.EClass;

import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triple Graph Grammar Axiom</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TripleGraphGrammarAxiomImpl extends TripleGraphGrammarRuleImpl implements TripleGraphGrammarAxiom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TripleGraphGrammarAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TRIPLE_GRAPH_GRAMMAR_AXIOM;
	}

} //TripleGraphGrammarAxiomImpl
