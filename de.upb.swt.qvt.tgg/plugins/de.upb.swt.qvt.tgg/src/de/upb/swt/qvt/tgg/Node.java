/**
 * <copyright>
 * </copyright>
 *
 * $Id: Node.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getOutgoingEdge <em>Outgoing Edge</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getIncomingEdge <em>Incoming Edge</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getGraph <em>Graph</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getGraphPattern <em>Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getTypedName <em>Typed Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#isMatchSubtype <em>Match Subtype</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getEType <em>EType</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getRefinedNode <em>Refined Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getAllOutgoingEdges <em>All Outgoing Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getAllIncomingEdges <em>All Incoming Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getAllRefinedNodes <em>All Refined Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getAllGraphPatterns <em>All Graph Patterns</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Node#getMatchingPriority <em>Matching Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getNode()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='RefiningNodeOnSameGraphGrammarSideAsRefinedNode\r\nRefiningNodeInSameDomainAsRefiningNode\r\nRefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass\r\nNodeMustHaveDomainGraphPattern\r\nRefiningNodeMustHaveTheSameNameAsTheRefinedNode\r\nAtMostOneNodeRefiningTheSameOtherNode\r\nNoAbstractTypeForProducedNodes'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL NodeMustHaveDomainGraphPattern='self.getDomainGraphPattern() <> null' RefiningNodeOnSameGraphGrammarSideAsRefinedNode='self.refinedNode = null or (self.left = self.refinedNode.left and self.right = self.refinedNode.right)' RefiningNodeInSameDomainAsRefiningNode='self.refinedNode = null or (self.getDomainGraphPattern().typedModel = self.refinedNode.getDomainGraphPattern().typedModel)' RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass='self.refinedNode = null or (self.refinedNode.eType = self.eType or self.refinedNode.eType.isSuperTypeOf(self.eType))' RefiningNodeMustHaveTheSameNameAsTheRefinedNode='self.refinedNode = null or (self.name = self.refinedNode.name)' AtMostOneNodeRefiningTheSameOtherNode='self.refinedNode = null or self.graph.node->select(n| n <> self and  n.refinedNode <> null and n.refinedNode = self.refinedNode)->isEmpty()' NoAbstractTypeForProducedNodes='(not self.left and self.right) implies not self.eType.abstract'"
 * @generated
 */
public interface Node extends GraphElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Edge</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getSourceNode <em>Source Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Edge</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Edge</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_OutgoingEdge()
	 * @see de.upb.swt.qvt.tgg.Edge#getSourceNode
	 * @model opposite="sourceNode" containment="true"
	 * @generated
	 */
	EList<Edge> getOutgoingEdge();

	/**
	 * Returns the value of the '<em><b>Incoming Edge</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getTargetNode <em>Target Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Edge</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Edge</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_IncomingEdge()
	 * @see de.upb.swt.qvt.tgg.Edge#getTargetNode
	 * @model opposite="targetNode"
	 * @generated
	 */
	EList<Edge> getIncomingEdge();

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Graph#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(Graph)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_Graph()
	 * @see de.upb.swt.qvt.tgg.Graph#getNode
	 * @model opposite="node" transient="false"
	 * @generated
	 */
	Graph getGraph();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Node#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(Graph value);

	/**
	 * Returns the value of the '<em><b>Graph Pattern</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.GraphPattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.GraphPattern#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Pattern</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Pattern</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_GraphPattern()
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getNode
	 * @model opposite="node"
	 * @generated
	 */
	EList<GraphPattern> getGraphPattern();

	/**
	 * Returns the value of the '<em><b>Match Subtype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Match Subtype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Match Subtype</em>' attribute.
	 * @see #setMatchSubtype(boolean)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_MatchSubtype()
	 * @model
	 * @generated
	 */
	boolean isMatchSubtype();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Node#isMatchSubtype <em>Match Subtype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Match Subtype</em>' attribute.
	 * @see #isMatchSubtype()
	 * @generated
	 */
	void setMatchSubtype(boolean value);

	/**
	 * Returns the value of the '<em><b>EType</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EType</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EType</em>' reference.
	 * @see #setEType(EClass)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_EType()
	 * @model
	 * @generated
	 */
	EClass getEType();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Node#getEType <em>EType</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EType</em>' reference.
	 * @see #getEType()
	 * @generated
	 */
	void setEType(EClass value);

	/**
	 * Returns the value of the '<em><b>Refined Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refined Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A node may refine nodes of (directly or indirectly) refined rules.
	 * The type of the node must be the same or a subtype of the
	 * refined node's type
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Refined Node</em>' reference.
	 * @see #setRefinedNode(Node)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_RefinedNode()
	 * @model
	 * @generated
	 */
	Node getRefinedNode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Node#getRefinedNode <em>Refined Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refined Node</em>' reference.
	 * @see #getRefinedNode()
	 * @generated
	 */
	void setRefinedNode(Node value);

	/**
	 * Returns the value of the '<em><b>All Outgoing Edges</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Outgoing Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains the outgoing edges of this node and all refined nodes
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Outgoing Edges</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_AllOutgoingEdges()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Edge> getAllOutgoingEdges();

	/**
	 * Returns the value of the '<em><b>All Incoming Edges</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Incoming Edges</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains the incoming edges of this node and all refined nodes
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Incoming Edges</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_AllIncomingEdges()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Edge> getAllIncomingEdges();

	/**
	 * Returns the value of the '<em><b>All Refined Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Refined Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Refined Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_AllRefinedNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllRefinedNodes();

	/**
	 * Returns the value of the '<em><b>All Graph Patterns</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.GraphPattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Graph Patterns</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Graph Patterns</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_AllGraphPatterns()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<GraphPattern> getAllGraphPatterns();

	/**
	 * Returns the value of the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * can be used to prioritize nodes that the graph matcher naviates to first
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Matching Priority</em>' attribute.
	 * @see #setMatchingPriority(int)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_MatchingPriority()
	 * @model
	 * @generated
	 */
	int getMatchingPriority();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Node#getMatchingPriority <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matching Priority</em>' attribute.
	 * @see #getMatchingPriority()
	 * @generated
	 */
	void setMatchingPriority(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Graph Pattern</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	DomainGraphPattern getDomainGraphPattern();

	/**
	 * Returns the value of the '<em><b>Typed Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Name</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getNode_TypedName()
	 * @model default="" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getTypedName();

} // Node