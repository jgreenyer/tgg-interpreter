/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triple Graph Grammar Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarAxiom()
 * @model
 * @generated
 */
public interface TripleGraphGrammarAxiom extends TripleGraphGrammarRule {
} // TripleGraphGrammarAxiom
