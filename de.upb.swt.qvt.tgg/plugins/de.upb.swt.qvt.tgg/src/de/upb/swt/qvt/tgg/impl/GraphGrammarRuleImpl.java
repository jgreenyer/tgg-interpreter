/**
 * <copyright>
 * </copyright>
 *
 * $Id: GraphGrammarRuleImpl.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Graph Grammar Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getLeftGraphPattern <em>Left Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getRightGraphPattern <em>Right Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getContextNodes <em>Context Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getProducedNodes <em>Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getNonproducedNodes <em>Nonproduced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getReusableNodes <em>Reusable Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getRefinedRule <em>Refined Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllNodes <em>All Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllContextNodes <em>All Context Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllProducedNodes <em>All Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllNonproducedNodes <em>All Nonproduced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllReusableNodes <em>All Reusable Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getAllRefinedRules <em>All Refined Rules</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl#getReusablePattern <em>Reusable Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GraphGrammarRuleImpl extends GraphImpl implements GraphGrammarRule {
	/**
	 * The cached value of the '{@link #getLeftGraphPattern() <em>Left Graph Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftGraphPattern()
	 * @generated
	 * @ordered
	 */
	protected LeftGraphPattern leftGraphPattern;

	/**
	 * The cached value of the '{@link #getRightGraphPattern() <em>Right Graph Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightGraphPattern()
	 * @generated
	 * @ordered
	 */
	protected RightGraphPattern rightGraphPattern;

	/**
	 * The cached value of the '{@link #getRefinedRule() <em>Refined Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefinedRule()
	 * @generated
	 * @ordered
	 */
	protected GraphGrammarRule refinedRule;

	/**
	 * The cached value of the '{@link #getReusablePattern() <em>Reusable Pattern</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReusablePattern()
	 * @generated
	 * @ordered
	 */
	protected EList<ReusablePattern> reusablePattern;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphGrammarRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.GRAPH_GRAMMAR_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeftGraphPattern getLeftGraphPattern() {
		return leftGraphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftGraphPattern(LeftGraphPattern newLeftGraphPattern, NotificationChain msgs) {
		LeftGraphPattern oldLeftGraphPattern = leftGraphPattern;
		leftGraphPattern = newLeftGraphPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN, oldLeftGraphPattern, newLeftGraphPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftGraphPattern(LeftGraphPattern newLeftGraphPattern) {
		if (newLeftGraphPattern != leftGraphPattern) {
			NotificationChain msgs = null;
			if (leftGraphPattern != null)
				msgs = ((InternalEObject)leftGraphPattern).eInverseRemove(this, TggPackage.LEFT_GRAPH_PATTERN__GRAPH_GRAMMAR, LeftGraphPattern.class, msgs);
			if (newLeftGraphPattern != null)
				msgs = ((InternalEObject)newLeftGraphPattern).eInverseAdd(this, TggPackage.LEFT_GRAPH_PATTERN__GRAPH_GRAMMAR, LeftGraphPattern.class, msgs);
			msgs = basicSetLeftGraphPattern(newLeftGraphPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN, newLeftGraphPattern, newLeftGraphPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RightGraphPattern getRightGraphPattern() {
		return rightGraphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightGraphPattern(RightGraphPattern newRightGraphPattern, NotificationChain msgs) {
		RightGraphPattern oldRightGraphPattern = rightGraphPattern;
		rightGraphPattern = newRightGraphPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN, oldRightGraphPattern, newRightGraphPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightGraphPattern(RightGraphPattern newRightGraphPattern) {
		if (newRightGraphPattern != rightGraphPattern) {
			NotificationChain msgs = null;
			if (rightGraphPattern != null)
				msgs = ((InternalEObject)rightGraphPattern).eInverseRemove(this, TggPackage.RIGHT_GRAPH_PATTERN__GRAPH_GRAMMAR, RightGraphPattern.class, msgs);
			if (newRightGraphPattern != null)
				msgs = ((InternalEObject)newRightGraphPattern).eInverseAdd(this, TggPackage.RIGHT_GRAPH_PATTERN__GRAPH_GRAMMAR, RightGraphPattern.class, msgs);
			msgs = basicSetRightGraphPattern(newRightGraphPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN, newRightGraphPattern, newRightGraphPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getContextNodes() {
		//jgreen: Calculating this list via the EObjectEList makes the GMF-Editor dirty upon opening
		//EList<Node> producedNodes = new EObjectEList<Node>(Node.class, this, TggPackage.GRAPH_GRAMMAR_RULE__PRODUCED_NODES);
		BasicEList<Node> resultList = new UniqueEList<Node>();
		if (getRightGraphPattern() == null) //might happen during creation/deletion
			return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
					.getGraphGrammarRule_ContextNodes(), resultList.size(), resultList.data());
		for (Node node : getRightGraphPattern().getNode()) {
			if (getLeftGraphPattern().getNode().contains(node))
				resultList.add(node);
		}
		return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
				.getGraphGrammarRule_ContextNodes(), resultList.size(), resultList.data());
	}

	/**
	 * <!-- begin-user-doc -->
	 * All such nodes in the right, but not the left graph pattern
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getProducedNodes() {
		//jgreen: Calculating this list via the EObjectEList makes the GMF-Editor dirty upon opening
		//EList<Node> producedNodes = new EObjectEList<Node>(Node.class, this, TggPackage.GRAPH_GRAMMAR_RULE__PRODUCED_NODES);
		BasicEList<Node> resultList = new UniqueEList<Node>();
		if (getRightGraphPattern() == null) //might happen during creation/deletion
			return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
					.getGraphGrammarRule_ProducedNodes(), resultList.size(), resultList.data());
		for (Node node : getRightGraphPattern().getNode()) {
			if (!getLeftGraphPattern().getNode().contains(node))
				resultList.add(node);
		}
		return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
				.getGraphGrammarRule_ProducedNodes(), resultList.size(), resultList.data());
	}

	/**
	 * <!-- begin-user-doc -->
	 * All such nodes which are context or reusable,
	 * the complement of the produced nodes.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getNonproducedNodes() {
		//jgreen: Calculating this list via the EObjectEList makes the GMF-Editor dirty upon opening
		//EList<Node> nonproducedNodes = new EObjectEList<Node>(Node.class, this, TggPackage.GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES);
		BasicEList<Node> resultList = new UniqueEList<Node>();
		for (Node node : getNode()) {
			if (!getProducedNodes().contains(node))
				resultList.add(node);
		}
		return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
				.getGraphGrammarRule_NonproducedNodes(), resultList.size(), resultList.data());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getReusableNodes() {
		//jgreen: Calculating this list via the EObjectEList makes the GMF-Editor dirty upon opening
		//EList<Node> reusableNodes = new EObjectEList<Node>(Node.class, this, TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_NODES);
		BasicEList<Node> resultList = new UniqueEList<Node>();
		for (Node node : getNode()) {
			if (!getLeftGraphPattern().getNode().contains(node) && !!getRightGraphPattern().getNode().contains(node))
				resultList.add(node);
		}
		return new EcoreEList.UnmodifiableEList.FastCompare<Node>(this, TggPackage.eINSTANCE
				.getGraphGrammarRule_ReusableNodes(), resultList.size(), resultList.data());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphGrammarRule getRefinedRule() {
		if (refinedRule != null && refinedRule.eIsProxy()) {
			InternalEObject oldRefinedRule = (InternalEObject)refinedRule;
			refinedRule = (GraphGrammarRule)eResolveProxy(oldRefinedRule);
			if (refinedRule != oldRefinedRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE, oldRefinedRule, refinedRule));
			}
		}
		return refinedRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphGrammarRule basicGetRefinedRule() {
		return refinedRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefinedRule(GraphGrammarRule newRefinedRule) {
		GraphGrammarRule oldRefinedRule = refinedRule;
		refinedRule = newRefinedRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE, oldRefinedRule, refinedRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllNodes() {
		if (getRefinedRule() == null) {
			return getNode();
		} else {
			if (cachedAllNodes == null){
				EList<Node> allNodesList = new UniqueEList<Node>(getRefinedRule().getAllNodes());
				for (Node node : getNode()) {
					if (allNodesList.contains(node.getRefinedNode()))
						allNodesList.remove(node.getRefinedNode());
				}
				allNodesList.addAll(getNode());
				cachedAllNodes = new EcoreEList.UnmodifiableEList<Node>(this, TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_NODES,
						allNodesList.size(), allNodesList.toArray());
			}
			return cachedAllNodes;
		}
	}
	
	private EList<Node> cachedAllNodes;
	private EList<Node> cachedAllContextNodes;
	private EList<Node> cachedAllProducedNodes;
	private EList<Node> cachedAllNonProducedNodes;
	private EList<Node> cachedAllReusableNodes;
	private EList<GraphGrammarRule> cachedAllRefinedRules;
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllContextNodes() {
		if (getRefinedRule() == null) {
			return getContextNodes();
		} else {
			if (cachedAllContextNodes == null){
				EList<Node> allNodesList = new UniqueEList<Node>(getRefinedRule().getAllContextNodes());
				for (Node node : getContextNodes()) {
					if (allNodesList.contains(node.getRefinedNode()))
						allNodesList.remove(node.getRefinedNode());
				}
				allNodesList.addAll(getContextNodes());
				cachedAllContextNodes =  new EcoreEList.UnmodifiableEList<Node>(this,
						TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES, allNodesList.size(), allNodesList
								.toArray());
			}
			return cachedAllContextNodes;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllProducedNodes() {
		if (getRefinedRule() == null) {
			return getProducedNodes();
		} else {
			if (cachedAllProducedNodes == null){
				EList<Node> allNodesList = new UniqueEList<Node>(getRefinedRule().getAllProducedNodes());
				for (Node node : getProducedNodes()) {
					if (allNodesList.contains(node.getRefinedNode()))
						allNodesList.remove(node.getRefinedNode());
				}
				allNodesList.addAll(getProducedNodes());
				cachedAllProducedNodes =  new EcoreEList.UnmodifiableEList<Node>(this,
						TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES, allNodesList.size(), allNodesList
								.toArray());
			}
			return cachedAllProducedNodes;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllNonproducedNodes() {
		if (getRefinedRule() == null) {
			return getNonproducedNodes();
		} else {
			if (cachedAllNonProducedNodes == null){
				EList<Node> allNodesList = new UniqueEList<Node>(getRefinedRule().getAllNonproducedNodes());
				for (Node node : getNonproducedNodes()) {
					if (allNodesList.contains(node.getRefinedNode()))
						allNodesList.remove(node.getRefinedNode());
				}
				allNodesList.addAll(getNonproducedNodes());
				cachedAllNonProducedNodes = new EcoreEList.UnmodifiableEList<Node>(this,
						TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES, allNodesList.size(), allNodesList
								.toArray());
			} 
			return cachedAllNonProducedNodes;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllReusableNodes() {
		if (getRefinedRule() == null) {
			return getReusableNodes();
		} else {
			if (cachedAllReusableNodes == null){
				EList<Node> allNodesList = new UniqueEList<Node>(getRefinedRule().getAllReusableNodes());
				for (Node node : getReusableNodes()) {
					if (allNodesList.contains(node.getRefinedNode()))
						allNodesList.remove(node.getRefinedNode());
				}
				allNodesList.addAll(getReusableNodes());
				cachedAllReusableNodes = new EcoreEList.UnmodifiableEList<Node>(this,
						TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES, allNodesList.size(), allNodesList
								.toArray());
			}
			return cachedAllReusableNodes;
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<GraphGrammarRule> getAllRefinedRules() {
		if (getRefinedRule() == null)
			return new EcoreEList.UnmodifiableEList<GraphGrammarRule>(this,
					TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES, 0, new GraphGrammarRule[0]);
		else {
			if (cachedAllRefinedRules == null){
				List<GraphGrammarRule> allRulesList = new ArrayList<GraphGrammarRule>(getRefinedRule().getAllRefinedRules());
				allRulesList.add(getRefinedRule());
				cachedAllRefinedRules = new EcoreEList.UnmodifiableEList<GraphGrammarRule>(this,
						TggPackage.Literals.GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES, allRulesList.size(), allRulesList
								.toArray());
			}
			return cachedAllRefinedRules;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReusablePattern> getReusablePattern() {
		if (reusablePattern == null) {
			reusablePattern = new EObjectContainmentWithInverseEList<ReusablePattern>(ReusablePattern.class, this, TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN, TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR);
		}
		return reusablePattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Node getMostRefinedNodeOf(Node node) {
		for (Node ruleNode : getAllNodes()) {
			if (ruleNode.getAllRefinedNodes().contains(node))
				return ruleNode;
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				if (leftGraphPattern != null)
					msgs = ((InternalEObject)leftGraphPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN, null, msgs);
				return basicSetLeftGraphPattern((LeftGraphPattern)otherEnd, msgs);
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				if (rightGraphPattern != null)
					msgs = ((InternalEObject)rightGraphPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN, null, msgs);
				return basicSetRightGraphPattern((RightGraphPattern)otherEnd, msgs);
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getReusablePattern()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				return basicSetLeftGraphPattern(null, msgs);
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				return basicSetRightGraphPattern(null, msgs);
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				return ((InternalEList<?>)getReusablePattern()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				return getLeftGraphPattern();
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				return getRightGraphPattern();
			case TggPackage.GRAPH_GRAMMAR_RULE__CONTEXT_NODES:
				return getContextNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__PRODUCED_NODES:
				return getProducedNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES:
				return getNonproducedNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_NODES:
				return getReusableNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE:
				if (resolve) return getRefinedRule();
				return basicGetRefinedRule();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_NODES:
				return getAllNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES:
				return getAllContextNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES:
				return getAllProducedNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES:
				return getAllNonproducedNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES:
				return getAllReusableNodes();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES:
				return getAllRefinedRules();
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				return getReusablePattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				setLeftGraphPattern((LeftGraphPattern)newValue);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				setRightGraphPattern((RightGraphPattern)newValue);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE:
				setRefinedRule((GraphGrammarRule)newValue);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				getReusablePattern().clear();
				getReusablePattern().addAll((Collection<? extends ReusablePattern>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				setLeftGraphPattern((LeftGraphPattern)null);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				setRightGraphPattern((RightGraphPattern)null);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE:
				setRefinedRule((GraphGrammarRule)null);
				return;
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				getReusablePattern().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN:
				return leftGraphPattern != null;
			case TggPackage.GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN:
				return rightGraphPattern != null;
			case TggPackage.GRAPH_GRAMMAR_RULE__CONTEXT_NODES:
				return !getContextNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__PRODUCED_NODES:
				return !getProducedNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES:
				return !getNonproducedNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_NODES:
				return !getReusableNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__REFINED_RULE:
				return refinedRule != null;
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_NODES:
				return !getAllNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES:
				return !getAllContextNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES:
				return !getAllProducedNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES:
				return !getAllNonproducedNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES:
				return !getAllReusableNodes().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES:
				return !getAllRefinedRules().isEmpty();
			case TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN:
				return reusablePattern != null && !reusablePattern.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GraphGrammarRuleImpl