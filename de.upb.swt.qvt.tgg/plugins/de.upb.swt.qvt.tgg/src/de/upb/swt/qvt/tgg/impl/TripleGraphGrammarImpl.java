/**
 * <copyright>
 * </copyright>
 *
 * $Id: TripleGraphGrammarImpl.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEList;

import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.impl.TransformationImpl;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triple Graph Grammar</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl#getRelevantReferences <em>Relevant References</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl#getTypeClassToRuleMap <em>Type Class To Rule Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl#getAxiom <em>Axiom</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TripleGraphGrammarImpl extends TransformationImpl implements TripleGraphGrammar {
	/**
	 * The cached value of the '{@link #getRelevantReferences() <em>Relevant References</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelevantReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<EReference> relevantReferences;

	/**
	 * The cached value of the '{@link #getTypeClassToRuleMap() <em>Type Class To Rule Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeClassToRuleMap()
	 * @generated
	 * @ordered
	 */
	protected Map<EClassifier, EList<Rule>> typeClassToRuleMap;

	/**
	 * The cached value of the '{@link #getAxiom() <em>Axiom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxiom()
	 * @generated
	 * @ordered
	 */
	protected TripleGraphGrammarAxiom axiom;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TripleGraphGrammarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TRIPLE_GRAPH_GRAMMAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EReference> getRelevantReferences() {
		if (relevantReferences == null) {
			BasicEList<EReference> resultList = new UniqueEList<EReference>();
			for (Rule rule : getAllRules()) {
				for (Node node : ((TripleGraphGrammarRule) rule).getNode()) {
					for (Edge outgoingEdge : node.getOutgoingEdge()) {
						resultList.add(outgoingEdge.getTypeReference());
					}
				}
			}
			relevantReferences = new EcoreEList.UnmodifiableEList.FastCompare<EReference>(this, TggPackage.eINSTANCE
					.getTripleGraphGrammar_RelevantReferences(), resultList.size(), resultList.data());
		}
		return relevantReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<EClassifier, EList<Rule>> getTypeClassToRuleMap() {
		return typeClassToRuleMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarAxiom getAxiom() {
		if (axiom != null && axiom.eIsProxy()) {
			InternalEObject oldAxiom = (InternalEObject)axiom;
			axiom = (TripleGraphGrammarAxiom)eResolveProxy(oldAxiom);
			if (axiom != oldAxiom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM, oldAxiom, axiom));
			}
		}
		return axiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarAxiom basicGetAxiom() {
		return axiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAxiom(TripleGraphGrammarAxiom newAxiom) {
		TripleGraphGrammarAxiom oldAxiom = axiom;
		axiom = newAxiom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM, oldAxiom, axiom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__RELEVANT_REFERENCES:
				return getRelevantReferences();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__TYPE_CLASS_TO_RULE_MAP:
				return getTypeClassToRuleMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM:
				if (resolve) return getAxiom();
				return basicGetAxiom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM:
				setAxiom((TripleGraphGrammarAxiom)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM:
				setAxiom((TripleGraphGrammarAxiom)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__RELEVANT_REFERENCES:
				return relevantReferences != null;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__TYPE_CLASS_TO_RULE_MAP:
				return typeClassToRuleMap != null;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR__AXIOM:
				return axiom != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (relevantReferences: ");
		result.append(relevantReferences);
		result.append(", typeClassToRuleMap: ");
		result.append(typeClassToRuleMap);
		result.append(')');
		return result.toString();
	}

} //TripleGraphGrammarImpl