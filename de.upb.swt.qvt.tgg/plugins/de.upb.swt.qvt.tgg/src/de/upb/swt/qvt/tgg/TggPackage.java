/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggPackage.java 1836 2007-03-17 14:22:16 +0000 (Sa, 17 Mrz 2007) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.TggFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface TggPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tgg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.upb.swt.qvt.tgg";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.upb.swt.qvt.tgg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TggPackage eINSTANCE = de.upb.swt.qvt.tgg.impl.TggPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.GraphImpl <em>Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.GraphImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraph()
	 * @generated
	 */
	int GRAPH = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__ANNOTATIONS = QvtbasePackage.NAMED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__ANNOTATION_STRING = QvtbasePackage.NAMED_ELEMENT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__NAME = QvtbasePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__NODE = QvtbasePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_FEATURE_COUNT = QvtbasePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl <em>Graph Grammar Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphGrammarRule()
	 * @generated
	 */
	int GRAPH_GRAMMAR_RULE = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ANNOTATIONS = GRAPH__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ANNOTATION_STRING = GRAPH__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__NAME = GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__NODE = GRAPH__NODE;

	/**
	 * The feature id for the '<em><b>Left Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN = GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN = GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__CONTEXT_NODES = GRAPH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__PRODUCED_NODES = GRAPH_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES = GRAPH_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__REUSABLE_NODES = GRAPH_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Refined Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__REFINED_RULE = GRAPH_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>All Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_NODES = GRAPH_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>All Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES = GRAPH_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>All Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES = GRAPH_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>All Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES = GRAPH_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>All Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES = GRAPH_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>All Refined Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES = GRAPH_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Reusable Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN = GRAPH_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>Graph Grammar Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_GRAMMAR_RULE_FEATURE_COUNT = GRAPH_FEATURE_COUNT + 14;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl <em>Triple Graph Grammar Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammarRule()
	 * @generated
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ANNOTATIONS = GRAPH_GRAMMAR_RULE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ANNOTATION_STRING = GRAPH_GRAMMAR_RULE__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__NAME = GRAPH_GRAMMAR_RULE__NAME;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__NODE = GRAPH_GRAMMAR_RULE__NODE;

	/**
	 * The feature id for the '<em><b>Left Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN = GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Right Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN = GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__CONTEXT_NODES = GRAPH_GRAMMAR_RULE__CONTEXT_NODES;

	/**
	 * The feature id for the '<em><b>Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__PRODUCED_NODES = GRAPH_GRAMMAR_RULE__PRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES = GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__REUSABLE_NODES = GRAPH_GRAMMAR_RULE__REUSABLE_NODES;

	/**
	 * The feature id for the '<em><b>Refined Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__REFINED_RULE = GRAPH_GRAMMAR_RULE__REFINED_RULE;

	/**
	 * The feature id for the '<em><b>All Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_NODES = GRAPH_GRAMMAR_RULE__ALL_NODES;

	/**
	 * The feature id for the '<em><b>All Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES = GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES;

	/**
	 * The feature id for the '<em><b>All Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES = GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>All Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES = GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>All Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES = GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES;

	/**
	 * The feature id for the '<em><b>All Refined Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES = GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES;

	/**
	 * The feature id for the '<em><b>Reusable Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN = GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN;

	/**
	 * The feature id for the '<em><b>Transformation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Overrides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overridden</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Domains Left Side Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Domains Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Typed Model To Domains Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>All Constraints</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Rules Advised To Apply Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Triple Graph Grammar Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_RULE_FEATURE_COUNT = GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 11;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.GraphPatternImpl <em>Graph Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.GraphPatternImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphPattern()
	 * @generated
	 */
	int GRAPH_PATTERN = 3;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_PATTERN__NODE = 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_PATTERN__CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_PATTERN__EDGE = 2;

	/**
	 * The number of structural features of the '<em>Graph Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_PATTERN_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.GraphElementImpl <em>Graph Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.GraphElementImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphElement()
	 * @generated
	 */
	int GRAPH_ELEMENT = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT__ANNOTATIONS = QvtbasePackage.NAMED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT__ANNOTATION_STRING = QvtbasePackage.NAMED_ELEMENT__ANNOTATION_STRING;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.NodeImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT__NAME = QvtbasePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT__LEFT = QvtbasePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT__RIGHT = QvtbasePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Graph Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_ELEMENT_FEATURE_COUNT = QvtbasePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ANNOTATIONS = GRAPH_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ANNOTATION_STRING = GRAPH_ELEMENT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NAME = GRAPH_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__LEFT = GRAPH_ELEMENT__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__RIGHT = GRAPH_ELEMENT__RIGHT;

	/**
	 * The feature id for the '<em><b>Outgoing Edge</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__OUTGOING_EDGE = GRAPH_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Incoming Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__INCOMING_EDGE = GRAPH_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__GRAPH = GRAPH_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Graph Pattern</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__GRAPH_PATTERN = GRAPH_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__TYPED_NAME = GRAPH_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Match Subtype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__MATCH_SUBTYPE = GRAPH_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>EType</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ETYPE = GRAPH_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Refined Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__REFINED_NODE = GRAPH_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>All Outgoing Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ALL_OUTGOING_EDGES = GRAPH_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>All Incoming Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ALL_INCOMING_EDGES = GRAPH_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>All Refined Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ALL_REFINED_NODES = GRAPH_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>All Graph Patterns</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ALL_GRAPH_PATTERNS = GRAPH_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__MATCHING_PRIORITY = GRAPH_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = GRAPH_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.EdgeImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__ANNOTATIONS = GRAPH_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__ANNOTATION_STRING = GRAPH_ELEMENT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__NAME = GRAPH_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__LEFT = GRAPH_ELEMENT__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__RIGHT = GRAPH_ELEMENT__RIGHT;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TYPE_REFERENCE = GRAPH_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SOURCE_NODE = GRAPH_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TARGET_NODE = GRAPH_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Graph Pattern</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__GRAPH_PATTERN = GRAPH_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TYPED_NAME = GRAPH_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Set Modifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SET_MODIFIER = GRAPH_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Link Constraint Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__LINK_CONSTRAINT_TYPE = GRAPH_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Direct Successor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__DIRECT_SUCCESSOR = GRAPH_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Indirect Successor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__INDIRECT_SUCCESSOR = GRAPH_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Index Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__INDEX_EXPRESSION = GRAPH_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Direct Predecessor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__DIRECT_PREDECESSOR = GRAPH_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Indirect Predecessor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__INDIRECT_PREDECESSOR = GRAPH_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = GRAPH_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.LeftGraphPatternImpl <em>Left Graph Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.LeftGraphPatternImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getLeftGraphPattern()
	 * @generated
	 */
	int LEFT_GRAPH_PATTERN = 6;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEFT_GRAPH_PATTERN__NODE = GRAPH_PATTERN__NODE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEFT_GRAPH_PATTERN__CONSTRAINT = GRAPH_PATTERN__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEFT_GRAPH_PATTERN__EDGE = GRAPH_PATTERN__EDGE;

	/**
	 * The feature id for the '<em><b>Graph Grammar</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEFT_GRAPH_PATTERN__GRAPH_GRAMMAR = GRAPH_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Left Graph Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEFT_GRAPH_PATTERN_FEATURE_COUNT = GRAPH_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.RightGraphPatternImpl <em>Right Graph Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.RightGraphPatternImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getRightGraphPattern()
	 * @generated
	 */
	int RIGHT_GRAPH_PATTERN = 7;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHT_GRAPH_PATTERN__NODE = GRAPH_PATTERN__NODE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHT_GRAPH_PATTERN__CONSTRAINT = GRAPH_PATTERN__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHT_GRAPH_PATTERN__EDGE = GRAPH_PATTERN__EDGE;

	/**
	 * The feature id for the '<em><b>Graph Grammar</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHT_GRAPH_PATTERN__GRAPH_GRAMMAR = GRAPH_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Right Graph Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHT_GRAPH_PATTERN_FEATURE_COUNT = GRAPH_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl <em>Domain Graph Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getDomainGraphPattern()
	 * @generated
	 */
	int DOMAIN_GRAPH_PATTERN = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__ANNOTATIONS = QvtbasePackage.DOMAIN__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__ANNOTATION_STRING = QvtbasePackage.DOMAIN__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__NAME = QvtbasePackage.DOMAIN__NAME;

	/**
	 * The feature id for the '<em><b>Is Checkable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__IS_CHECKABLE = QvtbasePackage.DOMAIN__IS_CHECKABLE;

	/**
	 * The feature id for the '<em><b>Is Enforceable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__IS_ENFORCEABLE = QvtbasePackage.DOMAIN__IS_ENFORCEABLE;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__RULE = QvtbasePackage.DOMAIN__RULE;

	/**
	 * The feature id for the '<em><b>Typed Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__TYPED_MODEL = QvtbasePackage.DOMAIN__TYPED_MODEL;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__NODE = QvtbasePackage.DOMAIN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__CONSTRAINT = QvtbasePackage.DOMAIN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN__EDGE = QvtbasePackage.DOMAIN_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Domain Graph Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_GRAPH_PATTERN_FEATURE_COUNT = QvtbasePackage.DOMAIN_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl <em>Triple Graph Grammar</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammar()
	 * @generated
	 */
	int TRIPLE_GRAPH_GRAMMAR = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__ANNOTATIONS = QvtbasePackage.TRANSFORMATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__ANNOTATION_STRING = QvtbasePackage.TRANSFORMATION__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__NAME = QvtbasePackage.TRANSFORMATION__NAME;

	/**
	 * The feature id for the '<em><b>Extended By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__EXTENDED_BY = QvtbasePackage.TRANSFORMATION__EXTENDED_BY;

	/**
	 * The feature id for the '<em><b>Extends Transformation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__EXTENDS_TRANSFORMATION = QvtbasePackage.TRANSFORMATION__EXTENDS_TRANSFORMATION;

	/**
	 * The feature id for the '<em><b>Model Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__MODEL_PARAMETER = QvtbasePackage.TRANSFORMATION__MODEL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__RULE = QvtbasePackage.TRANSFORMATION__RULE;

	/**
	 * The feature id for the '<em><b>Child Transformation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__CHILD_TRANSFORMATION = QvtbasePackage.TRANSFORMATION__CHILD_TRANSFORMATION;

	/**
	 * The feature id for the '<em><b>Parent Transformation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__PARENT_TRANSFORMATION = QvtbasePackage.TRANSFORMATION__PARENT_TRANSFORMATION;

	/**
	 * The feature id for the '<em><b>All Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__ALL_RULES = QvtbasePackage.TRANSFORMATION__ALL_RULES;

	/**
	 * The feature id for the '<em><b>All Model Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__ALL_MODEL_PARAMETERS = QvtbasePackage.TRANSFORMATION__ALL_MODEL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Relevant References</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__RELEVANT_REFERENCES = QvtbasePackage.TRANSFORMATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type Class To Rule Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__TYPE_CLASS_TO_RULE_MAP = QvtbasePackage.TRANSFORMATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Axiom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR__AXIOM = QvtbasePackage.TRANSFORMATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Triple Graph Grammar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_FEATURE_COUNT = QvtbasePackage.TRANSFORMATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.ConstraintImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ANNOTATIONS = QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ANNOTATION_STRING = QvtbasePackage.ANNOTATED_ELEMENT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Triple Graph Grammar Rule</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE = QvtbasePackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Graph Pattern</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__GRAPH_PATTERN = QvtbasePackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = QvtbasePackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl <em>TGG Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.TGGConstraintImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTGGConstraint()
	 * @generated
	 */
	int TGG_CONSTRAINT = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__ANNOTATIONS = CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__ANNOTATION_STRING = CONSTRAINT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Triple Graph Grammar Rule</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE = CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE;

	/**
	 * The feature id for the '<em><b>Graph Pattern</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__GRAPH_PATTERN = CONSTRAINT__GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Slot Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__SLOT_NODE = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Slot Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__SLOT_ATTRIBUTE = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Slot Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME = CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Checkonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__CHECKONLY = CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Evaluate After Matching</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING = CONSTRAINT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>TGG Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_CONSTRAINT_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.ITransformAttribute <em>ITransform Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.ITransformAttribute
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getITransformAttribute()
	 * @generated
	 */
	int ITRANSFORM_ATTRIBUTE = 13;

	/**
	 * The number of structural features of the '<em>ITransform Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITRANSFORM_ATTRIBUTE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.OCLConstraintImpl <em>OCL Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.OCLConstraintImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getOCLConstraint()
	 * @generated
	 */
	int OCL_CONSTRAINT = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__ANNOTATIONS = TGG_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__ANNOTATION_STRING = TGG_CONSTRAINT__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Triple Graph Grammar Rule</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE = TGG_CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE;

	/**
	 * The feature id for the '<em><b>Graph Pattern</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__GRAPH_PATTERN = TGG_CONSTRAINT__GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Slot Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__SLOT_NODE = TGG_CONSTRAINT__SLOT_NODE;

	/**
	 * The feature id for the '<em><b>Slot Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__SLOT_ATTRIBUTE = TGG_CONSTRAINT__SLOT_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Slot Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__SLOT_ATTRIBUTE_NAME = TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME;

	/**
	 * The feature id for the '<em><b>Checkonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__CHECKONLY = TGG_CONSTRAINT__CHECKONLY;

	/**
	 * The feature id for the '<em><b>Evaluate After Matching</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__EVALUATE_AFTER_MATCHING = TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT__EXPRESSION = TGG_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>OCL Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CONSTRAINT_FEATURE_COUNT = TGG_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarAxiomImpl <em>Triple Graph Grammar Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarAxiomImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammarAxiom()
	 * @generated
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ANNOTATIONS = TRIPLE_GRAPH_GRAMMAR_RULE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Annotation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ANNOTATION_STRING = TRIPLE_GRAPH_GRAMMAR_RULE__ANNOTATION_STRING;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__NAME = TRIPLE_GRAPH_GRAMMAR_RULE__NAME;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__NODE = TRIPLE_GRAPH_GRAMMAR_RULE__NODE;

	/**
	 * The feature id for the '<em><b>Left Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__LEFT_GRAPH_PATTERN = TRIPLE_GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Right Graph Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__RIGHT_GRAPH_PATTERN = TRIPLE_GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN;

	/**
	 * The feature id for the '<em><b>Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__CONTEXT_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__CONTEXT_NODES;

	/**
	 * The feature id for the '<em><b>Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__PRODUCED_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__PRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__NONPRODUCED_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__REUSABLE_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__REUSABLE_NODES;

	/**
	 * The feature id for the '<em><b>Refined Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__REFINED_RULE = TRIPLE_GRAPH_GRAMMAR_RULE__REFINED_RULE;

	/**
	 * The feature id for the '<em><b>All Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_NODES;

	/**
	 * The feature id for the '<em><b>All Context Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_CONTEXT_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES;

	/**
	 * The feature id for the '<em><b>All Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_PRODUCED_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>All Nonproduced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_NONPRODUCED_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES;

	/**
	 * The feature id for the '<em><b>All Reusable Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_REUSABLE_NODES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES;

	/**
	 * The feature id for the '<em><b>All Refined Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_REFINED_RULES = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES;

	/**
	 * The feature id for the '<em><b>Reusable Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__REUSABLE_PATTERN = TRIPLE_GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN;

	/**
	 * The feature id for the '<em><b>Transformation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__TRANSFORMATION = TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__DOMAIN = TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN;

	/**
	 * The feature id for the '<em><b>Overrides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__OVERRIDES = TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES;

	/**
	 * The feature id for the '<em><b>Overridden</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__OVERRIDDEN = TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN;

	/**
	 * The feature id for the '<em><b>Domains Left Side Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__DOMAINS_LEFT_SIDE_NODES_MAP = TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP;

	/**
	 * The feature id for the '<em><b>Domains Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__DOMAINS_NODES_MAP = TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP;

	/**
	 * The feature id for the '<em><b>Typed Model To Domains Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__TYPED_MODEL_TO_DOMAINS_MAP = TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__CONSTRAINT = TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ABSTRACT = TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT;

	/**
	 * The feature id for the '<em><b>All Constraints</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__ALL_CONSTRAINTS = TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Rules Advised To Apply Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM__RULES_ADVISED_TO_APPLY_NEXT = TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT;

	/**
	 * The number of structural features of the '<em>Triple Graph Grammar Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLE_GRAPH_GRAMMAR_AXIOM_FEATURE_COUNT = TRIPLE_GRAPH_GRAMMAR_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.impl.ReusablePatternImpl <em>Reusable Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.impl.ReusablePatternImpl
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getReusablePattern()
	 * @generated
	 */
	int REUSABLE_PATTERN = 16;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REUSABLE_PATTERN__NODE = GRAPH_PATTERN__NODE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REUSABLE_PATTERN__CONSTRAINT = GRAPH_PATTERN__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REUSABLE_PATTERN__EDGE = GRAPH_PATTERN__EDGE;

	/**
	 * The feature id for the '<em><b>Graph Grammar</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REUSABLE_PATTERN__GRAPH_GRAMMAR = GRAPH_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reusable Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REUSABLE_PATTERN_FEATURE_COUNT = GRAPH_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.LinkConstraintType <em>Link Constraint Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.LinkConstraintType
	 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getLinkConstraintType()
	 * @generated
	 */
	int LINK_CONSTRAINT_TYPE = 17;

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.Graph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph</em>'.
	 * @see de.upb.swt.qvt.tgg.Graph
	 * @generated
	 */
	EClass getGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.Graph#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Node</em>'.
	 * @see de.upb.swt.qvt.tgg.Graph#getNode()
	 * @see #getGraph()
	 * @generated
	 */
	EReference getGraph_Node();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triple Graph Grammar Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule
	 * @generated
	 */
	EClass getTripleGraphGrammarRule();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsLeftSideNodesMap <em>Domains Left Side Nodes Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domains Left Side Nodes Map</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsLeftSideNodesMap()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EAttribute getTripleGraphGrammarRule_DomainsLeftSideNodesMap();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsNodesMap <em>Domains Nodes Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domains Nodes Map</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsNodesMap()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EAttribute getTripleGraphGrammarRule_DomainsNodesMap();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getTypedModelToDomainsMap <em>Typed Model To Domains Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Typed Model To Domains Map</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getTypedModelToDomainsMap()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EAttribute getTripleGraphGrammarRule_TypedModelToDomainsMap();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getConstraint()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EReference getTripleGraphGrammarRule_Constraint();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#isAbstract()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EAttribute getTripleGraphGrammarRule_Abstract();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getAllConstraints <em>All Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Constraints</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getAllConstraints()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EReference getTripleGraphGrammarRule_AllConstraints();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getRulesAdvisedToApplyNext <em>Rules Advised To Apply Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rules Advised To Apply Next</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getRulesAdvisedToApplyNext()
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	EReference getTripleGraphGrammarRule_RulesAdvisedToApplyNext();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.GraphGrammarRule <em>Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph Grammar Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule
	 * @generated
	 */
	EClass getGraphGrammarRule();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getLeftGraphPattern <em>Left Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getLeftGraphPattern()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_LeftGraphPattern();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern <em>Right Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_RightGraphPattern();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getContextNodes <em>Context Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Context Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getContextNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_ContextNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getProducedNodes <em>Produced Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Produced Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getProducedNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_ProducedNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getNonproducedNodes <em>Nonproduced Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Nonproduced Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getNonproducedNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_NonproducedNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getReusableNodes <em>Reusable Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reusable Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getReusableNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_ReusableNodes();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRefinedRule <em>Refined Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refined Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getRefinedRule()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_RefinedRule();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNodes <em>All Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllContextNodes <em>All Context Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Context Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllContextNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllContextNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllProducedNodes <em>All Produced Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Produced Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllProducedNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllProducedNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNonproducedNodes <em>All Nonproduced Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Nonproduced Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNonproducedNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllNonproducedNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllReusableNodes <em>All Reusable Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Reusable Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllReusableNodes()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllReusableNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllRefinedRules <em>All Refined Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Refined Rules</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getAllRefinedRules()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_AllRefinedRules();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getReusablePattern <em>Reusable Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reusable Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getReusablePattern()
	 * @see #getGraphGrammarRule()
	 * @generated
	 */
	EReference getGraphGrammarRule_ReusablePattern();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.GraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphPattern
	 * @generated
	 */
	EClass getGraphPattern();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphPattern#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Node</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getNode()
	 * @see #getGraphPattern()
	 * @generated
	 */
	EReference getGraphPattern_Node();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphPattern#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraint</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getConstraint()
	 * @see #getGraphPattern()
	 * @generated
	 */
	EReference getGraphPattern_Constraint();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.GraphPattern#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Edge</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getEdge()
	 * @see #getGraphPattern()
	 * @generated
	 */
	EReference getGraphPattern_Edge();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see de.upb.swt.qvt.tgg.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.Node#getOutgoingEdge <em>Outgoing Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outgoing Edge</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getOutgoingEdge()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_OutgoingEdge();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getIncomingEdge <em>Incoming Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Edge</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getIncomingEdge()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_IncomingEdge();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.Node#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getGraph()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Graph();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getGraphPattern()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_GraphPattern();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Node#isMatchSubtype <em>Match Subtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match Subtype</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#isMatchSubtype()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_MatchSubtype();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Node#getEType <em>EType</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EType</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getEType()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_EType();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Node#getRefinedNode <em>Refined Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refined Node</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getRefinedNode()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_RefinedNode();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getAllOutgoingEdges <em>All Outgoing Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Outgoing Edges</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getAllOutgoingEdges()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_AllOutgoingEdges();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getAllIncomingEdges <em>All Incoming Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Incoming Edges</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getAllIncomingEdges()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_AllIncomingEdges();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getAllRefinedNodes <em>All Refined Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Refined Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getAllRefinedNodes()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_AllRefinedNodes();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Node#getAllGraphPatterns <em>All Graph Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Graph Patterns</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getAllGraphPatterns()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_AllGraphPatterns();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Node#getMatchingPriority <em>Matching Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matching Priority</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getMatchingPriority()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_MatchingPriority();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Node#getTypedName <em>Typed Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Typed Name</em>'.
	 * @see de.upb.swt.qvt.tgg.Node#getTypedName()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_TypedName();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Edge#getTypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Reference</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getTypeReference()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_TypeReference();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.Edge#getSourceNode <em>Source Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Source Node</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getSourceNode()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_SourceNode();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Edge#getTargetNode <em>Target Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Node</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getTargetNode()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_TargetNode();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Edge#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getGraphPattern()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_GraphPattern();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Edge#getTypedName <em>Typed Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Typed Name</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getTypedName()
	 * @see #getEdge()
	 * @generated
	 */
	EAttribute getEdge_TypedName();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Edge#getSetModifier <em>Set Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Set Modifier</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getSetModifier()
	 * @see #getEdge()
	 * @generated
	 */
	EAttribute getEdge_SetModifier();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Edge#getLinkConstraintType <em>Link Constraint Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Link Constraint Type</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getLinkConstraintType()
	 * @see #getEdge()
	 * @generated
	 */
	EAttribute getEdge_LinkConstraintType();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Edge#getDirectSuccessor <em>Direct Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Successor</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getDirectSuccessor()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_DirectSuccessor();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Edge#getIndirectSuccessor <em>Indirect Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Indirect Successor</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getIndirectSuccessor()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_IndirectSuccessor();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.Edge#getIndexExpression <em>Index Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index Expression</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getIndexExpression()
	 * @see #getEdge()
	 * @generated
	 */
	EAttribute getEdge_IndexExpression();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Edge#getDirectPredecessor <em>Direct Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Predecessor</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getDirectPredecessor()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_DirectPredecessor();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.Edge#getIndirectPredecessor <em>Indirect Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Indirect Predecessor</em>'.
	 * @see de.upb.swt.qvt.tgg.Edge#getIndirectPredecessor()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_IndirectPredecessor();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.LeftGraphPattern <em>Left Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Left Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.LeftGraphPattern
	 * @generated
	 */
	EClass getLeftGraphPattern();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.LeftGraphPattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph Grammar</em>'.
	 * @see de.upb.swt.qvt.tgg.LeftGraphPattern#getGraphGrammar()
	 * @see #getLeftGraphPattern()
	 * @generated
	 */
	EReference getLeftGraphPattern_GraphGrammar();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.RightGraphPattern <em>Right Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Right Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.RightGraphPattern
	 * @generated
	 */
	EClass getRightGraphPattern();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph Grammar</em>'.
	 * @see de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar()
	 * @see #getRightGraphPattern()
	 * @generated
	 */
	EReference getRightGraphPattern_GraphGrammar();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.DomainGraphPattern <em>Domain Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Domain Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.DomainGraphPattern
	 * @generated
	 */
	EClass getDomainGraphPattern();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar <em>Triple Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triple Graph Grammar</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammar
	 * @generated
	 */
	EClass getTripleGraphGrammar();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getRelevantReferences <em>Relevant References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relevant References</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammar#getRelevantReferences()
	 * @see #getTripleGraphGrammar()
	 * @generated
	 */
	EAttribute getTripleGraphGrammar_RelevantReferences();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getTypeClassToRuleMap <em>Type Class To Rule Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Class To Rule Map</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammar#getTypeClassToRuleMap()
	 * @see #getTripleGraphGrammar()
	 * @generated
	 */
	EAttribute getTripleGraphGrammar_TypeClassToRuleMap();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getAxiom <em>Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Axiom</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammar#getAxiom()
	 * @see #getTripleGraphGrammar()
	 * @generated
	 */
	EReference getTripleGraphGrammar_Axiom();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see de.upb.swt.qvt.tgg.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Triple Graph Grammar Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_TripleGraphGrammarRule();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.Constraint#getGraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.Constraint#getGraphPattern()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_GraphPattern();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.TGGConstraint <em>TGG Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Constraint</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint
	 * @generated
	 */
	EClass getTGGConstraint();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotNode <em>Slot Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Slot Node</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint#getSlotNode()
	 * @see #getTGGConstraint()
	 * @generated
	 */
	EReference getTGGConstraint_SlotNode();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttribute <em>Slot Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Slot Attribute</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttribute()
	 * @see #getTGGConstraint()
	 * @generated
	 */
	EReference getTGGConstraint_SlotAttribute();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttributeName <em>Slot Attribute Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Slot Attribute Name</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttributeName()
	 * @see #getTGGConstraint()
	 * @generated
	 */
	EAttribute getTGGConstraint_SlotAttributeName();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TGGConstraint#isCheckonly <em>Checkonly</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checkonly</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint#isCheckonly()
	 * @see #getTGGConstraint()
	 * @generated
	 */
	EAttribute getTGGConstraint_Checkonly();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.TGGConstraint#isEvaluateAfterMatching <em>Evaluate After Matching</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluate After Matching</em>'.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint#isEvaluateAfterMatching()
	 * @see #getTGGConstraint()
	 * @generated
	 */
	EAttribute getTGGConstraint_EvaluateAfterMatching();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.GraphElement <em>Graph Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph Element</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphElement
	 * @generated
	 */
	EClass getGraphElement();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.GraphElement#isLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphElement#isLeft()
	 * @see #getGraphElement()
	 * @generated
	 */
	EAttribute getGraphElement_Left();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.GraphElement#isRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right</em>'.
	 * @see de.upb.swt.qvt.tgg.GraphElement#isRight()
	 * @see #getGraphElement()
	 * @generated
	 */
	EAttribute getGraphElement_Right();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.ITransformAttribute <em>ITransform Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ITransform Attribute</em>'.
	 * @see de.upb.swt.qvt.tgg.ITransformAttribute
	 * @generated
	 */
	EClass getITransformAttribute();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.OCLConstraint <em>OCL Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Constraint</em>'.
	 * @see de.upb.swt.qvt.tgg.OCLConstraint
	 * @generated
	 */
	EClass getOCLConstraint();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.OCLConstraint#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see de.upb.swt.qvt.tgg.OCLConstraint#getExpression()
	 * @see #getOCLConstraint()
	 * @generated
	 */
	EAttribute getOCLConstraint_Expression();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom <em>Triple Graph Grammar Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triple Graph Grammar Axiom</em>'.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom
	 * @generated
	 */
	EClass getTripleGraphGrammarAxiom();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.ReusablePattern <em>Reusable Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reusable Pattern</em>'.
	 * @see de.upb.swt.qvt.tgg.ReusablePattern
	 * @generated
	 */
	EClass getReusablePattern();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph Grammar</em>'.
	 * @see de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar()
	 * @see #getReusablePattern()
	 * @generated
	 */
	EReference getReusablePattern_GraphGrammar();

	/**
	 * Returns the meta object for enum '{@link de.upb.swt.qvt.tgg.LinkConstraintType <em>Link Constraint Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Link Constraint Type</em>'.
	 * @see de.upb.swt.qvt.tgg.LinkConstraintType
	 * @generated
	 */
	EEnum getLinkConstraintType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TggFactory getTggFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.GraphImpl <em>Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.GraphImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraph()
		 * @generated
		 */
		EClass GRAPH = eINSTANCE.getGraph();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH__NODE = eINSTANCE.getGraph_Node();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl <em>Triple Graph Grammar Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammarRule()
		 * @generated
		 */
		EClass TRIPLE_GRAPH_GRAMMAR_RULE = eINSTANCE.getTripleGraphGrammarRule();

		/**
		 * The meta object literal for the '<em><b>Domains Left Side Nodes Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP = eINSTANCE.getTripleGraphGrammarRule_DomainsLeftSideNodesMap();

		/**
		 * The meta object literal for the '<em><b>Domains Nodes Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP = eINSTANCE.getTripleGraphGrammarRule_DomainsNodesMap();

		/**
		 * The meta object literal for the '<em><b>Typed Model To Domains Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP = eINSTANCE.getTripleGraphGrammarRule_TypedModelToDomainsMap();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT = eINSTANCE.getTripleGraphGrammarRule_Constraint();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT = eINSTANCE.getTripleGraphGrammarRule_Abstract();

		/**
		 * The meta object literal for the '<em><b>All Constraints</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS = eINSTANCE.getTripleGraphGrammarRule_AllConstraints();

		/**
		 * The meta object literal for the '<em><b>Rules Advised To Apply Next</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT = eINSTANCE.getTripleGraphGrammarRule_RulesAdvisedToApplyNext();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl <em>Graph Grammar Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.GraphGrammarRuleImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphGrammarRule()
		 * @generated
		 */
		EClass GRAPH_GRAMMAR_RULE = eINSTANCE.getGraphGrammarRule();

		/**
		 * The meta object literal for the '<em><b>Left Graph Pattern</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN = eINSTANCE.getGraphGrammarRule_LeftGraphPattern();

		/**
		 * The meta object literal for the '<em><b>Right Graph Pattern</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN = eINSTANCE.getGraphGrammarRule_RightGraphPattern();

		/**
		 * The meta object literal for the '<em><b>Context Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__CONTEXT_NODES = eINSTANCE.getGraphGrammarRule_ContextNodes();

		/**
		 * The meta object literal for the '<em><b>Produced Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__PRODUCED_NODES = eINSTANCE.getGraphGrammarRule_ProducedNodes();

		/**
		 * The meta object literal for the '<em><b>Nonproduced Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES = eINSTANCE.getGraphGrammarRule_NonproducedNodes();

		/**
		 * The meta object literal for the '<em><b>Reusable Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__REUSABLE_NODES = eINSTANCE.getGraphGrammarRule_ReusableNodes();

		/**
		 * The meta object literal for the '<em><b>Refined Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__REFINED_RULE = eINSTANCE.getGraphGrammarRule_RefinedRule();

		/**
		 * The meta object literal for the '<em><b>All Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_NODES = eINSTANCE.getGraphGrammarRule_AllNodes();

		/**
		 * The meta object literal for the '<em><b>All Context Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES = eINSTANCE.getGraphGrammarRule_AllContextNodes();

		/**
		 * The meta object literal for the '<em><b>All Produced Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES = eINSTANCE.getGraphGrammarRule_AllProducedNodes();

		/**
		 * The meta object literal for the '<em><b>All Nonproduced Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES = eINSTANCE.getGraphGrammarRule_AllNonproducedNodes();

		/**
		 * The meta object literal for the '<em><b>All Reusable Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES = eINSTANCE.getGraphGrammarRule_AllReusableNodes();

		/**
		 * The meta object literal for the '<em><b>All Refined Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES = eINSTANCE.getGraphGrammarRule_AllRefinedRules();

		/**
		 * The meta object literal for the '<em><b>Reusable Pattern</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN = eINSTANCE.getGraphGrammarRule_ReusablePattern();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.GraphPatternImpl <em>Graph Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.GraphPatternImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphPattern()
		 * @generated
		 */
		EClass GRAPH_PATTERN = eINSTANCE.getGraphPattern();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_PATTERN__NODE = eINSTANCE.getGraphPattern_Node();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_PATTERN__CONSTRAINT = eINSTANCE.getGraphPattern_Constraint();

		/**
		 * The meta object literal for the '<em><b>Edge</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_PATTERN__EDGE = eINSTANCE.getGraphPattern_Edge();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.NodeImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Outgoing Edge</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__OUTGOING_EDGE = eINSTANCE.getNode_OutgoingEdge();

		/**
		 * The meta object literal for the '<em><b>Incoming Edge</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__INCOMING_EDGE = eINSTANCE.getNode_IncomingEdge();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__GRAPH = eINSTANCE.getNode_Graph();

		/**
		 * The meta object literal for the '<em><b>Graph Pattern</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__GRAPH_PATTERN = eINSTANCE.getNode_GraphPattern();

		/**
		 * The meta object literal for the '<em><b>Match Subtype</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__MATCH_SUBTYPE = eINSTANCE.getNode_MatchSubtype();

		/**
		 * The meta object literal for the '<em><b>EType</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ETYPE = eINSTANCE.getNode_EType();

		/**
		 * The meta object literal for the '<em><b>Refined Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__REFINED_NODE = eINSTANCE.getNode_RefinedNode();

		/**
		 * The meta object literal for the '<em><b>All Outgoing Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ALL_OUTGOING_EDGES = eINSTANCE.getNode_AllOutgoingEdges();

		/**
		 * The meta object literal for the '<em><b>All Incoming Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ALL_INCOMING_EDGES = eINSTANCE.getNode_AllIncomingEdges();

		/**
		 * The meta object literal for the '<em><b>All Refined Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ALL_REFINED_NODES = eINSTANCE.getNode_AllRefinedNodes();

		/**
		 * The meta object literal for the '<em><b>All Graph Patterns</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ALL_GRAPH_PATTERNS = eINSTANCE.getNode_AllGraphPatterns();

		/**
		 * The meta object literal for the '<em><b>Matching Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__MATCHING_PRIORITY = eINSTANCE.getNode_MatchingPriority();

		/**
		 * The meta object literal for the '<em><b>Typed Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__TYPED_NAME = eINSTANCE.getNode_TypedName();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.EdgeImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>Type Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TYPE_REFERENCE = eINSTANCE.getEdge_TypeReference();

		/**
		 * The meta object literal for the '<em><b>Source Node</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SOURCE_NODE = eINSTANCE.getEdge_SourceNode();

		/**
		 * The meta object literal for the '<em><b>Target Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TARGET_NODE = eINSTANCE.getEdge_TargetNode();

		/**
		 * The meta object literal for the '<em><b>Graph Pattern</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__GRAPH_PATTERN = eINSTANCE.getEdge_GraphPattern();

		/**
		 * The meta object literal for the '<em><b>Typed Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE__TYPED_NAME = eINSTANCE.getEdge_TypedName();

		/**
		 * The meta object literal for the '<em><b>Set Modifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE__SET_MODIFIER = eINSTANCE.getEdge_SetModifier();

		/**
		 * The meta object literal for the '<em><b>Link Constraint Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE__LINK_CONSTRAINT_TYPE = eINSTANCE.getEdge_LinkConstraintType();

		/**
		 * The meta object literal for the '<em><b>Direct Successor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__DIRECT_SUCCESSOR = eINSTANCE.getEdge_DirectSuccessor();

		/**
		 * The meta object literal for the '<em><b>Indirect Successor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__INDIRECT_SUCCESSOR = eINSTANCE.getEdge_IndirectSuccessor();

		/**
		 * The meta object literal for the '<em><b>Index Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE__INDEX_EXPRESSION = eINSTANCE.getEdge_IndexExpression();

		/**
		 * The meta object literal for the '<em><b>Direct Predecessor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__DIRECT_PREDECESSOR = eINSTANCE.getEdge_DirectPredecessor();

		/**
		 * The meta object literal for the '<em><b>Indirect Predecessor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__INDIRECT_PREDECESSOR = eINSTANCE.getEdge_IndirectPredecessor();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.LeftGraphPatternImpl <em>Left Graph Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.LeftGraphPatternImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getLeftGraphPattern()
		 * @generated
		 */
		EClass LEFT_GRAPH_PATTERN = eINSTANCE.getLeftGraphPattern();

		/**
		 * The meta object literal for the '<em><b>Graph Grammar</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEFT_GRAPH_PATTERN__GRAPH_GRAMMAR = eINSTANCE.getLeftGraphPattern_GraphGrammar();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.RightGraphPatternImpl <em>Right Graph Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.RightGraphPatternImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getRightGraphPattern()
		 * @generated
		 */
		EClass RIGHT_GRAPH_PATTERN = eINSTANCE.getRightGraphPattern();

		/**
		 * The meta object literal for the '<em><b>Graph Grammar</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RIGHT_GRAPH_PATTERN__GRAPH_GRAMMAR = eINSTANCE.getRightGraphPattern_GraphGrammar();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl <em>Domain Graph Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getDomainGraphPattern()
		 * @generated
		 */
		EClass DOMAIN_GRAPH_PATTERN = eINSTANCE.getDomainGraphPattern();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl <em>Triple Graph Grammar</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammar()
		 * @generated
		 */
		EClass TRIPLE_GRAPH_GRAMMAR = eINSTANCE.getTripleGraphGrammar();

		/**
		 * The meta object literal for the '<em><b>Relevant References</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR__RELEVANT_REFERENCES = eINSTANCE.getTripleGraphGrammar_RelevantReferences();

		/**
		 * The meta object literal for the '<em><b>Type Class To Rule Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLE_GRAPH_GRAMMAR__TYPE_CLASS_TO_RULE_MAP = eINSTANCE.getTripleGraphGrammar_TypeClassToRuleMap();

		/**
		 * The meta object literal for the '<em><b>Axiom</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIPLE_GRAPH_GRAMMAR__AXIOM = eINSTANCE.getTripleGraphGrammar_Axiom();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.ConstraintImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '<em><b>Triple Graph Grammar Rule</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE = eINSTANCE.getConstraint_TripleGraphGrammarRule();

		/**
		 * The meta object literal for the '<em><b>Graph Pattern</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__GRAPH_PATTERN = eINSTANCE.getConstraint_GraphPattern();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.TGGConstraintImpl <em>TGG Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.TGGConstraintImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTGGConstraint()
		 * @generated
		 */
		EClass TGG_CONSTRAINT = eINSTANCE.getTGGConstraint();

		/**
		 * The meta object literal for the '<em><b>Slot Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_CONSTRAINT__SLOT_NODE = eINSTANCE.getTGGConstraint_SlotNode();

		/**
		 * The meta object literal for the '<em><b>Slot Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_CONSTRAINT__SLOT_ATTRIBUTE = eINSTANCE.getTGGConstraint_SlotAttribute();

		/**
		 * The meta object literal for the '<em><b>Slot Attribute Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME = eINSTANCE.getTGGConstraint_SlotAttributeName();

		/**
		 * The meta object literal for the '<em><b>Checkonly</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_CONSTRAINT__CHECKONLY = eINSTANCE.getTGGConstraint_Checkonly();

		/**
		 * The meta object literal for the '<em><b>Evaluate After Matching</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING = eINSTANCE.getTGGConstraint_EvaluateAfterMatching();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.GraphElementImpl <em>Graph Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.GraphElementImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getGraphElement()
		 * @generated
		 */
		EClass GRAPH_ELEMENT = eINSTANCE.getGraphElement();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_ELEMENT__LEFT = eINSTANCE.getGraphElement_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_ELEMENT__RIGHT = eINSTANCE.getGraphElement_Right();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.ITransformAttribute <em>ITransform Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.ITransformAttribute
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getITransformAttribute()
		 * @generated
		 */
		EClass ITRANSFORM_ATTRIBUTE = eINSTANCE.getITransformAttribute();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.OCLConstraintImpl <em>OCL Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.OCLConstraintImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getOCLConstraint()
		 * @generated
		 */
		EClass OCL_CONSTRAINT = eINSTANCE.getOCLConstraint();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCL_CONSTRAINT__EXPRESSION = eINSTANCE.getOCLConstraint_Expression();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarAxiomImpl <em>Triple Graph Grammar Axiom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.TripleGraphGrammarAxiomImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getTripleGraphGrammarAxiom()
		 * @generated
		 */
		EClass TRIPLE_GRAPH_GRAMMAR_AXIOM = eINSTANCE.getTripleGraphGrammarAxiom();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.impl.ReusablePatternImpl <em>Reusable Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.impl.ReusablePatternImpl
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getReusablePattern()
		 * @generated
		 */
		EClass REUSABLE_PATTERN = eINSTANCE.getReusablePattern();

		/**
		 * The meta object literal for the '<em><b>Graph Grammar</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REUSABLE_PATTERN__GRAPH_GRAMMAR = eINSTANCE.getReusablePattern_GraphGrammar();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.LinkConstraintType <em>Link Constraint Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.LinkConstraintType
		 * @see de.upb.swt.qvt.tgg.impl.TggPackageImpl#getLinkConstraintType()
		 * @generated
		 */
		EEnum LINK_CONSTRAINT_TYPE = eINSTANCE.getLinkConstraintType();

	}

} //TggPackage
