/**
 * <copyright>
 * </copyright>
 *
 * $Id: EqualsAttributeConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equals Attribute Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.EqualsAttributeConstraint#getTransformAttributeClass <em>Transform Attribute Class</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.EqualsAttributeConstraint#getBundle <em>Bundle</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getEqualsAttributeConstraint()
 * @model
 * @generated
 */
public interface EqualsAttributeConstraint extends AttributeConstraint {

	/**
	 * Returns the value of the '<em><b>Transform Attribute Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transform Attribute Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transform Attribute Class</em>' attribute.
	 * @see #setTransformAttributeClass(String)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEqualsAttributeConstraint_TransformAttributeClass()
	 * @model
	 * @generated
	 */
	String getTransformAttributeClass();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.EqualsAttributeConstraint#getTransformAttributeClass <em>Transform Attribute Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transform Attribute Class</em>' attribute.
	 * @see #getTransformAttributeClass()
	 * @generated
	 */
	void setTransformAttributeClass(String value);

	/**
	 * Returns the value of the '<em><b>Bundle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bundle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bundle</em>' attribute.
	 * @see #setBundle(String)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEqualsAttributeConstraint_Bundle()
	 * @model
	 * @generated
	 */
	String getBundle();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.EqualsAttributeConstraint#getBundle <em>Bundle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bundle</em>' attribute.
	 * @see #getBundle()
	 * @generated
	 */
	void setBundle(String value);
} // EqualsAttributeConstraint
