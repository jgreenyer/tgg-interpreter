/**
 * <copyright>
 * </copyright>
 *
 * $Id: EdgeImpl.java 1810 2007-02-23 12:29:01 +0000 (Fr, 23 Feb 2007) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.LinkConstraintType;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getTypeReference <em>Type Reference</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getSourceNode <em>Source Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getTargetNode <em>Target Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getGraphPattern <em>Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getTypedName <em>Typed Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getSetModifier <em>Set Modifier</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getLinkConstraintType <em>Link Constraint Type</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getDirectSuccessor <em>Direct Successor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getIndirectSuccessor <em>Indirect Successor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getIndexExpression <em>Index Expression</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getDirectPredecessor <em>Direct Predecessor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.EdgeImpl#getIndirectPredecessor <em>Indirect Predecessor</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EdgeImpl extends GraphElementImpl implements Edge {
	/**
	 * The cached value of the '{@link #getTypeReference() <em>Type Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeReference()
	 * @generated
	 * @ordered
	 */
	protected EReference typeReference;

	/**
	 * The cached value of the '{@link #getTargetNode() <em>Target Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetNode()
	 * @generated
	 * @ordered
	 */
	protected Node targetNode;

	/**
	 * The cached value of the '{@link #getGraphPattern() <em>Graph Pattern</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphPattern()
	 * @generated
	 * @ordered
	 */
	protected EList<GraphPattern> graphPattern;

	/**
	 * The default value of the '{@link #getTypedName() <em>Typed Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPED_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSetModifier() <em>Set Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetModifier()
	 * @generated
	 * @ordered
	 */
	protected static final String SET_MODIFIER_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getSetModifier() <em>Set Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetModifier()
	 * @generated
	 * @ordered
	 */
	protected String setModifier = SET_MODIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getLinkConstraintType() <em>Link Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkConstraintType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkConstraintType LINK_CONSTRAINT_TYPE_EDEFAULT = LinkConstraintType.UNDEFINED;

	/**
	 * The cached value of the '{@link #getLinkConstraintType() <em>Link Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkConstraintType()
	 * @generated
	 * @ordered
	 */
	protected LinkConstraintType linkConstraintType = LINK_CONSTRAINT_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDirectSuccessor() <em>Direct Successor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectSuccessor()
	 * @generated
	 * @ordered
	 */
	protected Edge directSuccessor;

	/**
	 * The cached value of the '{@link #getIndirectSuccessor() <em>Indirect Successor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndirectSuccessor()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> indirectSuccessor;

	/**
	 * The default value of the '{@link #getIndexExpression() <em>Index Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String INDEX_EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIndexExpression() <em>Index Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexExpression()
	 * @generated
	 * @ordered
	 */
	protected String indexExpression = INDEX_EXPRESSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDirectPredecessor() <em>Direct Predecessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectPredecessor()
	 * @generated
	 * @ordered
	 */
	protected Edge directPredecessor;

	/**
	 * The cached value of the '{@link #getIndirectPredecessor() <em>Indirect Predecessor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndirectPredecessor()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> indirectPredecessor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeReference() {
		if (typeReference != null && typeReference.eIsProxy()) {
			InternalEObject oldTypeReference = (InternalEObject)typeReference;
			typeReference = (EReference)eResolveProxy(oldTypeReference);
			if (typeReference != oldTypeReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.EDGE__TYPE_REFERENCE, oldTypeReference, typeReference));
			}
		}
		return typeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetTypeReference() {
		return typeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTypeReference(EReference newTypeReference) {
		EReference oldTypeReference = typeReference;
		String oldTypedName = getTypedName();
		typeReference = newTypeReference;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__TYPE_REFERENCE, oldTypeReference,
					typeReference));
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__TYPED_NAME, oldTypedName,
					getTypedName()));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getSourceNode() {
		if (eContainerFeatureID() != TggPackage.EDGE__SOURCE_NODE) return null;
		return (Node)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceNode(Node newSourceNode, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSourceNode, TggPackage.EDGE__SOURCE_NODE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceNode(Node newSourceNode) {
		if (newSourceNode != eInternalContainer() || (eContainerFeatureID() != TggPackage.EDGE__SOURCE_NODE && newSourceNode != null)) {
			if (EcoreUtil.isAncestor(this, newSourceNode))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSourceNode != null)
				msgs = ((InternalEObject)newSourceNode).eInverseAdd(this, TggPackage.NODE__OUTGOING_EDGE, Node.class, msgs);
			msgs = basicSetSourceNode(newSourceNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__SOURCE_NODE, newSourceNode, newSourceNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getTargetNode() {
		if (targetNode != null && targetNode.eIsProxy()) {
			InternalEObject oldTargetNode = (InternalEObject)targetNode;
			targetNode = (Node)eResolveProxy(oldTargetNode);
			if (targetNode != oldTargetNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.EDGE__TARGET_NODE, oldTargetNode, targetNode));
			}
		}
		return targetNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetTargetNode() {
		return targetNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetNode(Node newTargetNode, NotificationChain msgs) {
		Node oldTargetNode = targetNode;
		targetNode = newTargetNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__TARGET_NODE, oldTargetNode, newTargetNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetNode(Node newTargetNode) {
		if (newTargetNode != targetNode) {
			NotificationChain msgs = null;
			if (targetNode != null)
				msgs = ((InternalEObject)targetNode).eInverseRemove(this, TggPackage.NODE__INCOMING_EDGE, Node.class, msgs);
			if (newTargetNode != null)
				msgs = ((InternalEObject)newTargetNode).eInverseAdd(this, TggPackage.NODE__INCOMING_EDGE, Node.class, msgs);
			msgs = basicSetTargetNode(newTargetNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__TARGET_NODE, newTargetNode, newTargetNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GraphPattern> getGraphPattern() {
		if (graphPattern == null) {
			graphPattern = new EObjectWithInverseResolvingEList.ManyInverse<GraphPattern>(GraphPattern.class, this, TggPackage.EDGE__GRAPH_PATTERN, TggPackage.GRAPH_PATTERN__EDGE);
		}
		return graphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getTypedName() {
		if (getTypeReference() != null) {
			return getTypeReference().getName();
		} else {
			return "type_unknown";
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSetModifier() {
		return setModifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSetModifier(String newSetModifier) {
		String oldSetModifier = setModifier;
		setModifier = newSetModifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__SET_MODIFIER, oldSetModifier, setModifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkConstraintType getLinkConstraintType() {
		return linkConstraintType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkConstraintType(LinkConstraintType newLinkConstraintType) {
		LinkConstraintType oldLinkConstraintType = linkConstraintType;
		linkConstraintType = newLinkConstraintType == null ? LINK_CONSTRAINT_TYPE_EDEFAULT : newLinkConstraintType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__LINK_CONSTRAINT_TYPE, oldLinkConstraintType, linkConstraintType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge getDirectSuccessor() {
		if (directSuccessor != null && directSuccessor.eIsProxy()) {
			InternalEObject oldDirectSuccessor = (InternalEObject)directSuccessor;
			directSuccessor = (Edge)eResolveProxy(oldDirectSuccessor);
			if (directSuccessor != oldDirectSuccessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.EDGE__DIRECT_SUCCESSOR, oldDirectSuccessor, directSuccessor));
			}
		}
		return directSuccessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge basicGetDirectSuccessor() {
		return directSuccessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDirectSuccessor(Edge newDirectSuccessor, NotificationChain msgs) {
		Edge oldDirectSuccessor = directSuccessor;
		directSuccessor = newDirectSuccessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__DIRECT_SUCCESSOR, oldDirectSuccessor, newDirectSuccessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectSuccessor(Edge newDirectSuccessor) {
		if (newDirectSuccessor != directSuccessor) {
			NotificationChain msgs = null;
			if (directSuccessor != null)
				msgs = ((InternalEObject)directSuccessor).eInverseRemove(this, TggPackage.EDGE__DIRECT_PREDECESSOR, Edge.class, msgs);
			if (newDirectSuccessor != null)
				msgs = ((InternalEObject)newDirectSuccessor).eInverseAdd(this, TggPackage.EDGE__DIRECT_PREDECESSOR, Edge.class, msgs);
			msgs = basicSetDirectSuccessor(newDirectSuccessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__DIRECT_SUCCESSOR, newDirectSuccessor, newDirectSuccessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getIndirectSuccessor() {
		if (indirectSuccessor == null) {
			indirectSuccessor = new EObjectWithInverseResolvingEList.ManyInverse<Edge>(Edge.class, this, TggPackage.EDGE__INDIRECT_SUCCESSOR, TggPackage.EDGE__INDIRECT_PREDECESSOR);
		}
		return indirectSuccessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIndexExpression() {
		return indexExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexExpression(String newIndexExpression) {
		String oldIndexExpression = indexExpression;
		indexExpression = newIndexExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__INDEX_EXPRESSION, oldIndexExpression, indexExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge getDirectPredecessor() {
		if (directPredecessor != null && directPredecessor.eIsProxy()) {
			InternalEObject oldDirectPredecessor = (InternalEObject)directPredecessor;
			directPredecessor = (Edge)eResolveProxy(oldDirectPredecessor);
			if (directPredecessor != oldDirectPredecessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.EDGE__DIRECT_PREDECESSOR, oldDirectPredecessor, directPredecessor));
			}
		}
		return directPredecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge basicGetDirectPredecessor() {
		return directPredecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDirectPredecessor(Edge newDirectPredecessor, NotificationChain msgs) {
		Edge oldDirectPredecessor = directPredecessor;
		directPredecessor = newDirectPredecessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__DIRECT_PREDECESSOR, oldDirectPredecessor, newDirectPredecessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectPredecessor(Edge newDirectPredecessor) {
		if (newDirectPredecessor != directPredecessor) {
			NotificationChain msgs = null;
			if (directPredecessor != null)
				msgs = ((InternalEObject)directPredecessor).eInverseRemove(this, TggPackage.EDGE__DIRECT_SUCCESSOR, Edge.class, msgs);
			if (newDirectPredecessor != null)
				msgs = ((InternalEObject)newDirectPredecessor).eInverseAdd(this, TggPackage.EDGE__DIRECT_SUCCESSOR, Edge.class, msgs);
			msgs = basicSetDirectPredecessor(newDirectPredecessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.EDGE__DIRECT_PREDECESSOR, newDirectPredecessor, newDirectPredecessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getIndirectPredecessor() {
		if (indirectPredecessor == null) {
			indirectPredecessor = new EObjectWithInverseResolvingEList.ManyInverse<Edge>(Edge.class, this, TggPackage.EDGE__INDIRECT_PREDECESSOR, TggPackage.EDGE__INDIRECT_SUCCESSOR);
		}
		return indirectPredecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.EDGE__SOURCE_NODE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSourceNode((Node)otherEnd, msgs);
			case TggPackage.EDGE__TARGET_NODE:
				if (targetNode != null)
					msgs = ((InternalEObject)targetNode).eInverseRemove(this, TggPackage.NODE__INCOMING_EDGE, Node.class, msgs);
				return basicSetTargetNode((Node)otherEnd, msgs);
			case TggPackage.EDGE__GRAPH_PATTERN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGraphPattern()).basicAdd(otherEnd, msgs);
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				if (directSuccessor != null)
					msgs = ((InternalEObject)directSuccessor).eInverseRemove(this, TggPackage.EDGE__DIRECT_PREDECESSOR, Edge.class, msgs);
				return basicSetDirectSuccessor((Edge)otherEnd, msgs);
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIndirectSuccessor()).basicAdd(otherEnd, msgs);
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				if (directPredecessor != null)
					msgs = ((InternalEObject)directPredecessor).eInverseRemove(this, TggPackage.EDGE__DIRECT_SUCCESSOR, Edge.class, msgs);
				return basicSetDirectPredecessor((Edge)otherEnd, msgs);
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIndirectPredecessor()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.EDGE__SOURCE_NODE:
				return basicSetSourceNode(null, msgs);
			case TggPackage.EDGE__TARGET_NODE:
				return basicSetTargetNode(null, msgs);
			case TggPackage.EDGE__GRAPH_PATTERN:
				return ((InternalEList<?>)getGraphPattern()).basicRemove(otherEnd, msgs);
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				return basicSetDirectSuccessor(null, msgs);
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				return ((InternalEList<?>)getIndirectSuccessor()).basicRemove(otherEnd, msgs);
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				return basicSetDirectPredecessor(null, msgs);
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				return ((InternalEList<?>)getIndirectPredecessor()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TggPackage.EDGE__SOURCE_NODE:
				return eInternalContainer().eInverseRemove(this, TggPackage.NODE__OUTGOING_EDGE, Node.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.EDGE__TYPE_REFERENCE:
				if (resolve) return getTypeReference();
				return basicGetTypeReference();
			case TggPackage.EDGE__SOURCE_NODE:
				return getSourceNode();
			case TggPackage.EDGE__TARGET_NODE:
				if (resolve) return getTargetNode();
				return basicGetTargetNode();
			case TggPackage.EDGE__GRAPH_PATTERN:
				return getGraphPattern();
			case TggPackage.EDGE__TYPED_NAME:
				return getTypedName();
			case TggPackage.EDGE__SET_MODIFIER:
				return getSetModifier();
			case TggPackage.EDGE__LINK_CONSTRAINT_TYPE:
				return getLinkConstraintType();
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				if (resolve) return getDirectSuccessor();
				return basicGetDirectSuccessor();
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				return getIndirectSuccessor();
			case TggPackage.EDGE__INDEX_EXPRESSION:
				return getIndexExpression();
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				if (resolve) return getDirectPredecessor();
				return basicGetDirectPredecessor();
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				return getIndirectPredecessor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.EDGE__TYPE_REFERENCE:
				setTypeReference((EReference)newValue);
				return;
			case TggPackage.EDGE__SOURCE_NODE:
				setSourceNode((Node)newValue);
				return;
			case TggPackage.EDGE__TARGET_NODE:
				setTargetNode((Node)newValue);
				return;
			case TggPackage.EDGE__GRAPH_PATTERN:
				getGraphPattern().clear();
				getGraphPattern().addAll((Collection<? extends GraphPattern>)newValue);
				return;
			case TggPackage.EDGE__SET_MODIFIER:
				setSetModifier((String)newValue);
				return;
			case TggPackage.EDGE__LINK_CONSTRAINT_TYPE:
				setLinkConstraintType((LinkConstraintType)newValue);
				return;
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				setDirectSuccessor((Edge)newValue);
				return;
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				getIndirectSuccessor().clear();
				getIndirectSuccessor().addAll((Collection<? extends Edge>)newValue);
				return;
			case TggPackage.EDGE__INDEX_EXPRESSION:
				setIndexExpression((String)newValue);
				return;
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				setDirectPredecessor((Edge)newValue);
				return;
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				getIndirectPredecessor().clear();
				getIndirectPredecessor().addAll((Collection<? extends Edge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.EDGE__TYPE_REFERENCE:
				setTypeReference((EReference)null);
				return;
			case TggPackage.EDGE__SOURCE_NODE:
				setSourceNode((Node)null);
				return;
			case TggPackage.EDGE__TARGET_NODE:
				setTargetNode((Node)null);
				return;
			case TggPackage.EDGE__GRAPH_PATTERN:
				getGraphPattern().clear();
				return;
			case TggPackage.EDGE__SET_MODIFIER:
				setSetModifier(SET_MODIFIER_EDEFAULT);
				return;
			case TggPackage.EDGE__LINK_CONSTRAINT_TYPE:
				setLinkConstraintType(LINK_CONSTRAINT_TYPE_EDEFAULT);
				return;
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				setDirectSuccessor((Edge)null);
				return;
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				getIndirectSuccessor().clear();
				return;
			case TggPackage.EDGE__INDEX_EXPRESSION:
				setIndexExpression(INDEX_EXPRESSION_EDEFAULT);
				return;
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				setDirectPredecessor((Edge)null);
				return;
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				getIndirectPredecessor().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.EDGE__TYPE_REFERENCE:
				return typeReference != null;
			case TggPackage.EDGE__SOURCE_NODE:
				return getSourceNode() != null;
			case TggPackage.EDGE__TARGET_NODE:
				return targetNode != null;
			case TggPackage.EDGE__GRAPH_PATTERN:
				return graphPattern != null && !graphPattern.isEmpty();
			case TggPackage.EDGE__TYPED_NAME:
				return TYPED_NAME_EDEFAULT == null ? getTypedName() != null : !TYPED_NAME_EDEFAULT.equals(getTypedName());
			case TggPackage.EDGE__SET_MODIFIER:
				return SET_MODIFIER_EDEFAULT == null ? setModifier != null : !SET_MODIFIER_EDEFAULT.equals(setModifier);
			case TggPackage.EDGE__LINK_CONSTRAINT_TYPE:
				return linkConstraintType != LINK_CONSTRAINT_TYPE_EDEFAULT;
			case TggPackage.EDGE__DIRECT_SUCCESSOR:
				return directSuccessor != null;
			case TggPackage.EDGE__INDIRECT_SUCCESSOR:
				return indirectSuccessor != null && !indirectSuccessor.isEmpty();
			case TggPackage.EDGE__INDEX_EXPRESSION:
				return INDEX_EXPRESSION_EDEFAULT == null ? indexExpression != null : !INDEX_EXPRESSION_EDEFAULT.equals(indexExpression);
			case TggPackage.EDGE__DIRECT_PREDECESSOR:
				return directPredecessor != null;
			case TggPackage.EDGE__INDIRECT_PREDECESSOR:
				return indirectPredecessor != null && !indirectPredecessor.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString() + ":" + getTypedName();
		return "EdgeImpl@" + hashCode() + " - "
				+ (this.getName() != null ? this.getName() + ":" + getTypedName() : ":" + getTypedName()) + "["
				+ (this.getSourceNode() != null ? this.getSourceNode().getName() : "<sourceUnknown>") + "->"
				+ (this.getTargetNode() != null ? this.getTargetNode().getName() : "<targetUnknown>") + "]";
	}

	@Override
	public boolean isRight() {
		if (getSourceNode() == null || getSourceNode().getGraph() == null) {
			// this does not happen during the normal application of the 
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc., 
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return false;
		}
		if (((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern() != null
				&& ((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern().getEdge().contains(this)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isLeft() {
		if (getSourceNode() == null || getSourceNode().getGraph() == null) {
			// this does not happen during the normal application of the 
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc., 
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return false;
		}
		if (((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern() != null
				&& ((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern().getEdge().contains(this)) {
			return true;
		}

		return false;
	}

	@Override
	public void setLeft(boolean newLeft) {
		if (getSourceNode() == null || getSourceNode().getGraph() == null
				|| ((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern() == null
				|| ((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern() == null) {
			//super.setLeft(newLeft);
		} else {
			if (newLeft) {
				setRight(true);
			}
			if (newLeft
					&& !(((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern().getEdge().contains(this))) {
				((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern().getEdge().add(this);
			}
			if (!newLeft
					&& (((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern().getEdge().contains(this))) {
				((GraphGrammarRule) getSourceNode().getGraph()).getLeftGraphPattern().getEdge().remove(this);
			}
		}

	}

	@Override
	public void setRight(boolean newRight) {
		if (getSourceNode() == null || getSourceNode().getGraph() == null
				|| ((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern() == null) {
			//super.setRight(newRight);
		} else {
			if (newRight
					&& !((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern().getEdge().contains(this)) {
				((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern().getEdge().add(this);
			}
			if (!newRight
					&& ((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern().getEdge().contains(this)) {
				((GraphGrammarRule) getSourceNode().getGraph()).getRightGraphPattern().getEdge().remove(this);
			}
		}

	}

} //EdgeImpl