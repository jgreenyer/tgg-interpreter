/**
 * <copyright>
 * </copyright>
 *
 * $Id: Edge.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getTypeReference <em>Type Reference</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getSourceNode <em>Source Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getTargetNode <em>Target Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getGraphPattern <em>Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getTypedName <em>Typed Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getSetModifier <em>Set Modifier</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getLinkConstraintType <em>Link Constraint Type</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getDirectSuccessor <em>Direct Successor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getIndirectSuccessor <em>Indirect Successor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getIndexExpression <em>Index Expression</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getDirectPredecessor <em>Direct Predecessor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Edge#getIndirectPredecessor <em>Indirect Predecessor</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='EdgeMustHaveTypeThatIsAReferenceOfSourceNode\r\nSuccessorsMustHaveSameSourceNodeAndSameReferenceType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL EdgeMustHaveTypeThatIsAReferenceOfSourceNode='self.sourceNode.eType.eAllReferences->includes(self.typeReference) or self.typeReference.name=\'EObject\'' SuccessorsMustHaveSameSourceNodeAndSameReferenceType='(self.directSuccessor = null or self.typeReference = self.directSuccessor.typeReference) and (indirectSuccessor->forAll(self.typeReference = typeReference))'"
 * @generated
 */
public interface Edge extends GraphElement {
	/**
	 * Returns the value of the '<em><b>Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Reference</em>' reference.
	 * @see #setTypeReference(EReference)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_TypeReference()
	 * @model required="true"
	 * @generated
	 */
	EReference getTypeReference();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getTypeReference <em>Type Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Reference</em>' reference.
	 * @see #getTypeReference()
	 * @generated
	 */
	void setTypeReference(EReference value);

	/**
	 * Returns the value of the '<em><b>Source Node</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Node#getOutgoingEdge <em>Outgoing Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Node</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Node</em>' container reference.
	 * @see #setSourceNode(Node)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_SourceNode()
	 * @see de.upb.swt.qvt.tgg.Node#getOutgoingEdge
	 * @model opposite="outgoingEdge" required="true" transient="false"
	 * @generated
	 */
	Node getSourceNode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getSourceNode <em>Source Node</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Node</em>' container reference.
	 * @see #getSourceNode()
	 * @generated
	 */
	void setSourceNode(Node value);

	/**
	 * Returns the value of the '<em><b>Target Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Node#getIncomingEdge <em>Incoming Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Node</em>' reference.
	 * @see #setTargetNode(Node)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_TargetNode()
	 * @see de.upb.swt.qvt.tgg.Node#getIncomingEdge
	 * @model opposite="incomingEdge" required="true"
	 * @generated
	 */
	Node getTargetNode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getTargetNode <em>Target Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Node</em>' reference.
	 * @see #getTargetNode()
	 * @generated
	 */
	void setTargetNode(Node value);

	/**
	 * Returns the value of the '<em><b>Graph Pattern</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.GraphPattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.GraphPattern#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Pattern</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Pattern</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_GraphPattern()
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getEdge
	 * @model opposite="edge"
	 * @generated
	 */
	EList<GraphPattern> getGraphPattern();

	/**
	 * Returns the value of the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Name</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_TypedName()
	 * @model transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	String getTypedName();

	/**
	 * Returns the value of the '<em><b>Set Modifier</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Modifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Modifier</em>' attribute.
	 * @see #setSetModifier(String)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_SetModifier()
	 * @model default=""
	 * @generated
	 */
	String getSetModifier();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getSetModifier <em>Set Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Set Modifier</em>' attribute.
	 * @see #getSetModifier()
	 * @generated
	 */
	void setSetModifier(String value);

	/**
	 * Returns the value of the '<em><b>Link Constraint Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.swt.qvt.tgg.LinkConstraintType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Constraint Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Constraint Type</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.LinkConstraintType
	 * @see #setLinkConstraintType(LinkConstraintType)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_LinkConstraintType()
	 * @model
	 * @generated
	 */
	LinkConstraintType getLinkConstraintType();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getLinkConstraintType <em>Link Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Constraint Type</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.LinkConstraintType
	 * @see #getLinkConstraintType()
	 * @generated
	 */
	void setLinkConstraintType(LinkConstraintType value);

	/**
	 * Returns the value of the '<em><b>Direct Successor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getDirectPredecessor <em>Direct Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Successor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Successor</em>' reference.
	 * @see #setDirectSuccessor(Edge)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_DirectSuccessor()
	 * @see de.upb.swt.qvt.tgg.Edge#getDirectPredecessor
	 * @model opposite="directPredecessor"
	 * @generated
	 */
	Edge getDirectSuccessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getDirectSuccessor <em>Direct Successor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Successor</em>' reference.
	 * @see #getDirectSuccessor()
	 * @generated
	 */
	void setDirectSuccessor(Edge value);

	/**
	 * Returns the value of the '<em><b>Indirect Successor</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getIndirectPredecessor <em>Indirect Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indirect Successor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indirect Successor</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_IndirectSuccessor()
	 * @see de.upb.swt.qvt.tgg.Edge#getIndirectPredecessor
	 * @model opposite="indirectPredecessor"
	 * @generated
	 */
	EList<Edge> getIndirectSuccessor();

	/**
	 * Returns the value of the '<em><b>Index Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Expression</em>' attribute.
	 * @see #setIndexExpression(String)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_IndexExpression()
	 * @model
	 * @generated
	 */
	String getIndexExpression();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getIndexExpression <em>Index Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Expression</em>' attribute.
	 * @see #getIndexExpression()
	 * @generated
	 */
	void setIndexExpression(String value);

	/**
	 * Returns the value of the '<em><b>Direct Predecessor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getDirectSuccessor <em>Direct Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Predecessor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Predecessor</em>' reference.
	 * @see #setDirectPredecessor(Edge)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_DirectPredecessor()
	 * @see de.upb.swt.qvt.tgg.Edge#getDirectSuccessor
	 * @model opposite="directSuccessor"
	 * @generated
	 */
	Edge getDirectPredecessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Edge#getDirectPredecessor <em>Direct Predecessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Predecessor</em>' reference.
	 * @see #getDirectPredecessor()
	 * @generated
	 */
	void setDirectPredecessor(Edge value);

	/**
	 * Returns the value of the '<em><b>Indirect Predecessor</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Edge}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Edge#getIndirectSuccessor <em>Indirect Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indirect Predecessor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indirect Predecessor</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getEdge_IndirectPredecessor()
	 * @see de.upb.swt.qvt.tgg.Edge#getIndirectSuccessor
	 * @model opposite="indirectSuccessor"
	 * @generated
	 */
	EList<Edge> getIndirectPredecessor();

} // Edge