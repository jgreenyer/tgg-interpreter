/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstraintImpl.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.qvtbase.impl.AnnotatedElementImpl;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.ConstraintImpl#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.ConstraintImpl#getGraphPattern <em>Graph Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ConstraintImpl extends AnnotatedElementImpl implements Constraint {
	/**
	 * The cached value of the '{@link #getGraphPattern() <em>Graph Pattern</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphPattern()
	 * @generated
	 * @ordered
	 */
	protected GraphPattern graphPattern;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule getTripleGraphGrammarRule() {
		if (eContainerFeatureID() != TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE) return null;
		return (TripleGraphGrammarRule)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTripleGraphGrammarRule(TripleGraphGrammarRule newTripleGraphGrammarRule,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTripleGraphGrammarRule, TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTripleGraphGrammarRule(TripleGraphGrammarRule newTripleGraphGrammarRule) {
		if (newTripleGraphGrammarRule != eInternalContainer() || (eContainerFeatureID() != TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE && newTripleGraphGrammarRule != null)) {
			if (EcoreUtil.isAncestor(this, newTripleGraphGrammarRule))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTripleGraphGrammarRule != null)
				msgs = ((InternalEObject)newTripleGraphGrammarRule).eInverseAdd(this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT, TripleGraphGrammarRule.class, msgs);
			msgs = basicSetTripleGraphGrammarRule(newTripleGraphGrammarRule, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE, newTripleGraphGrammarRule, newTripleGraphGrammarRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphPattern getGraphPattern() {
		if (graphPattern != null && graphPattern.eIsProxy()) {
			InternalEObject oldGraphPattern = (InternalEObject)graphPattern;
			graphPattern = (GraphPattern)eResolveProxy(oldGraphPattern);
			if (graphPattern != oldGraphPattern) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONSTRAINT__GRAPH_PATTERN, oldGraphPattern, graphPattern));
			}
		}
		return graphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphPattern basicGetGraphPattern() {
		return graphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphPattern(GraphPattern newGraphPattern, NotificationChain msgs) {
		GraphPattern oldGraphPattern = graphPattern;
		graphPattern = newGraphPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.CONSTRAINT__GRAPH_PATTERN, oldGraphPattern, newGraphPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphPattern(GraphPattern newGraphPattern) {
		if (newGraphPattern != graphPattern) {
			NotificationChain msgs = null;
			if (graphPattern != null)
				msgs = ((InternalEObject)graphPattern).eInverseRemove(this, TggPackage.GRAPH_PATTERN__CONSTRAINT, GraphPattern.class, msgs);
			if (newGraphPattern != null)
				msgs = ((InternalEObject)newGraphPattern).eInverseAdd(this, TggPackage.GRAPH_PATTERN__CONSTRAINT, GraphPattern.class, msgs);
			msgs = basicSetGraphPattern(newGraphPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONSTRAINT__GRAPH_PATTERN, newGraphPattern, newGraphPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTripleGraphGrammarRule((TripleGraphGrammarRule)otherEnd, msgs);
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				if (graphPattern != null)
					msgs = ((InternalEObject)graphPattern).eInverseRemove(this, TggPackage.GRAPH_PATTERN__CONSTRAINT, GraphPattern.class, msgs);
				return basicSetGraphPattern((GraphPattern)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				return basicSetTripleGraphGrammarRule(null, msgs);
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				return basicSetGraphPattern(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				return eInternalContainer().eInverseRemove(this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT, TripleGraphGrammarRule.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				return getTripleGraphGrammarRule();
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				if (resolve) return getGraphPattern();
				return basicGetGraphPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				setTripleGraphGrammarRule((TripleGraphGrammarRule)newValue);
				return;
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				setGraphPattern((GraphPattern)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				setTripleGraphGrammarRule((TripleGraphGrammarRule)null);
				return;
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				setGraphPattern((GraphPattern)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE:
				return getTripleGraphGrammarRule() != null;
			case TggPackage.CONSTRAINT__GRAPH_PATTERN:
				return graphPattern != null;
		}
		return super.eIsSet(featureID);
	}

} //ConstraintImpl