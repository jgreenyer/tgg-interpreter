/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggPackageImpl.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.ITransformAttribute;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.LinkConstraintType;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.util.TggValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TggPackageImpl extends EPackageImpl implements TggPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tripleGraphGrammarRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphGrammarRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leftGraphPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rightGraphPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass domainGraphPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tripleGraphGrammarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTransformAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oclConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tripleGraphGrammarAxiomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reusablePatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum linkConstraintTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.swt.qvt.tgg.TggPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TggPackageImpl() {
		super(eNS_URI, TggFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TggPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TggPackage init() {
		if (isInited) return (TggPackage)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI);

		// Obtain or create and register package
		TggPackageImpl theTggPackage = (TggPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TggPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TggPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		QvtbasePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTggPackage.createPackageContents();

		// Initialize created meta-data
		theTggPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theTggPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return TggValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theTggPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TggPackage.eNS_URI, theTggPackage);
		return theTggPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraph() {
		return graphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraph_Node() {
		return (EReference)graphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTripleGraphGrammarRule() {
		return tripleGraphGrammarRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammarRule_DomainsLeftSideNodesMap() {
		return (EAttribute)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammarRule_DomainsNodesMap() {
		return (EAttribute)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammarRule_TypedModelToDomainsMap() {
		return (EAttribute)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTripleGraphGrammarRule_Constraint() {
		return (EReference)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammarRule_Abstract() {
		return (EAttribute)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTripleGraphGrammarRule_AllConstraints() {
		return (EReference)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTripleGraphGrammarRule_RulesAdvisedToApplyNext() {
		return (EReference)tripleGraphGrammarRuleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphGrammarRule() {
		return graphGrammarRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_LeftGraphPattern() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_RightGraphPattern() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_ContextNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_ProducedNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_NonproducedNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_ReusableNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_RefinedRule() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllContextNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllProducedNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllNonproducedNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllReusableNodes() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_AllRefinedRules() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphGrammarRule_ReusablePattern() {
		return (EReference)graphGrammarRuleEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphPattern() {
		return graphPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphPattern_Node() {
		return (EReference)graphPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphPattern_Constraint() {
		return (EReference)graphPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphPattern_Edge() {
		return (EReference)graphPatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNode() {
		return nodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_OutgoingEdge() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_IncomingEdge() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_Graph() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_GraphPattern() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_MatchSubtype() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_EType() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_RefinedNode() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_AllOutgoingEdges() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_AllIncomingEdges() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_AllRefinedNodes() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_AllGraphPatterns() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_MatchingPriority() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_TypedName() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEdge() {
		return edgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_TypeReference() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_SourceNode() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_TargetNode() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_GraphPattern() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdge_TypedName() {
		return (EAttribute)edgeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdge_SetModifier() {
		return (EAttribute)edgeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdge_LinkConstraintType() {
		return (EAttribute)edgeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_DirectSuccessor() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_IndirectSuccessor() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdge_IndexExpression() {
		return (EAttribute)edgeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_DirectPredecessor() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdge_IndirectPredecessor() {
		return (EReference)edgeEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeftGraphPattern() {
		return leftGraphPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeftGraphPattern_GraphGrammar() {
		return (EReference)leftGraphPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRightGraphPattern() {
		return rightGraphPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRightGraphPattern_GraphGrammar() {
		return (EReference)rightGraphPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDomainGraphPattern() {
		return domainGraphPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTripleGraphGrammar() {
		return tripleGraphGrammarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammar_RelevantReferences() {
		return (EAttribute)tripleGraphGrammarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTripleGraphGrammar_TypeClassToRuleMap() {
		return (EAttribute)tripleGraphGrammarEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTripleGraphGrammar_Axiom() {
		return (EReference)tripleGraphGrammarEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstraint() {
		return constraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraint_TripleGraphGrammarRule() {
		return (EReference)constraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraint_GraphPattern() {
		return (EReference)constraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGConstraint() {
		return tggConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGConstraint_SlotNode() {
		return (EReference)tggConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGConstraint_SlotAttribute() {
		return (EReference)tggConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGConstraint_SlotAttributeName() {
		return (EAttribute)tggConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGConstraint_Checkonly() {
		return (EAttribute)tggConstraintEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGConstraint_EvaluateAfterMatching() {
		return (EAttribute)tggConstraintEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphElement() {
		return graphElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphElement_Left() {
		return (EAttribute)graphElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphElement_Right() {
		return (EAttribute)graphElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getITransformAttribute() {
		return iTransformAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLConstraint() {
		return oclConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOCLConstraint_Expression() {
		return (EAttribute)oclConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTripleGraphGrammarAxiom() {
		return tripleGraphGrammarAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReusablePattern() {
		return reusablePatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReusablePattern_GraphGrammar() {
		return (EReference)reusablePatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLinkConstraintType() {
		return linkConstraintTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggFactory getTggFactory() {
		return (TggFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		graphEClass = createEClass(GRAPH);
		createEReference(graphEClass, GRAPH__NODE);

		tripleGraphGrammarRuleEClass = createEClass(TRIPLE_GRAPH_GRAMMAR_RULE);
		createEAttribute(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP);
		createEAttribute(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP);
		createEAttribute(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP);
		createEReference(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT);
		createEAttribute(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT);
		createEReference(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS);
		createEReference(tripleGraphGrammarRuleEClass, TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT);

		graphGrammarRuleEClass = createEClass(GRAPH_GRAMMAR_RULE);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__LEFT_GRAPH_PATTERN);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__RIGHT_GRAPH_PATTERN);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__CONTEXT_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__PRODUCED_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__NONPRODUCED_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__REUSABLE_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__REFINED_RULE);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_CONTEXT_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_PRODUCED_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_NONPRODUCED_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_REUSABLE_NODES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__ALL_REFINED_RULES);
		createEReference(graphGrammarRuleEClass, GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN);

		graphPatternEClass = createEClass(GRAPH_PATTERN);
		createEReference(graphPatternEClass, GRAPH_PATTERN__NODE);
		createEReference(graphPatternEClass, GRAPH_PATTERN__CONSTRAINT);
		createEReference(graphPatternEClass, GRAPH_PATTERN__EDGE);

		nodeEClass = createEClass(NODE);
		createEReference(nodeEClass, NODE__OUTGOING_EDGE);
		createEReference(nodeEClass, NODE__INCOMING_EDGE);
		createEReference(nodeEClass, NODE__GRAPH);
		createEReference(nodeEClass, NODE__GRAPH_PATTERN);
		createEAttribute(nodeEClass, NODE__TYPED_NAME);
		createEAttribute(nodeEClass, NODE__MATCH_SUBTYPE);
		createEReference(nodeEClass, NODE__ETYPE);
		createEReference(nodeEClass, NODE__REFINED_NODE);
		createEReference(nodeEClass, NODE__ALL_OUTGOING_EDGES);
		createEReference(nodeEClass, NODE__ALL_INCOMING_EDGES);
		createEReference(nodeEClass, NODE__ALL_REFINED_NODES);
		createEReference(nodeEClass, NODE__ALL_GRAPH_PATTERNS);
		createEAttribute(nodeEClass, NODE__MATCHING_PRIORITY);

		edgeEClass = createEClass(EDGE);
		createEReference(edgeEClass, EDGE__TYPE_REFERENCE);
		createEReference(edgeEClass, EDGE__SOURCE_NODE);
		createEReference(edgeEClass, EDGE__TARGET_NODE);
		createEReference(edgeEClass, EDGE__GRAPH_PATTERN);
		createEAttribute(edgeEClass, EDGE__TYPED_NAME);
		createEAttribute(edgeEClass, EDGE__SET_MODIFIER);
		createEAttribute(edgeEClass, EDGE__LINK_CONSTRAINT_TYPE);
		createEReference(edgeEClass, EDGE__DIRECT_SUCCESSOR);
		createEReference(edgeEClass, EDGE__INDIRECT_SUCCESSOR);
		createEAttribute(edgeEClass, EDGE__INDEX_EXPRESSION);
		createEReference(edgeEClass, EDGE__DIRECT_PREDECESSOR);
		createEReference(edgeEClass, EDGE__INDIRECT_PREDECESSOR);

		leftGraphPatternEClass = createEClass(LEFT_GRAPH_PATTERN);
		createEReference(leftGraphPatternEClass, LEFT_GRAPH_PATTERN__GRAPH_GRAMMAR);

		rightGraphPatternEClass = createEClass(RIGHT_GRAPH_PATTERN);
		createEReference(rightGraphPatternEClass, RIGHT_GRAPH_PATTERN__GRAPH_GRAMMAR);

		domainGraphPatternEClass = createEClass(DOMAIN_GRAPH_PATTERN);

		tripleGraphGrammarEClass = createEClass(TRIPLE_GRAPH_GRAMMAR);
		createEAttribute(tripleGraphGrammarEClass, TRIPLE_GRAPH_GRAMMAR__RELEVANT_REFERENCES);
		createEAttribute(tripleGraphGrammarEClass, TRIPLE_GRAPH_GRAMMAR__TYPE_CLASS_TO_RULE_MAP);
		createEReference(tripleGraphGrammarEClass, TRIPLE_GRAPH_GRAMMAR__AXIOM);

		constraintEClass = createEClass(CONSTRAINT);
		createEReference(constraintEClass, CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE);
		createEReference(constraintEClass, CONSTRAINT__GRAPH_PATTERN);

		tggConstraintEClass = createEClass(TGG_CONSTRAINT);
		createEReference(tggConstraintEClass, TGG_CONSTRAINT__SLOT_NODE);
		createEReference(tggConstraintEClass, TGG_CONSTRAINT__SLOT_ATTRIBUTE);
		createEAttribute(tggConstraintEClass, TGG_CONSTRAINT__SLOT_ATTRIBUTE_NAME);
		createEAttribute(tggConstraintEClass, TGG_CONSTRAINT__CHECKONLY);
		createEAttribute(tggConstraintEClass, TGG_CONSTRAINT__EVALUATE_AFTER_MATCHING);

		graphElementEClass = createEClass(GRAPH_ELEMENT);
		createEAttribute(graphElementEClass, GRAPH_ELEMENT__LEFT);
		createEAttribute(graphElementEClass, GRAPH_ELEMENT__RIGHT);

		iTransformAttributeEClass = createEClass(ITRANSFORM_ATTRIBUTE);

		oclConstraintEClass = createEClass(OCL_CONSTRAINT);
		createEAttribute(oclConstraintEClass, OCL_CONSTRAINT__EXPRESSION);

		tripleGraphGrammarAxiomEClass = createEClass(TRIPLE_GRAPH_GRAMMAR_AXIOM);

		reusablePatternEClass = createEClass(REUSABLE_PATTERN);
		createEReference(reusablePatternEClass, REUSABLE_PATTERN__GRAPH_GRAMMAR);

		// Create enums
		linkConstraintTypeEEnum = createEEnum(LINK_CONSTRAINT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		QvtbasePackage theQvtbasePackage = (QvtbasePackage)EPackage.Registry.INSTANCE.getEPackage(QvtbasePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		graphEClass.getESuperTypes().add(theQvtbasePackage.getNamedElement());
		tripleGraphGrammarRuleEClass.getESuperTypes().add(this.getGraphGrammarRule());
		tripleGraphGrammarRuleEClass.getESuperTypes().add(theQvtbasePackage.getRule());
		graphGrammarRuleEClass.getESuperTypes().add(this.getGraph());
		nodeEClass.getESuperTypes().add(this.getGraphElement());
		edgeEClass.getESuperTypes().add(this.getGraphElement());
		leftGraphPatternEClass.getESuperTypes().add(this.getGraphPattern());
		rightGraphPatternEClass.getESuperTypes().add(this.getGraphPattern());
		domainGraphPatternEClass.getESuperTypes().add(theQvtbasePackage.getDomain());
		domainGraphPatternEClass.getESuperTypes().add(this.getGraphPattern());
		tripleGraphGrammarEClass.getESuperTypes().add(theQvtbasePackage.getTransformation());
		constraintEClass.getESuperTypes().add(theQvtbasePackage.getAnnotatedElement());
		tggConstraintEClass.getESuperTypes().add(this.getConstraint());
		graphElementEClass.getESuperTypes().add(theQvtbasePackage.getNamedElement());
		oclConstraintEClass.getESuperTypes().add(this.getTGGConstraint());
		tripleGraphGrammarAxiomEClass.getESuperTypes().add(this.getTripleGraphGrammarRule());
		reusablePatternEClass.getESuperTypes().add(this.getGraphPattern());

		// Initialize classes and features; add operations and parameters
		initEClass(graphEClass, Graph.class, "Graph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraph_Node(), this.getNode(), this.getNode_Graph(), "node", null, 0, -1, Graph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tripleGraphGrammarRuleEClass, TripleGraphGrammarRule.class, "TripleGraphGrammarRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(theEcorePackage.getEMap());
		EGenericType g2 = createEGenericType(theQvtbasePackage.getTypedModel());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(this.getNode());
		g2.getETypeArguments().add(g3);
		initEAttribute(getTripleGraphGrammarRule_DomainsLeftSideNodesMap(), g1, "domainsLeftSideNodesMap", null, 0, 1, TripleGraphGrammarRule.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theQvtbasePackage.getTypedModel());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		g3 = createEGenericType(this.getNode());
		g2.getETypeArguments().add(g3);
		initEAttribute(getTripleGraphGrammarRule_DomainsNodesMap(), g1, "domainsNodesMap", null, 0, 1, TripleGraphGrammarRule.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theQvtbasePackage.getTypedModel());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theQvtbasePackage.getDomain());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTripleGraphGrammarRule_TypedModelToDomainsMap(), g1, "typedModelToDomainsMap", null, 0, 1, TripleGraphGrammarRule.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTripleGraphGrammarRule_Constraint(), this.getConstraint(), this.getConstraint_TripleGraphGrammarRule(), "constraint", null, 0, -1, TripleGraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTripleGraphGrammarRule_Abstract(), theEcorePackage.getEBoolean(), "abstract", null, 0, 1, TripleGraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTripleGraphGrammarRule_AllConstraints(), this.getConstraint(), null, "allConstraints", null, 0, -1, TripleGraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTripleGraphGrammarRule_RulesAdvisedToApplyNext(), this.getTripleGraphGrammarRule(), null, "rulesAdvisedToApplyNext", null, 0, -1, TripleGraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(tripleGraphGrammarRuleEClass, this.getDomainGraphPattern(), "getNodesDomainGraphPattern", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tripleGraphGrammarRuleEClass, this.getDomainGraphPattern(), "getDomainGraphPattern", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQvtbasePackage.getTypedModel(), "typedModel", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tripleGraphGrammarRuleEClass, null, "getDomainsNodes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theQvtbasePackage.getDomain(), "domain", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(graphGrammarRuleEClass, GraphGrammarRule.class, "GraphGrammarRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraphGrammarRule_LeftGraphPattern(), this.getLeftGraphPattern(), this.getLeftGraphPattern_GraphGrammar(), "leftGraphPattern", null, 1, 1, GraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_RightGraphPattern(), this.getRightGraphPattern(), this.getRightGraphPattern_GraphGrammar(), "rightGraphPattern", null, 1, 1, GraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_ContextNodes(), this.getNode(), null, "contextNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_ProducedNodes(), this.getNode(), null, "producedNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_NonproducedNodes(), this.getNode(), null, "nonproducedNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_ReusableNodes(), this.getNode(), null, "reusableNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_RefinedRule(), this.getGraphGrammarRule(), null, "refinedRule", null, 0, 1, GraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllNodes(), this.getNode(), null, "allNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllContextNodes(), this.getNode(), null, "allContextNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllProducedNodes(), this.getNode(), null, "allProducedNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllNonproducedNodes(), this.getNode(), null, "allNonproducedNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllReusableNodes(), this.getNode(), null, "allReusableNodes", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_AllRefinedRules(), this.getGraphGrammarRule(), null, "allRefinedRules", null, 0, -1, GraphGrammarRule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getGraphGrammarRule_ReusablePattern(), this.getReusablePattern(), this.getReusablePattern_GraphGrammar(), "reusablePattern", null, 0, -1, GraphGrammarRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(graphGrammarRuleEClass, this.getNode(), "getMostRefinedNodeOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(graphPatternEClass, GraphPattern.class, "GraphPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraphPattern_Node(), this.getNode(), this.getNode_GraphPattern(), "node", null, 0, -1, GraphPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphPattern_Constraint(), this.getConstraint(), this.getConstraint_GraphPattern(), "constraint", null, 0, -1, GraphPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphPattern_Edge(), this.getEdge(), this.getEdge_GraphPattern(), "edge", null, 0, -1, GraphPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeEClass, Node.class, "Node", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNode_OutgoingEdge(), this.getEdge(), this.getEdge_SourceNode(), "outgoingEdge", null, 0, -1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_IncomingEdge(), this.getEdge(), this.getEdge_TargetNode(), "incomingEdge", null, 0, -1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_Graph(), this.getGraph(), this.getGraph_Node(), "graph", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_GraphPattern(), this.getGraphPattern(), this.getGraphPattern_Node(), "graphPattern", null, 0, -1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_TypedName(), theEcorePackage.getEString(), "typedName", "", 0, 1, Node.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_MatchSubtype(), theEcorePackage.getEBoolean(), "matchSubtype", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_EType(), theEcorePackage.getEClass(), null, "eType", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_RefinedNode(), this.getNode(), null, "refinedNode", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_AllOutgoingEdges(), this.getEdge(), null, "allOutgoingEdges", null, 0, -1, Node.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNode_AllIncomingEdges(), this.getEdge(), null, "allIncomingEdges", null, 0, -1, Node.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNode_AllRefinedNodes(), this.getNode(), null, "allRefinedNodes", null, 0, -1, Node.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNode_AllGraphPatterns(), this.getGraphPattern(), null, "allGraphPatterns", null, 0, -1, Node.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_MatchingPriority(), theEcorePackage.getEInt(), "matchingPriority", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(nodeEClass, this.getDomainGraphPattern(), "getDomainGraphPattern", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(edgeEClass, Edge.class, "Edge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEdge_TypeReference(), theEcorePackage.getEReference(), null, "typeReference", null, 1, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_SourceNode(), this.getNode(), this.getNode_OutgoingEdge(), "sourceNode", null, 1, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_TargetNode(), this.getNode(), this.getNode_IncomingEdge(), "targetNode", null, 1, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_GraphPattern(), this.getGraphPattern(), this.getGraphPattern_Edge(), "graphPattern", null, 0, -1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdge_TypedName(), theEcorePackage.getEString(), "typedName", null, 0, 1, Edge.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdge_SetModifier(), theEcorePackage.getEString(), "setModifier", "", 0, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdge_LinkConstraintType(), this.getLinkConstraintType(), "linkConstraintType", null, 0, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_DirectSuccessor(), this.getEdge(), this.getEdge_DirectPredecessor(), "directSuccessor", null, 0, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_IndirectSuccessor(), this.getEdge(), this.getEdge_IndirectPredecessor(), "indirectSuccessor", null, 0, -1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdge_IndexExpression(), theEcorePackage.getEString(), "indexExpression", null, 0, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_DirectPredecessor(), this.getEdge(), this.getEdge_DirectSuccessor(), "directPredecessor", null, 0, 1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEdge_IndirectPredecessor(), this.getEdge(), this.getEdge_IndirectSuccessor(), "indirectPredecessor", null, 0, -1, Edge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leftGraphPatternEClass, LeftGraphPattern.class, "LeftGraphPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeftGraphPattern_GraphGrammar(), this.getGraphGrammarRule(), this.getGraphGrammarRule_LeftGraphPattern(), "graphGrammar", null, 1, 1, LeftGraphPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rightGraphPatternEClass, RightGraphPattern.class, "RightGraphPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRightGraphPattern_GraphGrammar(), this.getGraphGrammarRule(), this.getGraphGrammarRule_RightGraphPattern(), "graphGrammar", null, 1, 1, RightGraphPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(domainGraphPatternEClass, DomainGraphPattern.class, "DomainGraphPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tripleGraphGrammarEClass, TripleGraphGrammar.class, "TripleGraphGrammar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEReference());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTripleGraphGrammar_RelevantReferences(), g1, "relevantReferences", null, 0, 1, TripleGraphGrammar.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theEcorePackage.getEClassifier());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		g3 = createEGenericType(theQvtbasePackage.getRule());
		g2.getETypeArguments().add(g3);
		initEAttribute(getTripleGraphGrammar_TypeClassToRuleMap(), g1, "typeClassToRuleMap", null, 0, 1, TripleGraphGrammar.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTripleGraphGrammar_Axiom(), this.getTripleGraphGrammarAxiom(), null, "axiom", null, 0, 1, TripleGraphGrammar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constraintEClass, Constraint.class, "Constraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstraint_TripleGraphGrammarRule(), this.getTripleGraphGrammarRule(), this.getTripleGraphGrammarRule_Constraint(), "tripleGraphGrammarRule", null, 0, 1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConstraint_GraphPattern(), this.getGraphPattern(), this.getGraphPattern_Constraint(), "graphPattern", null, 0, 1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tggConstraintEClass, TGGConstraint.class, "TGGConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGConstraint_SlotNode(), this.getNode(), null, "slotNode", null, 0, 1, TGGConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGConstraint_SlotAttribute(), theEcorePackage.getEAttribute(), null, "slotAttribute", null, 0, 1, TGGConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGConstraint_SlotAttributeName(), theEcorePackage.getEString(), "slotAttributeName", null, 0, 1, TGGConstraint.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGConstraint_Checkonly(), theEcorePackage.getEBoolean(), "checkonly", null, 0, 1, TGGConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGConstraint_EvaluateAfterMatching(), ecorePackage.getEBoolean(), "evaluateAfterMatching", null, 0, 1, TGGConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(graphElementEClass, GraphElement.class, "GraphElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGraphElement_Left(), theEcorePackage.getEBoolean(), "left", null, 0, 1, GraphElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphElement_Right(), theEcorePackage.getEBoolean(), "right", null, 0, 1, GraphElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(graphElementEClass, theEcorePackage.getEBoolean(), "isContext", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(graphElementEClass, theEcorePackage.getEBoolean(), "isProduced", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(graphElementEClass, theEcorePackage.getEBoolean(), "isReusable", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iTransformAttributeEClass, ITransformAttribute.class, "ITransformAttribute", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(iTransformAttributeEClass, theEcorePackage.getEString(), "transform", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(oclConstraintEClass, OCLConstraint.class, "OCLConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOCLConstraint_Expression(), theEcorePackage.getEString(), "expression", null, 0, 1, OCLConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(oclConstraintEClass, ecorePackage.getEString(), "analyseConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tripleGraphGrammarAxiomEClass, TripleGraphGrammarAxiom.class, "TripleGraphGrammarAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(reusablePatternEClass, ReusablePattern.class, "ReusablePattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReusablePattern_GraphGrammar(), this.getGraphGrammarRule(), this.getGraphGrammarRule_ReusablePattern(), "graphGrammar", null, 0, 1, ReusablePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(linkConstraintTypeEEnum, LinkConstraintType.class, "LinkConstraintType");
		addEEnumLiteral(linkConstraintTypeEEnum, LinkConstraintType.UNDEFINED);
		addEEnumLiteral(linkConstraintTypeEEnum, LinkConstraintType.FIRST);
		addEEnumLiteral(linkConstraintTypeEEnum, LinkConstraintType.NEITHER_FIRST_NOR_LAST);
		addEEnumLiteral(linkConstraintTypeEEnum, LinkConstraintType.LAST);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL"
		   });											
		addAnnotation
		  (nodeEClass, 
		   source, 
		   new String[] {
			 "constraints", "RefiningNodeOnSameGraphGrammarSideAsRefinedNode\r\nRefiningNodeInSameDomainAsRefiningNode\r\nRefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass\r\nNodeMustHaveDomainGraphPattern\r\nRefiningNodeMustHaveTheSameNameAsTheRefinedNode\r\nAtMostOneNodeRefiningTheSameOtherNode\r\nNoAbstractTypeForProducedNodes"
		   });							
		addAnnotation
		  (edgeEClass, 
		   source, 
		   new String[] {
			 "constraints", "EdgeMustHaveTypeThatIsAReferenceOfSourceNode\r\nSuccessorsMustHaveSameSourceNodeAndSameReferenceType"
		   });			
		addAnnotation
		  (tripleGraphGrammarEClass, 
		   source, 
		   new String[] {
			 "constraints", "RootTransformationMustHaveAnAxiom"
		   });			
		addAnnotation
		  (tggConstraintEClass, 
		   source, 
		   new String[] {
			 "constraints", "NoEnforcingWithoutSlotAttribute\r\nSlotNodeAttributeMustBeNullIfNoSlotNodeSet"
		   });			
		addAnnotation
		  (reusablePatternEClass, 
		   source, 
		   new String[] {
			 "constraints", "AllNodesMustBeReusable"
		   });	
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL";													
		addAnnotation
		  (nodeEClass, 
		   source, 
		   new String[] {
			 "NodeMustHaveDomainGraphPattern", "self.getDomainGraphPattern() <> null",
			 "RefiningNodeOnSameGraphGrammarSideAsRefinedNode", "self.refinedNode = null or (self.left = self.refinedNode.left and self.right = self.refinedNode.right)",
			 "RefiningNodeInSameDomainAsRefiningNode", "self.refinedNode = null or (self.getDomainGraphPattern().typedModel = self.refinedNode.getDomainGraphPattern().typedModel)",
			 "RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass", "self.refinedNode = null or (self.refinedNode.eType = self.eType or self.refinedNode.eType.isSuperTypeOf(self.eType))",
			 "RefiningNodeMustHaveTheSameNameAsTheRefinedNode", "self.refinedNode = null or (self.name = self.refinedNode.name)",
			 "AtMostOneNodeRefiningTheSameOtherNode", "self.refinedNode = null or self.graph.node->select(n| n <> self and  n.refinedNode <> null and n.refinedNode = self.refinedNode)->isEmpty()",
			 "NoAbstractTypeForProducedNodes", "(not self.left and self.right) implies not self.eType.abstract"
		   });							
		addAnnotation
		  (edgeEClass, 
		   source, 
		   new String[] {
			 "EdgeMustHaveTypeThatIsAReferenceOfSourceNode", "self.sourceNode.eType.eAllReferences->includes(self.typeReference) or self.typeReference.name=\'EObject\'",
			 "SuccessorsMustHaveSameSourceNodeAndSameReferenceType", "(self.directSuccessor = null or self.typeReference = self.directSuccessor.typeReference) and (indirectSuccessor->forAll(self.typeReference = typeReference))"
		   });			
		addAnnotation
		  (tripleGraphGrammarEClass, 
		   source, 
		   new String[] {
			 "RootTransformationMustHaveAnAxiom", "(axiom <> null) or (parentTransformation <> null)"
		   });			
		addAnnotation
		  (tggConstraintEClass, 
		   source, 
		   new String[] {
			 "NoEnforcingWithoutSlotAttribute", "not (not self.checkonly and self.slotAttribute = null and self.slotNode <> null and self.slotNode.isProduced())",
			 "SlotNodeAttributeMustBeNullIfNoSlotNodeSet", "self.slotNode <> null or self.slotAttribute = null"
		   });			
		addAnnotation
		  (reusablePatternEClass, 
		   source, 
		   new String[] {
			 "AllNodesMustBeReusable", "self.node->forAll(n|n.isReusable())"
		   });
	}

} //TggPackageImpl
