/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.util.TggResourceFactoryImpl
 * @generated
 */
public class TggResourceImpl extends XMIResourceImpl {

	/*
	 * Use Unique IDs because elements should be reorderd without loosing the reference. 
	 */
	@Override
	protected boolean useUUIDs() {
		return true;
	}

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public TggResourceImpl(URI uri) {
		super(uri);
	}

} //TggResourceImpl
