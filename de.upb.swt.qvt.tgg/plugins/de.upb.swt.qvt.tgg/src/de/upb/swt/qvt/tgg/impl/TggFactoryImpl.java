/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggFactoryImpl.java 1649 2006-07-07 09:22:27 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.LinkConstraintType;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TggFactoryImpl extends EFactoryImpl implements TggFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TggFactory init() {
		try {
			TggFactory theTggFactory = (TggFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.tgg"); 
			if (theTggFactory != null) {
				return theTggFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TggFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TggPackage.GRAPH: return createGraph();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE: return createTripleGraphGrammarRule();
			case TggPackage.GRAPH_GRAMMAR_RULE: return createGraphGrammarRule();
			case TggPackage.GRAPH_PATTERN: return createGraphPattern();
			case TggPackage.NODE: return createNode();
			case TggPackage.EDGE: return createEdge();
			case TggPackage.LEFT_GRAPH_PATTERN: return createLeftGraphPattern();
			case TggPackage.RIGHT_GRAPH_PATTERN: return createRightGraphPattern();
			case TggPackage.DOMAIN_GRAPH_PATTERN: return createDomainGraphPattern();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR: return createTripleGraphGrammar();
			case TggPackage.OCL_CONSTRAINT: return createOCLConstraint();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_AXIOM: return createTripleGraphGrammarAxiom();
			case TggPackage.REUSABLE_PATTERN: return createReusablePattern();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TggPackage.LINK_CONSTRAINT_TYPE:
				return createLinkConstraintTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TggPackage.LINK_CONSTRAINT_TYPE:
				return convertLinkConstraintTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Graph createGraph() {
		GraphImpl graph = new GraphImpl();
		return graph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule createTripleGraphGrammarRule() {
		TripleGraphGrammarRuleImpl tripleGraphGrammarRule = new TripleGraphGrammarRuleImpl();
		return tripleGraphGrammarRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphGrammarRule createGraphGrammarRule() {
		GraphGrammarRuleImpl graphGrammarRule = new GraphGrammarRuleImpl();
		return graphGrammarRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphPattern createGraphPattern() {
		GraphPatternImpl graphPattern = new GraphPatternImpl();
		return graphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node createNode() {
		NodeImpl node = new NodeImpl();
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge createEdge() {
		EdgeImpl edge = new EdgeImpl();
		return edge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeftGraphPattern createLeftGraphPattern() {
		LeftGraphPatternImpl leftGraphPattern = new LeftGraphPatternImpl();
		return leftGraphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RightGraphPattern createRightGraphPattern() {
		RightGraphPatternImpl rightGraphPattern = new RightGraphPatternImpl();
		return rightGraphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainGraphPattern createDomainGraphPattern() {
		DomainGraphPatternImpl domainGraphPattern = new DomainGraphPatternImpl();
		return domainGraphPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammar createTripleGraphGrammar() {
		TripleGraphGrammarImpl tripleGraphGrammar = new TripleGraphGrammarImpl();
		return tripleGraphGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLConstraint createOCLConstraint() {
		OCLConstraintImpl oclConstraint = new OCLConstraintImpl();
		return oclConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarAxiom createTripleGraphGrammarAxiom() {
		TripleGraphGrammarAxiomImpl tripleGraphGrammarAxiom = new TripleGraphGrammarAxiomImpl();
		return tripleGraphGrammarAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReusablePattern createReusablePattern() {
		ReusablePatternImpl reusablePattern = new ReusablePatternImpl();
		return reusablePattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkConstraintType createLinkConstraintTypeFromString(EDataType eDataType, String initialValue) {
		LinkConstraintType result = LinkConstraintType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLinkConstraintTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggPackage getTggPackage() {
		return (TggPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TggPackage getPackage() {
		return TggPackage.eINSTANCE;
	}

} //TggFactoryImpl
