/**
 * <copyright>
 * </copyright>
 *
 * $Id: TripleGraphGrammarRule.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import java.util.Map;

import org.eclipse.emf.common.util.EList;

import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.TypedModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triple Graph Grammar Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsLeftSideNodesMap <em>Domains Left Side Nodes Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsNodesMap <em>Domains Nodes Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getTypedModelToDomainsMap <em>Typed Model To Domains Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getAllConstraints <em>All Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getRulesAdvisedToApplyNext <em>Rules Advised To Apply Next</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule()
 * @model
 * @generated
 */
public interface TripleGraphGrammarRule extends GraphGrammarRule, Rule {
	/**
	 * Returns the value of the '<em><b>Domains Left Side Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domains Left Side Nodes Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domains Left Side Nodes Map</em>' attribute.
	 * @see #isSetDomainsLeftSideNodesMap()
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_DomainsLeftSideNodesMap()
	 * @model unsettable="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	Map<TypedModel, EList<Node>> getDomainsLeftSideNodesMap();

	/**
	 * Returns whether the value of the '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsLeftSideNodesMap <em>Domains Left Side Nodes Map</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domains Left Side Nodes Map</em>' attribute is set.
	 * @see #getDomainsLeftSideNodesMap()
	 * @generated
	 */
	boolean isSetDomainsLeftSideNodesMap();

	/**
	 * Returns the value of the '<em><b>Domains Nodes Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domains Nodes Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domains Nodes Map</em>' attribute.
	 * @see #isSetDomainsNodesMap()
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_DomainsNodesMap()
	 * @model unsettable="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	Map<TypedModel, EList<Node>> getDomainsNodesMap();

	/**
	 * Returns whether the value of the '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getDomainsNodesMap <em>Domains Nodes Map</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domains Nodes Map</em>' attribute is set.
	 * @see #getDomainsNodesMap()
	 * @generated
	 */
	boolean isSetDomainsNodesMap();

	/**
	 * Returns the value of the '<em><b>Typed Model To Domains Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Model To Domains Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Model To Domains Map</em>' attribute.
	 * @see #isSetTypedModelToDomainsMap()
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_TypedModelToDomainsMap()
	 * @model unsettable="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	Map<TypedModel, Domain> getTypedModelToDomainsMap();

	/**
	 * Returns whether the value of the '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getTypedModelToDomainsMap <em>Typed Model To Domains Map</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Typed Model To Domains Map</em>' attribute is set.
	 * @see #getTypedModelToDomainsMap()
	 * @generated
	 */
	boolean isSetTypedModelToDomainsMap();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Constraint}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_Constraint()
	 * @see de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule
	 * @model opposite="tripleGraphGrammarRule" containment="true"
	 * @generated
	 */
	EList<Constraint> getConstraint();

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_Abstract()
	 * @model
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>All Constraints</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Constraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Constraints</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains all constraints of the rule and all refined rules.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Constraints</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_AllConstraints()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Constraint> getAllConstraints();

	/**
	 * Returns the value of the '<em><b>Rules Advised To Apply Next</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules Advised To Apply Next</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Advised To Apply Next</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammarRule_RulesAdvisedToApplyNext()
	 * @model
	 * @generated
	 */
	EList<TripleGraphGrammarRule> getRulesAdvisedToApplyNext();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	DomainGraphPattern getNodesDomainGraphPattern(Node node);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	DomainGraphPattern getDomainGraphPattern(TypedModel typedModel);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model many="false"
	 * @generated
	 */
	EList<Node> getDomainsNodes(Domain domain);

} // TripleGraphGrammarRule