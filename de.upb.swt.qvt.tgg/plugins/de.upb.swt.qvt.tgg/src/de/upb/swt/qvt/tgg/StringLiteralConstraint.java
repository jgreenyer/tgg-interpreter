/**
 * <copyright>
 * </copyright>
 *
 * $Id: StringLiteralConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Literal Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.StringLiteralConstraint#getStringValue <em>String Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getStringLiteralConstraint()
 * @model
 * @generated
 */
public interface StringLiteralConstraint extends PrimitiveLiteralConstraint {
	/**
	 * Returns the value of the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Value</em>' attribute.
	 * @see #setStringValue(String)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getStringLiteralConstraint_StringValue()
	 * @model
	 * @generated
	 */
	String getStringValue();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.StringLiteralConstraint#getStringValue <em>String Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Value</em>' attribute.
	 * @see #getStringValue()
	 * @generated
	 */
	void setStringValue(String value);

} // StringLiteralConstraint