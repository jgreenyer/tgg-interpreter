/**
 * <copyright>
 * </copyright>
 *
 * $Id: GraphGrammarRule.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph Grammar Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getLeftGraphPattern <em>Left Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern <em>Right Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getContextNodes <em>Context Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getProducedNodes <em>Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getNonproducedNodes <em>Nonproduced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getReusableNodes <em>Reusable Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRefinedRule <em>Refined Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNodes <em>All Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllContextNodes <em>All Context Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllProducedNodes <em>All Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllNonproducedNodes <em>All Nonproduced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllReusableNodes <em>All Reusable Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getAllRefinedRules <em>All Refined Rules</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getReusablePattern <em>Reusable Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule()
 * @model
 * @generated
 */
public interface GraphGrammarRule extends Graph {
	/**
	 * Returns the value of the '<em><b>Left Graph Pattern</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.LeftGraphPattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Graph Pattern</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Graph Pattern</em>' containment reference.
	 * @see #setLeftGraphPattern(LeftGraphPattern)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_LeftGraphPattern()
	 * @see de.upb.swt.qvt.tgg.LeftGraphPattern#getGraphGrammar
	 * @model opposite="graphGrammar" containment="true" required="true"
	 * @generated
	 */
	LeftGraphPattern getLeftGraphPattern();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getLeftGraphPattern <em>Left Graph Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Graph Pattern</em>' containment reference.
	 * @see #getLeftGraphPattern()
	 * @generated
	 */
	void setLeftGraphPattern(LeftGraphPattern value);

	/**
	 * Returns the value of the '<em><b>Right Graph Pattern</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Graph Pattern</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Graph Pattern</em>' containment reference.
	 * @see #setRightGraphPattern(RightGraphPattern)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_RightGraphPattern()
	 * @see de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar
	 * @model opposite="graphGrammar" containment="true" required="true"
	 * @generated
	 */
	RightGraphPattern getRightGraphPattern();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern <em>Right Graph Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Graph Pattern</em>' containment reference.
	 * @see #getRightGraphPattern()
	 * @generated
	 */
	void setRightGraphPattern(RightGraphPattern value);

	/**
	 * Returns the value of the '<em><b>Context Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_ContextNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getContextNodes();

	/**
	 * Returns the value of the '<em><b>Produced Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Produced Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Produced Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_ProducedNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getProducedNodes();

	/**
	 * Returns the value of the '<em><b>Nonproduced Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nonproduced Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nonproduced Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_NonproducedNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getNonproducedNodes();

	/**
	 * Returns the value of the '<em><b>Reusable Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reusable Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reusable Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_ReusableNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getReusableNodes();

	/**
	 * Returns the value of the '<em><b>Refined Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refined Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A rule may refine another rule by the "refines" relationship. Then, the refining rule may define nodes that refine nodes from the (directly or indirectly) refined rule. These refining nodes may have a type class that is equal to or a subclass of the type class of the refined node. The refining rule may contain additional nodes, edges, and constraints. The semantics is that the refining rule, unless marked as abstract, is equal to a TGG rule that is constructed as follows: Take all nodes, edges, and constraints of the refined rule. For a refined node take as the type class the possibly specialized type class of the refining node. Additionally, the constructed rule consists of the graph pattern in the refining rule where edges and constraints that are attached to the refining nodes are attached to the nodes in the constructed rule that correspond to the refined nodes.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Refined Rule</em>' reference.
	 * @see #setRefinedRule(GraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_RefinedRule()
	 * @model
	 * @generated
	 */
	GraphGrammarRule getRefinedRule();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRefinedRule <em>Refined Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refined Rule</em>' reference.
	 * @see #getRefinedRule()
	 * @generated
	 */
	void setRefinedRule(GraphGrammarRule value);

	/**
	 * Returns the value of the '<em><b>All Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If the rule is not a refining rule, this list contains all 
	 * nodes in the rule.
	 * Is the rule is a refining rule, this list contains all nodes 
	 * in the rule and nodes in all refined rules that are not refined
	 * by a more special node (,i.e. all nodes that have a refining node in 
	 * the refnement hierarchy of this rule are not contained in this list).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllNodes();

	/**
	 * Returns the value of the '<em><b>All Context Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Context Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains all context nodes of this rule and refined rules
	 * (see the comment for the allNodes reference)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Context Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllContextNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllContextNodes();

	/**
	 * Returns the value of the '<em><b>All Produced Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Produced Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains all produced nodes of this rule and refined rules
	 * (see the comment for the allNodes reference)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Produced Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllProducedNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllProducedNodes();

	/**
	 * Returns the value of the '<em><b>All Nonproduced Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Nonproduced Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains all non-produced nodes of this rule and refined rules
	 * (see the comment for the allNodes reference)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Nonproduced Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllNonproducedNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllNonproducedNodes();

	/**
	 * Returns the value of the '<em><b>All Reusable Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Reusable Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * contains all reusable nodes of this rule and refined rules
	 * (see the comment for the allNodes reference)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Reusable Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllReusableNodes()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Node> getAllReusableNodes();

	/**
	 * Returns the value of the '<em><b>All Refined Rules</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.GraphGrammarRule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns a recursively built list of all refined rules
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>All Refined Rules</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_AllRefinedRules()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<GraphGrammarRule> getAllRefinedRules();

	/**
	 * Returns the value of the '<em><b>Reusable Pattern</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.ReusablePattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar <em>Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reusable Pattern</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reusable Pattern</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraphGrammarRule_ReusablePattern()
	 * @see de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar
	 * @model opposite="graphGrammar" containment="true"
	 * @generated
	 */
	EList<ReusablePattern> getReusablePattern();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns the given node or a node refining the given node such
	 * that in the  refinement hierarchy of this rule there is no node
	 * refining the returned node.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	Node getMostRefinedNodeOf(Node node);

} // GraphGrammarRule