/**
 * <copyright>
 * </copyright>
 *
 * $Id: DomainGraphPattern.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import de.upb.swt.qvt.qvtbase.Domain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Graph Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getDomainGraphPattern()
 * @model
 * @generated
 */
public interface DomainGraphPattern extends Domain, GraphPattern {
} // DomainGraphPattern