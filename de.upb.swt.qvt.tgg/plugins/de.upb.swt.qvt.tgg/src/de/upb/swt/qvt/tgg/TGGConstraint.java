/**
 * <copyright>
 * </copyright>
 *
 * $Id: TGGConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TGG Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotNode <em>Slot Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttribute <em>Slot Attribute</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttributeName <em>Slot Attribute Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TGGConstraint#isCheckonly <em>Checkonly</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TGGConstraint#isEvaluateAfterMatching <em>Evaluate After Matching</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='NoEnforcingWithoutSlotAttribute\r\nSlotNodeAttributeMustBeNullIfNoSlotNodeSet'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL NoEnforcingWithoutSlotAttribute='not (not self.checkonly and self.slotAttribute = null and self.slotNode <> null and self.slotNode.isProduced())' SlotNodeAttributeMustBeNullIfNoSlotNodeSet='self.slotNode <> null or self.slotAttribute = null'"
 * @generated
 */
public interface TGGConstraint extends Constraint {
	/**
	 * Returns the value of the '<em><b>Slot Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slot Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slot Node</em>' reference.
	 * @see #setSlotNode(Node)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint_SlotNode()
	 * @model
	 * @generated
	 */
	Node getSlotNode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotNode <em>Slot Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slot Node</em>' reference.
	 * @see #getSlotNode()
	 * @generated
	 */
	void setSlotNode(Node value);

	/**
	 * Returns the value of the '<em><b>Slot Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slot Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slot Attribute</em>' reference.
	 * @see #setSlotAttribute(EAttribute)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint_SlotAttribute()
	 * @model
	 * @generated
	 */
	EAttribute getSlotAttribute();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TGGConstraint#getSlotAttribute <em>Slot Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slot Attribute</em>' reference.
	 * @see #getSlotAttribute()
	 * @generated
	 */
	void setSlotAttribute(EAttribute value);

	/**
	 * Returns the value of the '<em><b>Slot Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slot Attribute Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slot Attribute Name</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint_SlotAttributeName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getSlotAttributeName();

	/**
	 * Returns the value of the '<em><b>Checkonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checkonly</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checkonly</em>' attribute.
	 * @see #setCheckonly(boolean)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint_Checkonly()
	 * @model
	 * @generated
	 */
	boolean isCheckonly();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TGGConstraint#isCheckonly <em>Checkonly</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checkonly</em>' attribute.
	 * @see #isCheckonly()
	 * @generated
	 */
	void setCheckonly(boolean value);

	/**
	 * Returns the value of the '<em><b>Evaluate After Matching</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluate After Matching</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluate After Matching</em>' attribute.
	 * @see #setEvaluateAfterMatching(boolean)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTGGConstraint_EvaluateAfterMatching()
	 * @model
	 * @generated
	 */
	boolean isEvaluateAfterMatching();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TGGConstraint#isEvaluateAfterMatching <em>Evaluate After Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluate After Matching</em>' attribute.
	 * @see #isEvaluateAfterMatching()
	 * @generated
	 */
	void setEvaluateAfterMatching(boolean value);

} // TGGConstraint