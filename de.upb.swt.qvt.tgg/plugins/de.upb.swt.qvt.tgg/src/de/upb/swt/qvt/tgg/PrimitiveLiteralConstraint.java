/**
 * <copyright>
 * </copyright>
 *
 * $Id: PrimitiveLiteralConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Literal Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getPrimitiveLiteralConstraint()
 * @model abstract="true"
 * @generated
 */
public interface PrimitiveLiteralConstraint extends TGGConstraint {
} // PrimitiveLiteralConstraint