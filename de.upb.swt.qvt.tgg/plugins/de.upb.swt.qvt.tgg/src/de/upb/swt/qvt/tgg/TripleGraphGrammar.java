/**
 * <copyright>
 * </copyright>
 *
 * $Id: TripleGraphGrammar.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;

import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.Transformation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triple Graph Grammar</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getRelevantReferences <em>Relevant References</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getTypeClassToRuleMap <em>Type Class To Rule Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getAxiom <em>Axiom</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammar()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='RootTransformationMustHaveAnAxiom'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL RootTransformationMustHaveAnAxiom='(axiom <> null) or (parentTransformation <> null)'"
 * @generated
 */
public interface TripleGraphGrammar extends Transformation {
	/**
	 * Returns the value of the '<em><b>Relevant References</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relevant References</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relevant References</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammar_RelevantReferences()
	 * @model many="false" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<EReference> getRelevantReferences();

	/**
	 * Returns the value of the '<em><b>Type Class To Rule Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Class To Rule Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Class To Rule Map</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammar_TypeClassToRuleMap()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	Map<EClassifier, EList<Rule>> getTypeClassToRuleMap();

	/**
	 * Returns the value of the '<em><b>Axiom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axiom</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Axiom</em>' reference.
	 * @see #setAxiom(TripleGraphGrammarAxiom)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getTripleGraphGrammar_Axiom()
	 * @model
	 * @generated
	 */
	TripleGraphGrammarAxiom getAxiom();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar#getAxiom <em>Axiom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Axiom</em>' reference.
	 * @see #getAxiom()
	 * @generated
	 */
	void setAxiom(TripleGraphGrammarAxiom value);

} // TripleGraphGrammar