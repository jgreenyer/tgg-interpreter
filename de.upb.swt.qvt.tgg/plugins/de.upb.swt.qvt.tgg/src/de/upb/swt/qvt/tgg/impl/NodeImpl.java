/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeImpl.java 1835 2007-03-17 14:06:46 +0000 (Sa, 17 Mrz 2007) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getOutgoingEdge <em>Outgoing Edge</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getIncomingEdge <em>Incoming Edge</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getGraph <em>Graph</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getGraphPattern <em>Graph Pattern</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getTypedName <em>Typed Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#isMatchSubtype <em>Match Subtype</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getEType <em>EType</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getRefinedNode <em>Refined Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getAllOutgoingEdges <em>All Outgoing Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getAllIncomingEdges <em>All Incoming Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getAllRefinedNodes <em>All Refined Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getAllGraphPatterns <em>All Graph Patterns</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.NodeImpl#getMatchingPriority <em>Matching Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NodeImpl extends GraphElementImpl implements Node {
	/**
	 * The cached value of the '{@link #getOutgoingEdge() <em>Outgoing Edge</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getOutgoingEdge()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> outgoingEdge;

	/**
	 * The cached value of the '{@link #getIncomingEdge() <em>Incoming Edge</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIncomingEdge()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> incomingEdge;

	/**
	 * The cached value of the '{@link #getGraphPattern() <em>Graph Pattern</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getGraphPattern()
	 * @generated
	 * @ordered
	 */
	protected EList<GraphPattern> graphPattern;

	/**
	 * The default value of the '{@link #getTypedName() <em>Typed Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypedName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPED_NAME_EDEFAULT = "";

	/**
	 * The default value of the '{@link #isMatchSubtype() <em>Match Subtype</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isMatchSubtype()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MATCH_SUBTYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMatchSubtype() <em>Match Subtype</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isMatchSubtype()
	 * @generated
	 * @ordered
	 */
	protected boolean matchSubtype = MATCH_SUBTYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEType() <em>EType</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEType()
	 * @generated
	 * @ordered
	 */
	protected EClass eType;

	/**
	 * The cached value of the '{@link #getRefinedNode() <em>Refined Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefinedNode()
	 * @generated
	 * @ordered
	 */
	protected Node refinedNode;

	/**
	 * The default value of the '{@link #getMatchingPriority() <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatchingPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int MATCHING_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMatchingPriority() <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatchingPriority()
	 * @generated
	 * @ordered
	 */
	protected int matchingPriority = MATCHING_PRIORITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Edge> getOutgoingEdge() {
		if (outgoingEdge == null) {
			outgoingEdge = new EObjectContainmentWithInverseEList<Edge>(Edge.class, this,
					TggPackage.NODE__OUTGOING_EDGE, TggPackage.EDGE__SOURCE_NODE);
		}
		return outgoingEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getIncomingEdge() {
		if (incomingEdge == null) {
			incomingEdge = new EObjectWithInverseResolvingEList<Edge>(Edge.class, this, TggPackage.NODE__INCOMING_EDGE, TggPackage.EDGE__TARGET_NODE);
		}
		return incomingEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Graph getGraph() {
		if (eContainerFeatureID() != TggPackage.NODE__GRAPH) return null;
		return (Graph)eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraph(Graph newGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGraph, TggPackage.NODE__GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraph(Graph newGraph) {
		if (newGraph != eInternalContainer() || (eContainerFeatureID() != TggPackage.NODE__GRAPH && newGraph != null)) {
			if (EcoreUtil.isAncestor(this, newGraph))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGraph != null)
				msgs = ((InternalEObject)newGraph).eInverseAdd(this, TggPackage.GRAPH__NODE, Graph.class, msgs);
			msgs = basicSetGraph(newGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__GRAPH, newGraph, newGraph));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GraphPattern> getGraphPattern() {
		if (graphPattern == null) {
			graphPattern = new EObjectWithInverseResolvingEList.ManyInverse<GraphPattern>(GraphPattern.class, this, TggPackage.NODE__GRAPH_PATTERN, TggPackage.GRAPH_PATTERN__NODE);
		}
		return graphPattern;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isLeft() {
		if (getGraph() == null) {
			// this does not happen during the normal application of the
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc.,
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return false;
		}
		if (((GraphGrammarRule) getGraph()).getLeftGraphPattern() == null) {
			initLeftRightGraphPatterns();
		}
		return ((GraphGrammarRule) getGraph()).getLeftGraphPattern().getNode().contains(this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setLeft(boolean newLeft) {

		boolean oldLeft = isLeft();

		if (getGraph() == null) {
			// this does not happen during the normal application of the
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc.,
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return;
		}
		if (((GraphGrammarRule) getGraph()).getLeftGraphPattern() == null
				|| ((GraphGrammarRule) getGraph()).getRightGraphPattern() == null) {
			initLeftRightGraphPatterns();
		}
		TripleGraphGrammarRule tripleGraphGrammarRule = (TripleGraphGrammarRule) getGraph();
		LeftGraphPattern leftGraphPattern = tripleGraphGrammarRule.getLeftGraphPattern();

		if (newLeft && !leftGraphPattern.getNode().contains(this)) {
			leftGraphPattern.getNode().add(this);
			getGraphPattern().add(leftGraphPattern);
		}

		if (!newLeft && leftGraphPattern.getNode().contains(this)) {
			leftGraphPattern.getNode().remove(this);
			getGraphPattern().remove(leftGraphPattern);
		}
		if (newLeft && !isRight()) {
			setRight(true);
		}
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__LEFT, oldLeft, newLeft));
		}

	}

	private void initLeftRightGraphPatterns() {
		// System.out.println("Calling initLeftRightGraphPatterns...");
		TripleGraphGrammarRule tripleGraphGrammarRule = (TripleGraphGrammarRule) getGraph();
		LeftGraphPattern leftGraphPattern;
		RightGraphPattern rightGraphPattern;
		if (tripleGraphGrammarRule.getLeftGraphPattern() == null) {
			leftGraphPattern = TggFactory.eINSTANCE.createLeftGraphPattern();
			tripleGraphGrammarRule.setLeftGraphPattern(leftGraphPattern);
		} else {
			leftGraphPattern = tripleGraphGrammarRule.getLeftGraphPattern();
		}
		if (tripleGraphGrammarRule.getRightGraphPattern() == null) {
			rightGraphPattern = TggFactory.eINSTANCE.createRightGraphPattern();
			tripleGraphGrammarRule.setRightGraphPattern(rightGraphPattern);
		} else {
			rightGraphPattern = tripleGraphGrammarRule.getRightGraphPattern();
		}

		// this is the default setting: right=true & left=false
		if (!rightGraphPattern.getNode().contains(this)) {
			rightGraphPattern.getNode().add(this);
			getGraphPattern().add(rightGraphPattern);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isRight() {
		if (getGraph() == null) {
			// this does not happen during the normal application of the
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc.,
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return false;
		}

		// to initialize the default right=true & left=false

		if (((GraphGrammarRule) getGraph()).getRightGraphPattern() == null) {
			initLeftRightGraphPatterns();
		}
		return ((GraphGrammarRule) getGraph()).getRightGraphPattern().getNode().contains(this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setRight(boolean newRight) {

		boolean oldRight = isRight();

		if (getGraph() == null) {
			// this does not happen during the normal application of the
			// TGG interpreter or editor. However, when working with the
			// generic EMF facitilites, generated editors etc.,
			// it might lead to problems to assume that a Node is always
			// child of a Graph.
			return;
		}
		if (((GraphGrammarRule) getGraph()).getLeftGraphPattern() == null
				|| ((GraphGrammarRule) getGraph()).getRightGraphPattern() == null) {
			initLeftRightGraphPatterns();
		}

		TripleGraphGrammarRule tripleGraphGrammarRule = (TripleGraphGrammarRule) getGraph();
		RightGraphPattern rightGraphPattern = tripleGraphGrammarRule.getRightGraphPattern();

		if (isLeft() && !newRight) {
			setLeft(false);
		}

		if (newRight && !rightGraphPattern.getNode().contains(this)) {
			rightGraphPattern.getNode().add(this);
			getGraphPattern().add(rightGraphPattern);
		}

		if (!newRight && rightGraphPattern.getNode().contains(this)) {
			rightGraphPattern.getNode().remove(this);
			getGraphPattern().remove(rightGraphPattern);
		}
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__RIGHT, oldRight, newRight));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMatchSubtype() {
		return matchSubtype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchSubtype(boolean newMatchSubtype) {
		boolean oldMatchSubtype = matchSubtype;
		matchSubtype = newMatchSubtype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__MATCH_SUBTYPE, oldMatchSubtype, matchSubtype));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEType() {
		if (eType != null && eType.eIsProxy()) {
			InternalEObject oldEType = (InternalEObject)eType;
			eType = (EClass)eResolveProxy(oldEType);
			if (eType != oldEType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.NODE__ETYPE, oldEType, eType));
			}
		}
		return eType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetEType() {
		return eType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setEType(EClass newEType) {
		EClass oldEType = eType;
		String oldTypedName = getTypedName();
		eType = newEType;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__ETYPE, oldEType, eType));
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__TYPED_NAME, oldTypedName,
					getTypedName()));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getRefinedNode() {
		if (refinedNode != null && refinedNode.eIsProxy()) {
			InternalEObject oldRefinedNode = (InternalEObject)refinedNode;
			refinedNode = (Node)eResolveProxy(oldRefinedNode);
			if (refinedNode != oldRefinedNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.NODE__REFINED_NODE, oldRefinedNode, refinedNode));
			}
		}
		return refinedNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetRefinedNode() {
		return refinedNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefinedNode(Node newRefinedNode) {
		Node oldRefinedNode = refinedNode;
		refinedNode = newRefinedNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__REFINED_NODE, oldRefinedNode, refinedNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Edge> getAllOutgoingEdges() {
		if (getRefinedNode() == null) {
			return getOutgoingEdge();
		} else {
			if (allOutgoingEdges == null){
				EList<Edge> allOutgoingEdgesList = new UniqueEList<Edge>(getRefinedNode().getAllOutgoingEdges());
				allOutgoingEdgesList.addAll(getOutgoingEdge());
				allOutgoingEdges = new EcoreEList.UnmodifiableEList<Edge>(this, TggPackage.Literals.NODE__ALL_OUTGOING_EDGES,
						allOutgoingEdgesList.size(), allOutgoingEdgesList.toArray());
			}
			return allOutgoingEdges;
		}
	}

	private EList<Edge> allOutgoingEdges;
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Edge> getAllIncomingEdges() {
		if (getRefinedNode() == null) {
			return getIncomingEdge();
		} else {
			if (allIncomingEdges == null){
				EList<Edge> allIncomingEdgesList = new UniqueEList<Edge>(getRefinedNode().getAllIncomingEdges());
				allIncomingEdgesList.addAll(getIncomingEdge());
				allIncomingEdges = new EcoreEList.UnmodifiableEList<Edge>(this, TggPackage.Literals.NODE__ALL_INCOMING_EDGES,
						allIncomingEdgesList.size(), allIncomingEdgesList.toArray());
			}
			return allIncomingEdges;
		}
	}

	private EList<Edge> allIncomingEdges;

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getAllRefinedNodes() {
		if (allRefinedNodesList == null){
			EList<Node> allRefinedNodes = new UniqueEList<Node>();
			// IMPORTANT: The most refined node should be at the beginning of the list. See RuleBindingImpl.pushNodeBinding(Node node, EObject modelObject) 
			allRefinedNodes.add(this);
			if (getRefinedNode() != null) {
				allRefinedNodes.addAll(getRefinedNode().getAllRefinedNodes());
			}
			allRefinedNodesList = new EcoreEList.UnmodifiableEList<Node>(this, TggPackage.Literals.NODE__ALL_REFINED_NODES,
					allRefinedNodes.size(), allRefinedNodes.toArray());
		}
		return allRefinedNodesList;
	}

	private EList<Node> allRefinedNodesList;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<GraphPattern> getAllGraphPatterns() {
		if (getRefinedNode() == null){
			return getGraphPattern();
		}else{
			if (allGraphPatterns == null){
				EList<GraphPattern> allGraphPatternsList = new UniqueEList<GraphPattern>(getRefinedNode().getAllGraphPatterns());
				allGraphPatternsList.addAll(getGraphPattern());
				allGraphPatterns = new EcoreEList.UnmodifiableEList<GraphPattern>(this, TggPackage.Literals.NODE__ALL_GRAPH_PATTERNS,
						allGraphPatternsList.size(), allGraphPatternsList.toArray());
			}
			return allGraphPatterns;
		}
	}
	
	private EList<GraphPattern> allGraphPatterns;
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMatchingPriority() {
		return matchingPriority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchingPriority(int newMatchingPriority) {
		int oldMatchingPriority = matchingPriority;
		matchingPriority = newMatchingPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.NODE__MATCHING_PRIORITY, oldMatchingPriority, matchingPriority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DomainGraphPattern getDomainGraphPattern() {
		for (GraphPattern graphPattern : getGraphPattern()) {
			if (graphPattern instanceof DomainGraphPattern)
				return (DomainGraphPattern) graphPattern;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getTypedName() {
		if (eType == null) {
			return "type_unknown";
		} else {
			return eType.getName();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoingEdge()).basicAdd(otherEnd, msgs);
			case TggPackage.NODE__INCOMING_EDGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingEdge()).basicAdd(otherEnd, msgs);
			case TggPackage.NODE__GRAPH:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGraph((Graph)otherEnd, msgs);
			case TggPackage.NODE__GRAPH_PATTERN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGraphPattern()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				return ((InternalEList<?>)getOutgoingEdge()).basicRemove(otherEnd, msgs);
			case TggPackage.NODE__INCOMING_EDGE:
				return ((InternalEList<?>)getIncomingEdge()).basicRemove(otherEnd, msgs);
			case TggPackage.NODE__GRAPH:
				return basicSetGraph(null, msgs);
			case TggPackage.NODE__GRAPH_PATTERN:
				return ((InternalEList<?>)getGraphPattern()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TggPackage.NODE__GRAPH:
				return eInternalContainer().eInverseRemove(this, TggPackage.GRAPH__NODE, Graph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				return getOutgoingEdge();
			case TggPackage.NODE__INCOMING_EDGE:
				return getIncomingEdge();
			case TggPackage.NODE__GRAPH:
				return getGraph();
			case TggPackage.NODE__GRAPH_PATTERN:
				return getGraphPattern();
			case TggPackage.NODE__TYPED_NAME:
				return getTypedName();
			case TggPackage.NODE__MATCH_SUBTYPE:
				return isMatchSubtype();
			case TggPackage.NODE__ETYPE:
				if (resolve) return getEType();
				return basicGetEType();
			case TggPackage.NODE__REFINED_NODE:
				if (resolve) return getRefinedNode();
				return basicGetRefinedNode();
			case TggPackage.NODE__ALL_OUTGOING_EDGES:
				return getAllOutgoingEdges();
			case TggPackage.NODE__ALL_INCOMING_EDGES:
				return getAllIncomingEdges();
			case TggPackage.NODE__ALL_REFINED_NODES:
				return getAllRefinedNodes();
			case TggPackage.NODE__ALL_GRAPH_PATTERNS:
				return getAllGraphPatterns();
			case TggPackage.NODE__MATCHING_PRIORITY:
				return getMatchingPriority();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				getOutgoingEdge().clear();
				getOutgoingEdge().addAll((Collection<? extends Edge>)newValue);
				return;
			case TggPackage.NODE__INCOMING_EDGE:
				getIncomingEdge().clear();
				getIncomingEdge().addAll((Collection<? extends Edge>)newValue);
				return;
			case TggPackage.NODE__GRAPH:
				setGraph((Graph)newValue);
				return;
			case TggPackage.NODE__GRAPH_PATTERN:
				getGraphPattern().clear();
				getGraphPattern().addAll((Collection<? extends GraphPattern>)newValue);
				return;
			case TggPackage.NODE__MATCH_SUBTYPE:
				setMatchSubtype((Boolean)newValue);
				return;
			case TggPackage.NODE__ETYPE:
				setEType((EClass)newValue);
				return;
			case TggPackage.NODE__REFINED_NODE:
				setRefinedNode((Node)newValue);
				return;
			case TggPackage.NODE__MATCHING_PRIORITY:
				setMatchingPriority((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				getOutgoingEdge().clear();
				return;
			case TggPackage.NODE__INCOMING_EDGE:
				getIncomingEdge().clear();
				return;
			case TggPackage.NODE__GRAPH:
				setGraph((Graph)null);
				return;
			case TggPackage.NODE__GRAPH_PATTERN:
				getGraphPattern().clear();
				return;
			case TggPackage.NODE__MATCH_SUBTYPE:
				setMatchSubtype(MATCH_SUBTYPE_EDEFAULT);
				return;
			case TggPackage.NODE__ETYPE:
				setEType((EClass)null);
				return;
			case TggPackage.NODE__REFINED_NODE:
				setRefinedNode((Node)null);
				return;
			case TggPackage.NODE__MATCHING_PRIORITY:
				setMatchingPriority(MATCHING_PRIORITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.NODE__OUTGOING_EDGE:
				return outgoingEdge != null && !outgoingEdge.isEmpty();
			case TggPackage.NODE__INCOMING_EDGE:
				return incomingEdge != null && !incomingEdge.isEmpty();
			case TggPackage.NODE__GRAPH:
				return getGraph() != null;
			case TggPackage.NODE__GRAPH_PATTERN:
				return graphPattern != null && !graphPattern.isEmpty();
			case TggPackage.NODE__TYPED_NAME:
				return TYPED_NAME_EDEFAULT == null ? getTypedName() != null : !TYPED_NAME_EDEFAULT.equals(getTypedName());
			case TggPackage.NODE__MATCH_SUBTYPE:
				return matchSubtype != MATCH_SUBTYPE_EDEFAULT;
			case TggPackage.NODE__ETYPE:
				return eType != null;
			case TggPackage.NODE__REFINED_NODE:
				return refinedNode != null;
			case TggPackage.NODE__ALL_OUTGOING_EDGES:
				return !getAllOutgoingEdges().isEmpty();
			case TggPackage.NODE__ALL_INCOMING_EDGES:
				return !getAllIncomingEdges().isEmpty();
			case TggPackage.NODE__ALL_REFINED_NODES:
				return !getAllRefinedNodes().isEmpty();
			case TggPackage.NODE__ALL_GRAPH_PATTERNS:
				return !getAllGraphPatterns().isEmpty();
			case TggPackage.NODE__MATCHING_PRIORITY:
				return matchingPriority != MATCHING_PRIORITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (matchSubtype: ");
		result.append(matchSubtype);
		result.append(", matchingPriority: ");
		result.append(matchingPriority);
		result.append(')');
		return result.toString();
	}

} // NodeImpl