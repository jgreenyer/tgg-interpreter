/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.ecore.Constraint;

import de.upb.swt.qvt.qvtbase.util.QvtbaseValidator;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.ITransformAttribute;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.LinkConstraintType;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.TggPackage
 * @generated
 */
public class TggValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final TggValidator INSTANCE = new TggValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "de.upb.swt.qvt.tgg";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QvtbaseValidator qvtbaseValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggValidator() {
		super();
		qvtbaseValidator = QvtbaseValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return TggPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case TggPackage.GRAPH:
				return validateGraph((Graph)value, diagnostics, context);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE:
				return validateTripleGraphGrammarRule((TripleGraphGrammarRule)value, diagnostics, context);
			case TggPackage.GRAPH_GRAMMAR_RULE:
				return validateGraphGrammarRule((GraphGrammarRule)value, diagnostics, context);
			case TggPackage.GRAPH_PATTERN:
				return validateGraphPattern((GraphPattern)value, diagnostics, context);
			case TggPackage.NODE:
				return validateNode((Node)value, diagnostics, context);
			case TggPackage.EDGE:
				return validateEdge((Edge)value, diagnostics, context);
			case TggPackage.LEFT_GRAPH_PATTERN:
				return validateLeftGraphPattern((LeftGraphPattern)value, diagnostics, context);
			case TggPackage.RIGHT_GRAPH_PATTERN:
				return validateRightGraphPattern((RightGraphPattern)value, diagnostics, context);
			case TggPackage.DOMAIN_GRAPH_PATTERN:
				return validateDomainGraphPattern((DomainGraphPattern)value, diagnostics, context);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR:
				return validateTripleGraphGrammar((TripleGraphGrammar)value, diagnostics, context);
			case TggPackage.CONSTRAINT:
				return validateConstraint((Constraint)value, diagnostics, context);
			case TggPackage.TGG_CONSTRAINT:
				return validateTGGConstraint((TGGConstraint)value, diagnostics, context);
			case TggPackage.GRAPH_ELEMENT:
				return validateGraphElement((GraphElement)value, diagnostics, context);
			case TggPackage.ITRANSFORM_ATTRIBUTE:
				return validateITransformAttribute((ITransformAttribute)value, diagnostics, context);
			case TggPackage.OCL_CONSTRAINT:
				return validateOCLConstraint((OCLConstraint)value, diagnostics, context);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_AXIOM:
				return validateTripleGraphGrammarAxiom((TripleGraphGrammarAxiom)value, diagnostics, context);
			case TggPackage.REUSABLE_PATTERN:
				return validateReusablePattern((ReusablePattern)value, diagnostics, context);
			case TggPackage.LINK_CONSTRAINT_TYPE:
				return validateLinkConstraintType((LinkConstraintType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGraph(Graph graph, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(graph, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTripleGraphGrammarRule(TripleGraphGrammarRule tripleGraphGrammarRule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tripleGraphGrammarRule, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tripleGraphGrammarRule, diagnostics, context);
		if (result || diagnostics != null) result &= qvtbaseValidator.validateRule_uniqueName(tripleGraphGrammarRule, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGraphGrammarRule(GraphGrammarRule graphGrammarRule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(graphGrammarRule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGraphPattern(GraphPattern graphPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(graphPattern, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(node, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(node, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_RefiningNodeOnSameGraphGrammarSideAsRefinedNode(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_RefiningNodeInSameDomainAsRefiningNode(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_NodeMustHaveDomainGraphPattern(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_RefiningNodeMustHaveTheSameNameAsTheRefinedNode(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_AtMostOneNodeRefiningTheSameOtherNode(node, diagnostics, context);
		if (result || diagnostics != null) result &= validateNode_NoAbstractTypeForProducedNodes(node, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the RefiningNodeOnSameGraphGrammarSideAsRefinedNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__REFINING_NODE_ON_SAME_GRAPH_GRAMMAR_SIDE_AS_REFINED_NODE__EEXPRESSION = "self.refinedNode = null or (self.left = self.refinedNode.left and self.right = self.refinedNode.right)";

	/**
	 * Validates the RefiningNodeOnSameGraphGrammarSideAsRefinedNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_RefiningNodeOnSameGraphGrammarSideAsRefinedNode(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "RefiningNodeOnSameGraphGrammarSideAsRefinedNode",
				 NODE__REFINING_NODE_ON_SAME_GRAPH_GRAMMAR_SIDE_AS_REFINED_NODE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the RefiningNodeInSameDomainAsRefiningNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__REFINING_NODE_IN_SAME_DOMAIN_AS_REFINING_NODE__EEXPRESSION = "self.refinedNode = null or (self.getDomainGraphPattern().typedModel = self.refinedNode.getDomainGraphPattern().typedModel)";

	/**
	 * Validates the RefiningNodeInSameDomainAsRefiningNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_RefiningNodeInSameDomainAsRefiningNode(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "RefiningNodeInSameDomainAsRefiningNode",
				 NODE__REFINING_NODE_IN_SAME_DOMAIN_AS_REFINING_NODE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__REFINING_NODES_TYPE_CLASS_IS_EQUAL_TO_OR_SUBCLASS_OF_REFINED_NODES_TYPE_CLASS__EEXPRESSION = "self.refinedNode = null or (self.refinedNode.eType = self.eType or self.refinedNode.eType.isSuperTypeOf(self.eType))";

	/**
	 * Validates the RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "RefiningNodesTypeClassIsEqualToOrSubclassOfRefinedNodesTypeClass",
				 NODE__REFINING_NODES_TYPE_CLASS_IS_EQUAL_TO_OR_SUBCLASS_OF_REFINED_NODES_TYPE_CLASS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the NodeMustHaveDomainGraphPattern constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__NODE_MUST_HAVE_DOMAIN_GRAPH_PATTERN__EEXPRESSION = "self.getDomainGraphPattern() <> null";

	/**
	 * Validates the NodeMustHaveDomainGraphPattern constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_NodeMustHaveDomainGraphPattern(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "NodeMustHaveDomainGraphPattern",
				 NODE__NODE_MUST_HAVE_DOMAIN_GRAPH_PATTERN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the RefiningNodeMustHaveTheSameNameAsTheRefinedNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__REFINING_NODE_MUST_HAVE_THE_SAME_NAME_AS_THE_REFINED_NODE__EEXPRESSION = "self.refinedNode = null or (self.name = self.refinedNode.name)";

	/**
	 * Validates the RefiningNodeMustHaveTheSameNameAsTheRefinedNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_RefiningNodeMustHaveTheSameNameAsTheRefinedNode(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "RefiningNodeMustHaveTheSameNameAsTheRefinedNode",
				 NODE__REFINING_NODE_MUST_HAVE_THE_SAME_NAME_AS_THE_REFINED_NODE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the AtMostOneNodeRefiningTheSameOtherNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__AT_MOST_ONE_NODE_REFINING_THE_SAME_OTHER_NODE__EEXPRESSION = "self.refinedNode = null or self.graph.node->select(n| n <> self and  n.refinedNode <> null and n.refinedNode = self.refinedNode)->isEmpty()";

	/**
	 * Validates the AtMostOneNodeRefiningTheSameOtherNode constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_AtMostOneNodeRefiningTheSameOtherNode(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "AtMostOneNodeRefiningTheSameOtherNode",
				 NODE__AT_MOST_ONE_NODE_REFINING_THE_SAME_OTHER_NODE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the NoAbstractTypeForProducedNodes constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NODE__NO_ABSTRACT_TYPE_FOR_PRODUCED_NODES__EEXPRESSION = "(not self.left and self.right) implies not self.eType.abstract";

	/**
	 * Validates the NoAbstractTypeForProducedNodes constraint of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNode_NoAbstractTypeForProducedNodes(Node node, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.NODE,
				 node,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "NoAbstractTypeForProducedNodes",
				 NODE__NO_ABSTRACT_TYPE_FOR_PRODUCED_NODES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEdge(Edge edge, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(edge, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validateEdge_EdgeMustHaveTypeThatIsAReferenceOfSourceNode(edge, diagnostics, context);
		if (result || diagnostics != null) result &= validateEdge_SuccessorsMustHaveSameSourceNodeAndSameReferenceType(edge, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the EdgeMustHaveTypeThatIsAReferenceOfSourceNode constraint of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String EDGE__EDGE_MUST_HAVE_TYPE_THAT_IS_AREFERENCE_OF_SOURCE_NODE__EEXPRESSION = "self.sourceNode.eType.eAllReferences->includes(self.typeReference) or self.typeReference.name='EObject'";

	/**
	 * Validates the EdgeMustHaveTypeThatIsAReferenceOfSourceNode constraint of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateEdge_EdgeMustHaveTypeThatIsAReferenceOfSourceNode(Edge edge, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.EDGE,
				 edge,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "EdgeMustHaveTypeThatIsAReferenceOfSourceNode",
				 EDGE__EDGE_MUST_HAVE_TYPE_THAT_IS_AREFERENCE_OF_SOURCE_NODE__EEXPRESSION,
				 Diagnostic.WARNING,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the SuccessorsMustHaveSameSourceNodeAndSameReferenceType constraint of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String EDGE__SUCCESSORS_MUST_HAVE_SAME_SOURCE_NODE_AND_SAME_REFERENCE_TYPE__EEXPRESSION = "(self.directSuccessor = null or self.typeReference = self.directSuccessor.typeReference) and (indirectSuccessor->forAll(self.typeReference = typeReference))";

	/**
	 * Validates the SuccessorsMustHaveSameSourceNodeAndSameReferenceType constraint of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEdge_SuccessorsMustHaveSameSourceNodeAndSameReferenceType(Edge edge, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.EDGE,
				 edge,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "SuccessorsMustHaveSameSourceNodeAndSameReferenceType",
				 EDGE__SUCCESSORS_MUST_HAVE_SAME_SOURCE_NODE_AND_SAME_REFERENCE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLeftGraphPattern(LeftGraphPattern leftGraphPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(leftGraphPattern, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRightGraphPattern(RightGraphPattern rightGraphPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(rightGraphPattern, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDomainGraphPattern(DomainGraphPattern domainGraphPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(domainGraphPattern, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTripleGraphGrammar(TripleGraphGrammar tripleGraphGrammar, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tripleGraphGrammar, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tripleGraphGrammar, diagnostics, context);
		if (result || diagnostics != null) result &= validateTripleGraphGrammar_RootTransformationMustHaveAnAxiom(tripleGraphGrammar, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the RootTransformationMustHaveAnAxiom constraint of '<em>Triple Graph Grammar</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TRIPLE_GRAPH_GRAMMAR__ROOT_TRANSFORMATION_MUST_HAVE_AN_AXIOM__EEXPRESSION = "(axiom <> null) or (parentTransformation <> null)";

	/**
	 * Validates the RootTransformationMustHaveAnAxiom constraint of '<em>Triple Graph Grammar</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTripleGraphGrammar_RootTransformationMustHaveAnAxiom(TripleGraphGrammar tripleGraphGrammar, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.TRIPLE_GRAPH_GRAMMAR,
				 tripleGraphGrammar,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "RootTransformationMustHaveAnAxiom",
				 TRIPLE_GRAPH_GRAMMAR__ROOT_TRANSFORMATION_MUST_HAVE_AN_AXIOM__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstraint(Constraint constraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(constraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTGGConstraint(TGGConstraint tggConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tggConstraint, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validateTGGConstraint_NoEnforcingWithoutSlotAttribute(tggConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validateTGGConstraint_SlotNodeAttributeMustBeNullIfNoSlotNodeSet(tggConstraint, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the NoEnforcingWithoutSlotAttribute constraint of '<em>TGG Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TGG_CONSTRAINT__NO_ENFORCING_WITHOUT_SLOT_ATTRIBUTE__EEXPRESSION = "not (not self.checkonly and self.slotAttribute = null and self.slotNode <> null and self.slotNode.isProduced())";

	/**
	 * Validates the NoEnforcingWithoutSlotAttribute constraint of '<em>TGG Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTGGConstraint_NoEnforcingWithoutSlotAttribute(TGGConstraint tggConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.TGG_CONSTRAINT,
				 tggConstraint,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "NoEnforcingWithoutSlotAttribute",
				 TGG_CONSTRAINT__NO_ENFORCING_WITHOUT_SLOT_ATTRIBUTE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the SlotNodeAttributeMustBeNullIfNoSlotNodeSet constraint of '<em>TGG Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TGG_CONSTRAINT__SLOT_NODE_ATTRIBUTE_MUST_BE_NULL_IF_NO_SLOT_NODE_SET__EEXPRESSION = "self.slotNode <> null or self.slotAttribute = null";

	/**
	 * Validates the SlotNodeAttributeMustBeNullIfNoSlotNodeSet constraint of '<em>TGG Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTGGConstraint_SlotNodeAttributeMustBeNullIfNoSlotNodeSet(TGGConstraint tggConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.TGG_CONSTRAINT,
				 tggConstraint,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "SlotNodeAttributeMustBeNullIfNoSlotNodeSet",
				 TGG_CONSTRAINT__SLOT_NODE_ATTRIBUTE_MUST_BE_NULL_IF_NO_SLOT_NODE_SET__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGraphElement(GraphElement graphElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(graphElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateITransformAttribute(ITransformAttribute iTransformAttribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iTransformAttribute, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOCLConstraint(OCLConstraint oclConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(oclConstraint, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validateTGGConstraint_NoEnforcingWithoutSlotAttribute(oclConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validateTGGConstraint_SlotNodeAttributeMustBeNullIfNoSlotNodeSet(oclConstraint, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTripleGraphGrammarAxiom(TripleGraphGrammarAxiom tripleGraphGrammarAxiom, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tripleGraphGrammarAxiom, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tripleGraphGrammarAxiom, diagnostics, context);
		if (result || diagnostics != null) result &= qvtbaseValidator.validateRule_uniqueName(tripleGraphGrammarAxiom, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReusablePattern(ReusablePattern reusablePattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(reusablePattern, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(reusablePattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateReusablePattern_AllNodesMustBeReusable(reusablePattern, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the AllNodesMustBeReusable constraint of '<em>Reusable Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REUSABLE_PATTERN__ALL_NODES_MUST_BE_REUSABLE__EEXPRESSION = "self.node->forAll(n|n.isReusable())";

	/**
	 * Validates the AllNodesMustBeReusable constraint of '<em>Reusable Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReusablePattern_AllNodesMustBeReusable(ReusablePattern reusablePattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TggPackage.Literals.REUSABLE_PATTERN,
				 reusablePattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "AllNodesMustBeReusable",
				 REUSABLE_PATTERN__ALL_NODES_MUST_BE_REUSABLE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkConstraintType(LinkConstraintType linkConstraintType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //TggValidator
