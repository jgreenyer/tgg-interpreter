/**
 * <copyright>
 * </copyright>
 *
 * $Id: TripleGraphGrammarRuleImpl.java 1835 2007-03-17 14:06:46 +0000 (Sa, 17 Mrz 2007) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triple Graph Grammar Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getTransformation <em>Transformation</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getOverridden <em>Overridden</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getDomainsLeftSideNodesMap <em>Domains Left Side Nodes Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getDomainsNodesMap <em>Domains Nodes Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getTypedModelToDomainsMap <em>Typed Model To Domains Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getAllConstraints <em>All Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl#getRulesAdvisedToApplyNext <em>Rules Advised To Apply Next</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TripleGraphGrammarRuleImpl extends GraphGrammarRuleImpl implements TripleGraphGrammarRule {
	/**
	 * The cached value of the '{@link #getDomain() <em>Domain</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomain()
	 * @generated
	 * @ordered
	 */
	protected EList<Domain> domain;

	/**
	 * The cached value of the '{@link #getOverrides() <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverrides()
	 * @generated
	 * @ordered
	 */
	protected Rule overrides;

	/**
	 * The cached value of the '{@link #getOverridden() <em>Overridden</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden()
	 * @generated
	 * @ordered
	 */
	protected EList<Rule> overridden;

	/**
	 * The cached value of the '{@link #getDomainsLeftSideNodesMap() <em>Domains Left Side Nodes Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainsLeftSideNodesMap()
	 * @generated
	 * @ordered
	 */
	protected Map<TypedModel, EList<Node>> domainsLeftSideNodesMap;

	/**
	 * This is true if the Domains Left Side Nodes Map attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainsLeftSideNodesMapESet;

	/**
	 * The cached value of the '{@link #getDomainsNodesMap() <em>Domains Nodes Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainsNodesMap()
	 * @generated
	 * @ordered
	 */
	protected Map<TypedModel, EList<Node>> domainsNodesMap;

	/**
	 * This is true if the Domains Nodes Map attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainsNodesMapESet;

	/**
	 * The cached value of the '{@link #getTypedModelToDomainsMap() <em>Typed Model To Domains Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedModelToDomainsMap()
	 * @generated
	 * @ordered
	 */
	protected Map<TypedModel, Domain> typedModelToDomainsMap;

	/**
	 * This is true if the Typed Model To Domains Map attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typedModelToDomainsMapESet;

	/**
	 * The cached value of the '{@link #getConstraint() <em>Constraint</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraint()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraint;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRulesAdvisedToApplyNext() <em>Rules Advised To Apply Next</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesAdvisedToApplyNext()
	 * @generated
	 * @ordered
	 */
	protected EList<TripleGraphGrammarRule> rulesAdvisedToApplyNext;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected TripleGraphGrammarRuleImpl() {
		super();
		//		setLeftGraphPattern(TggFactory.eINSTANCE.createLeftGraphPattern());
		//		setRightGraphPattern(TggFactory.eINSTANCE.createRightGraphPattern());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TRIPLE_GRAPH_GRAMMAR_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation getTransformation() {
		if (eContainerFeatureID() != TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION) return null;
		return (Transformation)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transformation basicGetTransformation() {
		if (eContainerFeatureID() != TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION) return null;
		return (Transformation)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransformation(Transformation newTransformation, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTransformation, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformation(Transformation newTransformation) {
		if (newTransformation != eInternalContainer() || (eContainerFeatureID() != TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION && newTransformation != null)) {
			if (EcoreUtil.isAncestor(this, newTransformation))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTransformation != null)
				msgs = ((InternalEObject)newTransformation).eInverseAdd(this, QvtbasePackage.TRANSFORMATION__RULE, Transformation.class, msgs);
			msgs = basicSetTransformation(newTransformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION, newTransformation, newTransformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Domain> getDomain() {
		if (domain == null) {
			domain = new EObjectContainmentWithInverseEList.Resolving<Domain>(Domain.class, this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN, QvtbasePackage.DOMAIN__RULE);
		}
		return domain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule getOverrides() {
		if (overrides != null && overrides.eIsProxy()) {
			InternalEObject oldOverrides = (InternalEObject)overrides;
			overrides = (Rule)eResolveProxy(oldOverrides);
			if (overrides != oldOverrides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES, oldOverrides, overrides));
			}
		}
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule basicGetOverrides() {
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverrides(Rule newOverrides, NotificationChain msgs) {
		Rule oldOverrides = overrides;
		overrides = newOverrides;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES, oldOverrides, newOverrides);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverrides(Rule newOverrides) {
		if (newOverrides != overrides) {
			NotificationChain msgs = null;
			if (overrides != null)
				msgs = ((InternalEObject)overrides).eInverseRemove(this, QvtbasePackage.RULE__OVERRIDDEN, Rule.class, msgs);
			if (newOverrides != null)
				msgs = ((InternalEObject)newOverrides).eInverseAdd(this, QvtbasePackage.RULE__OVERRIDDEN, Rule.class, msgs);
			msgs = basicSetOverrides(newOverrides, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES, newOverrides, newOverrides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Rule> getOverridden() {
		if (overridden == null) {
			overridden = new EObjectWithInverseResolvingEList<Rule>(Rule.class, this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN, QvtbasePackage.RULE__OVERRIDES);
		}
		return overridden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<TypedModel, EList<Node>> getDomainsLeftSideNodesMap() {
		if (domainsLeftSideNodesMap == null) {
			domainsLeftSideNodesMap = new HashMap<TypedModel, EList<Node>>();

			Iterator<Domain> domainIterator = getDomain().iterator();
			while (domainIterator.hasNext()) {
				EList<Node> nodesInDomainsLeftSide = new BasicEList<Node>();
				DomainGraphPattern domainGraphPattern = (DomainGraphPattern) domainIterator.next();
				Iterator<Node> domainGraphPatternNodesIterator = domainGraphPattern.getNode().iterator();
				while (domainGraphPatternNodesIterator.hasNext()) {
					Node domainNode = (Node) domainGraphPatternNodesIterator.next();
					if (getLeftGraphPattern().getNode().contains(domainNode)) {
						if (!nodesInDomainsLeftSide.contains(domainNode)) {
							nodesInDomainsLeftSide.add(domainNode);
						}
					}
				}
				domainsLeftSideNodesMap.put(domainGraphPattern.getTypedModel(), nodesInDomainsLeftSide);
			}
		}

		return domainsLeftSideNodesMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainsLeftSideNodesMap() {
		return domainsLeftSideNodesMapESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<TypedModel, EList<Node>> getDomainsNodesMap() {
		if (domainsNodesMap == null) {
			domainsNodesMap = new HashMap<TypedModel, EList<Node>>();

			/*
			 * Add a map entry for all typedModels (not only for the typedModels of this rule)
			 * because this method is frequently used with all typedModels (from an out-of-rule 
			 * context). In order to reduce null pointer checks just add an empty list for 
			 * typedModels not used in this rule. 
			 */
			for (TypedModel typedModel : getTransformation().getAllModelParameters()) {
				EList<Node> nodesInDomain = new BasicEList<Node>();
				for (Node node : getAllNodes()) {
					if (node.getDomainGraphPattern().getTypedModel() == typedModel) {
						nodesInDomain.add(node);
					}
				}
				domainsNodesMap.put(typedModel, nodesInDomain);
			}
		}

		return domainsNodesMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainsNodesMap() {
		return domainsNodesMapESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<TypedModel, Domain> getTypedModelToDomainsMap() {

		if (typedModelToDomainsMap == null) {
			typedModelToDomainsMap = new HashMap<TypedModel, Domain>();
			//Iterator targetDomainModelIterator = ((TripleGraphGrammar)getTransformation()).getModelParameter().iterator();
			Iterator<Domain> domainGraphPatternIterator = getDomain().iterator();
			while (domainGraphPatternIterator.hasNext()) {
				DomainGraphPattern domainGraphPattern = (DomainGraphPattern) domainGraphPatternIterator.next();
				typedModelToDomainsMap.put(domainGraphPattern.getTypedModel(), domainGraphPattern);
			}

		}

		return typedModelToDomainsMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTypedModelToDomainsMap() {
		return typedModelToDomainsMapESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraint() {
		if (constraint == null) {
			constraint = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT, TggPackage.CONSTRAINT__TRIPLE_GRAPH_GRAMMAR_RULE);
		}
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Constraint> getAllConstraints() {
		if (getRefinedRule() == null) {
			return getConstraint();
		} else {
			EList<Constraint> allConstraintsList = new BasicEList<Constraint>(
					((TripleGraphGrammarRule) getRefinedRule()).getAllConstraints());
			allConstraintsList.addAll(getConstraint());
			return new EcoreEList.UnmodifiableEList<Constraint>(this,
					TggPackage.Literals.TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS, allConstraintsList.size(),
					allConstraintsList.toArray());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TripleGraphGrammarRule> getRulesAdvisedToApplyNext() {
		if (rulesAdvisedToApplyNext == null) {
			rulesAdvisedToApplyNext = new EObjectResolvingEList<TripleGraphGrammarRule>(TripleGraphGrammarRule.class, this, TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT);
		}
		return rulesAdvisedToApplyNext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DomainGraphPattern getNodesDomainGraphPattern(Node node) {
		for (Domain domain : getDomain()) {
			if (((DomainGraphPattern) domain).getNode().contains(node))
				return (DomainGraphPattern) domain;
		}
		if (getRefinedRule() != null)
			return ((TripleGraphGrammarRule) getRefinedRule()).getNodesDomainGraphPattern(node);
		else
			return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DomainGraphPattern getDomainGraphPattern(TypedModel typedModel) {
		for (Domain domain : getDomain()) {
			if (domain.getTypedModel().equals(typedModel))
				return (DomainGraphPattern) domain;
		}
		//else: no domainGraphPattern found for the TypedModel:
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getDomainsNodes(Domain domain) {
		return getDomainGraphPattern(domain.getTypedModel()).getNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTransformation((Transformation)otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDomain()).basicAdd(otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				if (overrides != null)
					msgs = ((InternalEObject)overrides).eInverseRemove(this, QvtbasePackage.RULE__OVERRIDDEN, Rule.class, msgs);
				return basicSetOverrides((Rule)otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOverridden()).basicAdd(otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraint()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				return basicSetTransformation(null, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				return ((InternalEList<?>)getDomain()).basicRemove(otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				return basicSetOverrides(null, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				return ((InternalEList<?>)getOverridden()).basicRemove(otherEnd, msgs);
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				return ((InternalEList<?>)getConstraint()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				return eInternalContainer().eInverseRemove(this, QvtbasePackage.TRANSFORMATION__RULE, Transformation.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				if (resolve) return getTransformation();
				return basicGetTransformation();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				return getDomain();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				if (resolve) return getOverrides();
				return basicGetOverrides();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				return getOverridden();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP:
				return getDomainsLeftSideNodesMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP:
				return getDomainsNodesMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP:
				return getTypedModelToDomainsMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				return getConstraint();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT:
				return isAbstract();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS:
				return getAllConstraints();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT:
				return getRulesAdvisedToApplyNext();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				setTransformation((Transformation)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				getDomain().clear();
				getDomain().addAll((Collection<? extends Domain>)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				setOverrides((Rule)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				getOverridden().clear();
				getOverridden().addAll((Collection<? extends Rule>)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				getConstraint().clear();
				getConstraint().addAll((Collection<? extends Constraint>)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT:
				getRulesAdvisedToApplyNext().clear();
				getRulesAdvisedToApplyNext().addAll((Collection<? extends TripleGraphGrammarRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				setTransformation((Transformation)null);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				getDomain().clear();
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				setOverrides((Rule)null);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				getOverridden().clear();
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				getConstraint().clear();
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT:
				getRulesAdvisedToApplyNext().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION:
				return basicGetTransformation() != null;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN:
				return domain != null && !domain.isEmpty();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES:
				return overrides != null;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN:
				return overridden != null && !overridden.isEmpty();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_LEFT_SIDE_NODES_MAP:
				return isSetDomainsLeftSideNodesMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAINS_NODES_MAP:
				return isSetDomainsNodesMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TYPED_MODEL_TO_DOMAINS_MAP:
				return isSetTypedModelToDomainsMap();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__CONSTRAINT:
				return constraint != null && !constraint.isEmpty();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__ALL_CONSTRAINTS:
				return !getAllConstraints().isEmpty();
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__RULES_ADVISED_TO_APPLY_NEXT:
				return rulesAdvisedToApplyNext != null && !rulesAdvisedToApplyNext.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Rule.class) {
			switch (derivedFeatureID) {
				case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION: return QvtbasePackage.RULE__TRANSFORMATION;
				case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN: return QvtbasePackage.RULE__DOMAIN;
				case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES: return QvtbasePackage.RULE__OVERRIDES;
				case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN: return QvtbasePackage.RULE__OVERRIDDEN;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Rule.class) {
			switch (baseFeatureID) {
				case QvtbasePackage.RULE__TRANSFORMATION: return TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__TRANSFORMATION;
				case QvtbasePackage.RULE__DOMAIN: return TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__DOMAIN;
				case QvtbasePackage.RULE__OVERRIDES: return TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDES;
				case QvtbasePackage.RULE__OVERRIDDEN: return TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE__OVERRIDDEN;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (domainsLeftSideNodesMap: ");
		if (domainsLeftSideNodesMapESet) result.append(domainsLeftSideNodesMap); else result.append("<unset>");
		result.append(", domainsNodesMap: ");
		if (domainsNodesMapESet) result.append(domainsNodesMap); else result.append("<unset>");
		result.append(", typedModelToDomainsMap: ");
		if (typedModelToDomainsMapESet) result.append(typedModelToDomainsMap); else result.append("<unset>");
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(')');
		return result.toString();
	}

} //TripleGraphGrammarRuleImpl