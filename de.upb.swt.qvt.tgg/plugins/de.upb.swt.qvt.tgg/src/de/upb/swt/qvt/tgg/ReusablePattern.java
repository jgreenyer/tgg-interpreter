/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reusable Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar <em>Graph Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getReusablePattern()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='AllNodesMustBeReusable'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL AllNodesMustBeReusable='self.node->forAll(n|n.isReusable())'"
 * @generated
 */
public interface ReusablePattern extends GraphPattern {
	/**
	 * Returns the value of the '<em><b>Graph Grammar</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getReusablePattern <em>Reusable Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Grammar</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Grammar</em>' container reference.
	 * @see #setGraphGrammar(GraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getReusablePattern_GraphGrammar()
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getReusablePattern
	 * @model opposite="reusablePattern" transient="false"
	 * @generated
	 */
	GraphGrammarRule getGraphGrammar();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.ReusablePattern#getGraphGrammar <em>Graph Grammar</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Grammar</em>' container reference.
	 * @see #getGraphGrammar()
	 * @generated
	 */
	void setGraphGrammar(GraphGrammarRule value);

} // ReusablePattern
