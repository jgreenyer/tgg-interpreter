/**
 * <copyright>
 * </copyright>
 *
 * $Id: IntegerLiteralConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Literal Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.IntegerLiteralConstraint#getIntegerValue <em>Integer Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getIntegerLiteralConstraint()
 * @model
 * @generated
 */
public interface IntegerLiteralConstraint extends PrimitiveLiteralConstraint {
	/**
	 * Returns the value of the '<em><b>Integer Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Value</em>' attribute.
	 * @see #setIntegerValue(int)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getIntegerLiteralConstraint_IntegerValue()
	 * @model
	 * @generated
	 */
	int getIntegerValue();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.IntegerLiteralConstraint#getIntegerValue <em>Integer Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Value</em>' attribute.
	 * @see #getIntegerValue()
	 * @generated
	 */
	void setIntegerValue(int value);

} // IntegerLiteralConstraint