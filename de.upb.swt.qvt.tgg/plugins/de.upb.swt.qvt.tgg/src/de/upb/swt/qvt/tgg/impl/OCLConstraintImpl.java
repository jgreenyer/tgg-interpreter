/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.expressions.ExpressionsFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.expressions.Variable;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.qvtbase.QvtbaseFactory;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OCL Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.OCLConstraintImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OCLConstraintImpl extends TGGConstraintImpl implements OCLConstraint {
	public static final String ANNOTATION_REQUIRED_NODES = "RequiredNodes";

	/**
	 * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected String expression = EXPRESSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OCLConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.OCL_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(String newExpression) {
		String oldExpression = expression;
		expression = newExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.OCL_CONSTRAINT__EXPRESSION, oldExpression, expression));
	}

	private void addObjectsBoundToSameRuleVariable(OCL ocl) {
		addNewVariable(ocl, "objectsBoundToSameRule", ocl.getEnvironment().getOCLStandardLibrary().getSet(), new BasicEList<EObject>());
	}

	private void addGloballyBoundObjectsVariable(OCL ocl) {
		addNewVariable(ocl, "globallyBoundObjects", ocl.getEnvironment().getOCLStandardLibrary().getSet(), new BasicEList<EObject>());
	}
	
	private void addNewVariable(OCL ocl, Node node, EObject eObject) {
		if (node.getName() != null && !node.getName().equals("")) {
			addNewVariable(ocl, node.getName(), node.getEType(), eObject);
		}
	}

	private void addNewVariable(OCL ocl, String variableName, EClassifier type, Object value) {
		for (Variable<?, ?> definedVariable : ocl.getEnvironment().getVariables()) {
			if (definedVariable.getName().equals(variableName))
				return;
		}
		Variable<EClassifier, EParameter> var = ExpressionsFactory.eINSTANCE.createVariable();
		var.setName(variableName);
		if (type == null) {
			if (value instanceof EObject) {
				type = ((EObject) value).eClass();
			} else if (value instanceof String) {
				type = EcorePackage.Literals.ESTRING;
			} else if (value instanceof Integer) {
				type = EcorePackage.Literals.EINTEGER_OBJECT;
			}
		}
		var.setType(type);
		assert (value != null);
		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) == null);
		ocl.getEnvironment().addElement(var.getName(), var, true);
		ocl.getEvaluationEnvironment().add(var.getName(), value);
		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) != null);
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String analyseConstraint() {
		TripleGraphGrammar tgg = (TripleGraphGrammar)getTripleGraphGrammarRule().getTransformation();

		OCL ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
		
		EList<Annotation> oclAnnotationList = tgg.getAnnotationsByKeyString("OCL");
		if (!oclAnnotationList.isEmpty()) {
			String uriString = oclAnnotationList.get(0).getValueString();
			URI tggURI = tgg.eResource().getURI();
			URI uri = tggURI.trimSegments(1).appendSegment(uriString);
			try {
				URL url = new URL(uri.toString());
				InputStream is = url.openStream();
				ocl.parse(new OCLInput(is));
				
			} catch (MalformedURLException e) {
				//logger.error(e);
			}
			catch (IOException e) {
				//logger.error(e);
			}
			catch (ParserException e) {
				//logger.error("There's a problem evaluating the OCL expressions: " + e);
			}
		}
		
		Helper helper = ocl.createOCLHelper();

		addObjectsBoundToSameRuleVariable(ocl);
		addGloballyBoundObjectsVariable(ocl);
		
		Variable<EClassifier, EParameter> self = ExpressionsFactory.eINSTANCE.createVariable();
		self.setType(getTripleGraphGrammarRule().eClass());
		ocl.getEnvironment().setSelfVariable(self);

		//add every node to the OCL environment
	    for (Node node : getTripleGraphGrammarRule().getNode()) {
	    	addNewVariable(ocl, node, /*node.getEType().getEPackage().getEFactoryInstance().create(node.getEType())*/ null);
		}

		//Now try evaluating. This will likely result in an exception, as all objects are null.
		//This is ok, unless we get a UnrecognizedVariableException, which means there is something wrong with the syntax.
		try {
			OCLExpression<EClassifier> expr = helper.createQuery(getExpression());
			ocl.evaluate(null, expr);
		} catch (ParserException e) {
			//if (e instanceof SemanticException && e.getMessage().contains("Unrecognized variable"))
				return e.getMessage();
		}

		//Everything is fine. Next, we try to find out which variable are required.
		List<Node> requiredNodes = new ArrayList<Node>();
	    for (Node node : getTripleGraphGrammarRule().getNode())  {
	    	if (node.getName() == null || node.getName().equals(""))
	    		continue;
			ocl.getEnvironment().deleteElement(node.getName());
			ocl.getEvaluationEnvironment().remove(node.getName());
			try {
				OCLExpression<EClassifier> expr = helper.createQuery(getExpression());
				ocl.evaluate(null, expr);
			} catch (ParserException e) {
				requiredNodes.add(node);
		    	addNewVariable(ocl, node, null);
			}
		}

	    // create the list of required nodes
	    Annotation annot = QvtbaseFactory.eINSTANCE.createAnnotation();
	    annot.setKey(ANNOTATION_REQUIRED_NODES);
	    annot.getValue().addAll(requiredNodes);
	    
	    //remove all previous annotations of the same kind
	    List<Annotation> toDelete = new ArrayList<Annotation>(1);
	    for (Annotation an : this.getAnnotations()) {
	    	if (an.getKey().equals(ANNOTATION_REQUIRED_NODES))
	    		toDelete.add(an);
	    }
		getAnnotations().removeAll(toDelete);
		getAnnotations().add(annot);
	    
		return "";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.OCL_CONSTRAINT__EXPRESSION:
				return getExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.OCL_CONSTRAINT__EXPRESSION:
				setExpression((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.OCL_CONSTRAINT__EXPRESSION:
				setExpression(EXPRESSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.OCL_CONSTRAINT__EXPRESSION:
				return EXPRESSION_EDEFAULT == null ? expression != null : !EXPRESSION_EDEFAULT.equals(expression);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (expression: ");
		result.append(expression);
		result.append(')');
		return result.toString();
	}

} //OCLConstraintImpl
