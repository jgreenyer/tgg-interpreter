/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggAdapterFactory.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.qvtbase.AnnotatedElement;
import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.ModelElement;
import de.upb.swt.qvt.qvtbase.NamedElement;
import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.ITransformAttribute;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.TggPackage
 * @generated
 */
public class TggAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TggPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TggPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TggSwitch<Adapter> modelSwitch = new TggSwitch<Adapter>() {
			@Override
			public Adapter caseGraph(Graph object) {
				return createGraphAdapter();
			}
			@Override
			public Adapter caseTripleGraphGrammarRule(TripleGraphGrammarRule object) {
				return createTripleGraphGrammarRuleAdapter();
			}
			@Override
			public Adapter caseGraphGrammarRule(GraphGrammarRule object) {
				return createGraphGrammarRuleAdapter();
			}
			@Override
			public Adapter caseGraphPattern(GraphPattern object) {
				return createGraphPatternAdapter();
			}
			@Override
			public Adapter caseNode(Node object) {
				return createNodeAdapter();
			}
			@Override
			public Adapter caseEdge(Edge object) {
				return createEdgeAdapter();
			}
			@Override
			public Adapter caseLeftGraphPattern(LeftGraphPattern object) {
				return createLeftGraphPatternAdapter();
			}
			@Override
			public Adapter caseRightGraphPattern(RightGraphPattern object) {
				return createRightGraphPatternAdapter();
			}
			@Override
			public Adapter caseDomainGraphPattern(DomainGraphPattern object) {
				return createDomainGraphPatternAdapter();
			}
			@Override
			public Adapter caseTripleGraphGrammar(TripleGraphGrammar object) {
				return createTripleGraphGrammarAdapter();
			}
			@Override
			public Adapter caseConstraint(Constraint object) {
				return createConstraintAdapter();
			}
			@Override
			public Adapter caseTGGConstraint(TGGConstraint object) {
				return createTGGConstraintAdapter();
			}
			@Override
			public Adapter caseGraphElement(GraphElement object) {
				return createGraphElementAdapter();
			}
			@Override
			public Adapter caseITransformAttribute(ITransformAttribute object) {
				return createITransformAttributeAdapter();
			}
			@Override
			public Adapter caseOCLConstraint(OCLConstraint object) {
				return createOCLConstraintAdapter();
			}
			@Override
			public Adapter caseTripleGraphGrammarAxiom(TripleGraphGrammarAxiom object) {
				return createTripleGraphGrammarAxiomAdapter();
			}
			@Override
			public Adapter caseReusablePattern(ReusablePattern object) {
				return createReusablePatternAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseModelElement(ModelElement object) {
				return createModelElementAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseRule(Rule object) {
				return createRuleAdapter();
			}
			@Override
			public Adapter caseDomain(Domain object) {
				return createDomainAdapter();
			}
			@Override
			public Adapter caseTransformation(Transformation object) {
				return createTransformationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.Graph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.Graph
	 * @generated
	 */
	public Adapter createGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule
	 * @generated
	 */
	public Adapter createTripleGraphGrammarRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.GraphGrammarRule <em>Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule
	 * @generated
	 */
	public Adapter createGraphGrammarRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.GraphPattern <em>Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.GraphPattern
	 * @generated
	 */
	public Adapter createGraphPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.Node
	 * @generated
	 */
	public Adapter createNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.Edge
	 * @generated
	 */
	public Adapter createEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.LeftGraphPattern <em>Left Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.LeftGraphPattern
	 * @generated
	 */
	public Adapter createLeftGraphPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.RightGraphPattern <em>Right Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.RightGraphPattern
	 * @generated
	 */
	public Adapter createRightGraphPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.DomainGraphPattern <em>Domain Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.DomainGraphPattern
	 * @generated
	 */
	public Adapter createDomainGraphPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammar <em>Triple Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammar
	 * @generated
	 */
	public Adapter createTripleGraphGrammarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.Constraint
	 * @generated
	 */
	public Adapter createConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.TGGConstraint <em>TGG Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.TGGConstraint
	 * @generated
	 */
	public Adapter createTGGConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.GraphElement <em>Graph Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.GraphElement
	 * @generated
	 */
	public Adapter createGraphElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.ITransformAttribute <em>ITransform Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.ITransformAttribute
	 * @generated
	 */
	public Adapter createITransformAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.OCLConstraint <em>OCL Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.OCLConstraint
	 * @generated
	 */
	public Adapter createOCLConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom <em>Triple Graph Grammar Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom
	 * @generated
	 */
	public Adapter createTripleGraphGrammarAxiomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.ReusablePattern <em>Reusable Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.ReusablePattern
	 * @generated
	 */
	public Adapter createReusablePatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.ModelElement
	 * @generated
	 */
	public Adapter createModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.Rule
	 * @generated
	 */
	public Adapter createRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.Domain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.Domain
	 * @generated
	 */
	public Adapter createDomainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.qvtbase.Transformation <em>Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.qvtbase.Transformation
	 * @generated
	 */
	public Adapter createTransformationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TggAdapterFactory
