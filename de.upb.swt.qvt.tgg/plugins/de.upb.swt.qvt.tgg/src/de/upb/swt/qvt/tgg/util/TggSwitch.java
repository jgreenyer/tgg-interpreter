/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggSwitch.java 1680 2006-07-20 10:55:15 +0000 (Do, 20 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.qvtbase.AnnotatedElement;
import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.ModelElement;
import de.upb.swt.qvt.qvtbase.NamedElement;
import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Graph;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.ITransformAttribute;
import de.upb.swt.qvt.tgg.LeftGraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.RightGraphPattern;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.TggPackage
 * @generated
 */
public class TggSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TggPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggSwitch() {
		if (modelPackage == null) {
			modelPackage = TggPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TggPackage.GRAPH: {
				Graph graph = (Graph)theEObject;
				T result = caseGraph(graph);
				if (result == null) result = caseNamedElement(graph);
				if (result == null) result = caseModelElement(graph);
				if (result == null) result = caseAnnotatedElement(graph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_RULE: {
				TripleGraphGrammarRule tripleGraphGrammarRule = (TripleGraphGrammarRule)theEObject;
				T result = caseTripleGraphGrammarRule(tripleGraphGrammarRule);
				if (result == null) result = caseGraphGrammarRule(tripleGraphGrammarRule);
				if (result == null) result = caseRule(tripleGraphGrammarRule);
				if (result == null) result = caseGraph(tripleGraphGrammarRule);
				if (result == null) result = caseNamedElement(tripleGraphGrammarRule);
				if (result == null) result = caseModelElement(tripleGraphGrammarRule);
				if (result == null) result = caseAnnotatedElement(tripleGraphGrammarRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.GRAPH_GRAMMAR_RULE: {
				GraphGrammarRule graphGrammarRule = (GraphGrammarRule)theEObject;
				T result = caseGraphGrammarRule(graphGrammarRule);
				if (result == null) result = caseGraph(graphGrammarRule);
				if (result == null) result = caseNamedElement(graphGrammarRule);
				if (result == null) result = caseModelElement(graphGrammarRule);
				if (result == null) result = caseAnnotatedElement(graphGrammarRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.GRAPH_PATTERN: {
				GraphPattern graphPattern = (GraphPattern)theEObject;
				T result = caseGraphPattern(graphPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.NODE: {
				Node node = (Node)theEObject;
				T result = caseNode(node);
				if (result == null) result = caseGraphElement(node);
				if (result == null) result = caseNamedElement(node);
				if (result == null) result = caseModelElement(node);
				if (result == null) result = caseAnnotatedElement(node);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.EDGE: {
				Edge edge = (Edge)theEObject;
				T result = caseEdge(edge);
				if (result == null) result = caseGraphElement(edge);
				if (result == null) result = caseNamedElement(edge);
				if (result == null) result = caseModelElement(edge);
				if (result == null) result = caseAnnotatedElement(edge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.LEFT_GRAPH_PATTERN: {
				LeftGraphPattern leftGraphPattern = (LeftGraphPattern)theEObject;
				T result = caseLeftGraphPattern(leftGraphPattern);
				if (result == null) result = caseGraphPattern(leftGraphPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.RIGHT_GRAPH_PATTERN: {
				RightGraphPattern rightGraphPattern = (RightGraphPattern)theEObject;
				T result = caseRightGraphPattern(rightGraphPattern);
				if (result == null) result = caseGraphPattern(rightGraphPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.DOMAIN_GRAPH_PATTERN: {
				DomainGraphPattern domainGraphPattern = (DomainGraphPattern)theEObject;
				T result = caseDomainGraphPattern(domainGraphPattern);
				if (result == null) result = caseDomain(domainGraphPattern);
				if (result == null) result = caseGraphPattern(domainGraphPattern);
				if (result == null) result = caseNamedElement(domainGraphPattern);
				if (result == null) result = caseModelElement(domainGraphPattern);
				if (result == null) result = caseAnnotatedElement(domainGraphPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.TRIPLE_GRAPH_GRAMMAR: {
				TripleGraphGrammar tripleGraphGrammar = (TripleGraphGrammar)theEObject;
				T result = caseTripleGraphGrammar(tripleGraphGrammar);
				if (result == null) result = caseTransformation(tripleGraphGrammar);
				if (result == null) result = caseNamedElement(tripleGraphGrammar);
				if (result == null) result = caseModelElement(tripleGraphGrammar);
				if (result == null) result = caseAnnotatedElement(tripleGraphGrammar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONSTRAINT: {
				Constraint constraint = (Constraint)theEObject;
				T result = caseConstraint(constraint);
				if (result == null) result = caseAnnotatedElement(constraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.TGG_CONSTRAINT: {
				TGGConstraint tggConstraint = (TGGConstraint)theEObject;
				T result = caseTGGConstraint(tggConstraint);
				if (result == null) result = caseConstraint(tggConstraint);
				if (result == null) result = caseAnnotatedElement(tggConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.GRAPH_ELEMENT: {
				GraphElement graphElement = (GraphElement)theEObject;
				T result = caseGraphElement(graphElement);
				if (result == null) result = caseNamedElement(graphElement);
				if (result == null) result = caseModelElement(graphElement);
				if (result == null) result = caseAnnotatedElement(graphElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.ITRANSFORM_ATTRIBUTE: {
				ITransformAttribute iTransformAttribute = (ITransformAttribute)theEObject;
				T result = caseITransformAttribute(iTransformAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.OCL_CONSTRAINT: {
				OCLConstraint oclConstraint = (OCLConstraint)theEObject;
				T result = caseOCLConstraint(oclConstraint);
				if (result == null) result = caseTGGConstraint(oclConstraint);
				if (result == null) result = caseConstraint(oclConstraint);
				if (result == null) result = caseAnnotatedElement(oclConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.TRIPLE_GRAPH_GRAMMAR_AXIOM: {
				TripleGraphGrammarAxiom tripleGraphGrammarAxiom = (TripleGraphGrammarAxiom)theEObject;
				T result = caseTripleGraphGrammarAxiom(tripleGraphGrammarAxiom);
				if (result == null) result = caseTripleGraphGrammarRule(tripleGraphGrammarAxiom);
				if (result == null) result = caseGraphGrammarRule(tripleGraphGrammarAxiom);
				if (result == null) result = caseRule(tripleGraphGrammarAxiom);
				if (result == null) result = caseGraph(tripleGraphGrammarAxiom);
				if (result == null) result = caseNamedElement(tripleGraphGrammarAxiom);
				if (result == null) result = caseModelElement(tripleGraphGrammarAxiom);
				if (result == null) result = caseAnnotatedElement(tripleGraphGrammarAxiom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.REUSABLE_PATTERN: {
				ReusablePattern reusablePattern = (ReusablePattern)theEObject;
				T result = caseReusablePattern(reusablePattern);
				if (result == null) result = caseGraphPattern(reusablePattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraph(Graph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triple Graph Grammar Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triple Graph Grammar Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTripleGraphGrammarRule(TripleGraphGrammarRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph Grammar Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph Grammar Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphGrammarRule(GraphGrammarRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphPattern(GraphPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEdge(Edge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Left Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Left Graph Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeftGraphPattern(LeftGraphPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Right Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Right Graph Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRightGraphPattern(RightGraphPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Domain Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Domain Graph Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDomainGraphPattern(DomainGraphPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triple Graph Grammar</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triple Graph Grammar</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTripleGraphGrammar(TripleGraphGrammar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstraint(Constraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGConstraint(TGGConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphElement(GraphElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITransform Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITransform Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITransformAttribute(ITransformAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OCL Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OCL Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOCLConstraint(OCLConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triple Graph Grammar Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triple Graph Grammar Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTripleGraphGrammarAxiom(TripleGraphGrammarAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reusable Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reusable Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReusablePattern(ReusablePattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelElement(ModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRule(Rule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Domain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Domain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDomain(Domain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transformation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transformation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransformation(Transformation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //TggSwitch
