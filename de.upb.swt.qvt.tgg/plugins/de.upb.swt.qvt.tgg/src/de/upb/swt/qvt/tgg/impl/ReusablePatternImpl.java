/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reusable Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.ReusablePatternImpl#getGraphGrammar <em>Graph Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReusablePatternImpl extends GraphPatternImpl implements ReusablePattern {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReusablePatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.REUSABLE_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphGrammarRule getGraphGrammar() {
		if (eContainerFeatureID() != TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR) return null;
		return (GraphGrammarRule)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphGrammar(GraphGrammarRule newGraphGrammar, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGraphGrammar, TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphGrammar(GraphGrammarRule newGraphGrammar) {
		if (newGraphGrammar != eInternalContainer() || (eContainerFeatureID() != TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR && newGraphGrammar != null)) {
			if (EcoreUtil.isAncestor(this, newGraphGrammar))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGraphGrammar != null)
				msgs = ((InternalEObject)newGraphGrammar).eInverseAdd(this, TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN, GraphGrammarRule.class, msgs);
			msgs = basicSetGraphGrammar(newGraphGrammar, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR, newGraphGrammar, newGraphGrammar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGraphGrammar((GraphGrammarRule)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				return basicSetGraphGrammar(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				return eInternalContainer().eInverseRemove(this, TggPackage.GRAPH_GRAMMAR_RULE__REUSABLE_PATTERN, GraphGrammarRule.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				return getGraphGrammar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				setGraphGrammar((GraphGrammarRule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				setGraphGrammar((GraphGrammarRule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.REUSABLE_PATTERN__GRAPH_GRAMMAR:
				return getGraphGrammar() != null;
		}
		return super.eIsSet(featureID);
	}

} //ReusablePatternImpl
