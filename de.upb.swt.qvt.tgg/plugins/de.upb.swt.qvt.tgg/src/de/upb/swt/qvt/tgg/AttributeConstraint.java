/**
 * <copyright>
 * </copyright>
 *
 * $Id: AttributeConstraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.AttributeConstraint#getValueNode <em>Value Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.AttributeConstraint#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.AttributeConstraint#getValueAttributeName <em>Value Attribute Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getAttributeConstraint()
 * @model abstract="true"
 * @generated
 */
public interface AttributeConstraint extends TGGConstraint {
	/**
	 * Returns the value of the '<em><b>Value Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Node</em>' reference.
	 * @see #setValueNode(Node)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getAttributeConstraint_ValueNode()
	 * @model required="true"
	 * @generated
	 */
	Node getValueNode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.AttributeConstraint#getValueNode <em>Value Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Node</em>' reference.
	 * @see #getValueNode()
	 * @generated
	 */
	void setValueNode(Node value);

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' reference.
	 * @see #setValueAttribute(EAttribute)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getAttributeConstraint_ValueAttribute()
	 * @model required="true"
	 * @generated
	 */
	EAttribute getValueAttribute();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.AttributeConstraint#getValueAttribute <em>Value Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Attribute</em>' reference.
	 * @see #getValueAttribute()
	 * @generated
	 */
	void setValueAttribute(EAttribute value);

	/**
	 * Returns the value of the '<em><b>Value Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute Name</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getAttributeConstraint_ValueAttributeName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getValueAttributeName();

} // AttributeConstraint
