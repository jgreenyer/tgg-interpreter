/**
 * <copyright>
 * </copyright>
 *
 * $Id: DomainGraphPatternImpl.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.impl.DomainImpl;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Graph Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl#getNode <em>Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.impl.DomainGraphPatternImpl#getEdge <em>Edge</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DomainGraphPatternImpl extends DomainImpl implements DomainGraphPattern {
	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> node;

	/**
	 * The cached value of the '{@link #getConstraint() <em>Constraint</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraint()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraint;

	/**
	 * The cached value of the '{@link #getEdge() <em>Edge</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdge()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> edge;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainGraphPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.DOMAIN_GRAPH_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Node> getNode() {
		if (node == null) {
			node = new EObjectWithInverseResolvingEList.ManyInverse<Node>(Node.class, this, TggPackage.DOMAIN_GRAPH_PATTERN__NODE, TggPackage.NODE__GRAPH_PATTERN);
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraint() {
		if (constraint == null) {
			constraint = new EObjectWithInverseResolvingEList<Constraint>(Constraint.class, this, TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT, TggPackage.CONSTRAINT__GRAPH_PATTERN);
		}
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getEdge() {
		if (edge == null) {
			edge = new EObjectWithInverseResolvingEList.ManyInverse<Edge>(Edge.class, this, TggPackage.DOMAIN_GRAPH_PATTERN__EDGE, TggPackage.EDGE__GRAPH_PATTERN);
		}
		return edge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNode()).basicAdd(otherEnd, msgs);
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraint()).basicAdd(otherEnd, msgs);
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEdge()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				return ((InternalEList<?>)getNode()).basicRemove(otherEnd, msgs);
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				return ((InternalEList<?>)getConstraint()).basicRemove(otherEnd, msgs);
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				return ((InternalEList<?>)getEdge()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				return getNode();
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				return getConstraint();
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				return getEdge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				getNode().clear();
				getNode().addAll((Collection<? extends Node>)newValue);
				return;
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				getConstraint().clear();
				getConstraint().addAll((Collection<? extends Constraint>)newValue);
				return;
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				getEdge().clear();
				getEdge().addAll((Collection<? extends Edge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				getNode().clear();
				return;
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				getConstraint().clear();
				return;
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				getEdge().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.DOMAIN_GRAPH_PATTERN__NODE:
				return node != null && !node.isEmpty();
			case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT:
				return constraint != null && !constraint.isEmpty();
			case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE:
				return edge != null && !edge.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GraphPattern.class) {
			switch (derivedFeatureID) {
				case TggPackage.DOMAIN_GRAPH_PATTERN__NODE: return TggPackage.GRAPH_PATTERN__NODE;
				case TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT: return TggPackage.GRAPH_PATTERN__CONSTRAINT;
				case TggPackage.DOMAIN_GRAPH_PATTERN__EDGE: return TggPackage.GRAPH_PATTERN__EDGE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GraphPattern.class) {
			switch (baseFeatureID) {
				case TggPackage.GRAPH_PATTERN__NODE: return TggPackage.DOMAIN_GRAPH_PATTERN__NODE;
				case TggPackage.GRAPH_PATTERN__CONSTRAINT: return TggPackage.DOMAIN_GRAPH_PATTERN__CONSTRAINT;
				case TggPackage.GRAPH_PATTERN__EDGE: return TggPackage.DOMAIN_GRAPH_PATTERN__EDGE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //DomainGraphPatternImpl