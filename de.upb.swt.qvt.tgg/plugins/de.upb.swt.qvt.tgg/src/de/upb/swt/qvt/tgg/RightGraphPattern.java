/**
 * <copyright>
 * </copyright>
 *
 * $Id: RightGraphPattern.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Right Graph Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar <em>Graph Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getRightGraphPattern()
 * @model
 * @generated
 */
public interface RightGraphPattern extends GraphPattern {
	/**
	 * Returns the value of the '<em><b>Graph Grammar</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern <em>Right Graph Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Grammar</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Grammar</em>' container reference.
	 * @see #setGraphGrammar(GraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getRightGraphPattern_GraphGrammar()
	 * @see de.upb.swt.qvt.tgg.GraphGrammarRule#getRightGraphPattern
	 * @model opposite="rightGraphPattern" required="true" transient="false"
	 * @generated
	 */
	GraphGrammarRule getGraphGrammar();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.RightGraphPattern#getGraphGrammar <em>Graph Grammar</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Grammar</em>' container reference.
	 * @see #getGraphGrammar()
	 * @generated
	 */
	void setGraphGrammar(GraphGrammarRule value);

} // RightGraphPattern