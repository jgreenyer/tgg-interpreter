/**
 * <copyright>
 * </copyright>
 *
 * $Id: Constraint.java 1520 2006-05-02 18:29:31 +0000 (Di, 02 Mai 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import de.upb.swt.qvt.qvtbase.AnnotatedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.Constraint#getGraphPattern <em>Graph Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getConstraint()
 * @model abstract="true"
 * @generated
 */
public interface Constraint extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Triple Graph Grammar Rule</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triple Graph Grammar Rule</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triple Graph Grammar Rule</em>' container reference.
	 * @see #setTripleGraphGrammarRule(TripleGraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getConstraint_TripleGraphGrammarRule()
	 * @see de.upb.swt.qvt.tgg.TripleGraphGrammarRule#getConstraint
	 * @model opposite="constraint" transient="false"
	 * @generated
	 */
	TripleGraphGrammarRule getTripleGraphGrammarRule();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Constraint#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Triple Graph Grammar Rule</em>' container reference.
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	void setTripleGraphGrammarRule(TripleGraphGrammarRule value);

	/**
	 * Returns the value of the '<em><b>Graph Pattern</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.GraphPattern#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Pattern</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Pattern</em>' reference.
	 * @see #setGraphPattern(GraphPattern)
	 * @see de.upb.swt.qvt.tgg.TggPackage#getConstraint_GraphPattern()
	 * @see de.upb.swt.qvt.tgg.GraphPattern#getConstraint
	 * @model opposite="constraint"
	 * @generated
	 */
	GraphPattern getGraphPattern();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.Constraint#getGraphPattern <em>Graph Pattern</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Pattern</em>' reference.
	 * @see #getGraphPattern()
	 * @generated
	 */
	void setGraphPattern(GraphPattern value);

} // Constraint