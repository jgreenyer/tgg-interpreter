/**
 * <copyright>
 * </copyright>
 *
 * $Id: TggFactory.java 1649 2006-07-07 09:22:27 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.TggPackage
 * @generated
 */
public interface TggFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TggFactory eINSTANCE = de.upb.swt.qvt.tgg.impl.TggFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Graph</em>'.
	 * @generated
	 */
	Graph createGraph();

	/**
	 * Returns a new object of class '<em>Triple Graph Grammar Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triple Graph Grammar Rule</em>'.
	 * @generated
	 */
	TripleGraphGrammarRule createTripleGraphGrammarRule();

	/**
	 * Returns a new object of class '<em>Graph Grammar Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Graph Grammar Rule</em>'.
	 * @generated
	 */
	GraphGrammarRule createGraphGrammarRule();

	/**
	 * Returns a new object of class '<em>Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Graph Pattern</em>'.
	 * @generated
	 */
	GraphPattern createGraphPattern();

	/**
	 * Returns a new object of class '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Node</em>'.
	 * @generated
	 */
	Node createNode();

	/**
	 * Returns a new object of class '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Edge</em>'.
	 * @generated
	 */
	Edge createEdge();

	/**
	 * Returns a new object of class '<em>Left Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Left Graph Pattern</em>'.
	 * @generated
	 */
	LeftGraphPattern createLeftGraphPattern();

	/**
	 * Returns a new object of class '<em>Right Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Right Graph Pattern</em>'.
	 * @generated
	 */
	RightGraphPattern createRightGraphPattern();

	/**
	 * Returns a new object of class '<em>Domain Graph Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Domain Graph Pattern</em>'.
	 * @generated
	 */
	DomainGraphPattern createDomainGraphPattern();

	/**
	 * Returns a new object of class '<em>Triple Graph Grammar</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triple Graph Grammar</em>'.
	 * @generated
	 */
	TripleGraphGrammar createTripleGraphGrammar();

	/**
	 * Returns a new object of class '<em>OCL Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Constraint</em>'.
	 * @generated
	 */
	OCLConstraint createOCLConstraint();

	/**
	 * Returns a new object of class '<em>Triple Graph Grammar Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triple Graph Grammar Axiom</em>'.
	 * @generated
	 */
	TripleGraphGrammarAxiom createTripleGraphGrammarAxiom();

	/**
	 * Returns a new object of class '<em>Reusable Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reusable Pattern</em>'.
	 * @generated
	 */
	ReusablePattern createReusablePattern();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TggPackage getTggPackage();

} //TggFactory
