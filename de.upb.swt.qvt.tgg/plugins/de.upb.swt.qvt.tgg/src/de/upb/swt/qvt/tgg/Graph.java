/**
 * <copyright>
 * </copyright>
 *
 * $Id: Graph.java 1600 2006-06-13 10:33:21 +0000 (Di, 13 Jun 2006) jgreen $
 */
package de.upb.swt.qvt.tgg;

import org.eclipse.emf.common.util.EList;

import de.upb.swt.qvt.qvtbase.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.Graph#getNode <em>Node</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.TggPackage#getGraph()
 * @model
 * @generated
 */
public interface Graph extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.Node#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.TggPackage#getGraph_Node()
	 * @see de.upb.swt.qvt.tgg.Node#getGraph
	 * @model opposite="graph" containment="true"
	 * @generated
	 */
	EList<Node> getNode();

} // Graph