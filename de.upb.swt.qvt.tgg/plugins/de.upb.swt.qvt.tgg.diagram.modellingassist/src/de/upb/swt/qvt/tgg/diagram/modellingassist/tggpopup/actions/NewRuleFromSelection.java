package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class NewRuleFromSelection implements IObjectActionDelegate {

	private Shell shell;
	
	private ISelection selection;
	
	/**
	 * Constructor for Action1.
	 */
	public NewRuleFromSelection() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		final boolean copyToContext = action.getId().equals("de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions.NewRuleContextFromSelection");
		final boolean createRefinedRule = action.getId().equals("de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions.NewRefinedRuleFromSelection");
		

		/*if (!MessageDialog.openQuestion(shell,"Save Changes?", "Would you like to save the new rule (and all previous changes)?"))
			return;
		// Now save the content.
		try {
			containingGraph.eResource().save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		// Instantiates and initializes the wizard
		TggNewDiagramFromExistingDiagramWizard wizard = new TggNewDiagramFromExistingDiagramWizard(selection, copyToContext, createRefinedRule);
		//IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(containingGraph.eResource().getURI().toPlatformString(true)));
		//wizard.init(PlatformUI.getWorkbench(), new StructuredSelection(file));
	    // Instantiates the wizard container with the wizard and opens it
	    WizardDialog dialog = new WizardDialog(shell, wizard);
	    dialog.create();
	    dialog.open();
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
