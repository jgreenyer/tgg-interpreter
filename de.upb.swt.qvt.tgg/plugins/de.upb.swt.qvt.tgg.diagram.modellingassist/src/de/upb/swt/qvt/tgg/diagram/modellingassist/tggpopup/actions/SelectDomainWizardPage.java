package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class SelectDomainWizardPage extends WizardPage {
	
	private TripleGraphGrammarRule tggRule;
	
	Button domain1;
	Button domain2;
	Button domain3;
	
	protected SelectDomainWizardPage(String pageName, TripleGraphGrammarRule tggRule) {
		super(pageName);
		this.setTitle("Select the domain");
		this.tggRule=tggRule;
	}

	@Override
	public void createControl(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout gl = new GridLayout();
		int ncol = 4;
		gl.numColumns = ncol;
		composite.setLayout(gl);

		// 1. Domain
		domain1 = new Button(composite, SWT.RADIO);
		domain1.setText(tggRule.getDomain().get(0).getTypedModel().getName());
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = ncol;
		domain1.setLayoutData(gd);
		domain1.setSelection(true);

		// 2. Domain
		domain2 = new Button(composite, SWT.RADIO);
		domain2.setText(tggRule.getDomain().get(1).getTypedModel().getName());
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = ncol;
		domain2.setLayoutData(gd);
		domain2.setSelection(false);
		
		// 3. Domain
		domain3 = new Button(composite, SWT.RADIO);
		domain3.setText(tggRule.getDomain().get(2).getTypedModel().getName());
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = ncol;
		domain3.setLayoutData(gd);
		domain3.setSelection(false);

		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(composite);
	}
	
	public String getDomain(){
		if (domain1.getSelection()) return domain1.getText();
		if (domain2.getSelection()) return domain2.getText();
		if (domain3.getSelection()) return domain3.getText();
		return "";
	}
}
