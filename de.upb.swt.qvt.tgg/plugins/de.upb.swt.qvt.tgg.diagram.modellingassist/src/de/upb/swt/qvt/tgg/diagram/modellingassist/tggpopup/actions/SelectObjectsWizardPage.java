package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class SelectObjectsWizardPage extends WizardPage {
	
	private ComposedAdapterFactory adapterFactory;
	private SelectResourceWizardPage resourcePage;
	private TripleGraphGrammarRule tggRule;
	
	/**
	 * Constructs this DiagramModelSelectionPage.
	 * @param resourceSet 
	 * 
	 * @param pageId
	 *            The ID for this Page.
	 * @param diagramElementValidator
	 *            The Validator that can check, if the current selection is a
	 *            valid Diagram Element.
	 * @param domainModelSelectionPage 
	 * @param canSelectMultipleElements
	 * @param tgg
	 */
	public SelectObjectsWizardPage(String pageName, SelectResourceWizardPage resourcePage, TripleGraphGrammarRule tgg) {
		super(pageName);
		this.resourcePage=resourcePage;
		this.tggRule = tgg;
	}


	/**
	 * The top level control for this Page Extension.
	 */
	private Composite plate;

	private CheckboxTreeViewer modelViewer;
	
	private Button selectAllButton;

	/**
	 * Create the controls for this Page Extension.
	 */
	@Override
	public void createControl(Composite parent) {
		
		adapterFactory = createAdapterFactory();

		// create the top level control
		plate = new Composite(parent, SWT.MULTI);
		plate.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		plate.setLayout(layout);

		// create the label
		Label label = new Label(plate, SWT.NONE);
		label.setText("test");
		label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));

		// create the tree viewer
		int style = SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.MULTI;

		modelViewer = new CheckboxTreeViewer(plate, style);

		GridData layoutData = new GridData(GridData.FILL_BOTH);
		layoutData.heightHint = 180;
		layoutData.widthHint = 200;
		modelViewer.getTree().setLayoutData(layoutData);

		modelViewer.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));
		modelViewer.addCheckStateListener(new ICheckStateListener() {
			
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (modelViewer.getCheckedElements().length > 0) {
					if (modelViewer.getCheckedElements().length == getAllItemsCount(modelViewer.getTree())) {
						selectAllButton.setSelection(true);
						selectAllButton.setGrayed(false);
					}
					else {
						selectAllButton.setSelection(true);
						selectAllButton.setGrayed(true);
					}
				}
				else {
					selectAllButton.setSelection(false);
					selectAllButton.setGrayed(false);
				}
			}
		});
		
		selectAllButton = new Button(plate, SWT.CHECK);
		selectAllButton.setText("Select/Unselect all");
		/* Make the button toggle between three states */
		selectAllButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent (Event e) {
				if (selectAllButton.getSelection()) {
					modelViewer.setSubtreeChecked(modelViewer.getTree().getItem(0).getData(), true);
					//modelViewer.setAllChecked(true);
					selectAllButton.setGrayed(false);
				} else {
					modelViewer.setSubtreeChecked(modelViewer.getTree().getItem(0).getData(), false);
					//modelViewer.setAllChecked(false);
					selectAllButton.setGrayed(false);
				}
			}
		});
		
		setControl(plate);
	}
	
	private int getAllItemsCount(Tree tree) {
		int count = tree.getItemCount();
		for (TreeItem ti : tree.getItems()) {
			count += getAllItemsCount(ti);
		}
		return count;
	}
	private int getAllItemsCount(TreeItem tree) {
		int count = tree.getItemCount();
		for (TreeItem ti : tree.getItems()) {
			count += getAllItemsCount(ti);
		}
		return count;
	}

	protected ComposedAdapterFactory createAdapterFactory() {
		ArrayList<AdapterFactory> factories = new ArrayList<AdapterFactory>();
		fillItemProviderFactories(factories);
		return new ComposedAdapterFactory(factories);
	}

	/**
	 * @generated
	 */
	protected void fillItemProviderFactories(List<AdapterFactory> factories) {
		factories.add(new ResourceItemProviderAdapterFactory());
		factories.add(new ReflectiveItemProviderAdapterFactory());
	}
	
	public void setResource(Resource resource) {
		if (resource != null && modelViewer.getInput() != resource) {
			modelViewer.setInput(resource);
			if (!resource.getContents().isEmpty()) {
				EObject firstElement = resource.getContents().get(0);
				modelViewer.setSelection(new StructuredSelection(firstElement));
			}
			modelViewer.expandAll();
		}
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			
			ResourceSet resourceSet = tggRule.eResource().getResourceSet();
			
			//redirectResourceSetRegisteredPackagesToTGGUsedPackages((TripleGraphGrammar) tggRule.getTransformation(), resourceSet);

			// Get the resource
			Resource resource = resourceSet.getResource(
					URI.createURI(resourcePage.getURI()), true);	

			setResource(resource);
		}
	}

	public Object[] getObjects(){
		return modelViewer.getCheckedElements();
	}
	
//	private void redirectResourceSetRegisteredPackagesToTGGUsedPackages(TripleGraphGrammar tgg, ResourceSet resourceSet){
//		Iterator<TypedModel> typedModelIterator = tgg.getAllModelParameters().iterator();
//		while (typedModelIterator.hasNext()) {
//			TypedModel typedModel = (TypedModel) typedModelIterator.next();
//			Iterator<EPackage> usedPackageIterator = typedModel.getUsedPackage().iterator();
//			while (usedPackageIterator.hasNext()) {
//				EPackage usedPackage = (EPackage) usedPackageIterator.next();
//				redirectResourceSetRegisteredPackageToUsedPackage(usedPackage, resourceSet);
//			}
//		}
//	}
//	
//	
//	private static void redirectResourceSetRegisteredPackageToUsedPackage(EPackage usedPackage, ResourceSet resourceSet) {
//
//		//move to the topmost package and use that 
//		while (usedPackage.getESuperPackage() != null)
//			usedPackage = usedPackage.getESuperPackage();
//		
////		System.out.println("usedPackage.getNsURI():                   " + usedPackage.getNsURI().toString());
////		System.out.println("usedPackage.eResource().getURI():         " + usedPackage.eResource().getURI());
////		System.out.println("resourceSet.getPackageRegistry().get(..): " + resourceSet.getPackageRegistry().getEPackage(usedPackage.getNsURI().toString()));
////		
////		System.out.println("replacing...");
//		resourceSet.getPackageRegistry().put(usedPackage.getNsURI().toString(), usedPackage);
////		System.out.println("resourceSet.getPackageRegistry().get(..): " + resourceSet.getPackageRegistry().getEPackage(usedPackage.getNsURI().toString()));
////
////		System.out.println("-------");
//		
//	}
	
}
