package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class CopyObjectsFromExistingModelWizard extends Wizard implements IImportWizard {
	
	SelectResourceWizardPage resourcePage;
	SelectObjectsWizardPage objectsPage;
	SelectDomainWizardPage domainPage;	
	Object[] objects;
	String domain;
	
	public CopyObjectsFromExistingModelWizard(TripleGraphGrammarRule tggRule) {
		super();
		resourcePage = new SelectResourceWizardPage("Existing model");
		objectsPage = new SelectObjectsWizardPage("Objects", resourcePage, tggRule);
		domainPage = new SelectDomainWizardPage("Domain",tggRule);
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	public boolean performFinish() {
		objects = objectsPage.getObjects();
		domain = domainPage.getDomain();
        return true;
	}
	 
	public Object[] getObjects()
	{
		return objects;
	}
	
	public String getDomain()
	{
		return domain;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setNeedsProgressMonitor(true);		
	}
	
	/* (non-Javadoc)
     * @see org.eclipse.jface.wizard.IWizard#addPages()
     */
    public void addPages() {
        super.addPages();  
        addPage(resourcePage);  
        addPage(objectsPage);
        addPage(domainPage);
    }

}
