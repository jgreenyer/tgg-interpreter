package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.provider.EcoreEditPlugin;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ListSelectionDialog;

import de.upb.swt.qvt.qvtbase.Transformation;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class NewCorrespondenceType implements IObjectActionDelegate {

	private Shell shell;
	
	private ISelection selection;
	
	private List<Node> nodeList;
	
	/**
	 * Constructor for Action1.
	 */
	public NewCorrespondenceType() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		nodeList = new BasicEList<Node>();
		
		for (Iterator<?> selectionIterator = ((StructuredSelection)selection).iterator(); selectionIterator.hasNext();) {
			GraphicalEditPart graphicalEditPart = (GraphicalEditPart) selectionIterator.next();
			Object modelElement = ((org.eclipse.gmf.runtime.notation.Node)graphicalEditPart.getModel()).getElement();
			if (modelElement != null &&  modelElement instanceof Node){
				nodeList.add((Node) modelElement);
			}
		}
		
		if (nodeList.isEmpty()){
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in",
					"There was no node selected please select one or more nodes to create a new corresdondence type and node.");
			return;
		}

		ListSelectionDialog listSelectionDialog = new ListSelectionDialog(shell, nodeList.get(0).getGraph().getNode(), 
				new IStructuredContentProvider(){

					@Override
					public Object[] getElements(Object inputElement) {
						if (inputElement instanceof Collection<?>) return ((Collection<?>)inputElement).toArray();
						else return null;
					}

					@Override
					public void dispose() {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void inputChanged(Viewer viewer, Object oldInput,
							Object newInput) {
						// TODO Auto-generated method stub
						
					}
				},
				new LabelProvider(),
				"Choose");
		
		listSelectionDialog.setInitialElementSelections(nodeList);
		
		if (listSelectionDialog.open() == ListSelectionDialog.CANCEL){
			return;
		}
		
		InputDialog inputDialog = new InputDialog(shell, 
				"Correspondence Type Name", 
				"Please specify a name for the new correspondence class",
				"NewCorrespondenceClassName", null);

		if (inputDialog.open() == InputDialog.CANCEL){
			return;
		}

		//add it to the TGG's correspondence package.
		final TripleGraphGrammarRule tggRule = ((TripleGraphGrammarRule)nodeList.get(0).getGraph());
		
		ElementTreeSelectionDialog elementTreeSelectionDialog = new ElementTreeSelectionDialog(shell,
				new LabelProvider(){
					public Image getImage(Object element) {
						if (element instanceof EPackage && EcoreEditPlugin.INSTANCE.getImage("full/obj16/EPackage") instanceof Image)
						return (Image) EcoreEditPlugin.INSTANCE.getImage("full/obj16/EPackage");
						else return null;
					}
				},
				new ITreeContentProvider(){

					@Override
					public Object[] getChildren(Object parentElement) {
						if (parentElement instanceof TypedModel) {
							TypedModel parentTypedModel = (TypedModel) parentElement;
							return parentTypedModel.getUsedPackage().toArray();
						}
						return null;
					}

					@Override
					public Object getParent(Object element) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public boolean hasChildren(Object element) {
						if (element instanceof TypedModel) {
							TypedModel parentTypedModel = (TypedModel) element;
							return !parentTypedModel.getUsedPackage().isEmpty();
						}
						return false;
					}

					@Override
					public Object[] getElements(Object inputElement) {
						if (inputElement instanceof Transformation) {
							Transformation transformation = (Transformation) inputElement;
							return transformation.getAllModelParameters().toArray();
						}
						return null;
					}

					@Override
					public void dispose() {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void inputChanged(Viewer viewer, Object oldInput,
							Object newInput) {
						// TODO Auto-generated method stub
						
					}
			
		});
		
		elementTreeSelectionDialog.setMessage("Select the correspondence package into which you want to add the new correspondence type class");
		elementTreeSelectionDialog.setTitle("Select the correspondence package");
		elementTreeSelectionDialog.setInput(tggRule.getTransformation());
		
		if (elementTreeSelectionDialog.open() == ElementTreeSelectionDialog.CANCEL){
			return;
		}

		EPackage selectedEPackage_temp = null;
		try {
			selectedEPackage_temp = (EPackage) elementTreeSelectionDialog.getResult()[0];

		} catch (ClassCastException e) {
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in",
					"You did not select and EPackage.");
			return;
		} catch (ArrayIndexOutOfBoundsException e) {
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in",
					"You made no selection.");
			return;
		}
		final EPackage selectedEPackage = selectedEPackage_temp;

		
		/*
		 * All information has been gathered to create a correspondence node and it's corresponding type class:
		 * 1. Create the type class and add it to the selected package
		 * 2. Create a new node and add it to the rule as a produced node
		 * 3. Create EReferences for all the selected nodes
		 * 4. Create produced edges to the selected nodes that are typed over the corresponding references.
		 */
		
		TransactionalEditingDomain transactionalEditingDomain = ((GraphicalEditPart)((StructuredSelection)selection).getFirstElement()).getEditingDomain();
		
		
		final String enteredCorrespondenceClassName = inputDialog.getValue();
		final Object[] selectedNodes = listSelectionDialog.getResult();

		AbstractTransactionalCommand createCorrespondenceNodeAndTypeClassAndEdgesCommand = new AbstractTransactionalCommand(transactionalEditingDomain, 
				"createCorrespondenceType", null){

			
			@Override
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {


				EClass newCorrespondenceClass = EcoreFactory.eINSTANCE.createEClass();
				newCorrespondenceClass.setName(enteredCorrespondenceClassName);

				//1.a. Create generic containment element and reference.
				/*
				 * If not already existing, we insert an abstract correspondence element that can
				 * contain an arbitrary number of its own kind. We check if not already an abstract
				 * correspondence class 'AbstractContainerCorrespondenceNode' is present. If this
				 * is not the case, we create it and add it as a super type to all classes in the 
				 * package. We skip this step if the class is already present.
				 * 
				 * Next, we add the 'AbstractContainerCorrespondenceNode' class as a super type to
				 * the new correspondence node.
				 */
					EClass abstractContainerCorrespondenceNode;
					
					if (selectedEPackage.getEClassifier("AbstractContainerCorrespondenceNode") == null) {
						abstractContainerCorrespondenceNode = EcoreFactory.eINSTANCE.createEClass();
						selectedEPackage.getEClassifiers().add(abstractContainerCorrespondenceNode);
						abstractContainerCorrespondenceNode.setName("AbstractContainerCorrespondenceNode");
						abstractContainerCorrespondenceNode.setAbstract(true);
						EReference childCorresp = EcoreFactory.eINSTANCE.createEReference();
						childCorresp.setName("childCorresp");
						childCorresp.setContainment(true);
						childCorresp.setUpperBound(-1);
						childCorresp.setEType(abstractContainerCorrespondenceNode);
						abstractContainerCorrespondenceNode.getEStructuralFeatures().add(childCorresp);
						for (EClassifier eClassifier : selectedEPackage.getEClassifiers()) {
							if (eClassifier instanceof EClass && !eClassifier.getName().equals("AbstractContainerCorrespondenceNode"))
								((EClass)eClassifier).getESuperTypes().add(abstractContainerCorrespondenceNode);
						}
					}else{
						abstractContainerCorrespondenceNode = (EClass) selectedEPackage.getEClassifier("AbstractContainerCorrespondenceNode");
					}

					newCorrespondenceClass.getESuperTypes().add(abstractContainerCorrespondenceNode);
					
				selectedEPackage.getEClassifiers().add(newCorrespondenceClass);

				//2. create a new produced Node typed over the newly created class
				Node newCorrespondenceNode = TggFactory.eINSTANCE.createNode();
				newCorrespondenceNode.setEType(newCorrespondenceClass);
				tggRule.getNode().add(newCorrespondenceNode);
				tggRule.getRightGraphPattern().getNode().add(newCorrespondenceNode);
				
				for (TypedModel typedModel : tggRule.getTransformation().getAllModelParameters()) {
					if (typedModel.getUsedPackage().contains(selectedEPackage)){
						((DomainGraphPattern)tggRule.getTypedModelToDomainsMap().get(typedModel)).getNode().add(newCorrespondenceNode);
					}
				}
				
				List<String> referenceNamesList = new ArrayList<String>();
				
				//3. For all the selected nodes, add the references and produced edges typed over the references.
				for (int i = 0; i < selectedNodes.length; i++) {
					if (selectedNodes[i] instanceof Node) {
						Node selectedNode = (Node) selectedNodes[i];
						
						// good idea, but names get too long: String referenceNameString = newCorrespondenceClass.getName() + "2";
						String referenceNameString = "";
						
						if (!(selectedNode.getName() == null) && !selectedNode.getName().equals("")) referenceNameString += selectedNode.getName();
						else referenceNameString += selectedNode.getEType().getName();

						
						if (referenceNamesList.contains(referenceNameString)){ // name has already been given
							int j = 1;
							while(referenceNamesList.contains(referenceNameString.concat("_" + j))){
								//find a unique name:
								j++;
							}
							referenceNameString.concat("_" + j);
						}
						
						referenceNamesList.add(referenceNameString);

						EReference newReference = EcoreFactory.eINSTANCE.createEReference();
						Edge newEdge = TggFactory.eINSTANCE.createEdge();
						
						newReference.setEType(selectedNode.getEType());
						newReference.setName(referenceNameString);

						newEdge.setTypeReference(newReference);
						newEdge.setSourceNode(newCorrespondenceNode);
						newEdge.setTargetNode(selectedNode);
						
						newCorrespondenceClass.getEStructuralFeatures().add(newReference);
						tggRule.getRightGraphPattern().getEdge().add(newEdge);
					}
				}

				
				return CommandResult.newOKCommandResult();
			}
			
		};
		

		
		try {
			/*IStatus status =*/ OperationHistoryFactory.getOperationHistory().execute(createCorrespondenceNodeAndTypeClassAndEdgesCommand, new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in", "An error occured while executing the command: \n" + 
					e.getMessage());
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
