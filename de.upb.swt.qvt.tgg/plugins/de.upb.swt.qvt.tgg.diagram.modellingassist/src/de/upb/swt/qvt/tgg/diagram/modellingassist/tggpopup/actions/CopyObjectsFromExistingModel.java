package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import java.util.HashMap;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public class CopyObjectsFromExistingModel implements IObjectActionDelegate {

	private Shell shell;
	
	private ISelection selection;
		
	private TripleGraphGrammarRule tggRule;
	
	HashMap<EObject,Node> objectToNode;
	
	/**
	 * Constructor for Action1.
	 */
	public CopyObjectsFromExistingModel() {
		super();
	}
	

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		tggRule = getContainingGraph((EditPart) ((StructuredSelection)selection).getFirstElement());

		CopyObjectsFromExistingModelWizard wizard = new CopyObjectsFromExistingModelWizard(tggRule);
		WizardDialog dialog = new WizardDialog(shell, wizard);
		dialog.create();
		dialog.open();		
			
		String domain = wizard.getDomain();
		
		// get the domain graph pattern
		TypedModel typedModel = null;			
		for(Domain d : tggRule.getDomain())
		{
			if (d.getTypedModel().getName().equals(domain))
				typedModel = d.getTypedModel();
		}		
		final DomainGraphPattern domainGraphPattern = tggRule.getDomainGraphPattern(typedModel);
	
		// get the selected objects
		final EList<EObject> objects = new BasicEList<EObject>();		
		for(Object obj : wizard.getObjects())
			objects.add((EObject) obj);
	
		TransactionalEditingDomain transactionalEditingDomain = ((GraphicalEditPart)((StructuredSelection)selection).getFirstElement()).getEditingDomain();		
		
		AbstractTransactionalCommand copyObjectsFromExistingModel = new AbstractTransactionalCommand(transactionalEditingDomain, 
				"newObjects", null){
			
			@Override
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {							
				
				objectToNode = new HashMap<EObject,Node>();
				
				for (EObject n : objects) {
					Node newNode = TggFactory.eINSTANCE.createNode();
					newNode.setEType(n.eClass());
					newNode.setRight(true);
					
					EList<EAttribute> allAttributes = n.eClass().getEAllAttributes();
					
					String name = null;
					
					// set attributes
					if (allAttributes.size() > 0){
						for (EAttribute attr : allAttributes) {
							if (n.eGet(attr) != null) {
								//if object has a name, use it as a name for the node
								if (attr.getName().equals("name")) {
									name = n.eGet(attr).toString();
								}
								
								//for each attribute generate a constraint
								OCLConstraint con = TggFactory.eINSTANCE
										.createOCLConstraint();
								con.setSlotNode(newNode);
								con.setSlotAttribute(attr);
								
								if(attr.getEType().getName().equals("EString"))
									con.setExpression("'"+n.eGet(attr).toString()+"'");
								else
									con.setExpression(n.eGet(attr).toString());								
								
								tggRule.getConstraint().add(con);
							}
						}
					}
					if (name != null)
						newNode.setName(name);
					
					tggRule.getRightGraphPattern().getNode().add(newNode);	
					newNode.setRight(true);
					tggRule.getNode().add(newNode);

					domainGraphPattern.getNode().add(newNode);
					
					objectToNode.put(n, newNode);	
				}			

				for (EObject n1 : objects) {

					EList<EReference> allReferences = n1.eClass().getEAllReferences();

					for (EReference ref : allReferences) {
						if (!ref.isDerived() && n1.eGet(ref) != null) {
							Object obj1 = n1.eGet(ref);
							if (obj1 instanceof EList) {
								for (Object obj2 : (EList<?>) obj1) {
									if (objects.contains(obj2)) {
										createNewEdge(n1, obj2, ref);
									}
								}
							} else {
								if (objects.contains(obj1)) {
									createNewEdge(n1, obj1, ref);
								}
							}

						}
					}
				}
				return CommandResult.newOKCommandResult();

			} 
			
		};
		try {
			/*IStatus status =*/ OperationHistoryFactory.getOperationHistory().execute(copyObjectsFromExistingModel, new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in", "An error occured while executing the command: \n" + 
					e.getMessage());
		}			
	}
	
	private TripleGraphGrammarRule getContainingGraph(EditPart editPart) {
		//go to the root of the edit part tree and get the rule from this edit part.
		if (editPart != null) {
			editPart = editPart.getRoot();
			if ((((EditPart)editPart.getChildren().get(0)).getModel() instanceof View) &&
					(((View)((EditPart)editPart.getChildren().get(0)).getModel()).getElement() instanceof GraphGrammarRule))
				return (TripleGraphGrammarRule)((View)((EditPart)editPart.getChildren().get(0)).getModel()).getElement();
			else
				return null;
		} else
			return null;
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

	public void createNewEdge(Object source, Object target, EReference ref) {
		Edge newEdge = TggFactory.eINSTANCE.createEdge();

		newEdge.setSourceNode(objectToNode.get(source));
		newEdge.setTargetNode(objectToNode.get(target));
		newEdge.setRight(true);

		newEdge.setTypeReference(ref);

		tggRule.getRightGraphPattern().getEdge().add(newEdge);
	}
}

