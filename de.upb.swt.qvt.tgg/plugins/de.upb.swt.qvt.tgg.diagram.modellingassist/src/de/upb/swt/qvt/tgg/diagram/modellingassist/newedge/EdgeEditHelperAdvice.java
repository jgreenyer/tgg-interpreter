package de.upb.swt.qvt.tgg.diagram.modellingassist.newedge;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.menus.PopupMenu;
import org.eclipse.gmf.runtime.diagram.ui.menus.PopupMenu.CascadingMenu;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateRelationshipCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import de.upb.swt.qvt.qvtbase.Domain;
import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.providers.TggElementTypes;

public class EdgeEditHelperAdvice extends AbstractEditHelperAdvice {

	private class MapContainingLists <K, V>{
		private Map<K, List<V>> map;
		public Map<K, List<V>> getMap() {
			if (map == null)
				map = new HashMap<K, List<V>>();
			return map;
		}
		public List<V> getList(K object, boolean create) {
			List<V> result = getMap().get(object);
			if (result == null && create) {
				result = new Vector<V>();
				getMap().put(object, result);
			}
			return result;
		}
	}
	
	
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
//		System.out.println("getAfterConfigureCommand(ConfigureRequest " + request);
//		System.out.println("request.getClientContext() " + request.getClientContext());
//		System.out.println("request.getEditHelperContext() " + request.getEditHelperContext());
//		System.out.println("request.getElementsToEdit() " + request.getElementsToEdit());
//		System.out.println("Edge: " + ((Edge)request.getElementsToEdit().get(0)).getTargetNode());
		
		
		CompositeCommand compositeCommand = new CompositeCommand("Edge Configuring");

		Edge edge = (Edge)(request.getElementToConfigure());
		MapContainingLists<EClass, EReference> possibleReferenceList = new MapContainingLists<EClass, EReference>();
		Node sourceNode = edge.getSourceNode();
		Node targetNode = edge.getTargetNode();
		
		EClass targetNodeTypeClass = null;
		//EReference edgeTypeReference = null;

		//handle possible illegal state.
		if (targetNode == null || sourceNode == null || edge.getTypeReference() != null){
			return null;
		}

		EClass selectedTypeClass = null;
		EReference selectedReference = null;

		/*
		 * *** CASE 1: ***
		 * the target node may not have a type, i.e. if a new node is created along with a new edge.
		 */
		if (targetNode.getEType() == null) {
			
			EList<CascadingMenu> cascadingMenuList = new BasicEList<CascadingMenu>();
			
			
			// for all references of the source node's class...
			for (EReference reference : (sourceNode.getEType()).getEAllReferences()) {

				EList<EClass> targetClassList = new BasicEList<EClass>();
				
				//...find all classes of all used packages of all typed models that may fit this reference...
				for (Domain domain : ((TripleGraphGrammarRule)sourceNode.getGraph()).getDomain()) {
					for (EPackage ePackage : domain.getTypedModel().getUsedPackage()) {
						for (EClassifier eClassifier : ePackage.getEClassifiers()) {
							System.out.println("-- " + eClassifier);
							if (eClassifier instanceof EClass && 
									(((EClass)reference.getEType()).isSuperTypeOf(((EClass)eClassifier)) 
											|| "EObject".equals(reference.getEType().getName())) // if the edge type reference is EObject, it fits any target node type, but EObject may not be element of every target node type class' EAllSuperTypes.
							){ 
								targetClassList.add((EClass) eClassifier);
							}
						}
					}
				}

				// sorting results alphabetically.
				java.util.Collections.sort(targetClassList, new Comparator<EClass>(){
					@Override
					public int compare(EClass o1, EClass o2) {
						return o1.getName().compareTo(o2.getName());
					}
					
				});
				
				/* ... add them to a list that we want to display in the popup menu. 
				 * The popup shall contain a nested menu of 
				 * 			(1) the list of outgoing references and
				 * 						(2) the list of "fitting" target classes.
				 */
				cascadingMenuList.add(new CascadingMenu(reference, new PopupMenu(targetClassList, new EClassPopupLabelProvider())));
			}
			
			PopupMenu selectReferenceAndTargetNodeClassPopupMenu = new PopupMenu(cascadingMenuList, new EdgePopupLabelProvider());
			selectReferenceAndTargetNodeClassPopupMenu.show(Display.getCurrent().getActiveShell());
			
			Object resultList = selectReferenceAndTargetNodeClassPopupMenu.getResult();

			if (resultList == null || !(resultList instanceof List<?>) || ((List<?>)resultList).isEmpty())
				return null;
			
			//from a cascading menu, we retrieve a list as the result which contains 
			//(1) the selected reference and (2) the selected compatible class
			selectedReference = (EReference) ((List<?>)resultList).get(0);
			selectedTypeClass = (EClass) ((List<?>)resultList).get(1);
			
			//in order to create a command to change the value of the edited model elements, we need to create such a request first,
			// (1) we create a command to set the edge's type reference
			SetRequest setTypeReferenceRequest = new SetRequest(edge, edge.eClass().getEStructuralFeature("typeReference"), selectedReference);
			compositeCommand.add(new SetValueCommand(setTypeReferenceRequest));					
			
			// (1) we create a command to set the target node's type class
			SetRequest setNodesTypeClassRequest = new SetRequest(edge.getTargetNode(), edge.getTargetNode().eClass().getEStructuralFeature("eType"), selectedTypeClass);
			compositeCommand.add(new SetValueCommand(setNodesTypeClassRequest));

			/*
			 * We have to associate the new target node to a domain graph pattern of the rule.
			 * First, we determine if this new node could be associated to the same
			 * domain graph pattern as the source node.
			 * 
			 * If this is not the case, we find another suitable domain graph pattern
			 * 
			 * TODO: jrieke: Shouldn't we skip this check and immediately search for all domain 
			 * possibilities?
			 */
			List<Domain> selectedDomains = new Vector<Domain>();
			Domain selectedDomain = null;

			boolean typeClassExistsInSameDomain = false;

			DomainGraphPattern sourceNodesDomainGraphPattern = null;
			for (GraphPattern gp : sourceNode.getGraphPattern()) {
				if (gp instanceof DomainGraphPattern) {
					sourceNodesDomainGraphPattern = (DomainGraphPattern) gp;
				}
			}
			
			if (sourceNodesDomainGraphPattern == null)
				return compositeCommand;
			for (EPackage ePackage : sourceNodesDomainGraphPattern.getTypedModel().getUsedPackage()) {
				if (ePackage.getEClassifiers().contains(selectedTypeClass)){
					typeClassExistsInSameDomain = true;
					selectedDomains.add(sourceNodesDomainGraphPattern);
				}
			}
			
			// find another suitable domain graph pattern.
			
			if (!typeClassExistsInSameDomain){
				for (Domain domain : ((TripleGraphGrammarRule)sourceNode.getGraph()).getDomain()) {
					for (EPackage ePackage : domain.getTypedModel().getUsedPackage()) {
						if (ePackage.getEClassifiers().contains(selectedTypeClass)){
							selectedDomains.add(domain);
							break;
						}
					}
				}
			}
			
			// another popup menu if several domains/typed models are possible. 
			
			if (selectedDomains.size() > 1) {
				PopupMenu selectDomainGraphPatternPopupMenu = new PopupMenu(selectedDomains, new DomainPopupLabelProvider());
				selectDomainGraphPatternPopupMenu.show(Display.getCurrent().getActiveShell());
				
				selectedDomain = (Domain) selectDomainGraphPatternPopupMenu.getResult();
			}
			else
				selectedDomain = selectedDomains.get(0);

			if (selectedDomain == null || !(selectedDomain instanceof DomainGraphPattern))
				return compositeCommand;
			
			DomainGraphPattern domainGraphPattern = (DomainGraphPattern)selectedDomain;

			//create the association to the domain graph pattern
			SetRequest setNodesDomainGraphPatternRequest = new SetRequest(edge.getTargetNode(), edge.getTargetNode().eClass().getEStructuralFeature("graphPattern"), domainGraphPattern);
			compositeCommand.add(new SetValueCommand(setNodesDomainGraphPatternRequest));
			
			targetNodeTypeClass = selectedTypeClass;
			//edgeTypeReference = selectedReference;
 		
		}else
		/*
		 * *** CASE 2: ***
		 * if the target node exists and has a type
		 */
		{ 
		
			targetNodeTypeClass = targetNode.getEType();

			EList<CascadingMenu> cascadingMenuList = new BasicEList<CascadingMenu>();

			if (sourceNode.getEType() == null) {
				// If source node does not exist, use the cross referencer.
				Vector<Resource> reslist = new Vector<Resource>();
				for (Domain domain : ((TripleGraphGrammarRule)sourceNode.getGraph()).getDomain()) {
					for (EPackage ePackage : domain.getTypedModel().getUsedPackage()) {
						if (!reslist.contains(ePackage.eResource())) 
							reslist.add(ePackage.eResource());
					}
				}
				
				BasicEList<EClass> classHierachy = new BasicEList<EClass>();
				classHierachy.add(targetNodeTypeClass);
				classHierachy.addAll(targetNodeTypeClass.getEAllSuperTypes());
				
				Map<EObject, Collection<Setting>> crlist = new EcoreUtil.UsageCrossReferencer(reslist){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected boolean crossReference(EObject eObject,
							EReference eReference,
							EObject crossReferencedEObject) {
						return super.crossReference(eObject, eReference, crossReferencedEObject);
					}

					@Override
					public Map<EObject, Collection<Setting>> findAllUsage(
							Collection<?> eObjectsOfInterest) {
						return super.findAllUsage(eObjectsOfInterest);
					}

					
				}.findAllUsage(classHierachy);

				// find all possible references for an edge between the source and target node...
				for (Collection<Setting> settinglist : crlist.values()) {
					for (Setting setting : settinglist) {
						EObject eObj = setting.getEObject();
						if (eObj instanceof EReference) {
							EClass eClass = ((EReference) eObj).getEContainingClass();
							if (eClass != null &&
								!possibleReferenceList.getList(eClass, true).contains(((EReference) eObj)))
								possibleReferenceList.getList(eClass, true).add((EReference) eObj);
						}
					}
				}
				
				//add all additional fitting references from superclasses
				for (Map.Entry<EClass, List<EReference>> entry : possibleReferenceList.getMap().entrySet()) {
					for (EReference reference : entry.getKey().getEAllReferences()) {
						if (classHierachy.contains(reference.getEType())
								&& !entry.getValue().contains(reference)){
							entry.getValue().add(reference);
						}
					}
				}
				
				
			} else {
				// for all possible references for an edge between the source and target node...
				for (EReference reference : (sourceNode.getEType()).getEAllReferences()) {
					if (reference.getEType() == null){					
						showWarning("There is an untyped reference in your model: " + reference);
						return null;
					} 
					//...find all classes that may fit this reference...
					//TODO: Bug here?
					if (reference.getEType().equals(targetNodeTypeClass) || 
							((targetNodeTypeClass.getEAllSuperTypes() != null)
							&& targetNodeTypeClass.getEAllSuperTypes().contains(reference.getEType()))
							&& !possibleReferenceList.getList(sourceNode.getEType(), true).contains(reference)
							|| 	"EObject".equals(reference.getEType().getName()) // if the edge type reference is EObject, it fits any target node type, but EObject may not be element of targetNodeTypeClass.getEAllSuperTypes()   
							){
						possibleReferenceList.getList(sourceNode.getEType(), true).add(reference);
					}
				}
			}
			
			int size = possibleReferenceList.getMap().keySet().size();
			if (size == 0){
				//not good
				showWarning("There may be no edge between the selected source and target nodes");
			}

			// if there is just one possible class, use a simple popup menu
			if (size == 1) {
				Map.Entry<EClass, List<EReference>> entry = possibleReferenceList.getMap().entrySet().iterator().next();
				selectedTypeClass = entry.getKey();
				List<EReference> list = entry.getValue();
				if (list.size() == 0) {
					//should not happen
					showWarning("There may be no edge between the selected source and target nodes");
				} else if (list.size() == 1) {
					// if there is just one possible reference, use it as the edge's type reference
					selectedReference = list.get(0);
				} else {
					 // if there is more than one possible class open a simple popup to let the user choose.
					PopupMenu selectReferencePopupMenu = new PopupMenu(list, new EdgePopupLabelProvider());
					
					selectReferencePopupMenu.show(Display.getCurrent().getActiveShell());
					selectedReference = (EReference)selectReferencePopupMenu.getResult();
				}
			} else // if there is more than one possible class open a cascading popup to let the user choose.
			{
				/* ... add them to a list that we want to display in a cascading popup menu. 
				 * The popup shall contain a nested menu of 
				 *		(1) the list of "fitting" source classes and
				 * 			(2) the list of references.
				 */
				for (Map.Entry<EClass, List<EReference>> entry : possibleReferenceList.getMap().entrySet()) {
					cascadingMenuList.add(new CascadingMenu(entry.getKey(), new PopupMenu(entry.getValue(), new EdgePopupLabelProvider())));
				}

				PopupMenu selectReferenceAndSourceNodeClassPopupMenu = new PopupMenu(cascadingMenuList, new EClassPopupLabelProvider());
				selectReferenceAndSourceNodeClassPopupMenu.show(Display.getCurrent().getActiveShell());

				Object resultList = selectReferenceAndSourceNodeClassPopupMenu.getResult();
				if (resultList == null || !(resultList instanceof List<?>) || ((List<?>)resultList).isEmpty())
					return null;
				
				//from a cascading menu, we retrieve a list as the result which contains 
				//(1) the selected reference and (2) the selected compatible class
				selectedTypeClass = (EClass) ((List<?>)resultList).get(0);
				selectedReference = (EReference) ((List<?>)resultList).get(1);
			}
			

			if (sourceNode.getEType() == null) {
				SetRequest setNodeTypeRequest = new SetRequest(sourceNode, TggPackage.Literals.NODE__ETYPE, selectedTypeClass);
				compositeCommand.add(new SetValueCommand(setNodeTypeRequest));					
			}
			SetRequest setRequest = new SetRequest(edge, edge.eClass().getEStructuralFeature("typeReference"), selectedReference);
			compositeCommand.add(new SetValueCommand(setRequest));					

			if (targetNode.getDomainGraphPattern() == null){
			
				//Add a connection to a domain graph pattern.
				List<Domain> selectedDomains = new Vector<Domain>();
				Domain selectedDomain = null;
	
				//Search for all applicable domain graph patterns
				for (Domain domain : ((TripleGraphGrammarRule)targetNode.getGraph()).getDomain()) {
					for (EPackage ePackage : domain.getTypedModel().getUsedPackage()) {
						if (ePackage.getEClassifiers().contains(selectedTypeClass)){
							selectedDomains.add(domain);
							break;
						}
					}
				}
				
				if (selectedDomains.size() > 1) {
					// Several possibilities, ask user 
					PopupMenu selectDomainGraphPatternPopupMenu = new PopupMenu(selectedDomains, new DomainPopupLabelProvider());
					selectDomainGraphPatternPopupMenu.show(Display.getCurrent().getActiveShell());
					
					selectedDomain = (Domain) selectDomainGraphPatternPopupMenu.getResult();
				}
				else
					selectedDomain = selectedDomains.get(0);
	
				if (selectedDomain == null || !(selectedDomain instanceof DomainGraphPattern))
					return compositeCommand;
				
				DomainGraphPattern domainGraphPattern = (DomainGraphPattern)selectedDomain;
	
				//Create the link to the domain graph pattern
				SetRequest setNodesDomainGraphPatternRequest = new SetRequest(sourceNode, sourceNode.eClass().getEStructuralFeature("graphPattern"), domainGraphPattern);
				compositeCommand.add(new SetValueCommand(setNodesDomainGraphPatternRequest));
			}
		}

		/*
		 * Automatically create the OPPOSITE EDGE in case that the selected type reference has an eOpposite
		 */
		// if user has chosen a value from the popup (and has for example not canceled the selection)
		if (selectedReference != null){
			// if there selected reference has an opposite reference...
			if (selectedReference.getEOpposite() != null){
				//1. Is there a already an corresponding backward edge between the nodes?
				Edge backwardEdge = null;
				for (Edge outgoingEdge : targetNode.getOutgoingEdge()){
					if (outgoingEdge.getTargetNode() != null
							&& outgoingEdge.getTargetNode().equals(sourceNode)
							&& outgoingEdge.getTypeReference() != null
							&& outgoingEdge.getTypeReference().getEOpposite().equals(selectedReference)){
						backwardEdge = outgoingEdge;
					}
				}
				//if not create it.
				if (backwardEdge == null){

					final ConfigureRequest configureRequest = request;
		 						
					CreateRelationshipRequest createRelationshipRequest = 
						new CreateRelationshipRequest(
								request.getEditingDomain(), 
								targetNode, 
								targetNode, 
								sourceNode, 
								TggElementTypes.Edge_4001,
								(EReference) targetNode.eClass().getEStructuralFeature("outgoingEdge"));
					CreateRelationshipCommand createRelationshipCommand = new CreateRelationshipCommand(createRelationshipRequest){

						@Override
						protected CommandResult doExecuteWithResult(
								IProgressMonitor monitor,
								IAdaptable info)
								throws ExecutionException {
							
							CommandResult commandResult = super.doExecuteWithResult(monitor, info);
							
							Edge fwdEdge = 	((Edge)configureRequest.getElementToConfigure());
							
							Edge bwEdge = ((Edge)getNewElement());
															
							bwEdge.setTargetNode((Node) getTarget());
							bwEdge.setTypeReference(fwdEdge.getTypeReference().getEOpposite());
							
							// setting its left and right association to the same as the forward edge will do as default
							bwEdge.setLeft(fwdEdge.isLeft());
							bwEdge.setRight(fwdEdge.isRight());

							return commandResult;
						}
						
						@Override
						public boolean canExecute() {
							return true;
						}
					};
					
					compositeCommand.add(createRelationshipCommand);
				
				}
					
			}
		}


		return compositeCommand;
			

	}
	
	private void showWarning(String msg){
		MessageDialog.openWarning(
				new Shell(),
				"TGG Editor Plug-in",
				msg);
	}

	protected class EdgePopupLabelProvider extends LabelProvider{
		@Override
		public String getText(Object element) {
			if (element instanceof EReference){
				EReference ref = (EReference)element;
				return ref.getName() + " [" + (ref.getEContainingClass()!=null?ref.getEContainingClass().getName():"") + "->" + ref.getEReferenceType().getName() + "]";
			} else {				
				return super.getText(element);
			}
		}
	}

	protected class EClassPopupLabelProvider extends LabelProvider{
		@Override
		public String getText(Object element) {
			if (element instanceof EClass){
				return ((EClass)element).getName();
			}else{
					return super.getText(element);
			}
		}
	}
	
	protected class DomainPopupLabelProvider extends LabelProvider{
		@Override
		public String getText(Object element) {
			if (element instanceof Domain){
				String name = ((Domain)element).eIsSet(QvtbasePackage.Literals.NAMED_ELEMENT__NAME)
					&& !((Domain)element).getName().equals("")
							?((Domain)element).getName()
							:((Domain)element).getTypedModel().getName();
				return "Connect to Domain Graph Pattern " + name;
			}else{
					return super.getText(element);
			}
		}
	}
}
