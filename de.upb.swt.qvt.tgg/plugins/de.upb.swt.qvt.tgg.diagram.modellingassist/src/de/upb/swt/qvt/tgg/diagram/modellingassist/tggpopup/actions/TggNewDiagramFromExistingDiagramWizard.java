package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import de.upb.swt.qvt.tgg.DomainGraphPattern;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphGrammarRule;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.part.Messages;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorUtil;
import de.upb.swt.qvt.tgg.diagram.part.TggVisualIDRegistry;

public class TggNewDiagramFromExistingDiagramWizard extends Wizard {

	private WizardNewFileCreationPage myFileCreationPage;

	private WizardEnterNamePage myEnterNamePage;

	private ISelection selection;

	private TransactionalEditingDomain transactionalEditingDomain;

	private GraphGrammarRule containingGraph;

	private boolean copyToContext;
	
	private boolean createRefinedRule;

	private DiagramEditPart diagramEditPart;

	public TggNewDiagramFromExistingDiagramWizard(ISelection selection, boolean copyToContext, boolean createRefinedRule) {
		if (selection == null) 
			throw new IllegalArgumentException("Selection must not be null."); //$NON-NLS-1$
		this.selection = selection;
		
		this.copyToContext = copyToContext;
		this.createRefinedRule = createRefinedRule;
		
		containingGraph = getContainingGraph((EditPart) ((StructuredSelection)selection).getFirstElement());
		if (containingGraph == null) 
			throw new IllegalArgumentException("Unable to get containingGraph."); //$NON-NLS-1$

		transactionalEditingDomain = getTransactionalEditingDomain((EditPart) ((StructuredSelection)selection).getFirstElement());
		if (transactionalEditingDomain == null) 
			throw new IllegalArgumentException("Unable to get transactionalEditingDomain."); //$NON-NLS-1$

		diagramEditPart = getDiagramEditPart((EditPart) ((StructuredSelection)selection).getFirstElement());
		if (diagramEditPart == null) 
			throw new IllegalArgumentException("Unable to get diagramEditPart."); //$NON-NLS-1$
		
		IPath filePath;
		//String fileName = URI.decode(domainModelURI.trimFileExtension().lastSegment());
		URI tggRuleURI = containingGraph.eResource().getURI();
		if (tggRuleURI.isPlatformResource()) {
			filePath = new Path(tggRuleURI.trimSegments(1).toPlatformString(true));
		} else if (tggRuleURI.isFile()) {
			filePath = new Path(tggRuleURI.trimSegments(1).toFileString());
		} else {
			// TODO : use some default path
			throw new IllegalArgumentException("Unsupported URI: " + containingGraph); //$NON-NLS-1$
		}

		myFileCreationPage = new WizardNewFileCreationPage(Messages.TggNewDiagramFileWizard_CreationPageName,
				StructuredSelection.EMPTY);
		myFileCreationPage.setTitle(Messages.TggNewDiagramFileWizard_CreationPageTitle);
		myFileCreationPage.setDescription(NLS.bind(Messages.TggNewDiagramFileWizard_CreationPageDescription,
				TripleGraphGrammarRuleEditPart.MODEL_ID));
		myFileCreationPage.setContainerFullPath(filePath);
		
		myEnterNamePage = new WizardEnterNamePage("TGGRuleName", myFileCreationPage, filePath);
		myEnterNamePage.setTitle("TGG Rule Name");
		myEnterNamePage.setDescription("Please specify a name for the new TGG rule:");

		//myFileCreationPage.setFileName(TggDiagramEditorUtil.getUniqueFileName(filePath, proposedFileName, "tgg_diagram")); //$NON-NLS-1$
	}

	public void addPages() {
		addPage(myEnterNamePage);
		addPage(myFileCreationPage);
	}

	public boolean performFinish() {
		final boolean createDiagram = (this.getContainer().getCurrentPage().equals(myFileCreationPage));
		List<IFile> affectedFiles = new LinkedList<IFile>();
		Resource diagramResourceNF = null;
		URI diagramModelURI = null;
		if (createDiagram) {
			IFile diagramFile = myFileCreationPage.createNewFile();
			TggDiagramEditorUtil.setCharset(diagramFile);
			affectedFiles.add(diagramFile);
			diagramModelURI = URI.createPlatformResourceURI(diagramFile.getFullPath().toString(), true);
			ResourceSet resourceSet = transactionalEditingDomain.getResourceSet();
			diagramResourceNF = resourceSet.createResource(diagramModelURI);
		}
		final Resource diagramResource = diagramResourceNF;

		/*
		 * All information has been gathered to create a new rule based on the selection above.
		 * 1. Create a new TGG rule with the specified name.
		 * 2. Create the domain graph patterns to the current rule and associate the new nodes accordingly.
		 * 3. Create context nodes based on the selected nodes in the current rule.
		 * 4. Create the edges which exist between the nodes (create nodes if they don't exists yet).
		 * 5. Create constraints.
		 */
		// * 0. Sort selected elements that should be processed
		final List<Node> nodeList = new Vector<Node>();
		final List<Edge> edgeList = new Vector<Edge>();
		final List<OCLConstraint> constraintList = new Vector<OCLConstraint>(); 
		final List<DomainGraphPattern> dgpList = new Vector<DomainGraphPattern>();
		final GraphGrammarRule baseRule = containingGraph;
		
		Iterator<?> it = ((StructuredSelection)selection).iterator();
		while (it.hasNext()) {
			Object selectedElement = it.next();
			EditPart selectedEditPart;
			Object modelElement;
			if (selectedElement instanceof EditPart)
				selectedEditPart = (EditPart)selectedElement;
			else
				continue;
			if (selectedEditPart.getModel() instanceof View)
				modelElement = ((View)selectedEditPart.getModel()).getElement();
			else
				continue;
			
			
			if (modelElement instanceof Node)
				nodeList.add((Node) modelElement);
			else if (modelElement instanceof Edge && !edgeList.contains((Edge) modelElement))
				edgeList.add((Edge) modelElement);
			else if (modelElement instanceof OCLConstraint)
				constraintList.add((OCLConstraint) modelElement); 
			else if (modelElement instanceof DomainGraphPattern)
				dgpList.add((DomainGraphPattern) modelElement); 
			
			//Automatically add egdes between selected nodes, but not when creating a refined rule
			if (!createRefinedRule) {
				for (Node node : nodeList) {
					for (Edge outgoingEdge : node.getOutgoingEdge()) {
						if (nodeList.contains(outgoingEdge.getTargetNode())
								&& !edgeList.contains(outgoingEdge)) edgeList.add(outgoingEdge);
					}
				}
			}
		}
		
		// a map that is build up during the creation of the new elements. It links the existing elements to their new counterparts.
		final Map<EObject, EObject> selectedElementsToCreatedElementsMap = new HashMap<EObject, EObject>();
		
		AbstractTransactionalCommand createNewRuleCommand = new AbstractTransactionalCommand(transactionalEditingDomain,
				Messages.TggNewDiagramFileWizard_InitDiagramCommand, affectedFiles) {

			
			@Override
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {


				// * 1. Create a new TGG rule with the specified name.
				TripleGraphGrammarRule newTGGRule = TggFactory.eINSTANCE.createTripleGraphGrammarRule();
				newTGGRule.setName(myEnterNamePage.getName());
				TripleGraphGrammarRule currentTGGRule = (TripleGraphGrammarRule)containingGraph;
				currentTGGRule.getTransformation().getRule().add(newTGGRule);
				newTGGRule.setLeftGraphPattern(TggFactory.eINSTANCE.createLeftGraphPattern());
				newTGGRule.setRightGraphPattern(TggFactory.eINSTANCE.createRightGraphPattern());
				if (createRefinedRule)
					newTGGRule.setRefinedRule(baseRule);
								
				// * 2. Create the domain graph patterns to the current rule and associate the new nodes accordingly.
				for (DomainGraphPattern domain : dgpList) {
					cloneDomain(selectedElementsToCreatedElementsMap, newTGGRule, domain);
				}
				
				// * 3. Create context nodes based on the selected nodes in the current rule.
				for (Node selectedNode : nodeList) {
					cloneNode(selectedElementsToCreatedElementsMap, newTGGRule,
							selectedNode);
				}
				
				// * 4. Create the edges which exist between the nodes (create nodes if they don't exists yet).
				for (Edge currentEdge : edgeList) {
					if (!selectedElementsToCreatedElementsMap.containsKey(currentEdge.getSourceNode())) {
						cloneNode(selectedElementsToCreatedElementsMap, newTGGRule, currentEdge.getSourceNode());
					}
					if (!selectedElementsToCreatedElementsMap.containsKey(currentEdge.getTargetNode())) {
						cloneNode(selectedElementsToCreatedElementsMap, newTGGRule, currentEdge.getTargetNode());
					}
					
					cloneEdge(copyToContext,
							selectedElementsToCreatedElementsMap, newTGGRule,
							currentEdge);
				}

				// * 5. Create constraints.
				for (OCLConstraint currentConstraint : constraintList) {
					if (currentConstraint instanceof TGGConstraint) {
						cloneConstraint(selectedElementsToCreatedElementsMap, newTGGRule, (OCLConstraint)currentConstraint);
					}
				}
				
				return CommandResult.newOKCommandResult(newTGGRule);
			}

			/**
			 * @param selectedElementsToCreatedElementsMap
			 * @param newTGGRule
			 * @param currentConstraint
			 */
			private void cloneConstraint(
					Map<EObject, EObject> selectedElementsToCreatedElementsMap,
					TripleGraphGrammarRule newTGGRule,
					OCLConstraint currentConstraint) {
				Node slotNode = ((TGGConstraint)currentConstraint).getSlotNode();
				Node newSlotNode = null;
				if (slotNode != null) {
					newSlotNode = (Node) selectedElementsToCreatedElementsMap.get(slotNode);
					if (newSlotNode == null)
						newSlotNode = cloneNode(selectedElementsToCreatedElementsMap, newTGGRule, slotNode);
				}

				OCLConstraint newConstraint = TggFactory.eINSTANCE.createOCLConstraint();
				newConstraint.setCheckonly(currentConstraint.isCheckonly());
				newConstraint.setSlotNode(newSlotNode);
				newConstraint.setSlotAttribute(currentConstraint.getSlotAttribute());
				newConstraint.setExpression(currentConstraint.getExpression());
				newTGGRule.getConstraint().add(newConstraint);
				selectedElementsToCreatedElementsMap.put(currentConstraint, newConstraint);
			}

			/**
			 * @param copyToContext
			 * @param selectedElementsToCreatedElementsMap
			 * @param newTGGRule
			 * @param currentEdge
			 */
			private Edge cloneEdge(final boolean copyToContext,
					Map<EObject, EObject> selectedElementsToCreatedElementsMap,
					TripleGraphGrammarRule newTGGRule, Edge currentEdge) {
				// (a) create the edge, (b) set the name and type, (c) add it to the corresponding source and target nodes
				// (d) to the left and right graph pattern
				Edge newEdge = TggFactory.eINSTANCE.createEdge();
				newEdge.setTypeReference(currentEdge.getTypeReference());
				if (currentEdge.getName() != null) newEdge.setName(currentEdge.getName());
				newEdge.setSourceNode((Node) selectedElementsToCreatedElementsMap.get(currentEdge.getSourceNode()));
				newEdge.setTargetNode((Node) selectedElementsToCreatedElementsMap.get(currentEdge.getTargetNode()));
				newEdge.setSetModifier(currentEdge.getSetModifier());
				if (copyToContext || currentEdge.isLeft())
					newTGGRule.getLeftGraphPattern().getEdge().add(newEdge);
				if (copyToContext || currentEdge.isRight())
					newTGGRule.getRightGraphPattern().getEdge().add(newEdge);
				selectedElementsToCreatedElementsMap.put(currentEdge, newEdge);
				return newEdge;
			}

			/**
			 * @param copyToContext
			 * @param selectedElementsToCreatedElementsMap
			 * @param newTGGRule
			 * @param selectedNode
			 * @return
			 */
			private Node cloneNode(Map<EObject, EObject> selectedElementsToCreatedElementsMap,
					TripleGraphGrammarRule newTGGRule, Node selectedNode) {
				// (a) create the node, (b) set the name and type, (c) add it to the rule, 
				// (d) to the left and right graph pattern, (e) add it to the right domain 
				//  graph pattern (create one if it does not exists).
				Node newNode = TggFactory.eINSTANCE.createNode();
				if (selectedNode.getName() != null) newNode.setName(selectedNode.getName());
				newNode.setEType(selectedNode.getEType());
				newNode.setMatchSubtype(selectedNode.isMatchSubtype());
				newNode.setRefinedNode(selectedNode.getRefinedNode());
				newTGGRule.getNode().add(newNode);
				if (copyToContext || selectedNode.isLeft())
					newTGGRule.getLeftGraphPattern().getNode().add(newNode);
				if (copyToContext || selectedNode.isRight())
					newTGGRule.getRightGraphPattern().getNode().add(newNode);
				if (createRefinedRule)
					newNode.setRefinedNode(selectedNode);
				
				DomainGraphPattern oldDGP = selectedNode.getDomainGraphPattern();
				DomainGraphPattern newDGP = (DomainGraphPattern) selectedElementsToCreatedElementsMap.get(selectedNode.getDomainGraphPattern());
				if (newDGP == null) {
					newDGP = cloneDomain(selectedElementsToCreatedElementsMap, newTGGRule, oldDGP);
					selectedElementsToCreatedElementsMap.put(oldDGP, newDGP);
				}
				newDGP.getNode().add(newNode);
				selectedElementsToCreatedElementsMap.put(selectedNode, newNode);
				return newNode;
			}

			/**
			 * @param selectedElementsToCreatedElementsMap 
			 * @param selectedElementsToCreatedElementsMap
			 * @param newTGGRule
			 * @param domain
			 */
			private DomainGraphPattern cloneDomain(Map<EObject, EObject> selectedElementsToCreatedElementsMap, TripleGraphGrammarRule newTGGRule, DomainGraphPattern domain) {
				DomainGraphPattern newDomainGraphPattern = TggFactory.eINSTANCE.createDomainGraphPattern();
				newDomainGraphPattern.setTypedModel(domain.getTypedModel());
				newDomainGraphPattern.setName(domain.getName());
				newTGGRule.getDomain().add(newDomainGraphPattern);
				selectedElementsToCreatedElementsMap.put(domain, newDomainGraphPattern);
				return newDomainGraphPattern;
			}

			
		};
		
		

		
		try {
			OperationHistoryFactory.getOperationHistory().execute(createNewRuleCommand, new NullProgressMonitor(), null);
			containingGraph.eResource().save(TggDiagramEditorUtil.getSaveOptions());
		} catch (ExecutionException e) {
			TggDiagramEditorPlugin.getInstance().logError("Unable to create model", e); //$NON-NLS-1$
		} catch (IOException ex) {
			TggDiagramEditorPlugin.getInstance().logError("Save operation failed for: " + (diagramModelURI==null?"":diagramModelURI), ex); //$NON-NLS-1$
		} /*catch (PartInitException ex) {
			TggDiagramEditorPlugin.getInstance().logError("Unable to open editor", ex); //$NON-NLS-1$
		}*/


		if (createDiagram) {

			final TripleGraphGrammarRule newTGGRule = (TripleGraphGrammarRule) createNewRuleCommand.getCommandResult().getReturnValue();


			AbstractTransactionalCommand createNewDiagramCommand = new AbstractTransactionalCommand(transactionalEditingDomain,
					Messages.TggNewDiagramFileWizard_InitDiagramCommand, affectedFiles) {

				@Override
				protected CommandResult doExecuteWithResult(
						IProgressMonitor monitor, IAdaptable info)
				throws ExecutionException {
					Diagram diagram = null;

					//Create the diagram file.
					int diagramVID = TggVisualIDRegistry.getDiagramVisualID(newTGGRule);
					if (diagramVID != TripleGraphGrammarRuleEditPart.VISUAL_ID) {
						return CommandResult.newErrorCommandResult(Messages.TggNewDiagramFileWizard_IncorrectRootError);
					}
					diagram = ViewService.createDiagram(newTGGRule,
							TripleGraphGrammarRuleEditPart.MODEL_ID, TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
					diagramResource.getContents().add(diagram);

					TggDiagramEditorUtil.LazyElement2ViewMap element2ViewMap = new TggDiagramEditorUtil.LazyElement2ViewMap(
							diagramEditPart.getDiagramView(), selectedElementsToCreatedElementsMap.keySet());

					for (Map.Entry<EObject,EObject> elementPair : selectedElementsToCreatedElementsMap.entrySet()) {
						int visualID = TggVisualIDRegistry.getNodeVisualID(diagram, elementPair.getValue());
						if (TggVisualIDRegistry.canCreateNode(diagram, visualID)) { // can we create such a view type?
							// first locate the original view
							View origElementView = TggDiagramEditorUtil.findView(diagramEditPart, elementPair.getKey(), element2ViewMap);
							if (origElementView != null) {
								if (origElementView instanceof org.eclipse.gmf.runtime.notation.Node) {
									org.eclipse.gmf.runtime.notation.Node newNode = ViewService.createNode(diagram, elementPair.getValue(), TggVisualIDRegistry.getType(visualID), TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
									if (newNode != null) {
										newNode.setLayoutConstraint(((org.eclipse.gmf.runtime.notation.Node)origElementView).getLayoutConstraint());
									}
								}
							}
						}
					}
					return CommandResult.newOKCommandResult(diagram);
				}
			};

			try {
				OperationHistoryFactory.getOperationHistory().execute(createNewDiagramCommand, new NullProgressMonitor(), null);
				diagramResource.save(TggDiagramEditorUtil.getSaveOptions());
				//TggDiagramEditorUtil.openDiagram(diagramResource);
			} catch (ExecutionException e) {
				TggDiagramEditorPlugin.getInstance().logError("Unable to create diagram", e); //$NON-NLS-1$
			} catch (IOException ex) {
				TggDiagramEditorPlugin.getInstance().logError("Save operation failed for: " + (diagramModelURI==null?"":diagramModelURI), ex); //$NON-NLS-1$
			} /*catch (PartInitException ex) {
			TggDiagramEditorPlugin.getInstance().logError("Unable to open editor", ex); //$NON-NLS-1$
		}*/
		}


		return true;
	}

	private GraphGrammarRule getContainingGraph(EditPart editPart) {
		//go to the root of the edit part tree and get the rule from this edit part.
		if (editPart != null) {
			editPart = editPart.getRoot();
			if ((((EditPart)editPart.getChildren().get(0)).getModel() instanceof View) &&
					(((View)((EditPart)editPart.getChildren().get(0)).getModel()).getElement() instanceof GraphGrammarRule))
				return (GraphGrammarRule)((View)((EditPart)editPart.getChildren().get(0)).getModel()).getElement();
			else
				return null;
		} else
			return null;
	}

	private TransactionalEditingDomain getTransactionalEditingDomain(EditPart editPart) {
		//go to the root of the edit part tree and get the rule from this edit part.
		if (editPart != null) {
			if (editPart instanceof GraphicalEditPart)
				return ((GraphicalEditPart)editPart).getEditingDomain();
			editPart = editPart.getRoot();
			if (editPart instanceof GraphicalEditPart)
				return ((GraphicalEditPart)editPart).getEditingDomain();
			else {
				for (Object ep : editPart.getChildren()) {
					if (ep instanceof GraphicalEditPart) 
						return ((GraphicalEditPart)ep).getEditingDomain();
				}
				return null;
			}
		} else
			return null;
	}

	private DiagramEditPart getDiagramEditPart(EditPart firstElement) {
		return (DiagramEditPart)firstElement.getRoot().getContents();
	}

}
