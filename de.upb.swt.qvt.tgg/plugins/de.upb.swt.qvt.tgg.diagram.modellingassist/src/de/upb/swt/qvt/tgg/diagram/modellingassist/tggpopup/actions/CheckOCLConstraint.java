package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.diagram.modellingassist.Activator;

public class CheckOCLConstraint  implements IObjectActionDelegate {
	private Shell shell;
	
	private ISelection selection;
		
	public CheckOCLConstraint() {
		super();
	}

	@Override
	public void run(IAction action) {
		
		final OCLConstraint constraint = (OCLConstraint) ((Node)((EditPart) ((StructuredSelection)selection).getFirstElement()).getModel()).getElement();

		TransactionalEditingDomain transactionalEditingDomain = ((GraphicalEditPart)((StructuredSelection)selection).getFirstElement()).getEditingDomain();		
		
		AbstractTransactionalCommand analyseConstraintCommand = new AbstractTransactionalCommand(transactionalEditingDomain, 
				"newObjects", null){

					@Override
					protected CommandResult doExecuteWithResult(
							IProgressMonitor monitor, IAdaptable info)
							throws ExecutionException {
						String result = constraint.analyseConstraint();
						IStatus status = new Status("".equals(result)?IStatus.OK:IStatus.ERROR, Activator.PLUGIN_ID, result);
						return new CommandResult(status, result);
					}
		};

		
		IStatus status = null;
		try {
			status = OperationHistoryFactory.getOperationHistory().execute(analyseConstraintCommand, new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			MessageDialog.openError(
					shell,
					"TGG Editor Plug-in", "An error occured while executing the command: \n" + 
					e.getMessage());
		}
		String msg = status != null ? status.getMessage() : "An error occured while checking the constraint's syntax.";
		if (!msg.equals("")) {
			MessageDialog.openWarning(
					shell,
					"TGG Editor Plug-in",
					"The constraint has NOT passed the syntax check.\nMessage: \""+msg+"\"");
		}
		else {
			MessageDialog.openInformation(
					shell,
					"TGG Editor Plug-in",
					"The constraint has passed the syntax check.");
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

}
