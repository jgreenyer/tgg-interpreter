package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorUtil;

public class WizardEnterNamePage extends WizardPage {

	private Text nameField;
	private WizardNewFileCreationPage myFileCreationPage;
	private IPath filePath;

	protected WizardEnterNamePage(String pageName, WizardNewFileCreationPage myFileCreationPage, IPath filePath) {
		super(pageName);
		this.filePath = filePath;
		this.myFileCreationPage = myFileCreationPage;
	}

	@Override
	public void createControl(Composite parent) {
		// top level group
		Composite topLevel = new Composite(parent, SWT.NONE);
		topLevel.setLayout(new GridLayout());
		topLevel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL
				| GridData.HORIZONTAL_ALIGN_FILL));
		topLevel.setFont(parent.getFont());
		

		Label label = new Label(topLevel, SWT.NONE);
		label.setText("Rule Name:");

		// resource name entry field
		nameField = new Text(topLevel, SWT.BORDER);
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL);
		nameField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				myFileCreationPage.setFileName(TggDiagramEditorUtil.getUniqueFileName(filePath, nameField.getText(), "tgg_diagram"));
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				myFileCreationPage.setFileName(TggDiagramEditorUtil.getUniqueFileName(filePath, nameField.getText(), "tgg_diagram"));
			}
		});
		data.widthHint = 200;
		nameField.setLayoutData(data);
		nameField.setText("newTGGRule");
		myFileCreationPage.setFileName(TggDiagramEditorUtil.getUniqueFileName(filePath, nameField.getText(), "tgg_diagram"));
		
		
		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(topLevel);		
	}
	public String getName() {
		return nameField.getText();
	}
}
