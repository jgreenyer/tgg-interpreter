package de.upb.swt.qvt.tgg.diagram.modellingassist.tggpopup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class SelectResourceWizardPage extends WizardPage {

	private Text fileURI;	

	protected SelectResourceWizardPage(String pageName) {
		super(pageName);
		this.setTitle("Select an existing model");		
	}

	@Override
	public void createControl(Composite parent) {
		// top level group
		Composite topLevel = new Composite(parent, SWT.NONE);		
				
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		
		topLevel.setLayout(gridLayout);

		Label label = new Label(topLevel, SWT.NONE);
		label.setText("Existing model:");

		// resource name entry field
		fileURI = new Text(topLevel, SWT.BORDER);
		fileURI.setLayoutData(gridData);
		fileURI.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {

			}
			
			@Override
			public void keyPressed(KeyEvent e) {

			}
		});
		fileURI.setText("");
		
		Button browse = new Button(topLevel, SWT.PUSH);
		browse.setText("Browse");

		browse.addListener(SWT.Selection, new Listener(){

			@Override
			public void handleEvent(Event event) {
				fileURI.setText(handleBrowseFile().toString());
				
			}});
		
		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(topLevel);		
	}
	
	private URI handleBrowseFile()
    {
       ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
             getShell(), new WorkbenchLabelProvider(),
             new BaseWorkbenchContentProvider());
       dialog.setTitle("Existing model");
       dialog.setMessage("Select a model from the workspace:");
       dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
       ViewerFilter extensionFilter = new ViewerFilter()
       {
          @Override
          public boolean select(Viewer viewer, Object parentElement,
                Object element)
          {
             return !(element instanceof IFile)
                   || !((IFile) element).getFileExtension().isEmpty();
          }
       };
       dialog.addFilter(extensionFilter);

       if (dialog.open() == ContainerSelectionDialog.OK)
       {
          Object[] result = dialog.getResult();
          if (result.length == 1)
          {
             return URI.createPlatformResourceURI(((IFile) result[0])
                   .getFullPath().toString(), true);
          }
       }
       return null;
    }

	public String getURI(){
		return fileURI.getText();
	} 
}
