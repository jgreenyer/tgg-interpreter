package de.upb.swt.qvt.tgg.interpreter.ui.popup.actions;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ListDialog;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

public class CreateForwardTransformationConfigurationAction implements IObjectActionDelegate{


	//private static Logger logger = Activator.getLogManager().getLogger(CreateForwardTransformationConfigurationAction.class.getName());
	
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		List<?> selectionList = structuredSelection.toList();

		IFile tggFile = null;
		IFile modelFile = null;

		if ("tgg".equals(((IFile)selectionList.get(0)).getFileExtension())){
			tggFile = ((IFile)selectionList.get(0));
			modelFile = ((IFile)selectionList.get(1));
		}

		if ("tgg".equals(((IFile)selectionList.get(1)).getFileExtension())){
			tggFile = ((IFile)selectionList.get(1));
			modelFile = ((IFile)selectionList.get(0));
		}

		if (tggFile == null){
			MessageDialog.openError(new Shell(), "Invalid selection", "Please mark a source model file and a .tgg file containing the TGG that the source model shall be transformed with.");
			return;
		}
		
		try {
			ResourceSet resourceSet = new ResourceSetImpl();
			
			Resource interpreterConfigurationResource;
			@SuppressWarnings("null")
			URI interpreterConfigurationFileURI = URI.createPlatformResourceURI(modelFile.getFullPath().toString(), true)
			.trimFileExtension().appendFileExtension("interpreterconfiguration");
			try {
				interpreterConfigurationResource = resourceSet.getResource(interpreterConfigurationFileURI, true);

				if (interpreterConfigurationResource != null){
					MessageDialog.openError(new Shell(), "An error occurred while creating the interpreter configuration", 
							"There already exists a configuration at the default location:\n" 
							+ interpreterConfigurationFileURI + "\n\n"
							+ "Remove it first to create a new one.");
					return;
				}
			} catch (Exception e) {
				// that's good. Continue.
			}
			
			TripleGraphGrammar tgg;
			try {
				tgg = InterpreterconfigurationUtil.loadAndInitTripleGraphGrammar(URI.createPlatformResourceURI(tggFile.getFullPath().toString(), true), resourceSet);
			} catch (IllegalArgumentException e) {
				MessageDialog.openError(new Shell(), "There was a problem loading the TGG", e.getMessage());
				return;
			}

			ListDialog chooseCorrespondenceTypedModelDialog = new ListDialog(new Shell());
			chooseCorrespondenceTypedModelDialog.setTitle("Select correspondence typed model");
			chooseCorrespondenceTypedModelDialog.setMessage("Please select the correspondence typed model?");
			chooseCorrespondenceTypedModelDialog.setInput(tgg.getAllModelParameters().toArray());
			chooseCorrespondenceTypedModelDialog.setContentProvider(new ArrayContentProvider());
			chooseCorrespondenceTypedModelDialog.setLabelProvider(new LabelProvider(){
				@Override
				public String getText(Object element) {
					TypedModel typedModel = (TypedModel) element;
					String returnString = "";
					
					for (EPackage ePackage : typedModel.getUsedPackage()) {
						if (!"".equals(returnString)) returnString += ", ";
						returnString += ePackage.getName();
					}
					return typedModel.getName() + " (used packages: " + returnString + ")";
				}
			});			
			chooseCorrespondenceTypedModelDialog.open();
			
			if (chooseCorrespondenceTypedModelDialog.getReturnCode() == Window.CANCEL) return;
			if (chooseCorrespondenceTypedModelDialog.getResult().length != 1){
				MessageDialog.openError(new Shell(), "Invalid selection", "Please select one typed model.");
				return;
			}
			TypedModel correspondenceTypedModel = (TypedModel) chooseCorrespondenceTypedModelDialog.getResult()[0];
			
			Configuration configuration = InterpreterconfigurationUtil.buildConfiguration(modelFile.getFullPath().toString(), tgg, correspondenceTypedModel);


			interpreterConfigurationResource = 
				resourceSet.createResource(URI.createPlatformResourceURI(modelFile.getFullPath().toString(), true)
						.trimFileExtension().appendFileExtension("interpreterconfiguration"));
			interpreterConfigurationResource.getContents().add(configuration);
			try {
				interpreterConfigurationResource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IllegalArgumentException e) {
			MessageDialog.openError(new Shell(), "An error occurred while creating the interpreter configuration", 
					"An error occurred while creating the interpreter configuration: " + e.getMessage());
		}

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

}
