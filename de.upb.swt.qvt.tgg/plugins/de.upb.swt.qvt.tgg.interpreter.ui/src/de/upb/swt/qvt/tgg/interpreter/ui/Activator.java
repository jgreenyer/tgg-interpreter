package de.upb.swt.qvt.tgg.interpreter.ui;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.ui.plugin.*;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.BundleContext;

import com.tools.logging.PluginLogManager;

/**
 * The main plugin class to be used in the desktop.
 */
public class Activator extends AbstractUIPlugin {

	// logger configuration
    private static final String LOG_PROPERTIES_FILE = "logger.properties";

    //log manager
    private PluginLogManager logManager;

	
	//The shared instance.
	private static Activator plugin;
	
	/**
	 * The constructor.
	 */
	public Activator() {
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		configure();
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
        if(this.logManager != null) {
            logManager.shutdown();
            logManager = null;
        }
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return the shared instance.
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
     * @return Returns the logManager.
     */
    public static PluginLogManager getLogManager() {
        return getDefault().logManager;
    }

	
    private void configure() {
        URL url = getBundle().getEntry("/" + LOG_PROPERTIES_FILE);
        try {
            InputStream inputStream = url.openStream();
            if(inputStream != null) {
                Properties properties = new Properties();
                properties.load(inputStream);
                inputStream.close();
                logManager = new PluginLogManager(getDefault(), properties);
                //                logManager.hookPlugin(getDefault().getBundle()
                //                        .getSymbolicName(), getDefault().getLog());
            }
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    getDefault().getBundle().getSymbolicName(),
                    IStatus.ERROR,
                    "Error while initializing log properties." + e.getMessage(),
                    e);
            getDefault().getLog().log(status);
            throw new RuntimeException(
                    "Error while initializing log properties.", e);
        }
    }


	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path.
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin("de.upb.swt.qvt.tgg.interpreter.ui", path);
	}
}
