package de.upb.swt.qvt.tgg.interpreter.ui.popup.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.ui.Activator;
import de.upb.swt.qvt.tgg.interpreter.util.InterpreterStatus;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

public class StartInterpreterAction implements IObjectActionDelegate {

	private static Logger logger = Activator.getLogManager().getLogger(StartInterpreterAction.class.getName());

	/**
	 * Constructor for Action1.
	 */
	public StartInterpreterAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		
		try {


			IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
			IFile file = (IFile) structuredSelection.getFirstElement();
			
			final Configuration configuration = InterpreterconfigurationUtil.loadInterpreterConfiguration(URI.createPlatformResourceURI(file.getFullPath().toString(), true));
			
			final Job job = new Job("TGG Transformation: " + configuration.getTripleGraphGrammar().getName()){

				protected IStatus run(IProgressMonitor monitor) {
					Interpreter interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
					interpreter.setConfiguration(configuration);
					interpreter.initializeConfiguration();
					IStatus returnStatus = interpreter.performTransformation(monitor);

		        	postTransformationFinished(returnStatus, interpreter);

					return returnStatus;
				}
				
			};
						
		    job.setUser(true);
		    job.schedule();
		} catch (Exception e) {
			//Shell shell = new Shell();
			logger.error("Error caught while running the Interpreter",e);
		}
	}
	
	
	public static void postTransformationFinished(final IStatus resultStatus, final Interpreter interpreter){
		Display.getDefault().asyncExec (new Runnable() {
			final class RulesAppliedList<E> extends ArrayList<E> {
				private static final long serialVersionUID = 1L;
				public RulesAppliedList(int arg0) {
					super(arg0);
				}
				
			}; 

			final class RulesRevokedList<E> extends ArrayList<E> {
				private static final long serialVersionUID = 1L;
				public RulesRevokedList(int arg0) {
					super(arg0);
				}
				
			}; 

			final class RulesStillValidList<E> extends ArrayList<E> {
				private static final long serialVersionUID = 1L;
				public RulesStillValidList(int arg0) {
					super(arg0);
				}
			}; 
			
			public void run() {
//				MessageDialog dialog = new MessageDialog(new Shell(), "Transformation Performed", null, resultStatus.getMessage() + "\n\nDo you want to save the created model(s)?", 
//						MessageDialog.QUESTION_WITH_CANCEL, 
//						new String[] { IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL}, 0){
//					public int open(){
//						return 1;
//					}
//				};
								
				/*Map<TripleGraphGrammarRule, Integer> numberOfTimesRuleWasApplied = new HashMap<TripleGraphGrammarRule, Integer>();
				for (RuleBinding ruleBinding : ((InterpreterStatus) resultStatus).getConfiguration().getRuleBindingContainer().getRuleBinding()) {
					TripleGraphGrammarRule tggRule = ruleBinding.getTggRule();
					if (numberOfTimesRuleWasApplied.containsKey(tggRule)){
						numberOfTimesRuleWasApplied.put(tggRule, numberOfTimesRuleWasApplied.get(tggRule).intValue()+1);
					}else{
						numberOfTimesRuleWasApplied.put(tggRule,1);
					}
				}*/
				boolean isSynchronize = ApplicationMode.SYNCHRONIZE.equals(((InterpreterStatus) resultStatus).getConfiguration().getActiveApplicationScenario().getMode())
								   		|| ApplicationMode.UPDATE.equals(((InterpreterStatus) resultStatus).getConfiguration().getActiveApplicationScenario().getMode());
				Map<TripleGraphGrammarRule, Integer> ruleCounter = ((InterpreterStatus) resultStatus).getRulesApplied();
				
				List<String> numberOfTimesRulesRevokedString = null;
				List<String> numberOfTimesRulesStillValidString = null;
				
				List<String> numberOfTimesRulesAppliedString = new RulesAppliedList<String>(ruleCounter.keySet().size());
				for (TripleGraphGrammarRule tggRule : ruleCounter.keySet()) {
					numberOfTimesRulesAppliedString.add(ruleCounter.get(tggRule) + "x  " + tggRule.getName());
				}

				if (isSynchronize) {
					ruleCounter = ((InterpreterStatus) resultStatus).getRulesRevoked();
					
					numberOfTimesRulesRevokedString = new RulesRevokedList<String>(ruleCounter.keySet().size());
					for (TripleGraphGrammarRule tggRule : ruleCounter.keySet()) {
						numberOfTimesRulesRevokedString.add(ruleCounter.get(tggRule) + "x  " + tggRule.getName());
					}
					
					ruleCounter = ((InterpreterStatus) resultStatus).getRulesStillValid();
					
					numberOfTimesRulesStillValidString = new RulesStillValidList<String>(ruleCounter.keySet().size());
					for (TripleGraphGrammarRule tggRule : ruleCounter.keySet()) {
						numberOfTimesRulesStillValidString.add(ruleCounter.get(tggRule) + "x  " + tggRule.getName());
					}
				}
				
				List<Object> contentList = new ArrayList<Object>();
				if (isSynchronize) {
					contentList.add(numberOfTimesRulesStillValidString);
					contentList.add(numberOfTimesRulesRevokedString);
				}
				contentList.add(numberOfTimesRulesAppliedString);
				contentList.add(((InterpreterStatus) resultStatus).getConfiguration().getRuleBindingContainer());
				if (((InterpreterStatus) resultStatus).getUnboundSourceObjects() != null 
						&& !((InterpreterStatus) resultStatus).getUnboundSourceObjects().isEmpty()){
					contentList.add(((InterpreterStatus) resultStatus).getUnboundSourceObjects());
				}

				ElementTreeSelectionDialog treeDialog = new ElementTreeSelectionDialog(new Shell(), new ILabelProvider() {
					
					@Override
					public void removeListener(ILabelProviderListener listener) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public boolean isLabelProperty(Object element, String property) {
						// TODO Auto-generated method stub
						return false;
					}
					
					@Override
					public void dispose() {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void addListener(ILabelProviderListener listener) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public String getText(Object element) {
						if (element instanceof RulesAppliedList<?>){
							return "Applied TGG rules:";
						}
						else if (element instanceof RulesRevokedList<?>){
							return "Revoked TGG rules:";
						}
						else if (element instanceof RulesStillValidList<?>){
							return "Checked and valid TGG rules:";
						}
						else if (element instanceof List<?>){
							return "Unbound Objects:";
						}
						else if (element instanceof Node){
							Node node = (Node)element;
							String returnString = ":" + node.getTypedName();
							if (node.getName() != null){
								returnString = node.getName() + returnString;
							}
							if (node.isContext()) returnString = " context node: " + returnString;
							if (node.isProduced()) returnString = " produced node: " + returnString;
							if (node.isReusable()) returnString = " reusable node: " + returnString;
							if (node.getRefinedNode() != null)
								return " refined " + returnString;
							else
								return returnString;
						}
						else if (element instanceof Edge){
							Edge edge = (Edge) element;
							String returnString = "";
							if (edge.isContext()) returnString = " context edge: ";
							if (edge.isProduced()) returnString = " produced edge: ";
							if (edge.isReusable()) returnString = " reusable edge: ";

							if (edge.getName() != null) returnString += edge.getName(); 
							return returnString + ":" + edge.getTypedName() 
								+ " (" + edge.getSourceNode().getTypedName() 
								+ " -> " + edge.getTargetNode().getTypedName() + ")";
						}
						else		
						if (element instanceof Map.Entry<?, ?>){
							if (((Map.Entry<?, ?>)element).getKey() instanceof EObject){
								return " object:   " + getExtendedEObjectName((EObject)((Map.Entry<?, ?>)element).getKey());
							}else{ // if its not an EObject it must be a "VirtualModelEdge" (EList with source object, type Reference, target object)
								return " link:       " + getLinkName((EList<?>) ((Map.Entry<?, ?>)element).getKey());
							}
						}else if (element instanceof RuleBindingContainer){
							return "Rule bindings in their order of application:";
						}else if (element instanceof RuleBinding){
							return ((InterpreterStatus) resultStatus).getConfiguration().getRuleBindingContainer().getRuleBinding().indexOf(element)+1
							+ ". " + ((RuleBinding)element).getTggRule().getName();
						}else{
							return element.toString();
						}
						
					}
					
					protected String getLinkName(EList<?> list){
						String returnString = "(";
						returnString += getEObjectName((EObject) list.get(0)) + " -> ";
						returnString += getEObjectName((EObject) list.get(1)) + ")";
						returnString += " via EReference " + ((EReference)list.get(2)).getEContainingClass().getName() + "." +((EReference)list.get(2)).getName();
						return returnString;
					}
					
					protected String getEObjectName(EObject eObject){
						for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()){
							if ("name".equals(eAttribute.getName()))
								if (eObject.eGet(eAttribute) instanceof String){
									return (String) eObject.eGet(eAttribute) + ":" + eObject.eClass().getName();
								}
						}
						return ":" + eObject.eClass().getName();
					}
					
					protected String getExtendedEObjectName(EObject eObject){
						return getEObjectName(eObject) + " (" + eObject.toString() + ")"; 
					} 
					
					@Override
					public Image getImage(Object element) {
						// TODO Auto-generated method stub
						return null;
					}
				}, new ITreeContentProvider() {
					
					@Override
					public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void dispose() {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public Object[] getElements(Object inputElement) {
						if (inputElement instanceof List<?>)
							return ((List<?>)inputElement).toArray(); 
						else
							return null;
					}
					
					@Override
					public boolean hasChildren(Object element) {
						if (element instanceof List<?> 
						|| element instanceof RuleBindingContainer
						|| element instanceof RuleBinding
						|| element instanceof Map.Entry<?, ?>)
							return true; 
						else
							return false;
					}
					
					@Override
					public Object getParent(Object element) {
						// TODO Auto-generated method stub
						return null;
					}
					
					@Override
					public Object[] getChildren(Object parentElement) {
						if (parentElement instanceof List<?>)
							return ((List<?>)parentElement).toArray(); 
						else if (parentElement instanceof RuleBindingContainer)
							return ((RuleBindingContainer)parentElement).getRuleBinding().toArray(); 
						else if (parentElement instanceof RuleBinding){
							RuleBinding ruleBinding = (RuleBinding) parentElement;
							List<Map.Entry<?, ?>> returnList = new ArrayList<Map.Entry<?, ?>>(
									ruleBinding.getNodeBinding().entrySet().size() + ruleBinding.getEdgeBinding().entrySet().size());
							returnList.addAll(ruleBinding.getNodeBinding().entrySet());
							returnList.addAll(ruleBinding.getEdgeBinding().entrySet());
							return returnList.toArray();
						}
						else if(parentElement instanceof Map.Entry<?, ?>){
								Map.Entry<?, ?> entry = (Map.Entry<?, ?>) parentElement;
								if (entry.getKey() instanceof EObject && entry.getValue() instanceof EList<?>){
									return ((EList<?>)entry.getValue()).toArray(); // nodes
								}
								if (entry.getKey() instanceof EList<?> && entry.getValue() instanceof EList<?>){
									return ((EList<?>)entry.getValue()).toArray(); // edges
								}								
							return null;
						}else
							return null;
					}
				});
				treeDialog.setInput(contentList);
				String message = resultStatus.getMessage() + "\nProcessing time: " 
						+ ((InterpreterStatus)resultStatus).getTransformationTime() + " miliseconds.";
				if (logger.isDebugEnabled()){
					int numberOfRulesApplied = 0;
					for (TripleGraphGrammarRule tggRule : ((InterpreterStatus)resultStatus).getRulesApplied().keySet()) {
						numberOfRulesApplied += ((InterpreterStatus)resultStatus).getRulesApplied().get(tggRule);
					}
					message += "\n There were " + numberOfRulesApplied + " rules applied in total.";
					if (resultStatus instanceof InterpreterStatus){
						InterpreterStatus interpreterStatus = (InterpreterStatus) resultStatus;
						message += "\n   requiring " + interpreterStatus.getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() + " edge explorations.";
						message += "\n There were " + interpreterStatus.getUnsuccessfulRuleApplicationAttempts() + " unsuccessful rule application attempts.";
						message += "\n   requiring " + interpreterStatus.getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() + " edge explorations.";
					} 
				}
				treeDialog.setMessage(message);
				treeDialog.setStatusLineAboveButtons(true);
				if (resultStatus.getSeverity() == Status.OK) {
					treeDialog.setTitle("Transformation finished");
				} else if (resultStatus.getSeverity() == Status.INFO) {
					treeDialog.setImage(Dialog
							.getImage(Dialog.DLG_IMG_MESSAGE_INFO));
					treeDialog.setTitle("Transformation finished");
				} else if (resultStatus.getSeverity() == Status.WARNING) {
					treeDialog.setImage(Dialog
							.getImage(Dialog.DLG_IMG_MESSAGE_WARNING));
					treeDialog
							.setTitle("Transformation finished with warnings");
				} else if (resultStatus.getSeverity() == Status.ERROR) {
					treeDialog.setImage(Dialog
							.getImage(Dialog.DLG_IMG_MESSAGE_ERROR));
					treeDialog.setTitle("Transformation finished with errors");
				}
				
				int dialogReturn = treeDialog.open();
				if (dialogReturn == 0){
					boolean saveResult = MessageDialog.openQuestion(new Shell(), "Transformation performed", "Do you want to save the created model(s)?");
					if (saveResult){
						interpreter.storeResult();
						if (ApplicationMode.UPDATE.equals(interpreter.getConfiguration().getActiveApplicationScenario().getMode()) 
								|| ApplicationMode.SYNCHRONIZE.equals(interpreter.getConfiguration().getActiveApplicationScenario().getMode())) {
							boolean saveConfiguration = MessageDialog.openQuestion(new Shell(), "Transformation performed", "Do you want to save the configuration?");
							if (saveConfiguration){
								interpreter.storeConfiguration();
							}
						}
					}
				}
			}
		}
		);
	
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
