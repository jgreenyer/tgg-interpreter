/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConstraint Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintEnforcementPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IConstraintEnforcementPolicy extends IConstraintCheckingPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * this method is called each time that we match node to a candidate object.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean enforceConstraints(Node node, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns true when the policy is responsible for enforcing this constraint.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean canEnforceConstraint(Node node, EObject candidateObject, Constraint constraint);

} // IConstraintEnforcementPolicy
