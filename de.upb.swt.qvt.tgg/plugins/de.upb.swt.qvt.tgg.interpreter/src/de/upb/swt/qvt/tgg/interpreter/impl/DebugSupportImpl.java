/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.IDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Debug Support</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl#isTerminate <em>Terminate</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl#isDebug <em>Debug</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl#isRunning <em>Running</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl#getDebugEventListener <em>Debug Event Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DebugSupportImpl extends EObjectImpl implements DebugSupport {
	/**
	 * The default value of the '{@link #isTerminate() <em>Terminate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTerminate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TERMINATE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTerminate() <em>Terminate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTerminate()
	 * @generated
	 * @ordered
	 */
	protected boolean terminate = TERMINATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isDebug() <em>Debug</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebug()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEBUG_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDebug() <em>Debug</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebug()
	 * @generated
	 * @ordered
	 */
	protected boolean debug = DEBUG_EDEFAULT;

	/**
	 * The default value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RUNNING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected boolean running = RUNNING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDebugEventListener() <em>Debug Event Listener</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDebugEventListener()
	 * @generated
	 * @ordered
	 */
	protected EList<IDebugEventListener> debugEventListener;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DebugSupportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.DEBUG_SUPPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTerminate() {
		return terminate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTerminate(boolean newTerminate) {
		boolean oldTerminate = terminate;
		terminate = newTerminate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.DEBUG_SUPPORT__TERMINATE, oldTerminate, terminate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDebug(boolean newDebug) {
		boolean oldDebug = debug;
		debug = newDebug;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.DEBUG_SUPPORT__DEBUG, oldDebug, debug));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunning(boolean newRunning) {
		boolean oldRunning = running;
		running = newRunning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.DEBUG_SUPPORT__RUNNING, oldRunning, running));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IDebugEventListener> getDebugEventListener() {
		if (debugEventListener == null) {
			debugEventListener = new EObjectResolvingEList<IDebugEventListener>(IDebugEventListener.class, this, InterpreterPackage.DEBUG_SUPPORT__DEBUG_EVENT_LISTENER);
		}
		return debugEventListener;
	}

//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated NOT
//	 */
//	public void matchingCandidateObjectToNode(Node node, EObject object, RuleBinding ruleBinding, boolean successful) {
//		if (!isDebug()) return;
//		for (IDebugEventListener listener : getDebugEventListener()) {
//			listener.matchingCandidateObjectToNode(node, object, ruleBinding, successful);
//		}
//	}
//
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated NOT
//	 */
//	public void finishedMatchingRule(RuleBinding ruleBinding, boolean successful) {
//		if (!isDebug()) return;
//		for (IDebugEventListener listener : getDebugEventListener()) {
//			listener.finishedMatchingRule(ruleBinding, successful);
//		}
//	}
//
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated NOT
//	 */
//	public void startMatchingRule(RuleBinding ruleBinding) {
//		if (!isDebug()) return;
//		for (IDebugEventListener listener : getDebugEventListener()) {
//			listener.startMatchingRule(ruleBinding);
//		}
//	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startTransformation(Configuration configuration) {
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.startTransformation(configuration);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void finishedTransformation(Configuration configuration, boolean successful) {
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.finishedTransformation(configuration, successful);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.DEBUG_SUPPORT__TERMINATE:
				return isTerminate();
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG:
				return isDebug();
			case InterpreterPackage.DEBUG_SUPPORT__RUNNING:
				return isRunning();
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG_EVENT_LISTENER:
				return getDebugEventListener();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.DEBUG_SUPPORT__TERMINATE:
				setTerminate((Boolean)newValue);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG:
				setDebug((Boolean)newValue);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__RUNNING:
				setRunning((Boolean)newValue);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG_EVENT_LISTENER:
				getDebugEventListener().clear();
				getDebugEventListener().addAll((Collection<? extends IDebugEventListener>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.DEBUG_SUPPORT__TERMINATE:
				setTerminate(TERMINATE_EDEFAULT);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG:
				setDebug(DEBUG_EDEFAULT);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__RUNNING:
				setRunning(RUNNING_EDEFAULT);
				return;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG_EVENT_LISTENER:
				getDebugEventListener().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.DEBUG_SUPPORT__TERMINATE:
				return terminate != TERMINATE_EDEFAULT;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG:
				return debug != DEBUG_EDEFAULT;
			case InterpreterPackage.DEBUG_SUPPORT__RUNNING:
				return running != RUNNING_EDEFAULT;
			case InterpreterPackage.DEBUG_SUPPORT__DEBUG_EVENT_LISTENER:
				return debugEventListener != null && !debugEventListener.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (terminate: ");
		result.append(terminate);
		result.append(", debug: ");
		result.append(debug);
		result.append(", running: ");
		result.append(running);
		result.append(')');
		return result.toString();
	}

	@Override
	public void isApplyRuleRunning(
			TripleGraphGrammarRule tripleGraphGrammarRule, boolean isRunning,
			Object classInstance) {

		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.isApplyRuleRunning(tripleGraphGrammarRule, isRunning,
					classInstance);
		}		
	}

	@Override
	public void matchedNode(Node node, EObject eObject,
			RuleBinding ruleBinding, boolean isFrontTransformationNode,
			boolean isRunning, Object classInstance) {

		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.matchedNode(node,  eObject,
					 ruleBinding,  isFrontTransformationNode,
					 isRunning,  classInstance);
		}		
	}

	@Override
	public void candidateNode(Node node, EObject eObject, Object classInstance) {

		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.candidateNode(node,  eObject,  classInstance);
		}
	}

	@Override
	public void enforcedNode(Node node, EObject eObject,
			RuleBinding ruleBinding, Object classInstance, boolean isRunning) {
		
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.enforcedNode(node,  eObject,
					 ruleBinding,  classInstance,  isRunning);
		}
		
	}

	@Override
	public void currentRuleBinding(RuleBinding ruleBinding) {
		
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.currentRuleBinding(ruleBinding);
		}
		
	}

	@Override
	public IDebugTarget getDebugTarget(ILaunch launch, IProcess process) {
		return getDebugEventListener().get(0).getDebugTarget(launch, process);
		// TODO Auto-generated method stub
	}

	@Override
	public void isTransformationRunning(Configuration configuration,
			boolean isRunning) {
		
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.isTransformationRunning(configuration,isRunning);
		}
		
	}

	@Override
	public void isMatchingRuleRunning(RuleBinding ruleBinding,
			boolean isRunning, Object classInstance) {
		
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.isMatchingRuleRunning (ruleBinding,
					 isRunning,  classInstance );
		}		
	}

	@Override
	public void isPatternMatchingRunning(RuleBinding ruleBinding,
			boolean isRunning, Object classInstance) {
		
		if (!isDebug()) return;
		for (IDebugEventListener listener : getDebugEventListener()) {
			listener.isPatternMatchingRunning(ruleBinding,
					 isRunning,  classInstance );
		}
		
	}

	public void debugSupportForMatchedNode(Node node, EObject eObject, RuleBinding rulebinding, boolean isTransformationNode, boolean isRunning, Object classInstance) {
		try {
			matchedNode(node, eObject, rulebinding, isTransformationNode,isRunning, classInstance);
		} catch (Exception e) {

		}
	}
	
	public void debugSupportForCandidateNode(Node node, EObject eObject, Object classInstance) {
		try{	
			candidateNode(node, eObject,classInstance);
		} catch(Exception e){
			
		}
	}
	
	public void debugSupportForPatternMatchingRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance) {
		try {
			isPatternMatchingRunning(ruleBinding, isRunning, classInstance);
		} catch (Exception e) {

		}
	}
	
	public void debugSupportForApplyRuleOrMatchRuleBindig(Object model, boolean isRunning, Object classInstance) {
		try{		
			if(model instanceof TripleGraphGrammarRule){			
				isApplyRuleRunning((TripleGraphGrammarRule)model, isRunning, classInstance);
			} else if(model instanceof RuleBinding){				
				isMatchingRuleRunning((RuleBinding)model, isRunning, classInstance);
			}	
		}
		catch(Exception e){
			
		}
	}
	
	public void debugSupportForPatternMatching(RuleBinding ruleBinding, boolean isRunning, Object classInstance) {
		try {
			isPatternMatchingRunning(ruleBinding, isRunning, classInstance);
		} catch (Exception e) {

		}
	}
	
	public void debugSupportForEnforcedNode(Node node, EObject eObject, RuleBinding ruleBinding, Object classInstance, boolean isRunning) {
		try {
			enforcedNode(node, eObject, ruleBinding, classInstance, isRunning);
		} catch (Exception e) {

		}
	}
	
	public void debugSupportForCurrentRuleBinding(RuleBinding rulebinding) {
		try {
			currentRuleBinding(rulebinding);
		} catch (Exception e) {
		}
	}
	
} //DebugSupportImpl
