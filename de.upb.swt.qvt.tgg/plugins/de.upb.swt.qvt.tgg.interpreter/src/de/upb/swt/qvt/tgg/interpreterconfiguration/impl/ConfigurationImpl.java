/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getTripleGraphGrammar <em>Triple Graph Grammar</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#isRecordRuleBindings <em>Record Rule Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getApplicationScenario <em>Application Scenario</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getDomainModel <em>Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getRuleBindingContainer <em>Rule Binding Container</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getActiveApplicationScenario <em>Active Application Scenario</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getDomainModelByTypedModel <em>Domain Model By Typed Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#getDomainRootObjects <em>Domain Root Objects</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl#isNoPackageUriRedirection <em>No Package Uri Redirection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConfigurationImpl extends EObjectImpl implements Configuration {
	/**
	 * The cached value of the '{@link #getTripleGraphGrammar() <em>Triple Graph Grammar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTripleGraphGrammar()
	 * @generated
	 * @ordered
	 */
	protected TripleGraphGrammar tripleGraphGrammar;

	/**
	 * The default value of the '{@link #isRecordRuleBindings() <em>Record Rule Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecordRuleBindings()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RECORD_RULE_BINDINGS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRecordRuleBindings() <em>Record Rule Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecordRuleBindings()
	 * @generated
	 * @ordered
	 */
	protected boolean recordRuleBindings = RECORD_RULE_BINDINGS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getApplicationScenario() <em>Application Scenario</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationScenario()
	 * @generated
	 * @ordered
	 */
	protected EList<ApplicationScenario> applicationScenario;

	/**
	 * The cached value of the '{@link #getDomainModel() <em>Domain Model</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainModel()
	 * @generated
	 * @ordered
	 */
	protected EList<DomainModel> domainModel;

	/**
	 * The cached value of the '{@link #getRuleBindingContainer() <em>Rule Binding Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleBindingContainer()
	 * @generated
	 * @ordered
	 */
	protected RuleBindingContainer ruleBindingContainer;

	/**
	 * The cached value of the '{@link #getActiveApplicationScenario() <em>Active Application Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveApplicationScenario()
	 * @generated
	 * @ordered
	 */
	protected ApplicationScenario activeApplicationScenario;

	/**
	 * The cached value of the '{@link #getDomainModelByTypedModel() <em>Domain Model By Typed Model</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainModelByTypedModel()
	 * @generated
	 * @ordered
	 */
	protected EMap<TypedModel, DomainModel> domainModelByTypedModel;

	/**
	 * The default value of the '{@link #isNoPackageUriRedirection() <em>No Package Uri Redirection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoPackageUriRedirection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NO_PACKAGE_URI_REDIRECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNoPackageUriRedirection() <em>No Package Uri Redirection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoPackageUriRedirection()
	 * @generated
	 * @ordered
	 */
	protected boolean noPackageUriRedirection = NO_PACKAGE_URI_REDIRECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammar getTripleGraphGrammar() {
		if (tripleGraphGrammar != null && tripleGraphGrammar.eIsProxy()) {
			InternalEObject oldTripleGraphGrammar = (InternalEObject)tripleGraphGrammar;
			tripleGraphGrammar = (TripleGraphGrammar)eResolveProxy(oldTripleGraphGrammar);
			if (tripleGraphGrammar != oldTripleGraphGrammar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR, oldTripleGraphGrammar, tripleGraphGrammar));
			}
		}
		return tripleGraphGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammar basicGetTripleGraphGrammar() {
		return tripleGraphGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTripleGraphGrammar(TripleGraphGrammar newTripleGraphGrammar) {
		TripleGraphGrammar oldTripleGraphGrammar = tripleGraphGrammar;
		tripleGraphGrammar = newTripleGraphGrammar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR, oldTripleGraphGrammar, tripleGraphGrammar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRecordRuleBindings() {
		return recordRuleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordRuleBindings(boolean newRecordRuleBindings) {
		boolean oldRecordRuleBindings = recordRuleBindings;
		recordRuleBindings = newRecordRuleBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS, oldRecordRuleBindings, recordRuleBindings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ApplicationScenario> getApplicationScenario() {
		if (applicationScenario == null) {
			applicationScenario = new EObjectContainmentWithInverseEList<ApplicationScenario>(ApplicationScenario.class, this, InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION);
		}
		return applicationScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DomainModel> getDomainModel() {
		if (domainModel == null) {
			domainModel = new EObjectContainmentEList<DomainModel>(DomainModel.class, this, InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL);
		}
		return domainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBindingContainer getRuleBindingContainer() {
		return ruleBindingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleBindingContainer(RuleBindingContainer newRuleBindingContainer, NotificationChain msgs) {
		RuleBindingContainer oldRuleBindingContainer = ruleBindingContainer;
		ruleBindingContainer = newRuleBindingContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER, oldRuleBindingContainer, newRuleBindingContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleBindingContainer(RuleBindingContainer newRuleBindingContainer) {
		if (newRuleBindingContainer != ruleBindingContainer) {
			NotificationChain msgs = null;
			if (ruleBindingContainer != null)
				msgs = ((InternalEObject)ruleBindingContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER, null, msgs);
			if (newRuleBindingContainer != null)
				msgs = ((InternalEObject)newRuleBindingContainer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER, null, msgs);
			msgs = basicSetRuleBindingContainer(newRuleBindingContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER, newRuleBindingContainer, newRuleBindingContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationScenario getActiveApplicationScenario() {
		if (activeApplicationScenario != null && activeApplicationScenario.eIsProxy()) {
			InternalEObject oldActiveApplicationScenario = (InternalEObject)activeApplicationScenario;
			activeApplicationScenario = (ApplicationScenario)eResolveProxy(oldActiveApplicationScenario);
			if (activeApplicationScenario != oldActiveApplicationScenario) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO, oldActiveApplicationScenario, activeApplicationScenario));
			}
		}
		return activeApplicationScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationScenario basicGetActiveApplicationScenario() {
		return activeApplicationScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveApplicationScenario(ApplicationScenario newActiveApplicationScenario) {
		ApplicationScenario oldActiveApplicationScenario = activeApplicationScenario;
		activeApplicationScenario = newActiveApplicationScenario;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO, oldActiveApplicationScenario, activeApplicationScenario));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> getDomainRootObjects() {
		EList<EObject> domainRootObjectList = new EObjectResolvingEList<EObject>(EObject.class, this,
				InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_ROOT_OBJECTS);
		for (DomainModel domainModel : getDomainModel()) {
			if (!domainModel.getRootObject().isEmpty()){
				domainRootObjectList.addAll(domainModel.getRootObject());
			}else{
				for (Resource resource : domainModel.getResource()) {
					domainRootObjectList.addAll(resource.getContents());
				}
			}
		}
		return domainRootObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNoPackageUriRedirection() {
		return noPackageUriRedirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoPackageUriRedirection(boolean newNoPackageUriRedirection) {
		boolean oldNoPackageUriRedirection = noPackageUriRedirection;
		noPackageUriRedirection = newNoPackageUriRedirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION, oldNoPackageUriRedirection, noPackageUriRedirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<TypedModel, DomainModel> getDomainModelByTypedModel() {
		if (domainModelByTypedModel == null) {
			domainModelByTypedModel = new EcoreEMap<TypedModel,DomainModel>(InterpreterconfigurationPackage.Literals.TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY, TypedModelToDomainModelMapEntryImpl.class, this, InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL);
			for (DomainModel domainModel : getDomainModel()) {
				domainModelByTypedModel.put(domainModel.getTypedModel(), domainModel);
			}
		}
		return domainModelByTypedModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getApplicationScenario()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				return ((InternalEList<?>)getApplicationScenario()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
				return ((InternalEList<?>)getDomainModel()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				return basicSetRuleBindingContainer(null, msgs);
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL:
				return ((InternalEList<?>)getDomainModelByTypedModel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR:
				if (resolve) return getTripleGraphGrammar();
				return basicGetTripleGraphGrammar();
			case InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS:
				return isRecordRuleBindings();
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				return getApplicationScenario();
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
				return getDomainModel();
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				return getRuleBindingContainer();
			case InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO:
				if (resolve) return getActiveApplicationScenario();
				return basicGetActiveApplicationScenario();
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL:
				if (coreType) return getDomainModelByTypedModel();
				else return getDomainModelByTypedModel().map();
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_ROOT_OBJECTS:
				return getDomainRootObjects();
			case InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION:
				return isNoPackageUriRedirection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR:
				setTripleGraphGrammar((TripleGraphGrammar)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS:
				setRecordRuleBindings((Boolean)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				getApplicationScenario().clear();
				getApplicationScenario().addAll((Collection<? extends ApplicationScenario>)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
				getDomainModel().clear();
				getDomainModel().addAll((Collection<? extends DomainModel>)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				setRuleBindingContainer((RuleBindingContainer)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO:
				setActiveApplicationScenario((ApplicationScenario)newValue);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION:
				setNoPackageUriRedirection((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR:
				setTripleGraphGrammar((TripleGraphGrammar)null);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS:
				setRecordRuleBindings(RECORD_RULE_BINDINGS_EDEFAULT);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				getApplicationScenario().clear();
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
				getDomainModel().clear();
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				setRuleBindingContainer((RuleBindingContainer)null);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO:
				setActiveApplicationScenario((ApplicationScenario)null);
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION:
				setNoPackageUriRedirection(NO_PACKAGE_URI_REDIRECTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR:
				return tripleGraphGrammar != null;
			case InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS:
				return recordRuleBindings != RECORD_RULE_BINDINGS_EDEFAULT;
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
				return applicationScenario != null && !applicationScenario.isEmpty();
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
				return domainModel != null && !domainModel.isEmpty();
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				return ruleBindingContainer != null;
			case InterpreterconfigurationPackage.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO:
				return activeApplicationScenario != null;
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL:
				return domainModelByTypedModel != null && !domainModelByTypedModel.isEmpty();
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_ROOT_OBJECTS:
				return !getDomainRootObjects().isEmpty();
			case InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION:
				return noPackageUriRedirection != NO_PACKAGE_URI_REDIRECTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (recordRuleBindings: ");
		result.append(recordRuleBindings);
		result.append(", noPackageUriRedirection: ");
		result.append(noPackageUriRedirection);
		result.append(')');
		return result.toString();
	}

} //ConfigurationImpl
