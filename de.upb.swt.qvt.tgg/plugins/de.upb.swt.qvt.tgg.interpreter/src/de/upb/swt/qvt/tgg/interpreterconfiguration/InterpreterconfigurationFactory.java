/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage
 * @generated
 */
public interface InterpreterconfigurationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InterpreterconfigurationFactory eINSTANCE = de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration</em>'.
	 * @generated
	 */
	Configuration createConfiguration();

	/**
	 * Returns a new object of class '<em>Domain Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Domain Model</em>'.
	 * @generated
	 */
	DomainModel createDomainModel();

	/**
	 * Returns a new object of class '<em>Rule Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Binding</em>'.
	 * @generated
	 */
	RuleBinding createRuleBinding();

	/**
	 * Returns a new object of class '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Application Scenario</em>'.
	 * @generated
	 */
	ApplicationScenario createApplicationScenario();

	/**
	 * Returns a new object of class '<em>Rule Binding Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Binding Container</em>'.
	 * @generated
	 */
	RuleBindingContainer createRuleBindingContainer();

	/**
	 * Returns a new object of class '<em>Axiom Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Axiom Binding</em>'.
	 * @generated
	 */
	AxiomBinding createAxiomBinding();

	/**
	 * Returns a new object of class '<em>Initial Axiom Node Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Initial Axiom Node Binding</em>'.
	 * @generated
	 */
	InitialAxiomNodeBinding createInitialAxiomNodeBinding();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	InterpreterconfigurationPackage getInterpreterconfigurationPackage();

} //InterpreterconfigurationFactory
