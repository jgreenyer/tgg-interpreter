/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl.PartialMatchingTreeRootVertex;
import de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl.PartialMatchingTreeVertex;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.TransformationProcessorUtil;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Rule Processor</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getGraphMatcher <em>Graph Matcher</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getNodeEnforcementPolicy <em>Node Enforcement Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getEdgeEnforcementPolicy <em>Edge Enforcement Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl#getConstraintEnforcementPolicy <em>Constraint Enforcement Policy</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleProcessorImpl extends EObjectImpl implements RuleProcessor {

	/**
	 * The cached value of the '{@link #getTripleGraphGrammarRule() <em>Triple Graph Grammar Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 * @ordered
	 */
	protected TripleGraphGrammarRule tripleGraphGrammarRule;

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(RuleProcessorImpl.class.getName());

	/**
	 * The cached value of the '{@link #getGraphMatcher() <em>Graph Matcher</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphMatcher()
	 * @generated
	 * @ordered
	 */
	protected GraphMatcher graphMatcher;

	/**
	 * The cached value of the '{@link #getNodeEnforcementPolicy() <em>Node Enforcement Policy</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getNodeEnforcementPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<INodeEnforcementPolicy> nodeEnforcementPolicy;

	/**
	 * The cached value of the '{@link #getEdgeEnforcementPolicy() <em>Edge Enforcement Policy</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getEdgeEnforcementPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<IEdgeEnforcementPolicy> edgeEnforcementPolicy;

	/**
	 * The cached value of the '{@link #getConstraintEnforcementPolicy()
	 * <em>Constraint Enforcement Policy</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConstraintEnforcementPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<IConstraintEnforcementPolicy> constraintEnforcementPolicy;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RuleProcessorImpl() {
		super();
		// We always need a graph matcher singleton
		setGraphMatcher(InterpreterFactory.eINSTANCE.createGraphMatcher());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.RULE_PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule getTripleGraphGrammarRule() {
		if (tripleGraphGrammarRule != null && tripleGraphGrammarRule.eIsProxy()) {
			InternalEObject oldTripleGraphGrammarRule = (InternalEObject)tripleGraphGrammarRule;
			tripleGraphGrammarRule = (TripleGraphGrammarRule)eResolveProxy(oldTripleGraphGrammarRule);
			if (tripleGraphGrammarRule != oldTripleGraphGrammarRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE, oldTripleGraphGrammarRule, tripleGraphGrammarRule));
			}
		}
		return tripleGraphGrammarRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule basicGetTripleGraphGrammarRule() {
		return tripleGraphGrammarRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTripleGraphGrammarRule(TripleGraphGrammarRule newTripleGraphGrammarRule) {
		TripleGraphGrammarRule oldTripleGraphGrammarRule = tripleGraphGrammarRule;
		tripleGraphGrammarRule = newTripleGraphGrammarRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE, oldTripleGraphGrammarRule, tripleGraphGrammarRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessor getTransformationProcessor() {
		if (eContainerFeatureID() != InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR) return null;
		return (TransformationProcessor)eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransformationProcessor(TransformationProcessor newTransformationProcessor,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTransformationProcessor, InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationProcessor(TransformationProcessor newTransformationProcessor) {
		if (newTransformationProcessor != eInternalContainer() || (eContainerFeatureID() != InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR && newTransformationProcessor != null)) {
			if (EcoreUtil.isAncestor(this, newTransformationProcessor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTransformationProcessor != null)
				msgs = ((InternalEObject)newTransformationProcessor).eInverseAdd(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR, TransformationProcessor.class, msgs);
			msgs = basicSetTransformationProcessor(newTransformationProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR, newTransformationProcessor, newTransformationProcessor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher getGraphMatcher() {
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphMatcher(GraphMatcher newGraphMatcher, NotificationChain msgs) {
		GraphMatcher oldGraphMatcher = graphMatcher;
		graphMatcher = newGraphMatcher;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER, oldGraphMatcher, newGraphMatcher);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphMatcher(GraphMatcher newGraphMatcher) {
		if (newGraphMatcher != graphMatcher) {
			NotificationChain msgs = null;
			if (graphMatcher != null)
				msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR, GraphMatcher.class, msgs);
			if (newGraphMatcher != null)
				msgs = ((InternalEObject)newGraphMatcher).eInverseAdd(this, InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR, GraphMatcher.class, msgs);
			msgs = basicSetGraphMatcher(newGraphMatcher, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER, newGraphMatcher, newGraphMatcher));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<INodeEnforcementPolicy> getNodeEnforcementPolicy() {
		if (nodeEnforcementPolicy == null) {
			nodeEnforcementPolicy = new EObjectResolvingEList<INodeEnforcementPolicy>(INodeEnforcementPolicy.class, this, InterpreterPackage.RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY);
		}
		return nodeEnforcementPolicy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IEdgeEnforcementPolicy> getEdgeEnforcementPolicy() {
		if (edgeEnforcementPolicy == null) {
			edgeEnforcementPolicy = new EObjectResolvingEList<IEdgeEnforcementPolicy>(IEdgeEnforcementPolicy.class, this, InterpreterPackage.RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY);
		}
		return edgeEnforcementPolicy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IConstraintEnforcementPolicy> getConstraintEnforcementPolicy() {
		if (constraintEnforcementPolicy == null) {
			constraintEnforcementPolicy = new EObjectResolvingEList<IConstraintEnforcementPolicy>(IConstraintEnforcementPolicy.class, this, InterpreterPackage.RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY);
		}
		return constraintEnforcementPolicy;
	}

	protected void setupGraphMatcher(RuleBinding initialRuleBinding) {
		/*if (getGraphMatcher() == null){
			setGraphMatcher(InterpreterFactory.eINSTANCE.createGraphMatcher());
		}*/
		getGraphMatcher().setRuleBinding(initialRuleBinding);
		getGraphMatcher().getDomainsToMatch().clear();
		getGraphMatcher().getDomainsToMatch().addAll(
				getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario()
						.getSourceDomainModel());
		getGraphMatcher().setMatchReusablePatternOnly(false);
		setupElementProcessor();
	}

	private void setupElementProcessor() {
		// load node matching policies
		initNodeProcessingPolicies();

		// load edge matching policies
		initEdgeProcessingPolicies();

		// load constraint checking policies
		initConstraintProcessingPolicies();
	}
	
	protected void initNodeProcessingPolicies() {
		if (getNodeEnforcementPolicy().isEmpty()){
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
					"de.upb.swt.qvt.tgg.interpreter.NodeProcessingPolicy");
			for (IConfigurationElement configurationElement : config) {
	
				EObject eObj = doLoadProcessingPolicy(configurationElement);
				if (eObj == null)
					return;
				if (eObj instanceof BaseNodeProcessingPolicy) {
					BaseNodeProcessingPolicy nodeProcessingPolicy = (BaseNodeProcessingPolicy) eObj;
					nodeProcessingPolicy.setTransformationProcessor(getTransformationProcessor());
					nodeProcessingPolicy.setRuleProcessor(this);
					getGraphMatcher().getNodeMatchingPolicy().add(nodeProcessingPolicy);
					this.getNodeEnforcementPolicy().add(nodeProcessingPolicy);
				} else {
					logger.error("The provided class " + configurationElement.getAttribute("nsURI") + "."
							+ configurationElement.getAttribute("eClass")
							+ " does not extend de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy.");
				}
			}
		}
	}

	protected void initEdgeProcessingPolicies() {
		if (getEdgeEnforcementPolicy().isEmpty()){
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
					"de.upb.swt.qvt.tgg.interpreter.EdgeProcessingPolicy");
			for (IConfigurationElement configurationElement : config) {
	
				EObject eObj = doLoadProcessingPolicy(configurationElement);
				if (eObj == null)
					return;
				if (eObj instanceof IEdgeMatchingPolicy) {
					BaseEdgeProcessingPolicy edgeProcessingPolicy = (BaseEdgeProcessingPolicy) eObj;
					edgeProcessingPolicy.setRuleProcessor(this);
					edgeProcessingPolicy.setTransformationProcessor(getTransformationProcessor());
					getGraphMatcher().getEdgeMatchingPolicy().add(edgeProcessingPolicy);
					this.getEdgeEnforcementPolicy().add(edgeProcessingPolicy);
	
				} else {
					logger.error("The provided class " + configurationElement.getAttribute("nsURI") + "."
							+ configurationElement.getAttribute("eClass")
							+ " does not extend de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy.");
				}
			}
		}
	}

	protected void initConstraintProcessingPolicies() {
		if (this.getConstraintEnforcementPolicy().isEmpty()){
		//this.getConstraintEnforcementPolicy().clear();

			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
					"de.upb.swt.qvt.tgg.interpreter.ConstraintProcessingPolicy");
			for (IConfigurationElement configurationElement : config) {
	
				EObject eObj = doLoadProcessingPolicy(configurationElement);
				if (eObj == null)
					return;
				if (eObj instanceof BaseConstraintProcessingPolicy) {
					BaseConstraintProcessingPolicy constraintProcessingPolicy = (BaseConstraintProcessingPolicy) eObj;
					getGraphMatcher().getConstraintCheckingPolicy().add(constraintProcessingPolicy);
					this.getConstraintEnforcementPolicy().add(constraintProcessingPolicy);
					getGraphMatcher().getConstraintCheckingPolicy().add(constraintProcessingPolicy);
	
				} else {
					logger.error("The provided class " + configurationElement.getAttribute("nsURI") + "."
							+ configurationElement.getAttribute("eClass")
							+ " does not implement de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy.");
				}
			}
		}
		for (IConstraintEnforcementPolicy constraintEnforcementPolicy : this.getConstraintEnforcementPolicy()) {
			if (constraintEnforcementPolicy instanceof BaseConstraintProcessingPolicy){
				constraintEnforcementPolicy.setRuleBinding(getGraphMatcher().getRuleBinding());
			}
		}
		
	}
		

	private EObject doLoadProcessingPolicy(IConfigurationElement configurationElement) {
		EPackage processingPolicyPackage = EPackage.Registry.INSTANCE.getEPackage(configurationElement
				.getAttribute("nsURI"));
		if (processingPolicyPackage == null) {
			logger.error("The specified package with nsURI \"" + configurationElement.getAttribute("nsURI")
					+ "\" cannot be found");
			return null;
		}
		EClass processingPolicyClass = (EClass) processingPolicyPackage.getEClassifier(configurationElement
				.getAttribute("eClass"));
		if (processingPolicyClass == null) {
			logger.error("The specified class \"" + configurationElement.getAttribute("eClass") + "\" cannot be found");
			return null;
		}
		return EPackage.Registry.INSTANCE.getEFactory(configurationElement.getAttribute("nsURI")).create(
				processingPolicyClass);
	}

	private void matchReusablePatternsInEnforceableDomains() {
		
		// enforceable domains are the correspondence/target domains.
		EList<DomainModel> enforceableDomains = new BasicEList<DomainModel>();
		enforceableDomains.addAll(getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getCorrespondenceModel());
		enforceableDomains.addAll(getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getTargetDomainModel());

		if (domainsContainReusableNode(enforceableDomains)){
			// only match reusable patterns
			getGraphMatcher().setMatchReusablePatternOnly(true);
			logger.debug("Now matching REUSABLE PATTERNS IN ENFORCEABLE DOMAINS:");
			getGraphMatcher().getDomainsToMatch().clear();
			getGraphMatcher().getDomainsToMatch().addAll(enforceableDomains);
			getGraphMatcher().match();
			getGraphMatcher().setMatchReusablePatternOnly(false);
		}
	}
	
	private boolean domainContainsReusableNode(DomainModel domainModel) {
		if (getTripleGraphGrammarRule().getDomainsNodesMap().get(domainModel.getTypedModel()) != null) {
			for (Node domainNode : getTripleGraphGrammarRule().getDomainsNodesMap().get(domainModel.getTypedModel())) {
				if (domainNode.isReusable())
					return true;
			}
		}
		return false;
	}
	
	private boolean domainsContainReusableNode(EList<DomainModel> domainModels) {
		for (DomainModel domainModel : domainModels) {
			if (domainContainsReusableNode(domainModel)) return true;
		}
		return false;
	}

	/**
	 * This method checks whether there are produced nodes in the source domain
	 * patterns of the given TGG rule. If that is not the case, it returns TRUE.
	 * If there are produced nodes in the source domain patterns, it checks
	 * whether there are enough unbound source domain objects (see
	 * {@link FrontTransformationProcessorImpl#getUnmatchedSourceDomainObjects()}
	 * ) to satisfy the produced nodes.
	 * 
	 * This is an optimization idea to avoid unnecessary expensive graph
	 * matching. The problem is that this operation is also expensive,
	 * especially the call to
	 * {@link TransformationProcessorUtil#filterEObjectsMatchableByNodesFromList(EList, EList)}
	 * , so that it is not be a good idea to always call this method. A
	 * heuristic that is applied here is that this method is only performed with
	 * a certain probability. This probability increases the more often we see
	 * the check returning false.
	 * 
	 * @return whether it is worth to start matching the rule or not.
	 */
	private boolean existEnoughUnboundSourceObjectsForRulesProducedSourceDomainNodes() {
		//FIXME: Bug in getUnmatchedSourceDomainObjects() causes only objects in the primary resource to be counted, and not referenced objects 
		
		/*if (!(getTransformationProcessor() instanceof FrontTransformationProcessor))
			return true;

		if (probability == 0.0) {
			// just some base probability depending on the number of rules.
			// probability =
			// (1.0/getTripleGraphGrammarRule().getTransformation().getRule().size());
			// just some estimate value, I think best values are somewhere
			// between 0.2 and 0.5:
			probability = 0.33;
		}
		if (Math.random() > probability)
			return true;

		EList<EObject> unmatchedSourceDomainObjects = ((FrontTransformationProcessor) getTransformationProcessor())
		.getUnmatchedSourceDomainObjects();

		// 1. does the rule have produced nodes in its source domain patterns?
		EList<Node> rulesProducedSourceDomainNodes = getTransformationProcessor().getHelper().getProducedSourceDomainRuleNodes(
				getTripleGraphGrammarRule());
		if (rulesProducedSourceDomainNodes.size() > 0) {
			EList<EObject> filteredList = TransformationProcessorUtil.filterEObjectsMatchableByNodesFromList(
					unmatchedSourceDomainObjects, rulesProducedSourceDomainNodes);
			if (filteredList.size() < rulesProducedSourceDomainNodes.size()) {
				probability += (1 - probability) / 1.5;
				return false;
			}
		}*/

		return true;

	}

	/**
	 *  starts the graph matching after setting up the GraphMatcher
	 *  and the element processors.
	 *
	 *  It also applies the heuristic
	 *  {@link #existEnoughUnboundSourceObjectsForRulesProducedSourceDomainNodes()}
	 *  to check beforehand whether the rule can match.
 	 */
	protected boolean ruleMatches(RuleBinding ruleBinding){

		//--DEBUG--//
		DebugSupport ds = this.getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding, true, this);
		
		// optimization, see method's API doc.
		if (!existEnoughUnboundSourceObjectsForRulesProducedSourceDomainNodes()) {
			if (logger.isDebugEnabled())
				logger.debug("Rule "
						+ ruleBinding.getTggRule().getName() + " cannot be matched as there exist not enough unbound source objects.");
			return false;
		}

		setupGraphMatcher(ruleBinding);
		
		boolean match = getGraphMatcher().match();
		
		//--DEBUG--//		
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForPatternMatching(ruleBinding, false, this);
	
		return match;
		
	}
	

	/**
	 * The probability is initialized in
	 * {@link #existEnoughUnboundSourceObjectsForRulesProducedSourceDomainNodes()}
	 * .
	 */
	private double probability = 0.0;

	//private EList<TripleGraphGrammarRule> matchedRules = new BasicEList<TripleGraphGrammarRule>();

	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean processRule(RuleBinding ruleBinding) {
		
		//--DEBUG--//
		DebugSupport ds = this.getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding.getTggRule(), true, this);
		
		setTripleGraphGrammarRule(ruleBinding.getTggRule());
		
		logger.info("Processing TGG rule " + getTripleGraphGrammarRule().getName());
		
// jgreen: I think its not a good idea to simply not apply the rule. We discussed that we can have rules of that form where its application is constrained by a precondition.
//         Why is there a redundant statement in the if-clause anyways?
//		/*
//		 * 0. prechecks: if rule contains no produced source pattern, it must not be applied.
//		 */
//		if (!getTransformationProcessor().getHelper().hasProducedSourceDomainNodes(ruleBinding.getTggRule()) && !getTransformationProcessor().getHelper().hasProducedSourceDomainEdges(ruleBinding.getTggRule())) {
//			if (logger.isDebugEnabled())
//				logger.debug("Rule "
//						+ ruleBinding.getTggRule().getName() + " has no produced source domain elements. Applying would be possible infinitely often. Cancelling rule application.");
//			return false;
//		}

		
		/*
		 * 1. the run of the graph matcher matches the complete source+context pattern
		 *    => rule matches
		 *      => then, try to match the reusable patterns in the enforceable domains of the rule (if there are any)
		 *      => then, enforce the enforceable domains
		 * 2. the run of the graph matcher fails to match
		 *    => the rule does not match, return false
		 */
		if (ruleMatches(ruleBinding)) {

			if (logger.isInfoEnabled())
				logger.info("Rule "
						+ ruleBinding.getTggRule().getName() + " matches. ");

			// enforce patterns:
			edgesToEnforce = calculateEdgesToEnfore(ruleBinding);

			if (!edgesToEnforce.isEmpty()) {
				// check reusable nodes in enforceable patterns
				matchReusablePatternsInEnforceableDomains();
				
				// PARTIALLY REUSABLE PATTERN SEARCH
				// check if previously deleted elements can be reused for the produced corr/target pattern
				if (getTransformationProcessor().usePartiallyReusablePatternSearch()) {
					PartialMatchingTreeVertex reuseThisPattern = reuseDeletedElementsForEnforceableDomains(ruleBinding, ((TransformationProcessorImpl)getTransformationProcessor()).getDeletedNodeBindings());
					if (reuseThisPattern != null) {
						//add all given bindings to our RuleBinding
						while (reuseThisPattern.parent != null) {
							ruleBinding.pushEdgeBinding(reuseThisPattern.boundEdge, reuseThisPattern.boundLink);
							ruleBinding.pushNodeBinding(reuseThisPattern.boundNode, reuseThisPattern.boundElement);

							if (edgesToEnforce.contains(reuseThisPattern.boundEdge))
								edgesToEnforce.remove(reuseThisPattern.boundEdge);

							((TransformationProcessorImpl)getTransformationProcessor()).getDeletedNodeBindings().remove(reuseThisPattern.boundNode);

							reuseThisPattern = reuseThisPattern.parent;
						}
					}
				}
				
				// TODO check enforceable patterns

				//recalculate edges to enforce after having potentially reused target/corresponden side elements
				edgesToEnforce = calculateEdgesToEnfore(ruleBinding);
			}

			
			if (logger.isDebugEnabled())
				logger.debug("Edges to enforce: " + edgesToEnforce);

			while (!edgesToEnforce.isEmpty()) {
				enforceEdge(edgesToEnforce.get(0));
			}

			
			
			EList<Constraint> processedConstraintsList = new UniqueEList<Constraint>();
			// iterate over all constraint enforcement policies. Issue an error when there are unprocessed constraints
			// left and dispose the constraint enforcement policies.
			for (IConstraintEnforcementPolicy constraintEnforcementPolicy : getConstraintEnforcementPolicy()) {
				for (EList<Constraint> constraintsList : constraintEnforcementPolicy.getProcessedConstraints().values()) {
					processedConstraintsList.addAll(constraintsList);
				}
				if (constraintEnforcementPolicy.hasUnprocessedConstraintsLeft()){
					logger.error("There are constraints that could not be processed by a constraint processing policy:" +
							" Policy: " + constraintEnforcementPolicy +
							" TGG rule: " + getTripleGraphGrammarRule().getName() + 
							" unprocessable constraints: "+ constraintEnforcementPolicy.getUnprocessableConstraints());
				}
				constraintEnforcementPolicy.dispose();
			}
			if (getTripleGraphGrammarRule().getAllConstraints().size() != processedConstraintsList.size()){
				// That means that there are constraints that were not considered by any constraint checking/enforcement policy
				EList<Constraint> unconsideredConstraintsList = new BasicEList<Constraint>(getTripleGraphGrammarRule().getAllConstraints());
				unconsideredConstraintsList.removeAll(processedConstraintsList);
				logger.error("There are constraints that were not considered by any constraint processing policy: " 
						+ unconsideredConstraintsList
						+ " in TGG Rule " + getTripleGraphGrammarRule().getName());
			}

			//--DEBUG--//			
			if(ds!=null){
				((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding, false, this);
				((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding.getTggRule(), false, this);
			}			
			
			return true;
		} else {
			
			//--DEBUG--//			
			if(ds!=null){
				((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding, false, this);
				((DebugSupportImpl)ds).debugSupportForApplyRuleOrMatchRuleBindig(ruleBinding.getTggRule(), false, this);
			}
			
			logger.info("Rule "
					+ ruleBinding.getTggRule().getName() + " does NOT match. ");

			return false;
		}
	}
	
	/**
	 * retrieve all unbound edges from the rule binding that are attached
	 * to bound nodes
	 * 
	 * @param ruleBinding
	 * @return
	 */
	private EList<Edge> calculateEdgesToEnfore(RuleBinding ruleBinding){
		EList<Edge> edgesToEnforceList = new UniqueEList<Edge>();
		for (Entry<Node, EObject> nodeBinding : ruleBinding.getNodeToEObjectMap().entrySet()) {
			for (Edge outgoingEdge : nodeBinding.getKey().getAllOutgoingEdges()) {
				if (!ruleBinding.getGraphElementStack().contains(outgoingEdge))
					edgesToEnforceList.add(outgoingEdge);
			}
			for (Edge incomingEdge : nodeBinding.getKey().getAllIncomingEdges()) {
				if (!ruleBinding.getGraphElementStack().contains(incomingEdge))
					edgesToEnforceList.add(incomingEdge);
			}
		}
		return edgesToEnforceList;
	}

	private PartialMatchingTreeVertex reuseDeletedElementsForEnforceableDomains(RuleBinding ruleBinding, Map<EObject, Node> deletedObjects) {
		PartialMatchingTreeRootVertex patternSearchTree = ((GraphMatcherImpl)graphMatcher).reuseDeletedElementsForEnforceableDomains(ruleBinding, deletedObjects);
		
		if (patternSearchTree == null)
			return null;
		
		int maxDepth = -1;
		PartialMatchingTreeVertex maxDepthNode = null; 
		Queue<PartialMatchingTreeVertex> queue = new LinkedList<PartialMatchingTreeVertex>();
		queue.add(patternSearchTree);
		while (!queue.isEmpty()) {
			PartialMatchingTreeVertex currNode = queue.poll();
			if (currNode.getDepth() > maxDepth) {
				maxDepthNode = currNode;
				maxDepth = currNode.getDepth();
			}
			queue.addAll(currNode.children);
		}
		
		return maxDepthNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method tries to repair an invalid rule binding by trying to re-enforce the
	 * constraints and rebuilding the rule structure.
	 * 
	 * The graph matching done during rule checking has stored the defect type which
	 * can be reused here.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean repairRule(RuleBinding ruleBinding) {
		// TODO: implement this method
		return false;
	}


	protected EList<Edge> edgesToEnforce;

	@SuppressWarnings("unchecked")
	protected void enforceEdge(Edge edge) {

		Node sourceNode = getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getSourceNode());
		Node targetNode = getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getTargetNode());

		if (logger.isDebugEnabled()) {
			logger.debug("Enforcing edge: " + sourceNode.getName() + " -> " + targetNode.getName());
		}

		if (getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().containsKey(edge)) {
			logger.warn("ruleEdgeToEdgeBindingUtilityMap.containsKey(edge) should not happen");
			edgesToEnforce.remove(edge);
		}


		EObject sourceObject = null;
		EObject targetObject = null;

		if (!getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(sourceNode)
				&& !getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(targetNode)) {
			logger.warn("Edge cannot be enforced: source and target node are not bound!");
		}

		if (getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(sourceNode)
				&& !getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(targetNode)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Target node not bound, creating target object...");
			}

			targetObject = enforceNode(targetNode);
		}

		if (!getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(sourceNode)
				&& getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(targetNode)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Source node not bound, creating source object...");
			}

			sourceObject = enforceNode(sourceNode);
		}

		if (getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(sourceNode)
				&& getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(targetNode)) {

			sourceObject = getGraphMatcher().getRuleBinding().getNodeToEObjectMap().get(sourceNode);
			targetObject = getGraphMatcher().getRuleBinding().getNodeToEObjectMap().get(targetNode);

			// issue warning if enforcing the edge will result in changing the container of an object.
			if (edge.getTypeReference().isContainment() 
					&& targetObject.eContainer() != null 
					&& !targetObject.eContainer().equals(sourceObject)){
				logger.warn("Trying to create a containment link, but the target object is already contained elsewhere! \n" + 
						"The target object: " + targetObject + "\n" +
						"The target object's current container: " + targetObject.eContainer() + "\n" +
						"The source object (new container): " + sourceObject + "\n" +
						"The rule that is being enforced : " + getGraphMatcher().getRuleBinding().getTggRule().getName() + "\n" +
						"The edge that is being enforced : " + edge + "\n" +
						"The edge's type reference : " + edge.getTypeReference());
			}
			
			if (edge.getTypeReference().isMany()) {
				try {
					((EList<EObject>) sourceObject.eGet(edge.getTypeReference())).add(targetObject);
				} catch (Exception e) {
					logger.error("Failed to add reference to '" + edge.getTypeReference().getName() + "'./n In Object'" + sourceObject
							+ "'./n Reference '" + targetObject + "'.");
				}
				if (logger.isDebugEnabled())
					logger.debug("added reference: sourceObject:" + sourceObject + ", edge.getTypeReference(): "
							+ edge.getTypeReference() + ", targetObject: " + targetObject
							+ ", targetObject.eContainer: " + targetObject.eContainer()
							+ ", targetObject.eContainingFeature: " + targetObject.eContainingFeature());
			} else {
				if (sourceObject.eIsSet(edge.getTypeReference()) && sourceObject.eGet(edge.getTypeReference()) != targetObject)
					logger.warn("Reference already set when trying to enforce edge " + edge + " of TGG rule " + getTripleGraphGrammarRule() + " on object " + sourceObject +
							"trying to refer to: \"" + targetObject + "\" while already set is \"" + sourceObject.eGet(edge.getTypeReference()) + "\"");
				try {
					sourceObject.eSet(edge.getTypeReference(), targetObject);
				} catch (Exception e) {
					logger.error("Failed to set reference to '" + edge.getTypeReference().getName() + "'./n In Object'" + sourceObject
							+ "'./n Reference '" + targetObject + "'.");
				}
				if (logger.isDebugEnabled())
					logger.debug("sourceObject.eSet(edge.getTypeReference(), targetObject): sourceObject:"
							+ sourceObject + ", edge.getTypeReference(): " + edge.getTypeReference()
							+ ", targetObject: " + targetObject);
			}

			EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
			virtualModelEdge.add(sourceObject);
			virtualModelEdge.add(targetObject);
			virtualModelEdge.add(edge.getTypeReference());

			//Call all registered EdgeProcessingPolicyies' enforceEdge()
			for (IEdgeEnforcementPolicy edgeEnforcementPolicy : getEdgeEnforcementPolicy()) {
				edgeEnforcementPolicy.enforceEdge(edge, virtualModelEdge);
			}

			if (logger.isDebugEnabled()) {
				logger.debug(" Created reference " + virtualModelEdge);
			}

			getGraphMatcher().getRuleBinding().pushEdgeBinding(edge, virtualModelEdge);
			getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().put(edge, virtualModelEdge);

			edgesToEnforce.remove(edge);

		}

	}

	protected EObject enforceNode(Node node) {
		
		// enforce the node: create the object
		logger.debug("enforceNode: " + node);
		EObject newModelObject = node.getEType().getEPackage().getEFactoryInstance().create((EClass) node.getEType());
		
		//Call all registered NodeProcessingPolicyies' enforceNode()
		for (INodeEnforcementPolicy nodeEnforcementPolicy : getNodeEnforcementPolicy()) {
			nodeEnforcementPolicy.enforceNode(node, newModelObject);
		}

		getGraphMatcher().getRuleBinding().pushNodeBinding(node, newModelObject);
		
		//--DEBUG--//
		DebugSupport ds = this.getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForEnforcedNode(node, newModelObject, getGraphMatcher().getRuleBinding(), this, true);
		
		if (logger.isDebugEnabled()) {
			logger.debug(" Created " + newModelObject);
		}

		// add to be enforced edges:
		EList<Edge> attachedEdges = new BasicEList<Edge>();
		attachedEdges.addAll(node.getAllIncomingEdges());
		attachedEdges.addAll(node.getAllOutgoingEdges());
		for (Edge edge : attachedEdges) {
			if (!getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().containsKey(edge)) {
				if (!edgesToEnforce.contains(edge) && !edge.isLeft()) {
					if (logger.isDebugEnabled()) {
						logger.debug("ADDING to-be-enforced edge: " + edge);
					}
					edgesToEnforce.add(edge);
				}
			}
		}

		for (IConstraintEnforcementPolicy constraintEnforcementPolicy : getConstraintEnforcementPolicy()) {
			if (!constraintEnforcementPolicy.enforceConstraints(node, newModelObject)) {
				logger.error("Constraint problems during model object creation. This should not happen...");
				// This should really not happen...
			}
		}

		return newModelObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTransformationProcessor((TransformationProcessor)otherEnd, msgs);
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				if (graphMatcher != null)
					msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER, null, msgs);
				return basicSetGraphMatcher((GraphMatcher)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				return basicSetTransformationProcessor(null, msgs);
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				return basicSetGraphMatcher(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				return eInternalContainer().eInverseRemove(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR, TransformationProcessor.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE:
				if (resolve) return getTripleGraphGrammarRule();
				return basicGetTripleGraphGrammarRule();
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				return getTransformationProcessor();
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				return getGraphMatcher();
			case InterpreterPackage.RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY:
				return getNodeEnforcementPolicy();
			case InterpreterPackage.RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY:
				return getEdgeEnforcementPolicy();
			case InterpreterPackage.RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY:
				return getConstraintEnforcementPolicy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE:
				setTripleGraphGrammarRule((TripleGraphGrammarRule)newValue);
				return;
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)newValue);
				return;
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)newValue);
				return;
			case InterpreterPackage.RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY:
				getNodeEnforcementPolicy().clear();
				getNodeEnforcementPolicy().addAll((Collection<? extends INodeEnforcementPolicy>)newValue);
				return;
			case InterpreterPackage.RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY:
				getEdgeEnforcementPolicy().clear();
				getEdgeEnforcementPolicy().addAll((Collection<? extends IEdgeEnforcementPolicy>)newValue);
				return;
			case InterpreterPackage.RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY:
				getConstraintEnforcementPolicy().clear();
				getConstraintEnforcementPolicy().addAll((Collection<? extends IConstraintEnforcementPolicy>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE:
				setTripleGraphGrammarRule((TripleGraphGrammarRule)null);
				return;
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)null);
				return;
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)null);
				return;
			case InterpreterPackage.RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY:
				getNodeEnforcementPolicy().clear();
				return;
			case InterpreterPackage.RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY:
				getEdgeEnforcementPolicy().clear();
				return;
			case InterpreterPackage.RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY:
				getConstraintEnforcementPolicy().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE:
				return tripleGraphGrammarRule != null;
			case InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR:
				return getTransformationProcessor() != null;
			case InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER:
				return graphMatcher != null;
			case InterpreterPackage.RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY:
				return nodeEnforcementPolicy != null && !nodeEnforcementPolicy.isEmpty();
			case InterpreterPackage.RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY:
				return edgeEnforcementPolicy != null && !edgeEnforcementPolicy.isEmpty();
			case InterpreterPackage.RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY:
				return constraintEnforcementPolicy != null && !constraintEnforcementPolicy.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // RuleProcessorImpl
