package de.upb.swt.qvt.tgg.interpreter.util;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EContentsEList;
import org.eclipse.emf.ecore.util.EContentsEList.FeatureIterator;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.ECrossReferenceEList;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

public class RelevantReferencesCrossReferenceAdapter extends
		ECrossReferenceAdapter {

	public RelevantReferencesCrossReferenceAdapter(
			Collection<?> relevantEReferences) {
		inverseCrossReferencer = createInverseCrossReferencer(relevantEReferences);
	}

	protected InverseRelevantReferencesCrossReferencer createInverseCrossReferencer(
			Collection<?> relevantEReferences) {
		return new InverseRelevantReferencesCrossReferencer(relevantEReferences);
	}

	protected class InverseRelevantReferencesCrossReferencer extends
			ECrossReferenceAdapter.InverseCrossReferencer {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		protected Collection<?> relevantEReferences;
		
		protected InverseRelevantReferencesCrossReferencer(Collection<?> relevantEReferences) {
			super();
			this.relevantEReferences = relevantEReferences;
		}
		
	    /**
	     * Return true if the specified eReference from eObject to crossReferencedEObject should be
	     * considered a cross reference by this cross referencer.
	     * @param eObject an object in the cross referencer's content tree.
	     * @param eReference a reference from the object.
	     * @param crossReferencedEObject the target of the specified reference.
	     * @return if the cross referencer should consider the specified reference a cross reference.
	     */
		@Override
	    protected boolean crossReference(EObject eObject, EReference eReference, EObject crossReferencedEObject)
	    {
			//TODO check whether it's necessary to include the super method...
			return relevantEReferences.contains(eReference);
			//return super.crossReference(eObject, eReference, crossReferencedEObject);
	    } 

	    @Override
	    protected void handleCrossReference(EObject eObject)
	    {
	    	super.handleCrossReference(eObject);
	    	handleContainmentReferences(eObject);
	    }

		@Override
		protected FeatureIterator<EObject> getCrossReferences(EObject eObject) {
			return new ECrossReferenceEList.FeatureIteratorImpl<EObject>(
					eObject) {
				@Override
				protected boolean isIncluded(
						EStructuralFeature eStructuralFeature) {
					try {
						// We have to type-check the referenced object, as objects
						// of wrong type let the crossreferencer fail.
						Object referencedObject = eObject.eGet(eStructuralFeature, true);
						if (referencedObject instanceof EObject && !eStructuralFeature.getEType().isInstance(((EObject)referencedObject)))
							return false;
					} catch (Exception e) {
						return false;
					}
					
					return FeatureMapUtil.isFeatureMap(eStructuralFeature)
							|| RelevantReferencesCrossReferenceAdapter.this
									.isIncluded((EReference) eStructuralFeature);
				}

				@Override
				protected boolean resolve() {
					return RelevantReferencesCrossReferenceAdapter.this
							.resolve();
				}
			};
		}

		protected void handleContainmentReferences(EObject eObject){
			
			InternalEObject internalEObject = (InternalEObject)eObject;
			for (EContentsEList.FeatureIterator<EObject> containmentReferences 
					= (EContentsEList.FeatureIterator<EObject>) internalEObject.eContents().iterator(); containmentReferences.hasNext();)
			{
				EObject containedEObject = containmentReferences.next();
				if (containedEObject != null)
				{
					EReference eReference = (EReference)containmentReferences.feature();
					if (crossReference(internalEObject, eReference, containedEObject))
					{
						add(internalEObject, eReference, containedEObject);
						if (eReference.getEOpposite() != null){
							add((InternalEObject) containedEObject, eReference.getEOpposite(), internalEObject);
						}
					}
				}
			}
		}
		
	    /**
	     * Reset this cross referencer's object set.
	     */
		@Override
	    protected void done()
	    {
	      emfObjects = null;
	      relevantEReferences = null;
	    }
	}// end protected class InverseCrossReferencer

//	  public Collection<EStructuralFeature.Setting> getInverseReferences(EObject eObject, boolean resolve)
//	  {
//	    Collection<EStructuralFeature.Setting> result = new ArrayList<EStructuralFeature.Setting>();
//	    
//	    if (resolve)
//	    {
//	      resolveAll(eObject);
//	    }
//	    
//	    EObject eContainer = eObject.eContainer();
//	    if (eContainer != null)
//	    {
//	      result.add(((InternalEObject)eContainer).eSetting(eObject.eContainmentFeature()));
//	    }
//	    
//	    Collection<EStructuralFeature.Setting> nonNavigableInverseReferences = inverseCrossReferencer.get(eObject);
//	    if (nonNavigableInverseReferences != null)
//	    {
//	      result.addAll(nonNavigableInverseReferences);
//	    }
//	    
//	    for (EReference eReference : eObject.eClass().getEAllReferences())
//	    {
//	      EReference eOpposite = eReference.getEOpposite();
//	      // jgreen TODO: check whether modifying this condition works--the problem is that containment references may not be cross-referenced,
//	      // I don't know whether this helps or not...
////	      if (eOpposite != null && !eReference.isContainer() && eObject.eIsSet(eReference))
//		  if (eOpposite != null && !eReference.isContainer() && eObject.eIsSet(eReference))
//	      {
//	        if (eReference.isMany())
//	        {
//	          Object collection = eObject.eGet(eReference);
//	          for (@SuppressWarnings("unchecked") Iterator<EObject> j = 
//	                 resolve() ? 
//	                   ((Collection<EObject>)collection).iterator() : 
//	                   ((InternalEList<EObject>)collection).basicIterator(); 
//	               j.hasNext(); )
//	          {
//	            InternalEObject referencingEObject = (InternalEObject)j.next();
//	            result.add(referencingEObject.eSetting(eOpposite));
//	          }
//	        }
//	        else
//	        {
//	          result.add(((InternalEObject)eObject.eGet(eReference, resolve())).eSetting(eOpposite));
//	        }
//	      }
//	    }
//	    
//	    return result;
//	  }

}
