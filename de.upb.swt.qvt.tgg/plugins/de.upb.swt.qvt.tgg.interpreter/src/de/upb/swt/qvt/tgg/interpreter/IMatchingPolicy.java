/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IMatching Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getRuleProcessor <em>Rule Processor</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIMatchingPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IMatchingPolicy extends EObject {
	/**
	 * Returns the value of the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Processor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Processor</em>' reference.
	 * @see #setTransformationProcessor(TransformationProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIMatchingPolicy_TransformationProcessor()
	 * @model
	 * @generated
	 */
	TransformationProcessor getTransformationProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getTransformationProcessor <em>Transformation Processor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Processor</em>' reference.
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	void setTransformationProcessor(TransformationProcessor value);

	/**
	 * Returns the value of the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Processor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Processor</em>' reference.
	 * @see #setRuleProcessor(RuleProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIMatchingPolicy_RuleProcessor()
	 * @model
	 * @generated
	 */
	RuleProcessor getRuleProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getRuleProcessor <em>Rule Processor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Processor</em>' reference.
	 * @see #getRuleProcessor()
	 * @generated
	 */
	void setRuleProcessor(RuleProcessor value);

} // IMatchingPolicy
