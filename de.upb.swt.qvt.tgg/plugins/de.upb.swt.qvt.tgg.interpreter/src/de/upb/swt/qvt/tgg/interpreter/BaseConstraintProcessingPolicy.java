/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Constraint Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class uses three distinct maps to maintain 
 * (a) the constraints that have not been processed yet, because they were not yet considered (unprocessedConstraints)
 * (b) the constraints that have not been processed yet, because we could not evaluate them yet (unprocessableConstraints)
 * (for instance because the necessary node bindings do not yet exist)
 * (c) the constraints that have been processed (checked or enforced) successfully (processedConstraints)
 * 
 * The maps mentioned above maintain lists of constraints by TGG nodes. Although the signature
 * of the maps is the same, nodes and constraints have different relationships in these maps:
 * (a) unprocessedConstraints: an entry contains all constraints for a key node that is the slot node of the constraint
 * (b/c) unprocessableConstraints, processedConstraints: an entry contains all constraints that could not be processed yet 
 * or were processed successfully at the point of matching the key node.
 * 
 * This base class implements an adapter for the rule binding of the rule currently processed.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getBaseConstraintProcessingPolicy()
 * @model abstract="true"
 * @generated
 */
public interface BaseConstraintProcessingPolicy extends IConstraintEnforcementPolicy {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.upb.swt.qvt.tgg.interpreter.EValueClashException de.upb.swt.qvt.tgg.interpreter.ConstraintUnprocessableException"
	 * @generated
	 */
	boolean checkConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException, ConstraintUnprocessableException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.upb.swt.qvt.tgg.interpreter.EValueClashException de.upb.swt.qvt.tgg.interpreter.ConstraintUnprocessableException"
	 * @generated
	 */
	boolean enforceConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException, ConstraintUnprocessableException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * clients may overwriting this method must invoke
	 * super.nodeBindingRemovedFromRuleBinding(nodeBinding)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void nodeBindingRemovedFromRuleBinding(Node nodeBinding);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * clients may overwriting this method must invoke
	 * super.nodeBindingAddedToRuleBinding(nodeBinding)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void nodeBindingAddedToRuleBinding(Node node, EObject eObject);
} // BaseConstraintProcessingPolicy
