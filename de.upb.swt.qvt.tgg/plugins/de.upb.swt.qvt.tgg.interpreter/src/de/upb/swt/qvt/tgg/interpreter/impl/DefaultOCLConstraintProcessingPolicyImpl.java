/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.EcoreEnvironment;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.expressions.ExpressionsFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.expressions.Variable;
import org.eclipse.ocl.helper.OCLHelper;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.impl.OCLConstraintImpl;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Default OCL Constraint Processing Policy</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class DefaultOCLConstraintProcessingPolicyImpl extends BaseConstraintProcessingPolicyImpl implements
		DefaultOCLConstraintProcessingPolicy {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(
			DefaultOCLConstraintProcessingPolicyImpl.class.getName());

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultOCLConstraintProcessingPolicyImpl() {
		super();
	}

	protected OCL<?, EClassifier, ?, EStructuralFeature, ?, EParameter, ?, ?, ?, org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl;
	protected OCLHelper<EClassifier, ?, EStructuralFeature, org.eclipse.ocl.ecore.Constraint> helper;

	public boolean canCheckConstraint(Node node, EObject eObject, Constraint constraint){
		if (!(constraint instanceof OCLConstraint)) {
			// Here we only support OCL constraints...
			return false;
		}
		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		if (oclConstraint.getSlotAttribute() != null && !oclConstraint.getSlotNode().getEType().getEAllAttributes().contains(oclConstraint.getSlotAttribute())){
			// this may happen for example when the constraint refers to an attribute value of an applied
			// stereotype.
			logger.debug("The slot attribute is not an attribute of the slot node's type class, " +
				"the DefaultOCLConstraintProcessor is not responsible");
			return false;
		}
		return true;
	}
	
	@Override
	public boolean checkConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException,
			ConstraintUnprocessableException {

//		if (!canCheckConstraint(node, eObject, constraint))
//			throw new ConstraintUnprocessableException("The default OCL processing policy is not responsible for checking this constraint.");

		Object result = null;
		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		// fetch currently processed TGG rule from the ruleBinding (see FrontTransformationProcessor.createInitialRuleBinding())
		TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
		Node refinedConstraintSlotNode = tggRule.getMostRefinedNodeOf(oclConstraint.getSlotNode());
		
		//The constraint may be scheduled to be checked at the end of the matching process.
		//In this case, only continue when there is no edge left to match.
		if (oclConstraint.isEvaluateAfterMatching()) {
			if (!getGraphMatcher().isNoElementsToMatchLeft())
				throw new ConstraintUnprocessableException("Constraint is set to be evaluated at the end of the matching.");
		}

		boolean constraintHasSlotAttribute = (oclConstraint.getSlotAttribute() != null);
		EObject contextObject = null;
		
		// check string literal values -> do this without invoking OCL!
		if (constraintHasSlotAttribute && 
				oclConstraint.getExpression().indexOf("'")==0 && 
				oclConstraint.getExpression().indexOf("'", 1) == oclConstraint.getExpression().length()-1){
			return eObject.eGet(oclConstraint.getSlotAttribute()).equals(oclConstraint.getExpression().substring(1, oclConstraint.getExpression().length()-1));
		}
		
	    // if there is a statically computed list of required node bindings, 
		//  check if all required nodes are bound  
	    List<Annotation> annots = oclConstraint.getAnnotationsByKeyString(OCLConstraintImpl.ANNOTATION_REQUIRED_NODES);
	    if (annots.size() > 0) {
	    	for (EObject n : annots.get(0).getValue()) {
	    		assert n instanceof Node;
	    		if (!getGraphMatcher().getRuleBinding().getNodeToEObjectMap().containsKey(node))
					throw new ConstraintUnprocessableException("Not all required nodes are bound, yet.");
	    	}
	    }
	    
		// TODO this is a temporary modification. Instead of adding and
		// removing variables and their bindings whenever a node binding is
		// added or removed from the rule binding, we initialize a new OCL
		// environment each time that we check or enforce a constraint.
		initializeOCL(oclConstraint);
//		if (ocl == null) {
//			initializeOCL();
//		}

		if (constraintHasSlotAttribute) {
			helper.setAttributeContext(refinedConstraintSlotNode.getEType(), oclConstraint.getSlotAttribute());
		} else {
			if (refinedConstraintSlotNode == null) {
				helper.setContext(EcoreFactory.eINSTANCE.createEClass());
			} else {
				helper.setContext(refinedConstraintSlotNode.getEType());
				contextObject = getGraphMatcher().getRuleBinding().getNodeToEObjectMap().get(
						refinedConstraintSlotNode);
			}
		}

		if (!constraintHasSlotAttribute) {
			// there was no slot attribute set. Then we check whether the
			// constraint is an
			// expression returning a Boolean value.
			try {
				OCLExpression<EClassifier> expr = helper.createQuery(oclConstraint.getExpression());
				result = ocl.evaluate(contextObject, expr);
				if (!(result instanceof Boolean)) {
					throw new ValueClashException("OCL expression \"" + oclConstraint.getExpression()
							+ "\" in TGG rule \"" + oclConstraint.getTripleGraphGrammarRule().getName()
							+ "\" does not evaluate to a boolean value."
							+ "It should return a boolean value since no slot attribute is assigned to the constraint");
				} else {
					if (!(Boolean) result) {
						return false;
					}
				}
			} catch (ParserException e) {
				if (e instanceof SemanticException && e.getMessage().contains("Unrecognized variable")) {
					throw new ConstraintUnprocessableException(e.getMessage());
				}
				logger.error("An exception occurred while parsing the ocl expression \""
						+ oclConstraint.getExpression() + "\"", e);
			}
		} else {// has a slot attribute
			try {
				/*
				 * The node and object passed to this method are not necessarily
				 * the slot node and slot object of this constraint, because the
				 * constraint could be a previously unprocessable constraint
				 * attached to another node.
				 */
				if (refinedConstraintSlotNode != node)
					eObject = getRuleBinding().getNodeToEObjectMap().get(refinedConstraintSlotNode);

				if (eObject == null)
					throw new ConstraintUnprocessableException("");

				OCLExpression<EClassifier> expr = helper.createQuery(oclConstraint.getExpression());
				result = ocl.evaluate(null, expr);
				Object slotAttribValue = eObject.eGet(oclConstraint.getSlotAttribute());
				boolean isSatisfied = (result != null) ? result.equals(slotAttribValue) : (slotAttribValue == null);
				return isSatisfied;
			} catch (ParserException e) {
				if (e instanceof SemanticException && e.getMessage().contains("Unrecognized variable")) {
					throw new ConstraintUnprocessableException(e.getMessage());
				}
				logger.error("An exception occurred while parsing the ocl expression \""
						+ oclConstraint.getExpression() + "\"", e);
			}
		}

		return true;
	}

	public boolean canEnforceConstraint(Node node, EObject eObject, Constraint constraint){
		if (!(constraint instanceof OCLConstraint)) {
			// Here we only do OCL constraints...
			return false;
		}
		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		if (oclConstraint.getSlotAttribute() == null){
			//there is no slot attribute to enforce
			return false;
		}
		if (!oclConstraint.getSlotNode().getEType().getEAllAttributes().contains(oclConstraint.getSlotAttribute())){
			// this may happen for example when the constraint refers to an attribute value of an applied
			// stereotype.
			logger.debug("The slot attribute is not an attribute of the slot node's type class, " +
				"the DefaultOCLConstraintProcessor is not responsible");
			return false;
		}
		return true;
	}
	
	@Override
	public boolean enforceConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException,
			ConstraintUnprocessableException {
		
//		if (!canEnforceConstraint(node, eObject, constraint))
//			throw new ConstraintUnprocessableException("The default OCL processing policy is not responsible for enforing this constraint.");

		Object result = null;
		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		// fetch currently processed TGG rule from the ruleBinding (see FrontTransformationProcessor.createInitialRuleBinding())
		TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
		Node refinedConstraintSlotNode = tggRule.getMostRefinedNodeOf(oclConstraint.getSlotNode());

		if (oclConstraint.isCheckonly()) return checkConstraint(node, eObject, constraint);

		//The constraint may be scheduled to be checked at the end of the matching process.
		//In this case, only continue when there is no edge left to match.
		if (oclConstraint.isEvaluateAfterMatching()) {
			if (!getGraphMatcher().isNoElementsToMatchLeft())
				throw new ConstraintUnprocessableException("Constraint is set to be evaluated at the end of the matching.");
		}
		
		/*
		 * The node and object passed to this method are not necessarily the
		 * slot node and slot object of this constraint. If the passed node
		 * is the slot node and the passed object is the slot object, then
		 * the node cannot be bound. When the node is already bound, the
		 * object bound to the node is the slot object.
		 */
		EObject slotObject;
		if (getRuleBinding().getNodeToEObjectMap().get(refinedConstraintSlotNode) != null) {
			slotObject = getRuleBinding().getNodeToEObjectMap().get(refinedConstraintSlotNode);
		} else {
			slotObject = eObject;
		}

		
		// enforce string literal values -> do this without invoking OCL!
		if (oclConstraint.getExpression().indexOf("'")==0 && 
				oclConstraint.getExpression().indexOf("'", 1)==oclConstraint.getExpression().length()-1){

            slotObject.eSet(oclConstraint.getSlotAttribute(),
                    oclConstraint.getSlotAttribute().getEType().getEPackage().getEFactoryInstance().createFromString(
                            (EDataType)oclConstraint.getSlotAttribute().getEType(),
                            oclConstraint.getExpression().substring(1, oclConstraint.getExpression().length()-1)
                        )
                    );

			return true;
		}

		
		
		// TODO this is a temporary modification. Instead of adding and
		// removing variables and their bindings wheneve a node binding is
		// added or removed from the rule binding, we initialize a new OCL
		// environment each time that we check or enforce a constraint.
		initializeOCL(oclConstraint);

//			if (ocl == null) {
//				initializeOCL();
//			}

		helper.setAttributeContext(refinedConstraintSlotNode.getEType(), oclConstraint.getSlotAttribute());
		try {
			OCLExpression<EClassifier> expr = helper.createQuery(oclConstraint.getExpression());
			result = ocl.evaluate(null, expr);
			if (result != null){
				// eki, 27. 3. 3012: fixed the problem with conversion of Double (as OCL representation
				//                       for real) to a float here (added some other type conversions, but
				//                       this is way from being complete)
				if (result instanceof Number ) {
					Class<?> clazz = oclConstraint.getSlotAttribute().getEType().getInstanceClass();
					if (clazz.equals(float.class)) {
						result = ((Number) result).floatValue();
					} else if (clazz.equals(double.class)) {
						result = ((Number) result).doubleValue();
					} else if (clazz.equals(int.class)) {
						result = ((Number) result).intValue();
					}
				}
				
				// TODO: test whether this also makes the number conversion above obsolete.
				if (result.getClass() != oclConstraint.getSlotAttribute().getEType().getInstanceClass()){
		            slotObject.eSet(oclConstraint.getSlotAttribute(),
		                    oclConstraint.getSlotAttribute().getEType().getEPackage().getEFactoryInstance().createFromString(
		                            (EDataType)oclConstraint.getSlotAttribute().getEType(),
		                            result.toString()
		                        )
		                    );
					
				}else{
					slotObject.eSet(oclConstraint.getSlotAttribute(), result);
				}
			}
			else {
				slotObject.eUnset(oclConstraint.getSlotAttribute());
			}
		} catch (ParserException e) {
			if (e instanceof SemanticException && e.getMessage().contains("Unrecognized variable")) {
				throw new ConstraintUnprocessableException(e.getMessage());
			}else
				logger.error("An exception occurred while parsing the ocl expression \""
					+ oclConstraint.getExpression() + "\"", e);
		} catch (ClassCastException e) {
			// jgreen: we want to log this in a meaningful way.
			logger.error("A class cast exception occurred while enforcing the evaluation result of \""
					+ oclConstraint.getExpression() + "\" " + "The query evaluated to \"" + result
					+ "\" and shall be assigned to the attribute " + "\"" + oclConstraint.getSlotAttributeName()
					+ "\" of " + slotObject + ".", e);
		}


		return true;
	}

	
	/**
	 * Initialize OCL interpreter;
	 */
	protected void initializeOCL() {
		if (ocl != null)
			ocl.dispose();
		Interpreter interpreter = getGraphMatcher().getRuleProcessor().getTransformationProcessor().getInterpreter(); 
		if (interpreter.getOclEnvironment() != null){
				EcoreEnvironment nestedEcoreEnvironment = (EcoreEnvironment) EcoreEnvironmentFactory.INSTANCE.createEnvironment(interpreter.getOclEnvironment());
				ocl = OCL.newInstance(nestedEcoreEnvironment);
			}
		else
			ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
		helper = ocl.createOCLHelper();
		for (Entry<Node, EObject> nodeBinding : getRuleBinding().getNodeToEObjectMap().entrySet()) {
			if (nodeBinding.getValue() != null && nodeBinding.getKey().getName() != null && !nodeBinding.getKey().getName().equals("")) {
				System.out.println(nodeBinding.getKey() + " -> " + nodeBinding.getValue());
				addNewVariable(nodeBinding.getKey(), nodeBinding.getValue());
			}
		}
		
	}
	
	protected void initializeOCL(OCLConstraint oclConstraint) {
		initializeOCL();
		if (oclConstraint.getExpression().contains("objectsBoundToSameRule"))
			addObjectsBoundToSameRuleVariable();
		if (oclConstraint.getExpression().contains("globallyBoundObjects"))
			addGloballyBoundObjectsVariable();
	}
	
	private void addObjectsBoundToSameRuleVariable() {
		TripleGraphGrammarRule tggRule = getGraphMatcher().getRuleProcessor().getTripleGraphGrammarRule();
		EList<EObject> objectsBoundToSameRule = new BasicEList<EObject>();
		for (Entry<EObject, EList<Node>> mapEntry : getGraphMatcher().getRuleProcessor().getTransformationProcessor().getGlobalNodeBinding().entrySet()) {
			for (Node node : mapEntry.getValue()) {
				if (((TripleGraphGrammarRule)node.eContainer()).equals(tggRule)){
					objectsBoundToSameRule.add(mapEntry.getKey());
					break;
				}
			}
		}
		addNewVariable("objectsBoundToSameRule", ocl.getEnvironment().getOCLStandardLibrary().getSet(), objectsBoundToSameRule);
	}

	private void addGloballyBoundObjectsVariable() {
		addNewVariable("globallyBoundObjects", ocl.getEnvironment().getOCLStandardLibrary().getSet(), getGraphMatcher().getRuleProcessor().getTransformationProcessor().getGlobalNodeBinding().keySet());
	}

	@Override
	public void nodeBindingAddedToRuleBinding(Node node, EObject eObject) {
		super.nodeBindingAddedToRuleBinding(node, eObject);
		addNewVariable(node, eObject);
	}

	@Override
	public void nodeBindingRemovedFromRuleBinding(Node node) {
		super.nodeBindingRemovedFromRuleBinding(node);

	}

	protected void addNewVariable(Node node, EObject eObject) {
		if (node.getName() != null && !node.getName().equals("")) {
			addNewVariable(node.getName(), node.getEType(), eObject);
		}
	}

	protected void addNewVariable(String variableName, EClassifier type, Object value) {
		if (ocl == null) {
			initializeOCL();
		}

		// TODO variable may not be removed!!
		for (Variable<?, ?> definedVariable : ocl.getEnvironment().getVariables()) {
			if (definedVariable.getName().equals(variableName))
				return;
		}
		Variable<EClassifier, EParameter> var = ExpressionsFactory.eINSTANCE.createVariable();
		var.setName(variableName);
		if (type == null) {
			if (value instanceof EObject) {
				type = ((EObject) value).eClass();
			} else if (value instanceof String) {
				type = EcorePackage.Literals.ESTRING;
			} else if (value instanceof Integer) {
				type = EcorePackage.Literals.EINTEGER_OBJECT;
			}
		}
		var.setType(type);
		assert (value != null);
		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) == null);
		ocl.getEnvironment().addElement(var.getName(), var, true);
		ocl.getEvaluationEnvironment().add(var.getName(), value);
		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) != null);
		// if (logger.isDebugEnabled())
		// logger.debug("Variable added: " + var.getName() + " type: " +
		// var.getType() + " value: " + value);
	}

	protected void removeVariable(Node node) {
		if (node.getName() != null) {
			removeVariable(node.getName());
		}
	}

	protected void removeVariable(String variableName) {
		logger.debug("removing " + variableName);
		assert (ocl.getEvaluationEnvironment().getValueOf(variableName) != null);
		ocl.getEvaluationEnvironment().remove(variableName);
		for (Variable<EClassifier, EParameter> var : ocl.getEnvironment().getVariables()) {
			if (var.getName().equals(variableName))
				ocl.getEnvironment().getVariables().remove(var);
		}
		// removing variables from the environment doesn't work...
		// for (Variable<EClassifier, EParameter> var :
		// ocl.getEnvironment().getVariables()){
		// if (var.getName().equals(variableName))
		// assert(false);
		// }

	}

	@Override
	public void dispose() {
		if (ocl != null)
			ocl.dispose();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY;
	}

} // DefaultOCLConstraintProcessingPolicyImpl
