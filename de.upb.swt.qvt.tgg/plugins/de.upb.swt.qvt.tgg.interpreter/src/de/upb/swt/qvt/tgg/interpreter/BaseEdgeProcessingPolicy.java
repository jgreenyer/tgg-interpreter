/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getBaseEdgeProcessingPolicy()
 * @model abstract="true"
 * @generated
 */
public interface BaseEdgeProcessingPolicy extends IEdgeEnforcementPolicy, IEdgeMatchingPolicy {
} // BaseEdgeProcessingPolicy
