/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainModel <em>Source Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainModel <em>Target Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceModel <em>Correspondence Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getMode <em>Mode</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainObject <em>Source Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainObject <em>Correspondence Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainObject <em>Target Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNode <em>Source Domain Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNode <em>Correspondence Domain Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodes <em>Target Domain Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNodesByTGGRule <em>Source Domain Nodes By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNodesByTGGRule <em>Correspondence Domain Nodes By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodeByTGGRule <em>Target Domain Node By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#isDeactivateFront <em>Deactivate Front</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='eachDomainModelEitherSourceCorrespondenceOrTarget\r\neitherResourceURIOrRootObjectsSetForDomainModels'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL eachDomainModelEitherSourceCorrespondenceOrTarget='self.sourceDomainModel->select(dm|self.correspondenceModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\nand self.correspondenceModel->select(dm|self.sourceDomainModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\nand self.targetDomainModel->select(dm|self.sourceDomainModel->includes(dm) or self.correspondenceModel->includes(dm))->isEmpty()' eitherResourceURIOrRootObjectsSetForDomainModels='self.sourceDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\nself.targetDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\nself.correspondenceModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty()'"
 * @generated
 */
public interface ApplicationScenario extends EObject {
	/**
	 * Returns the value of the '<em><b>Source Domain Model</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Domain Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Domain Model</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_SourceDomainModel()
	 * @model
	 * @generated
	 */
	EList<DomainModel> getSourceDomainModel();

	/**
	 * Returns the value of the '<em><b>Target Domain Model</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Domain Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Domain Model</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_TargetDomainModel()
	 * @model
	 * @generated
	 */
	EList<DomainModel> getTargetDomainModel();

	/**
	 * Returns the value of the '<em><b>Correspondence Model</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Model</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_CorrespondenceModel()
	 * @model required="true"
	 * @generated
	 */
	EList<DomainModel> getCorrespondenceModel();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode
	 * @see #setMode(ApplicationMode)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_Mode()
	 * @model
	 * @generated
	 */
	ApplicationMode getMode();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode
	 * @see #getMode()
	 * @generated
	 */
	void setMode(ApplicationMode value);

	/**
	 * Returns the value of the '<em><b>Source Domain Object</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Domain Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Domain Object</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_SourceDomainObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EObject> getSourceDomainObject();

	/**
	 * Returns the value of the '<em><b>Correspondence Domain Object</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Domain Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Domain Object</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_CorrespondenceDomainObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EObject> getCorrespondenceDomainObject();

	/**
	 * Returns the value of the '<em><b>Target Domain Object</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Domain Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Domain Object</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_TargetDomainObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EObject> getTargetDomainObject();

	/**
	 * Returns the value of the '<em><b>Source Domain Node</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Domain Node</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Domain Node</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_SourceDomainNode()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<Node> getSourceDomainNode();

	/**
	 * Returns the value of the '<em><b>Correspondence Domain Node</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Domain Node</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Domain Node</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_CorrespondenceDomainNode()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<Node> getCorrespondenceDomainNode();

	/**
	 * Returns the value of the '<em><b>Target Domain Nodes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Domain Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Domain Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_TargetDomainNodes()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<Node> getTargetDomainNodes();

	/**
	 * Returns the value of the '<em><b>Source Domain Nodes By TGG Rule</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Domain Nodes By TGG Rule</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Domain Nodes By TGG Rule</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_SourceDomainNodesByTGGRule()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.TGGRuleToNodesMapEntry<de.upb.swt.qvt.tgg.TripleGraphGrammarRule, de.upb.swt.qvt.tgg.Node>" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EMap<TripleGraphGrammarRule, EList<Node>> getSourceDomainNodesByTGGRule();

	/**
	 * Returns the value of the '<em><b>Correspondence Domain Nodes By TGG Rule</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Domain Nodes By TGG Rule</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Domain Nodes By TGG Rule</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_CorrespondenceDomainNodesByTGGRule()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.TGGRuleToNodesMapEntry<de.upb.swt.qvt.tgg.TripleGraphGrammarRule, de.upb.swt.qvt.tgg.Node>" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EMap<TripleGraphGrammarRule, EList<Node>> getCorrespondenceDomainNodesByTGGRule();

	/**
	 * Returns the value of the '<em><b>Target Domain Node By TGG Rule</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Domain Node By TGG Rule</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Domain Node By TGG Rule</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_TargetDomainNodeByTGGRule()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.TGGRuleToNodesMapEntry<de.upb.swt.qvt.tgg.TripleGraphGrammarRule, de.upb.swt.qvt.tgg.Node>" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EMap<TripleGraphGrammarRule, EList<Node>> getTargetDomainNodeByTGGRule();

	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getApplicationScenario <em>Application Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' container reference.
	 * @see #setConfiguration(Configuration)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_Configuration()
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getApplicationScenario
	 * @model opposite="applicationScenario" transient="false"
	 * @generated
	 */
	Configuration getConfiguration();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration <em>Configuration</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' container reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Deactivate Front</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deactivate Front</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deactivate Front</em>' attribute.
	 * @see #setDeactivateFront(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getApplicationScenario_DeactivateFront()
	 * @model
	 * @generated
	 */
	boolean isDeactivateFront();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#isDeactivateFront <em>Deactivate Front</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deactivate Front</em>' attribute.
	 * @see #isDeactivateFront()
	 * @generated
	 */
	void setDeactivateFront(boolean value);

} // ApplicationScenario
