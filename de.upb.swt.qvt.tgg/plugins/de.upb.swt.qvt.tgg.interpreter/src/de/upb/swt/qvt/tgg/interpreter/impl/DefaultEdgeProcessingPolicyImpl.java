/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultEdgeProcessingPolicyImpl extends BaseEdgeProcessingPolicyImpl implements DefaultEdgeProcessingPolicy {
	@Override
	public void enforceEdge(Edge edge, EList<EObject> virtualModelEdge) {
		//return; //nothing to do, edge has already been created
		
		//BEGIN LinkConstraint processing

		//TODO: Solving link constraints may require complex computations,
		//  e.g., when there are several link constraints in several rules.
		// At the moment, we consider only one constraint and only at its time of enforcement.

		if (edge.getDirectSuccessor() != null) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			EList<EObject> successorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(edge.getDirectSuccessor());
			
			if (successorLink != null) { //if null, the other edge was not created yet
				assert edge.getTypeReference().equals(successorLink.get(2));
				assert virtualModelEdge.get(0).equals(successorLink.get(0)); // same source object
	
				EObject sourceObject = virtualModelEdge.get(0);
				EReference reference = edge.getTypeReference();
				EObject targetObject0 = virtualModelEdge.get(1);
				EObject targetObject1 = successorLink.get(1);
				
				@SuppressWarnings("unchecked")
				EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
				int index0 = list.indexOf(targetObject0);
				int index1 = list.indexOf(targetObject1);
				
				if (index0 + 1 != index1) {
					//reorder links
					list.move(index1, targetObject0);
				}
			}
		}
		if (edge.getDirectPredecessor() != null) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			EList<EObject> predecessorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(edge.getDirectPredecessor());
			
			if (predecessorLink != null) { //if null, the other edge was not created yet
				assert edge.getTypeReference().equals(predecessorLink.get(2));
				assert virtualModelEdge.get(0).equals(predecessorLink.get(0)); // same source object
	
				EObject sourceObject = virtualModelEdge.get(0);
				EReference reference = edge.getTypeReference();
				EObject targetObject0 = virtualModelEdge.get(1);
				EObject targetObject1 = predecessorLink.get(1);
				
				@SuppressWarnings("unchecked")
				EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
				int index0 = list.indexOf(targetObject0);
				int index1 = list.indexOf(targetObject1);
				
				if (index0 != index1 + 1) {
					//reorder links
					list.move(index1 + 1, targetObject0);
				}
			}
		}
		if (edge.getIndirectSuccessor().size() > 0) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			
			for (Edge indirectSuccessor: edge.getIndirectSuccessor()) {
				EList<EObject> successorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(indirectSuccessor);
				
				if (successorLink != null) { //if null, the other edge was not bound yet
					assert edge.getTypeReference().equals(successorLink.get(2));
					assert virtualModelEdge.get(0).equals(successorLink.get(0)); // same source object
		
					EObject sourceObject = virtualModelEdge.get(0);
					EReference reference = edge.getTypeReference();
					EObject targetObject0 = virtualModelEdge.get(1);
					EObject targetObject1 = successorLink.get(1);
					
					@SuppressWarnings("unchecked")
					EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
					int index0 = list.indexOf(targetObject0);
					int index1 = list.indexOf(targetObject1);
					
					if (index0 > index1) {
						//reorder links, but as it is an indirect successor, try not to destroy direct successors
						if (index1 > 0)
							list.move(index1 - 1, targetObject0);
						else
							list.move(index1, targetObject0);
					}
				}
			}
		}
		if (edge.getIndirectPredecessor().size() > 0) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			
			for (Edge indirectPredecessor: edge.getIndirectPredecessor()) {
				EList<EObject> predecessorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(indirectPredecessor);
				
				if (predecessorLink != null) { //if null, the other edge was not bound yet
					assert edge.getTypeReference().equals(predecessorLink.get(2));
					assert virtualModelEdge.get(0).equals(predecessorLink.get(0)); // same source object
		
					EObject sourceObject = virtualModelEdge.get(0);
					EReference reference = edge.getTypeReference();
					EObject targetObject0 = virtualModelEdge.get(1);
					EObject targetObject1 = predecessorLink.get(1);
					
					@SuppressWarnings("unchecked")
					EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
					int index0 = list.indexOf(targetObject0);
					int index1 = list.indexOf(targetObject1);
					
					if (index0 < index1) {
						//reorder links, but as it is an indirect successor, try not to destroy direct successors
						if (index1 + 2 > list.size())
							list.move(index1, targetObject0);
						else
							list.move(index1+1, targetObject0);
					}
				}
			}
		}
		
		//END LinkConstraint processing
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultEdgeProcessingPolicyImpl() {
		super();
	}

	@Override
	public boolean edgeMatches(Edge edge,
			EList<EObject> candidateVirtualModelEdge) {

		if (edge.isLeft() && edge.isRight()
				&& getTransformationProcessor().getGlobalEdgeBinding().get(candidateVirtualModelEdge) == null)
			return false;

		if (!edge.isLeft() && edge.isRight()
				&& getTransformationProcessor().getGlobalEdgeBinding().get(candidateVirtualModelEdge) != null)
			return false;
		//Check if produced node matching is constrained
		//FIXME: move this check to own NodeProcessingPolicy
		if (edge.isProduced() && !getGraphMatcher().isRestrictProducedGraphMatching()
				&& (getGraphMatcher().getLinksNotAvailableForProducedEdges().contains(candidateVirtualModelEdge)))
			return false;


		//BEGIN LinkConstraint processing

		//VME: (start, target, reference)
		if (edge.getDirectSuccessor() != null) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			EList<EObject> successorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(edge.getDirectSuccessor());
			
			if (successorLink != null) { //if the link is null, the other edge was not bound yet
				assert edge.getTypeReference().equals(successorLink.get(2));
				assert candidateVirtualModelEdge.get(0).equals(successorLink.get(0)); // same source object
	
				EObject sourceObject = candidateVirtualModelEdge.get(0);
				EReference reference = edge.getTypeReference();
				EObject targetObject0 = candidateVirtualModelEdge.get(1);
				EObject targetObject1 = successorLink.get(1);
				
				@SuppressWarnings("unchecked")
				EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
				int index0 = list.indexOf(targetObject0);
				int index1 = list.indexOf(targetObject1);
				
				if (index0 + 1 != index1)
					return false;
			}
		}
		
		if (edge.getDirectPredecessor() != null) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references

			EList<EObject> predecessorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(edge.getDirectPredecessor());
			
			if (predecessorLink != null) { //if the link is null, the other edge was not bound yet
				assert edge.getTypeReference().equals(predecessorLink.get(2));
				assert candidateVirtualModelEdge.get(0).equals(predecessorLink.get(0)); // same source object
	
				EObject sourceObject = candidateVirtualModelEdge.get(0);
				EReference reference = edge.getTypeReference();
				EObject targetObject0 = candidateVirtualModelEdge.get(1);
				EObject targetObject1 = predecessorLink.get(1);
				
				@SuppressWarnings("unchecked")
				EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
				int index0 = list.indexOf(targetObject0);
				int index1 = list.indexOf(targetObject1);
				
				if (index0 != index1 + 1)
					return false;
			}
		}

		if (edge.getIndirectSuccessor().size() > 0) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			
			for (Edge indirectSuccessor: edge.getIndirectSuccessor()) {
				EList<EObject> successorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(indirectSuccessor);
				
				if (successorLink != null) { //if the link is null, the other edge was not bound yet
					assert edge.getTypeReference().equals(successorLink.get(2));
					assert candidateVirtualModelEdge.get(0).equals(successorLink.get(0)); // same source object
		
					EObject sourceObject = candidateVirtualModelEdge.get(0);
					EReference reference = edge.getTypeReference();
					EObject targetObject0 = candidateVirtualModelEdge.get(1);
					EObject targetObject1 = successorLink.get(1);
					
					@SuppressWarnings("unchecked")
					EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
					int index0 = list.indexOf(targetObject0);
					int index1 = list.indexOf(targetObject1);
					
					if (index0 > index1)
						return false;
				}
			}
		}

		if (edge.getIndirectPredecessor().size() > 0) {
			assert edge.getTypeReference().isMany(); // link constraints only allowed for to-n references
			
			for (Edge indirectPredecessor: edge.getIndirectPredecessor()) {
				EList<EObject> predecessorLink = getGraphMatcher().getRuleBinding().getEdgeToVirtualModelEdgeMap().get(indirectPredecessor);
				
				if (predecessorLink != null) { //if the link is null, the other edge was not bound yet
					assert edge.getTypeReference().equals(predecessorLink.get(2));
					assert candidateVirtualModelEdge.get(0).equals(predecessorLink.get(0)); // same source object
		
					EObject sourceObject = candidateVirtualModelEdge.get(0);
					EReference reference = edge.getTypeReference();
					EObject targetObject0 = candidateVirtualModelEdge.get(1);
					EObject targetObject1 = predecessorLink.get(1);
					
					@SuppressWarnings("unchecked")
					EList<EObject> list = (EList<EObject>) sourceObject.eGet(reference);
					int index0 = list.indexOf(targetObject0);
					int index1 = list.indexOf(targetObject1);
					
					if (index0 < index1)
						return false;
				}
			}
		}
		
		//END LinkConstraint processing
		
		return true;
	}
		
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.DEFAULT_EDGE_PROCESSING_POLICY;
	}

} //DefaultEdgeProcessingPolicyImpl
