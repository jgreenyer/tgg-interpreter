/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.swt.qvt.qvtbase.QvtbasePackage;
import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Severity;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterconfigurationPackageImpl extends EPackageImpl implements InterpreterconfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass domainModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectToNodeMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass virtualModelEdgeToEdgeMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicationScenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleBindingContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typedModelToDomainModelMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggRuleToNodesMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edgeToVirtualModelEdgeMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass axiomBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass initialAxiomNodeBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum applicationModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum severityEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InterpreterconfigurationPackageImpl() {
		super(eNS_URI, InterpreterconfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InterpreterconfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InterpreterconfigurationPackage init() {
		if (isInited) return (InterpreterconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(InterpreterconfigurationPackage.eNS_URI);

		// Obtain or create and register package
		InterpreterconfigurationPackageImpl theInterpreterconfigurationPackage = (InterpreterconfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InterpreterconfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InterpreterconfigurationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TggPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theInterpreterconfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theInterpreterconfigurationPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theInterpreterconfigurationPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return InterpreterconfigurationValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theInterpreterconfigurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InterpreterconfigurationPackage.eNS_URI, theInterpreterconfigurationPackage);
		return theInterpreterconfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfiguration() {
		return configurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_TripleGraphGrammar() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfiguration_RecordRuleBindings() {
		return (EAttribute)configurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_ApplicationScenario() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_DomainModel() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_RuleBindingContainer() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_ActiveApplicationScenario() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_DomainRootObjects() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConfiguration_NoPackageUriRedirection() {
		return (EAttribute)configurationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_DomainModelByTypedModel() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDomainModel() {
		return domainModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDomainModel_TypedModel() {
		return (EReference)domainModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDomainModel_RootObject() {
		return (EReference)domainModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDomainModel_Name() {
		return (EAttribute)domainModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDomainModel_ResourceURI() {
		return (EAttribute)domainModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDomainModel_Resource() {
		return (EAttribute)domainModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDomainModel_DomainObject() {
		return (EReference)domainModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDomainModel_ShowDomainModelDiagnostic() {
		return (EAttribute)domainModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectToNodeMapEntry() {
		return eObjectToNodeMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToNodeMapEntry_Key() {
		return (EReference)eObjectToNodeMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToNodeMapEntry_Value() {
		return (EReference)eObjectToNodeMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVirtualModelEdgeToEdgeMapEntry() {
		return virtualModelEdgeToEdgeMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVirtualModelEdgeToEdgeMapEntry_Key() {
		return (EAttribute)virtualModelEdgeToEdgeMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVirtualModelEdgeToEdgeMapEntry_Value() {
		return (EReference)virtualModelEdgeToEdgeMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleBinding() {
		return ruleBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_NodeToEObjectMap() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_EdgeBinding() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_NodeBinding() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_RuleBindingContainer() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_EdgeToVirtualModelEdgeMap() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_GraphElementStack() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBinding_TggRule() {
		return (EReference)ruleBindingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicationScenario() {
		return applicationScenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_SourceDomainModel() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_TargetDomainModel() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_CorrespondenceModel() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApplicationScenario_Name() {
		return (EAttribute)applicationScenarioEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApplicationScenario_Mode() {
		return (EAttribute)applicationScenarioEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_SourceDomainObject() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_CorrespondenceDomainObject() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_TargetDomainObject() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_SourceDomainNode() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_CorrespondenceDomainNode() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_TargetDomainNodes() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_SourceDomainNodesByTGGRule() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_CorrespondenceDomainNodesByTGGRule() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_TargetDomainNodeByTGGRule() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationScenario_Configuration() {
		return (EReference)applicationScenarioEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApplicationScenario_DeactivateFront() {
		return (EAttribute)applicationScenarioEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleBindingContainer() {
		return ruleBindingContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBindingContainer_RuleBinding() {
		return (EReference)ruleBindingContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleBindingContainer_InitialAxiomBinding() {
		return (EReference)ruleBindingContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypedModelToDomainModelMapEntry() {
		return typedModelToDomainModelMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypedModelToDomainModelMapEntry_Key() {
		return (EReference)typedModelToDomainModelMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypedModelToDomainModelMapEntry_Value() {
		return (EReference)typedModelToDomainModelMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGRuleToNodesMapEntry() {
		return tggRuleToNodesMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleToNodesMapEntry_Key() {
		return (EReference)tggRuleToNodesMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleToNodesMapEntry_Value() {
		return (EReference)tggRuleToNodesMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeToEObjectMapEntry() {
		return nodeToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeToEObjectMapEntry_Key() {
		return (EReference)nodeToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeToEObjectMapEntry_Value() {
		return (EReference)nodeToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEdgeToVirtualModelEdgeMapEntry() {
		return edgeToVirtualModelEdgeMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEdgeToVirtualModelEdgeMapEntry_Key() {
		return (EReference)edgeToVirtualModelEdgeMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeToVirtualModelEdgeMapEntry_Value() {
		return (EAttribute)edgeToVirtualModelEdgeMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAxiomBinding() {
		return axiomBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAxiomBinding_InitialAxiomNodeBinding() {
		return (EReference)axiomBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInitialAxiomNodeBinding() {
		return initialAxiomNodeBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInitialAxiomNodeBinding_EObject() {
		return (EReference)initialAxiomNodeBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInitialAxiomNodeBinding_Node() {
		return (EReference)initialAxiomNodeBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getApplicationMode() {
		return applicationModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSeverity() {
		return severityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterconfigurationFactory getInterpreterconfigurationFactory() {
		return (InterpreterconfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		configurationEClass = createEClass(CONFIGURATION);
		createEReference(configurationEClass, CONFIGURATION__TRIPLE_GRAPH_GRAMMAR);
		createEAttribute(configurationEClass, CONFIGURATION__RECORD_RULE_BINDINGS);
		createEReference(configurationEClass, CONFIGURATION__APPLICATION_SCENARIO);
		createEReference(configurationEClass, CONFIGURATION__DOMAIN_MODEL);
		createEReference(configurationEClass, CONFIGURATION__RULE_BINDING_CONTAINER);
		createEReference(configurationEClass, CONFIGURATION__ACTIVE_APPLICATION_SCENARIO);
		createEReference(configurationEClass, CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL);
		createEReference(configurationEClass, CONFIGURATION__DOMAIN_ROOT_OBJECTS);
		createEAttribute(configurationEClass, CONFIGURATION__NO_PACKAGE_URI_REDIRECTION);

		domainModelEClass = createEClass(DOMAIN_MODEL);
		createEReference(domainModelEClass, DOMAIN_MODEL__TYPED_MODEL);
		createEReference(domainModelEClass, DOMAIN_MODEL__ROOT_OBJECT);
		createEAttribute(domainModelEClass, DOMAIN_MODEL__NAME);
		createEAttribute(domainModelEClass, DOMAIN_MODEL__RESOURCE_URI);
		createEAttribute(domainModelEClass, DOMAIN_MODEL__RESOURCE);
		createEReference(domainModelEClass, DOMAIN_MODEL__DOMAIN_OBJECT);
		createEAttribute(domainModelEClass, DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC);

		eObjectToNodeMapEntryEClass = createEClass(EOBJECT_TO_NODE_MAP_ENTRY);
		createEReference(eObjectToNodeMapEntryEClass, EOBJECT_TO_NODE_MAP_ENTRY__KEY);
		createEReference(eObjectToNodeMapEntryEClass, EOBJECT_TO_NODE_MAP_ENTRY__VALUE);

		virtualModelEdgeToEdgeMapEntryEClass = createEClass(VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY);
		createEReference(virtualModelEdgeToEdgeMapEntryEClass, VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__VALUE);
		createEAttribute(virtualModelEdgeToEdgeMapEntryEClass, VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__KEY);

		ruleBindingEClass = createEClass(RULE_BINDING);
		createEReference(ruleBindingEClass, RULE_BINDING__NODE_TO_EOBJECT_MAP);
		createEReference(ruleBindingEClass, RULE_BINDING__EDGE_BINDING);
		createEReference(ruleBindingEClass, RULE_BINDING__NODE_BINDING);
		createEReference(ruleBindingEClass, RULE_BINDING__RULE_BINDING_CONTAINER);
		createEReference(ruleBindingEClass, RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP);
		createEReference(ruleBindingEClass, RULE_BINDING__GRAPH_ELEMENT_STACK);
		createEReference(ruleBindingEClass, RULE_BINDING__TGG_RULE);

		applicationScenarioEClass = createEClass(APPLICATION_SCENARIO);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__CORRESPONDENCE_MODEL);
		createEAttribute(applicationScenarioEClass, APPLICATION_SCENARIO__NAME);
		createEAttribute(applicationScenarioEClass, APPLICATION_SCENARIO__MODE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__TARGET_DOMAIN_NODES);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE);
		createEReference(applicationScenarioEClass, APPLICATION_SCENARIO__CONFIGURATION);
		createEAttribute(applicationScenarioEClass, APPLICATION_SCENARIO__DEACTIVATE_FRONT);

		ruleBindingContainerEClass = createEClass(RULE_BINDING_CONTAINER);
		createEReference(ruleBindingContainerEClass, RULE_BINDING_CONTAINER__RULE_BINDING);
		createEReference(ruleBindingContainerEClass, RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING);

		typedModelToDomainModelMapEntryEClass = createEClass(TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY);
		createEReference(typedModelToDomainModelMapEntryEClass, TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__KEY);
		createEReference(typedModelToDomainModelMapEntryEClass, TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__VALUE);

		tggRuleToNodesMapEntryEClass = createEClass(TGG_RULE_TO_NODES_MAP_ENTRY);
		createEReference(tggRuleToNodesMapEntryEClass, TGG_RULE_TO_NODES_MAP_ENTRY__KEY);
		createEReference(tggRuleToNodesMapEntryEClass, TGG_RULE_TO_NODES_MAP_ENTRY__VALUE);

		nodeToEObjectMapEntryEClass = createEClass(NODE_TO_EOBJECT_MAP_ENTRY);
		createEReference(nodeToEObjectMapEntryEClass, NODE_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(nodeToEObjectMapEntryEClass, NODE_TO_EOBJECT_MAP_ENTRY__VALUE);

		edgeToVirtualModelEdgeMapEntryEClass = createEClass(EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY);
		createEReference(edgeToVirtualModelEdgeMapEntryEClass, EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__KEY);
		createEAttribute(edgeToVirtualModelEdgeMapEntryEClass, EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__VALUE);

		axiomBindingEClass = createEClass(AXIOM_BINDING);
		createEReference(axiomBindingEClass, AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING);

		initialAxiomNodeBindingEClass = createEClass(INITIAL_AXIOM_NODE_BINDING);
		createEReference(initialAxiomNodeBindingEClass, INITIAL_AXIOM_NODE_BINDING__EOBJECT);
		createEReference(initialAxiomNodeBindingEClass, INITIAL_AXIOM_NODE_BINDING__NODE);

		// Create enums
		applicationModeEEnum = createEEnum(APPLICATION_MODE);
		severityEEnum = createEEnum(SEVERITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TggPackage theTggPackage = (TggPackage)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		QvtbasePackage theQvtbasePackage = (QvtbasePackage)EPackage.Registry.INSTANCE.getEPackage(QvtbasePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		axiomBindingEClass.getESuperTypes().add(this.getRuleBinding());

		// Initialize classes and features; add operations and parameters
		initEClass(configurationEClass, Configuration.class, "Configuration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConfiguration_TripleGraphGrammar(), theTggPackage.getTripleGraphGrammar(), null, "tripleGraphGrammar", null, 1, 1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfiguration_RecordRuleBindings(), theEcorePackage.getEBoolean(), "recordRuleBindings", null, 0, 1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_ApplicationScenario(), this.getApplicationScenario(), this.getApplicationScenario_Configuration(), "applicationScenario", null, 1, -1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_DomainModel(), this.getDomainModel(), null, "domainModel", null, 0, -1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_RuleBindingContainer(), this.getRuleBindingContainer(), null, "ruleBindingContainer", null, 0, 1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_ActiveApplicationScenario(), this.getApplicationScenario(), null, "activeApplicationScenario", null, 1, 1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_DomainModelByTypedModel(), this.getTypedModelToDomainModelMapEntry(), null, "domainModelByTypedModel", null, 0, -1, Configuration.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getConfiguration_DomainRootObjects(), theEcorePackage.getEObject(), null, "domainRootObjects", null, 0, -1, Configuration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfiguration_NoPackageUriRedirection(), ecorePackage.getEBoolean(), "noPackageUriRedirection", null, 0, 1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(domainModelEClass, DomainModel.class, "DomainModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDomainModel_TypedModel(), theQvtbasePackage.getTypedModel(), null, "typedModel", null, 1, 1, DomainModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDomainModel_RootObject(), theEcorePackage.getEObject(), null, "rootObject", null, 0, -1, DomainModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDomainModel_Name(), theEcorePackage.getEString(), "name", null, 0, 1, DomainModel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDomainModel_ResourceURI(), theEcorePackage.getEString(), "resourceURI", null, 0, -1, DomainModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDomainModel_Resource(), theEcorePackage.getEResource(), "resource", null, 0, -1, DomainModel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDomainModel_DomainObject(), theEcorePackage.getEObject(), null, "domainObject", null, 0, -1, DomainModel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDomainModel_ShowDomainModelDiagnostic(), this.getSeverity(), "showDomainModelDiagnostic", null, 0, 1, DomainModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eObjectToNodeMapEntryEClass, Map.Entry.class, "EObjectToNodeMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectToNodeMapEntry_Key(), theEcorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEObjectToNodeMapEntry_Value(), theTggPackage.getNode(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(virtualModelEdgeToEdgeMapEntryEClass, Map.Entry.class, "VirtualModelEdgeToEdgeMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVirtualModelEdgeToEdgeMapEntry_Value(), theTggPackage.getEdge(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEEList());
		EGenericType g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getVirtualModelEdgeToEdgeMapEntry_Key(), g1, "key", null, 1, 1, Map.Entry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleBindingEClass, RuleBinding.class, "RuleBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleBinding_NodeToEObjectMap(), this.getNodeToEObjectMapEntry(), null, "nodeToEObjectMap", null, 0, -1, RuleBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_EdgeBinding(), this.getVirtualModelEdgeToEdgeMapEntry(), null, "edgeBinding", null, 0, -1, RuleBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_NodeBinding(), this.getEObjectToNodeMapEntry(), null, "nodeBinding", null, 0, -1, RuleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_RuleBindingContainer(), this.getRuleBindingContainer(), this.getRuleBindingContainer_RuleBinding(), "ruleBindingContainer", null, 0, 1, RuleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_EdgeToVirtualModelEdgeMap(), this.getEdgeToVirtualModelEdgeMapEntry(), null, "edgeToVirtualModelEdgeMap", null, 0, -1, RuleBinding.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_GraphElementStack(), theTggPackage.getGraphElement(), null, "graphElementStack", null, 0, -1, RuleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBinding_TggRule(), theTggPackage.getTripleGraphGrammarRule(), null, "tggRule", null, 0, 1, RuleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(ruleBindingEClass, null, "removeElementBindingsFromStack", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getGraphElement(), "element", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ruleBindingEClass, null, "pushEdgeBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getEdge(), "edge", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "virtualModelEdge", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ruleBindingEClass, null, "pushNodeBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "modelObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(applicationScenarioEClass, ApplicationScenario.class, "ApplicationScenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApplicationScenario_SourceDomainModel(), this.getDomainModel(), null, "sourceDomainModel", null, 0, -1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_TargetDomainModel(), this.getDomainModel(), null, "targetDomainModel", null, 0, -1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_CorrespondenceModel(), this.getDomainModel(), null, "correspondenceModel", null, 1, -1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getApplicationScenario_Name(), theEcorePackage.getEString(), "name", null, 0, 1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getApplicationScenario_Mode(), this.getApplicationMode(), "mode", null, 0, 1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_SourceDomainObject(), theEcorePackage.getEObject(), null, "sourceDomainObject", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_CorrespondenceDomainObject(), theEcorePackage.getEObject(), null, "correspondenceDomainObject", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_TargetDomainObject(), theEcorePackage.getEObject(), null, "targetDomainObject", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_SourceDomainNode(), theTggPackage.getNode(), null, "sourceDomainNode", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_CorrespondenceDomainNode(), theTggPackage.getNode(), null, "correspondenceDomainNode", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_TargetDomainNodes(), theTggPackage.getNode(), null, "targetDomainNodes", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_SourceDomainNodesByTGGRule(), this.getTGGRuleToNodesMapEntry(), null, "sourceDomainNodesByTGGRule", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_CorrespondenceDomainNodesByTGGRule(), this.getTGGRuleToNodesMapEntry(), null, "correspondenceDomainNodesByTGGRule", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_TargetDomainNodeByTGGRule(), this.getTGGRuleToNodesMapEntry(), null, "targetDomainNodeByTGGRule", null, 0, -1, ApplicationScenario.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getApplicationScenario_Configuration(), this.getConfiguration(), this.getConfiguration_ApplicationScenario(), "configuration", null, 0, 1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getApplicationScenario_DeactivateFront(), theEcorePackage.getEBoolean(), "deactivateFront", null, 0, 1, ApplicationScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleBindingContainerEClass, RuleBindingContainer.class, "RuleBindingContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleBindingContainer_RuleBinding(), this.getRuleBinding(), this.getRuleBinding_RuleBindingContainer(), "ruleBinding", null, 0, -1, RuleBindingContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleBindingContainer_InitialAxiomBinding(), this.getAxiomBinding(), null, "initialAxiomBinding", null, 0, 1, RuleBindingContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typedModelToDomainModelMapEntryEClass, Map.Entry.class, "TypedModelToDomainModelMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypedModelToDomainModelMapEntry_Key(), theQvtbasePackage.getTypedModel(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTypedModelToDomainModelMapEntry_Value(), this.getDomainModel(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tggRuleToNodesMapEntryEClass, Map.Entry.class, "TGGRuleToNodesMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGRuleToNodesMapEntry_Key(), theTggPackage.getTripleGraphGrammarRule(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRuleToNodesMapEntry_Value(), theTggPackage.getNode(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeToEObjectMapEntryEClass, Map.Entry.class, "NodeToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNodeToEObjectMapEntry_Key(), theTggPackage.getNode(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNodeToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(edgeToVirtualModelEdgeMapEntryEClass, Map.Entry.class, "EdgeToVirtualModelEdgeMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEdgeToVirtualModelEdgeMapEntry_Key(), theTggPackage.getEdge(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getEdgeToVirtualModelEdgeMapEntry_Value(), g1, "value", null, 1, 1, Map.Entry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(axiomBindingEClass, AxiomBinding.class, "AxiomBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAxiomBinding_InitialAxiomNodeBinding(), this.getInitialAxiomNodeBinding(), null, "initialAxiomNodeBinding", null, 0, -1, AxiomBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(initialAxiomNodeBindingEClass, InitialAxiomNodeBinding.class, "InitialAxiomNodeBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInitialAxiomNodeBinding_EObject(), theEcorePackage.getEObject(), null, "eObject", null, 0, 1, InitialAxiomNodeBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInitialAxiomNodeBinding_Node(), theTggPackage.getNode(), null, "node", null, 0, 1, InitialAxiomNodeBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(applicationModeEEnum, ApplicationMode.class, "ApplicationMode");
		addEEnumLiteral(applicationModeEEnum, ApplicationMode.TRANSFORM);
		addEEnumLiteral(applicationModeEEnum, ApplicationMode.UPDATE);
		addEEnumLiteral(applicationModeEEnum, ApplicationMode.SYNCHRONIZE);

		initEEnum(severityEEnum, Severity.class, "Severity");
		addEEnumLiteral(severityEEnum, Severity.OK);
		addEEnumLiteral(severityEEnum, Severity.INFO);
		addEEnumLiteral(severityEEnum, Severity.WARNING);
		addEEnumLiteral(severityEEnum, Severity.ERROR);
		addEEnumLiteral(severityEEnum, Severity.NONE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL";				
		addAnnotation
		  (configurationEClass, 
		   source, 
		   new String[] {
			 "numberOfDomainModelsMatchesNumberOfTypedModels", "self.domainModel->size() = self.tripleGraphGrammar.modelParameter->size()"
		   });							
		addAnnotation
		  (applicationScenarioEClass, 
		   source, 
		   new String[] {
			 "eachDomainModelEitherSourceCorrespondenceOrTarget", "self.sourceDomainModel->select(dm|self.correspondenceModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\nand self.correspondenceModel->select(dm|self.sourceDomainModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\nand self.targetDomainModel->select(dm|self.sourceDomainModel->includes(dm) or self.correspondenceModel->includes(dm))->isEmpty()",
			 "eitherResourceURIOrRootObjectsSetForDomainModels", "self.sourceDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\nself.targetDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\nself.correspondenceModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty()"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL"
		   });		
		addAnnotation
		  (configurationEClass, 
		   source, 
		   new String[] {
			 "constraints", "numberOfDomainModelsMatchesNumberOfTypedModels"
		   });							
		addAnnotation
		  (applicationScenarioEClass, 
		   source, 
		   new String[] {
			 "constraints", "eachDomainModelEitherSourceCorrespondenceOrTarget\r\neitherResourceURIOrRootObjectsSetForDomainModels"
		   });	
	}

} //InterpreterconfigurationPackageImpl
