package de.upb.swt.qvt.tgg.interpreter.util;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;

public class InterpreterStatus extends MultiStatus {

	protected List<EObject> unboundSourceObjects;
	protected Configuration configuration;
	protected long transformationTime;
	protected Map<TripleGraphGrammarRule, Integer> rulesApplied;
	protected Map<TripleGraphGrammarRule, Integer> rulesRevoked;
	protected Map<TripleGraphGrammarRule, Integer> rulesStillValid;
	private EList<EObject> changedObjects;
	private int unsuccessfulRuleApplicationAttempts;
	private int edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
	
	
	public int getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() {
		return edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
	}

	public void setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(
			int edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications) {
		this.edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications = edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
	}

	public int getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() {
		return edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;
	}

	public void setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(
			int edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications) {
		this.edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications = edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;
	}

	private int edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;

	public int getUnsuccessfulRuleApplicationAttempts() {
		return unsuccessfulRuleApplicationAttempts;
	}

	public void setUnsuccessfulRuleApplicationAttempts(
			int unsuccessfulRuleApplicationAttempts) {
		this.unsuccessfulRuleApplicationAttempts = unsuccessfulRuleApplicationAttempts;
	}

	public long getTransformationTime() {
		return transformationTime;
	}

	public void setTransformationTime(long transformationTime) {
		this.transformationTime = transformationTime;
	}

	public InterpreterStatus(String pluginId, int severity, String message, Configuration configuration, Throwable exception) {
		super(pluginId, severity, message, exception);
		this.configuration = configuration;
	}

	public InterpreterStatus(String pluginId, int severity, IStatus[] children, String message, Configuration configuration, Throwable exception) {
		super(pluginId, severity, children, message, exception);
		this.configuration = configuration;
	}
	
	
	@Override
	public int getSeverity() {
		// TODO Auto-generated method stub
		return super.getSeverity();
	}
	@Override
	public void setSeverity(int severity) {
		// TODO Auto-generated method stub
		super.setSeverity(severity);
	}
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}
	@Override
	public void setMessage(String message) {
		// TODO Auto-generated method stub
		super.setMessage(message);
	}	

	public List<EObject> getUnboundSourceObjects() {
		return unboundSourceObjects;
	}
	
	public void setUnboundSourceObjects(List<EObject> unboundSourceObjects) {
		this.unboundSourceObjects = unboundSourceObjects;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public Map<TripleGraphGrammarRule, Integer> getRulesApplied() {
		return rulesApplied;
	}

	public void setRulesApplied(Map<TripleGraphGrammarRule, Integer> newRulesApplied) {
		rulesApplied = newRulesApplied;
	}

	public Map<TripleGraphGrammarRule, Integer> getRulesRevoked() {
		return rulesRevoked;
	}
	
	public void setRulesRevoked(Map<TripleGraphGrammarRule, Integer> newRulesRevoked) {
		rulesRevoked = newRulesRevoked;
	}

	public Map<TripleGraphGrammarRule, Integer> getRulesStillValid() {
		return rulesStillValid;
	}

	public void setRulesStillValid(Map<TripleGraphGrammarRule, Integer> newRulesStillValid) {
		rulesStillValid = newRulesStillValid;
	}

	public void setChangedObjects(EList<EObject> newChangedObjects) {
		changedObjects = newChangedObjects;
	}

	public EList<EObject> getChangedObjects() {
		return changedObjects;
	}
}
