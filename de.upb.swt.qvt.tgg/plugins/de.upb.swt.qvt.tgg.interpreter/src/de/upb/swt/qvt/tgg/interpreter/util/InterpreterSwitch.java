/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.util;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage
 * @generated
 */
public class InterpreterSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static InterpreterPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterSwitch() {
		if (modelPackage == null) {
			modelPackage = InterpreterPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case InterpreterPackage.INTERPRETER: {
				Interpreter interpreter = (Interpreter)theEObject;
				T result = caseInterpreter(interpreter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.FRONT_MANAGER: {
				FrontManager frontManager = (FrontManager)theEObject;
				T result = caseFrontManager(frontManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.RULE_PROCESSOR: {
				RuleProcessor ruleProcessor = (RuleProcessor)theEObject;
				T result = caseRuleProcessor(ruleProcessor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.TRANSFORMATION_PROCESSOR: {
				TransformationProcessor transformationProcessor = (TransformationProcessor)theEObject;
				T result = caseTransformationProcessor(transformationProcessor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.INODE_MATCHING_POLICY: {
				INodeMatchingPolicy iNodeMatchingPolicy = (INodeMatchingPolicy)theEObject;
				T result = caseINodeMatchingPolicy(iNodeMatchingPolicy);
				if (result == null) result = caseIMatchingPolicy(iNodeMatchingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.IEDGE_MATCHING_POLICY: {
				IEdgeMatchingPolicy iEdgeMatchingPolicy = (IEdgeMatchingPolicy)theEObject;
				T result = caseIEdgeMatchingPolicy(iEdgeMatchingPolicy);
				if (result == null) result = caseIMatchingPolicy(iEdgeMatchingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR: {
				FrontTransformationProcessor frontTransformationProcessor = (FrontTransformationProcessor)theEObject;
				T result = caseFrontTransformationProcessor(frontTransformationProcessor);
				if (result == null) result = caseTransformationProcessor(frontTransformationProcessor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.GRAPH_MATCHER: {
				GraphMatcher graphMatcher = (GraphMatcher)theEObject;
				T result = caseGraphMatcher(graphMatcher);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY: {
				BaseNodeProcessingPolicy baseNodeProcessingPolicy = (BaseNodeProcessingPolicy)theEObject;
				T result = caseBaseNodeProcessingPolicy(baseNodeProcessingPolicy);
				if (result == null) result = caseINodeMatchingPolicy(baseNodeProcessingPolicy);
				if (result == null) result = caseINodeEnforcementPolicy(baseNodeProcessingPolicy);
				if (result == null) result = caseIMatchingPolicy(baseNodeProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.BASE_EDGE_PROCESSING_POLICY: {
				BaseEdgeProcessingPolicy baseEdgeProcessingPolicy = (BaseEdgeProcessingPolicy)theEObject;
				T result = caseBaseEdgeProcessingPolicy(baseEdgeProcessingPolicy);
				if (result == null) result = caseIEdgeEnforcementPolicy(baseEdgeProcessingPolicy);
				if (result == null) result = caseIEdgeMatchingPolicy(baseEdgeProcessingPolicy);
				if (result == null) result = caseIMatchingPolicy(baseEdgeProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.ICONSTRAINT_CHECKING_POLICY: {
				IConstraintCheckingPolicy iConstraintCheckingPolicy = (IConstraintCheckingPolicy)theEObject;
				T result = caseIConstraintCheckingPolicy(iConstraintCheckingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY: {
				BaseConstraintProcessingPolicy baseConstraintProcessingPolicy = (BaseConstraintProcessingPolicy)theEObject;
				T result = caseBaseConstraintProcessingPolicy(baseConstraintProcessingPolicy);
				if (result == null) result = caseIConstraintEnforcementPolicy(baseConstraintProcessingPolicy);
				if (result == null) result = caseIConstraintCheckingPolicy(baseConstraintProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.INODE_ENFORCEMENT_POLICY: {
				INodeEnforcementPolicy iNodeEnforcementPolicy = (INodeEnforcementPolicy)theEObject;
				T result = caseINodeEnforcementPolicy(iNodeEnforcementPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.IEDGE_ENFORCEMENT_POLICY: {
				IEdgeEnforcementPolicy iEdgeEnforcementPolicy = (IEdgeEnforcementPolicy)theEObject;
				T result = caseIEdgeEnforcementPolicy(iEdgeEnforcementPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.ICONSTRAINT_ENFORCEMENT_POLICY: {
				IConstraintEnforcementPolicy iConstraintEnforcementPolicy = (IConstraintEnforcementPolicy)theEObject;
				T result = caseIConstraintEnforcementPolicy(iConstraintEnforcementPolicy);
				if (result == null) result = caseIConstraintCheckingPolicy(iConstraintEnforcementPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.DEFAULT_NODE_PROCESSING_POLICY: {
				DefaultNodeProcessingPolicy defaultNodeProcessingPolicy = (DefaultNodeProcessingPolicy)theEObject;
				T result = caseDefaultNodeProcessingPolicy(defaultNodeProcessingPolicy);
				if (result == null) result = caseBaseNodeProcessingPolicy(defaultNodeProcessingPolicy);
				if (result == null) result = caseINodeMatchingPolicy(defaultNodeProcessingPolicy);
				if (result == null) result = caseINodeEnforcementPolicy(defaultNodeProcessingPolicy);
				if (result == null) result = caseIMatchingPolicy(defaultNodeProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.DEFAULT_EDGE_PROCESSING_POLICY: {
				DefaultEdgeProcessingPolicy defaultEdgeProcessingPolicy = (DefaultEdgeProcessingPolicy)theEObject;
				T result = caseDefaultEdgeProcessingPolicy(defaultEdgeProcessingPolicy);
				if (result == null) result = caseBaseEdgeProcessingPolicy(defaultEdgeProcessingPolicy);
				if (result == null) result = caseIEdgeEnforcementPolicy(defaultEdgeProcessingPolicy);
				if (result == null) result = caseIEdgeMatchingPolicy(defaultEdgeProcessingPolicy);
				if (result == null) result = caseIMatchingPolicy(defaultEdgeProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY: {
				DefaultOCLConstraintProcessingPolicy defaultOCLConstraintProcessingPolicy = (DefaultOCLConstraintProcessingPolicy)theEObject;
				T result = caseDefaultOCLConstraintProcessingPolicy(defaultOCLConstraintProcessingPolicy);
				if (result == null) result = caseBaseConstraintProcessingPolicy(defaultOCLConstraintProcessingPolicy);
				if (result == null) result = caseIConstraintEnforcementPolicy(defaultOCLConstraintProcessingPolicy);
				if (result == null) result = caseIConstraintCheckingPolicy(defaultOCLConstraintProcessingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.NODE_TO_CONSTRAINT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Node, EList<Constraint>> nodeToConstraintMapEntry = (Map.Entry<Node, EList<Constraint>>)theEObject;
				T result = caseNodeToConstraintMapEntry(nodeToConstraintMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.IMATCHING_POLICY: {
				IMatchingPolicy iMatchingPolicy = (IMatchingPolicy)theEObject;
				T result = caseIMatchingPolicy(iMatchingPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER: {
				TransformationProcessorHelper transformationProcessorHelper = (TransformationProcessorHelper)theEObject;
				T result = caseTransformationProcessorHelper(transformationProcessorHelper);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.DEBUG_SUPPORT: {
				DebugSupport debugSupport = (DebugSupport)theEObject;
				T result = caseDebugSupport(debugSupport);
				if (result == null) result = caseIDebugEventListener(debugSupport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterPackage.IDEBUG_EVENT_LISTENER: {
				IDebugEventListener iDebugEventListener = (IDebugEventListener)theEObject;
				T result = caseIDebugEventListener(iDebugEventListener);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interpreter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interpreter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterpreter(Interpreter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Front Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Front Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFrontManager(FrontManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Processor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Processor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleProcessor(RuleProcessor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transformation Processor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transformation Processor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransformationProcessor(TransformationProcessor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INode Matching Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INode Matching Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINodeMatchingPolicy(INodeMatchingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEdge Matching Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEdge Matching Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEdgeMatchingPolicy(IEdgeMatchingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Front Transformation Processor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Front Transformation Processor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFrontTransformationProcessor(FrontTransformationProcessor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph Matcher</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph Matcher</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphMatcher(GraphMatcher object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Node Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Node Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseNodeProcessingPolicy(BaseNodeProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Edge Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Edge Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseEdgeProcessingPolicy(BaseEdgeProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConstraint Checking Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConstraint Checking Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConstraintCheckingPolicy(IConstraintCheckingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Constraint Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Constraint Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseConstraintProcessingPolicy(BaseConstraintProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INode Enforcement Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INode Enforcement Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINodeEnforcementPolicy(INodeEnforcementPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEdge Enforcement Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEdge Enforcement Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEdgeEnforcementPolicy(IEdgeEnforcementPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConstraint Enforcement Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConstraint Enforcement Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConstraintEnforcementPolicy(IConstraintEnforcementPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Node Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Node Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultNodeProcessingPolicy(DefaultNodeProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Edge Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Edge Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultEdgeProcessingPolicy(DefaultEdgeProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default OCL Constraint Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default OCL Constraint Processing Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultOCLConstraintProcessingPolicy(DefaultOCLConstraintProcessingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node To Constraint Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node To Constraint Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeToConstraintMapEntry(Map.Entry<Node, EList<Constraint>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IMatching Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IMatching Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIMatchingPolicy(IMatchingPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transformation Processor Helper</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transformation Processor Helper</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransformationProcessorHelper(TransformationProcessorHelper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Debug Support</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Debug Support</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDebugSupport(DebugSupport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDebug Event Listener</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDebug Event Listener</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDebugEventListener(IDebugEventListener object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //InterpreterSwitch
