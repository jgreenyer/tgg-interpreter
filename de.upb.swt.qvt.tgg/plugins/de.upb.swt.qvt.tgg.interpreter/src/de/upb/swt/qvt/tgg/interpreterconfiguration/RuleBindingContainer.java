/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Binding Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getRuleBinding <em>Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getInitialAxiomBinding <em>Initial Axiom Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBindingContainer()
 * @model
 * @generated
 */
public interface RuleBindingContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Rule Binding</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer <em>Rule Binding Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Binding</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBindingContainer_RuleBinding()
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer
	 * @model opposite="ruleBindingContainer" containment="true"
	 * @generated
	 */
	EList<RuleBinding> getRuleBinding();

	/**
	 * Returns the value of the '<em><b>Initial Axiom Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Axiom Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Axiom Binding</em>' containment reference.
	 * @see #setInitialAxiomBinding(AxiomBinding)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBindingContainer_InitialAxiomBinding()
	 * @model containment="true"
	 * @generated
	 */
	AxiomBinding getInitialAxiomBinding();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getInitialAxiomBinding <em>Initial Axiom Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Axiom Binding</em>' containment reference.
	 * @see #getInitialAxiomBinding()
	 * @generated
	 */
	void setInitialAxiomBinding(AxiomBinding value);

} // RuleBindingContainer
