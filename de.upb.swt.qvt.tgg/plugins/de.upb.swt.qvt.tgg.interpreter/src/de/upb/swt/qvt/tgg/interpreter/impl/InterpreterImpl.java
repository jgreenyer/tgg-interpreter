/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreEnvironment;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interpreter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl#getOclEnvironment <em>Ocl Environment</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl#getDebugSupport <em>Debug Support</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl#getOcl <em>Ocl</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InterpreterImpl extends EObjectImpl implements Interpreter {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(InterpreterImpl.class.getName());
	
	/**
	 * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Configuration configuration;

	/**
	 * The cached value of the '{@link #getTransformationProcessor() <em>Transformation Processor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationProcessor()
	 * @generated
	 * @ordered
	 */
	protected TransformationProcessor transformationProcessor;

	/**
	 * The default value of the '{@link #getOclEnvironment() <em>Ocl Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclEnvironment()
	 * @generated
	 * @ordered
	 */
	protected static final EcoreEnvironment OCL_ENVIRONMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOclEnvironment() <em>Ocl Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclEnvironment()
	 * @generated
	 * @ordered
	 */
	protected EcoreEnvironment oclEnvironment = OCL_ENVIRONMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDebugSupport() <em>Debug Support</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDebugSupport()
	 * @generated
	 * @ordered
	 */
	protected DebugSupport debugSupport;

	/**
	 * The default value of the '{@link #getOcl() <em>Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOcl()
	 * @generated
	 * @ordered
	 */
	protected static final OCL OCL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOcl() <em>Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOcl()
	 * @generated
	 * @ordered
	 */
	protected OCL ocl = OCL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected InterpreterImpl() {
		super();		
		setTransformationProcessor(InterpreterFactory.eINSTANCE.createFrontTransformationProcessor());		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.INTERPRETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getConfiguration() {
		if (configuration != null && configuration.eIsProxy()) {
			InternalEObject oldConfiguration = (InternalEObject)configuration;
			configuration = (Configuration)eResolveProxy(oldConfiguration);
			if (configuration != oldConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.INTERPRETER__CONFIGURATION, oldConfiguration, configuration));
			}
		}
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration basicGetConfiguration() {
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(Configuration newConfiguration) {
		Configuration oldConfiguration = configuration;
		configuration = newConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__CONFIGURATION, oldConfiguration, configuration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessor getTransformationProcessor() {
		return transformationProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransformationProcessor(TransformationProcessor newTransformationProcessor, NotificationChain msgs) {
		TransformationProcessor oldTransformationProcessor = transformationProcessor;
		transformationProcessor = newTransformationProcessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR, oldTransformationProcessor, newTransformationProcessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationProcessor(TransformationProcessor newTransformationProcessor) {
		if (newTransformationProcessor != transformationProcessor) {
			NotificationChain msgs = null;
			if (transformationProcessor != null)
				msgs = ((InternalEObject)transformationProcessor).eInverseRemove(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER, TransformationProcessor.class, msgs);
			if (newTransformationProcessor != null)
				msgs = ((InternalEObject)newTransformationProcessor).eInverseAdd(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER, TransformationProcessor.class, msgs);
			msgs = basicSetTransformationProcessor(newTransformationProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR, newTransformationProcessor, newTransformationProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoreEnvironment getOclEnvironment() {
		return oclEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOclEnvironment(EcoreEnvironment newOclEnvironment) {
		EcoreEnvironment oldOclEnvironment = oclEnvironment;
		oclEnvironment = newOclEnvironment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__OCL_ENVIRONMENT, oldOclEnvironment, oclEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DebugSupport getDebugSupport() {
		return debugSupport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDebugSupport(DebugSupport newDebugSupport, NotificationChain msgs) {
		DebugSupport oldDebugSupport = debugSupport;
		debugSupport = newDebugSupport;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__DEBUG_SUPPORT, oldDebugSupport, newDebugSupport);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDebugSupport(DebugSupport newDebugSupport) {
		if (newDebugSupport != debugSupport) {
			NotificationChain msgs = null;
			if (debugSupport != null)
				msgs = ((InternalEObject)debugSupport).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.INTERPRETER__DEBUG_SUPPORT, null, msgs);
			if (newDebugSupport != null)
				msgs = ((InternalEObject)newDebugSupport).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.INTERPRETER__DEBUG_SUPPORT, null, msgs);
			msgs = basicSetDebugSupport(newDebugSupport, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__DEBUG_SUPPORT, newDebugSupport, newDebugSupport));
		}
		//InterpreterPlugin.getLogManager().enableDebug();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCL getOcl() {
		return ocl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOcl(OCL newOcl) {
		OCL oldOcl = ocl;
		ocl = newOcl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__OCL, oldOcl, ocl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void initializeConfiguration() throws IllegalArgumentException{
		
		Configuration configuration = getConfiguration();

		Diagnostic configurationDiagnostic = Diagnostician.INSTANCE.validate(configuration);
		if (configurationDiagnostic.getSeverity() != Diagnostic.OK){
			//logger.error("The Configuration file is not valid: " + configurationDiagnostic);
			throw new IllegalArgumentException("The Configuration is not valid: " + configurationDiagnostic);
		}
		
		Diagnostic tggDiagnostic = Diagnostician.INSTANCE.validate(configuration.getTripleGraphGrammar());
		if (tggDiagnostic.getSeverity() == Diagnostic.ERROR){
			//logger.error("The Configuration file is not valid: " + configurationDiagnostic);
			throw new IllegalArgumentException("The Triple Graph Grammar is not valid: " + tggDiagnostic);
		}

		
		Resource configurationResource = configuration.eResource();
		if (configurationResource != null) {
			ResourceSet configurationResourceSet = configurationResource.getResourceSet();

			for (DomainModel domainModel : configuration.getDomainModel()) {

				// derive resources from given root objects
				if (!domainModel.getRootObject().isEmpty()) {
					for (EObject rootEObject : domainModel.getRootObject()) {
						domainModel.getResource().add(rootEObject.eResource());
					}
				}

				for (String resourceURIString : domainModel.getResourceURI()) {
					URI domainModelResourceURI;
					Resource domainModelResource;

					if (!resourceURIString.startsWith("/")){ //it is an absolute workspace path
						URI relativeDomainModelResourceURI = URI.createURI(resourceURIString);
						domainModelResourceURI = relativeDomainModelResourceURI.resolve(configurationResource.getURI());
					}else{ //it is path relative to the configuration file
						domainModelResourceURI = URI.createPlatformResourceURI(resourceURIString, true);
					}

					logger.debug("Path of domain model " + "\"" + domainModel.getTypedModel().getName()+ "\": " + domainModelResourceURI);

					try {
						domainModelResource = configurationResourceSet.getResource(domainModelResourceURI, true);
						// set resource' root objects as domain root objects.
						domainModel.getRootObject().addAll(domainModelResource.getContents());
						domainModel.getResource().add(domainModelResource);
						logger.debug("root objects in the given resource: " + domainModelResource.getContents());
						if (configuration.getActiveApplicationScenario().getSourceDomainModel().contains(domainModel)
								&& domainModelResource.getContents().isEmpty()){
							throw new IllegalArgumentException("The Configuration file is not valid: " + 
									"The resource " + domainModelResourceURI + " does not contain any model elements");
						}
					} catch (WrappedException e) {
						// a source domain resource must exist. If it does not, throw an exception
						if (configuration.getActiveApplicationScenario().getSourceDomainModel().contains(domainModel)){
							String errorMsg = "Could not find the resource of the source domain model: " + domainModelResourceURI
									+ " please check the path \"" + domainModel.getResourceURI() + "\" in your domain model configuration."
									+ " The path should either be an absolute path in the workspace of the form '/<project>/<path in project>/<file name>' " +
									" or a path relative to the configuration file of the form (then it must not start with a '/').";
							throw new IllegalArgumentException("The Configuration file is not valid: " + errorMsg);
						}else{
							domainModelResource = configurationResourceSet.createResource(domainModelResourceURI);
							domainModel.getResource().add(domainModelResource);
						}
					}
					catch (Exception e) {
						String errorMsg = "An error occurred while loading resource \"" + domainModelResourceURI + ". Check whether the resource is a valid XMI file.";
						throw new IllegalArgumentException("The Configuration file is not valid: " + errorMsg);
					}
				}
			}
		}
		
		if (getConfiguration().getRuleBindingContainer() == null) {
			getConfiguration().setRuleBindingContainer(
					InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer());
		}
		
		
		EList<Annotation> oclAnnotationList = configuration.getTripleGraphGrammar().getAnnotationsByKeyString("OCL");
		if (!oclAnnotationList.isEmpty()) {
			String uriString = oclAnnotationList.get(0).getValueString();
			URI tggURI = configuration.getTripleGraphGrammar().eResource().getURI();
			URI uri = tggURI.trimSegments(1).appendSegment(uriString);
			setOcl(OCL
					.newInstance(EcoreEnvironmentFactory.INSTANCE));
			try {
				URL url = new URL(uri.toString());
				InputStream is = url.openStream();
				getOcl().parse(new OCLInput(is));
				
			} catch (MalformedURLException e) {
				logger.error(e);
			}
			catch (IOException e) {
				logger.error(e);
			}
			catch (ParserException e) {
				logger.error("There's a problem evaluating the OCL expressions: " + e);
			}
			setOclEnvironment((EcoreEnvironment) ocl.getEnvironment());
		}

		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void storeConfiguration() {
		//Storing the root objects may lead to invalid configurations if one
		// of the referenced objects is removed. So remove the root objects if 
		// they are not necessary, i.e., if a resource is given.
		for (DomainModel domMod : getConfiguration().getDomainModel()) {
			if (!domMod.getResourceURI().isEmpty())
				domMod.getRootObject().clear();
		}
		
		try {
			getConfiguration().eResource().save(new HashMap<Object, Object>());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IStatus performTransformation(IProgressMonitor monitor) {
		
		DebugSupport debugSupport = getDebugSupport();
		if(debugSupport!=null)
		{
			debugSupport.setRunning(true);
			debugSupport.startTransformation(getConfiguration());
		}
		else{
			//InterpreterPlugin.getLogManager().disableDebug();
		}
		
		IStatus result = getTransformationProcessor().performTransformation(monitor);
		
		if(debugSupport!=null)
		{
			debugSupport.setRunning(false);
			debugSupport.finishedTransformation(getConfiguration(), result.isOK());
		}
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void storeResult() {
		logger.debug("Storing result...");
		EList<DomainModel> targetAndCorrespondenceDomainModelsList = new BasicEList<DomainModel>();
		targetAndCorrespondenceDomainModelsList.addAll(getConfiguration().getActiveApplicationScenario().getTargetDomainModel());
		targetAndCorrespondenceDomainModelsList.addAll(getConfiguration().getActiveApplicationScenario().getCorrespondenceModel());
		for (DomainModel domainModel : targetAndCorrespondenceDomainModelsList) {
			for (Resource resource : domainModel.getResource()) {
				try {
					logger.debug("Saving resource: " + resource);
					Map<String, Object> options = new HashMap<String, Object>();
					options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
					resource.save(options);
				} catch (IOException e1) {
					logger.error("Problem saving resources " + e1.getMessage());
				}
				
			}
		}			
		logger.debug("...done");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransformationRunning() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				if (transformationProcessor != null)
					msgs = ((InternalEObject)transformationProcessor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR, null, msgs);
				return basicSetTransformationProcessor((TransformationProcessor)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				return basicSetTransformationProcessor(null, msgs);
			case InterpreterPackage.INTERPRETER__DEBUG_SUPPORT:
				return basicSetDebugSupport(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__CONFIGURATION:
				if (resolve) return getConfiguration();
				return basicGetConfiguration();
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				return getTransformationProcessor();
			case InterpreterPackage.INTERPRETER__OCL_ENVIRONMENT:
				return getOclEnvironment();
			case InterpreterPackage.INTERPRETER__DEBUG_SUPPORT:
				return getDebugSupport();
			case InterpreterPackage.INTERPRETER__OCL:
				return getOcl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__CONFIGURATION:
				setConfiguration((Configuration)newValue);
				return;
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)newValue);
				return;
			case InterpreterPackage.INTERPRETER__OCL_ENVIRONMENT:
				setOclEnvironment((EcoreEnvironment)newValue);
				return;
			case InterpreterPackage.INTERPRETER__DEBUG_SUPPORT:
				setDebugSupport((DebugSupport)newValue);
				return;
			case InterpreterPackage.INTERPRETER__OCL:
				setOcl((OCL)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__CONFIGURATION:
				setConfiguration((Configuration)null);
				return;
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)null);
				return;
			case InterpreterPackage.INTERPRETER__OCL_ENVIRONMENT:
				setOclEnvironment(OCL_ENVIRONMENT_EDEFAULT);
				return;
			case InterpreterPackage.INTERPRETER__DEBUG_SUPPORT:
				setDebugSupport((DebugSupport)null);
				return;
			case InterpreterPackage.INTERPRETER__OCL:
				setOcl(OCL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__CONFIGURATION:
				return configuration != null;
			case InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR:
				return transformationProcessor != null;
			case InterpreterPackage.INTERPRETER__OCL_ENVIRONMENT:
				return OCL_ENVIRONMENT_EDEFAULT == null ? oclEnvironment != null : !OCL_ENVIRONMENT_EDEFAULT.equals(oclEnvironment);
			case InterpreterPackage.INTERPRETER__DEBUG_SUPPORT:
				return debugSupport != null;
			case InterpreterPackage.INTERPRETER__OCL:
				return OCL_EDEFAULT == null ? ocl != null : !OCL_EDEFAULT.equals(ocl);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (oclEnvironment: ");
		result.append(oclEnvironment);
		result.append(", ocl: ");
		result.append(ocl);
		result.append(')');
		return result.toString();
	}

} //InterpreterImpl
