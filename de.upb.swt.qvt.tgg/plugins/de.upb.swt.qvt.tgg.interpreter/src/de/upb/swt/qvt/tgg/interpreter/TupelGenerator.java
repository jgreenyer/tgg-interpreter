/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.query.conditions.Condition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tupel Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The TupelGenerator systematically generates tupels from a (ordered)
 * list of input lists. E.g. given a list of three lists (numbers representing
 * arbitrary EObjects) [[1, 2, 3],  [1, 2, 3], [1, 2, 3]], calling getNextTupel 
 * for the first  time will yield [1, 1, 1], then [1, 1, 2], [1, 1, 3], [1, 2, 1], 
 * ... until [3, 3, 3]. When no further tupel can be generated, the generator
 * returns an empty EList.
 * 
 * Optionally, a condition can be specified to return only tupels that fulfill
 * this condition.
 * 
 * the replaceInputList method can be used to replace a list at a given position.
 * The counters of this list and all succeeding lists will be reset.
 * 
 * Subclasses may override the the getNextTupel method to replace input lists =)
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getInputLists <em>Input Lists</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getTupelConstraint <em>Tupel Constraint</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTupelGenerator()
 * @model
 * @generated
 */
public interface TupelGenerator extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Lists</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.common.util.EList}&lt;org.eclipse.emf.ecore.EObject>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Lists</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Lists</em>' attribute list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTupelGenerator_InputLists()
	 * @model unique="false" transient="true"
	 * @generated
	 */
	EList<EList<EObject>> getInputLists();

	/**
	 * Returns the value of the '<em><b>Tupel Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tupel Constraint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tupel Constraint</em>' attribute.
	 * @see #setTupelConstraint(Condition)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTupelGenerator_TupelConstraint()
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.Condition" transient="true"
	 * @generated
	 */
	Condition getTupelConstraint();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getTupelConstraint <em>Tupel Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tupel Constraint</em>' attribute.
	 * @see #getTupelConstraint()
	 * @generated
	 */
	void setTupelConstraint(Condition value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" many="false"
	 * @generated
	 */
	EList<EObject> getNextTupel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model inputListMany="false"
	 * @generated
	 */
	void replaceInputList(int position, EList<EObject> inputList);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean hasNext();

} // TupelGenerator
