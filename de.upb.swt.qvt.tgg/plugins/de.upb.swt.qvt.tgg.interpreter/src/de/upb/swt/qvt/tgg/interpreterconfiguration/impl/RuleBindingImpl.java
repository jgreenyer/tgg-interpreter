/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getNodeToEObjectMap <em>Node To EObject Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getEdgeBinding <em>Edge Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getNodeBinding <em>Node Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getRuleBindingContainer <em>Rule Binding Container</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getEdgeToVirtualModelEdgeMap <em>Edge To Virtual Model Edge Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getGraphElementStack <em>Graph Element Stack</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl#getTggRule <em>Tgg Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleBindingImpl extends EObjectImpl implements RuleBinding {
	/**
	 * The cached value of the '{@link #getNodeToEObjectMap() <em>Node To EObject Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeToEObjectMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Node, EObject> nodeToEObjectMap;

	/**
	 * The cached value of the '{@link #getEdgeBinding() <em>Edge Binding</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeBinding()
	 * @generated
	 * @ordered
	 */
	protected EMap<EList<EObject>, EList<Edge>> edgeBinding;

	/**
	 * The cached value of the '{@link #getNodeBinding() <em>Node Binding</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeBinding()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<Node>> nodeBinding;

	/**
	 * The cached value of the '{@link #getEdgeToVirtualModelEdgeMap() <em>Edge To Virtual Model Edge Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeToVirtualModelEdgeMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Edge, EList<EObject>> edgeToVirtualModelEdgeMap;

	/**
	 * The cached value of the '{@link #getGraphElementStack() <em>Graph Element Stack</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphElementStack()
	 * @generated
	 * @ordered
	 */
	protected EList<GraphElement> graphElementStack;

	/**
	 * The cached value of the '{@link #getTggRule() <em>Tgg Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTggRule()
	 * @generated
	 * @ordered
	 */
	protected TripleGraphGrammarRule tggRule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.RULE_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Node, EObject> getNodeToEObjectMap() {
		if (nodeToEObjectMap == null) {
			nodeToEObjectMap = new EcoreEMap<Node,EObject>(InterpreterconfigurationPackage.Literals.NODE_TO_EOBJECT_MAP_ENTRY, NodeToEObjectMapEntryImpl.class, this, InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP);
		}
		return nodeToEObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EList<EObject>, EList<Edge>> getEdgeBinding() {
		if (edgeBinding == null) {
			edgeBinding = new EcoreEMap<EList<EObject>,EList<Edge>>(InterpreterconfigurationPackage.Literals.VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY, VirtualModelEdgeToEdgeMapEntryImpl.class, this, InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING);
		}
		return edgeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<Node>> getNodeBinding() {
		if (nodeBinding == null) {
			nodeBinding = new EcoreEMap<EObject,EList<Node>>(InterpreterconfigurationPackage.Literals.EOBJECT_TO_NODE_MAP_ENTRY, EObjectToNodeMapEntryImpl.class, this, InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING);
		}
		return nodeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBindingContainer getRuleBindingContainer() {
		if (eContainerFeatureID() != InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER) return null;
		return (RuleBindingContainer)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleBindingContainer(RuleBindingContainer newRuleBindingContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRuleBindingContainer, InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleBindingContainer(RuleBindingContainer newRuleBindingContainer) {
		if (newRuleBindingContainer != eInternalContainer() || (eContainerFeatureID() != InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER && newRuleBindingContainer != null)) {
			if (EcoreUtil.isAncestor(this, newRuleBindingContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRuleBindingContainer != null)
				msgs = ((InternalEObject)newRuleBindingContainer).eInverseAdd(this, InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING, RuleBindingContainer.class, msgs);
			msgs = basicSetRuleBindingContainer(newRuleBindingContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER, newRuleBindingContainer, newRuleBindingContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Edge, EList<EObject>> getEdgeToVirtualModelEdgeMap() {
		if (edgeToVirtualModelEdgeMap == null) {
			edgeToVirtualModelEdgeMap = new EcoreEMap<Edge,EList<EObject>>(InterpreterconfigurationPackage.Literals.EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY, EdgeToVirtualModelEdgeMapEntryImpl.class, this, InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP);
		}
		return edgeToVirtualModelEdgeMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GraphElement> getGraphElementStack() {
		if (graphElementStack == null) {
			graphElementStack = new EObjectResolvingEList<GraphElement>(GraphElement.class, this, InterpreterconfigurationPackage.RULE_BINDING__GRAPH_ELEMENT_STACK);
		}
		return graphElementStack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule getTggRule() {
		if (tggRule != null && tggRule.eIsProxy()) {
			InternalEObject oldTggRule = (InternalEObject)tggRule;
			tggRule = (TripleGraphGrammarRule)eResolveProxy(oldTggRule);
			if (tggRule != oldTggRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE, oldTggRule, tggRule));
			}
		}
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule basicGetTggRule() {
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggRule(TripleGraphGrammarRule newTggRule) {
		TripleGraphGrammarRule oldTggRule = tggRule;
		tggRule = newTggRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE, oldTggRule, tggRule));
	}
	
//	protected Map<Node, OCL> nodeToOldOCLEnvironmentMap = new HashMap<Node, OCL>();
//	protected OCL ocl;
//	protected EcoreEnvironment initialOCLEnv;
//	
//	public OCL getOCL(){
//		if (ocl == null){
//			ocl = createOCL();
//		}
//		return ocl;
//	}
//	
//	public void setInitialOCLEnvironment(EcoreEnvironment initialOCLEnv){
//		this.initialOCLEnv = initialOCLEnv;
//	}
//	
//	/**
//	 * Initialize OCL interpreter;
//	 */
//	protected OCL initialOCL;
//	protected OCL createOCL() {
//		logger.debug("Creating initial OCL...");
//		//if (initialOCL != null) return initialOCL;
//		if (initialOCLEnv != null){
//				EcoreEnvironment nestedEcoreEnvironment = (EcoreEnvironment) EcoreEnvironmentFactory.INSTANCE.createEnvironment(initialOCLEnv);
//				initialOCL = OCL.newInstance(nestedEcoreEnvironment);
//			}
//		else{
//			initialOCL = OCL.newInstance((EcoreEnvironment)EcoreEnvironmentFactory.INSTANCE.createEnvironment());
//		}
//		OCLHelper<EClassifier, ?, EStructuralFeature, org.eclipse.ocl.ecore.Constraint> helper = initialOCL.createOCLHelper();
//		for (Entry<Node, EObject> nodeBinding : getNodeToEObjectMap().entrySet()) {
//			if (nodeBinding.getKey().getName() != null && !nodeBinding.getKey().getName().equals("")){
//				addNewVariable(nodeBinding.getKey(), nodeBinding.getValue(), initialOCL);
//			}
//		}
//		return initialOCL;
//	}
//
//	protected void addNewVariable(Node node, EObject eObject, OCL ocl) {
//		if (node.getName() != null && !node.getName().equals("")) {
//			addNewVariable(node.getName(), node.getEType(), eObject, ocl);
//		}
//	}
//
//	protected void addNewVariable(String variableName, EClassifier type, Object value, OCL ocl) {
//
//		logger.debug("Adding new variable: " + variableName + " - " + value);
//
//		
//		// TODO variable may not be removed!!
//		for (Variable<?, ?> definedVariable : ocl.getEnvironment().getVariables()) {
//			if (definedVariable.getName().equals(variableName)){
//				//logger.error("How can that be?");
//				return;
//			}
//		}
//		Variable<EClassifier, EParameter> var = ExpressionsFactory.eINSTANCE.createVariable();
//		var.setName(variableName);
//		if (type == null) {
//			if (value instanceof EObject) {
//				type = ((EObject) value).eClass();
//			} else if (value instanceof String) {
//				type = EcorePackage.Literals.ESTRING;
//			} else if (value instanceof Integer) {
//				type = EcorePackage.Literals.EINTEGER_OBJECT;
//			}
//		}
//		var.setType(type);
//		assert (value != null);
//		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) == null);
//		ocl.getEnvironment().addElement(var.getName(), var, true);
//		ocl.getEvaluationEnvironment().add(var.getName(), value);
//		assert (ocl.getEvaluationEnvironment().getValueOf(var.getName()) != null);
//		// if (logger.isDebugEnabled())
//		// logger.debug("Variable added: " + var.getName() + " type: " +
//		// var.getType() + " value: " + value);
//	}
//
//	
//	protected void createNewOCLFor(Node node, EObject eObject) {
//		if (node.getName() != null && !node.getName().equals("")){
//			
//			ocl = createOCL();
//
////			logger.debug("Create new OCL for node " + node.getName());
////			
////			if (ocl == null){
////				ocl = createOCL();
////			}
////
////			nodeToOldOCLEnvironmentMap.put(node, ocl);
////
////			EcoreEnvironment nestedEcoreEnvironment = (EcoreEnvironment) EcoreEnvironmentFactory.INSTANCE.createEnvironment(ocl.getEnvironment());
////			EcoreEvaluationEnvironment nestedEcoreEvaluationEnvironment = (EcoreEvaluationEnvironment)EcoreEnvironmentFactory.INSTANCE.createEvaluationEnvironment(ocl.getEvaluationEnvironment());
////
////			ocl = OCL.newInstance(nestedEcoreEnvironment, nestedEcoreEvaluationEnvironment);
////			
////			addNewVariable(node.getName(), node.getEType(), eObject, ocl);
//		}
//	}
//
//	protected void rollBackOCL(Node node){
//		logger.debug("Roll back OCL for node " + node.getName());
//		ocl = nodeToOldOCLEnvironmentMap.get(node);
//	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeElementBindingsFromStack(GraphElement element) {
		int elementPosition = getGraphElementStack().indexOf(element);
		for (int i = getGraphElementStack().size()-1; i >= elementPosition; i--) {
			GraphElement elementToRemove = getGraphElementStack().get(i);
			if (elementToRemove instanceof Node){
				Node node = (Node) elementToRemove;
				EObject boundObject = getNodeToEObjectMap().get(node);
				getNodeBinding().get(boundObject).remove(node);
				if (getNodeBinding().get(boundObject).isEmpty())
					getNodeBinding().remove(boundObject);
				getNodeToEObjectMap().remove(node);
				
				// OCL
				//rollBackOCL(node);
				
			}
			if (elementToRemove instanceof Edge){
				Edge edge = (Edge) elementToRemove;
				EList<EObject> virtualModelEdge = getEdgeToVirtualModelEdgeMap().get(edge);
				getEdgeBinding().get(virtualModelEdge).remove(edge);
				if (getEdgeBinding().get(virtualModelEdge).isEmpty())
					getEdgeBinding().remove(virtualModelEdge);
				getEdgeToVirtualModelEdgeMap().remove(edge);
			}
			getGraphElementStack().remove(i);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void pushEdgeBinding(Edge edge, EList<EObject> virtualModelEdge) {
//		// do not add this edge binding when the edge is already bound
		if(getEdgeBinding().get(virtualModelEdge) == null){
			getEdgeBinding().put(virtualModelEdge, new UniqueEList<Edge>());
		}
		getEdgeBinding().get(virtualModelEdge).add(edge);
		getEdgeToVirtualModelEdgeMap().put(edge, virtualModelEdge);
		getGraphElementStack().add(edge);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void pushNodeBinding(Node node, EObject modelObject) {
		// do not add this node binding when the node is already bound
		if (getNodeBinding().get(modelObject) == null){
			getNodeBinding().put(modelObject, new UniqueEList<Node>());
		}
		for (Node refinedNode : node.getAllRefinedNodes()) {
			getNodeBinding().get(modelObject).add(refinedNode);
			getNodeToEObjectMap().put(refinedNode, modelObject);
			getGraphElementStack().add(refinedNode);
		}
		
		//OCL
		//createNewOCLFor(node, modelObject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRuleBindingContainer((RuleBindingContainer)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP:
				return ((InternalEList<?>)getNodeToEObjectMap()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING:
				return ((InternalEList<?>)getEdgeBinding()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING:
				return ((InternalEList<?>)getNodeBinding()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				return basicSetRuleBindingContainer(null, msgs);
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP:
				return ((InternalEList<?>)getEdgeToVirtualModelEdgeMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				return eInternalContainer().eInverseRemove(this, InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING, RuleBindingContainer.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP:
				if (coreType) return getNodeToEObjectMap();
				else return getNodeToEObjectMap().map();
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING:
				if (coreType) return getEdgeBinding();
				else return getEdgeBinding().map();
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING:
				if (coreType) return getNodeBinding();
				else return getNodeBinding().map();
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				return getRuleBindingContainer();
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP:
				if (coreType) return getEdgeToVirtualModelEdgeMap();
				else return getEdgeToVirtualModelEdgeMap().map();
			case InterpreterconfigurationPackage.RULE_BINDING__GRAPH_ELEMENT_STACK:
				return getGraphElementStack();
			case InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE:
				if (resolve) return getTggRule();
				return basicGetTggRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP:
				((EStructuralFeature.Setting)getNodeToEObjectMap()).set(newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING:
				((EStructuralFeature.Setting)getEdgeBinding()).set(newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING:
				((EStructuralFeature.Setting)getNodeBinding()).set(newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				setRuleBindingContainer((RuleBindingContainer)newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP:
				((EStructuralFeature.Setting)getEdgeToVirtualModelEdgeMap()).set(newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__GRAPH_ELEMENT_STACK:
				getGraphElementStack().clear();
				getGraphElementStack().addAll((Collection<? extends GraphElement>)newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE:
				setTggRule((TripleGraphGrammarRule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP:
				getNodeToEObjectMap().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING:
				getEdgeBinding().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING:
				getNodeBinding().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				setRuleBindingContainer((RuleBindingContainer)null);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP:
				getEdgeToVirtualModelEdgeMap().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__GRAPH_ELEMENT_STACK:
				getGraphElementStack().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE:
				setTggRule((TripleGraphGrammarRule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_TO_EOBJECT_MAP:
				return nodeToEObjectMap != null && !nodeToEObjectMap.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_BINDING:
				return edgeBinding != null && !edgeBinding.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING__NODE_BINDING:
				return nodeBinding != null && !nodeBinding.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER:
				return getRuleBindingContainer() != null;
			case InterpreterconfigurationPackage.RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP:
				return edgeToVirtualModelEdgeMap != null && !edgeToVirtualModelEdgeMap.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING__GRAPH_ELEMENT_STACK:
				return graphElementStack != null && !graphElementStack.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING__TGG_RULE:
				return tggRule != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public String toString() {
		String returnString = "Rule Binding: ";
		if (getTggRule() != null){
			returnString += getTggRule().getName();
		}
		returnString += " [";
		for (GraphElement graphElement : getGraphElementStack()) {
			if(graphElement instanceof Node){
				returnString += "[TGGNode " + graphElement.getName() + "=>" + 
					getNodeToEObjectMap().get(graphElement) + "]";
			}
			if(graphElement instanceof Edge){
				returnString += "[TGGEdge " + graphElement.getName() + "=>" + 
					getEdgeToVirtualModelEdgeMap().get(graphElement) + "]";
			}
			if (graphElement != getGraphElementStack().get(getGraphElementStack().size()-1)){
				returnString += ", \n";
			}
		}
		returnString += "]";
		return returnString;
	}
} //RuleBindingImpl
