/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Binding Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl#getRuleBinding <em>Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl#getInitialAxiomBinding <em>Initial Axiom Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleBindingContainerImpl extends EObjectImpl implements RuleBindingContainer {
	/**
	 * The cached value of the '{@link #getRuleBinding() <em>Rule Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleBinding()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleBinding> ruleBinding;

	/**
	 * The cached value of the '{@link #getInitialAxiomBinding() <em>Initial Axiom Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialAxiomBinding()
	 * @generated
	 * @ordered
	 */
	protected AxiomBinding initialAxiomBinding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleBindingContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.RULE_BINDING_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleBinding> getRuleBinding() {
		if (ruleBinding == null) {
			ruleBinding = new EObjectContainmentWithInverseEList<RuleBinding>(RuleBinding.class, this, InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING, InterpreterconfigurationPackage.RULE_BINDING__RULE_BINDING_CONTAINER);
		}
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxiomBinding getInitialAxiomBinding() {
		return initialAxiomBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialAxiomBinding(AxiomBinding newInitialAxiomBinding, NotificationChain msgs) {
		AxiomBinding oldInitialAxiomBinding = initialAxiomBinding;
		initialAxiomBinding = newInitialAxiomBinding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING, oldInitialAxiomBinding, newInitialAxiomBinding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialAxiomBinding(AxiomBinding newInitialAxiomBinding) {
		if (newInitialAxiomBinding != initialAxiomBinding) {
			NotificationChain msgs = null;
			if (initialAxiomBinding != null)
				msgs = ((InternalEObject)initialAxiomBinding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING, null, msgs);
			if (newInitialAxiomBinding != null)
				msgs = ((InternalEObject)newInitialAxiomBinding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING, null, msgs);
			msgs = basicSetInitialAxiomBinding(newInitialAxiomBinding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING, newInitialAxiomBinding, newInitialAxiomBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRuleBinding()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				return ((InternalEList<?>)getRuleBinding()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING:
				return basicSetInitialAxiomBinding(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				return getRuleBinding();
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING:
				return getInitialAxiomBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				getRuleBinding().clear();
				getRuleBinding().addAll((Collection<? extends RuleBinding>)newValue);
				return;
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING:
				setInitialAxiomBinding((AxiomBinding)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				getRuleBinding().clear();
				return;
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING:
				setInitialAxiomBinding((AxiomBinding)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__RULE_BINDING:
				return ruleBinding != null && !ruleBinding.isEmpty();
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING:
				return initialAxiomBinding != null;
		}
		return super.eIsSet(featureID);
	}

} //RuleBindingContainerImpl
