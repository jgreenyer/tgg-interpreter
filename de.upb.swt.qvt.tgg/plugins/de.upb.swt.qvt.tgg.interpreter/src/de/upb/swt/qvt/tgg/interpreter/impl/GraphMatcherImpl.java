/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.TransformationProcessorUtil;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Graph Matcher</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getRuleBinding <em>Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getNodeMatchingPolicy <em>Node Matching Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getEdgeMatchingPolicy <em>Edge Matching Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getRuleProcessor <em>Rule Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getDomainsToMatch <em>Domains To Match</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getConstraintCheckingPolicy <em>Constraint Checking Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#isMatchReusablePatternOnly <em>Match Reusable Pattern Only</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getElementsNotAvailableForProducedNodes <em>Elements Not Available For Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getLinksNotAvailableForProducedEdges <em>Links Not Available For Produced Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#isRestrictProducedGraphMatching <em>Restrict Produced Graph Matching</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getStartNodeOfMatching <em>Start Node Of Matching</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#isNoElementsToMatchLeft <em>No Elements To Match Left</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl#getEdgeExploreOperationsPerformed <em>Edge Explore Operations Performed</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GraphMatcherImpl extends EObjectImpl implements GraphMatcher {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(GraphMatcherImpl.class.getName());
	
	
	/**
	 * The cached value of the '{@link #getRuleBinding() <em>Rule Binding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleBinding()
	 * @generated
	 * @ordered
	 */
	protected RuleBinding ruleBinding;

	/**
	 * The cached value of the '{@link #getNodeMatchingPolicy() <em>Node Matching Policy</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeMatchingPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<INodeMatchingPolicy> nodeMatchingPolicy;

	/**
	 * The cached value of the '{@link #getEdgeMatchingPolicy() <em>Edge Matching Policy</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeMatchingPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<IEdgeMatchingPolicy> edgeMatchingPolicy;

	/**
	 * The cached value of the '{@link #getDomainsToMatch() <em>Domains To Match</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainsToMatch()
	 * @generated
	 * @ordered
	 */
	protected EList<DomainModel> domainsToMatch;


	/**
	 * The cached value of the '{@link #getConstraintCheckingPolicy() <em>Constraint Checking Policy</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintCheckingPolicy()
	 * @generated
	 * @ordered
	 */
	protected EList<IConstraintCheckingPolicy> constraintCheckingPolicy;

	/**
	 * The default value of the '{@link #isMatchReusablePatternOnly() <em>Match Reusable Pattern Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMatchReusablePatternOnly()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MATCH_REUSABLE_PATTERN_ONLY_EDEFAULT = false;


	/**
	 * The cached value of the '{@link #isMatchReusablePatternOnly() <em>Match Reusable Pattern Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMatchReusablePatternOnly()
	 * @generated
	 * @ordered
	 */
	protected boolean matchReusablePatternOnly = MATCH_REUSABLE_PATTERN_ONLY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElementsNotAvailableForProducedNodes() <em>Elements Not Available For Produced Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementsNotAvailableForProducedNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> elementsNotAvailableForProducedNodes;


	/**
	 * The cached value of the '{@link #getLinksNotAvailableForProducedEdges() <em>Links Not Available For Produced Edges</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinksNotAvailableForProducedEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<EList<EObject>> linksNotAvailableForProducedEdges;


	/**
	 * The default value of the '{@link #isRestrictProducedGraphMatching() <em>Restrict Produced Graph Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRestrictProducedGraphMatching()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESTRICT_PRODUCED_GRAPH_MATCHING_EDEFAULT = false;


	/**
	 * The cached value of the '{@link #isRestrictProducedGraphMatching() <em>Restrict Produced Graph Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRestrictProducedGraphMatching()
	 * @generated
	 * @ordered
	 */
	protected boolean restrictProducedGraphMatching = RESTRICT_PRODUCED_GRAPH_MATCHING_EDEFAULT;


	/**
	 * The cached value of the '{@link #getStartNodeOfMatching() <em>Start Node Of Matching</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartNodeOfMatching()
	 * @generated
	 * @ordered
	 */
	protected Node startNodeOfMatching;


	/**
	 * The default value of the '{@link #isNoElementsToMatchLeft() <em>No Elements To Match Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoElementsToMatchLeft()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NO_ELEMENTS_TO_MATCH_LEFT_EDEFAULT = false;


	/**
	 * The default value of the '{@link #getEdgeExploreOperationsPerformed() <em>Edge Explore Operations Performed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformed()
	 * @generated
	 * @ordered
	 */
	protected static final int EDGE_EXPLORE_OPERATIONS_PERFORMED_EDEFAULT = 0;


	/**
	 * The cached value of the '{@link #getEdgeExploreOperationsPerformed() <em>Edge Explore Operations Performed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformed()
	 * @generated
	 * @ordered
	 */
	protected int edgeExploreOperationsPerformed = EDGE_EXPLORE_OPERATIONS_PERFORMED_EDEFAULT;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphMatcherImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.GRAPH_MATCHER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding getRuleBinding() {
		if (ruleBinding != null && ruleBinding.eIsProxy()) {
			InternalEObject oldRuleBinding = (InternalEObject)ruleBinding;
			ruleBinding = (RuleBinding)eResolveProxy(oldRuleBinding);
			if (ruleBinding != oldRuleBinding) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.GRAPH_MATCHER__RULE_BINDING, oldRuleBinding, ruleBinding));
			}
		}
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding basicGetRuleBinding() {
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleBinding(RuleBinding newRuleBinding) {
		RuleBinding oldRuleBinding = ruleBinding;
		ruleBinding = newRuleBinding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__RULE_BINDING, oldRuleBinding, ruleBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<INodeMatchingPolicy> getNodeMatchingPolicy() {
		if (nodeMatchingPolicy == null) {
			nodeMatchingPolicy = new EObjectWithInverseResolvingEList<INodeMatchingPolicy>(INodeMatchingPolicy.class, this, InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY, InterpreterPackage.INODE_MATCHING_POLICY__GRAPH_MATCHER);
		}
		return nodeMatchingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IEdgeMatchingPolicy> getEdgeMatchingPolicy() {
		if (edgeMatchingPolicy == null) {
			edgeMatchingPolicy = new EObjectWithInverseResolvingEList<IEdgeMatchingPolicy>(IEdgeMatchingPolicy.class, this, InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY, InterpreterPackage.IEDGE_MATCHING_POLICY__GRAPH_MATCHER);
		}
		return edgeMatchingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleProcessor getRuleProcessor() {
		if (eContainerFeatureID() != InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR) return null;
		return (RuleProcessor)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleProcessor(RuleProcessor newRuleProcessor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRuleProcessor, InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleProcessor(RuleProcessor newRuleProcessor) {
		if (newRuleProcessor != eInternalContainer() || (eContainerFeatureID() != InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR && newRuleProcessor != null)) {
			if (EcoreUtil.isAncestor(this, newRuleProcessor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRuleProcessor != null)
				msgs = ((InternalEObject)newRuleProcessor).eInverseAdd(this, InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER, RuleProcessor.class, msgs);
			msgs = basicSetRuleProcessor(newRuleProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR, newRuleProcessor, newRuleProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DomainModel> getDomainsToMatch() {
		if (domainsToMatch == null) {
			domainsToMatch = new EObjectResolvingEList<DomainModel>(DomainModel.class, this, InterpreterPackage.GRAPH_MATCHER__DOMAINS_TO_MATCH);
		}
		return domainsToMatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IConstraintCheckingPolicy> getConstraintCheckingPolicy() {
		if (constraintCheckingPolicy == null) {
			constraintCheckingPolicy = new EObjectWithInverseResolvingEList<IConstraintCheckingPolicy>(IConstraintCheckingPolicy.class, this, InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY, InterpreterPackage.ICONSTRAINT_CHECKING_POLICY__GRAPH_MATCHER);
		}
		return constraintCheckingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMatchReusablePatternOnly() {
		return matchReusablePatternOnly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchReusablePatternOnly(boolean newMatchReusablePatternOnly) {
		boolean oldMatchReusablePatternOnly = matchReusablePatternOnly;
		matchReusablePatternOnly = newMatchReusablePatternOnly;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY, oldMatchReusablePatternOnly, matchReusablePatternOnly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getElementsNotAvailableForProducedNodes() {
		if (elementsNotAvailableForProducedNodes == null) {
			elementsNotAvailableForProducedNodes = new EObjectResolvingEList<EObject>(EObject.class, this, InterpreterPackage.GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES);
		}
		return elementsNotAvailableForProducedNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EList<EObject>> getLinksNotAvailableForProducedEdges() {
		if (linksNotAvailableForProducedEdges == null) {
			linksNotAvailableForProducedEdges = new EDataTypeUniqueEList<EList<EObject>>(EList.class, this, InterpreterPackage.GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES);
		}
		return linksNotAvailableForProducedEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRestrictProducedGraphMatching() {
		return restrictProducedGraphMatching;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictProducedGraphMatching(boolean newRestrictProducedGraphMatching) {
		boolean oldRestrictProducedGraphMatching = restrictProducedGraphMatching;
		restrictProducedGraphMatching = newRestrictProducedGraphMatching;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING, oldRestrictProducedGraphMatching, restrictProducedGraphMatching));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getStartNodeOfMatching() {
		if (startNodeOfMatching != null && startNodeOfMatching.eIsProxy()) {
			InternalEObject oldStartNodeOfMatching = (InternalEObject)startNodeOfMatching;
			startNodeOfMatching = (Node)eResolveProxy(oldStartNodeOfMatching);
			if (startNodeOfMatching != oldStartNodeOfMatching) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING, oldStartNodeOfMatching, startNodeOfMatching));
			}
		}
		return startNodeOfMatching;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetStartNodeOfMatching() {
		return startNodeOfMatching;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartNodeOfMatching(Node newStartNodeOfMatching) {
		Node oldStartNodeOfMatching = startNodeOfMatching;
		startNodeOfMatching = newStartNodeOfMatching;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING, oldStartNodeOfMatching, startNodeOfMatching));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isNoElementsToMatchLeft() {
		//FIXME: This only counts node bindings. Do we have to consider edge bindings, too?
		return (getRuleBinding().getNodeBinding().size() +1 >= getRuleBinding().getTggRule().getAllNodes().size());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEdgeExploreOperationsPerformed() {
		return edgeExploreOperationsPerformed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEdgeExploreOperationsPerformed(int newEdgeExploreOperationsPerformed) {
		int oldEdgeExploreOperationsPerformed = edgeExploreOperationsPerformed;
		edgeExploreOperationsPerformed = newEdgeExploreOperationsPerformed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED, oldEdgeExploreOperationsPerformed, edgeExploreOperationsPerformed));
	}

	private String getNodeBindingString(Node node, EObject eObject){
		return "["+node.getName()+"->"+getObjectNameString(eObject)+"]";
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * 1. Query the NodeMatchingPolicy whether the given node matches the given object
	 * Return false when this is not the case.
	 * 2. Push it on the rule binding's stack.
	 * 3. Ask the patternMatchingStrategy for an outgoing or incoming edge to 
	 * match next. Call the methods exploreOutgoingEdge(), resp. 
	 * exploreIncomingEdge() with that edge.
	 * In case that any call of exploreOutgoingEdge(), resp. exploreIncomingEdge()
	 * return false and PatternMatchingStrategy.edgeMatchMandatory() returns true,
	 * then remove the node binding from the rule binding's stack and 
	 * return false.
	 * Otherwise 
	 * return true.
	 * 
	 * (When this method PatternMatchingStrategy.edgeMatchMandatory() returns
	 * true, the graph matcher performs a naive partial matching that does not 
	 * backtrack on nodes.)
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings({ "unchecked" })
	public boolean exploreEdges(Node startNode, EObject startObject, int utilityDepthCounter) {
		
		assert(startObject != null);
		
		// for diagnostics
		edgeExploreOperationsPerformed++;
		
		if (logger.isDebugEnabled())
			logger.debug(getUtilityString(utilityDepthCounter) + 
				 "STARTING   from node binding " + getNodeBindingString(startNode, startObject));
		
		// check if it will be possible to create and match a link according to a produced edge
		// between context nodes in a target/correspondence pattern. 
		// Otherwise, i.e., if there is already a bound link, the rule cannot be matched.
		if (!isNodeInDomainsToMatch(startNode)){ // if the node is in a target/correspondence domain...
			EList<Edge> allOutgoingEdges = startNode.getAllOutgoingEdges();
			for (Edge outgoingEdge : allOutgoingEdges) {
				// edge is produced and there is an object bound to the target node
				if (outgoingEdge.isProduced() 
						&& getRuleBinding().getNodeToEObjectMap().get(outgoingEdge.getTargetNode()) != null){
					EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
					virtualModelEdge.add(startObject);
					virtualModelEdge.add(getRuleBinding().getNodeToEObjectMap().get(outgoingEdge.getTargetNode()));
					virtualModelEdge.add(outgoingEdge.getTypeReference());
					if (getRuleProcessor().getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge) != null){
						logger.debug(getUtilityString(utilityDepthCounter) + 
								 "the link (" + virtualModelEdge + ") is already bound. It will not be possible to create and bind the produced edge " + outgoingEdge);
						return false;
					}
				}
			}
			EList<Edge> allIncomingEdges = startNode.getAllIncomingEdges();
			for (Edge incomingEdge : allIncomingEdges) {
				// edge is produced and there is an object bound to the source node
				if (incomingEdge.isProduced() 
						&& getRuleBinding().getNodeToEObjectMap().get(incomingEdge.getSourceNode()) != null){
					EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
					virtualModelEdge.add(getRuleBinding().getNodeToEObjectMap().get(incomingEdge.getTargetNode()));
					virtualModelEdge.add(startObject);
					virtualModelEdge.add(incomingEdge.getTypeReference());
					if (getRuleProcessor().getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge) != null){
						logger.debug(getUtilityString(utilityDepthCounter) + 
								 "the link (" + virtualModelEdge + ") is already bound. It will not be possible to create and bind the produced edge " + incomingEdge);
						return false;
					}
				}
			}
		}
		// END check if it will be possible to create and match a link according to a produced edge
		// between context nodes in a target/correspondence pattern. 
		
		
		EList<Edge> unboundEdgesToMatchNext = getUnboundEdgeToMatchNext(startNode, startObject);

		if (logger.isDebugEnabled())
			logger.debug(getUtilityString(utilityDepthCounter) + 
				 "unbound edges to match next: " + " " + unboundEdgesToMatchNext.size() + " " + unboundEdgesToMatchNext);

		
		//if there is nothing more to match from here on, return TRUE.
		if (unboundEdgesToMatchNext.isEmpty()) {
			if (logger.isDebugEnabled())
				logger.debug(getUtilityString(utilityDepthCounter) + 
					 "SUCCESSFUL from node binding " + getNodeBindingString(startNode, startObject));
			return true;
		}
				
		int edgeCounter = 0;
		int numberOfEdges = unboundEdgesToMatchNext.size();
		int[] candidateObjectCounter = new int[unboundEdgesToMatchNext.size()];
		EList<EObject>[] candidateObjectList = new EList[unboundEdgesToMatchNext.size()];
		
		//--DEBUG--//
		DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForCandidateNode(null, null, this);
	
		
		// match edges
		while(true){
			//lazy init of candidateObjectList entries
			if (candidateObjectList[edgeCounter] == null){
				if(logger.isDebugEnabled())
					logger.debug(getUtilityString(utilityDepthCounter) + " collecting candidates for edge " + unboundEdgesToMatchNext.get(edgeCounter));
				candidateObjectList[edgeCounter] = getCandidateNeighborObjects(startNode, startObject, unboundEdgesToMatchNext.get(edgeCounter));
				if (candidateObjectList[edgeCounter].isEmpty()){
					if (logger.isDebugEnabled()){
						logger.debug(getUtilityString(utilityDepthCounter) + 
							 "FAILED     from node binding " + getNodeBindingString(startNode, startObject));
						logger.debug(getUtilityString(utilityDepthCounter) + 
							 " REASON: No candidate neighbor objects available for edge " + unboundEdgesToMatchNext.get(edgeCounter));
					}
					return false;
				}				
			}
			// start exploring the edge with neighbor candidate objects. 
			if (logger.isDebugEnabled())
				logger.debug(getUtilityString(utilityDepthCounter) + 
					 "unbound edge trying to match next: " + edgeCounter + " "
						+ unboundEdgesToMatchNext.get(edgeCounter) 
						+ " with candidate neighbor object " + candidateObjectList[edgeCounter].get(candidateObjectCounter[edgeCounter]));
			assert(candidateObjectList[edgeCounter].get(candidateObjectCounter[edgeCounter]) != null);

			// check whether the neighbor node is already bound. Then, don't iterate over all the candidate neighbor objects
			if (getRuleBinding().getNodeToEObjectMap().get(getNeighborNode(startNode, unboundEdgesToMatchNext.get(edgeCounter))) != null){
				// check whether the object bound to the neighbor node is included in the neighbor objects list of the considered edge
				// if this is not the case, the current binding is invalid. Then return false.
				EObject objectBoundToNeighborNode = getRuleBinding().getNodeToEObjectMap().get(getNeighborNode(startNode, unboundEdgesToMatchNext.get(edgeCounter)));
				if (!candidateObjectList[edgeCounter].contains(objectBoundToNeighborNode)){
					if (logger.isDebugEnabled()){
						logger.debug(getUtilityString(utilityDepthCounter) + 
							 "FAILED     from node binding " + getNodeBindingString(startNode, startObject));
						logger.debug(getUtilityString(utilityDepthCounter) + 
							 " REASON: The neighbor node " + getNeighborNode(startNode, unboundEdgesToMatchNext.get(edgeCounter)) 
							 + " reachable via the edge " + unboundEdgesToMatchNext.get(edgeCounter)
							 + " is already bound, but the object bound to this node " + objectBoundToNeighborNode
							 + " is not referenced by the candidate object " + startObject 
							 + " via the edge's type reference " + unboundEdgesToMatchNext.get(edgeCounter).getTypeReference());
					}
					return false;
				}
				if (exploreEdge(startNode, startObject,
						unboundEdgesToMatchNext.get(edgeCounter),
						getRuleBinding().getNodeToEObjectMap().get(getNeighborNode(startNode, unboundEdgesToMatchNext.get(edgeCounter))),
						utilityDepthCounter)){
					edgeCounter++;					
					if (edgeCounter >= numberOfEdges) {
						if (logger.isDebugEnabled())
							logger.debug(getUtilityString(utilityDepthCounter) + 
								 "SUCCESSFUL from node binding " + getNodeBindingString(startNode, startObject));
						return true;
					}
				}else{
					return false;
				}
			}else //otherwise iterate over all the candidate neighbor objects
			if (exploreEdge(startNode, startObject, 
					unboundEdgesToMatchNext.get(edgeCounter),
					candidateObjectList[edgeCounter].get(candidateObjectCounter[edgeCounter]),
					utilityDepthCounter)){
				edgeCounter++;
				if (edgeCounter >= numberOfEdges) {
					if (logger.isDebugEnabled())
						logger.debug(getUtilityString(utilityDepthCounter) + 
							 "SUCCESSFUL from node binding " + getNodeBindingString(startNode, startObject));
					return true;
				}
			}else{
				candidateObjectCounter[edgeCounter]++;
				while(candidateObjectCounter[edgeCounter] >= candidateObjectList[edgeCounter].size() && edgeCounter >= 0) {
					candidateObjectCounter[edgeCounter] = 0;
					edgeCounter--;
					if(edgeCounter < 0) {
						if (logger.isDebugEnabled()){
							logger.debug(getUtilityString(utilityDepthCounter) + 
								 "FAILED     from node binding " + getNodeBindingString(startNode, startObject));
							logger.debug(getUtilityString(utilityDepthCounter) + 
								 " REASON: Could not match all edges");
						}
						return false;
					}
					candidateObjectCounter[edgeCounter]++;
				}
			}
		}
	}
	
	private Node getNeighborNode(Node startNode, Edge edge){
		assert(startNode.getAllOutgoingEdges().contains(edge) || startNode.getAllIncomingEdges().contains(edge));
		if (startNode.getAllOutgoingEdges().contains(edge)){
			return getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getTargetNode());
		}
		if (startNode.getAllIncomingEdges().contains(edge)){
			return getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getSourceNode());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected EList<EObject> getCandidateNeighborObjects(Node startNode, EObject startObject, Edge edge){
		EList<EObject> candidateObjectList;
		if (startNode.getAllOutgoingEdges().contains(edge)){ //is an outgoing edge
			if (edge.getTypeReference().isMany()) {
				candidateObjectList = ((EList<EObject>)startObject.eGet(edge.getTypeReference()));
			} else {
				EObject candidateEObject = (EObject)startObject.eGet(edge.getTypeReference(), true);
				candidateObjectList = new UniqueEList<EObject>();
				if (candidateEObject != null)
					candidateObjectList.add(candidateEObject);
			}
		}else{ // is an incoming edge
			candidateObjectList = new UniqueEList<EObject>();
			
	//		if (logger.isDebugEnabled()){
	//			for (EObject obj : getCrossReferencer().keySet()) {
	//				logger.debug("target Object " + obj);
	//				for (EStructuralFeature.Setting setting : getCrossReferencer().get(obj)) {
	//					logger.debug("setting " + setting.getEStructuralFeature());
	//				}
	//			}
	//			logger.debug("");
	//		}
			
			ECrossReferenceAdapter crossReferenceAdapter = 
				getRuleProcessor().getTransformationProcessor().getCrossReferenceAdapter();
	
			Collection<Setting> references = crossReferenceAdapter.getInverseReferences(startObject);
			if (references != null){
	//			logger.debug(" key obj : " + startObject);
	//			logger.debug(" setting : " + crossReferenceAdapter.getInverseReferences(startObject));
				for (EStructuralFeature.Setting setting : references) {
	//				logger.debug(" link :    " + setting.getEStructuralFeature());
	//				logger.debug(" src obj : " + setting.getEObject());
					if (setting.getEStructuralFeature().equals(edge.getTypeReference())){
						EObject crossReferencedObject = setting.getEObject();
						candidateObjectList.add(crossReferencedObject);
					}
				}
			}
		}

		return candidateObjectList;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * a) check whether the node type matches the neighbor object
	 * (the node's type class equals the object's class. The node's type class
	 * may be a superclass if so specified by the node.)
	 * b) if a) holds, call edgeBindingEditPolicy.edgeMatches() to determine
	 * whether the edge matches a given link.
	 * c) if b) holds, call nodeMatchingPolicy.nodeMatches() to determine
	 * whether the neighbor object matches the neighbor node.
	 * d) if c) holds, create an edge binding and push it on the ruleBinding stack
	 * call exploreEdges() with the candidate neighbor node and object
	 * if d) holds for any link, return true.
	 * 
	 * @generated NOT
	 */
	public boolean exploreEdge(Node startNode, EObject startObject, Edge edge, EObject neighborObject, int utilityDepthCounter) {
		
		assert(neighborObject != null);
		
		boolean isOutgoingEdge = startNode.getAllOutgoingEdges().contains(edge);
		
		Node neighborNode = getNeighborNode(startNode, edge);
		
		//--DEBUG--//
		DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForCandidateNode(neighborNode, neighborObject, this);
		
		// check whether the neighbor object is already bound to ANOTHER node. Then return false: 
		if (getRuleBinding().getNodeBinding().keySet().contains(neighborObject)
				&& !getRuleBinding().getNodeBinding().get(neighborObject).contains(neighborNode)
				){
			if (logger.isDebugEnabled())
				logger.debug(" " + getUtilityString(utilityDepthCounter) + 
					 " CANNOT MATCH node " + neighborNode + " to object " + getObjectNameString(neighborObject) +
					 " because the object is already matched to "
					 + getRuleBinding().getNodeBinding().get(neighborObject));
			return false;
		}
		
		
		
		// a) check whether the node type matches the neighbor object
		// (We assume that this has to hold despite all possible extensions of this graph matcher)
		if (!TransformationProcessorUtil.nodeTypeMatches(neighborNode, neighborObject)){ // is outgoing edge
			if (logger.isDebugEnabled())
				logger.debug(" " + getUtilityString(utilityDepthCounter) + 
					 " node " + neighborNode + "TYPE DOES NOT MATCH object " + getObjectNameString(neighborObject));
			return false;
		}

		//b) call edgeBindingEditPolicy.edgeMatches() to determine whether the edge matches a given link
		EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
		if (isOutgoingEdge){ //beware: the order is very important!
			virtualModelEdge.add(startObject);
			virtualModelEdge.add(neighborObject);
		}else{
			virtualModelEdge.add(neighborObject);
			virtualModelEdge.add(startObject);
		}
		virtualModelEdge.add(edge.getTypeReference());
		if (!edgeMatches(edge, virtualModelEdge)){
			return false;
		}

		//c) call nodeMatchingPolicy.nodeMatches() to determine whether the neighbor object matches the neighbor node.
		/*
		 * (this calls the DefaultNodeProcessingPolicy.nodeMatches, which checks that a context node matches only 
		 * already (globally) bound objects, or a produced node matches only (globally) unbound objects.)
		 */
		if (!nodeMatches(neighborNode, neighborObject)) 
			return false;
		
		//d) create an edge and node binding and push it on the ruleBinding stack
		/* 
		 * if the neighbor object is already bound by a node in the rule graph, it may be that
		 * the current neighbor node is already bound to that object: in this case, we're simply matching an edge 
		 * to a previously bound node.
		 * (The case that the object is bound by another node is already handled above)
		 */
		getRuleBinding().pushEdgeBinding(edge, virtualModelEdge);
		if (!getRuleBinding().getNodeBinding().keySet().contains(neighborObject) ){
			getRuleBinding().pushNodeBinding(neighborNode, neighborObject);
			
			//--DEBUG--//			
			if(ds!=null)((DebugSupportImpl)ds).debugSupportForMatchedNode(neighborNode, neighborObject, getRuleBinding(), false, true, this);
			
		}

		//e) call nodeMatchingPolicy.nodeConstraintsMatch() to determine whether all applicable constraints are true.
		if (!nodeConstraintsMatch(neighborNode, neighborObject)) {
			getRuleBinding().removeElementBindingsFromStack(edge);
			return false;
		}
		
		// f) call exploreEdges() with the candidate neighbor node and object.
		// If exploreEdges() returns true, return true.
		// If exploreEdges() returns false, clear the stack until the edge binding added last in this recursion.
		if (exploreEdges(neighborNode, neighborObject, utilityDepthCounter+1)){
			return true;
		}else{
			/* 
			 * if we come to this point, the edge and its neighbor node were matched successfully, but
			 * exploring the edges of the neighbor node is not successful. That means that the
			 * matching this edge with the given neighbor object ultimately fails.
			 */
			getRuleBinding().removeElementBindingsFromStack(edge);
			return false;
		}
	}
	

	/**
	 * This method returns a list of the union of the unbound edges to match next that all node matching
	 * policies provide (see {@link INodeMatchingPolicy#getUnboundEdgeToMatchNext(Node, EObject)}).
	 * 
	 * @param node
	 * @param eObject
	 * @return whether all node matching policies agree that the node matches the candidate object.
	 */
	protected EList<Edge> getUnboundEdgeToMatchNext(Node node, EObject eObject) {
		EList<Edge> resultList = new UniqueEList<Edge>();
		for (INodeMatchingPolicy nodeMatchingPolicy : getNodeMatchingPolicy()) {
			EList<Edge> edgesToMatchNext = nodeMatchingPolicy.getUnboundEdgeToMatchNext(node, eObject);
			if (edgesToMatchNext != null) resultList.addAll(edgesToMatchNext);
		}
		return resultList;
	}
	
	/**
	 * This method queries the edge matching policies whether the edge matches the candidate virtual model edge.
	 * 
	 * @param edge
	 * @param virtualModelEdge
	 * @return whether all edge matching policies agree that the edge matches the candidate virtual model edge.
	 */
	protected boolean edgeMatches(Edge edge, EList<EObject> virtualModelEdge) {
		for (IEdgeMatchingPolicy edgeMatchingPolicy : getEdgeMatchingPolicy()) {
			if (!edgeMatchingPolicy.edgeMatches(edge, virtualModelEdge)) return false;
		}
		return true;
	}
	
	/**
	 * This method queries the node matching policies whether the node matches the candidate object.
	 * 
	 * @param node
	 * @param candidateEObject
	 * @return whether all node matching policies agree that the node matches the candidate object.
	 */
	protected boolean nodeMatches(Node node, EObject candidateEObject){
		for (INodeMatchingPolicy nodeMatchingPolicy : getNodeMatchingPolicy()) {
			if (!nodeMatchingPolicy.nodeMatches(node, candidateEObject)) return false;
		}
		return true;
	}

	protected boolean nodeConstraintsMatch(Node node, EObject candidateEObject){
		for (IConstraintCheckingPolicy constraintCheckingPolicy : getConstraintCheckingPolicy()) {
			if (!constraintCheckingPolicy.evaluateConstraints(node, candidateEObject)) return false;
		}
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * This operation starts the matching process.
	 * When calling this method, it is important that an initial rule binding is set
	 * which contains at least one valid node binding (see {@link #nodeMatches(Node, EObject)}).
	 * Otherwise an {@link IllegalArgumentException} is thrown.
	 * 
	 * It also requires that the {@link #domainsToMatch} list contains the domain models 
	 * of the domains that shall be matched, e.g. the source domain models of the current active
	 * application scenario {@link Configuration#getActiveApplicationScenario()}
	 * 
	 * 
	 * This operation then brow
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean match() {
		
		//--DEBUG--//
		DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
		if(ds!=null)((DebugSupportImpl)ds).debugSupportForPatternMatchingRunning(getRuleBinding(), true, this);
		
		
		// checks whether the rule binding is set up properly
		if (!checkRuleBinding()) 
			return false;
						
		/*
		 * get one node binding from the rule binding where we can start matching, 
		 * i.e. the node should have outgoing or incoming edges to explore.
		 * Repeat that as long as there are no further nodes that need exploring.
		 */

		
		EList<Entry<Node, EObject>> nodesWithUnboundEdgeToMatchNext = null;
		alreadyTriedStartNodes = new UniqueEList<Node>();
		
		edgeExploreOperationsPerformed = 0;
		
		while(true){
			// initial node binding where matching starts or continues
			setStartNodeOfMatching(null);
			nodesWithUnboundEdgeToMatchNext = findNodeWithUnboundEdgeToMatchNext();
			if (nodesWithUnboundEdgeToMatchNext.isEmpty()) break;
			
			for (Entry<Node, EObject> nodeWithUnboundEdgeToMatchNext : nodesWithUnboundEdgeToMatchNext) {
				
				Node node = nodeWithUnboundEdgeToMatchNext.getKey();
				EObject eObject = nodeWithUnboundEdgeToMatchNext.getValue();
				setStartNodeOfMatching(node);
				
				if (!match(node, eObject)){
					logger.debug("The matching was unsuccessful from " + nodesWithUnboundEdgeToMatchNext);
					alreadyTriedStartNodes.add(node);
				}else{
					logger.debug("The matching was successful from " + nodesWithUnboundEdgeToMatchNext);
				}
				
			}
			
			
			/* 
			 * do only leave this loop when all matchable nodes are bound
			 * or there are nodes with unbound edges to match next.
			 * The purpose of this is that this way multiple unconnected reusable 
			 * patterns can be matched in a target domain and the graph matcher 
			 * does not prematurely abort when one reusable patter does not match.
			 */
			if (allMatchableNodesBound()) return true;
		}
		
		/* We have not yet found all matchable nodes. This may happen if there are
		 * unconnected context patterns.
		 * Thus, we generate a new node binding for one yet unmatched node. 
		 */
		
		//RuleBinding extendedRuleBinding = createInitialRuleBinding();
		//getRuleBinding().getNodeBinding().addAll(extendedRuleBinding.getNodeBinding());
		//return match();
		
		
		if (allMatchableNodesBound()){
			logger.debug("All matchable nodes are bound.");
			return true;
		}else{
			logger.debug("Not all matchable nodes are bound.");
			return false;
		}
	}
	
	/**
	 * Check whether all context nodes of the rule and all nodes of the source domain patterns
	 * are bound in this rule binding.
	 * @param ruleBinding
	 * @return
	 */
	private boolean allMatchableNodesBound() {
		for (Node node : getRuleBinding().getTggRule().getAllNodes()) {
			if ((node.isContext() || isNodeInDomainsToMatch(node))
					&& !getRuleBinding().getNodeToEObjectMap().keySet().contains(node))
				return false;
		}
		return true;
	}

	private EList<Node> alreadyTriedStartNodes;
	
	private EList<Entry<Node, EObject>> findNodeWithUnboundEdgeToMatchNext(){
		
		EList<Entry<Node, EObject>> nodesWithUnboundEdgeToMatchNext = new BasicEList<Entry<Node, EObject>>();
		
		for (Iterator<Entry<Node, EObject>> nodeIterator = getRuleBinding().getNodeToEObjectMap().entrySet().iterator(); nodeIterator.hasNext();) {
			Entry<Node, EObject> startNodeBinding = nodeIterator.next();
						
			Node node = startNodeBinding.getKey();
			// skip this node binding if a matching already started from here.
			if (alreadyTriedStartNodes.contains(node)) continue;
			// skip this node binding if the node is not in a domain to match.
			if (!isNodeInDomainsToMatch(node)) continue;
			
			EObject eObject = startNodeBinding.getValue();
						
			EList<Edge> unboundEdgeToMatchNextForNode = getUnboundEdgeToMatchNext(node, eObject);
			if (!unboundEdgeToMatchNextForNode.isEmpty()) {
				logger.debug(startNodeBinding);
				// (jgreen) Optimization: preferably start where a node has an outgoing edge that must be matched next 
				// (This may not be better in every transformation, but in most)
				boolean edgeIsOutgoing = false;
				for (Edge edge : unboundEdgeToMatchNextForNode) {
					if (startNodeBinding.getKey().equals(edge.eContainer())){
						edgeIsOutgoing = true;
						break;
					}
				}
				if (edgeIsOutgoing)
					nodesWithUnboundEdgeToMatchNext.add(0,startNodeBinding);
				else
					nodesWithUnboundEdgeToMatchNext.add(startNodeBinding);
			}
			
		}
		return nodesWithUnboundEdgeToMatchNext;
		
//		//FIXME: No further node to explore found in the rule binding.
//		// However, we should check for other nodes in unconnected patterns. 
//		return null;
	}
	
	private boolean isNodeInDomainsToMatch(Node node){
		for (DomainModel domainModel : getDomainsToMatch()) {
			if (domainModel.getTypedModel().equals(node.getDomainGraphPattern().getTypedModel()))
				return true;
		}
		return false;
	}
	
	
	// this field is just used to check whether that rule binding was checked already
	// the field should only be accessed in the checkRuleBinding() method!
	private RuleBinding checkedRuleBinding;
	
	/**
	 * Some general checks to make sure that the rule binding is there, 
	 * contains at least one node binding, and that there are domains to match.
	 */
	private boolean checkRuleBinding(){
		if (getRuleBinding() == checkedRuleBinding) return true;
		if (getRuleBinding() == null){
			throw new IllegalArgumentException("No rule binding specified!");
		}
		if (getRuleBinding().getNodeBinding().isEmpty()){
			throw new IllegalArgumentException("The initial rule binding contains no node binding!");
		}
		
		if (getDomainsToMatch().isEmpty())
			throw new IllegalArgumentException("The list of domains to match must not be empty.");

		for (Iterator<Entry<Node, EObject>> nodeIterator = getRuleBinding().getNodeToEObjectMap().entrySet().iterator(); nodeIterator.hasNext();) {
			Entry<Node, EObject> startNodeBinding = nodeIterator.next();
			
			Node node = startNodeBinding.getKey();
			EObject eObject = startNodeBinding.getValue();
			if (eObject == null) {
				logger.debug("A node binding in the given rule binding is not valid: The bound eobject to node " + node + " is null.");

				return false;
			}
			if (eObject.eIsProxy())
				return false; //during update, there might be unresolvable proxies due to object deletions.
			
			if (!nodeMatches(node, eObject)){
				logger.debug("A node binding in the given rule binding is not valid: " + eObject + " bound to node " + node
						+ ". A node matching policy returned false.");
				
				return false;
			}
			
			// it's important to let the constraint processors run over the 
			// rule binding before starting the matching. Otherwise, some OCL variables may not be made known.
			// Then it may not be possible to evaluate some constraints during the matching/enforcment of the rule.
			if (!nodeConstraintsMatch(node, eObject)){
				logger.debug("A node binding in the given rule binding is not valid: " + eObject + " bound to node " + node
						+ ". A constraint processing policy returned false.");
				
				return false;
			}
		}
		
		for (Entry<Edge, EList<EObject>> vme : getRuleBinding().getEdgeToVirtualModelEdgeMap()) {
			//Edge edge = vme.getKey();
			EObject startObject = vme.getValue().get(0);
			EObject targetObject = vme.getValue().get(1);
			EReference ref = (EReference) vme.getValue().get(2);

			//all this should have been checked when testing the node bindings
			assert startObject != null && !startObject.eIsProxy();
			assert targetObject != null && !targetObject.eIsProxy();
			assert ref != null;
			
			if (ref.isMany()) {
				@SuppressWarnings("unchecked")
				EList<EObject> list = (EList<EObject>) startObject.eGet(ref);
				if (!list.contains(targetObject)) {
					logger.debug("An edge binding in the given rule binding is not valid: No link exists of type " + ref.getName() + " between " + startObject + " and " + targetObject + ".");
					return false;
				}
			} else {
				if (!targetObject.equals(startObject.eGet(ref))) {
					logger.debug("An edge binding in the given rule binding is not valid: No link exists of type " + ref.getName() + " between " + startObject + " and " + targetObject + ".");
					return false;
				}
			}
		}

		
		//FIXME: this does not check global constraints!
		
		checkedRuleBinding = getRuleBinding();
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean match(Node startNode, EObject startObject) {
				
		// jgreen: Why is it necessary to do that each time we call this function? its already done in match(). Performance?
//		if (!checkRuleBinding()) 
//			return false;
		
		// The startNode must be already matched to startObject within the rule binding,
		// otherwise it wouldn't make sense to pursue the graph matching from here.
		assert(getRuleBinding().getNodeToEObjectMap().get(startNode) == startObject); 
		
//		if (getRuleBinding().getNodeToEObjectMap().get(startNode) != startObject){
//			throw new IllegalArgumentException("The startNode " + startNode + " and startObject " + startObject 
//					+ " are not a node binding in the current rule binding");
//		}
		
		if (logger.isDebugEnabled())
			logger.debug("START matching of rule " + getRuleProcessor().getTripleGraphGrammarRule().getName());
		
		
		/*
		 * The graph matching starts on the basis of a rule binding that has one or many initial node bindings
		 * (and 0..* edge bindings). We require that, when the graph is not connected, there is at least one initial
		 * node binding for each of the isolated subgraphs. Otherwise, the graph matcher may report a successful match
		 * of a rule while there are unvisited subgraphs left.
		 * 
		 * When the graph is connected, we will visit all nodes in a single recursion of exploreEdges(). When the graph 
		 * is not connected, a single run of exploreEdges() will not match all of the rule graph that we want to match.
		 * Therefore, we have to check after the successful call of exploreEdges() whether or not there are nodeBindings
		 * left in the rule binding that have unmatched, but matchable edges attached. Then, we call exploreEdges() starting
		 * at these node bindings.
		 * 
		 */
		boolean graphMatchingSuccessful = true;
		boolean nodeBindingWithUnmatchedMatchableEdgeAttachedExists = true;
		while(graphMatchingSuccessful && nodeBindingWithUnmatchedMatchableEdgeAttachedExists){
			graphMatchingSuccessful = exploreEdges(startNode, startObject, 0);
			if (!graphMatchingSuccessful) return false;
			nodeBindingWithUnmatchedMatchableEdgeAttachedExists = false;			
			for (Entry<Node, EObject> nodeBinding : getRuleBinding().getNodeToEObjectMap().entrySet()) {
				if (!getUnboundEdgeToMatchNext(nodeBinding.getKey(), nodeBinding.getValue()).isEmpty()){
					nodeBindingWithUnmatchedMatchableEdgeAttachedExists = true;
					startNode = nodeBinding.getKey();
					startObject = nodeBinding.getValue();
					break;
				}
			}
		}
			
		if (!graphMatchingSuccessful){
			return false;
		}

		for (IConstraintCheckingPolicy constraintCheckingPolicy : getConstraintCheckingPolicy()) {
			if (constraintCheckingPolicy.hasUnprocessedConstraintsLeft()){
				if (logger.isInfoEnabled())
				logger.info("There are unprocessed constraints left after completing the graph matching process: "
						+ constraintCheckingPolicy.getUnprocessableConstraints());
			}
		}

		return true;

	}

	protected String getUtilityString(int utilityDepthCounter){
		String utilityString = "    ";
		for(int i=0;i<utilityDepthCounter;i++){
			utilityString = utilityString + "| ";
		}
		return utilityString;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodeMatchingPolicy()).basicAdd(otherEnd, msgs);
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEdgeMatchingPolicy()).basicAdd(otherEnd, msgs);
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRuleProcessor((RuleProcessor)otherEnd, msgs);
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraintCheckingPolicy()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				return ((InternalEList<?>)getNodeMatchingPolicy()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				return ((InternalEList<?>)getEdgeMatchingPolicy()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				return basicSetRuleProcessor(null, msgs);
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				return ((InternalEList<?>)getConstraintCheckingPolicy()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				return eInternalContainer().eInverseRemove(this, InterpreterPackage.RULE_PROCESSOR__GRAPH_MATCHER, RuleProcessor.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__RULE_BINDING:
				if (resolve) return getRuleBinding();
				return basicGetRuleBinding();
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				return getNodeMatchingPolicy();
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				return getEdgeMatchingPolicy();
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				return getRuleProcessor();
			case InterpreterPackage.GRAPH_MATCHER__DOMAINS_TO_MATCH:
				return getDomainsToMatch();
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				return getConstraintCheckingPolicy();
			case InterpreterPackage.GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY:
				return isMatchReusablePatternOnly();
			case InterpreterPackage.GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES:
				return getElementsNotAvailableForProducedNodes();
			case InterpreterPackage.GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES:
				return getLinksNotAvailableForProducedEdges();
			case InterpreterPackage.GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING:
				return isRestrictProducedGraphMatching();
			case InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING:
				if (resolve) return getStartNodeOfMatching();
				return basicGetStartNodeOfMatching();
			case InterpreterPackage.GRAPH_MATCHER__NO_ELEMENTS_TO_MATCH_LEFT:
				return isNoElementsToMatchLeft();
			case InterpreterPackage.GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED:
				return getEdgeExploreOperationsPerformed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__RULE_BINDING:
				setRuleBinding((RuleBinding)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				getNodeMatchingPolicy().clear();
				getNodeMatchingPolicy().addAll((Collection<? extends INodeMatchingPolicy>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				getEdgeMatchingPolicy().clear();
				getEdgeMatchingPolicy().addAll((Collection<? extends IEdgeMatchingPolicy>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__DOMAINS_TO_MATCH:
				getDomainsToMatch().clear();
				getDomainsToMatch().addAll((Collection<? extends DomainModel>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				getConstraintCheckingPolicy().clear();
				getConstraintCheckingPolicy().addAll((Collection<? extends IConstraintCheckingPolicy>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY:
				setMatchReusablePatternOnly((Boolean)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES:
				getElementsNotAvailableForProducedNodes().clear();
				getElementsNotAvailableForProducedNodes().addAll((Collection<? extends EObject>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES:
				getLinksNotAvailableForProducedEdges().clear();
				getLinksNotAvailableForProducedEdges().addAll((Collection<? extends EList<EObject>>)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING:
				setRestrictProducedGraphMatching((Boolean)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING:
				setStartNodeOfMatching((Node)newValue);
				return;
			case InterpreterPackage.GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED:
				setEdgeExploreOperationsPerformed((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__RULE_BINDING:
				setRuleBinding((RuleBinding)null);
				return;
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				getNodeMatchingPolicy().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				getEdgeMatchingPolicy().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)null);
				return;
			case InterpreterPackage.GRAPH_MATCHER__DOMAINS_TO_MATCH:
				getDomainsToMatch().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				getConstraintCheckingPolicy().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY:
				setMatchReusablePatternOnly(MATCH_REUSABLE_PATTERN_ONLY_EDEFAULT);
				return;
			case InterpreterPackage.GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES:
				getElementsNotAvailableForProducedNodes().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES:
				getLinksNotAvailableForProducedEdges().clear();
				return;
			case InterpreterPackage.GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING:
				setRestrictProducedGraphMatching(RESTRICT_PRODUCED_GRAPH_MATCHING_EDEFAULT);
				return;
			case InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING:
				setStartNodeOfMatching((Node)null);
				return;
			case InterpreterPackage.GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED:
				setEdgeExploreOperationsPerformed(EDGE_EXPLORE_OPERATIONS_PERFORMED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * 
	 * @return Value of the 'name' attribute if it has one that is not null. Returns eObject otherwise
	 */
	protected String getObjectNameString(EObject eObject){
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if (eAttribute.getName().equals("name")
					&& eObject.eGet(eAttribute) != null
					&& eObject.eGet(eAttribute) instanceof String
					){
				return (String) eObject.eGet(eAttribute)+ ":" + eObject.eClass().getName();
			}
		}
		return eObject.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.GRAPH_MATCHER__RULE_BINDING:
				return ruleBinding != null;
			case InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY:
				return nodeMatchingPolicy != null && !nodeMatchingPolicy.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__EDGE_MATCHING_POLICY:
				return edgeMatchingPolicy != null && !edgeMatchingPolicy.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__RULE_PROCESSOR:
				return getRuleProcessor() != null;
			case InterpreterPackage.GRAPH_MATCHER__DOMAINS_TO_MATCH:
				return domainsToMatch != null && !domainsToMatch.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY:
				return constraintCheckingPolicy != null && !constraintCheckingPolicy.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY:
				return matchReusablePatternOnly != MATCH_REUSABLE_PATTERN_ONLY_EDEFAULT;
			case InterpreterPackage.GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES:
				return elementsNotAvailableForProducedNodes != null && !elementsNotAvailableForProducedNodes.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES:
				return linksNotAvailableForProducedEdges != null && !linksNotAvailableForProducedEdges.isEmpty();
			case InterpreterPackage.GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING:
				return restrictProducedGraphMatching != RESTRICT_PRODUCED_GRAPH_MATCHING_EDEFAULT;
			case InterpreterPackage.GRAPH_MATCHER__START_NODE_OF_MATCHING:
				return startNodeOfMatching != null;
			case InterpreterPackage.GRAPH_MATCHER__NO_ELEMENTS_TO_MATCH_LEFT:
				return isNoElementsToMatchLeft() != NO_ELEMENTS_TO_MATCH_LEFT_EDEFAULT;
			case InterpreterPackage.GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED:
				return edgeExploreOperationsPerformed != EDGE_EXPLORE_OPERATIONS_PERFORMED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (matchReusablePatternOnly: ");
		result.append(matchReusablePatternOnly);
		result.append(", linksNotAvailableForProducedEdges: ");
		result.append(linksNotAvailableForProducedEdges);
		result.append(", restrictProducedGraphMatching: ");
		result.append(restrictProducedGraphMatching);
		result.append(", edgeExploreOperationsPerformed: ");
		result.append(edgeExploreOperationsPerformed);
		result.append(')');
		return result.toString();
	}

	
	//TODO: Make this nice, as this is a quick hack.
	public class PartialMatchingTreeVertex {
		public PartialMatchingTreeVertex(PartialMatchingTreeVertex parent,
				Node boundNode, EObject boundElement, Edge boundEdge,
				EList<EObject> boundLink, int recursionLevel) {
			super();
			this.parent = parent;
			this.boundNode = boundNode;
			this.boundElement = boundElement;
			this.boundEdge = boundEdge;
			this.boundLink = boundLink;
			this.recursionLevel = recursionLevel;
			
			if (parent != null) {
				parent.children.add(this);
				depth = parent.getDepth() + 1;
			} else
				depth = 0;
		}

		PartialMatchingTreeVertex parent;
		Node boundNode;
		EObject boundElement;
		Edge boundEdge;
		EList<EObject> boundLink;
		int depth;
		int recursionLevel;
		
		RuleBinding cachedBinding = null;
		List<PartialMatchingTreeVertex> children = new ArrayList<PartialMatchingTreeVertex>();
		
		public RuleBinding getBinding() {
			if (cachedBinding == null) {
				RuleBinding parentBinding = parent.getBinding();
				cachedBinding = InterpreterconfigurationFactory.eINSTANCE.createRuleBinding();
				for (Map.Entry<Node, EObject> binding : parentBinding.getNodeToEObjectMap()){
					cachedBinding.pushNodeBinding(binding.getKey(), binding.getValue());
				}
				cachedBinding.pushNodeBinding(boundNode, boundElement);
				for (Map.Entry<Edge, EList<EObject>> binding : parentBinding.getEdgeToVirtualModelEdgeMap()){
					cachedBinding.pushEdgeBinding(binding.getKey(), binding.getValue());
				}
				cachedBinding.pushEdgeBinding(boundEdge, boundLink);
			}
			return cachedBinding;
		}
		
		public int getDepth() {
			return depth;
		}
	};
	public class PartialMatchingTreeRootVertex extends PartialMatchingTreeVertex {
		public PartialMatchingTreeRootVertex(RuleBinding rootBinding) {
			super(null, null, null, null, null, 0);
			this.rootBinding = rootBinding;
		}

		RuleBinding rootBinding;

		@Override
		public RuleBinding getBinding() {
			return rootBinding;
		}
	};
	
	static class MatchingPosition {
		public MatchingPosition(PartialMatchingTreeVertex treeNode,
				Node nodeToContinuePatternMatchingFrom) {
			super();
			this.treeVertex = treeNode;
			this.nodeToContinuePatternMatchingFrom = nodeToContinuePatternMatchingFrom;
		}
		PartialMatchingTreeVertex treeVertex;
		Node nodeToContinuePatternMatchingFrom;
	}

	public PartialMatchingTreeRootVertex reuseDeletedElementsForEnforceableDomains(RuleBinding ruleBinding, Map<EObject, Node> deletedObjects) {
		
		if (deletedObjects.isEmpty())
			return null;
		
		Deque<MatchingPosition> worklist = new ArrayDeque<MatchingPosition>();
		
		PartialMatchingTreeRootVertex treeRoot = new PartialMatchingTreeRootVertex(ruleBinding);
		for (Node currentNode : ruleBinding.getNodeToEObjectMap().keySet()) {
			//Start from every node in the context binding to ensure unconnected graph patterns are found, too.
			worklist.add(new MatchingPosition(treeRoot, currentNode));
		}
		
		while (!worklist.isEmpty()) {
			MatchingPosition currentMatchingPosition = worklist.pop();
			//System.out.println("Now matching: Node " + currentMatchingPosition.nodeToContinuePatternMatchingFrom + ", Tree Depth: " + currentMatchingPosition.treeVertex.getDepth() + ", Recursion Level: "+ currentMatchingPosition.treeVertex.recursionLevel);
			
			EObject currentNodesObject = currentMatchingPosition.treeVertex.getBinding().getNodeToEObjectMap().get(currentMatchingPosition.nodeToContinuePatternMatchingFrom);
			List<Edge> allEdges = new ArrayList<Edge>();
			allEdges.addAll(currentMatchingPosition.nodeToContinuePatternMatchingFrom.getAllOutgoingEdges());
			allEdges.addAll(currentMatchingPosition.nodeToContinuePatternMatchingFrom.getAllIncomingEdges());
			boolean hasSteppedIn = false;
			for (Edge nextEdgeToMatch : allEdges){
				boolean isOutgoingEdge = nextEdgeToMatch.getSourceNode().equals(currentMatchingPosition.nodeToContinuePatternMatchingFrom);
				Node nextNodeToMatch = isOutgoingEdge?nextEdgeToMatch.getTargetNode():nextEdgeToMatch.getSourceNode();
				if (!currentMatchingPosition.treeVertex.getBinding().getNodeToEObjectMap().containsKey(nextNodeToMatch)) { //yet unbound node, possible next matching step
					//gather candidates
					EList<EObject> candidateObjectList = getCandidateNeighborObjects(currentMatchingPosition.nodeToContinuePatternMatchingFrom, currentNodesObject, nextEdgeToMatch);

					//check every candidate and add a new child vertex for fitting ones
					for (EObject targetElement : candidateObjectList) {
						if (deletedObjects.containsKey(targetElement) //is really marked for deletion?
								&& TransformationProcessorUtil.nodeTypeMatches(nextNodeToMatch, targetElement)){
							if (nodeMatches(nextNodeToMatch, targetElement)) {
								EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
								if (isOutgoingEdge){ //beware: the order is very important!
									virtualModelEdge.add(currentNodesObject);
									virtualModelEdge.add(targetElement);
								}else{
									virtualModelEdge.add(targetElement);
									virtualModelEdge.add(currentNodesObject);
								}
								virtualModelEdge.add(nextEdgeToMatch.getTypeReference());
								if (edgeMatches(nextEdgeToMatch, virtualModelEdge)){
									//everything is fine and matches
									PartialMatchingTreeVertex newVertex = new PartialMatchingTreeVertex(currentMatchingPosition.treeVertex, nextNodeToMatch, targetElement, nextEdgeToMatch, virtualModelEdge, currentMatchingPosition.treeVertex.recursionLevel+1);
									worklist.push(new MatchingPosition(newVertex, nextNodeToMatch));
									hasSteppedIn = true; //we will step one recursion level deeper in the pattern matching
								}
							}
						}
					}
				}
				if (hasSteppedIn)
					break; //there might be remaining edges to be matched, but they will be matched later when recursing up
			}
			if (!hasSteppedIn) {
				//We didn't find a way to step deeper into the pattern matching. 
				//That's probably because we have matched a whole sub-pattern.
				//--> Step out one recursion level.
				//We cannot simply use the parent vertex, because this might also be a vertex for stepping out one recursion level.
				//So we have to search the tree upwards until we find our current node.
				
				PartialMatchingTreeVertex vertexToPutChildIn = currentMatchingPosition.treeVertex;
				while (vertexToPutChildIn.parent != null && vertexToPutChildIn.parent.parent != null 
						&& vertexToPutChildIn.recursionLevel >= currentMatchingPosition.treeVertex.recursionLevel) {
					vertexToPutChildIn = vertexToPutChildIn.parent; 
				}
				
				if (vertexToPutChildIn.parent != null && vertexToPutChildIn.parent.parent != null ) {
					PartialMatchingTreeVertex newVertex = new PartialMatchingTreeVertex(currentMatchingPosition.treeVertex, vertexToPutChildIn.parent.boundNode, vertexToPutChildIn.parent.boundElement, vertexToPutChildIn.parent.boundEdge, vertexToPutChildIn.parent.boundLink, currentMatchingPosition.treeVertex.recursionLevel-1);
					worklist.push(new MatchingPosition(newVertex, vertexToPutChildIn.parent.boundNode));
				}//otherwise we are finished with the matching
			}
		}
		return treeRoot;
	}	
	
} //GraphMatcherImpl
