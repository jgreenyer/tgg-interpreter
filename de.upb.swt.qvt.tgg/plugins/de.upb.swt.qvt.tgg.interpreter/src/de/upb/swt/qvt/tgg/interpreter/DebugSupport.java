/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Debug Support</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isTerminate <em>Terminate</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isDebug <em>Debug</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isRunning <em>Running</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#getDebugEventListener <em>Debug Event Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDebugSupport()
 * @model
 * @generated
 */
public interface DebugSupport extends IDebugEventListener {
	/**
	 * Returns the value of the '<em><b>Terminate</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminate</em>' attribute.
	 * @see #setTerminate(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDebugSupport_Terminate()
	 * @model default="false"
	 * @generated
	 */
	boolean isTerminate();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isTerminate <em>Terminate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Terminate</em>' attribute.
	 * @see #isTerminate()
	 * @generated
	 */
	void setTerminate(boolean value);

	/**
	 * Returns the value of the '<em><b>Debug</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug</em>' attribute.
	 * @see #setDebug(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDebugSupport_Debug()
	 * @model default="false"
	 * @generated
	 */
	boolean isDebug();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isDebug <em>Debug</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug</em>' attribute.
	 * @see #isDebug()
	 * @generated
	 */
	void setDebug(boolean value);

	/**
	 * Returns the value of the '<em><b>Running</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Running</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Running</em>' attribute.
	 * @see #setRunning(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDebugSupport_Running()
	 * @model
	 * @generated
	 */
	boolean isRunning();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isRunning <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Running</em>' attribute.
	 * @see #isRunning()
	 * @generated
	 */
	void setRunning(boolean value);

	/**
	 * Returns the value of the '<em><b>Debug Event Listener</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.IDebugEventListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug Event Listener</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug Event Listener</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDebugSupport_DebugEventListener()
	 * @model
	 * @generated
	 */
	EList<IDebugEventListener> getDebugEventListener();

} // DebugSupport
