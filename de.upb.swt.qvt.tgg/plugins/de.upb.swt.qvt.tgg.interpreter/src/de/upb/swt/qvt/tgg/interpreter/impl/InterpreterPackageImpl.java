/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.query.conditions.Condition;
import org.eclipse.ocl.ecore.EcoreEnvironment;

import de.upb.swt.qvt.tgg.TggPackage;
import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterPackageImpl extends EPackageImpl implements InterpreterPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interpreterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass frontManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iNodeMatchingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iEdgeMatchingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass frontTransformationProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphMatcherEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseNodeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseEdgeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConstraintCheckingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseConstraintProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iNodeEnforcementPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iEdgeEnforcementPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConstraintEnforcementPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultNodeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultEdgeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultOCLConstraintProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeToConstraintMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iMatchingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationProcessorHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass debugSupportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iDebugEventListenerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType statusEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eValueClashExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iProgressMonitorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iteratorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType conditionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType collectionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType settingEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType constraintUnprocessableExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eCrossReferenceAdapterEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType oclEnvironmentEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType oclEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InterpreterPackageImpl() {
		super(eNS_URI, InterpreterFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InterpreterPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InterpreterPackage init() {
		if (isInited) return (InterpreterPackage)EPackage.Registry.INSTANCE.getEPackage(InterpreterPackage.eNS_URI);

		// Obtain or create and register package
		InterpreterPackageImpl theInterpreterPackage = (InterpreterPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InterpreterPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InterpreterPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		InterpreterconfigurationPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theInterpreterPackage.createPackageContents();

		// Initialize created meta-data
		theInterpreterPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInterpreterPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InterpreterPackage.eNS_URI, theInterpreterPackage);
		return theInterpreterPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterpreter() {
		return interpreterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterpreter_Configuration() {
		return (EReference)interpreterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterpreter_TransformationProcessor() {
		return (EReference)interpreterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterpreter_OclEnvironment() {
		return (EAttribute)interpreterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterpreter_DebugSupport() {
		return (EReference)interpreterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterpreter_Ocl() {
		return (EAttribute)interpreterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFrontManager() {
		return frontManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontManager_FrontTransformationProcessor() {
		return (EReference)frontManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontManager_NodeBinding() {
		return (EReference)frontManagerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleProcessor() {
		return ruleProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_TripleGraphGrammarRule() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_TransformationProcessor() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_GraphMatcher() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_NodeEnforcementPolicy() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_EdgeEnforcementPolicy() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleProcessor_ConstraintEnforcementPolicy() {
		return (EReference)ruleProcessorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformationProcessor() {
		return transformationProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessor_GlobalEdgeBinding() {
		return (EReference)transformationProcessorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessor_GlobalNodeBinding() {
		return (EReference)transformationProcessorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessor_RuleProcessor() {
		return (EReference)transformationProcessorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessor_Interpreter() {
		return (EReference)transformationProcessorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_ProgressMonitor() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_CrossReferenceAdapter() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessor_Helper() {
		return (EReference)transformationProcessorEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_RulesApplied() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_RulesRevoked() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_RulesStillValid() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_DeletedNodeBindings() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_DeletedEdgeBindings() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_UnsuccessfulRuleApplicationAttempts() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() {
		return (EAttribute)transformationProcessorEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getINodeMatchingPolicy() {
		return iNodeMatchingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getINodeMatchingPolicy_GraphMatcher() {
		return (EReference)iNodeMatchingPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIEdgeMatchingPolicy() {
		return iEdgeMatchingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIEdgeMatchingPolicy_GraphMatcher() {
		return (EReference)iEdgeMatchingPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFrontTransformationProcessor() {
		return frontTransformationProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontTransformationProcessor_FrontManager() {
		return (EReference)frontTransformationProcessorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontTransformationProcessor_SourceDomainObjects() {
		return (EReference)frontTransformationProcessorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontTransformationProcessor_CurrentTGGRule() {
		return (EReference)frontTransformationProcessorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrontTransformationProcessor_UnmatchedSourceDomainObjects() {
		return (EReference)frontTransformationProcessorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphMatcher() {
		return graphMatcherEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_RuleBinding() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_NodeMatchingPolicy() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_EdgeMatchingPolicy() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_RuleProcessor() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_DomainsToMatch() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_ConstraintCheckingPolicy() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphMatcher_MatchReusablePatternOnly() {
		return (EAttribute)graphMatcherEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_ElementsNotAvailableForProducedNodes() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphMatcher_LinksNotAvailableForProducedEdges() {
		return (EAttribute)graphMatcherEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphMatcher_RestrictProducedGraphMatching() {
		return (EAttribute)graphMatcherEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphMatcher_StartNodeOfMatching() {
		return (EReference)graphMatcherEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphMatcher_NoElementsToMatchLeft() {
		return (EAttribute)graphMatcherEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphMatcher_EdgeExploreOperationsPerformed() {
		return (EAttribute)graphMatcherEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseNodeProcessingPolicy() {
		return baseNodeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseEdgeProcessingPolicy() {
		return baseEdgeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConstraintCheckingPolicy() {
		return iConstraintCheckingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConstraintCheckingPolicy_GraphMatcher() {
		return (EReference)iConstraintCheckingPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIConstraintCheckingPolicy_UnprocessableConstraints() {
		return (EAttribute)iConstraintCheckingPolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConstraintCheckingPolicy_UnprocessedConstraints() {
		return (EReference)iConstraintCheckingPolicyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConstraintCheckingPolicy_ProcessedConstraints() {
		return (EReference)iConstraintCheckingPolicyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConstraintCheckingPolicy_RuleBinding() {
		return (EReference)iConstraintCheckingPolicyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseConstraintProcessingPolicy() {
		return baseConstraintProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getINodeEnforcementPolicy() {
		return iNodeEnforcementPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIEdgeEnforcementPolicy() {
		return iEdgeEnforcementPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConstraintEnforcementPolicy() {
		return iConstraintEnforcementPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultNodeProcessingPolicy() {
		return defaultNodeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultEdgeProcessingPolicy() {
		return defaultEdgeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultOCLConstraintProcessingPolicy() {
		return defaultOCLConstraintProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeToConstraintMapEntry() {
		return nodeToConstraintMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeToConstraintMapEntry_Value() {
		return (EReference)nodeToConstraintMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeToConstraintMapEntry_Key() {
		return (EReference)nodeToConstraintMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIMatchingPolicy() {
		return iMatchingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIMatchingPolicy_TransformationProcessor() {
		return (EReference)iMatchingPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIMatchingPolicy_RuleProcessor() {
		return (EReference)iMatchingPolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformationProcessorHelper() {
		return transformationProcessorHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessorHelper_TransformationProcessor() {
		return (EReference)transformationProcessorHelperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationProcessorHelper_SortedConcreteRuleList() {
		return (EReference)transformationProcessorHelperEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDebugSupport() {
		return debugSupportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDebugSupport_Terminate() {
		return (EAttribute)debugSupportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDebugSupport_Debug() {
		return (EAttribute)debugSupportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDebugSupport_Running() {
		return (EAttribute)debugSupportEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDebugSupport_DebugEventListener() {
		return (EReference)debugSupportEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIDebugEventListener() {
		return iDebugEventListenerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getStatus() {
		return statusEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEValueClashException() {
		return eValueClashExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIProgressMonitor() {
		return iProgressMonitorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIterator() {
		return iteratorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCondition() {
		return conditionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCollection() {
		return collectionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSetting() {
		return settingEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getConstraintUnprocessableException() {
		return constraintUnprocessableExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getECrossReferenceAdapter() {
		return eCrossReferenceAdapterEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOCLEnvironment() {
		return oclEnvironmentEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOCL() {
		return oclEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterFactory getInterpreterFactory() {
		return (InterpreterFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		interpreterEClass = createEClass(INTERPRETER);
		createEReference(interpreterEClass, INTERPRETER__CONFIGURATION);
		createEReference(interpreterEClass, INTERPRETER__TRANSFORMATION_PROCESSOR);
		createEAttribute(interpreterEClass, INTERPRETER__OCL_ENVIRONMENT);
		createEReference(interpreterEClass, INTERPRETER__DEBUG_SUPPORT);
		createEAttribute(interpreterEClass, INTERPRETER__OCL);

		frontManagerEClass = createEClass(FRONT_MANAGER);
		createEReference(frontManagerEClass, FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR);
		createEReference(frontManagerEClass, FRONT_MANAGER__NODE_BINDING);

		ruleProcessorEClass = createEClass(RULE_PROCESSOR);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__TRANSFORMATION_PROCESSOR);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__GRAPH_MATCHER);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY);
		createEReference(ruleProcessorEClass, RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY);

		transformationProcessorEClass = createEClass(TRANSFORMATION_PROCESSOR);
		createEReference(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING);
		createEReference(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING);
		createEReference(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__RULE_PROCESSOR);
		createEReference(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__INTERPRETER);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER);
		createEReference(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__HELPER);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__RULES_APPLIED);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__RULES_REVOKED);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__RULES_STILL_VALID);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS);
		createEAttribute(transformationProcessorEClass, TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS);

		iNodeMatchingPolicyEClass = createEClass(INODE_MATCHING_POLICY);
		createEReference(iNodeMatchingPolicyEClass, INODE_MATCHING_POLICY__GRAPH_MATCHER);

		iEdgeMatchingPolicyEClass = createEClass(IEDGE_MATCHING_POLICY);
		createEReference(iEdgeMatchingPolicyEClass, IEDGE_MATCHING_POLICY__GRAPH_MATCHER);

		frontTransformationProcessorEClass = createEClass(FRONT_TRANSFORMATION_PROCESSOR);
		createEReference(frontTransformationProcessorEClass, FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER);
		createEReference(frontTransformationProcessorEClass, FRONT_TRANSFORMATION_PROCESSOR__SOURCE_DOMAIN_OBJECTS);
		createEReference(frontTransformationProcessorEClass, FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE);
		createEReference(frontTransformationProcessorEClass, FRONT_TRANSFORMATION_PROCESSOR__UNMATCHED_SOURCE_DOMAIN_OBJECTS);

		graphMatcherEClass = createEClass(GRAPH_MATCHER);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__RULE_BINDING);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__NODE_MATCHING_POLICY);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__EDGE_MATCHING_POLICY);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__RULE_PROCESSOR);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__DOMAINS_TO_MATCH);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY);
		createEAttribute(graphMatcherEClass, GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES);
		createEAttribute(graphMatcherEClass, GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES);
		createEAttribute(graphMatcherEClass, GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING);
		createEReference(graphMatcherEClass, GRAPH_MATCHER__START_NODE_OF_MATCHING);
		createEAttribute(graphMatcherEClass, GRAPH_MATCHER__NO_ELEMENTS_TO_MATCH_LEFT);
		createEAttribute(graphMatcherEClass, GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED);

		baseNodeProcessingPolicyEClass = createEClass(BASE_NODE_PROCESSING_POLICY);

		baseEdgeProcessingPolicyEClass = createEClass(BASE_EDGE_PROCESSING_POLICY);

		iConstraintCheckingPolicyEClass = createEClass(ICONSTRAINT_CHECKING_POLICY);
		createEReference(iConstraintCheckingPolicyEClass, ICONSTRAINT_CHECKING_POLICY__GRAPH_MATCHER);
		createEAttribute(iConstraintCheckingPolicyEClass, ICONSTRAINT_CHECKING_POLICY__UNPROCESSABLE_CONSTRAINTS);
		createEReference(iConstraintCheckingPolicyEClass, ICONSTRAINT_CHECKING_POLICY__UNPROCESSED_CONSTRAINTS);
		createEReference(iConstraintCheckingPolicyEClass, ICONSTRAINT_CHECKING_POLICY__PROCESSED_CONSTRAINTS);
		createEReference(iConstraintCheckingPolicyEClass, ICONSTRAINT_CHECKING_POLICY__RULE_BINDING);

		baseConstraintProcessingPolicyEClass = createEClass(BASE_CONSTRAINT_PROCESSING_POLICY);

		iNodeEnforcementPolicyEClass = createEClass(INODE_ENFORCEMENT_POLICY);

		iEdgeEnforcementPolicyEClass = createEClass(IEDGE_ENFORCEMENT_POLICY);

		iConstraintEnforcementPolicyEClass = createEClass(ICONSTRAINT_ENFORCEMENT_POLICY);

		defaultNodeProcessingPolicyEClass = createEClass(DEFAULT_NODE_PROCESSING_POLICY);

		defaultEdgeProcessingPolicyEClass = createEClass(DEFAULT_EDGE_PROCESSING_POLICY);

		defaultOCLConstraintProcessingPolicyEClass = createEClass(DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY);

		nodeToConstraintMapEntryEClass = createEClass(NODE_TO_CONSTRAINT_MAP_ENTRY);
		createEReference(nodeToConstraintMapEntryEClass, NODE_TO_CONSTRAINT_MAP_ENTRY__VALUE);
		createEReference(nodeToConstraintMapEntryEClass, NODE_TO_CONSTRAINT_MAP_ENTRY__KEY);

		iMatchingPolicyEClass = createEClass(IMATCHING_POLICY);
		createEReference(iMatchingPolicyEClass, IMATCHING_POLICY__TRANSFORMATION_PROCESSOR);
		createEReference(iMatchingPolicyEClass, IMATCHING_POLICY__RULE_PROCESSOR);

		transformationProcessorHelperEClass = createEClass(TRANSFORMATION_PROCESSOR_HELPER);
		createEReference(transformationProcessorHelperEClass, TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR);
		createEReference(transformationProcessorHelperEClass, TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST);

		debugSupportEClass = createEClass(DEBUG_SUPPORT);
		createEAttribute(debugSupportEClass, DEBUG_SUPPORT__TERMINATE);
		createEAttribute(debugSupportEClass, DEBUG_SUPPORT__DEBUG);
		createEAttribute(debugSupportEClass, DEBUG_SUPPORT__RUNNING);
		createEReference(debugSupportEClass, DEBUG_SUPPORT__DEBUG_EVENT_LISTENER);

		iDebugEventListenerEClass = createEClass(IDEBUG_EVENT_LISTENER);

		// Create data types
		statusEDataType = createEDataType(STATUS);
		eValueClashExceptionEDataType = createEDataType(EVALUE_CLASH_EXCEPTION);
		iProgressMonitorEDataType = createEDataType(IPROGRESS_MONITOR);
		iteratorEDataType = createEDataType(ITERATOR);
		conditionEDataType = createEDataType(CONDITION);
		collectionEDataType = createEDataType(COLLECTION);
		settingEDataType = createEDataType(SETTING);
		constraintUnprocessableExceptionEDataType = createEDataType(CONSTRAINT_UNPROCESSABLE_EXCEPTION);
		eCrossReferenceAdapterEDataType = createEDataType(ECROSS_REFERENCE_ADAPTER);
		oclEnvironmentEDataType = createEDataType(OCL_ENVIRONMENT);
		oclEDataType = createEDataType(OCL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InterpreterconfigurationPackage theInterpreterconfigurationPackage = (InterpreterconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(InterpreterconfigurationPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		TggPackage theTggPackage = (TggPackage)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI);

		// Create type parameters
		addETypeParameter(iteratorEDataType, "E");
		addETypeParameter(collectionEDataType, "E");

		// Set bounds for type parameters

		// Add supertypes to classes
		iNodeMatchingPolicyEClass.getESuperTypes().add(this.getIMatchingPolicy());
		iEdgeMatchingPolicyEClass.getESuperTypes().add(this.getIMatchingPolicy());
		frontTransformationProcessorEClass.getESuperTypes().add(this.getTransformationProcessor());
		baseNodeProcessingPolicyEClass.getESuperTypes().add(this.getINodeMatchingPolicy());
		baseNodeProcessingPolicyEClass.getESuperTypes().add(this.getINodeEnforcementPolicy());
		baseEdgeProcessingPolicyEClass.getESuperTypes().add(this.getIEdgeEnforcementPolicy());
		baseEdgeProcessingPolicyEClass.getESuperTypes().add(this.getIEdgeMatchingPolicy());
		baseConstraintProcessingPolicyEClass.getESuperTypes().add(this.getIConstraintEnforcementPolicy());
		iConstraintEnforcementPolicyEClass.getESuperTypes().add(this.getIConstraintCheckingPolicy());
		defaultNodeProcessingPolicyEClass.getESuperTypes().add(this.getBaseNodeProcessingPolicy());
		defaultEdgeProcessingPolicyEClass.getESuperTypes().add(this.getBaseEdgeProcessingPolicy());
		defaultOCLConstraintProcessingPolicyEClass.getESuperTypes().add(this.getBaseConstraintProcessingPolicy());
		debugSupportEClass.getESuperTypes().add(this.getIDebugEventListener());

		// Initialize classes and features; add operations and parameters
		initEClass(interpreterEClass, Interpreter.class, "Interpreter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterpreter_Configuration(), theInterpreterconfigurationPackage.getConfiguration(), null, "configuration", null, 1, 1, Interpreter.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterpreter_TransformationProcessor(), this.getTransformationProcessor(), this.getTransformationProcessor_Interpreter(), "transformationProcessor", null, 1, 1, Interpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterpreter_OclEnvironment(), this.getOCLEnvironment(), "oclEnvironment", null, 0, 1, Interpreter.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterpreter_DebugSupport(), this.getDebugSupport(), null, "debugSupport", null, 0, 1, Interpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterpreter_Ocl(), this.getOCL(), "ocl", null, 0, 1, Interpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(interpreterEClass, null, "initializeConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(interpreterEClass, null, "storeConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(interpreterEClass, this.getStatus(), "performTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(interpreterEClass, null, "storeResult", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(interpreterEClass, theEcorePackage.getEBoolean(), "isTransformationRunning", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(frontManagerEClass, FrontManager.class, "FrontManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFrontManager_FrontTransformationProcessor(), this.getFrontTransformationProcessor(), this.getFrontTransformationProcessor_FrontManager(), "frontTransformationProcessor", null, 1, 1, FrontManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFrontManager_NodeBinding(), theInterpreterconfigurationPackage.getEObjectToNodeMapEntry(), null, "nodeBinding", null, 0, -1, FrontManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(frontManagerEClass, theEcorePackage.getEBoolean(), "update", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ruleProcessorEClass, RuleProcessor.class, "RuleProcessor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleProcessor_TripleGraphGrammarRule(), theTggPackage.getTripleGraphGrammarRule(), null, "tripleGraphGrammarRule", null, 1, 1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleProcessor_TransformationProcessor(), this.getTransformationProcessor(), this.getTransformationProcessor_RuleProcessor(), "transformationProcessor", null, 1, 1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleProcessor_GraphMatcher(), this.getGraphMatcher(), this.getGraphMatcher_RuleProcessor(), "graphMatcher", null, 1, 1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleProcessor_NodeEnforcementPolicy(), this.getINodeEnforcementPolicy(), null, "nodeEnforcementPolicy", null, 0, -1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleProcessor_EdgeEnforcementPolicy(), this.getIEdgeEnforcementPolicy(), null, "edgeEnforcementPolicy", null, 0, -1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleProcessor_ConstraintEnforcementPolicy(), this.getIConstraintEnforcementPolicy(), null, "constraintEnforcementPolicy", null, 0, -1, RuleProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ruleProcessorEClass, theEcorePackage.getEBoolean(), "processRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "initialRuleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ruleProcessorEClass, theEcorePackage.getEBoolean(), "repairRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(transformationProcessorEClass, TransformationProcessor.class, "TransformationProcessor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransformationProcessor_GlobalEdgeBinding(), theInterpreterconfigurationPackage.getVirtualModelEdgeToEdgeMapEntry(), null, "globalEdgeBinding", null, 0, -1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationProcessor_GlobalNodeBinding(), theInterpreterconfigurationPackage.getEObjectToNodeMapEntry(), null, "globalNodeBinding", null, 0, -1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationProcessor_RuleProcessor(), this.getRuleProcessor(), this.getRuleProcessor_TransformationProcessor(), "ruleProcessor", null, 1, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationProcessor_Interpreter(), this.getInterpreter(), this.getInterpreter_TransformationProcessor(), "interpreter", null, 1, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationProcessor_ProgressMonitor(), this.getIProgressMonitor(), "progressMonitor", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationProcessor_CrossReferenceAdapter(), this.getECrossReferenceAdapter(), "crossReferenceAdapter", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationProcessor_Helper(), this.getTransformationProcessorHelper(), this.getTransformationProcessorHelper_TransformationProcessor(), "helper", null, 1, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEMap());
		EGenericType g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEIntegerObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTransformationProcessor_RulesApplied(), g1, "rulesApplied", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEIntegerObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTransformationProcessor_RulesRevoked(), g1, "rulesRevoked", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEIntegerObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTransformationProcessor_RulesStillValid(), g1, "rulesStillValid", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theTggPackage.getNode());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTransformationProcessor_DeletedNodeBindings(), g1, "deletedNodeBindings", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theEcorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(theEcorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		g2 = createEGenericType(theTggPackage.getEdge());
		g1.getETypeArguments().add(g2);
		initEAttribute(getTransformationProcessor_DeletedEdgeBindings(), g1, "deletedEdgeBindings", null, 0, 1, TransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationProcessor_UnsuccessfulRuleApplicationAttempts(), theEcorePackage.getEInt(), "unsuccessfulRuleApplicationAttempts", null, 0, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(), theEcorePackage.getEInt(), "edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications", null, 0, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(), theEcorePackage.getEInt(), "edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications", null, 0, 1, TransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(transformationProcessorEClass, this.getStatus(), "performTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorEClass, null, "ruleApplicationPostProcessing", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationProcessorEClass, theEcorePackage.getEBoolean(), "rulesApplicable", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationProcessorEClass, theInterpreterconfigurationPackage.getRuleBinding(), "createInitialRuleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationProcessorEClass, this.getStatus(), "transformationSuccessful", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorEClass, null, "ruleApplicationUnsuccessfulPostProcessing", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorEClass, null, "ruleRepairUnsuccessful", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorEClass, theEcorePackage.getEInt(), "countRuleApplication", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEMap());
		g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEIntegerObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleMap", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "rule", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "count", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationProcessorEClass, theEcorePackage.getEBoolean(), "usePartiallyReusablePatternSearch", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationProcessorEClass, this.getStatus(), "calculatePossiblyChangedObjects", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(iNodeMatchingPolicyEClass, INodeMatchingPolicy.class, "INodeMatchingPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getINodeMatchingPolicy_GraphMatcher(), this.getGraphMatcher(), this.getGraphMatcher_NodeMatchingPolicy(), "graphMatcher", null, 1, 1, INodeMatchingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(iNodeMatchingPolicyEClass, theEcorePackage.getEBoolean(), "nodeMatches", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "candidateEObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iNodeMatchingPolicyEClass, null, "getUnboundEdgeToMatchNext", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theTggPackage.getEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = addEOperation(iNodeMatchingPolicyEClass, null, "getCandidateNeighborObjectsForEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "candidateObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getEdge(), "edge", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(iEdgeMatchingPolicyEClass, IEdgeMatchingPolicy.class, "IEdgeMatchingPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIEdgeMatchingPolicy_GraphMatcher(), this.getGraphMatcher(), this.getGraphMatcher_EdgeMatchingPolicy(), "graphMatcher", null, 1, 1, IEdgeMatchingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(iEdgeMatchingPolicyEClass, theEcorePackage.getEBoolean(), "edgeMatches", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getEdge(), "edge", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "candidateVirtualModelEdge", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(frontTransformationProcessorEClass, FrontTransformationProcessor.class, "FrontTransformationProcessor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFrontTransformationProcessor_FrontManager(), this.getFrontManager(), this.getFrontManager_FrontTransformationProcessor(), "frontManager", null, 1, 1, FrontTransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFrontTransformationProcessor_SourceDomainObjects(), theEcorePackage.getEObject(), null, "sourceDomainObjects", null, 0, -1, FrontTransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getFrontTransformationProcessor_CurrentTGGRule(), theTggPackage.getTripleGraphGrammarRule(), null, "currentTGGRule", null, 0, 1, FrontTransformationProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFrontTransformationProcessor_UnmatchedSourceDomainObjects(), theEcorePackage.getEObject(), null, "unmatchedSourceDomainObjects", null, 0, -1, FrontTransformationProcessor.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(graphMatcherEClass, GraphMatcher.class, "GraphMatcher", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraphMatcher_RuleBinding(), theInterpreterconfigurationPackage.getRuleBinding(), null, "ruleBinding", null, 1, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_NodeMatchingPolicy(), this.getINodeMatchingPolicy(), this.getINodeMatchingPolicy_GraphMatcher(), "nodeMatchingPolicy", null, 1, -1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_EdgeMatchingPolicy(), this.getIEdgeMatchingPolicy(), this.getIEdgeMatchingPolicy_GraphMatcher(), "edgeMatchingPolicy", null, 1, -1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_RuleProcessor(), this.getRuleProcessor(), this.getRuleProcessor_GraphMatcher(), "ruleProcessor", null, 1, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_DomainsToMatch(), theInterpreterconfigurationPackage.getDomainModel(), null, "domainsToMatch", null, 0, -1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_ConstraintCheckingPolicy(), this.getIConstraintCheckingPolicy(), this.getIConstraintCheckingPolicy_GraphMatcher(), "constraintCheckingPolicy", null, 0, -1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphMatcher_MatchReusablePatternOnly(), theEcorePackage.getEBoolean(), "matchReusablePatternOnly", null, 0, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_ElementsNotAvailableForProducedNodes(), theEcorePackage.getEObject(), null, "elementsNotAvailableForProducedNodes", null, 0, -1, GraphMatcher.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getGraphMatcher_LinksNotAvailableForProducedEdges(), g1, "linksNotAvailableForProducedEdges", null, 0, -1, GraphMatcher.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphMatcher_RestrictProducedGraphMatching(), theEcorePackage.getEBoolean(), "restrictProducedGraphMatching", null, 0, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphMatcher_StartNodeOfMatching(), theTggPackage.getNode(), null, "startNodeOfMatching", null, 0, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphMatcher_NoElementsToMatchLeft(), theEcorePackage.getEBoolean(), "noElementsToMatchLeft", "", 0, 1, GraphMatcher.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphMatcher_EdgeExploreOperationsPerformed(), theEcorePackage.getEInt(), "edgeExploreOperationsPerformed", null, 0, 1, GraphMatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(graphMatcherEClass, theEcorePackage.getEBoolean(), "exploreEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "startNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "startObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "utilityDepthCounter", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(graphMatcherEClass, theEcorePackage.getEBoolean(), "exploreEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "startNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "startObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getEdge(), "edge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "neighborObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "utilityDepthCounter", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(graphMatcherEClass, theEcorePackage.getEBoolean(), "match", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(graphMatcherEClass, theEcorePackage.getEBoolean(), "match", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "startNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "startObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(baseNodeProcessingPolicyEClass, BaseNodeProcessingPolicy.class, "BaseNodeProcessingPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseEdgeProcessingPolicyEClass, BaseEdgeProcessingPolicy.class, "BaseEdgeProcessingPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iConstraintCheckingPolicyEClass, IConstraintCheckingPolicy.class, "IConstraintCheckingPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIConstraintCheckingPolicy_GraphMatcher(), this.getGraphMatcher(), this.getGraphMatcher_ConstraintCheckingPolicy(), "graphMatcher", null, 1, 1, IConstraintCheckingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(theTggPackage.getConstraint());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIConstraintCheckingPolicy_UnprocessableConstraints(), g1, "unprocessableConstraints", null, 0, 1, IConstraintCheckingPolicy.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConstraintCheckingPolicy_UnprocessedConstraints(), this.getNodeToConstraintMapEntry(), null, "unprocessedConstraints", null, 0, -1, IConstraintCheckingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConstraintCheckingPolicy_ProcessedConstraints(), this.getNodeToConstraintMapEntry(), null, "processedConstraints", null, 0, -1, IConstraintCheckingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConstraintCheckingPolicy_RuleBinding(), theInterpreterconfigurationPackage.getRuleBinding(), null, "ruleBinding", null, 0, 1, IConstraintCheckingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(iConstraintCheckingPolicyEClass, theEcorePackage.getEBoolean(), "evaluateConstraints", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "candidateObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(iConstraintCheckingPolicyEClass, theEcorePackage.getEBoolean(), "hasUnprocessedConstraintsLeft", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(iConstraintCheckingPolicyEClass, null, "dispose", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iConstraintCheckingPolicyEClass, theEcorePackage.getEBoolean(), "canCheckConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "candidateObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getConstraint(), "constraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(baseConstraintProcessingPolicyEClass, BaseConstraintProcessingPolicy.class, "BaseConstraintProcessingPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(baseConstraintProcessingPolicyEClass, theEcorePackage.getEBoolean(), "checkConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getConstraint(), "constraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getEValueClashException());
		addEException(op, this.getConstraintUnprocessableException());

		op = addEOperation(baseConstraintProcessingPolicyEClass, theEcorePackage.getEBoolean(), "enforceConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getConstraint(), "constraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getEValueClashException());
		addEException(op, this.getConstraintUnprocessableException());

		op = addEOperation(baseConstraintProcessingPolicyEClass, null, "nodeBindingRemovedFromRuleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "nodeBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(baseConstraintProcessingPolicyEClass, null, "nodeBindingAddedToRuleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iNodeEnforcementPolicyEClass, INodeEnforcementPolicy.class, "INodeEnforcementPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(iNodeEnforcementPolicyEClass, null, "enforceNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iNodeEnforcementPolicyEClass, null, "removeNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iEdgeEnforcementPolicyEClass, IEdgeEnforcementPolicy.class, "IEdgeEnforcementPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(iEdgeEnforcementPolicyEClass, null, "enforceEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getEdge(), "edge", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "virtualModelEdge", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iConstraintEnforcementPolicyEClass, IConstraintEnforcementPolicy.class, "IConstraintEnforcementPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(iConstraintEnforcementPolicyEClass, theEcorePackage.getEBoolean(), "enforceConstraints", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iConstraintEnforcementPolicyEClass, theEcorePackage.getEBoolean(), "canEnforceConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "candidateObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getConstraint(), "constraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(defaultNodeProcessingPolicyEClass, DefaultNodeProcessingPolicy.class, "DefaultNodeProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(defaultEdgeProcessingPolicyEClass, DefaultEdgeProcessingPolicy.class, "DefaultEdgeProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(defaultOCLConstraintProcessingPolicyEClass, DefaultOCLConstraintProcessingPolicy.class, "DefaultOCLConstraintProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeToConstraintMapEntryEClass, Map.Entry.class, "NodeToConstraintMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNodeToConstraintMapEntry_Value(), theTggPackage.getConstraint(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNodeToConstraintMapEntry_Key(), theTggPackage.getNode(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iMatchingPolicyEClass, IMatchingPolicy.class, "IMatchingPolicy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIMatchingPolicy_TransformationProcessor(), this.getTransformationProcessor(), null, "transformationProcessor", null, 0, 1, IMatchingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIMatchingPolicy_RuleProcessor(), this.getRuleProcessor(), null, "ruleProcessor", null, 0, 1, IMatchingPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transformationProcessorHelperEClass, TransformationProcessorHelper.class, "TransformationProcessorHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransformationProcessorHelper_TransformationProcessor(), this.getTransformationProcessor(), this.getTransformationProcessor_Helper(), "transformationProcessor", null, 1, 1, TransformationProcessorHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationProcessorHelper_SortedConcreteRuleList(), theTggPackage.getTripleGraphGrammarRule(), null, "sortedConcreteRuleList", null, 0, -1, TransformationProcessorHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(transformationProcessorHelperEClass, theEcorePackage.getEBoolean(), "hasProducedSourceDomainNodes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorHelperEClass, theEcorePackage.getEBoolean(), "hasProducedSourceDomainEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorHelperEClass, theEcorePackage.getEBoolean(), "hasProducedTargetDomainNodes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorHelperEClass, null, "getSortedTGGRuleList", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = addEOperation(transformationProcessorHelperEClass, null, "sortTGGRuleList", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationProcessorHelperEClass, null, "getProducedSourceDomainRuleNodes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theTggPackage.getNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = addEOperation(transformationProcessorHelperEClass, null, "getRefiningRules", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTripleGraphGrammarRule(), "tggRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theTggPackage.getTripleGraphGrammarRule());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = addEOperation(transformationProcessorHelperEClass, theEcorePackage.getEInt(), "getAdmissibleStartPosition", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEInt(), "rulePosition", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(debugSupportEClass, DebugSupport.class, "DebugSupport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDebugSupport_Terminate(), theEcorePackage.getEBoolean(), "terminate", "false", 0, 1, DebugSupport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDebugSupport_Debug(), theEcorePackage.getEBoolean(), "debug", "false", 0, 1, DebugSupport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDebugSupport_Running(), theEcorePackage.getEBoolean(), "running", null, 0, 1, DebugSupport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDebugSupport_DebugEventListener(), this.getIDebugEventListener(), null, "debugEventListener", null, 0, -1, DebugSupport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iDebugEventListenerEClass, IDebugEventListener.class, "IDebugEventListener", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(iDebugEventListenerEClass, null, "matchingCandidateObjectToNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getNode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "object", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "successful", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iDebugEventListenerEClass, null, "finishedMatchingRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "successful", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iDebugEventListenerEClass, null, "startMatchingRule", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getRuleBinding(), "ruleBinding", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iDebugEventListenerEClass, null, "startTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getConfiguration(), "configuration", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iDebugEventListenerEClass, null, "finishedTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInterpreterconfigurationPackage.getConfiguration(), "configuration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "successful", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(statusEDataType, IStatus.class, "Status", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eValueClashExceptionEDataType, ValueClashException.class, "EValueClashException", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iProgressMonitorEDataType, IProgressMonitor.class, "IProgressMonitor", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iteratorEDataType, Iterator.class, "Iterator", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(conditionEDataType, Condition.class, "Condition", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(collectionEDataType, Collection.class, "Collection", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(settingEDataType, EStructuralFeature.Setting.class, "Setting", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(constraintUnprocessableExceptionEDataType, ConstraintUnprocessableException.class, "ConstraintUnprocessableException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eCrossReferenceAdapterEDataType, ECrossReferenceAdapter.class, "ECrossReferenceAdapter", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(oclEnvironmentEDataType, EcoreEnvironment.class, "OCLEnvironment", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(oclEDataType, org.eclipse.ocl.ecore.OCL.class, "OCL", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //InterpreterPackageImpl
