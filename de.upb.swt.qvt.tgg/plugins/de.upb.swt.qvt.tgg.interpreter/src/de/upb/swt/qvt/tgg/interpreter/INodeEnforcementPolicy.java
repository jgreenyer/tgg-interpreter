/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INode Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getINodeEnforcementPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface INodeEnforcementPolicy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void enforceNode(Node node, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void removeNode(Node node, EObject eObject);

} // INodeEnforcementPolicy
