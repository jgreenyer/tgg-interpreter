/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDebug Event Listener</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIDebugEventListener()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IDebugEventListener extends EObject {
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @param ruleBinding 
//	 * @model
//	 * @generated
//	 */
//	void matchingCandidateObjectToNode(Node node, EObject object, RuleBinding ruleBinding, boolean successful);
//
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @model
//	 * @generated
//	 */
//	void finishedMatchingRule(RuleBinding ruleBinding, boolean successful);
//
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @model
//	 * @generated
//	 */
//	void startMatchingRule(RuleBinding ruleBinding);
//
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void startTransformation(Configuration configuration);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void finishedTransformation(Configuration configuration, boolean successful);

	public void isApplyRuleRunning(TripleGraphGrammarRule tripleGraphGrammarRule, boolean isRunning, Object classInstance);
	
	public void matchedNode(Node node, EObject eObject, RuleBinding ruleBinding, boolean isFrontTransformationNode, boolean isRunning, Object classInstance);
	
	public void candidateNode(Node node, EObject eObject, Object classInstance);
	
	public void enforcedNode(Node node, EObject eObject, RuleBinding ruleBinding, Object classInstance, boolean isRunning);
	
	public void currentRuleBinding(RuleBinding ruleBinding);
	
	public IDebugTarget getDebugTarget(ILaunch launch, IProcess process);
	
	public void isTransformationRunning(Configuration configuration,boolean isRunning);
	
	public void isMatchingRuleRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance);
	
	public void isPatternMatchingRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance);
	
} // IDebugEventListener
