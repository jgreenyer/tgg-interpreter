/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphElement;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeToEObjectMap <em>Node To EObject Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeBinding <em>Edge Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeBinding <em>Node Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer <em>Rule Binding Container</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeToVirtualModelEdgeMap <em>Edge To Virtual Model Edge Map</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getGraphElementStack <em>Graph Element Stack</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getTggRule <em>Tgg Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding()
 * @model
 * @generated
 */
public interface RuleBinding extends EObject {
	/**
	 * Returns the value of the '<em><b>Node To EObject Map</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.Node},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is an auxilliary list for an easier query of whether a graph element
	 * is already bound. These queries happen quite often during graph matching.
	 * 
	 * TODO: Probably it makes no sense to persist this list. It should be made
	 * transient and have some lazy initialization mechanism.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Node To EObject Map</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_NodeToEObjectMap()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.NodeToEObjectMapEntry<de.upb.swt.qvt.tgg.Node, org.eclipse.emf.ecore.EObject>" transient="true"
	 * @generated
	 */
	EMap<Node, EObject> getNodeToEObjectMap();

	/**
	 * Returns the value of the '<em><b>Edge Binding</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject>},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Edge},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Binding</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Binding</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_EdgeBinding()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.VirtualModelEdgeToEdgeMapEntry<org.eclipse.emf.ecore.EEList<org.eclipse.emf.ecore.EObject>, de.upb.swt.qvt.tgg.Edge>" transient="true"
	 * @generated
	 */
	EMap<EList<EObject>, EList<Edge>> getEdgeBinding();

	/**
	 * Returns the value of the '<em><b>Node Binding</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Binding</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Binding</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_NodeBinding()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.EObjectToNodeMapEntry<org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Node>"
	 * @generated
	 */
	EMap<EObject, EList<Node>> getNodeBinding();

	/**
	 * Returns the value of the '<em><b>Rule Binding Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getRuleBinding <em>Rule Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Binding Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Binding Container</em>' container reference.
	 * @see #setRuleBindingContainer(RuleBindingContainer)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_RuleBindingContainer()
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getRuleBinding
	 * @model opposite="ruleBinding" transient="false"
	 * @generated
	 */
	RuleBindingContainer getRuleBindingContainer();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer <em>Rule Binding Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Binding Container</em>' container reference.
	 * @see #getRuleBindingContainer()
	 * @generated
	 */
	void setRuleBindingContainer(RuleBindingContainer value);

	/**
	 * Returns the value of the '<em><b>Edge To Virtual Model Edge Map</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.Edge},
	 * and the value is of type {@link org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject>},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge To Virtual Model Edge Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge To Virtual Model Edge Map</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_EdgeToVirtualModelEdgeMap()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.EdgeToVirtualModelEdgeMapEntry<de.upb.swt.qvt.tgg.Edge, org.eclipse.emf.ecore.EEList<org.eclipse.emf.ecore.EObject>>" transient="true"
	 * @generated
	 */
	EMap<Edge, EList<EObject>> getEdgeToVirtualModelEdgeMap();

	/**
	 * Returns the value of the '<em><b>Graph Element Stack</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.GraphElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Element Stack</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Element Stack</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_GraphElementStack()
	 * @model
	 * @generated
	 */
	EList<GraphElement> getGraphElementStack();

	/**
	 * Returns the value of the '<em><b>Tgg Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgg Rule</em>' reference.
	 * @see #setTggRule(TripleGraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getRuleBinding_TggRule()
	 * @model
	 * @generated
	 */
	TripleGraphGrammarRule getTggRule();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getTggRule <em>Tgg Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgg Rule</em>' reference.
	 * @see #getTggRule()
	 * @generated
	 */
	void setTggRule(TripleGraphGrammarRule value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void removeElementBindingsFromStack(GraphElement element);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model virtualModelEdgeMany="false"
	 * @generated
	 */
	void pushEdgeBinding(Edge edge, EList<EObject> virtualModelEdge);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void pushNodeBinding(Node node, EObject modelObject);

} // RuleBinding
