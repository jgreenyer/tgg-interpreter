/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default OCL Constraint Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDefaultOCLConstraintProcessingPolicy()
 * @model
 * @generated
 */
public interface DefaultOCLConstraintProcessingPolicy extends BaseConstraintProcessingPolicy {
} // DefaultOCLConstraintProcessingPolicy
