/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation Processor Helper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl#getSortedConcreteRuleList <em>Sorted Concrete Rule List</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransformationProcessorHelperImpl extends EObjectImpl implements TransformationProcessorHelper {
	/**
	 * The cached value of the '{@link #getSortedConcreteRuleList() <em>Sorted Concrete Rule List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSortedConcreteRuleList()
	 * @generated
	 * @ordered
	 */
	protected EList<TripleGraphGrammarRule> sortedConcreteRuleList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationProcessorHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.TRANSFORMATION_PROCESSOR_HELPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessor getTransformationProcessor() {
		if (eContainerFeatureID() != InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR) return null;
		return (TransformationProcessor)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransformationProcessor(TransformationProcessor newTransformationProcessor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTransformationProcessor, InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationProcessor(TransformationProcessor newTransformationProcessor) {
		if (newTransformationProcessor != eInternalContainer() || (eContainerFeatureID() != InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR && newTransformationProcessor != null)) {
			if (EcoreUtil.isAncestor(this, newTransformationProcessor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTransformationProcessor != null)
				msgs = ((InternalEObject)newTransformationProcessor).eInverseAdd(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER, TransformationProcessor.class, msgs);
			msgs = basicSetTransformationProcessor(newTransformationProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR, newTransformationProcessor, newTransformationProcessor));
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TripleGraphGrammarRule> getSortedConcreteRuleList() {
		if (sortedConcreteRuleList == null) {
			sortedConcreteRuleList = new EObjectResolvingEList<TripleGraphGrammarRule>(TripleGraphGrammarRule.class, this, InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST);
			initsortedConcreteRuleList();
		}
		return sortedConcreteRuleList;
	}


	private HashMap<TripleGraphGrammarRule, EList<Node>> tggRuleToProducedSourceDomainNodesMap = new HashMap<TripleGraphGrammarRule, EList<Node>>();

	private HashMap<TripleGraphGrammarRule, Boolean> tggRuleToHasTGGRuleProducedSourceDomainNodesMap = new HashMap<TripleGraphGrammarRule, Boolean>();
	private HashMap<TripleGraphGrammarRule, Boolean> tggRuleToHasTGGRuleProducedSourceDomainEdgesMap = new HashMap<TripleGraphGrammarRule, Boolean>();
	private HashMap<TripleGraphGrammarRule, Boolean> tggRuleToHasTGGRuleProducedTargetDomainNodesMap = new HashMap<TripleGraphGrammarRule, Boolean>();

	/**
	 *  call when application scenario changes.
	 */
	protected void reset(){
		tggRuleToProducedSourceDomainNodesMap = new HashMap<TripleGraphGrammarRule, EList<Node>>();
		tggRuleToHasTGGRuleProducedSourceDomainNodesMap = new HashMap<TripleGraphGrammarRule, Boolean>();
		sortedConcreteRuleList = null;
	}
	
	
	/**
	 * maps a tggRule to a list of the directly refining rules
	 */
	private HashMap<TripleGraphGrammarRule, EList<TripleGraphGrammarRule>> tggRuleToRefiningTGGRulesList = null;
	
	/**
	 * maps a tggRule to a list of all the directly and indirectly refining rules
	 */
	private HashMap<TripleGraphGrammarRule, EList<TripleGraphGrammarRule>> tggRuleToAllRefiningTGGRulesList = null;

	/**
	 * maps a tggRule to its most general rule
	 */
	//private HashMap<TripleGraphGrammarRule, TripleGraphGrammarRule> tggRuleToMostGeneralTGGRule = null;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns a list of all direct sub-rules of the given tgg rule.
	 * <!-- end-model-doc -->
	 * @generated NOT
	 */
	public EList<TripleGraphGrammarRule> getRefiningRules(TripleGraphGrammarRule tggRule) {
		if (tggRuleToRefiningTGGRulesList == null) {
			tggRuleToRefiningTGGRulesList = new HashMap<TripleGraphGrammarRule, EList<TripleGraphGrammarRule>>();
		}
		if (tggRuleToRefiningTGGRulesList.get(tggRule) == null){
			EList<TripleGraphGrammarRule> refinedRuleList = new BasicEList<TripleGraphGrammarRule>();
			for (Rule potentiallyRefinedTGGRule : getTransformationProcessor().getInterpreter().getConfiguration().getTripleGraphGrammar().getAllRules()) {
				if (((TripleGraphGrammarRule)potentiallyRefinedTGGRule).getRefinedRule() == tggRule) {
					refinedRuleList.add((TripleGraphGrammarRule) potentiallyRefinedTGGRule);
				}
			}
			tggRuleToRefiningTGGRulesList.put(tggRule, refinedRuleList);
		}
		return tggRuleToRefiningTGGRulesList.get(tggRule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getAdmissibleStartPosition(int rulePosition) {
		TripleGraphGrammarRule tggRuleAtGivenPosition = sortedConcreteRuleList.get(rulePosition);
		EList<TripleGraphGrammarRule> allRefinedRulesOfTGGRuleAtGivenPosition = tggRuleToAllRefiningTGGRulesList.get(tggRuleAtGivenPosition);
		for (int i = 0; i < rulePosition; i++){
			TripleGraphGrammarRule tggRule = sortedConcreteRuleList.get(i);
			if (allRefinedRulesOfTGGRuleAtGivenPosition.contains(tggRule))
				return i;
		}
		return rulePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * checks whether the TGG rule has produced nodes in the source domain according to the
	 * current application scenario.
	 * <!-- end-model-doc -->
	 * @generated NOT
	 */
	public boolean hasProducedSourceDomainNodes(TripleGraphGrammarRule tggRule) {
		if (!tggRuleToHasTGGRuleProducedSourceDomainNodesMap.containsKey(tggRule)){

			// jgreen: doing that kills the performance! Don't know why that is soo bad... 
			//cachedHasTGGRuleProducedSourceDomainNodesMap.put(tggRule, getProducedSourceDomainRuleNodes(tggRule, applicationScenario).isEmpty());

			// better:
			boolean ruleHasAtLeastOneProducedSourceNode = false;
			for (Node tggNode : getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getSourceDomainNodesByTGGRule().get(tggRule)) {
				if (tggNode.isProduced()) {
					ruleHasAtLeastOneProducedSourceNode = true;
					break;
				}
			}
			tggRuleToHasTGGRuleProducedSourceDomainNodesMap.put(tggRule, ruleHasAtLeastOneProducedSourceNode);
			
		}
		return tggRuleToHasTGGRuleProducedSourceDomainNodesMap.get(tggRule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * checks whether the TGG rule has produced nodes in the source domain according to the
	 * current application scenario.
	 * <!-- end-model-doc -->
	 * @generated NOT
	 */
	public boolean hasProducedSourceDomainEdges(TripleGraphGrammarRule tggRule) {
		if (!tggRuleToHasTGGRuleProducedSourceDomainEdgesMap.containsKey(tggRule)){
			boolean ruleHasAtLeastOneProducedSourceEdge = false;
			nodeFor:
			for (Node tggNode : getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getSourceDomainNodesByTGGRule().get(tggRule)) {
				for (Edge edge : tggNode.getAllOutgoingEdges()) { 
					if (edge.isProduced()) {
						ruleHasAtLeastOneProducedSourceEdge = true;
						break nodeFor;
					}
				}
			}
			tggRuleToHasTGGRuleProducedSourceDomainEdgesMap.put(tggRule, ruleHasAtLeastOneProducedSourceEdge);
			
		}
		return tggRuleToHasTGGRuleProducedSourceDomainEdgesMap.get(tggRule);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean hasProducedTargetDomainNodes(TripleGraphGrammarRule tggRule) {
		if (!tggRuleToHasTGGRuleProducedTargetDomainNodesMap.containsKey(tggRule)){

			// jgreen: doing that kills the performance! Don't know why that is soo bad... 
			//cachedHasTGGRuleProducedSourceDomainNodesMap.put(tggRule, getProducedSourceDomainRuleNodes(tggRule, applicationScenario).isEmpty());

			// better:
			boolean ruleHasAtLeastOneProducedTargetNode = false;
			for (Node tggNode : getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getTargetDomainNodeByTGGRule().get(tggRule)) {
				if (tggNode.isProduced()) {
					ruleHasAtLeastOneProducedTargetNode = true;
					break;
				}
			}
			tggRuleToHasTGGRuleProducedTargetDomainNodesMap.put(tggRule, ruleHasAtLeastOneProducedTargetNode);
			
		}
		return tggRuleToHasTGGRuleProducedTargetDomainNodesMap.get(tggRule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TripleGraphGrammarRule> getSortedTGGRuleList() {
		if (sortedConcreteRuleList == null){
			initsortedConcreteRuleList();
		}
		return sortedConcreteRuleList;
	}

	private TripleGraphGrammarRule lastTGGRuleSorted = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void sortTGGRuleList(TripleGraphGrammarRule tggRuleToMoveForward) {
		if (tggRuleToMoveForward == lastTGGRuleSorted) return;
		
		if (sortedConcreteRuleList == null) {
			initsortedConcreteRuleList();
		}
		
		
		//System.out.println(sortedConcreteRuleList.indexOf(tggRule));

		// sort tgg rule refinement sub-tree to come first in the sortedConcreteRuleList
		EList<TripleGraphGrammarRule> tggRuleRefinementHierarchyList = getSortedListOfTGGRuleRefinementHierarchy(tggRuleToMoveForward);
		moveSubListToPosition(sortedConcreteRuleList, tggRuleRefinementHierarchyList, 0);
		
		
//		System.out.println("sortedConcreteRuleList before optimization: " + getTGGRuleListString(sortedConcreteRuleList));
//		System.out.println("lastTGGRuleSorted: " + lastTGGRuleSorted);
		
		/* OPTIMIZATION: 
		 * move the tgg rule refinement sub-tree of the tgg rule that was sorted in previously as far back in the
		 * sortedConcreteRuleList as possible.
		 * That means
		 * 1. find the refined rule of the lastTGGRuleSorted
		 *   a) if it has no refined rule, then move the tgg rule refinement sub-tree of lastTGGRuleSorted
		 *      to the back of sortedConcreteRuleList.
		 *   b) if it has a refined rule, then more the tgg rule refinement sub-tree of lastTGGRuleSorted
		 *      to the position immediately before the position of lastTGGRuleSorted.getRefinedRule() in sortedConcreteRuleList.
		 */
		if (lastTGGRuleSorted != null) {
			EList<TripleGraphGrammarRule> lastTGGRuleRefinementHierarchyList = getSortedListOfTGGRuleRefinementHierarchy(lastTGGRuleSorted);
			sortedConcreteRuleList.removeAll(lastTGGRuleRefinementHierarchyList);
			int position;
			if (lastTGGRuleSorted.getRefinedRule() != null && sortedConcreteRuleList.contains(lastTGGRuleSorted.getRefinedRule())) {
				position = sortedConcreteRuleList.indexOf(lastTGGRuleSorted.getRefinedRule());
			} else {
				position = sortedConcreteRuleList.size();
			}
//			System.out.println("to be inserted to pos; " + position);
//			System.out.println("lastTGGRuleRefinementHierarchyList: " + getTGGRuleListString(lastTGGRuleRefinementHierarchyList));
			
			sortedConcreteRuleList.addAll(position, lastTGGRuleRefinementHierarchyList);
		}

		//System.out.println("sortedConcreteRuleList after optimization: " + getTGGRuleListString(sortedConcreteRuleList));
		// OPTIMIZATION END 

		
		/* OPTIMIZATION II:
		 * sort TGG rules which the current rule advises to apply next (their refinement hierarchy list) in the next possible position after the current rule (its refinement hiearchy list)
		 */
		int endPositionOfLastRefinementHierarchyListSortedIn = tggRuleRefinementHierarchyList.size() - 1; // sort other refinement hierarchy lists after this position (this is the )
		assert (sortedConcreteRuleList.get(endPositionOfLastRefinementHierarchyListSortedIn) == tggRuleToMoveForward);
		for (TripleGraphGrammarRule tggRuleAdvisedToApplyNext : tggRuleToMoveForward.getRulesAdvisedToApplyNext()) {
			
			EList<TripleGraphGrammarRule> nextRefinementHierarchyListToSortIn = getSortedListOfTGGRuleRefinementHierarchy(tggRuleAdvisedToApplyNext);
			endPositionOfLastRefinementHierarchyListSortedIn += nextRefinementHierarchyListToSortIn.size();
			moveSubListToPosition(sortedConcreteRuleList, getSortedListOfTGGRuleRefinementHierarchy(tggRuleAdvisedToApplyNext), endPositionOfLastRefinementHierarchyListSortedIn+1);
			
		}
		
//		System.out.print("Resorting InitialNodeBindingGenerator:");
//		for (TripleGraphGrammarRule sortedTGGRule : sortedConcreteRuleList) {
//			System.out.print(sortedTGGRule.getName() + ", ");
//		}
//		System.out.print("\n");


		assert isSorted(sortedConcreteRuleList);

		assert(!getRefiningRules(tggRuleToMoveForward).isEmpty() || sortedConcreteRuleList.get(0)==tggRuleToMoveForward); 

		
		// finally
		lastTGGRuleSorted = tggRuleToMoveForward;
	}
	
	
	@SuppressWarnings("unchecked")
	public static void moveSubListToPosition(@SuppressWarnings("rawtypes") EList hostList, @SuppressWarnings("rawtypes") EList subList, int position){
		for (int i = subList.size()-1; i >= 0; i--) {
			hostList.move(position, subList.get(i));
		}
	}
	
	
	/**
	 * checks whether a refining rule in the list always precedes the refined rule.
	 * @param tggRuleList
	 * @return
	 */
	private boolean isSorted(EList<TripleGraphGrammarRule> tggRuleList){
		for (TripleGraphGrammarRule tripleGraphGrammarRule : tggRuleList) {
			if (tripleGraphGrammarRule.getRefinedRule() != null && tggRuleList.contains(tripleGraphGrammarRule.getRefinedRule())){
				if (tggRuleList.indexOf(tripleGraphGrammarRule) > tggRuleList.indexOf(tripleGraphGrammarRule.getRefinedRule())){
//					System.out.println("the tgg rule " + tripleGraphGrammarRule.getName() + " is at " + tggRuleList.indexOf(tripleGraphGrammarRule));
//					System.out.println("while rule   " + tripleGraphGrammarRule.getRefinedRule().getName() + " is at " + tggRuleList.indexOf(tripleGraphGrammarRule.getRefinedRule()));
					return false;
				}
			}
		}
		return true;
	}
	

	/*private String getTGGRuleListString(EList<TripleGraphGrammarRule> tggRuleList){
		String s = "";
		for (TripleGraphGrammarRule tripleGraphGrammarRule : tggRuleList) {
			s += tripleGraphGrammarRule.getName() + ", ";
		}
		return s;
	}*/

	private void initsortedConcreteRuleList() {
		for (Rule rule : getTransformationProcessor().getInterpreter().getConfiguration().getTripleGraphGrammar()
				.getAllRules()) {
			TripleGraphGrammarRule tggRule = (TripleGraphGrammarRule) rule;
			if (!tggRule.isAbstract() 
					&& getTransformationProcessor().getInterpreter().getConfiguration().getTripleGraphGrammar().getAxiom() != tggRule) {
				sortedConcreteRuleList.addAll(getSortedListOfTGGRuleRefinementHierarchy(tggRule));
			}
		}
		assert isSorted(sortedConcreteRuleList);
	}
	
	
	/**
	 * Recursive method to sort the TGG rule refinement hierarchy of a TGG rule and its sub-rules
	 * into the given list such that a sub-rule always precedes its super-rule.
	 * 
	 * Works as follows:
	 * 1) if tggRule does not have any sub-rule, tggRule is appended to tggRuleList
	 * 2) else the method is called for each sub-rule of tggRule. 
	 * 
	 * @param tggRule
	 * @param tggRuleList
	 */
	private EList<TripleGraphGrammarRule> getSortedListOfTGGRuleRefinementHierarchy(TripleGraphGrammarRule tggRule){
		if (tggRuleToAllRefiningTGGRulesList == null) 
			tggRuleToAllRefiningTGGRulesList = new HashMap<TripleGraphGrammarRule, EList<TripleGraphGrammarRule>>();
		if (tggRuleToAllRefiningTGGRulesList.get(tggRule) != null)
			return tggRuleToAllRefiningTGGRulesList.get(tggRule);
		EList<TripleGraphGrammarRule> returnList = new BasicEList<TripleGraphGrammarRule>();
		if (getRefiningRules(tggRule).isEmpty() && !tggRule.isAbstract()) {
			returnList.add(tggRule);
		} else {
			for (TripleGraphGrammarRule tripleGraphGrammarRule : getRefiningRules(tggRule)) {
				returnList.addAll(getSortedListOfTGGRuleRefinementHierarchy(tripleGraphGrammarRule));
			}
			if (!tggRule.isAbstract()) {
				returnList.add(tggRule);
			}
		}
		tggRuleToAllRefiningTGGRulesList.put(tggRule, returnList);
		return returnList;
	}	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns a list of all produced nodes in the TGG rule that are in the source domain 
	 * according to the current application scenario.
	 * <!-- end-model-doc -->
	 * @generated NOT
	 */
	public EList<Node> getProducedSourceDomainRuleNodes(TripleGraphGrammarRule tggRule) {
		if (!tggRuleToProducedSourceDomainNodesMap.containsKey(tggRule)){		
			EList<Node> producedSourceNodesListList = new UniqueEList<Node>();
			for (Node node : getTransformationProcessor().getInterpreter().getConfiguration().getActiveApplicationScenario().getSourceDomainNodesByTGGRule().get(tggRule)) {
				if (node.isProduced()) producedSourceNodesListList.add(node);
			}
			tggRuleToProducedSourceDomainNodesMap.put(tggRule, producedSourceNodesListList);
		}
		
		return tggRuleToProducedSourceDomainNodesMap.get(tggRule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTransformationProcessor((TransformationProcessor)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				return basicSetTransformationProcessor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				return eInternalContainer().eInverseRemove(this, InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER, TransformationProcessor.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				return getTransformationProcessor();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST:
				return getSortedConcreteRuleList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST:
				getSortedConcreteRuleList().clear();
				getSortedConcreteRuleList().addAll((Collection<? extends TripleGraphGrammarRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST:
				getSortedConcreteRuleList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR:
				return getTransformationProcessor() != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST:
				return sortedConcreteRuleList != null && !sortedConcreteRuleList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransformationProcessorHelperImpl
