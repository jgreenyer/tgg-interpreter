/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getTripleGraphGrammar <em>Triple Graph Grammar</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isRecordRuleBindings <em>Record Rule Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getApplicationScenario <em>Application Scenario</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModel <em>Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getRuleBindingContainer <em>Rule Binding Container</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getActiveApplicationScenario <em>Active Application Scenario</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModelByTypedModel <em>Domain Model By Typed Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainRootObjects <em>Domain Root Objects</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isNoPackageUriRedirection <em>No Package Uri Redirection</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='numberOfDomainModelsMatchesNumberOfTypedModels'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL numberOfDomainModelsMatchesNumberOfTypedModels='self.domainModel->size() = self.tripleGraphGrammar.modelParameter->size()'"
 * @generated
 */
public interface Configuration extends EObject {
	/**
	 * Returns the value of the '<em><b>Triple Graph Grammar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triple Graph Grammar</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triple Graph Grammar</em>' reference.
	 * @see #setTripleGraphGrammar(TripleGraphGrammar)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_TripleGraphGrammar()
	 * @model required="true"
	 * @generated
	 */
	TripleGraphGrammar getTripleGraphGrammar();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getTripleGraphGrammar <em>Triple Graph Grammar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Triple Graph Grammar</em>' reference.
	 * @see #getTripleGraphGrammar()
	 * @generated
	 */
	void setTripleGraphGrammar(TripleGraphGrammar value);

	/**
	 * Returns the value of the '<em><b>Record Rule Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Rule Bindings</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Rule Bindings</em>' attribute.
	 * @see #setRecordRuleBindings(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_RecordRuleBindings()
	 * @model
	 * @generated
	 */
	boolean isRecordRuleBindings();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isRecordRuleBindings <em>Record Rule Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Rule Bindings</em>' attribute.
	 * @see #isRecordRuleBindings()
	 * @generated
	 */
	void setRecordRuleBindings(boolean value);

	/**
	 * Returns the value of the '<em><b>Application Scenario</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Scenario</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Scenario</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_ApplicationScenario()
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration
	 * @model opposite="configuration" containment="true" required="true"
	 * @generated
	 */
	EList<ApplicationScenario> getApplicationScenario();

	/**
	 * Returns the value of the '<em><b>Domain Model</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Model</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_DomainModel()
	 * @model containment="true"
	 * @generated
	 */
	EList<DomainModel> getDomainModel();

	/**
	 * Returns the value of the '<em><b>Rule Binding Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Binding Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Binding Container</em>' containment reference.
	 * @see #setRuleBindingContainer(RuleBindingContainer)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_RuleBindingContainer()
	 * @model containment="true"
	 * @generated
	 */
	RuleBindingContainer getRuleBindingContainer();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getRuleBindingContainer <em>Rule Binding Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Binding Container</em>' containment reference.
	 * @see #getRuleBindingContainer()
	 * @generated
	 */
	void setRuleBindingContainer(RuleBindingContainer value);

	/**
	 * Returns the value of the '<em><b>Active Application Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Application Scenario</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Application Scenario</em>' reference.
	 * @see #setActiveApplicationScenario(ApplicationScenario)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_ActiveApplicationScenario()
	 * @model required="true"
	 * @generated
	 */
	ApplicationScenario getActiveApplicationScenario();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getActiveApplicationScenario <em>Active Application Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Application Scenario</em>' reference.
	 * @see #getActiveApplicationScenario()
	 * @generated
	 */
	void setActiveApplicationScenario(ApplicationScenario value);

	/**
	 * Returns the value of the '<em><b>Domain Model By Typed Model</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.qvtbase.TypedModel},
	 * and the value is of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Model By Typed Model</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Model By Typed Model</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_DomainModelByTypedModel()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.TypedModelToDomainModelMapEntry<de.upb.swt.qvt.qvtbase.TypedModel, de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel>" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EMap<TypedModel, DomainModel> getDomainModelByTypedModel();

	/**
	 * Returns the value of the '<em><b>Domain Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * references all object of each domain model:
	 * 1. Either root objects are assigned explicity for
	 * a domain model. Then, these objects are added
	 * to this list.
	 * 2. when no root object is assigned explicitly for
	 * a domain model, all objects contained in the domain
	 * model resource are added to this list.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Domain Root Objects</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_DomainRootObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EObject> getDomainRootObjects();

	/**
	 * Returns the value of the '<em><b>No Package Uri Redirection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Package Uri Redirection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Package Uri Redirection</em>' attribute.
	 * @see #setNoPackageUriRedirection(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getConfiguration_NoPackageUriRedirection()
	 * @model
	 * @generated
	 */
	boolean isNoPackageUriRedirection();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isNoPackageUriRedirection <em>No Package Uri Redirection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Package Uri Redirection</em>' attribute.
	 * @see #isNoPackageUriRedirection()
	 * @generated
	 */
	void setNoPackageUriRedirection(boolean value);

} // Configuration
