/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConstraint Checking Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher <em>Graph Matcher</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessableConstraints <em>Unprocessable Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessedConstraints <em>Unprocessed Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getProcessedConstraints <em>Processed Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getRuleBinding <em>Rule Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IConstraintCheckingPolicy extends EObject {
	/**
	 * Returns the value of the '<em><b>Graph Matcher</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getConstraintCheckingPolicy <em>Constraint Checking Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Matcher</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Matcher</em>' reference.
	 * @see #setGraphMatcher(GraphMatcher)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy_GraphMatcher()
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getConstraintCheckingPolicy
	 * @model opposite="constraintCheckingPolicy" required="true"
	 * @generated
	 */
	GraphMatcher getGraphMatcher();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher <em>Graph Matcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Matcher</em>' reference.
	 * @see #getGraphMatcher()
	 * @generated
	 */
	void setGraphMatcher(GraphMatcher value);

	/**
	 * Returns the value of the '<em><b>Unprocessable Constraints</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unprocessable Constraints</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unprocessable Constraints</em>' attribute.
	 * @see #setUnprocessableConstraints(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy_UnprocessableConstraints()
	 * @model transient="true"
	 * @generated
	 */
	Map<Constraint, String> getUnprocessableConstraints();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessableConstraints <em>Unprocessable Constraints</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unprocessable Constraints</em>' attribute.
	 * @see #getUnprocessableConstraints()
	 * @generated
	 */
	void setUnprocessableConstraints(Map<Constraint, String> value);

	/**
	 * Returns the value of the '<em><b>Unprocessed Constraints</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.Node},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Constraint},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * unprocessedConstraints
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Unprocessed Constraints</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy_UnprocessedConstraints()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreter.NodeToConstraintMapEntry<de.upb.swt.qvt.tgg.Node, de.upb.swt.qvt.tgg.Constraint>"
	 * @generated
	 */
	EMap<Node, EList<Constraint>> getUnprocessedConstraints();

	/**
	 * Returns the value of the '<em><b>Processed Constraints</b></em>' map.
	 * The key is of type {@link de.upb.swt.qvt.tgg.Node},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Constraint},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processed Constraints</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processed Constraints</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy_ProcessedConstraints()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreter.NodeToConstraintMapEntry<de.upb.swt.qvt.tgg.Node, de.upb.swt.qvt.tgg.Constraint>"
	 * @generated
	 */
	EMap<Node, EList<Constraint>> getProcessedConstraints();

	/**
	 * Returns the value of the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Binding</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Binding</em>' reference.
	 * @see #setRuleBinding(RuleBinding)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIConstraintCheckingPolicy_RuleBinding()
	 * @model
	 * @generated
	 */
	RuleBinding getRuleBinding();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getRuleBinding <em>Rule Binding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Binding</em>' reference.
	 * @see #getRuleBinding()
	 * @generated
	 */
	void setRuleBinding(RuleBinding value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * we call this method any time that we try to match a node:
	 * We evaluate
	 * (1) all constraints attached to that node (by their slot node reference)
	 * (2) all previously unprocessed constraints
	 * 
	 * (Right from the start of matching a rule (when matching the initial node 
	 * binding), we try to process all TGG rule constraints that are not attached 
	 * to any node.)
	 * 
	 * In case that a constraint evaluates to TRUE, we add the constraint
	 * to the list of processed constraints (and remove it from the list of
	 * unprocessed constraints).
	 * In case that a constraint cannot be evaluated (because the necessary nodes
	 * are not bound, etc.) we put those constraints in the list of unprocessed
	 * constraints.
	 * In case that any constraint evaluates to FALSE, we return FLASE, otherwise
	 * we return TRUE.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean evaluateConstraints(Node node, EObject candidateObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns TRUE whether the list of unprocessed constraints is empty, 
	 * FALSE otherwise.
	 * 
	 * This method is called by the graph matcher at the end of graph matching
	 * to determine whether all constraints could be evaluated successfully.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean hasUnprocessedConstraintsLeft();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * jgreen:
	 * I just added this method here, because I want to be able to call the 
	 * OCL.dispose method in the DefaultOCLConstraintProcessingPolicy from
	 * the RuleProcessor.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void dispose();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns true when the policy is responsible for checking this constraint. Only then,
	 * this policy considers the constraint.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean canCheckConstraint(Node node, EObject candidateObject, Constraint constraint);

} // IConstraintCheckingPolicy
