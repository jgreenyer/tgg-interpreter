/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDefaultEdgeProcessingPolicy()
 * @model
 * @generated
 */
public interface DefaultEdgeProcessingPolicy extends BaseEdgeProcessingPolicy {
} // DefaultEdgeProcessingPolicy
