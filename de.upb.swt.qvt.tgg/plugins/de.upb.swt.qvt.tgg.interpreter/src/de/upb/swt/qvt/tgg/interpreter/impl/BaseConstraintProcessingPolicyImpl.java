/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TGGConstraint;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Constraint Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl#getGraphMatcher <em>Graph Matcher</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl#getUnprocessableConstraints <em>Unprocessable Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl#getUnprocessedConstraints <em>Unprocessed Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl#getProcessedConstraints <em>Processed Constraints</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl#getRuleBinding <em>Rule Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BaseConstraintProcessingPolicyImpl extends EObjectImpl implements BaseConstraintProcessingPolicy {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(BaseConstraintProcessingPolicyImpl.class.getName());
	
	/**
	 * The cached value of the '{@link #getGraphMatcher() <em>Graph Matcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphMatcher()
	 * @generated
	 * @ordered
	 */
	protected GraphMatcher graphMatcher;

	/**
	 * The cached value of the '{@link #getUnprocessableConstraints() <em>Unprocessable Constraints</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnprocessableConstraints()
	 * @generated
	 * @ordered
	 */
	protected Map<Constraint, String> unprocessableConstraints;

	/**
	 * The cached value of the '{@link #getUnprocessedConstraints() <em>Unprocessed Constraints</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnprocessedConstraints()
	 * @generated
	 * @ordered
	 */
	protected EMap<Node, EList<Constraint>> unprocessedConstraints;

	/**
	 * The cached value of the '{@link #getProcessedConstraints() <em>Processed Constraints</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessedConstraints()
	 * @generated
	 * @ordered
	 */
	protected EMap<Node, EList<Constraint>> processedConstraints;

	/**
	 * The cached value of the '{@link #getRuleBinding() <em>Rule Binding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleBinding()
	 * @generated
	 * @ordered
	 */
	protected RuleBinding ruleBinding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseConstraintProcessingPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.BASE_CONSTRAINT_PROCESSING_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Node, EList<Constraint>> getUnprocessedConstraints() {
		if (unprocessedConstraints == null) {
			unprocessedConstraints = new EcoreEMap<Node,EList<Constraint>>(InterpreterPackage.Literals.NODE_TO_CONSTRAINT_MAP_ENTRY, NodeToConstraintMapEntryImpl.class, this, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS);
		}
		return unprocessedConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<Constraint, String> getUnprocessableConstraints() {
		return unprocessableConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnprocessableConstraints(Map<Constraint, String> newUnprocessableConstraints) {
		Map<Constraint, String> oldUnprocessableConstraints = unprocessableConstraints;
		unprocessableConstraints = newUnprocessableConstraints;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS, oldUnprocessableConstraints, unprocessableConstraints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding getRuleBinding() {
		if (ruleBinding != null && ruleBinding.eIsProxy()) {
			InternalEObject oldRuleBinding = (InternalEObject)ruleBinding;
			ruleBinding = (RuleBinding)eResolveProxy(oldRuleBinding);
			if (ruleBinding != oldRuleBinding) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING, oldRuleBinding, ruleBinding));
			}
		}
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding basicGetRuleBinding() {
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setRuleBinding(RuleBinding newRuleBinding) {
		RuleBinding oldRuleBinding = ruleBinding;
		ruleBinding = newRuleBinding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING, oldRuleBinding, ruleBinding));
		
		if (ruleBindingAdapter == null){
			ruleBindingAdapter = new RuleBindingAdapter();
		}
		if (oldRuleBinding != null)
			oldRuleBinding.eAdapters().remove(ruleBindingAdapter);
		//TODO temporary removal of the RuleBindingAdapter
		ruleBinding.eAdapters().add(ruleBindingAdapter);
		getProcessedConstraints().clear();
		if (getUnprocessableConstraints() == null)
			setUnprocessableConstraints(new HashMap<Constraint, String>());
		else getUnprocessableConstraints().clear();
		getUnprocessableConstraints().clear();
		getUnprocessedConstraints().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException, ConstraintUnprocessableException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean enforceConstraint(Node node, EObject eObject, Constraint constraint) throws ValueClashException, ConstraintUnprocessableException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * If the node binding is removed from the rule binding, place all constraints
	 * processed at the position of this node in the list of unprocessed constraints
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void nodeBindingRemovedFromRuleBinding(Node node) {
		if (getProcessedConstraints().get(node) != null){
			if (getUnprocessedConstraints().get(node) == null){
				getUnprocessedConstraints().put(node, new UniqueEList<Constraint>());
			}
			getUnprocessedConstraints().get(node).addAll(
					getProcessedConstraints().get(node));
			getProcessedConstraints().get(node).clear();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void nodeBindingAddedToRuleBinding(Node node, EObject eObject) {
//		// TODO: implement this method
//		// Ensure that you remove @generated or mark it @generated NOT
//		throw new UnsupportedOperationException();	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Node, EList<Constraint>> getProcessedConstraints() {
		if (processedConstraints == null) {
			processedConstraints = new EcoreEMap<Node,EList<Constraint>>(InterpreterPackage.Literals.NODE_TO_CONSTRAINT_MAP_ENTRY, NodeToConstraintMapEntryImpl.class, this, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS);
		}
		return processedConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher getGraphMatcher() {
		if (graphMatcher != null && graphMatcher.eIsProxy()) {
			InternalEObject oldGraphMatcher = (InternalEObject)graphMatcher;
			graphMatcher = (GraphMatcher)eResolveProxy(oldGraphMatcher);
			if (graphMatcher != oldGraphMatcher) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER, oldGraphMatcher, graphMatcher));
			}
		}
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher basicGetGraphMatcher() {
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphMatcher(GraphMatcher newGraphMatcher, NotificationChain msgs) {
		GraphMatcher oldGraphMatcher = graphMatcher;
		graphMatcher = newGraphMatcher;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER, oldGraphMatcher, newGraphMatcher);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphMatcher(GraphMatcher newGraphMatcher) {
		if (newGraphMatcher != graphMatcher) {
			NotificationChain msgs = null;
			if (graphMatcher != null)
				msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY, GraphMatcher.class, msgs);
			if (newGraphMatcher != null)
				msgs = ((InternalEObject)newGraphMatcher).eInverseAdd(this, InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY, GraphMatcher.class, msgs);
			msgs = basicSetGraphMatcher(newGraphMatcher, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER, newGraphMatcher, newGraphMatcher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method is called by the graph matcher when matching the given node with the given eObject.
	 * Evaluating constraints works as follows:
	 * 
	 * (1) When calling evaluateConstraints for the first time, we 
	 *   (a) try to evaluate all constraints that are attached to the first node given and we
	 *       try to evaluate all constraints that are not attached to any node (rule constraints).
	 *       - All successfully checked constraints (checkConstraint() returns TRUE) are put into
	 *         the processedConstraints map
	 *       - All constraints that are not processable (checkConstraint() throws 
	 *         ConstraintUnprocessableException) are put into the unprocessableConstraints map.
	 *       - If one constraint evaluates to FALSE, we return FALSE immediately.
	 *   (b) all constraints attached to nodes other than the first one are put into the 
	 *       unprocessedConstraints map.
	 * (2) When calling evaluateConstraints for the next time, we
	 *   (a) try to evaluate all constraints that are attached to the given node given and we
	 *       try to evaluate all constraints that are hitherto unprocessable (in the 
	 *       unprocessableConstraints map).
	 *       - All successfully checked constraints (checkConstraint() returns TRUE) are put into
	 *         the processedConstraints map
	 *       - All constraints that are not processable (checkConstraint() throws 
	 *         ConstraintUnprocessableException) are put into the unprocessableConstraints map.
	 *       - If one constraint evaluates to FALSE, we return FALSE immediately.
	 *       
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean evaluateConstraints(Node node, EObject candidateObject) {
		if (getRuleBinding() == null) throw new IllegalStateException("A rule binding has to be assigned to the constraint processor.");
		
		// fetch currently processed TGG rule from the ruleBinding (see FrontTransformationProcessor.createInitialRuleBinding())
		TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
		if (tggRule.getAllConstraints().isEmpty()) {
			return true;
		}else{
			if (getUnprocessedConstraints().isEmpty() 
					&& getProcessedConstraints().isEmpty() 
					&& getUnprocessableConstraints().isEmpty()){ // calling evaluateConstraints for the first time...
				/*   (a) try to evaluate all constraints that are attached to the first node given and we
				 *       try to evaluate all constraints that are not attached to any node (rule constraints). */
				for (Constraint constraint : tggRule.getAllConstraints()){
					if (constraint instanceof TGGConstraint) {
						TGGConstraint tggConstraint = (TGGConstraint) constraint;
						Node refinedConstraintSlotNode = tggRule.getMostRefinedNodeOf(tggConstraint.getSlotNode());
						if ((canCheckConstraint(node, candidateObject, tggConstraint) || canEnforceConstraint(node,
								candidateObject, tggConstraint))) {
							if (refinedConstraintSlotNode == null || refinedConstraintSlotNode == node) {
								if (!doCheckConstraint(node, candidateObject, tggConstraint)) {
									// * - If one constraint evaluates to FALSE,
									// we return FALSE immediately.
									logger.debug("evaluating constraint " + tggConstraint
											+ "returned FALSE when matching candidate object " + candidateObject
											+ " with node " + node);
									return false;
								}
							} else {
								// * (b) all constraints attached to nodes other
								// than the first one are put into the
								// * unprocessedConstraints map.
								if (getUnprocessedConstraints().get(refinedConstraintSlotNode) == null) {
									getUnprocessedConstraints().put(refinedConstraintSlotNode,
											new UniqueEList<Constraint>());
								}
								getUnprocessedConstraints().get(refinedConstraintSlotNode).add(tggConstraint);
							}
						}
					} else {
//						if (logger.isDebugEnabled())
//							logger.debug("This policy does not support constraints of this kind " + constraint);
					}
				}
			}else{ // calling evaluateConstraints for the next time...
				// *   (a) try to evaluate all constraints that are attached to the given node given and we
				// *       try to evaluate all constraints that are hitherto unprocessable (in the 
				// *       unprocessableConstraints map).
				if (getUnprocessedConstraints().get(node) != null){
					Constraint[] unprocessedConstraint = getUnprocessedConstraints().get(node).toArray(new Constraint[0]);
					for (int i = 0; i < unprocessedConstraint.length; i++) {
						if (!doCheckConstraint(node, candidateObject, unprocessedConstraint[i])){
							// *       - If one constraint evaluates to FALSE, we return FALSE immediately.
							logger.debug("evaluating constraint " + unprocessedConstraint[i] + 
									"returned FALSE when matching candidate object " + candidateObject
									+ " with node " + node);
							return false;
						}
					}
				}
				Constraint[] constraints = getUnprocessableConstraints().keySet().toArray(new Constraint[0]);
				for (int j = 0; j < constraints.length; j++) {
					Constraint constraint = constraints[j];
					if (!doCheckConstraint(node, candidateObject, constraint)){
						// *       - If one constraint evaluates to FALSE, we return FALSE immediately.
						logger.debug("evaluating constraint " + constraint + 
								"returned FALSE when matching candidate object " + candidateObject
								+ " with node " + node);
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean doCheckConstraint(Node node, EObject candidateObject,
			Constraint constraint) {
		if (constraint instanceof TGGConstraint){
			TGGConstraint tggConstraint = (TGGConstraint) constraint;
			Node refinedConstraintSlotNode = getRuleBinding().getTggRule().getMostRefinedNodeOf(tggConstraint.getSlotNode());
			try {
				if (checkConstraint(node, candidateObject, tggConstraint)){
					// *       - All successfully checked constraints (checkConstraint() returns TRUE) are put into
					// *         the processedConstraints map
					if (getProcessedConstraints().get(node) == null){
						getProcessedConstraints().put(node, new UniqueEList<Constraint>());
					}
					getProcessedConstraints().get(node).add(tggConstraint);
					if (getUnprocessedConstraints().get(refinedConstraintSlotNode) != null)
						getUnprocessedConstraints().get(refinedConstraintSlotNode).remove(tggConstraint);
					getUnprocessableConstraints().remove(tggConstraint);
				}else {
					return false;
				}
			} catch (ValueClashException e) {
				logger.error("There was a value clash ", e);
			} catch (ConstraintUnprocessableException e) {
				// *       - All constraints that are not processable (checkConstraint() throws 
				// *         ConstraintUnprocessableException) are put into the unprocessableConstraints map.
				getUnprocessableConstraints().put(tggConstraint, e.getMessage());
				if (getUnprocessedConstraints().get(refinedConstraintSlotNode) != null)
					getUnprocessedConstraints().get(refinedConstraintSlotNode).remove(tggConstraint);
			}
		}else{
			if (logger.isDebugEnabled())
				logger.debug("This policy does not support constraints of this kind " + constraint);
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean enforceConstraints(Node node, EObject eObject) {
		
		// fetch currently processed TGG rule from the ruleBinding (see FrontTransformationProcessor.createInitialRuleBinding())
		TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
		if (!tggRule.getAllConstraints().isEmpty()
				&& getUnprocessedConstraints().isEmpty() 
				&& getProcessedConstraints().isEmpty() 
				&& getUnprocessableConstraints().isEmpty()){
			/* 
			 * This means that the constraint maps/lists have not yet been initialized
			 * because there were no constraint to be processed in the graph patterns that
			 * were matched. Therefore, we now have to put all constraints in the map of 
			 * unprocessed constraints
			 */
			for (Constraint constraint : tggRule.getAllConstraints()){
				if (constraint instanceof TGGConstraint && canEnforceConstraint(node, eObject, constraint)){
					TGGConstraint tggConstraint = (TGGConstraint) constraint;
					Node refinedConstraintSlotNode = tggRule.getMostRefinedNodeOf(tggConstraint.getSlotNode());
					if (getUnprocessedConstraints().get(refinedConstraintSlotNode) == null){
						getUnprocessedConstraints().put(refinedConstraintSlotNode, new UniqueEList<Constraint>());
					}
					getUnprocessedConstraints().get(refinedConstraintSlotNode).add(tggConstraint);
				}
			}
			
		}

		
		// *   (a) try to enforce all constraints that are attached to the given node and we
		// *       try to enforce all constraints that are hitherto unprocessable (in the 
		// *       unprocessableConstraints map).
		if (getUnprocessedConstraints().get(node) != null){
			Constraint[] unprocessedConstraint = (Constraint[]) getUnprocessedConstraints().get(node).toArray();
			for (int i = 0; i < unprocessedConstraint.length; i++) {
				if (!doEnforceConstraint(node, eObject, unprocessedConstraint[i])){
					// *       - If one constraint evaluates to FALSE, we return FALSE immediately.
					logger.debug("Enforcing constraint " + unprocessedConstraint[i] + 
							"returned FALSE when enforcing candidate object " + eObject
							+ " with node " + node);
					return false;
				}
			}
		}
		Constraint[] unprocessableConstraint = getUnprocessableConstraints().keySet().toArray(new Constraint[0]);
		for (int i = 0; i < unprocessableConstraint.length; i++) {
			if (!doEnforceConstraint(node, eObject, unprocessableConstraint[i])){
				// *       - If one constraint evaluates to FALSE, we return FALSE immediately.
				logger.debug("Enforcing constraint " + unprocessableConstraint[i] + 
						"returned FALSE when enforcing candidate object " + eObject
						+ " with node " + node);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean canEnforceConstraint(Node node, EObject candidateObject, Constraint constraint) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	private boolean doEnforceConstraint(Node node, EObject eObject,
			Constraint constraint) {
		if (constraint instanceof TGGConstraint) {
			TGGConstraint tggConstraint = (TGGConstraint) constraint;
			// fetch currently processed TGG rule from the ruleBinding (see FrontTransformationProcessor.createInitialRuleBinding())
			TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
			Node refinedConstraintSlotNode = tggRule.getMostRefinedNodeOf(tggConstraint.getSlotNode());
			try {
				boolean constraintProcessed;
				
				//Get typed model of slot node (if there is a slot node)
				TypedModel typedModel = null;
				if (refinedConstraintSlotNode != null)
					typedModel = tggRule.getNodesDomainGraphPattern(refinedConstraintSlotNode).getTypedModel();
				
				// 1. if there is no typed model, there is no slot node -> just check constraint
				// 2. if the slot node is in a source domain -> just check constraint
				if (typedModel == null || getGraphMatcher().getDomainsToMatch().contains(typedModel)) {
					constraintProcessed = checkConstraint(node, eObject, constraint);
					//TODO: throw ValueClashException when false?
				} else {
					constraintProcessed = enforceConstraint(node, eObject, tggConstraint);
				}
				if (constraintProcessed){
					// *       - All successfully enforced constraints (enforceConstraint() returns TRUE) are put into
					// *         the processedConstraints map
					if (getProcessedConstraints().get(node) == null){
						getProcessedConstraints().put(node, new UniqueEList<Constraint>());
					}
					getProcessedConstraints().get(node).add(tggConstraint);
					if (getUnprocessedConstraints().get(refinedConstraintSlotNode) != null)
						getUnprocessedConstraints().get(refinedConstraintSlotNode).remove(tggConstraint);
					getUnprocessableConstraints().remove(tggConstraint);
				}else {
					return false;
				}
			} catch (ValueClashException e) {
				logger.error("There was a value clash ", e);
			} catch (ConstraintUnprocessableException e) {
				// *       - All constraints that are not enforceable (enforceConstraint() throws 
				// *         ConstraintUnprocessableException) are put into the unprocessableConstraints map.
				getUnprocessableConstraints().put(tggConstraint, e.getMessage());
				if (getUnprocessedConstraints().get(refinedConstraintSlotNode) != null)
					getUnprocessedConstraints().get(refinedConstraintSlotNode).remove(tggConstraint);
			}
		}else{
			logger.warn("We do not support constraints of this kind " + constraint);
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean hasUnprocessedConstraintsLeft() {
		return !getUnprocessableConstraints().isEmpty();
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method may be overridden by clients to perform cleanup when
	 * this ConstraintProcessingPolicy is no longer used.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void dispose() {
		throw new UnsupportedOperationException("This operation is not implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean canCheckConstraint(Node node, EObject candidateObject, Constraint constraint) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	protected RuleBindingAdapter ruleBindingAdapter;
	
	protected class RuleBindingAdapter extends AdapterImpl{
		@Override
		public void notifyChanged(Notification msg) {
			if (msg.getFeature() == InterpreterconfigurationPackage.Literals.RULE_BINDING__GRAPH_ELEMENT_STACK){
				if (msg.getEventType() == Notification.ADD){
					if (msg.getNewValue() instanceof Node){
						nodeBindingAddedToRuleBinding((Node)msg.getNewValue(), getRuleBinding().getNodeToEObjectMap().get(msg.getNewValue()));
					}
				}
				if (msg.getEventType() == Notification.ADD_MANY){
					throw new UnsupportedOperationException("TODO: Handle the case of node bindings being" +
					"added to the rule binding stack");
				}
				if (msg.getEventType() == Notification.REMOVE){
					if (msg.getOldValue() instanceof Node){
						nodeBindingRemovedFromRuleBinding((Node) msg.getOldValue());
					}
				}
				if (msg.getEventType() == Notification.REMOVE_MANY){
					throw new UnsupportedOperationException("TODO: Handle the case of node bindings being" +
					"removed from the rule binding stack");
				}
				
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				if (graphMatcher != null)
					msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, InterpreterPackage.GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY, GraphMatcher.class, msgs);
				return basicSetGraphMatcher((GraphMatcher)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				return basicSetGraphMatcher(null, msgs);
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS:
				return ((InternalEList<?>)getUnprocessedConstraints()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS:
				return ((InternalEList<?>)getProcessedConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				if (resolve) return getGraphMatcher();
				return basicGetGraphMatcher();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS:
				return getUnprocessableConstraints();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS:
				if (coreType) return getUnprocessedConstraints();
				else return getUnprocessedConstraints().map();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS:
				if (coreType) return getProcessedConstraints();
				else return getProcessedConstraints().map();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING:
				if (resolve) return getRuleBinding();
				return basicGetRuleBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)newValue);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS:
				setUnprocessableConstraints((Map<Constraint, String>)newValue);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS:
				((EStructuralFeature.Setting)getUnprocessedConstraints()).set(newValue);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS:
				((EStructuralFeature.Setting)getProcessedConstraints()).set(newValue);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING:
				setRuleBinding((RuleBinding)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)null);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS:
				setUnprocessableConstraints((Map<Constraint, String>)null);
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS:
				getUnprocessedConstraints().clear();
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS:
				getProcessedConstraints().clear();
				return;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING:
				setRuleBinding((RuleBinding)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER:
				return graphMatcher != null;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS:
				return unprocessableConstraints != null;
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS:
				return unprocessedConstraints != null && !unprocessedConstraints.isEmpty();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS:
				return processedConstraints != null && !processedConstraints.isEmpty();
			case InterpreterPackage.BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING:
				return ruleBinding != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (unprocessableConstraints: ");
		result.append(unprocessableConstraints);
		result.append(')');
		return result.toString();
	}

} //BaseConstraintProcessingPolicyImpl
