/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph Matcher</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleBinding <em>Rule Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getNodeMatchingPolicy <em>Node Matching Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeMatchingPolicy <em>Edge Matching Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor <em>Rule Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getDomainsToMatch <em>Domains To Match</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getConstraintCheckingPolicy <em>Constraint Checking Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isMatchReusablePatternOnly <em>Match Reusable Pattern Only</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getElementsNotAvailableForProducedNodes <em>Elements Not Available For Produced Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getLinksNotAvailableForProducedEdges <em>Links Not Available For Produced Edges</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isRestrictProducedGraphMatching <em>Restrict Produced Graph Matching</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getStartNodeOfMatching <em>Start Node Of Matching</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isNoElementsToMatchLeft <em>No Elements To Match Left</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeExploreOperationsPerformed <em>Edge Explore Operations Performed</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher()
 * @model
 * @generated
 */
public interface GraphMatcher extends EObject {
	/**
	 * Returns the value of the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Binding</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Binding</em>' reference.
	 * @see #setRuleBinding(RuleBinding)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_RuleBinding()
	 * @model required="true"
	 * @generated
	 */
	RuleBinding getRuleBinding();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleBinding <em>Rule Binding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Binding</em>' reference.
	 * @see #getRuleBinding()
	 * @generated
	 */
	void setRuleBinding(RuleBinding value);

	/**
	 * Returns the value of the '<em><b>Node Matching Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Matching Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Matching Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_NodeMatchingPolicy()
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher
	 * @model opposite="graphMatcher" required="true"
	 * @generated
	 */
	EList<INodeMatchingPolicy> getNodeMatchingPolicy();

	/**
	 * Returns the value of the '<em><b>Edge Matching Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Matching Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Matching Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_EdgeMatchingPolicy()
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher
	 * @model opposite="graphMatcher" required="true"
	 * @generated
	 */
	EList<IEdgeMatchingPolicy> getEdgeMatchingPolicy();

	/**
	 * Returns the value of the '<em><b>Rule Processor</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Processor</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Processor</em>' container reference.
	 * @see #setRuleProcessor(RuleProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_RuleProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher
	 * @model opposite="graphMatcher" required="true" transient="false"
	 * @generated
	 */
	RuleProcessor getRuleProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor <em>Rule Processor</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Processor</em>' container reference.
	 * @see #getRuleProcessor()
	 * @generated
	 */
	void setRuleProcessor(RuleProcessor value);

	/**
	 * Returns the value of the '<em><b>Domains To Match</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domains To Match</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domains To Match</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_DomainsToMatch()
	 * @model
	 * @generated
	 */
	EList<DomainModel> getDomainsToMatch();

	/**
	 * Returns the value of the '<em><b>Constraint Checking Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Checking Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Checking Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_ConstraintCheckingPolicy()
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher
	 * @model opposite="graphMatcher"
	 * @generated
	 */
	EList<IConstraintCheckingPolicy> getConstraintCheckingPolicy();

	/**
	 * Returns the value of the '<em><b>Match Reusable Pattern Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Match Reusable Pattern Only</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Match Reusable Pattern Only</em>' attribute.
	 * @see #setMatchReusablePatternOnly(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_MatchReusablePatternOnly()
	 * @model
	 * @generated
	 */
	boolean isMatchReusablePatternOnly();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isMatchReusablePatternOnly <em>Match Reusable Pattern Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Match Reusable Pattern Only</em>' attribute.
	 * @see #isMatchReusablePatternOnly()
	 * @generated
	 */
	void setMatchReusablePatternOnly(boolean value);

	/**
	 * Returns the value of the '<em><b>Elements Not Available For Produced Nodes</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements Not Available For Produced Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements Not Available For Produced Nodes</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_ElementsNotAvailableForProducedNodes()
	 * @model transient="true"
	 * @generated
	 */
	EList<EObject> getElementsNotAvailableForProducedNodes();

	/**
	 * Returns the value of the '<em><b>Links Not Available For Produced Edges</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.common.util.EList}&lt;org.eclipse.emf.ecore.EObject>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Links Not Available For Produced Edges</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links Not Available For Produced Edges</em>' attribute list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_LinksNotAvailableForProducedEdges()
	 * @model transient="true"
	 * @generated
	 */
	EList<EList<EObject>> getLinksNotAvailableForProducedEdges();

	/**
	 * Returns the value of the '<em><b>Restrict Produced Graph Matching</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrict Produced Graph Matching</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrict Produced Graph Matching</em>' attribute.
	 * @see #setRestrictProducedGraphMatching(boolean)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_RestrictProducedGraphMatching()
	 * @model
	 * @generated
	 */
	boolean isRestrictProducedGraphMatching();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isRestrictProducedGraphMatching <em>Restrict Produced Graph Matching</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrict Produced Graph Matching</em>' attribute.
	 * @see #isRestrictProducedGraphMatching()
	 * @generated
	 */
	void setRestrictProducedGraphMatching(boolean value);

	/**
	 * Returns the value of the '<em><b>Start Node Of Matching</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Whenever the graph matcher starts or continues matching a rule based on some initial node binding 
	 * in the match() method, it sets a reference to the node in the initial node binding.
	 * The purpose of this reference is that the DefaultNodeProcessingPolicy knows when the matching
	 * of reusable nodes advanced to the next node.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Start Node Of Matching</em>' reference.
	 * @see #setStartNodeOfMatching(Node)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_StartNodeOfMatching()
	 * @model
	 * @generated
	 */
	Node getStartNodeOfMatching();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getStartNodeOfMatching <em>Start Node Of Matching</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Node Of Matching</em>' reference.
	 * @see #getStartNodeOfMatching()
	 * @generated
	 */
	void setStartNodeOfMatching(Node value);

	/**
	 * Returns the value of the '<em><b>No Elements To Match Left</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Elements To Match Left</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Elements To Match Left</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_NoElementsToMatchLeft()
	 * @model default="" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isNoElementsToMatchLeft();

	/**
	 * Returns the value of the '<em><b>Edge Explore Operations Performed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Explore Operations Performed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Explore Operations Performed</em>' attribute.
	 * @see #setEdgeExploreOperationsPerformed(int)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getGraphMatcher_EdgeExploreOperationsPerformed()
	 * @model
	 * @generated
	 */
	int getEdgeExploreOperationsPerformed();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeExploreOperationsPerformed <em>Edge Explore Operations Performed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge Explore Operations Performed</em>' attribute.
	 * @see #getEdgeExploreOperationsPerformed()
	 * @generated
	 */
	void setEdgeExploreOperationsPerformed(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 1. Query the NodeMatchingPolicy whether the given node matches the given object
	 * Return false when this is not the case.
	 * 2. Push it on the rule binding's stack.
	 * 3. Ask the patternMatchingStrategy for a list of outgoing or incoming edges to 
	 * match in that particular order. Call the methods exploreOutgoingEdge(), resp. 
	 * exploreIncomingEdge() with that edge and candidate objects.
	 * Note that in the case of edges with multi-valued type references, we may to try 
	 * all possible combinations of candidate neighbor objects. Furthermore, we have to 
	 * backtrack, which means that, in the case we cannot find a valid match for an edge, 
	 * we have to try different matches for previous, successfully matched edges.
	 * Matching is successful, when all edges are bound successfully. then we return TRUE.
	 * Matching is not successful when at least one edge was not bound successfully. Then
	 * we return FALSE.
	 * 
	 * 
	 * int edgeCounter;
	 * int[] objCounter;
	 * numberOfCandidateObjects[];
	 * numberOfEdges = number of considered incoming and outgoing edges
	 * 
	 * while(TRUE){
	 * 	if (exploreEdge(getEdge(edgeCounter), getNeighborObject(objCounter[edgeCounter]))){
	 * 		edgeCounter++;
	 * 		if (edgeCounter > numberOfEdges) return true;
	 * 	}else{
	 * 		objCounter[edgeCounter]++;
	 * 		if (objCounter[edgeCounter] > numberOfCandidateObjects[edgeCounter]) edgeCounter --;
	 * 		if (edgeCounter<0) return false;
	 * 	}
	 * }
	 * 
	 * 
	 * (When this method PatternMatchingStrategy.edgeMatchMandatory() returns
	 * true, the graph matcher performs a naive partial matching that does not 
	 * backtrack on nodes.)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean exploreEdges(Node startNode, EObject startObject, int utilityDepthCounter);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * a) check whether the node type matches the neighbor object
	 * b) call edgeBindingEditPolicy.edgeMatches() to determine whether the edge matches a given link
	 * c) call nodeMatchingPolicy.nodeMatches() to determine whether the neighbor object matches the neighbor node.
	 * d) create an edge binding and push it on the ruleBinding stack
	 * e) call exploreEdges() with the candidate neighbor node and object.
	 * If exploreEdges() returns true, return true.
	 * If exploreEdges() returns false, clear the stack until the just added edge binding
	 * 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean exploreEdge(Node startNode, EObject startObject, Edge edge, EObject neighborObject, int utilityDepthCounter);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method returns match(Node, EObject) for some node binding in the current
	 * ruleBinding that has attached unbound edges that can are left to match.
	 * The method returns true if no such node binding exists.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean match();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * starts the matching from a paricular node binding (Node+EObject).
	 * The argument startNode and startObject must be a binding in the 
	 * GraphMatcher's ruleBinding, otherwise an IllegalArgumentException
	 * is thrown.
	 * 
	 * The method does the following:
	 * 1. Initialize the crossReferencerMap
	 * 2. start the matching by calling the exploreEdges() method.
	 * exploreEdges() returns true when it finds a match for such patterns
	 * which are strictly required to match in the particular application scenario
	 * (see de.upb.swt.qvt.tgg.interpreter.GraphMatcher.exploreEdges()).
	 * It is required that exploreEdges() returns true for each node binding 
	 * that is predefined in the given rule binding.
	 * 
	 * When calling this method, it is important that an initial rule binding is set
	 * which contains at least one valid node binding (see {@link #nodeMatches(Node, EObject)}).
	 * Otherwise an {@link IllegalArgumentException} is thrown.
	 *  
	 * It also requires that the {@link #domainsToMatch} list contains the domain models 
	 * of the domains that shall be matched, e.g. the source domain models of the current active
	 * application scenario {@link Configuration#getActiveApplicationScenario()}
	 * 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean match(Node startNode, EObject startObject);

} // GraphMatcher
