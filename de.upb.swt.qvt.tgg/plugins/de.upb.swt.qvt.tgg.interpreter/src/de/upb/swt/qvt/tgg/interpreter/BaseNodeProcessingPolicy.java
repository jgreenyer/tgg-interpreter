/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getBaseNodeProcessingPolicy()
 * @model abstract="true"
 * @generated
 */
public interface BaseNodeProcessingPolicy extends INodeMatchingPolicy, INodeEnforcementPolicy {
} // BaseNodeProcessingPolicy
