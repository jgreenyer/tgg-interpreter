/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Front Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor <em>Front Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getNodeBinding <em>Node Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontManager()
 * @model
 * @generated
 */
public interface FrontManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Front Transformation Processor</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager <em>Front Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Front Transformation Processor</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Front Transformation Processor</em>' container reference.
	 * @see #setFrontTransformationProcessor(FrontTransformationProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontManager_FrontTransformationProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager
	 * @model opposite="frontManager" required="true" transient="false"
	 * @generated
	 */
	FrontTransformationProcessor getFrontTransformationProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor <em>Front Transformation Processor</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Front Transformation Processor</em>' container reference.
	 * @see #getFrontTransformationProcessor()
	 * @generated
	 */
	void setFrontTransformationProcessor(FrontTransformationProcessor value);

	/**
	 * Returns the value of the '<em><b>Node Binding</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Binding</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Binding</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontManager_NodeBinding()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.EObjectToNodeMapEntry<org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Node>"
	 * @generated
	 */
	EMap<EObject, EList<Node>> getNodeBinding();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean update();

} // FrontManager
