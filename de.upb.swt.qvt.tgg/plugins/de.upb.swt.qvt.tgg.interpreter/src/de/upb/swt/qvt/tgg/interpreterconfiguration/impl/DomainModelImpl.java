/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Severity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getTypedModel <em>Typed Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getRootObject <em>Root Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getResourceURI <em>Resource URI</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getDomainObject <em>Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl#getShowDomainModelDiagnostic <em>Show Domain Model Diagnostic</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DomainModelImpl extends EObjectImpl implements DomainModel {
	/**
	 * The cached value of the '{@link #getTypedModel() <em>Typed Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedModel()
	 * @generated
	 * @ordered
	 */
	protected TypedModel typedModel;

	/**
	 * The cached value of the '{@link #getRootObject() <em>Root Object</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootObject()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> rootObject;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResourceURI() <em>Resource URI</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceURI()
	 * @generated
	 * @ordered
	 */
	protected EList<String> resourceURI;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resource;

	/**
	 * The default value of the '{@link #getShowDomainModelDiagnostic() <em>Show Domain Model Diagnostic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShowDomainModelDiagnostic()
	 * @generated
	 * @ordered
	 */
	protected static final Severity SHOW_DOMAIN_MODEL_DIAGNOSTIC_EDEFAULT = Severity.OK;

	/**
	 * The cached value of the '{@link #getShowDomainModelDiagnostic() <em>Show Domain Model Diagnostic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShowDomainModelDiagnostic()
	 * @generated
	 * @ordered
	 */
	protected Severity showDomainModelDiagnostic = SHOW_DOMAIN_MODEL_DIAGNOSTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.DOMAIN_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedModel getTypedModel() {
		if (typedModel != null && typedModel.eIsProxy()) {
			InternalEObject oldTypedModel = (InternalEObject)typedModel;
			typedModel = (TypedModel)eResolveProxy(oldTypedModel);
			if (typedModel != oldTypedModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL, oldTypedModel, typedModel));
			}
		}
		return typedModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedModel basicGetTypedModel() {
		return typedModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTypedModel(TypedModel newTypedModel) {
		TypedModel oldTypedModel = typedModel;
		typedModel = newTypedModel;
		if (eNotificationRequired()){
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL, oldTypedModel, typedModel));
			
			//jgreen: added notification of change of name attribute
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.DOMAIN_MODEL__NAME, 
					(oldTypedModel == null ? noTypedModelString : oldTypedModel.getName()), 
					(typedModel == null ? noTypedModelString : typedModel.getName())));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getRootObject() {
		if (rootObject == null) {
			rootObject = new EObjectResolvingEList<EObject>(EObject.class, this, InterpreterconfigurationPackage.DOMAIN_MODEL__ROOT_OBJECT);
		}
		return rootObject;
	}

	private static final String noTypedModelString = "[no typed model set]";
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getName(){
	
		String name = getTypedModel() != null ? getTypedModel().getName() : noTypedModelString;
		String resourceURIString = !(getResourceURI().isEmpty() || getResourceURI().get(0) == "") ? getResourceURI().toString() : "[no resource URI set]";

		return name + " : \"" + resourceURIString + "\"";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getResourceURI() {
		if (resourceURI == null) {
			resourceURI = new EDataTypeUniqueEList<String>(String.class, this, InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE_URI);
		}
		return resourceURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResource() {
		if (resource == null) {
			resource = new EDataTypeUniqueEList<Resource>(Resource.class, this, InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE);
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> getDomainObject() {
		EList<EObject> domainObjectList = new EObjectResolvingEList<EObject>(EObject.class, this,
				InterpreterconfigurationPackage.DOMAIN_MODEL__DOMAIN_OBJECT);
		for (EObject rootObject : getRootObject()) {
		    for (TreeIterator<EObject> eObjectTreeIterator = rootObject.eAllContents();  eObjectTreeIterator.hasNext();){
		    	domainObjectList.add(eObjectTreeIterator.next());
		    }
		}
		return domainObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Severity getShowDomainModelDiagnostic() {
		return showDomainModelDiagnostic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowDomainModelDiagnostic(Severity newShowDomainModelDiagnostic) {
		Severity oldShowDomainModelDiagnostic = showDomainModelDiagnostic;
		showDomainModelDiagnostic = newShowDomainModelDiagnostic == null ? SHOW_DOMAIN_MODEL_DIAGNOSTIC_EDEFAULT : newShowDomainModelDiagnostic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC, oldShowDomainModelDiagnostic, showDomainModelDiagnostic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL:
				if (resolve) return getTypedModel();
				return basicGetTypedModel();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__ROOT_OBJECT:
				return getRootObject();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__NAME:
				return getName();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE_URI:
				return getResourceURI();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE:
				return getResource();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__DOMAIN_OBJECT:
				return getDomainObject();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC:
				return getShowDomainModelDiagnostic();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL:
				setTypedModel((TypedModel)newValue);
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__ROOT_OBJECT:
				getRootObject().clear();
				getRootObject().addAll((Collection<? extends EObject>)newValue);
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE_URI:
				getResourceURI().clear();
				getResourceURI().addAll((Collection<? extends String>)newValue);
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE:
				getResource().clear();
				getResource().addAll((Collection<? extends Resource>)newValue);
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC:
				setShowDomainModelDiagnostic((Severity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL:
				setTypedModel((TypedModel)null);
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__ROOT_OBJECT:
				getRootObject().clear();
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE_URI:
				getResourceURI().clear();
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE:
				getResource().clear();
				return;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC:
				setShowDomainModelDiagnostic(SHOW_DOMAIN_MODEL_DIAGNOSTIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.DOMAIN_MODEL__TYPED_MODEL:
				return typedModel != null;
			case InterpreterconfigurationPackage.DOMAIN_MODEL__ROOT_OBJECT:
				return rootObject != null && !rootObject.isEmpty();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE_URI:
				return resourceURI != null && !resourceURI.isEmpty();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__RESOURCE:
				return resource != null && !resource.isEmpty();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__DOMAIN_OBJECT:
				return !getDomainObject().isEmpty();
			case InterpreterconfigurationPackage.DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC:
				return showDomainModelDiagnostic != SHOW_DOMAIN_MODEL_DIAGNOSTIC_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resourceURI: ");
		result.append(resourceURI);
		result.append(", resource: ");
		result.append(resource);
		result.append(", showDomainModelDiagnostic: ");
		result.append(showDomainModelDiagnostic);
		result.append(')');
		return result.toString();
	}

} //DomainModelImpl
