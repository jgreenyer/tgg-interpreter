/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.util;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage
 * @generated
 */
public class InterpreterconfigurationSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static InterpreterconfigurationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterconfigurationSwitch() {
		if (modelPackage == null) {
			modelPackage = InterpreterconfigurationPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case InterpreterconfigurationPackage.CONFIGURATION: {
				Configuration configuration = (Configuration)theEObject;
				T result = caseConfiguration(configuration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.DOMAIN_MODEL: {
				DomainModel domainModel = (DomainModel)theEObject;
				T result = caseDomainModel(domainModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.EOBJECT_TO_NODE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, EList<Node>> eObjectToNodeMapEntry = (Map.Entry<EObject, EList<Node>>)theEObject;
				T result = caseEObjectToNodeMapEntry(eObjectToNodeMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EList<EObject>, EList<Edge>> virtualModelEdgeToEdgeMapEntry = (Map.Entry<EList<EObject>, EList<Edge>>)theEObject;
				T result = caseVirtualModelEdgeToEdgeMapEntry(virtualModelEdgeToEdgeMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.RULE_BINDING: {
				RuleBinding ruleBinding = (RuleBinding)theEObject;
				T result = caseRuleBinding(ruleBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO: {
				ApplicationScenario applicationScenario = (ApplicationScenario)theEObject;
				T result = caseApplicationScenario(applicationScenario);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER: {
				RuleBindingContainer ruleBindingContainer = (RuleBindingContainer)theEObject;
				T result = caseRuleBindingContainer(ruleBindingContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<TypedModel, DomainModel> typedModelToDomainModelMapEntry = (Map.Entry<TypedModel, DomainModel>)theEObject;
				T result = caseTypedModelToDomainModelMapEntry(typedModelToDomainModelMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.TGG_RULE_TO_NODES_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<TripleGraphGrammarRule, EList<Node>> tggRuleToNodesMapEntry = (Map.Entry<TripleGraphGrammarRule, EList<Node>>)theEObject;
				T result = caseTGGRuleToNodesMapEntry(tggRuleToNodesMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.NODE_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Node, EObject> nodeToEObjectMapEntry = (Map.Entry<Node, EObject>)theEObject;
				T result = caseNodeToEObjectMapEntry(nodeToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Edge, EList<EObject>> edgeToVirtualModelEdgeMapEntry = (Map.Entry<Edge, EList<EObject>>)theEObject;
				T result = caseEdgeToVirtualModelEdgeMapEntry(edgeToVirtualModelEdgeMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.AXIOM_BINDING: {
				AxiomBinding axiomBinding = (AxiomBinding)theEObject;
				T result = caseAxiomBinding(axiomBinding);
				if (result == null) result = caseRuleBinding(axiomBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InterpreterconfigurationPackage.INITIAL_AXIOM_NODE_BINDING: {
				InitialAxiomNodeBinding initialAxiomNodeBinding = (InitialAxiomNodeBinding)theEObject;
				T result = caseInitialAxiomNodeBinding(initialAxiomNodeBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfiguration(Configuration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Domain Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Domain Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDomainModel(DomainModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject To Node Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject To Node Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEObjectToNodeMapEntry(Map.Entry<EObject, EList<Node>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Virtual Model Edge To Edge Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Virtual Model Edge To Edge Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVirtualModelEdgeToEdgeMapEntry(Map.Entry<EList<EObject>, EList<Edge>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleBinding(RuleBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application Scenario</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplicationScenario(ApplicationScenario object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Binding Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Binding Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleBindingContainer(RuleBindingContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Model To Domain Model Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Model To Domain Model Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedModelToDomainModelMapEntry(Map.Entry<TypedModel, DomainModel> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule To Nodes Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule To Nodes Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRuleToNodesMapEntry(Map.Entry<TripleGraphGrammarRule, EList<Node>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeToEObjectMapEntry(Map.Entry<Node, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Edge To Virtual Model Edge Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edge To Virtual Model Edge Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEdgeToVirtualModelEdgeMapEntry(Map.Entry<Edge, EList<EObject>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Axiom Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Axiom Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAxiomBinding(AxiomBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Axiom Node Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Axiom Node Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInitialAxiomNodeBinding(InitialAxiomNodeBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //InterpreterconfigurationSwitch
