package de.upb.swt.qvt.tgg.interpreter.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

import com.tools.logging.PluginLogManager;

public class InterpreterPlugin extends Plugin{
	
    // logger configuration
    private static final String LOG_PROPERTIES_FILE = "logger.properties";

    //log manager
    private PluginLogManager logManager;
    
    private static InterpreterPlugin interpreterPlugin;

    public InterpreterPlugin(){
    	super();
    	interpreterPlugin = this;
    }
	
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		configure();
	}
	
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
        if(this.logManager != null) {
            logManager.shutdown();
            logManager = null;
        }
        interpreterPlugin = null;
	}

	/**
     * @return Returns the logManager.
     */
    public static PluginLogManager getLogManager() {
        return getDefault().logManager;
    }

	
    /**
     * Returns the shared plugin instance.
     */
    public static InterpreterPlugin getDefault() {
        return interpreterPlugin;
    }

	
    private void configure() {
        URL url = getBundle().getEntry("/" + LOG_PROPERTIES_FILE);
        try {
            InputStream inputStream = url.openStream();
            if(inputStream != null) {
                Properties properties = new Properties();
                properties.load(inputStream);
                inputStream.close();
                logManager = new PluginLogManager(getDefault(), properties);
                //                logManager.hookPlugin(getDefault().getBundle()
                //                        .getSymbolicName(), getDefault().getLog());
            }
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    getDefault().getBundle().getSymbolicName(),
                    IStatus.ERROR,
                    "Error while initializing log properties." + e.getMessage(),
                    e);
            getDefault().getLog().log(status);
            throw new RuntimeException(
                    "Error while initializing log properties.", e);
        }
    }

}
