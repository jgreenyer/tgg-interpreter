/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation Processor Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getSortedConcreteRuleList <em>Sorted Concrete Rule List</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessorHelper()
 * @model
 * @generated
 */
public interface TransformationProcessorHelper extends EObject {
	/**
	 * Returns the value of the '<em><b>Transformation Processor</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper <em>Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Processor</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Processor</em>' container reference.
	 * @see #setTransformationProcessor(TransformationProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessorHelper_TransformationProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper
	 * @model opposite="helper" required="true" transient="false"
	 * @generated
	 */
	TransformationProcessor getTransformationProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor <em>Transformation Processor</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Processor</em>' container reference.
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	void setTransformationProcessor(TransformationProcessor value);

	/**
	 * Returns the value of the '<em><b>Sorted Concrete Rule List</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.TripleGraphGrammarRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sorted Concrete Rule List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sorted Concrete Rule List</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessorHelper_SortedConcreteRuleList()
	 * @model
	 * @generated
	 */
	EList<TripleGraphGrammarRule> getSortedConcreteRuleList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * checks whether the TGG rule has produced nodes in the source domain according to the
	 * current application scenario.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean hasProducedSourceDomainNodes(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * checks whether the TGG rule has produced edges in the source domain according to the
	 * current application scenario.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean hasProducedSourceDomainEdges(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * checks whether the TGG rule has produced nodes in the target domain according to the
	 * current application scenario.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean hasProducedTargetDomainNodes(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns the internally maintained sorted list of TGG rules.
	 * The list contains all TGG rules that are not abstract, excluding the axiom.
	 * The sorting is always such that refining rules precede their refined rules.
	 * <!-- end-model-doc -->
	 * @model kind="operation" many="false"
	 * @generated
	 */
	EList<TripleGraphGrammarRule> getSortedTGGRuleList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * sorts the sortedConcreteRuleList.
	 * The sorting is such that the refinement hierarchy where the given TGG rule is the root
	 * is found most forward in the TGG rule list.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void sortTGGRuleList(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns a list of all produced nodes in the TGG rule that are in the source domain 
	 * according to the current application scenario.
	 * <!-- end-model-doc -->
	 * @model many="false"
	 * @generated
	 */
	EList<Node> getProducedSourceDomainRuleNodes(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * returns a list of all direct sub-rules of the given tgg rule.
	 * <!-- end-model-doc -->
	 * @model many="false"
	 * @generated
	 */
	EList<TripleGraphGrammarRule> getRefiningRules(TripleGraphGrammarRule tggRule);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * the given position is a position of a TGG rule in the
	 * sortedConcreteRuleList. The operation returns the position
	 * of the first rule in the list, which is a direct or indirect sub-rule
	 * of that rule, or it returns the given position value.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	int getAdmissibleStartPosition(int rulePosition);

} // TransformationProcessorHelper
