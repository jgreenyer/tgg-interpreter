/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IEdge Matching Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIEdgeMatchingPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IEdgeMatchingPolicy extends IMatchingPolicy {
	/**
	 * Returns the value of the '<em><b>Graph Matcher</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeMatchingPolicy <em>Edge Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Matcher</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Matcher</em>' reference.
	 * @see #setGraphMatcher(GraphMatcher)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIEdgeMatchingPolicy_GraphMatcher()
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeMatchingPolicy
	 * @model opposite="edgeMatchingPolicy" required="true"
	 * @generated
	 */
	GraphMatcher getGraphMatcher();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Matcher</em>' reference.
	 * @see #getGraphMatcher()
	 * @generated
	 */
	void setGraphMatcher(GraphMatcher value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method is responsible for checking matching constraints,
	 * a) a context edge matches only already bound links.
	 * b) a produced edge matches only already unbound links.
	 * c) a gray edge may be matched to bound or unbound links.
	 * 
	 * Subclasses may extend this behavior to consider and evaluate further
	 * constraints.
	 * 
	 * NOTE that the graph matcher already checks type conformance.
	 * <!-- end-model-doc -->
	 * @model candidateVirtualModelEdgeMany="false"
	 * @generated
	 */
	boolean edgeMatches(Edge edge, EList<EObject> candidateVirtualModelEdge);

} // IEdgeMatchingPolicy
