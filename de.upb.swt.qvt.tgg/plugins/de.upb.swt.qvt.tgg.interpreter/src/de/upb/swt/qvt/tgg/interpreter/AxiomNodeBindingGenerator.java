/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Axiom Node Binding Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getAxiomNodeBindingGenerator()
 * @model
 * @generated
 */
public interface AxiomNodeBindingGenerator extends TupelGenerator {

} // AxiomNodeBindingGenerator
