/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Front Transformation Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The FrontTransformationProcessor performs the transformation based
 * on the concept of "the front". The front is a list of nodeBindings of such 
 * nodes that are bound to objects that have yet unbound neighbors 
 * (considering those  neightbor objects that can be reached via relevant 
 * referenced, see de.upb.swt.qvt.tgg.TripleGraphGrammar.getRelevantReferences()).
 * 
 * Therefore, the methods ruleApplicable(),createInitialRuleBinding(), and
 * ruleSuccessfullyApplied(ruleBinding) are implemented by the following principle:
 * 
 * createInitialRuleBinding() -- systematically creates initial RuleBindings.
 * 	It does so by traversing the front node bindings and, for each front
 * 	node binding, creates an initial rule binding with every rule in the rule
 * 	set that has a accordingly typed context node in the according domain.
 * 	In case that no global node binding exists yet, it systematically creates
 * 	initial bindings for the axiom. It does so by iterating over the source nodes
 * 	of the axiom and then iterating over potentially matching source domain objects.
 * 
 * ruleApplicable() -- rules remain applicable as long as
 * 	a) the front is not empty
 * 	b) we have not tried all combinations of matching front node bindings with
 * 	according rule nodes.
 * 
 * ruleSuccessfullyApplied(ruleBinding) -- in addition to the inheriting method, we
 * 	call the frontManager.update() operation here, which triggers an update
 * 	of the front node binding list.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager <em>Front Manager</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getSourceDomainObjects <em>Source Domain Objects</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getCurrentTGGRule <em>Current TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getUnmatchedSourceDomainObjects <em>Unmatched Source Domain Objects</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontTransformationProcessor()
 * @model
 * @generated
 */
public interface FrontTransformationProcessor extends TransformationProcessor {
	/**
	 * Returns the value of the '<em><b>Front Manager</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor <em>Front Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Front Manager</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Front Manager</em>' containment reference.
	 * @see #setFrontManager(FrontManager)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontTransformationProcessor_FrontManager()
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor
	 * @model opposite="frontTransformationProcessor" containment="true" required="true"
	 * @generated
	 */
	FrontManager getFrontManager();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager <em>Front Manager</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Front Manager</em>' containment reference.
	 * @see #getFrontManager()
	 * @generated
	 */
	void setFrontManager(FrontManager value);

	/**
	 * Returns the value of the '<em><b>Source Domain Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is a set of all source domain objects that can be
	 * potentially matched by TGG of the current configuration.
	 * 
	 * To determine this set, we traverse all objects in all source
	 * domain resources and check whether they could be potentially
	 * bound to nodes in the TGG.
	 * 
	 * This way, we also determine the VIEW on the source model
	 * that is considered by the transformation, i.e. the transformation 
	 * is successful when all objects in this set are bound.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Source Domain Objects</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontTransformationProcessor_SourceDomainObjects()
	 * @model transient="true" changeable="false" derived="true" ordered="false"
	 * @generated
	 */
	EList<EObject> getSourceDomainObjects();

	/**
	 * Returns the value of the '<em><b>Current TGG Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current TGG Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current TGG Rule</em>' reference.
	 * @see #setCurrentTGGRule(TripleGraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontTransformationProcessor_CurrentTGGRule()
	 * @model
	 * @generated
	 */
	TripleGraphGrammarRule getCurrentTGGRule();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getCurrentTGGRule <em>Current TGG Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current TGG Rule</em>' reference.
	 * @see #getCurrentTGGRule()
	 * @generated
	 */
	void setCurrentTGGRule(TripleGraphGrammarRule value);

	/**
	 * Returns the value of the '<em><b>Unmatched Source Domain Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unmatched Source Domain Objects</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unmatched Source Domain Objects</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getFrontTransformationProcessor_UnmatchedSourceDomainObjects()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<EObject> getUnmatchedSourceDomainObjects();

} // FrontTransformationProcessor
