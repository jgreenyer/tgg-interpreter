/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Axiom Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding#getInitialAxiomNodeBinding <em>Initial Axiom Node Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getAxiomBinding()
 * @model
 * @generated
 */
public interface AxiomBinding extends RuleBinding {
	/**
	 * Returns the value of the '<em><b>Initial Axiom Node Binding</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Axiom Node Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Axiom Node Binding</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getAxiomBinding_InitialAxiomNodeBinding()
	 * @model containment="true"
	 * @generated
	 */
	EList<InitialAxiomNodeBinding> getInitialAxiomNodeBinding();

} // AxiomBinding
