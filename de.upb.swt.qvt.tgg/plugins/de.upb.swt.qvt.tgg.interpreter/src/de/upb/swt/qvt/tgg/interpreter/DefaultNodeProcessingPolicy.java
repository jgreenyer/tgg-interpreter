/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getDefaultNodeProcessingPolicy()
 * @model
 * @generated
 */
public interface DefaultNodeProcessingPolicy extends BaseNodeProcessingPolicy {
} // DefaultNodeProcessingPolicy
