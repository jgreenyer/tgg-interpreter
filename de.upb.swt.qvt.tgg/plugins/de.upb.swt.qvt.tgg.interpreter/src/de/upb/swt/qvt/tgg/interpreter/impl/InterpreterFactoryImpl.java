/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.ocl.ecore.OCL;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterFactoryImpl extends EFactoryImpl implements InterpreterFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InterpreterFactory init() {
		try {
			InterpreterFactory theInterpreterFactory = (InterpreterFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.tgg.interpreter"); 
			if (theInterpreterFactory != null) {
				return theInterpreterFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InterpreterFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InterpreterPackage.INTERPRETER: return createInterpreter();
			case InterpreterPackage.FRONT_MANAGER: return createFrontManager();
			case InterpreterPackage.RULE_PROCESSOR: return createRuleProcessor();
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR: return createFrontTransformationProcessor();
			case InterpreterPackage.GRAPH_MATCHER: return createGraphMatcher();
			case InterpreterPackage.DEFAULT_NODE_PROCESSING_POLICY: return createDefaultNodeProcessingPolicy();
			case InterpreterPackage.DEFAULT_EDGE_PROCESSING_POLICY: return createDefaultEdgeProcessingPolicy();
			case InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY: return createDefaultOCLConstraintProcessingPolicy();
			case InterpreterPackage.NODE_TO_CONSTRAINT_MAP_ENTRY: return (EObject)createNodeToConstraintMapEntry();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER: return createTransformationProcessorHelper();
			case InterpreterPackage.DEBUG_SUPPORT: return createDebugSupport();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case InterpreterPackage.COLLECTION:
				return createCollectionFromString(eDataType, initialValue);
			case InterpreterPackage.SETTING:
				return createSettingFromString(eDataType, initialValue);
			case InterpreterPackage.CONSTRAINT_UNPROCESSABLE_EXCEPTION:
				return createConstraintUnprocessableExceptionFromString(eDataType, initialValue);
			case InterpreterPackage.OCL:
				return createOCLFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case InterpreterPackage.COLLECTION:
				return convertCollectionToString(eDataType, instanceValue);
			case InterpreterPackage.SETTING:
				return convertSettingToString(eDataType, instanceValue);
			case InterpreterPackage.CONSTRAINT_UNPROCESSABLE_EXCEPTION:
				return convertConstraintUnprocessableExceptionToString(eDataType, instanceValue);
			case InterpreterPackage.OCL:
				return convertOCLToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter createInterpreter() {
		InterpreterImpl interpreter = new InterpreterImpl();
		return interpreter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrontManager createFrontManager() {
		FrontManagerImpl frontManager = new FrontManagerImpl();
		return frontManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleProcessor createRuleProcessor() {
		RuleProcessorImpl ruleProcessor = new RuleProcessorImpl();
		return ruleProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrontTransformationProcessor createFrontTransformationProcessor() {
		FrontTransformationProcessorImpl frontTransformationProcessor = new FrontTransformationProcessorImpl();
		return frontTransformationProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher createGraphMatcher() {
		GraphMatcherImpl graphMatcher = new GraphMatcherImpl();
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultNodeProcessingPolicy createDefaultNodeProcessingPolicy() {
		DefaultNodeProcessingPolicyImpl defaultNodeProcessingPolicy = new DefaultNodeProcessingPolicyImpl();
		return defaultNodeProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultEdgeProcessingPolicy createDefaultEdgeProcessingPolicy() {
		DefaultEdgeProcessingPolicyImpl defaultEdgeProcessingPolicy = new DefaultEdgeProcessingPolicyImpl();
		return defaultEdgeProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultOCLConstraintProcessingPolicy createDefaultOCLConstraintProcessingPolicy() {
		DefaultOCLConstraintProcessingPolicyImpl defaultOCLConstraintProcessingPolicy = new DefaultOCLConstraintProcessingPolicyImpl();
		return defaultOCLConstraintProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Node, EList<Constraint>> createNodeToConstraintMapEntry() {
		NodeToConstraintMapEntryImpl nodeToConstraintMapEntry = new NodeToConstraintMapEntryImpl();
		return nodeToConstraintMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessorHelper createTransformationProcessorHelper() {
		TransformationProcessorHelperImpl transformationProcessorHelper = new TransformationProcessorHelperImpl();
		return transformationProcessorHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DebugSupport createDebugSupport() {
		DebugSupportImpl debugSupport = new DebugSupportImpl();
		return debugSupport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection<?> createCollectionFromString(EDataType eDataType, String initialValue) {
		return (Collection<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCollectionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature.Setting createSettingFromString(EDataType eDataType, String initialValue) {
		return (EStructuralFeature.Setting)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSettingToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintUnprocessableException createConstraintUnprocessableExceptionFromString(EDataType eDataType, String initialValue) {
		return (ConstraintUnprocessableException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConstraintUnprocessableExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCL createOCLFromString(EDataType eDataType, String initialValue) {
		return (OCL)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOCLToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterPackage getInterpreterPackage() {
		return (InterpreterPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InterpreterPackage getPackage() {
		return InterpreterPackage.eINSTANCE;
	}

} //InterpreterFactoryImpl
