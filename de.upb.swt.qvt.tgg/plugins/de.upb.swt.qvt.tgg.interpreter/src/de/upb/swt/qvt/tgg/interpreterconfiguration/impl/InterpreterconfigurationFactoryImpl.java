/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Severity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterconfigurationFactoryImpl extends EFactoryImpl implements InterpreterconfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InterpreterconfigurationFactory init() {
		try {
			InterpreterconfigurationFactory theInterpreterconfigurationFactory = (InterpreterconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.tgg.interpreter.interpreterconfiguration"); 
			if (theInterpreterconfigurationFactory != null) {
				return theInterpreterconfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InterpreterconfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterconfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InterpreterconfigurationPackage.CONFIGURATION: return createConfiguration();
			case InterpreterconfigurationPackage.DOMAIN_MODEL: return createDomainModel();
			case InterpreterconfigurationPackage.EOBJECT_TO_NODE_MAP_ENTRY: return (EObject)createEObjectToNodeMapEntry();
			case InterpreterconfigurationPackage.VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY: return (EObject)createVirtualModelEdgeToEdgeMapEntry();
			case InterpreterconfigurationPackage.RULE_BINDING: return createRuleBinding();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO: return createApplicationScenario();
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER: return createRuleBindingContainer();
			case InterpreterconfigurationPackage.TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY: return (EObject)createTypedModelToDomainModelMapEntry();
			case InterpreterconfigurationPackage.TGG_RULE_TO_NODES_MAP_ENTRY: return (EObject)createTGGRuleToNodesMapEntry();
			case InterpreterconfigurationPackage.NODE_TO_EOBJECT_MAP_ENTRY: return (EObject)createNodeToEObjectMapEntry();
			case InterpreterconfigurationPackage.EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY: return (EObject)createEdgeToVirtualModelEdgeMapEntry();
			case InterpreterconfigurationPackage.AXIOM_BINDING: return createAxiomBinding();
			case InterpreterconfigurationPackage.INITIAL_AXIOM_NODE_BINDING: return createInitialAxiomNodeBinding();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case InterpreterconfigurationPackage.APPLICATION_MODE:
				return createApplicationModeFromString(eDataType, initialValue);
			case InterpreterconfigurationPackage.SEVERITY:
				return createSeverityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case InterpreterconfigurationPackage.APPLICATION_MODE:
				return convertApplicationModeToString(eDataType, instanceValue);
			case InterpreterconfigurationPackage.SEVERITY:
				return convertSeverityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration createConfiguration() {
		ConfigurationImpl configuration = new ConfigurationImpl();
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainModel createDomainModel() {
		DomainModelImpl domainModel = new DomainModelImpl();
		return domainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EObject, EList<Node>> createEObjectToNodeMapEntry() {
		EObjectToNodeMapEntryImpl eObjectToNodeMapEntry = new EObjectToNodeMapEntryImpl();
		return eObjectToNodeMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EList<EObject>, EList<Edge>> createVirtualModelEdgeToEdgeMapEntry() {
		VirtualModelEdgeToEdgeMapEntryImpl virtualModelEdgeToEdgeMapEntry = new VirtualModelEdgeToEdgeMapEntryImpl();
		return virtualModelEdgeToEdgeMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding createRuleBinding() {
		RuleBindingImpl ruleBinding = new RuleBindingImpl();
		return ruleBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationScenario createApplicationScenario() {
		ApplicationScenarioImpl applicationScenario = new ApplicationScenarioImpl();
		return applicationScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBindingContainer createRuleBindingContainer() {
		RuleBindingContainerImpl ruleBindingContainer = new RuleBindingContainerImpl();
		return ruleBindingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<TypedModel, DomainModel> createTypedModelToDomainModelMapEntry() {
		TypedModelToDomainModelMapEntryImpl typedModelToDomainModelMapEntry = new TypedModelToDomainModelMapEntryImpl();
		return typedModelToDomainModelMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<TripleGraphGrammarRule, EList<Node>> createTGGRuleToNodesMapEntry() {
		TGGRuleToNodesMapEntryImpl tggRuleToNodesMapEntry = new TGGRuleToNodesMapEntryImpl();
		return tggRuleToNodesMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Node, EObject> createNodeToEObjectMapEntry() {
		NodeToEObjectMapEntryImpl nodeToEObjectMapEntry = new NodeToEObjectMapEntryImpl();
		return nodeToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Edge, EList<EObject>> createEdgeToVirtualModelEdgeMapEntry() {
		EdgeToVirtualModelEdgeMapEntryImpl edgeToVirtualModelEdgeMapEntry = new EdgeToVirtualModelEdgeMapEntryImpl();
		return edgeToVirtualModelEdgeMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxiomBinding createAxiomBinding() {
		AxiomBindingImpl axiomBinding = new AxiomBindingImpl();
		return axiomBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialAxiomNodeBinding createInitialAxiomNodeBinding() {
		InitialAxiomNodeBindingImpl initialAxiomNodeBinding = new InitialAxiomNodeBindingImpl();
		return initialAxiomNodeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationMode createApplicationModeFromString(EDataType eDataType, String initialValue) {
		ApplicationMode result = ApplicationMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertApplicationModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Severity createSeverityFromString(EDataType eDataType, String initialValue) {
		Severity result = Severity.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSeverityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterconfigurationPackage getInterpreterconfigurationPackage() {
		return (InterpreterconfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InterpreterconfigurationPackage getPackage() {
		return InterpreterconfigurationPackage.eINSTANCE;
	}

} //InterpreterconfigurationFactoryImpl
