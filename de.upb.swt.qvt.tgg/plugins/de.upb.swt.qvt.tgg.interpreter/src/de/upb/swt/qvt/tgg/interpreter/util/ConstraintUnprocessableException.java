package de.upb.swt.qvt.tgg.interpreter.util;

public class ConstraintUnprocessableException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -147745080470216901L;

	public ConstraintUnprocessableException(String message) {
		super(message);
	}
}
