/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IElement Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getIElementBinding()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IElementBinding extends EObject {
} // IElementBinding
