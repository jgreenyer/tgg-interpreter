/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.GraphPattern;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.ReusablePattern;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultNodeProcessingPolicyImpl extends BaseNodeProcessingPolicyImpl implements DefaultNodeProcessingPolicy {
	@Override
	public void enforceNode(Node node, EObject eObject) {
		return; //nothing to do, object has already been created
	}

	@Override
	public void removeNode(Node node, EObject eObject) {
		return; //nothing to do, object can simply be deleted
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultNodeProcessingPolicyImpl() {
		super();
	}
	
	@Override
	public EList<Edge> getUnboundEdgeToMatchNext(Node node, EObject eObject) {
		EList<Edge> returnList = new UniqueEList<Edge>();

		// if the graph matcher is in matchReusablePatternOnly mode,
		if (getGraphMatcher().isMatchReusablePatternOnly()){
			
			// Always add outgoing reusable edges that are typed over a single-valued reference and for which the candidate object already specifies a link that is globally bound.
			// (i.e., a reusable node can only be reused if all such edges can be reused)
			for (Edge edge : node.getAllOutgoingEdges()) {
				if (getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue; //unless the edge was already matched...
				if (edge.isReusable() && !edge.getTypeReference().isMany() && eObject.eGet(edge.getTypeReference()) != null){
					EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
					virtualModelEdge.add(eObject);
					virtualModelEdge.add((EObject) eObject.eGet(edge.getTypeReference()));
					virtualModelEdge.add(edge.getTypeReference());
					if (!(getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge) == null
							|| getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge).isEmpty())){
						returnList.add(edge);
					}
				}
			}
			// Always add incoming reusable edges that are typed over a single-valued reference 
			// and where the source node is already bound within the current rule binding to an object that specifies a value for that reference
			// and where this value (->link) is also already globally bound, i.e., resetting this value will corrupt a global edge binding.
			//
			// Furthermore, also add incoming edges that are typed over containment edges if the candidate object already has a container, 
			// and the containment link is already globally bound.
			for (Edge edge : node.getAllIncomingEdges()) {
				if (getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue; //unless the edge was already matched...
				EObject currentCandidateSourceObject = getGraphMatcher().getRuleBinding().getNodeToEObjectMap().get(edge.getSourceNode());
				if (edge.isReusable() && !edge.getTypeReference().isMany() && currentCandidateSourceObject != null && currentCandidateSourceObject.eGet(edge.getTypeReference()) != null){
					EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
					virtualModelEdge.add(currentCandidateSourceObject);
					virtualModelEdge.add((EObject) currentCandidateSourceObject.eGet(edge.getTypeReference()));
					virtualModelEdge.add(edge.getTypeReference());
					if (!(getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge) == null
							|| getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge).isEmpty())){
						returnList.add(edge);
					}
				}
				if (edge.isReusable() && edge.getTypeReference().isContainment() && eObject.eContainer() != null){
//					EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
//					virtualModelEdge.add(eObject.eContainer());
//					virtualModelEdge.add(eObject);
//					virtualModelEdge.add(edge.getTypeReference());
//					if (!(getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge) == null
//							|| getTransformationProcessor().getGlobalEdgeBinding().get(virtualModelEdge).isEmpty())){
						returnList.add(edge);
//					}
				}
			}
			
			
			
			// find out if the (reusable) node is in any reusable pattern,
			// do this by listing all the reusable patterns that contain the node
			EList<ReusablePattern> containingReusablePatterns = getContainingReusablePatterns(node);
			// if the node is in a reusable pattern, return unmatched reusable edges to other 
			// reusable nodes in that pattern
			if (!containingReusablePatterns.isEmpty()){
				for (ReusablePattern reusablePattern : containingReusablePatterns) {
					for (Edge edge : node.getAllIncomingEdges()) {
						// do not consider edges that are not reusable
						if (!edge.isReusable() || getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue;
						
						Node sourceNode = getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getSourceNode());
						if (!sourceNode.isReusable()) continue;
						
						// add unmatched edges from nodes in the reusable pattern
						if (reusablePattern.getNode().contains(edge.getSourceNode()))
							returnList.add(edge);
					}
					for (Edge edge : node.getAllOutgoingEdges()) {
						// do not consider edges that are not reusable
						if (!edge.isReusable() || getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue;
						
						Node targetNode = getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getTargetNode());
						if (!targetNode.isReusable()) continue;

						// add unmatched edges to nodes in the reusable pattern
						if (reusablePattern.getNode().contains(edge.getTargetNode()))
							returnList.add(edge);
					}
				}
			} else{
				/* if not, we only want to return unbound reusable edges for the first node in a matching run. 
				 * We know this if the start node binding of the graph matcher is yet null. Then we 
				 * return an empty list. */
				if (getGraphMatcher().getStartNodeOfMatching() == null || getGraphMatcher().getStartNodeOfMatching() == node){
					for (Edge edge : node.getAllIncomingEdges()) {
						// do not consider edges that are not reusable or already matched
						if (!edge.isReusable() || getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue;
						
						Node sourceNode = getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getSourceNode());
						if (!sourceNode.isReusable()) continue;
						
						returnList.add(edge);
					}
					for (Edge edge : node.getAllOutgoingEdges()) {
						// do not consider edges that are not reusable or already matched
						if (!edge.isReusable() || getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue;
						
						Node targetNode = getRuleProcessor().getTripleGraphGrammarRule().getMostRefinedNodeOf(edge.getTargetNode());
						if (!targetNode.isReusable()) continue;
						
						returnList.add(edge);
					}
				}
			}
		}else{// if the graph matcher is not in matchReusablePatternOnly mode

			EList<Edge> tmpReturnList = new UniqueEList<Edge>();
			tmpReturnList.addAll(node.getAllOutgoingEdges());
			tmpReturnList.addAll(node.getAllIncomingEdges());

			// sort edges in the returnList according to the priority of the neighbor node.
			int i = 0;
			int maxNeighborNodePriority = 0;
			
			while (i <= maxNeighborNodePriority) {
				int edgesWithCurrentPriorityAdded = 0;
				for (Edge edge : tmpReturnList) {
					
					
					Node neighborNode;
					if (edge.getSourceNode() == node)
						neighborNode = edge.getTargetNode();
					else
						neighborNode = edge.getSourceNode();
					
					// determine the maximum matchingprioty among the neighbor nodes.
					if (maxNeighborNodePriority < neighborNode.getMatchingPriority())
						maxNeighborNodePriority = neighborNode.getMatchingPriority(); 
					
					// stop this loop iteration of the neighbor node's priority is smaller than i. Then the edge is already added to the return list.
					if (neighborNode.getMatchingPriority() != i) continue;

					// a) only include edges in the context and source domain patterns:
					if (!(edge.isLeft() || edgeBelongsToDomainToMatch(edge))) continue;

					// b) Do not include edges that are already bound in this rule binding:
					if (getGraphMatcher().getRuleBinding().getGraphElementStack().contains(edge)) continue;
		
					// Optimization idea: Do not include incoming edges when an outgoing edge
					// typed over the incoming edge's eOpposite reference exists.
					if (node.getAllIncomingEdges().contains(edge) 
							&& edge.getTypeReference().getEOpposite() != null){
						boolean oppositeOutgoingEdgeExists = false;
						for (Edge outgoingEdge : node.getAllOutgoingEdges()) {
							if (edge.getTypeReference().getEOpposite() == outgoingEdge.getTypeReference()){
								oppositeOutgoingEdgeExists = true;
								break;
							}
						}
						if (oppositeOutgoingEdgeExists) continue;
					}
			

					
					
					// c) move edges typed over single-valued references to the beginning of the list.
					if (!edge.getTypeReference().isMany() 
							/* and not incoming edge: */ && !node.getAllIncomingEdges().contains(edge)){
						returnList.add(0, edge);
					}else{
						returnList.add(edgesWithCurrentPriorityAdded, edge);
					}

					edgesWithCurrentPriorityAdded++;

				}
				i++;
			}
			
			
		}
		return returnList;
	}
	
	protected boolean edgeBelongsToDomainToMatch(Edge edge){
		Node tggNode = edge.getSourceNode();
		TripleGraphGrammarRule tggRule = (TripleGraphGrammarRule) tggNode.getGraph();
		for (DomainModel domainToMatch : getGraphMatcher().getDomainsToMatch()) {
			if (tggRule.getDomainsNodesMap().get(domainToMatch.getTypedModel()).contains(edge.getSourceNode())) return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 	 * This method is responsible for checking matching constraints,
	 * a) a context node matches only already bound objects.
	 * b) a produced node matches only unbound objects.
	 * c) reusable node may be matched to bound or unbound objects.
	 * 	c1)
	 * 
	 * Subclasses may extend this behavior to consider and evaluate further
	 * constraints.
	 * 
	 * NOTE that the graph matcher already checks type conformance.
	 * <!-- end-user-doc -->
	 */
	@Override
	public boolean nodeMatches(Node node, EObject candidateEObject) {

		// context node does not match a not globally bound object
		if (node.isContext() 
				&& !getTransformationProcessor().getGlobalNodeBinding().keySet().contains(candidateEObject)){
			return false;
			}
		// jgreen: what's that? context node is not globally bound to another node from another domain?? 
		if (node.isContext() 
				&& (getTransformationProcessor().getGlobalNodeBinding().get(candidateEObject).get(0).getDomainGraphPattern().getTypedModel() 
				 != node.getDomainGraphPattern().getTypedModel())
				)
			return false;
		// produced node does not match a globally bound object
		if (node.isProduced() 
				&& (getTransformationProcessor().getGlobalNodeBinding().keySet().contains(candidateEObject)))
			return false;
		
		
		// consider reusable pattern semantics: nodes in a reusable pattern must be either all bound 
		// to already globally bound objects or they must be all bound to yet globally unbound objects
		if (node.isReusable()){
			EList<ReusablePattern> containingReusablePatterns = getContainingReusablePatterns(node);
			if (!containingReusablePatterns.isEmpty()){
				// 1. is candidate object globally bound?
				boolean candidateObjectGloballyBound = getTransformationProcessor().getGlobalNodeBinding().keySet().contains(candidateEObject);
				for (ReusablePattern reusablePattern : containingReusablePatterns) {
					// 2. if the candidate object is globally bound, so must be any other object that is matched by a node in a containing reusable pattern
					// So, for every node in the reusable pattern
					for (Node otherNodeInReusablePattern : reusablePattern.getNode()) {
						// get the currently matched object
						EObject objectMatchedToOtherNodeInReusablePattern = getGraphMatcher().getRuleBinding().getNodeToEObjectMap().get(otherNodeInReusablePattern);
						// which may be null
						if (objectMatchedToOtherNodeInReusablePattern != null){
							// but if it isn't null, then 
							// 1. Either the candidate object is globally bound AND the matched object is already globally bound
							// 2. or the candidate object is not globally bound AND the matched object is not globally bound
							if (getTransformationProcessor().getGlobalNodeBinding().keySet().contains(objectMatchedToOtherNodeInReusablePattern)
									!= candidateObjectGloballyBound){
								return false;
							}
						}
					}
				}
			}
		}
		
		
		//Check if produced node matching is constrained
		//FIXME: move this check to own NodeProcessingPolicy
		if (node.isProduced() && getGraphMatcher().isRestrictProducedGraphMatching()
				&& (getGraphMatcher().getElementsNotAvailableForProducedNodes().contains(candidateEObject)))
			return false;
		
		return true;
	}
	
	private EList<ReusablePattern> getContainingReusablePatterns(Node node){
		EList<ReusablePattern> containingReusablePatterns = new BasicEList<ReusablePattern>();
		for (GraphPattern graphPattern : node.getAllGraphPatterns()) {
			if (graphPattern instanceof ReusablePattern){
				containingReusablePatterns.add((ReusablePattern) graphPattern);
			}
		}
		return containingReusablePatterns;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.DEFAULT_NODE_PROCESSING_POLICY;
	}

} //DefaultNodeProcessingPolicyImpl
