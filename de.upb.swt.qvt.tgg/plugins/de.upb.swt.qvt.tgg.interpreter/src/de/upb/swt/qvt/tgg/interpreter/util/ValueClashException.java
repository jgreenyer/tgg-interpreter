package de.upb.swt.qvt.tgg.interpreter.util;

public class ValueClashException extends Exception {

	/**
	 * The UID for serialization.
	 */
	private static final long serialVersionUID = -5015667101085839782L;

	public ValueClashException(String message) {
		super(message);
	}
	
}
