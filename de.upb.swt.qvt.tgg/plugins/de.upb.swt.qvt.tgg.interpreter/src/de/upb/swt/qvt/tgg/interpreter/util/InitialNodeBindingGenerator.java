package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.query.conditions.Condition;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;

public class InitialNodeBindingGenerator extends TupleGenerator {

	private ApplicationScenario applicationScenario;

	
	public ApplicationScenario getApplicationScenario() {
		return applicationScenario;
	}

	public void setApplicationScenario(ApplicationScenario applicationScenario) {
		this.applicationScenario = applicationScenario;
	}

	public InitialNodeBindingGenerator(Object[][] inputArrays, Condition tupleConstraint) {
		super(inputArrays, tupleConstraint);
	}

	/**
	 * When the pointer on the TGG rule array changes (position 0), change the
	 * TGG node array at position 2 according to the new TGG rule at position 0.
	 */
	@Override
	protected boolean increaseInputArrayPointer() {

		// return false when the binding generator is not properly initialized
		if (inputArrays.length < 3)
			return false;

		if (inputArrays[2] == null) {
			inputArrays[2] = ((TripleGraphGrammarRule) inputArrays[0][0]).getAllNodes().toArray(new EObject[0]);
		}

		int inputArrayIndex = arrayElementPointer.length - 1;
		boolean increaseInputListPointer = true;
		while (increaseInputListPointer) {
			arrayElementPointer[inputArrayIndex]++;
			increaseInputListPointer = false;
			if (arrayElementPointer[inputArrayIndex] >= inputArrays[inputArrayIndex].length) {
				arrayElementPointer[inputArrayIndex] = 0;
				increaseInputListPointer = true;
				inputArrayIndex--;
				if (inputArrayIndex < 0)
					return false;

			} else {
				// if the pointer on the TGG rules change, change TGG node array
				// (at position 2)
				// according to the new TGG rule at pos 0;
				if (inputArrayIndex == 0) {

					replaceInputArray(2, getSortedContextNodeArray(applicationScenario, ((TripleGraphGrammarRule) getArrayElement(inputArrayIndex,
							arrayElementPointer[inputArrayIndex]))));
				}
			}
		}

		return true;
	}

	@Override
	public void setArrayOffset(int inputArrayIndex, int offset) {

		// if the offset for the TGG array is changed, replace the TGG nodes array.
		if (inputArrayIndex == 0 && offset != arrayOffset[0]) {
			replaceInputArray(2, ((TripleGraphGrammarRule) getArrayElement(0, offset))
					.getAllNodes().toArray());
		}
		
		super.setArrayOffset(inputArrayIndex, offset);
	}
	
	
	public static Node[] getSortedContextNodeArray(ApplicationScenario applicationScenario, TripleGraphGrammarRule tggRule){
		// the nodes that we need to fill position 2 of the array with depend on whether in the current application scenario the front is active of not
		// IF   the front is deactivated, position 2 must be simply filled with ONE context node
		// ELSE position 2 must be filled with ALL context nodes
		// 			(because we don't know if all kinds of objects that can bind to one particular context node will be in the front, 
		//           therefore we have to try all combinations of context nodes with all objects in the front)
		
		if (applicationScenario.isDeactivateFront()){  
			Node startNode = null;
			for (Node node : applicationScenario.getSourceDomainNodesByTGGRule().get(tggRule)){
					if (node.isContext()){

					if (startNode == null || node.getMatchingPriority() >= startNode.getMatchingPriority()){
						startNode = node;
					}
				}
			}
			return new Node[]{startNode};
		}else{
			EList<Node> allSourceDomainContextRuleNodes = new BasicEList<Node>();
			for (Node node : applicationScenario.getSourceDomainNodesByTGGRule().get(tggRule)){
				if (node.isContext()){

					if (!allSourceDomainContextRuleNodes.isEmpty() && allSourceDomainContextRuleNodes.get(0).getMatchingPriority() < node.getMatchingPriority())
						allSourceDomainContextRuleNodes.add(0, node);	
					else
						allSourceDomainContextRuleNodes.add(node);
				}
			}
			return allSourceDomainContextRuleNodes.toArray(new Node[0]);
		}
	}
	
}
