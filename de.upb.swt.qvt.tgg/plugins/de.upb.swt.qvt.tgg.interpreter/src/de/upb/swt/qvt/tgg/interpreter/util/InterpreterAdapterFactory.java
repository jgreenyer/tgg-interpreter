/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage
 * @generated
 */
public class InterpreterAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static InterpreterPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = InterpreterPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterSwitch<Adapter> modelSwitch =
		new InterpreterSwitch<Adapter>() {
			@Override
			public Adapter caseInterpreter(Interpreter object) {
				return createInterpreterAdapter();
			}
			@Override
			public Adapter caseFrontManager(FrontManager object) {
				return createFrontManagerAdapter();
			}
			@Override
			public Adapter caseRuleProcessor(RuleProcessor object) {
				return createRuleProcessorAdapter();
			}
			@Override
			public Adapter caseTransformationProcessor(TransformationProcessor object) {
				return createTransformationProcessorAdapter();
			}
			@Override
			public Adapter caseINodeMatchingPolicy(INodeMatchingPolicy object) {
				return createINodeMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseIEdgeMatchingPolicy(IEdgeMatchingPolicy object) {
				return createIEdgeMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseFrontTransformationProcessor(FrontTransformationProcessor object) {
				return createFrontTransformationProcessorAdapter();
			}
			@Override
			public Adapter caseGraphMatcher(GraphMatcher object) {
				return createGraphMatcherAdapter();
			}
			@Override
			public Adapter caseBaseNodeProcessingPolicy(BaseNodeProcessingPolicy object) {
				return createBaseNodeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseBaseEdgeProcessingPolicy(BaseEdgeProcessingPolicy object) {
				return createBaseEdgeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseIConstraintCheckingPolicy(IConstraintCheckingPolicy object) {
				return createIConstraintCheckingPolicyAdapter();
			}
			@Override
			public Adapter caseBaseConstraintProcessingPolicy(BaseConstraintProcessingPolicy object) {
				return createBaseConstraintProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseINodeEnforcementPolicy(INodeEnforcementPolicy object) {
				return createINodeEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseIEdgeEnforcementPolicy(IEdgeEnforcementPolicy object) {
				return createIEdgeEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseIConstraintEnforcementPolicy(IConstraintEnforcementPolicy object) {
				return createIConstraintEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseDefaultNodeProcessingPolicy(DefaultNodeProcessingPolicy object) {
				return createDefaultNodeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseDefaultEdgeProcessingPolicy(DefaultEdgeProcessingPolicy object) {
				return createDefaultEdgeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseDefaultOCLConstraintProcessingPolicy(DefaultOCLConstraintProcessingPolicy object) {
				return createDefaultOCLConstraintProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseNodeToConstraintMapEntry(Map.Entry<Node, EList<Constraint>> object) {
				return createNodeToConstraintMapEntryAdapter();
			}
			@Override
			public Adapter caseIMatchingPolicy(IMatchingPolicy object) {
				return createIMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseTransformationProcessorHelper(TransformationProcessorHelper object) {
				return createTransformationProcessorHelperAdapter();
			}
			@Override
			public Adapter caseDebugSupport(DebugSupport object) {
				return createDebugSupportAdapter();
			}
			@Override
			public Adapter caseIDebugEventListener(IDebugEventListener object) {
				return createIDebugEventListenerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter <em>Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter
	 * @generated
	 */
	public Adapter createInterpreterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager <em>Front Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontManager
	 * @generated
	 */
	public Adapter createFrontManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor
	 * @generated
	 */
	public Adapter createRuleProcessorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor
	 * @generated
	 */
	public Adapter createTransformationProcessorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy <em>INode Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy
	 * @generated
	 */
	public Adapter createINodeMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy <em>IEdge Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy
	 * @generated
	 */
	public Adapter createIEdgeMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor <em>Front Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor
	 * @generated
	 */
	public Adapter createFrontTransformationProcessorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher
	 * @generated
	 */
	public Adapter createGraphMatcherAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy <em>Base Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseNodeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy <em>Base Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseEdgeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy <em>IConstraint Checking Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy
	 * @generated
	 */
	public Adapter createIConstraintCheckingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy <em>Base Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseConstraintProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy <em>INode Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy
	 * @generated
	 */
	public Adapter createINodeEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy <em>IEdge Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy
	 * @generated
	 */
	public Adapter createIEdgeEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy <em>IConstraint Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy
	 * @generated
	 */
	public Adapter createIConstraintEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy <em>Default Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy
	 * @generated
	 */
	public Adapter createDefaultNodeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy <em>Default Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy
	 * @generated
	 */
	public Adapter createDefaultEdgeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy <em>Default OCL Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy
	 * @generated
	 */
	public Adapter createDefaultOCLConstraintProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Node To Constraint Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createNodeToConstraintMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy <em>IMatching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy
	 * @generated
	 */
	public Adapter createIMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper <em>Transformation Processor Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper
	 * @generated
	 */
	public Adapter createTransformationProcessorHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport <em>Debug Support</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport
	 * @generated
	 */
	public Adapter createDebugSupportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IDebugEventListener <em>IDebug Event Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IDebugEventListener
	 * @generated
	 */
	public Adapter createIDebugEventListenerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //InterpreterAdapterFactory
