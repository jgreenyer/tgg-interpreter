/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IEdge Enforcement Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getIEdgeEnforcementPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IEdgeEnforcementPolicy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model virtualModelEdgeMany="false"
	 * @generated
	 */
	void enforceEdge(Edge edge, EList<EObject> virtualModelEdge);

} // IEdgeEnforcementPolicy
