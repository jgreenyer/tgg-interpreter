/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ecore.EcoreEnvironment;
import org.eclipse.ocl.ecore.OCL;

import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interpreter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOclEnvironment <em>Ocl Environment</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getDebugSupport <em>Debug Support</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOcl <em>Ocl</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter()
 * @model
 * @generated
 */
public interface Interpreter extends EObject {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' reference.
	 * @see #setConfiguration(Configuration)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter_Configuration()
	 * @model required="true" transient="true"
	 * @generated
	 */
	Configuration getConfiguration();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getConfiguration <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Transformation Processor</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter <em>Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Processor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Processor</em>' containment reference.
	 * @see #setTransformationProcessor(TransformationProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter_TransformationProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter
	 * @model opposite="interpreter" containment="true" required="true"
	 * @generated
	 */
	TransformationProcessor getTransformationProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor <em>Transformation Processor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Processor</em>' containment reference.
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	void setTransformationProcessor(TransformationProcessor value);

	/**
	 * Returns the value of the '<em><b>Ocl Environment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ocl Environment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ocl Environment</em>' attribute.
	 * @see #setOclEnvironment(EcoreEnvironment)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter_OclEnvironment()
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.OCLEnvironment" transient="true"
	 * @generated
	 */
	EcoreEnvironment getOclEnvironment();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOclEnvironment <em>Ocl Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ocl Environment</em>' attribute.
	 * @see #getOclEnvironment()
	 * @generated
	 */
	void setOclEnvironment(EcoreEnvironment value);

	/**
	 * Returns the value of the '<em><b>Debug Support</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug Support</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug Support</em>' containment reference.
	 * @see #setDebugSupport(DebugSupport)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter_DebugSupport()
	 * @model containment="true"
	 * @generated
	 */
	DebugSupport getDebugSupport();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getDebugSupport <em>Debug Support</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug Support</em>' containment reference.
	 * @see #getDebugSupport()
	 * @generated
	 */
	void setDebugSupport(DebugSupport value);

	/**
	 * Returns the value of the '<em><b>Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ocl</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ocl</em>' attribute.
	 * @see #setOcl(OCL)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getInterpreter_Ocl()
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.OCL"
	 * @generated
	 */
	OCL getOcl();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOcl <em>Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ocl</em>' attribute.
	 * @see #getOcl()
	 * @generated
	 */
	void setOcl(OCL value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * see the information on
	 * de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration and
	 * de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void initializeConfiguration() throws IllegalArgumentException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void storeConfiguration();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.Status" monitorDataType="de.upb.swt.qvt.tgg.interpreter.IProgressMonitor"
	 * @generated
	 */
	IStatus performTransformation(IProgressMonitor monitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void storeResult();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isTransformationRunning();

} // Interpreter
