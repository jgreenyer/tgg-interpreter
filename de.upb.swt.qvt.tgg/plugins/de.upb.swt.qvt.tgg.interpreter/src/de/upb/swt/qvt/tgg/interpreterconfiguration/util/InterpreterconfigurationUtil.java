package de.upb.swt.qvt.tgg.interpreterconfiguration.util;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;

public class InterpreterconfigurationUtil {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(InterpreterconfigurationUtil.class.getName());
	
	//private static Map<String, String> resourceURIMap;

	
//	public static Configuration loadInterpreterConfiguration2(URI resourceURI){
//
//		Configuration configuration = null;
//
//		
//		try {
//
//
//			ResourceSet resourceSet = new ResourceSetImpl();
//			Resource interpreterConfigurationResource = resourceSet.getResource(resourceURI, true);
//			
//			configuration = (Configuration) interpreterConfigurationResource.getContents().get(0);
//			
//			checkAndInitInterpreterConfigurationFile(configuration , interpreterConfigurationResource);
//			
//			
//			
//		}catch (Exception e) {
//			logger.error("Error caught while running the Interpreter",e);
//		}
//
//		return configuration;
//		
//	}
	
	
	/**
	 * loads a configuration from the file specified by the resourceURI.
	 * The task achieved by this utility method is that the models and rules are
	 * loaded referencing the same meta model instances, which is an important
	 * precondition for the matching process.
	 * 
	 * @param resourceURI
	 * @return
	 */	
	public static Configuration loadInterpreterConfiguration(URI resourceURI){
		
		Configuration configuration = null;
		
		try {


			ResourceSet resourceSet = new ResourceSetImpl();
			Resource interpreterConfigurationResource = resourceSet.getResource(resourceURI, true);
			
			configuration = (Configuration) interpreterConfigurationResource.getContents().get(0);
			
			loadAndInitTripleGraphGrammar(configuration.getTripleGraphGrammar().eResource().getURI(), resourceSet, !configuration.isNoPackageUriRedirection());
			
			//interpreterConfigurationResource = resourceSet.getResource(resourceURI, true);
			//configuration = (Configuration) interpreterConfigurationResource.getContents().get(0);
			
			
//			if(logger.isDebugEnabled()){
//				Iterator sourceDomainModelIterator = configuration.getActiveApplicationScenario().getSourceDomainModel().iterator();
//				while (sourceDomainModelIterator.hasNext()) {
//					DomainModel sourceDomainModel = (DomainModel) sourceDomainModelIterator.next();
//					logger.debug("sourceDomainModel: " + sourceDomainModel);
//					Iterator sourceDomainModelRootObjectIterator = sourceDomainModel.getRootObject().iterator();
//					while (sourceDomainModelRootObjectIterator.hasNext()) {
//						EObject rootObject = (EObject) sourceDomainModelRootObjectIterator.next();
//						logger.debug(" rootObject: " + rootObject);
//						Iterator contentsIterator = rootObject.eAllContents();
//						while (contentsIterator.hasNext()) {
//							EObject contentsObject = (EObject) contentsIterator.next();
//							logger.debug("  - contentsObject: " + contentsObject);
//						}
//					}
//					
//				}
//			}

		}catch (Exception e) {
			logger.error("Error caught while running the Interpreter",e);
		}

		return configuration;
	}
		

	/**
	 * Loads a TGG from the specified resource and loads the referenced meta models
	 * with the respective model packages registered in the package registry of the
	 * specified resourceSet.
	 * 
	 * For example: A TGG resource may reference another ecore file, ../com.otherplugin/model/othermodel.ecore
	 * Then, if a package for the model's NsURI is registered in the given resourceSet (e.g. http://com.otherplugin/),
	 * this method registeres this package also for the resource URI 
	 * (e.g. platform:/plugin/com.otherplugin/model.othermodel.ecore)
	 * 
	 * @param resourceURI
	 * @return
	 */
	public static TripleGraphGrammar loadAndInitTripleGraphGrammar(URI resourceURI, ResourceSet resourceSet) throws IllegalArgumentException{
		return loadAndInitTripleGraphGrammar(resourceURI, resourceSet, true);
	}

	/**
	 * Loads a TGG from the specified resource and loads the referenced meta models
	 * with the respective model packages registered in the package registry of the
	 * specified resourceSet.
	 * 
	 * For example: A TGG resource may reference another ecore file, ../com.otherplugin/model/othermodel.ecore
	 * Then, if a package for the model's NsURI is registered in the given resourceSet (e.g. http://com.otherplugin/),
	 * this method registeres this package also for the resource URI 
	 * (e.g. platform:/plugin/com.otherplugin/model.othermodel.ecore)
	 * 
	 * @param resourceURI
	 * @param packageUriRedirection Whether to redirect "resource:" file links to its URIs  
	 * @return
	 */
	public static TripleGraphGrammar loadAndInitTripleGraphGrammar(URI resourceURI, ResourceSet resourceSet, boolean packageUriRedirection) throws IllegalArgumentException{
		
		ResourceSet tmpResourceSet = new ResourceSetImpl();
		
		Resource tggResource = tmpResourceSet.getResource(resourceURI, true);
		TripleGraphGrammar tgg = (TripleGraphGrammar) tggResource.getContents().get(0);
		
		if (packageUriRedirection) {
			Iterator<TypedModel> typedModelIterator = tgg.getAllModelParameters().iterator();
			while (typedModelIterator.hasNext()) {
				TypedModel typedModel = (TypedModel) typedModelIterator.next();
				if (logger.isDebugEnabled()){
					logger.debug("### " + typedModel.getUsedPackage());
				}
				Iterator<EPackage> usedPackageIterator = typedModel.getUsedPackage().iterator();
				while (usedPackageIterator.hasNext()) {
					EPackage usedPackage = (EPackage) usedPackageIterator.next();
					redirectPackageURI(usedPackage, resourceSet);
				}
				
			}
		}
		
//		resourceSet.getPackageRegistry().put(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getNsURI().toString(), org.eclipse.emf.ecore.EcorePackage.eINSTANCE);
//		System.out.println(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getNsURI().toString());
//		System.out.println(org.eclipse.emf.ecore.EcorePackage.eINSTANCE);
//		System.out.println(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEFactoryInstance());
//		System.out.println(org.eclipse.emf.ecore.EcorePackage.eINSTANCE);
//
//		//this line is a temporary patch:
//		resourceSet.getPackageRegistry().put(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE.getNsURI().toString(), org.eclipse.ocl.ecore.EcorePackage.eINSTANCE);
//		System.out.println(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE.getNsURI().toString());
//		System.out.println(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE);
//		System.out.println(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE.getEFactoryInstance());
//		System.out.println(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE);


		tggResource = resourceSet.getResource(resourceURI, true);
		tgg = (TripleGraphGrammar) tggResource.getContents().get(0);

		return tgg;

	}
	
	private static void redirectPackageURI(EPackage usedPackage, ResourceSet resourceSet) {
		// jgreen: using the global package registry instead of the resource set specific one can cause problems
		// when editing the TGG later on.

		//move to the topmost package and use that 
		while (usedPackage.getESuperPackage() != null)
			usedPackage = usedPackage.getESuperPackage();
		
		String resURI = usedPackage.eResource().getURI().toString();
		String nsURI = usedPackage.getNsURI().toString();
		EPackage pack = resourceSet.getPackageRegistry().getEPackage(nsURI);
		if (pack != null) {
			resourceSet.getPackageRegistry().put(resURI, pack);
			resourceSet.getPackageRegistry().put(nsURI, pack);
		}
		if (logger.isDebugEnabled()){
			logger.debug("#### REDIRECTING PACKAGE ####");
			logger.debug("#### ResourceURI:           " + resURI);
			logger.debug("#### nsURI:                 " + nsURI);
			if (pack == null)
				logger.debug("#### No package is registered globally, nothing to do.");
			else
				logger.debug("#### Package is registered globally, overriding locally.");
			if (EPackage.Registry.INSTANCE.getEPackage(usedPackage.getNsURI().toString()) != null){
				logger.debug("#### Global pack for nsURI: " + EPackage.Registry.INSTANCE.getEPackage(usedPackage.getNsURI().toString()));
			}
			if (resourceSet.getPackageRegistry().getEPackage(usedPackage.getNsURI().toString()) != null){
				logger.debug("#### RS pack for nsURI:     " + resourceSet.getPackageRegistry().getEPackage(usedPackage.getNsURI().toString()));
			}
		}
//		if (pack != null) {
//			if (pack.getESuperPackage() != null)
//				pack = pack.getESuperPackage(); //start at the root package to make sure we redirect everything 
//			for (EPackage subpack : usedPackage.getESubpackages()) {
//				redirectPackageURI(subpack, resourceSet);
//			}
//		}
	}

	/**
	 * Method for cleaning up the entries in the global Package {@link Registry} (EPackage.Registry.INSTANCE).
	 * When loading the TGG and when working in the runtime workspace (development time), Registry entries for 
	 * nsURIs of the form 'platform:/resource/...' may have been mapped to compile time instances of the package
	 * during transformation.
	 * 
	 * Call this method when the transformation process is finished.
	 * 
	 * @param tgg
	 */
	public static void unloadTripleGraphGrammar(TripleGraphGrammar tgg){
		Registry tggResourcePackageRegistry = tgg.eResource().getResourceSet().getPackageRegistry();
		
		// creating array to avoid ConcurrentModificationExceptions when removing entries from the EPackage.Registry.INSTANCE
		Object[] keyStrings = EPackage.Registry.INSTANCE.keySet().toArray();
		for (int i = 0; i < keyStrings.length; i++) {
			String keyString = (String) keyStrings[i];
			if (keyString.startsWith("platform:/resource/") && tggResourcePackageRegistry.containsValue(EPackage.Registry.INSTANCE.get(keyString))){
				EPackage.Registry.INSTANCE.remove(keyString);
			}
		}
	}
	
	/**
	 * This is a convenience method that calls {@link unloadTripleGraphGrammar}.
	 * @param configuration
	 */
	public static void unloadConfiguration(Configuration configuration){
		unloadTripleGraphGrammar(configuration.getTripleGraphGrammar());
	}
	
	/**
	 * Builds up the resourceURIMap to ensure that the NsURIs of the rule's metamodel are mapped
	 * to the NsURIs of the models metamodel. They might be distinct if the rules were formulated
	 * with respect to some .ecore metamodel file, but the model itself is an instance of the
	 * generated code.
	 * 
	 * @param modelMetaClass
	 * @param ruleMetaClass
	 */
	/*private static void addResourceURIMapEntry(EClass modelMetaClass, EClass ruleMetaClass){

		try{

			String modelObjectsMetaPackageNsURI = modelMetaClass.getEPackage().getNsURI().toString();
			String ruleNodeMetaPackageNsURI = modelMetaClass.getEPackage().getNsURI().toString();
			if (ruleMetaClass.eResource() != null){
				ruleNodeMetaPackageNsURI = ruleMetaClass.eResource().getURI().toString();
			}
			
			if (!modelObjectsMetaPackageNsURI.equals(ruleNodeMetaPackageNsURI)
					&& resourceURIMap.get(modelMetaClass.getEPackage().getNsURI().toString()) == null){
				logger.debug("adding resourceURIMap entry: Mapping");
				logger.debug(" -> " + modelObjectsMetaPackageNsURI);
				logger.debug(" -> " + ruleNodeMetaPackageNsURI);
				
				resourceURIMap.put(modelMetaClass.getEPackage().getNsURI().toString(), ruleMetaClass.eResource().getURI().toString());
			}
		
		}catch (NullPointerException e) {
			logger.error("NullpointerException caught. Recheck 1. The interpreter configuration file, 2. The metamodels, missing NsURIs, NsPrefixes, etc", e);
			return;
		}


		EList<EClass> modelObjectSuperClassList = modelMetaClass.getESuperTypes();
		EList<EClass> nodeSuperClassList = ruleMetaClass.getESuperTypes();
		if (modelObjectSuperClassList.size() != 0){
			if (modelObjectSuperClassList.size() != nodeSuperClassList.size()){
				logger.error("The list of superTypes does not have the same size: " + modelObjectSuperClassList + " and " + nodeSuperClassList);
				return;
			}
			for (int i = 0; i < modelObjectSuperClassList.size(); i++) {
				EClass modelMetaSuperClass = (EClass) modelObjectSuperClassList.get(i);
				EClass ruleMetaSuperClass = (EClass) nodeSuperClassList.get(i);
				
				if (modelMetaSuperClass.getName().equals(ruleMetaSuperClass.getName())){
					addResourceURIMapEntry(modelMetaSuperClass, ruleMetaSuperClass);
				}else{
					logger.error("The two super classes from the lists do not matc: " + modelMetaSuperClass + " and " + ruleMetaSuperClass);
				}
			} 
		}

	}*/

	public static Configuration buildConfiguration(String sourceModelFilePath, TripleGraphGrammar tgg, TypedModel correspondenceTypedModel) throws IllegalArgumentException{

		//ResourceSet resourceSet = new ResourceSetImpl();
		ResourceSet resourceSet = tgg.eResource().getResourceSet();
		Resource sourceModelResource = resourceSet.getResource(URI.createPlatformResourceURI(sourceModelFilePath, true), true);
		EObject sourceRootObject = sourceModelResource.getContents().get(0);
		
		// 1. Create configuration and set TGG
		Configuration configuration = InterpreterconfigurationFactory.eINSTANCE.createConfiguration();		
		configuration.setTripleGraphGrammar(tgg);
		
		// 2. Create source domain model
		DomainModel sourceDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		// find correct typed model from the tgg
		for (TypedModel typedModel : tgg.getAllModelParameters()) {
			for (EPackage ePackage : typedModel.getUsedPackage()) {
				if (ePackage.equals(sourceRootObject.eClass().getEPackage()))
					sourceDomainModel.setTypedModel(typedModel);
			}			
		}
		if (sourceDomainModel.getTypedModel() == null) 
			throw new IllegalArgumentException("The TGG does not contain any typed model that fits the root object of the selected source model");
		// add typed model
		configuration.getDomainModel().add(sourceDomainModel);
	
		// add root object
		sourceDomainModel.getRootObject().add(sourceRootObject);
		
		// what is the source model's file name?
		String sourceModelResourceFilePrefix = sourceModelResource.getURI().trimFileExtension().lastSegment().toString();
		
		
		// 3. Create correspondence domain model
		DomainModel correspondenceDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		correspondenceDomainModel.setTypedModel(correspondenceTypedModel);
		configuration.getDomainModel().add(correspondenceDomainModel);
		//correspondenceDomainModel.getResourceURI().add(sourceModelResource.getURI().trimFileExtension()+".xmi");
		// relative path: 
		correspondenceDomainModel.getResourceURI().add(sourceModelResourceFilePrefix+".corr.xmi");

		// 4. Create target domain model
		DomainModel targetDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		// find correct typed model from the tgg
		for (TypedModel typedModel : tgg.getAllModelParameters()) {
			if (!typedModel.equals(sourceDomainModel.getTypedModel()) && !typedModel.equals(correspondenceDomainModel.getTypedModel())){
				targetDomainModel.setTypedModel(typedModel);
			}
		}
		configuration.getDomainModel().add(targetDomainModel);
		String targetModelSuffix = targetDomainModel.getTypedModel().getUsedPackage().get(0).getName();
		//targetDomainModel.getResourceURI().add(sourceModelResource.getURI().trimFileExtension()+"."+targetModelSuffix);
		// relative path: 
		targetDomainModel.getResourceURI().add(sourceModelResourceFilePrefix+"."+targetModelSuffix);

		ApplicationScenario scenario = InterpreterconfigurationFactory.eINSTANCE.createApplicationScenario();
		scenario.getSourceDomainModel().add(sourceDomainModel);
		scenario.getCorrespondenceModel().add(correspondenceDomainModel);
		scenario.getTargetDomainModel().add(targetDomainModel);
		scenario.setName("fwd");
		scenario.setMode(ApplicationMode.TRANSFORM);
		configuration.getApplicationScenario().add(scenario);
		configuration.setActiveApplicationScenario(scenario);
		
		return configuration;
	}

		
}
