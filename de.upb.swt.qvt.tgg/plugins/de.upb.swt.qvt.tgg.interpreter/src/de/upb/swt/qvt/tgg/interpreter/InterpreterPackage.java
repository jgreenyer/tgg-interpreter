/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterFactory
 * @model kind="package"
 * @generated
 */
public interface InterpreterPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "interpreter";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.upb.swt.qvt.tgg.interpreter";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.upb.swt.qvt.tgg.interpreter";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InterpreterPackage eINSTANCE = de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl <em>Interpreter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getInterpreter()
	 * @generated
	 */
	int INTERPRETER = 0;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__TRANSFORMATION_PROCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Ocl Environment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__OCL_ENVIRONMENT = 2;

	/**
	 * The feature id for the '<em><b>Debug Support</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__DEBUG_SUPPORT = 3;

	/**
	 * The feature id for the '<em><b>Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__OCL = 4;

	/**
	 * The number of structural features of the '<em>Interpreter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl <em>Front Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getFrontManager()
	 * @generated
	 */
	int FRONT_MANAGER = 1;

	/**
	 * The feature id for the '<em><b>Front Transformation Processor</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR = 0;

	/**
	 * The feature id for the '<em><b>Node Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_MANAGER__NODE_BINDING = 1;

	/**
	 * The number of structural features of the '<em>Front Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_MANAGER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl <em>Graph Matcher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getGraphMatcher()
	 * @generated
	 */
	int GRAPH_MATCHER = 7;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl <em>Rule Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getRuleProcessor()
	 * @generated
	 */
	int RULE_PROCESSOR = 2;

	/**
	 * The feature id for the '<em><b>Triple Graph Grammar Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE = 0;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__TRANSFORMATION_PROCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__GRAPH_MATCHER = 2;

	/**
	 * The feature id for the '<em><b>Node Enforcement Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY = 3;

	/**
	 * The feature id for the '<em><b>Edge Enforcement Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY = 4;

	/**
	 * The feature id for the '<em><b>Constraint Enforcement Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY = 5;

	/**
	 * The number of structural features of the '<em>Rule Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PROCESSOR_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl <em>Transformation Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getTransformationProcessor()
	 * @generated
	 */
	int TRANSFORMATION_PROCESSOR = 3;

	/**
	 * The feature id for the '<em><b>Global Edge Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING = 0;

	/**
	 * The feature id for the '<em><b>Global Node Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING = 1;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__RULE_PROCESSOR = 2;

	/**
	 * The feature id for the '<em><b>Interpreter</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__INTERPRETER = 3;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR = 4;

	/**
	 * The feature id for the '<em><b>Cross Reference Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER = 5;

	/**
	 * The feature id for the '<em><b>Helper</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__HELPER = 6;

	/**
	 * The feature id for the '<em><b>Rules Applied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__RULES_APPLIED = 7;

	/**
	 * The feature id for the '<em><b>Rules Revoked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__RULES_REVOKED = 8;

	/**
	 * The feature id for the '<em><b>Rules Still Valid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__RULES_STILL_VALID = 9;

	/**
	 * The feature id for the '<em><b>Deleted Node Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS = 10;

	/**
	 * The feature id for the '<em><b>Deleted Edge Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS = 11;

	/**
	 * The feature id for the '<em><b>Unsuccessful Rule Application Attempts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS = 12;

	/**
	 * The feature id for the '<em><b>Edge Explore Operations Performed Leading To Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS = 13;

	/**
	 * The feature id for the '<em><b>Edge Explore Operations Performed Leading To Un Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS = 14;

	/**
	 * The number of structural features of the '<em>Transformation Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR_FEATURE_COUNT = 15;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy <em>IMatching Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIMatchingPolicy()
	 * @generated
	 */
	int IMATCHING_POLICY = 19;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMATCHING_POLICY__TRANSFORMATION_PROCESSOR = 0;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMATCHING_POLICY__RULE_PROCESSOR = 1;

	/**
	 * The number of structural features of the '<em>IMatching Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMATCHING_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy <em>INode Matching Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getINodeMatchingPolicy()
	 * @generated
	 */
	int INODE_MATCHING_POLICY = 4;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_MATCHING_POLICY__TRANSFORMATION_PROCESSOR = IMATCHING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_MATCHING_POLICY__RULE_PROCESSOR = IMATCHING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_MATCHING_POLICY__GRAPH_MATCHER = IMATCHING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>INode Matching Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_MATCHING_POLICY_FEATURE_COUNT = IMATCHING_POLICY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy <em>IEdge Matching Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIEdgeMatchingPolicy()
	 * @generated
	 */
	int IEDGE_MATCHING_POLICY = 5;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_MATCHING_POLICY__TRANSFORMATION_PROCESSOR = IMATCHING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_MATCHING_POLICY__RULE_PROCESSOR = IMATCHING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_MATCHING_POLICY__GRAPH_MATCHER = IMATCHING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IEdge Matching Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_MATCHING_POLICY_FEATURE_COUNT = IMATCHING_POLICY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl <em>Front Transformation Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getFrontTransformationProcessor()
	 * @generated
	 */
	int FRONT_TRANSFORMATION_PROCESSOR = 6;

	/**
	 * The feature id for the '<em><b>Global Edge Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING = TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING;

	/**
	 * The feature id for the '<em><b>Global Node Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING = TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__RULE_PROCESSOR = TRANSFORMATION_PROCESSOR__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Interpreter</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__INTERPRETER = TRANSFORMATION_PROCESSOR__INTERPRETER;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR = TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR;

	/**
	 * The feature id for the '<em><b>Cross Reference Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER = TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER;

	/**
	 * The feature id for the '<em><b>Helper</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__HELPER = TRANSFORMATION_PROCESSOR__HELPER;

	/**
	 * The feature id for the '<em><b>Rules Applied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__RULES_APPLIED = TRANSFORMATION_PROCESSOR__RULES_APPLIED;

	/**
	 * The feature id for the '<em><b>Rules Revoked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__RULES_REVOKED = TRANSFORMATION_PROCESSOR__RULES_REVOKED;

	/**
	 * The feature id for the '<em><b>Rules Still Valid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__RULES_STILL_VALID = TRANSFORMATION_PROCESSOR__RULES_STILL_VALID;

	/**
	 * The feature id for the '<em><b>Deleted Node Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS = TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS;

	/**
	 * The feature id for the '<em><b>Deleted Edge Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS = TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS;

	/**
	 * The feature id for the '<em><b>Unsuccessful Rule Application Attempts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS = TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS;

	/**
	 * The feature id for the '<em><b>Edge Explore Operations Performed Leading To Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS = TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS;

	/**
	 * The feature id for the '<em><b>Edge Explore Operations Performed Leading To Un Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS = TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS;

	/**
	 * The feature id for the '<em><b>Front Manager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER = TRANSFORMATION_PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Domain Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__SOURCE_DOMAIN_OBJECTS = TRANSFORMATION_PROCESSOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Current TGG Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE = TRANSFORMATION_PROCESSOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Unmatched Source Domain Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR__UNMATCHED_SOURCE_DOMAIN_OBJECTS = TRANSFORMATION_PROCESSOR_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Front Transformation Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRONT_TRANSFORMATION_PROCESSOR_FEATURE_COUNT = TRANSFORMATION_PROCESSOR_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__RULE_BINDING = 0;

	/**
	 * The feature id for the '<em><b>Node Matching Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__NODE_MATCHING_POLICY = 1;

	/**
	 * The feature id for the '<em><b>Edge Matching Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__EDGE_MATCHING_POLICY = 2;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__RULE_PROCESSOR = 3;

	/**
	 * The feature id for the '<em><b>Domains To Match</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__DOMAINS_TO_MATCH = 4;

	/**
	 * The feature id for the '<em><b>Constraint Checking Policy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY = 5;

	/**
	 * The feature id for the '<em><b>Match Reusable Pattern Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY = 6;

	/**
	 * The feature id for the '<em><b>Elements Not Available For Produced Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES = 7;

	/**
	 * The feature id for the '<em><b>Links Not Available For Produced Edges</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES = 8;

	/**
	 * The feature id for the '<em><b>Restrict Produced Graph Matching</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING = 9;

	/**
	 * The feature id for the '<em><b>Start Node Of Matching</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__START_NODE_OF_MATCHING = 10;

	/**
	 * The feature id for the '<em><b>No Elements To Match Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__NO_ELEMENTS_TO_MATCH_LEFT = 11;

	/**
	 * The feature id for the '<em><b>Edge Explore Operations Performed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED = 12;

	/**
	 * The number of structural features of the '<em>Graph Matcher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_MATCHER_FEATURE_COUNT = 13;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl <em>Base Node Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseNodeProcessingPolicy()
	 * @generated
	 */
	int BASE_NODE_PROCESSING_POLICY = 8;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = INODE_MATCHING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR = INODE_MATCHING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER = INODE_MATCHING_POLICY__GRAPH_MATCHER;

	/**
	 * The number of structural features of the '<em>Base Node Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_NODE_PROCESSING_POLICY_FEATURE_COUNT = INODE_MATCHING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy <em>IEdge Enforcement Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIEdgeEnforcementPolicy()
	 * @generated
	 */
	int IEDGE_ENFORCEMENT_POLICY = 13;

	/**
	 * The number of structural features of the '<em>IEdge Enforcement Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_ENFORCEMENT_POLICY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseEdgeProcessingPolicyImpl <em>Base Edge Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseEdgeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseEdgeProcessingPolicy()
	 * @generated
	 */
	int BASE_EDGE_PROCESSING_POLICY = 9;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EDGE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = IEDGE_ENFORCEMENT_POLICY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EDGE_PROCESSING_POLICY__RULE_PROCESSOR = IEDGE_ENFORCEMENT_POLICY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EDGE_PROCESSING_POLICY__GRAPH_MATCHER = IEDGE_ENFORCEMENT_POLICY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Base Edge Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EDGE_PROCESSING_POLICY_FEATURE_COUNT = IEDGE_ENFORCEMENT_POLICY_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy <em>IConstraint Checking Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIConstraintCheckingPolicy()
	 * @generated
	 */
	int ICONSTRAINT_CHECKING_POLICY = 10;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY__GRAPH_MATCHER = 0;

	/**
	 * The feature id for the '<em><b>Unprocessable Constraints</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY__UNPROCESSABLE_CONSTRAINTS = 1;

	/**
	 * The feature id for the '<em><b>Unprocessed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY__UNPROCESSED_CONSTRAINTS = 2;

	/**
	 * The feature id for the '<em><b>Processed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY__PROCESSED_CONSTRAINTS = 3;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY__RULE_BINDING = 4;

	/**
	 * The number of structural features of the '<em>IConstraint Checking Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_CHECKING_POLICY_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy <em>IConstraint Enforcement Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIConstraintEnforcementPolicy()
	 * @generated
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY = 14;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY__GRAPH_MATCHER = ICONSTRAINT_CHECKING_POLICY__GRAPH_MATCHER;

	/**
	 * The feature id for the '<em><b>Unprocessable Constraints</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY__UNPROCESSABLE_CONSTRAINTS = ICONSTRAINT_CHECKING_POLICY__UNPROCESSABLE_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Unprocessed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY__UNPROCESSED_CONSTRAINTS = ICONSTRAINT_CHECKING_POLICY__UNPROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Processed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY__PROCESSED_CONSTRAINTS = ICONSTRAINT_CHECKING_POLICY__PROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY__RULE_BINDING = ICONSTRAINT_CHECKING_POLICY__RULE_BINDING;

	/**
	 * The number of structural features of the '<em>IConstraint Enforcement Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONSTRAINT_ENFORCEMENT_POLICY_FEATURE_COUNT = ICONSTRAINT_CHECKING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl <em>Base Constraint Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseConstraintProcessingPolicy()
	 * @generated
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY = 11;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER = ICONSTRAINT_ENFORCEMENT_POLICY__GRAPH_MATCHER;

	/**
	 * The feature id for the '<em><b>Unprocessable Constraints</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS = ICONSTRAINT_ENFORCEMENT_POLICY__UNPROCESSABLE_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Unprocessed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS = ICONSTRAINT_ENFORCEMENT_POLICY__UNPROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Processed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS = ICONSTRAINT_ENFORCEMENT_POLICY__PROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING = ICONSTRAINT_ENFORCEMENT_POLICY__RULE_BINDING;

	/**
	 * The number of structural features of the '<em>Base Constraint Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONSTRAINT_PROCESSING_POLICY_FEATURE_COUNT = ICONSTRAINT_ENFORCEMENT_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy <em>INode Enforcement Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getINodeEnforcementPolicy()
	 * @generated
	 */
	int INODE_ENFORCEMENT_POLICY = 12;

	/**
	 * The number of structural features of the '<em>INode Enforcement Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_ENFORCEMENT_POLICY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultNodeProcessingPolicyImpl <em>Default Node Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultNodeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultNodeProcessingPolicy()
	 * @generated
	 */
	int DEFAULT_NODE_PROCESSING_POLICY = 15;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_NODE_PROCESSING_POLICY__RULE_PROCESSOR = BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_NODE_PROCESSING_POLICY__GRAPH_MATCHER = BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The number of structural features of the '<em>Default Node Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_NODE_PROCESSING_POLICY_FEATURE_COUNT = BASE_NODE_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultEdgeProcessingPolicyImpl <em>Default Edge Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultEdgeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultEdgeProcessingPolicy()
	 * @generated
	 */
	int DEFAULT_EDGE_PROCESSING_POLICY = 16;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EDGE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = BASE_EDGE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EDGE_PROCESSING_POLICY__RULE_PROCESSOR = BASE_EDGE_PROCESSING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EDGE_PROCESSING_POLICY__GRAPH_MATCHER = BASE_EDGE_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The number of structural features of the '<em>Default Edge Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_EDGE_PROCESSING_POLICY_FEATURE_COUNT = BASE_EDGE_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultOCLConstraintProcessingPolicyImpl <em>Default OCL Constraint Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultOCLConstraintProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultOCLConstraintProcessingPolicy()
	 * @generated
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY = 17;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER = BASE_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The feature id for the '<em><b>Unprocessable Constraints</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS = BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Unprocessed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS = BASE_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Processed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS = BASE_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING = BASE_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING;

	/**
	 * The number of structural features of the '<em>Default OCL Constraint Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY_FEATURE_COUNT = BASE_CONSTRAINT_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.NodeToConstraintMapEntryImpl <em>Node To Constraint Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.NodeToConstraintMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getNodeToConstraintMapEntry()
	 * @generated
	 */
	int NODE_TO_CONSTRAINT_MAP_ENTRY = 18;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_CONSTRAINT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_CONSTRAINT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Node To Constraint Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_CONSTRAINT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl <em>Transformation Processor Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getTransformationProcessorHelper()
	 * @generated
	 */
	int TRANSFORMATION_PROCESSOR_HELPER = 20;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR = 0;

	/**
	 * The feature id for the '<em><b>Sorted Concrete Rule List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST = 1;

	/**
	 * The number of structural features of the '<em>Transformation Processor Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_PROCESSOR_HELPER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.IDebugEventListener <em>IDebug Event Listener</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.IDebugEventListener
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIDebugEventListener()
	 * @generated
	 */
	int IDEBUG_EVENT_LISTENER = 22;

	/**
	 * The number of structural features of the '<em>IDebug Event Listener</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEBUG_EVENT_LISTENER_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl <em>Debug Support</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDebugSupport()
	 * @generated
	 */
	int DEBUG_SUPPORT = 21;

	/**
	 * The feature id for the '<em><b>Terminate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEBUG_SUPPORT__TERMINATE = IDEBUG_EVENT_LISTENER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Debug</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEBUG_SUPPORT__DEBUG = IDEBUG_EVENT_LISTENER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Running</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEBUG_SUPPORT__RUNNING = IDEBUG_EVENT_LISTENER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Debug Event Listener</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEBUG_SUPPORT__DEBUG_EVENT_LISTENER = IDEBUG_EVENT_LISTENER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Debug Support</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEBUG_SUPPORT_FEATURE_COUNT = IDEBUG_EVENT_LISTENER_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '<em>Status</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.core.runtime.IStatus
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getStatus()
	 * @generated
	 */
	int STATUS = 23;

	/**
	 * The meta object id for the '<em>EValue Clash Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.util.ValueClashException
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getEValueClashException()
	 * @generated
	 */
	int EVALUE_CLASH_EXCEPTION = 24;

	/**
	 * The meta object id for the '<em>IProgress Monitor</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.core.runtime.IProgressMonitor
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIProgressMonitor()
	 * @generated
	 */
	int IPROGRESS_MONITOR = 25;


	/**
	 * The meta object id for the '<em>Iterator</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Iterator
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIterator()
	 * @generated
	 */
	int ITERATOR = 26;


	/**
	 * The meta object id for the '<em>Condition</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.query.conditions.Condition
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 27;


	/**
	 * The meta object id for the '<em>Collection</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Collection
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getCollection()
	 * @generated
	 */
	int COLLECTION = 28;

	/**
	 * The meta object id for the '<em>Setting</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EStructuralFeature.Setting
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getSetting()
	 * @generated
	 */
	int SETTING = 29;


	/**
	 * The meta object id for the '<em>Constraint Unprocessable Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getConstraintUnprocessableException()
	 * @generated
	 */
	int CONSTRAINT_UNPROCESSABLE_EXCEPTION = 30;


	/**
	 * The meta object id for the '<em>ECross Reference Adapter</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.util.ECrossReferenceAdapter
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getECrossReferenceAdapter()
	 * @generated
	 */
	int ECROSS_REFERENCE_ADAPTER = 31;

	/**
	 * The meta object id for the '<em>OCL Environment</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ocl.ecore.EcoreEnvironment
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getOCLEnvironment()
	 * @generated
	 */
	int OCL_ENVIRONMENT = 32;

	/**
	 * The meta object id for the '<em>OCL</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ocl.ecore.OCL
	 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getOCL()
	 * @generated
	 */
	int OCL = 33;

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter <em>Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interpreter</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter
	 * @generated
	 */
	EClass getInterpreter();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Configuration</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getConfiguration()
	 * @see #getInterpreter()
	 * @generated
	 */
	EReference getInterpreter_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor()
	 * @see #getInterpreter()
	 * @generated
	 */
	EReference getInterpreter_TransformationProcessor();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOclEnvironment <em>Ocl Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ocl Environment</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getOclEnvironment()
	 * @see #getInterpreter()
	 * @generated
	 */
	EAttribute getInterpreter_OclEnvironment();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getDebugSupport <em>Debug Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Debug Support</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getDebugSupport()
	 * @see #getInterpreter()
	 * @generated
	 */
	EReference getInterpreter_DebugSupport();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getOcl <em>Ocl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ocl</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getOcl()
	 * @see #getInterpreter()
	 * @generated
	 */
	EAttribute getInterpreter_Ocl();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager <em>Front Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Front Manager</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontManager
	 * @generated
	 */
	EClass getFrontManager();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor <em>Front Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Front Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontManager#getFrontTransformationProcessor()
	 * @see #getFrontManager()
	 * @generated
	 */
	EReference getFrontManager_FrontTransformationProcessor();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreter.FrontManager#getNodeBinding <em>Node Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Node Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontManager#getNodeBinding()
	 * @see #getFrontManager()
	 * @generated
	 */
	EReference getFrontManager_NodeBinding();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor
	 * @generated
	 */
	EClass getRuleProcessor();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Triple Graph Grammar Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTripleGraphGrammarRule()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_TripleGraphGrammarRule();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_TransformationProcessor();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Graph Matcher</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_GraphMatcher();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getNodeEnforcementPolicy <em>Node Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Node Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getNodeEnforcementPolicy()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_NodeEnforcementPolicy();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getEdgeEnforcementPolicy <em>Edge Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Edge Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getEdgeEnforcementPolicy()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_EdgeEnforcementPolicy();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getConstraintEnforcementPolicy <em>Constraint Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraint Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getConstraintEnforcementPolicy()
	 * @see #getRuleProcessor()
	 * @generated
	 */
	EReference getRuleProcessor_ConstraintEnforcementPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor
	 * @generated
	 */
	EClass getTransformationProcessor();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalEdgeBinding <em>Global Edge Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Global Edge Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalEdgeBinding()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EReference getTransformationProcessor_GlobalEdgeBinding();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalNodeBinding <em>Global Node Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Global Node Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalNodeBinding()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EReference getTransformationProcessor_GlobalNodeBinding();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rule Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EReference getTransformationProcessor_RuleProcessor();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter <em>Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Interpreter</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EReference getTransformationProcessor_Interpreter();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getProgressMonitor <em>Progress Monitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Progress Monitor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getProgressMonitor()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_ProgressMonitor();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getCrossReferenceAdapter <em>Cross Reference Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cross Reference Adapter</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getCrossReferenceAdapter()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_CrossReferenceAdapter();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper <em>Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Helper</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EReference getTransformationProcessor_Helper();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesApplied <em>Rules Applied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rules Applied</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesApplied()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_RulesApplied();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesRevoked <em>Rules Revoked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rules Revoked</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesRevoked()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_RulesRevoked();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesStillValid <em>Rules Still Valid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rules Still Valid</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesStillValid()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_RulesStillValid();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedNodeBindings <em>Deleted Node Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deleted Node Bindings</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedNodeBindings()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_DeletedNodeBindings();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedEdgeBindings <em>Deleted Edge Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deleted Edge Bindings</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedEdgeBindings()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_DeletedEdgeBindings();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getUnsuccessfulRuleApplicationAttempts <em>Unsuccessful Rule Application Attempts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unsuccessful Rule Application Attempts</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getUnsuccessfulRuleApplicationAttempts()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_UnsuccessfulRuleApplicationAttempts();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications()
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	EAttribute getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy <em>INode Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>INode Matching Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy
	 * @generated
	 */
	EClass getINodeMatchingPolicy();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph Matcher</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher()
	 * @see #getINodeMatchingPolicy()
	 * @generated
	 */
	EReference getINodeMatchingPolicy_GraphMatcher();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy <em>IEdge Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEdge Matching Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy
	 * @generated
	 */
	EClass getIEdgeMatchingPolicy();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph Matcher</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy#getGraphMatcher()
	 * @see #getIEdgeMatchingPolicy()
	 * @generated
	 */
	EReference getIEdgeMatchingPolicy_GraphMatcher();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor <em>Front Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Front Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor
	 * @generated
	 */
	EClass getFrontTransformationProcessor();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager <em>Front Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Front Manager</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getFrontManager()
	 * @see #getFrontTransformationProcessor()
	 * @generated
	 */
	EReference getFrontTransformationProcessor_FrontManager();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getSourceDomainObjects <em>Source Domain Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source Domain Objects</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getSourceDomainObjects()
	 * @see #getFrontTransformationProcessor()
	 * @generated
	 */
	EReference getFrontTransformationProcessor_SourceDomainObjects();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getCurrentTGGRule <em>Current TGG Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current TGG Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getCurrentTGGRule()
	 * @see #getFrontTransformationProcessor()
	 * @generated
	 */
	EReference getFrontTransformationProcessor_CurrentTGGRule();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getUnmatchedSourceDomainObjects <em>Unmatched Source Domain Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Unmatched Source Domain Objects</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor#getUnmatchedSourceDomainObjects()
	 * @see #getFrontTransformationProcessor()
	 * @generated
	 */
	EReference getFrontTransformationProcessor_UnmatchedSourceDomainObjects();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph Matcher</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher
	 * @generated
	 */
	EClass getGraphMatcher();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleBinding <em>Rule Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleBinding()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_RuleBinding();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getNodeMatchingPolicy <em>Node Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Node Matching Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getNodeMatchingPolicy()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_NodeMatchingPolicy();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeMatchingPolicy <em>Edge Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Edge Matching Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeMatchingPolicy()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_EdgeMatchingPolicy();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Rule Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_RuleProcessor();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getDomainsToMatch <em>Domains To Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Domains To Match</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getDomainsToMatch()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_DomainsToMatch();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getConstraintCheckingPolicy <em>Constraint Checking Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraint Checking Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getConstraintCheckingPolicy()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_ConstraintCheckingPolicy();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isMatchReusablePatternOnly <em>Match Reusable Pattern Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match Reusable Pattern Only</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isMatchReusablePatternOnly()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EAttribute getGraphMatcher_MatchReusablePatternOnly();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getElementsNotAvailableForProducedNodes <em>Elements Not Available For Produced Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements Not Available For Produced Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getElementsNotAvailableForProducedNodes()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_ElementsNotAvailableForProducedNodes();

	/**
	 * Returns the meta object for the attribute list '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getLinksNotAvailableForProducedEdges <em>Links Not Available For Produced Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Links Not Available For Produced Edges</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getLinksNotAvailableForProducedEdges()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EAttribute getGraphMatcher_LinksNotAvailableForProducedEdges();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isRestrictProducedGraphMatching <em>Restrict Produced Graph Matching</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Restrict Produced Graph Matching</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isRestrictProducedGraphMatching()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EAttribute getGraphMatcher_RestrictProducedGraphMatching();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getStartNodeOfMatching <em>Start Node Of Matching</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start Node Of Matching</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getStartNodeOfMatching()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EReference getGraphMatcher_StartNodeOfMatching();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isNoElementsToMatchLeft <em>No Elements To Match Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Elements To Match Left</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#isNoElementsToMatchLeft()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EAttribute getGraphMatcher_NoElementsToMatchLeft();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeExploreOperationsPerformed <em>Edge Explore Operations Performed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Edge Explore Operations Performed</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getEdgeExploreOperationsPerformed()
	 * @see #getGraphMatcher()
	 * @generated
	 */
	EAttribute getGraphMatcher_EdgeExploreOperationsPerformed();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy <em>Base Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Node Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy
	 * @generated
	 */
	EClass getBaseNodeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy <em>Base Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Edge Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy
	 * @generated
	 */
	EClass getBaseEdgeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy <em>IConstraint Checking Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConstraint Checking Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy
	 * @generated
	 */
	EClass getIConstraintCheckingPolicy();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher <em>Graph Matcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph Matcher</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getGraphMatcher()
	 * @see #getIConstraintCheckingPolicy()
	 * @generated
	 */
	EReference getIConstraintCheckingPolicy_GraphMatcher();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessableConstraints <em>Unprocessable Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unprocessable Constraints</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessableConstraints()
	 * @see #getIConstraintCheckingPolicy()
	 * @generated
	 */
	EAttribute getIConstraintCheckingPolicy_UnprocessableConstraints();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessedConstraints <em>Unprocessed Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Unprocessed Constraints</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getUnprocessedConstraints()
	 * @see #getIConstraintCheckingPolicy()
	 * @generated
	 */
	EReference getIConstraintCheckingPolicy_UnprocessedConstraints();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getProcessedConstraints <em>Processed Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Processed Constraints</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getProcessedConstraints()
	 * @see #getIConstraintCheckingPolicy()
	 * @generated
	 */
	EReference getIConstraintCheckingPolicy_ProcessedConstraints();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getRuleBinding <em>Rule Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy#getRuleBinding()
	 * @see #getIConstraintCheckingPolicy()
	 * @generated
	 */
	EReference getIConstraintCheckingPolicy_RuleBinding();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy <em>Base Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Constraint Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy
	 * @generated
	 */
	EClass getBaseConstraintProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy <em>INode Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>INode Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy
	 * @generated
	 */
	EClass getINodeEnforcementPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy <em>IEdge Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEdge Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy
	 * @generated
	 */
	EClass getIEdgeEnforcementPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy <em>IConstraint Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConstraint Enforcement Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy
	 * @generated
	 */
	EClass getIConstraintEnforcementPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy <em>Default Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Node Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultNodeProcessingPolicy
	 * @generated
	 */
	EClass getDefaultNodeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy <em>Default Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Edge Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultEdgeProcessingPolicy
	 * @generated
	 */
	EClass getDefaultEdgeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy <em>Default OCL Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default OCL Constraint Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy
	 * @generated
	 */
	EClass getDefaultOCLConstraintProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Node To Constraint Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node To Constraint Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="de.upb.swt.qvt.tgg.Constraint" valueMany="true"
	 *        keyType="de.upb.swt.qvt.tgg.Node" keyRequired="true"
	 * @generated
	 */
	EClass getNodeToConstraintMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNodeToConstraintMapEntry()
	 * @generated
	 */
	EReference getNodeToConstraintMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNodeToConstraintMapEntry()
	 * @generated
	 */
	EReference getNodeToConstraintMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy <em>IMatching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IMatching Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy
	 * @generated
	 */
	EClass getIMatchingPolicy();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getTransformationProcessor()
	 * @see #getIMatchingPolicy()
	 * @generated
	 */
	EReference getIMatchingPolicy_TransformationProcessor();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getRuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy#getRuleProcessor()
	 * @see #getIMatchingPolicy()
	 * @generated
	 */
	EReference getIMatchingPolicy_RuleProcessor();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper <em>Transformation Processor Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Processor Helper</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper
	 * @generated
	 */
	EClass getTransformationProcessorHelper();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Transformation Processor</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor()
	 * @see #getTransformationProcessorHelper()
	 * @generated
	 */
	EReference getTransformationProcessorHelper_TransformationProcessor();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getSortedConcreteRuleList <em>Sorted Concrete Rule List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sorted Concrete Rule List</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getSortedConcreteRuleList()
	 * @see #getTransformationProcessorHelper()
	 * @generated
	 */
	EReference getTransformationProcessorHelper_SortedConcreteRuleList();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport <em>Debug Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Debug Support</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport
	 * @generated
	 */
	EClass getDebugSupport();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isTerminate <em>Terminate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Terminate</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport#isTerminate()
	 * @see #getDebugSupport()
	 * @generated
	 */
	EAttribute getDebugSupport_Terminate();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isDebug <em>Debug</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Debug</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport#isDebug()
	 * @see #getDebugSupport()
	 * @generated
	 */
	EAttribute getDebugSupport_Debug();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#isRunning <em>Running</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Running</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport#isRunning()
	 * @see #getDebugSupport()
	 * @generated
	 */
	EAttribute getDebugSupport_Running();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreter.DebugSupport#getDebugEventListener <em>Debug Event Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Debug Event Listener</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.DebugSupport#getDebugEventListener()
	 * @see #getDebugSupport()
	 * @generated
	 */
	EReference getDebugSupport_DebugEventListener();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.IDebugEventListener <em>IDebug Event Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IDebug Event Listener</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.IDebugEventListener
	 * @generated
	 */
	EClass getIDebugEventListener();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.core.runtime.IStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Status</em>'.
	 * @see org.eclipse.core.runtime.IStatus
	 * @model instanceClass="org.eclipse.core.runtime.IStatus" serializeable="false"
	 * @generated
	 */
	EDataType getStatus();

	/**
	 * Returns the meta object for data type '{@link de.upb.swt.qvt.tgg.interpreter.util.ValueClashException <em>EValue Clash Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EValue Clash Exception</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.util.ValueClashException
	 * @model instanceClass="de.upb.swt.qvt.tgg.interpreter.util.ValueClashException" serializeable="false"
	 * @generated
	 */
	EDataType getEValueClashException();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.core.runtime.IProgressMonitor <em>IProgress Monitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IProgress Monitor</em>'.
	 * @see org.eclipse.core.runtime.IProgressMonitor
	 * @model instanceClass="org.eclipse.core.runtime.IProgressMonitor" serializeable="false"
	 * @generated
	 */
	EDataType getIProgressMonitor();

	/**
	 * Returns the meta object for data type '{@link java.util.Iterator <em>Iterator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Iterator</em>'.
	 * @see java.util.Iterator
	 * @model instanceClass="java.util.Iterator" serializeable="false" typeParameters="E"
	 * @generated
	 */
	EDataType getIterator();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.query.conditions.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Condition</em>'.
	 * @see org.eclipse.emf.query.conditions.Condition
	 * @model instanceClass="org.eclipse.emf.query.conditions.Condition" serializeable="false"
	 * @generated
	 */
	EDataType getCondition();

	/**
	 * Returns the meta object for data type '{@link java.util.Collection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Collection</em>'.
	 * @see java.util.Collection
	 * @model instanceClass="java.util.Collection" typeParameters="E"
	 * @generated
	 */
	EDataType getCollection();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.EStructuralFeature.Setting <em>Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Setting</em>'.
	 * @see org.eclipse.emf.ecore.EStructuralFeature.Setting
	 * @model instanceClass="org.eclipse.emf.ecore.EStructuralFeature$Setting"
	 * @generated
	 */
	EDataType getSetting();

	/**
	 * Returns the meta object for data type '{@link de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException <em>Constraint Unprocessable Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Constraint Unprocessable Exception</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException
	 * @model instanceClass="de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException"
	 * @generated
	 */
	EDataType getConstraintUnprocessableException();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.util.ECrossReferenceAdapter <em>ECross Reference Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ECross Reference Adapter</em>'.
	 * @see org.eclipse.emf.ecore.util.ECrossReferenceAdapter
	 * @model instanceClass="org.eclipse.emf.ecore.util.ECrossReferenceAdapter" serializeable="false"
	 * @generated
	 */
	EDataType getECrossReferenceAdapter();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ocl.ecore.EcoreEnvironment <em>OCL Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>OCL Environment</em>'.
	 * @see org.eclipse.ocl.ecore.EcoreEnvironment
	 * @model instanceClass="org.eclipse.ocl.ecore.EcoreEnvironment" serializeable="false"
	 * @generated
	 */
	EDataType getOCLEnvironment();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ocl.ecore.OCL <em>OCL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>OCL</em>'.
	 * @see org.eclipse.ocl.ecore.OCL
	 * @model instanceClass="org.eclipse.ocl.ecore.OCL"
	 * @generated
	 */
	EDataType getOCL();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InterpreterFactory getInterpreterFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl <em>Interpreter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getInterpreter()
		 * @generated
		 */
		EClass INTERPRETER = eINSTANCE.getInterpreter();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERPRETER__CONFIGURATION = eINSTANCE.getInterpreter_Configuration();

		/**
		 * The meta object literal for the '<em><b>Transformation Processor</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERPRETER__TRANSFORMATION_PROCESSOR = eINSTANCE.getInterpreter_TransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Ocl Environment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERPRETER__OCL_ENVIRONMENT = eINSTANCE.getInterpreter_OclEnvironment();

		/**
		 * The meta object literal for the '<em><b>Debug Support</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERPRETER__DEBUG_SUPPORT = eINSTANCE.getInterpreter_DebugSupport();

		/**
		 * The meta object literal for the '<em><b>Ocl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERPRETER__OCL = eINSTANCE.getInterpreter_Ocl();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl <em>Front Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getFrontManager()
		 * @generated
		 */
		EClass FRONT_MANAGER = eINSTANCE.getFrontManager();

		/**
		 * The meta object literal for the '<em><b>Front Transformation Processor</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR = eINSTANCE.getFrontManager_FrontTransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Node Binding</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_MANAGER__NODE_BINDING = eINSTANCE.getFrontManager_NodeBinding();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl <em>Rule Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.RuleProcessorImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getRuleProcessor()
		 * @generated
		 */
		EClass RULE_PROCESSOR = eINSTANCE.getRuleProcessor();

		/**
		 * The meta object literal for the '<em><b>Triple Graph Grammar Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__TRIPLE_GRAPH_GRAMMAR_RULE = eINSTANCE.getRuleProcessor_TripleGraphGrammarRule();

		/**
		 * The meta object literal for the '<em><b>Transformation Processor</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__TRANSFORMATION_PROCESSOR = eINSTANCE.getRuleProcessor_TransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Graph Matcher</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__GRAPH_MATCHER = eINSTANCE.getRuleProcessor_GraphMatcher();

		/**
		 * The meta object literal for the '<em><b>Node Enforcement Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__NODE_ENFORCEMENT_POLICY = eINSTANCE.getRuleProcessor_NodeEnforcementPolicy();

		/**
		 * The meta object literal for the '<em><b>Edge Enforcement Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__EDGE_ENFORCEMENT_POLICY = eINSTANCE.getRuleProcessor_EdgeEnforcementPolicy();

		/**
		 * The meta object literal for the '<em><b>Constraint Enforcement Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_PROCESSOR__CONSTRAINT_ENFORCEMENT_POLICY = eINSTANCE.getRuleProcessor_ConstraintEnforcementPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl <em>Transformation Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getTransformationProcessor()
		 * @generated
		 */
		EClass TRANSFORMATION_PROCESSOR = eINSTANCE.getTransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Global Edge Binding</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING = eINSTANCE.getTransformationProcessor_GlobalEdgeBinding();

		/**
		 * The meta object literal for the '<em><b>Global Node Binding</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING = eINSTANCE.getTransformationProcessor_GlobalNodeBinding();

		/**
		 * The meta object literal for the '<em><b>Rule Processor</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR__RULE_PROCESSOR = eINSTANCE.getTransformationProcessor_RuleProcessor();

		/**
		 * The meta object literal for the '<em><b>Interpreter</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR__INTERPRETER = eINSTANCE.getTransformationProcessor_Interpreter();

		/**
		 * The meta object literal for the '<em><b>Progress Monitor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR = eINSTANCE.getTransformationProcessor_ProgressMonitor();

		/**
		 * The meta object literal for the '<em><b>Cross Reference Adapter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER = eINSTANCE.getTransformationProcessor_CrossReferenceAdapter();

		/**
		 * The meta object literal for the '<em><b>Helper</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR__HELPER = eINSTANCE.getTransformationProcessor_Helper();

		/**
		 * The meta object literal for the '<em><b>Rules Applied</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__RULES_APPLIED = eINSTANCE.getTransformationProcessor_RulesApplied();

		/**
		 * The meta object literal for the '<em><b>Rules Revoked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__RULES_REVOKED = eINSTANCE.getTransformationProcessor_RulesRevoked();

		/**
		 * The meta object literal for the '<em><b>Rules Still Valid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__RULES_STILL_VALID = eINSTANCE.getTransformationProcessor_RulesStillValid();

		/**
		 * The meta object literal for the '<em><b>Deleted Node Bindings</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS = eINSTANCE.getTransformationProcessor_DeletedNodeBindings();

		/**
		 * The meta object literal for the '<em><b>Deleted Edge Bindings</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS = eINSTANCE.getTransformationProcessor_DeletedEdgeBindings();

		/**
		 * The meta object literal for the '<em><b>Unsuccessful Rule Application Attempts</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS = eINSTANCE.getTransformationProcessor_UnsuccessfulRuleApplicationAttempts();

		/**
		 * The meta object literal for the '<em><b>Edge Explore Operations Performed Leading To Successful Rule Applications</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS = eINSTANCE.getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications();

		/**
		 * The meta object literal for the '<em><b>Edge Explore Operations Performed Leading To Un Successful Rule Applications</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS = eINSTANCE.getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy <em>INode Matching Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getINodeMatchingPolicy()
		 * @generated
		 */
		EClass INODE_MATCHING_POLICY = eINSTANCE.getINodeMatchingPolicy();

		/**
		 * The meta object literal for the '<em><b>Graph Matcher</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INODE_MATCHING_POLICY__GRAPH_MATCHER = eINSTANCE.getINodeMatchingPolicy_GraphMatcher();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy <em>IEdge Matching Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIEdgeMatchingPolicy()
		 * @generated
		 */
		EClass IEDGE_MATCHING_POLICY = eINSTANCE.getIEdgeMatchingPolicy();

		/**
		 * The meta object literal for the '<em><b>Graph Matcher</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEDGE_MATCHING_POLICY__GRAPH_MATCHER = eINSTANCE.getIEdgeMatchingPolicy_GraphMatcher();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl <em>Front Transformation Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getFrontTransformationProcessor()
		 * @generated
		 */
		EClass FRONT_TRANSFORMATION_PROCESSOR = eINSTANCE.getFrontTransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Front Manager</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER = eINSTANCE.getFrontTransformationProcessor_FrontManager();

		/**
		 * The meta object literal for the '<em><b>Source Domain Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_TRANSFORMATION_PROCESSOR__SOURCE_DOMAIN_OBJECTS = eINSTANCE.getFrontTransformationProcessor_SourceDomainObjects();

		/**
		 * The meta object literal for the '<em><b>Current TGG Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE = eINSTANCE.getFrontTransformationProcessor_CurrentTGGRule();

		/**
		 * The meta object literal for the '<em><b>Unmatched Source Domain Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRONT_TRANSFORMATION_PROCESSOR__UNMATCHED_SOURCE_DOMAIN_OBJECTS = eINSTANCE.getFrontTransformationProcessor_UnmatchedSourceDomainObjects();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl <em>Graph Matcher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.GraphMatcherImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getGraphMatcher()
		 * @generated
		 */
		EClass GRAPH_MATCHER = eINSTANCE.getGraphMatcher();

		/**
		 * The meta object literal for the '<em><b>Rule Binding</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__RULE_BINDING = eINSTANCE.getGraphMatcher_RuleBinding();

		/**
		 * The meta object literal for the '<em><b>Node Matching Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__NODE_MATCHING_POLICY = eINSTANCE.getGraphMatcher_NodeMatchingPolicy();

		/**
		 * The meta object literal for the '<em><b>Edge Matching Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__EDGE_MATCHING_POLICY = eINSTANCE.getGraphMatcher_EdgeMatchingPolicy();

		/**
		 * The meta object literal for the '<em><b>Rule Processor</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__RULE_PROCESSOR = eINSTANCE.getGraphMatcher_RuleProcessor();

		/**
		 * The meta object literal for the '<em><b>Domains To Match</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__DOMAINS_TO_MATCH = eINSTANCE.getGraphMatcher_DomainsToMatch();

		/**
		 * The meta object literal for the '<em><b>Constraint Checking Policy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__CONSTRAINT_CHECKING_POLICY = eINSTANCE.getGraphMatcher_ConstraintCheckingPolicy();

		/**
		 * The meta object literal for the '<em><b>Match Reusable Pattern Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_MATCHER__MATCH_REUSABLE_PATTERN_ONLY = eINSTANCE.getGraphMatcher_MatchReusablePatternOnly();

		/**
		 * The meta object literal for the '<em><b>Elements Not Available For Produced Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__ELEMENTS_NOT_AVAILABLE_FOR_PRODUCED_NODES = eINSTANCE.getGraphMatcher_ElementsNotAvailableForProducedNodes();

		/**
		 * The meta object literal for the '<em><b>Links Not Available For Produced Edges</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_MATCHER__LINKS_NOT_AVAILABLE_FOR_PRODUCED_EDGES = eINSTANCE.getGraphMatcher_LinksNotAvailableForProducedEdges();

		/**
		 * The meta object literal for the '<em><b>Restrict Produced Graph Matching</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_MATCHER__RESTRICT_PRODUCED_GRAPH_MATCHING = eINSTANCE.getGraphMatcher_RestrictProducedGraphMatching();

		/**
		 * The meta object literal for the '<em><b>Start Node Of Matching</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH_MATCHER__START_NODE_OF_MATCHING = eINSTANCE.getGraphMatcher_StartNodeOfMatching();

		/**
		 * The meta object literal for the '<em><b>No Elements To Match Left</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_MATCHER__NO_ELEMENTS_TO_MATCH_LEFT = eINSTANCE.getGraphMatcher_NoElementsToMatchLeft();

		/**
		 * The meta object literal for the '<em><b>Edge Explore Operations Performed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPH_MATCHER__EDGE_EXPLORE_OPERATIONS_PERFORMED = eINSTANCE.getGraphMatcher_EdgeExploreOperationsPerformed();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl <em>Base Node Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseNodeProcessingPolicy()
		 * @generated
		 */
		EClass BASE_NODE_PROCESSING_POLICY = eINSTANCE.getBaseNodeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseEdgeProcessingPolicyImpl <em>Base Edge Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseEdgeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseEdgeProcessingPolicy()
		 * @generated
		 */
		EClass BASE_EDGE_PROCESSING_POLICY = eINSTANCE.getBaseEdgeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy <em>IConstraint Checking Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIConstraintCheckingPolicy()
		 * @generated
		 */
		EClass ICONSTRAINT_CHECKING_POLICY = eINSTANCE.getIConstraintCheckingPolicy();

		/**
		 * The meta object literal for the '<em><b>Graph Matcher</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONSTRAINT_CHECKING_POLICY__GRAPH_MATCHER = eINSTANCE.getIConstraintCheckingPolicy_GraphMatcher();

		/**
		 * The meta object literal for the '<em><b>Unprocessable Constraints</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ICONSTRAINT_CHECKING_POLICY__UNPROCESSABLE_CONSTRAINTS = eINSTANCE.getIConstraintCheckingPolicy_UnprocessableConstraints();

		/**
		 * The meta object literal for the '<em><b>Unprocessed Constraints</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONSTRAINT_CHECKING_POLICY__UNPROCESSED_CONSTRAINTS = eINSTANCE.getIConstraintCheckingPolicy_UnprocessedConstraints();

		/**
		 * The meta object literal for the '<em><b>Processed Constraints</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONSTRAINT_CHECKING_POLICY__PROCESSED_CONSTRAINTS = eINSTANCE.getIConstraintCheckingPolicy_ProcessedConstraints();

		/**
		 * The meta object literal for the '<em><b>Rule Binding</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONSTRAINT_CHECKING_POLICY__RULE_BINDING = eINSTANCE.getIConstraintCheckingPolicy_RuleBinding();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl <em>Base Constraint Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.BaseConstraintProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getBaseConstraintProcessingPolicy()
		 * @generated
		 */
		EClass BASE_CONSTRAINT_PROCESSING_POLICY = eINSTANCE.getBaseConstraintProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy <em>INode Enforcement Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getINodeEnforcementPolicy()
		 * @generated
		 */
		EClass INODE_ENFORCEMENT_POLICY = eINSTANCE.getINodeEnforcementPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy <em>IEdge Enforcement Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIEdgeEnforcementPolicy()
		 * @generated
		 */
		EClass IEDGE_ENFORCEMENT_POLICY = eINSTANCE.getIEdgeEnforcementPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy <em>IConstraint Enforcement Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIConstraintEnforcementPolicy()
		 * @generated
		 */
		EClass ICONSTRAINT_ENFORCEMENT_POLICY = eINSTANCE.getIConstraintEnforcementPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultNodeProcessingPolicyImpl <em>Default Node Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultNodeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultNodeProcessingPolicy()
		 * @generated
		 */
		EClass DEFAULT_NODE_PROCESSING_POLICY = eINSTANCE.getDefaultNodeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultEdgeProcessingPolicyImpl <em>Default Edge Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultEdgeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultEdgeProcessingPolicy()
		 * @generated
		 */
		EClass DEFAULT_EDGE_PROCESSING_POLICY = eINSTANCE.getDefaultEdgeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DefaultOCLConstraintProcessingPolicyImpl <em>Default OCL Constraint Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.DefaultOCLConstraintProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDefaultOCLConstraintProcessingPolicy()
		 * @generated
		 */
		EClass DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY = eINSTANCE.getDefaultOCLConstraintProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.NodeToConstraintMapEntryImpl <em>Node To Constraint Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.NodeToConstraintMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getNodeToConstraintMapEntry()
		 * @generated
		 */
		EClass NODE_TO_CONSTRAINT_MAP_ENTRY = eINSTANCE.getNodeToConstraintMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TO_CONSTRAINT_MAP_ENTRY__VALUE = eINSTANCE.getNodeToConstraintMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TO_CONSTRAINT_MAP_ENTRY__KEY = eINSTANCE.getNodeToConstraintMapEntry_Key();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy <em>IMatching Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIMatchingPolicy()
		 * @generated
		 */
		EClass IMATCHING_POLICY = eINSTANCE.getIMatchingPolicy();

		/**
		 * The meta object literal for the '<em><b>Transformation Processor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMATCHING_POLICY__TRANSFORMATION_PROCESSOR = eINSTANCE.getIMatchingPolicy_TransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Rule Processor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMATCHING_POLICY__RULE_PROCESSOR = eINSTANCE.getIMatchingPolicy_RuleProcessor();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl <em>Transformation Processor Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorHelperImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getTransformationProcessorHelper()
		 * @generated
		 */
		EClass TRANSFORMATION_PROCESSOR_HELPER = eINSTANCE.getTransformationProcessorHelper();

		/**
		 * The meta object literal for the '<em><b>Transformation Processor</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR = eINSTANCE.getTransformationProcessorHelper_TransformationProcessor();

		/**
		 * The meta object literal for the '<em><b>Sorted Concrete Rule List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_PROCESSOR_HELPER__SORTED_CONCRETE_RULE_LIST = eINSTANCE.getTransformationProcessorHelper_SortedConcreteRuleList();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl <em>Debug Support</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.DebugSupportImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getDebugSupport()
		 * @generated
		 */
		EClass DEBUG_SUPPORT = eINSTANCE.getDebugSupport();

		/**
		 * The meta object literal for the '<em><b>Terminate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEBUG_SUPPORT__TERMINATE = eINSTANCE.getDebugSupport_Terminate();

		/**
		 * The meta object literal for the '<em><b>Debug</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEBUG_SUPPORT__DEBUG = eINSTANCE.getDebugSupport_Debug();

		/**
		 * The meta object literal for the '<em><b>Running</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEBUG_SUPPORT__RUNNING = eINSTANCE.getDebugSupport_Running();

		/**
		 * The meta object literal for the '<em><b>Debug Event Listener</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEBUG_SUPPORT__DEBUG_EVENT_LISTENER = eINSTANCE.getDebugSupport_DebugEventListener();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.IDebugEventListener <em>IDebug Event Listener</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.IDebugEventListener
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIDebugEventListener()
		 * @generated
		 */
		EClass IDEBUG_EVENT_LISTENER = eINSTANCE.getIDebugEventListener();

		/**
		 * The meta object literal for the '<em>Status</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.core.runtime.IStatus
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getStatus()
		 * @generated
		 */
		EDataType STATUS = eINSTANCE.getStatus();

		/**
		 * The meta object literal for the '<em>EValue Clash Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.util.ValueClashException
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getEValueClashException()
		 * @generated
		 */
		EDataType EVALUE_CLASH_EXCEPTION = eINSTANCE.getEValueClashException();

		/**
		 * The meta object literal for the '<em>IProgress Monitor</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.core.runtime.IProgressMonitor
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIProgressMonitor()
		 * @generated
		 */
		EDataType IPROGRESS_MONITOR = eINSTANCE.getIProgressMonitor();

		/**
		 * The meta object literal for the '<em>Iterator</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Iterator
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getIterator()
		 * @generated
		 */
		EDataType ITERATOR = eINSTANCE.getIterator();

		/**
		 * The meta object literal for the '<em>Condition</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.query.conditions.Condition
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getCondition()
		 * @generated
		 */
		EDataType CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em>Collection</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Collection
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getCollection()
		 * @generated
		 */
		EDataType COLLECTION = eINSTANCE.getCollection();

		/**
		 * The meta object literal for the '<em>Setting</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.EStructuralFeature.Setting
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getSetting()
		 * @generated
		 */
		EDataType SETTING = eINSTANCE.getSetting();

		/**
		 * The meta object literal for the '<em>Constraint Unprocessable Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getConstraintUnprocessableException()
		 * @generated
		 */
		EDataType CONSTRAINT_UNPROCESSABLE_EXCEPTION = eINSTANCE.getConstraintUnprocessableException();

		/**
		 * The meta object literal for the '<em>ECross Reference Adapter</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.util.ECrossReferenceAdapter
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getECrossReferenceAdapter()
		 * @generated
		 */
		EDataType ECROSS_REFERENCE_ADAPTER = eINSTANCE.getECrossReferenceAdapter();

		/**
		 * The meta object literal for the '<em>OCL Environment</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ocl.ecore.EcoreEnvironment
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getOCLEnvironment()
		 * @generated
		 */
		EDataType OCL_ENVIRONMENT = eINSTANCE.getOCLEnvironment();

		/**
		 * The meta object literal for the '<em>OCL</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ocl.ecore.OCL
		 * @see de.upb.swt.qvt.tgg.interpreter.impl.InterpreterPackageImpl#getOCL()
		 * @generated
		 */
		EDataType OCL = eINSTANCE.getOCL();

	}

} //InterpreterPackage
