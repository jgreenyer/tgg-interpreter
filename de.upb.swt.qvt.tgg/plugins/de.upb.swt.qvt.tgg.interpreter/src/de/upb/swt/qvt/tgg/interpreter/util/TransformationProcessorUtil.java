package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.query.conditions.eobjects.EObjectCondition;
import org.eclipse.emf.query.conditions.eobjects.EObjectTypeRelationCondition;
import org.eclipse.emf.query.conditions.eobjects.TypeRelation;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;

public class TransformationProcessorUtil {

	
	public static EList<EObject> getSourceDomainObjectsMatchableByNodes(EList<Node> nodes, ApplicationScenario applicationScenario) {

		EList<EObject> sourceDomainObjectList = new UniqueEList<EObject>();

		EList<EClass> eClassesToConsider = new UniqueEList<EClass>();
		EList<EClass> eClassesToConsiderSubtypes = new UniqueEList<EClass>();

		for (Node node : nodes) {
			eClassesToConsider.add(node.getEType());
			if (node.isMatchSubtype())
				eClassesToConsiderSubtypes.add(node.getEType());
		}

		EList<Resource> sourceModelResourceList = new UniqueEList<Resource>();
		for (DomainModel domainModel : applicationScenario.getSourceDomainModel()) {
			for (EObject domainRootObject : domainModel.getRootObject()) {
				sourceModelResourceList.add(domainRootObject.eResource());
			}
		}

		for (Resource resource : sourceModelResourceList) {
			TreeIterator<EObject> treeIterator = resource.getAllContents();
			while (treeIterator.hasNext()) {
				EObject modelObject = treeIterator.next();
				if (eClassesToConsider.contains(modelObject.eClass())) {
					sourceDomainObjectList.add(modelObject);
				} else {
					for (EClass eClassToConsiderSubtype : eClassesToConsiderSubtypes) {
						EObjectCondition typeRelationCondition = new EObjectTypeRelationCondition(
								eClassToConsiderSubtype, TypeRelation.SAMETYPE_OR_SUBTYPE_LITERAL);
						if (typeRelationCondition.isSatisfied(modelObject)) {
							sourceDomainObjectList.add(modelObject);
							continue;
						}
					}
				}
			}
		}

		return sourceDomainObjectList;
	}
	
	
	public static EList<EObject> filterEObjectsMatchableByNodesFromList(EList<EObject> domainObjects, EList<Node> nodes) {
		EList<EObject> filteredDomainObjectList = new UniqueEList<EObject>();

		EList<EClass> eClassesToConsider = new UniqueEList<EClass>();
		EList<EClass> eClassesToConsiderSubtypes = new UniqueEList<EClass>();

		for (Node node : nodes) {
			eClassesToConsider.add(node.getEType());
			if (node.isMatchSubtype())
				eClassesToConsiderSubtypes.add(node.getEType());
		}

		for (EObject domainObject : domainObjects) {
			if (eClassesToConsider.contains(domainObject.eClass())) {
				filteredDomainObjectList.add(domainObject);
			} else {
				for (EClass eClassToConsiderSubtype : eClassesToConsiderSubtypes) {
					EObjectCondition typeRelationCondition = new EObjectTypeRelationCondition(eClassToConsiderSubtype,
							TypeRelation.SAMETYPE_OR_SUBTYPE_LITERAL);
					if (typeRelationCondition.isSatisfied(domainObject)) {
						filteredDomainObjectList.add(domainObject);
						continue;
					}
				}
			}
		}

		return filteredDomainObjectList;
	}

	/**
	 * This method checks whether the node matches the given candidate object. It also considers
	 * the fact that a TGG {@link Node} may match exactly an object that is instance of a class which
	 * is equal class or an object that is instance of a subclass of the node's type class
	 * (see {@link Node#isMatchSubtype()}).
	 * 
	 * @param node
	 * @param candidateEObject
	 * @return whether the node matches the given candidate object
	 */
	public static boolean nodeTypeMatches(Node node, EObject candidateEObject){
		if (node.isMatchSubtype()){
			return (EcorePackage.Literals.EOBJECT == node.getEType())
					|| (node.getEType() == candidateEObject.eClass())
					|| (candidateEObject.eClass().getEAllSuperTypes().contains(node.getEType()));
//			return (new EObjectTypeRelationCondition(node.getEType(), TypeRelation.SAMETYPE_OR_SUBTYPE_LITERAL).isSatisfied(candidateEObject));
//			return isSameOrSubtype(node.getEType(), candidateEObject.eClass());
		}else{
			return (node.getEType() == candidateEObject.eClass());
		}
		
	}
	
	
}