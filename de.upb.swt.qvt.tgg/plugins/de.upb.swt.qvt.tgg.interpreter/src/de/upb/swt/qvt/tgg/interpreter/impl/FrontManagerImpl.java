/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Front Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl#getFrontTransformationProcessor <em>Front Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontManagerImpl#getNodeBinding <em>Node Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FrontManagerImpl extends EObjectImpl implements FrontManager {

	/**
	 * The cached value of the '{@link #getNodeBinding() <em>Node Binding</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeBinding()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<Node>> nodeBinding;
	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(FrontManagerImpl.class.getName());

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FrontManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.FRONT_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrontTransformationProcessor getFrontTransformationProcessor() {
		if (eContainerFeatureID() != InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR) return null;
		return (FrontTransformationProcessor)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrontTransformationProcessor(FrontTransformationProcessor newFrontTransformationProcessor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFrontTransformationProcessor, InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrontTransformationProcessor(FrontTransformationProcessor newFrontTransformationProcessor) {
		if (newFrontTransformationProcessor != eInternalContainer() || (eContainerFeatureID() != InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR && newFrontTransformationProcessor != null)) {
			if (EcoreUtil.isAncestor(this, newFrontTransformationProcessor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFrontTransformationProcessor != null)
				msgs = ((InternalEObject)newFrontTransformationProcessor).eInverseAdd(this, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER, FrontTransformationProcessor.class, msgs);
			msgs = basicSetFrontTransformationProcessor(newFrontTransformationProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR, newFrontTransformationProcessor, newFrontTransformationProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<Node>> getNodeBinding() {
		if (nodeBinding == null) {
			nodeBinding = new EcoreEMap<EObject,EList<Node>>(InterpreterconfigurationPackage.Literals.EOBJECT_TO_NODE_MAP_ENTRY, EObjectToNodeMapEntryImpl.class, this, InterpreterPackage.FRONT_MANAGER__NODE_BINDING);
		}
		return nodeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public boolean update() {

		//Iterator<java.util.Map.Entry<EObject, EList<Node>>> frontMapEntryIterator = getNodeBinding().entrySet().iterator();
		
		ArrayList<java.util.Map.Entry<EObject, EList<Node>>> bindingEntriesToRemove = new ArrayList<java.util.Map.Entry<EObject, EList<Node>>>();
		
		for (java.util.Map.Entry<EObject, EList<Node>> nodeBinding : getNodeBinding().entrySet()) {
			
			if(logger.isDebugEnabled())
				logger.debug("Considering to remove NodeBinding: " + nodeBinding.getKey());

			boolean hasUnboundNeighbours = false;

			//TODO jgreen: actually, we also need to check the cross-references here, don't we?
			
			for (EReference reference : nodeBinding.getKey().eClass().getEAllReferences()) {
				if (getFrontTransformationProcessor().getInterpreter().getConfiguration().
						getTripleGraphGrammar().getRelevantReferences().contains(reference)){
					Collection<EObject> neighborObjectsList;
					if (!reference.isMany()){
						neighborObjectsList = new BasicEList<EObject>();
						neighborObjectsList.add((EObject) nodeBinding.getKey().eGet(reference, true));
					}else{
						neighborObjectsList = (Collection<EObject>) nodeBinding.getKey().eGet(reference, true);
//						// paphko: Somehow, I get null in some cases... Should not happen in EMF but it does..
//						if (neighborObjectsList == null) {
//							logger.warn("Collection '"+reference.getName()+"' is null! This should not happen. Model element: " + nodeBinding.getKey());
//							neighborObjectsList = new BasicEList<EObject>();
//						}
					}
					
					for (EObject neighborObject : neighborObjectsList) {
						if(neighborObject != null && !getFrontTransformationProcessor().
								getGlobalNodeBinding().keySet().contains(neighborObject)){
							if (logger.isDebugEnabled()) {
								logger.debug(" keeping NodeBinding: unbound neighbor object found: " + neighborObject);
							}
							hasUnboundNeighbours = true;
							break;
						} else if (neighborObject != null) {
							//see, if there is already an edgebinding for such an edge
							EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
							virtualModelEdge.add(nodeBinding.getKey());
							virtualModelEdge.add(neighborObject);
							virtualModelEdge.add(reference);
							if (!getFrontTransformationProcessor().getGlobalEdgeBinding().keySet().contains(virtualModelEdge)) {
								if (logger.isDebugEnabled()) {
									logger.debug(" keeping NodeBinding: virtualModelEdge not in edgebindings: " + virtualModelEdge);
								}
								hasUnboundNeighbours = true;
								break;
							}							
						}
					}
					if (hasUnboundNeighbours) break; // leave for look
				}
			}
			if (hasUnboundNeighbours) continue; // skip rest of while
			
			if (!hasUnboundNeighbours //no unbound neighbors found yet
					//and if the conainingFeature is not null...
					&& nodeBinding.getKey().eContainingFeature() != null
					//and if the conainingFeature of the modelObject is a 'relevant' reference 
					&& getFrontTransformationProcessor().getInterpreter().getConfiguration().getTripleGraphGrammar().
						getRelevantReferences().contains(nodeBinding.getKey().eContainingFeature())
					//and if the containing object is not null
					&& nodeBinding.getKey().eContainer() != null
					//and the containing object is not globally bound..
					&& !getFrontTransformationProcessor().getGlobalNodeBinding().keySet().contains(nodeBinding.getKey().eContainer())){
				if (logger.isDebugEnabled()) {
					logger.debug(" keeping NodeBinding: modelObject.eContainingFeature(): " 
							+ nodeBinding.getKey().eContainingFeature() 
							+ "; modelObject.eContainer(): " 
							+ nodeBinding.getKey().eContainer());
				}
				hasUnboundNeighbours = true;
				break;
			}
			

			ECrossReferenceAdapter eCrossReferenceAdapter = getFrontTransformationProcessor().getCrossReferenceAdapter();
			
			if (!hasUnboundNeighbours && eCrossReferenceAdapter.getNonNavigableInverseReferences(nodeBinding.getKey()) != null){
				
				for (EStructuralFeature.Setting setting : eCrossReferenceAdapter.getNonNavigableInverseReferences(nodeBinding.getKey())) {
					if (getFrontTransformationProcessor().getInterpreter().getConfiguration().getTripleGraphGrammar().
							getRelevantReferences().contains(setting.getEStructuralFeature())){
						EObject neighborObject = (EObject) setting.getEObject();
						if(neighborObject != null && !getFrontTransformationProcessor().getGlobalNodeBinding().keySet().contains(neighborObject)){
							if (logger.isDebugEnabled()) {
								logger.debug(" keeping NodeBinding: unbound neighbor object found: " + neighborObject);
							}
							hasUnboundNeighbours = true;
							break;
						}
					}
				}
			}
						
			if (!hasUnboundNeighbours){
				if (logger.isDebugEnabled()) {
					logger.debug("  removing NodeBinding: " + nodeBinding.getKey());
				}
				//getNodeBinding().remove(nodeBinding.getKey());
				//getNodeBinding().values().remove(nodeBinding);
				//frontMapEntryIterator.remove();
				bindingEntriesToRemove.add(nodeBinding);
			}

		}

		getNodeBinding().removeAll(bindingEntriesToRemove);
		
		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFrontTransformationProcessor((FrontTransformationProcessor)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				return basicSetFrontTransformationProcessor(null, msgs);
			case InterpreterPackage.FRONT_MANAGER__NODE_BINDING:
				return ((InternalEList<?>)getNodeBinding()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				return eInternalContainer().eInverseRemove(this, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER, FrontTransformationProcessor.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				return getFrontTransformationProcessor();
			case InterpreterPackage.FRONT_MANAGER__NODE_BINDING:
				if (coreType) return getNodeBinding();
				else return getNodeBinding().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				setFrontTransformationProcessor((FrontTransformationProcessor)newValue);
				return;
			case InterpreterPackage.FRONT_MANAGER__NODE_BINDING:
				((EStructuralFeature.Setting)getNodeBinding()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				setFrontTransformationProcessor((FrontTransformationProcessor)null);
				return;
			case InterpreterPackage.FRONT_MANAGER__NODE_BINDING:
				getNodeBinding().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR:
				return getFrontTransformationProcessor() != null;
			case InterpreterPackage.FRONT_MANAGER__NODE_BINDING:
				return nodeBinding != null && !nodeBinding.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FrontManagerImpl
