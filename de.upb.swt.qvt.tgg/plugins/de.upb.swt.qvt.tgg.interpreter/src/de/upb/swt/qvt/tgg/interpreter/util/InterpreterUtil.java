package de.upb.swt.qvt.tgg.interpreter.util;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EContentsEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class InterpreterUtil {
	
	public static class RelevantReferencesCrossReferencer extends EcoreUtil.CrossReferencer{

		protected Collection<?> relevantEReferences;
		
		protected RelevantReferencesCrossReferencer(Collection<?> emfObjects, Collection<?> relevantEReferences) {
			super(emfObjects);
			this.relevantEReferences = relevantEReferences;
		}
		
	    /**
	     * Return true if the specified eReference from eObject to crossReferencedEObject should be
	     * considered a cross reference by this cross referencer.
	     * @param eObject an object in the cross referencer's content tree.
	     * @param eReference a reference from the object.
	     * @param crossReferencedEObject the target of the specified reference.
	     * @return if the cross referencer should consider the specified reference a cross reference.
	     */
		@Override
	    protected boolean crossReference(EObject eObject, EReference eReference, EObject crossReferencedEObject)
	    {
			return relevantEReferences.contains(eReference);
	    }

	    @Override
	    protected void handleCrossReference(EObject eObject)
	    {
	    	super.handleCrossReference(eObject);
	    	handleContainmentReferences(eObject);
	    }
		
		protected void handleContainmentReferences(EObject eObject){
			
			InternalEObject internalEObject = (InternalEObject)eObject;
			for (EContentsEList.FeatureIterator<EObject> containmentReferences 
					= (EContentsEList.FeatureIterator<EObject>) internalEObject.eContents().iterator(); containmentReferences.hasNext();)
			{
				EObject containedEObject = containmentReferences.next();
				if (containedEObject != null)
				{
					EReference eReference = (EReference)containmentReferences.feature();
					if (crossReference(internalEObject, eReference, containedEObject))
					{
						add(internalEObject, eReference, containedEObject);
						if (eReference.getEOpposite() != null){
							add((InternalEObject) containedEObject, eReference.getEOpposite(), internalEObject);
						}
					}
				}
			}
		}
		
	    /**
	     * Reset this cross referencer's object set.
	     */
		@Override
	    protected void done()
	    {
	      emfObjects = null;
	      relevantEReferences = null;
	    }
		
	    /**
	     * Returns a map of all cross references in the content tree.
	     * @param emfObjects a collection of objects whose combined content trees should be considered.
	     * @return a map of cross references.
	     */
	    public static Map<EObject, Collection<EStructuralFeature.Setting>> find(Collection<?> emfObjects, Collection<?> relevantEReferences)
	    {
	    	RelevantReferencesCrossReferencer result = new RelevantReferencesCrossReferencer(emfObjects, relevantEReferences);
	    	result.crossReference();
	    	result.done();
	    	return result;
	    }
	    
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		  
	  }
	
}
