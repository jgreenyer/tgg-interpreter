/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher <em>Graph Matcher</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getNodeEnforcementPolicy <em>Node Enforcement Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getEdgeEnforcementPolicy <em>Edge Enforcement Policy</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getConstraintEnforcementPolicy <em>Constraint Enforcement Policy</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor()
 * @model
 * @generated
 */
public interface RuleProcessor extends EObject {

	/**
	 * Returns the value of the '<em><b>Triple Graph Grammar Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triple Graph Grammar Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triple Graph Grammar Rule</em>' reference.
	 * @see #setTripleGraphGrammarRule(TripleGraphGrammarRule)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_TripleGraphGrammarRule()
	 * @model required="true"
	 * @generated
	 */
	TripleGraphGrammarRule getTripleGraphGrammarRule();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTripleGraphGrammarRule <em>Triple Graph Grammar Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Triple Graph Grammar Rule</em>' reference.
	 * @see #getTripleGraphGrammarRule()
	 * @generated
	 */
	void setTripleGraphGrammarRule(TripleGraphGrammarRule value);

	/**
	 * Returns the value of the '<em><b>Transformation Processor</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Processor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Processor</em>' container reference.
	 * @see #setTransformationProcessor(TransformationProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_TransformationProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor
	 * @model opposite="ruleProcessor" required="true" transient="false"
	 * @generated
	 */
	TransformationProcessor getTransformationProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor <em>Transformation Processor</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Processor</em>' container reference.
	 * @see #getTransformationProcessor()
	 * @generated
	 */
	void setTransformationProcessor(TransformationProcessor value);

	/**
	 * Returns the value of the '<em><b>Graph Matcher</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor <em>Rule Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Matcher</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Matcher</em>' containment reference.
	 * @see #setGraphMatcher(GraphMatcher)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_GraphMatcher()
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getRuleProcessor
	 * @model opposite="ruleProcessor" containment="true" required="true"
	 * @generated
	 */
	GraphMatcher getGraphMatcher();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getGraphMatcher <em>Graph Matcher</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Matcher</em>' containment reference.
	 * @see #getGraphMatcher()
	 * @generated
	 */
	void setGraphMatcher(GraphMatcher value);

	/**
	 * Returns the value of the '<em><b>Node Enforcement Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Enforcement Policy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Enforcement Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_NodeEnforcementPolicy()
	 * @model
	 * @generated
	 */
	EList<INodeEnforcementPolicy> getNodeEnforcementPolicy();

	/**
	 * Returns the value of the '<em><b>Edge Enforcement Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Enforcement Policy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Enforcement Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_EdgeEnforcementPolicy()
	 * @model
	 * @generated
	 */
	EList<IEdgeEnforcementPolicy> getEdgeEnforcementPolicy();

	/**
	 * Returns the value of the '<em><b>Constraint Enforcement Policy</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Enforcement Policy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Enforcement Policy</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getRuleProcessor_ConstraintEnforcementPolicy()
	 * @model
	 * @generated
	 */
	EList<IConstraintEnforcementPolicy> getConstraintEnforcementPolicy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method applies the rule based on an initial rule binding, which contains at 
	 * least one candidate node binding as a starting point for the graph matching.
	 * 
	 * The rule application works as follows:
	 * 1. Initialize graphMatcher with initial rule binding
	 * 2. call graphMatcher.match()
	 * 3. if macht returns true, enfore the graph patterns that have to be enforced.
	 * 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean processRule(RuleBinding initialRuleBinding);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method tries to repair an invalid rule binding by trying to re-enforce the
	 * constraints and rebuilding the rule structure.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean repairRule(RuleBinding ruleBinding);

} // RuleProcessor
