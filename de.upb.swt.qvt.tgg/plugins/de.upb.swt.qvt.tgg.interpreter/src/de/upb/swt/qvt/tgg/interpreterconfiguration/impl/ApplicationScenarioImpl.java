/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Application Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getSourceDomainModel <em>Source Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getTargetDomainModel <em>Target Domain Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getCorrespondenceModel <em>Correspondence Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getSourceDomainObject <em>Source Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getCorrespondenceDomainObject <em>Correspondence Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getTargetDomainObject <em>Target Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getSourceDomainNode <em>Source Domain Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getCorrespondenceDomainNode <em>Correspondence Domain Node</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getTargetDomainNodes <em>Target Domain Nodes</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getSourceDomainNodesByTGGRule <em>Source Domain Nodes By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getCorrespondenceDomainNodesByTGGRule <em>Correspondence Domain Nodes By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getTargetDomainNodeByTGGRule <em>Target Domain Node By TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl#isDeactivateFront <em>Deactivate Front</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApplicationScenarioImpl extends EObjectImpl implements ApplicationScenario {
	/**
	 * The cached value of the '{@link #getSourceDomainModel() <em>Source Domain Model</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDomainModel()
	 * @generated
	 * @ordered
	 */
	protected EList<DomainModel> sourceDomainModel;

	/**
	 * The cached value of the '{@link #getTargetDomainModel() <em>Target Domain Model</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDomainModel()
	 * @generated
	 * @ordered
	 */
	protected EList<DomainModel> targetDomainModel;

	/**
	 * The cached value of the '{@link #getCorrespondenceModel() <em>Correspondence Model</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondenceModel()
	 * @generated
	 * @ordered
	 */
	protected EList<DomainModel> correspondenceModel;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final ApplicationMode MODE_EDEFAULT = ApplicationMode.TRANSFORM;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected ApplicationMode mode = MODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceDomainNode() <em>Source Domain Node</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDomainNode()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> sourceDomainNode;

	/**
	 * The cached value of the '{@link #getCorrespondenceDomainNode() <em>Correspondence Domain Node</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondenceDomainNode()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> correspondenceDomainNode;

	/**
	 * The cached value of the '{@link #getTargetDomainNodes() <em>Target Domain Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDomainNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> targetDomainNodes;

	/**
	 * The cached value of the '{@link #getSourceDomainNodesByTGGRule() <em>Source Domain Nodes By TGG Rule</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDomainNodesByTGGRule()
	 * @generated
	 * @ordered
	 */
	protected EMap<TripleGraphGrammarRule, EList<Node>> sourceDomainNodesByTGGRule;

	/**
	 * The cached value of the '{@link #getCorrespondenceDomainNodesByTGGRule() <em>Correspondence Domain Nodes By TGG Rule</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondenceDomainNodesByTGGRule()
	 * @generated
	 * @ordered
	 */
	protected EMap<TripleGraphGrammarRule, EList<Node>> correspondenceDomainNodesByTGGRule;

	/**
	 * The cached value of the '{@link #getTargetDomainNodeByTGGRule() <em>Target Domain Node By TGG Rule</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDomainNodeByTGGRule()
	 * @generated
	 * @ordered
	 */
	protected EMap<TripleGraphGrammarRule, EList<Node>> targetDomainNodeByTGGRule;

	/**
	 * The default value of the '{@link #isDeactivateFront() <em>Deactivate Front</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeactivateFront()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEACTIVATE_FRONT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeactivateFront() <em>Deactivate Front</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeactivateFront()
	 * @generated
	 * @ordered
	 */
	protected boolean deactivateFront = DEACTIVATE_FRONT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DomainModel> getSourceDomainModel() {
		if (sourceDomainModel == null) {
			sourceDomainModel = new EObjectResolvingEList<DomainModel>(DomainModel.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL);
		}
		return sourceDomainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DomainModel> getTargetDomainModel() {
		if (targetDomainModel == null) {
			targetDomainModel = new EObjectResolvingEList<DomainModel>(DomainModel.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL);
		}
		return targetDomainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DomainModel> getCorrespondenceModel() {
		if (correspondenceModel == null) {
			correspondenceModel = new EObjectResolvingEList<DomainModel>(DomainModel.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL);
		}
		return correspondenceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationMode getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(ApplicationMode newMode) {
		ApplicationMode oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE, oldMode, mode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> getSourceDomainObject() {
		EList<EObject> sourceDomainObjectList = new EObjectResolvingEList<EObject>(EObject.class, this,
				InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT);
		for (DomainModel domainModel : getSourceDomainModel()) {
			sourceDomainObjectList.addAll(domainModel.getRootObject());
			for (EObject rootObject : domainModel.getRootObject()) {
				for (TreeIterator<EObject> eObjectTreeIterator = rootObject.eAllContents(); eObjectTreeIterator.hasNext(); )
					sourceDomainObjectList.add(eObjectTreeIterator.next());
				}
		}
		return sourceDomainObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> getCorrespondenceDomainObject() {
		EList<EObject> correspondenceDomainObjectList = new EObjectResolvingEList<EObject>(EObject.class, this,
				InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT);
		for (DomainModel domainModel : getCorrespondenceModel()) {
			correspondenceDomainObjectList.addAll(domainModel.getRootObject());
			for (EObject rootObject : domainModel.getRootObject()) {
				for (TreeIterator<EObject> eObjectTreeIterator = rootObject.eAllContents(); eObjectTreeIterator.hasNext(); )
					correspondenceDomainObjectList.add(eObjectTreeIterator.next());
				}
		}
		return correspondenceDomainObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> getTargetDomainObject() {
		EList<EObject> targetDomainObjectList = new EObjectResolvingEList<EObject>(EObject.class, this,
				InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT);
		for (DomainModel domainModel : getTargetDomainModel()) {
			targetDomainObjectList.addAll(domainModel.getRootObject());
			for (EObject rootObject : domainModel.getRootObject()) {
				for (TreeIterator<EObject> eObjectTreeIterator = rootObject.eAllContents(); eObjectTreeIterator.hasNext(); )
					targetDomainObjectList.add(eObjectTreeIterator.next());
				}
		}
		return targetDomainObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getSourceDomainNode() {
		if (sourceDomainNode == null) {
			sourceDomainNode = new EObjectResolvingEList<Node>(Node.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE);
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel domainModel : getSourceDomainModel()) {
					sourceDomainNode.addAll(
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return sourceDomainNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getCorrespondenceDomainNode() {
		if (correspondenceDomainNode == null) {
			correspondenceDomainNode = new EObjectResolvingEList<Node>(Node.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE);
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel domainModel : getCorrespondenceModel()) {
					correspondenceDomainNode.addAll(
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return correspondenceDomainNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Node> getTargetDomainNodes() {
		if (targetDomainNodes == null) {
			targetDomainNodes = new EObjectResolvingEList<Node>(Node.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODES);
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel domainModel : getTargetDomainModel()) {
					targetDomainNodes.addAll(
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return targetDomainNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<TripleGraphGrammarRule, EList<Node>> getSourceDomainNodesByTGGRule() {
		if (sourceDomainNodesByTGGRule == null) {
			sourceDomainNodesByTGGRule = new EcoreEMap<TripleGraphGrammarRule,EList<Node>>(InterpreterconfigurationPackage.Literals.TGG_RULE_TO_NODES_MAP_ENTRY, TGGRuleToNodesMapEntryImpl.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE);
			if (getConfiguration().getTripleGraphGrammar() == null) return sourceDomainNodesByTGGRule;
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				sourceDomainNodesByTGGRule.put((TripleGraphGrammarRule)rule, new UniqueEList<Node>());
				for (DomainModel domainModel : getSourceDomainModel()) {
					sourceDomainNodesByTGGRule.get((TripleGraphGrammarRule)rule).addAll(
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return sourceDomainNodesByTGGRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<TripleGraphGrammarRule, EList<Node>> getCorrespondenceDomainNodesByTGGRule() {
		if (correspondenceDomainNodesByTGGRule == null) {
			correspondenceDomainNodesByTGGRule = new EcoreEMap<TripleGraphGrammarRule,EList<Node>>(InterpreterconfigurationPackage.Literals.TGG_RULE_TO_NODES_MAP_ENTRY, TGGRuleToNodesMapEntryImpl.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE);
			if (getConfiguration().getTripleGraphGrammar() == null) return correspondenceDomainNodesByTGGRule;
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel domainModel : getCorrespondenceModel()) {
					correspondenceDomainNodesByTGGRule.put((TripleGraphGrammarRule)rule,
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return correspondenceDomainNodesByTGGRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<TripleGraphGrammarRule, EList<Node>> getTargetDomainNodeByTGGRule() {
		if (targetDomainNodeByTGGRule == null) {
			targetDomainNodeByTGGRule = new EcoreEMap<TripleGraphGrammarRule,EList<Node>>(InterpreterconfigurationPackage.Literals.TGG_RULE_TO_NODES_MAP_ENTRY, TGGRuleToNodesMapEntryImpl.class, this, InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE);
			if (getConfiguration().getTripleGraphGrammar() == null) return targetDomainNodeByTGGRule;
			for (Rule rule : getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel domainModel : getTargetDomainModel()) {
					targetDomainNodeByTGGRule.put((TripleGraphGrammarRule)rule,
							((TripleGraphGrammarRule)rule).getDomainsNodesMap().get(domainModel.getTypedModel()));
				}
			}
		}
		return targetDomainNodeByTGGRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getConfiguration() {
		if (eContainerFeatureID() != InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION) return null;
		return (Configuration)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfiguration(Configuration newConfiguration, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newConfiguration, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(Configuration newConfiguration) {
		if (newConfiguration != eInternalContainer() || (eContainerFeatureID() != InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION && newConfiguration != null)) {
			if (EcoreUtil.isAncestor(this, newConfiguration))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newConfiguration != null)
				msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO, Configuration.class, msgs);
			msgs = basicSetConfiguration(newConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION, newConfiguration, newConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeactivateFront() {
		return deactivateFront;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeactivateFront(boolean newDeactivateFront) {
		boolean oldDeactivateFront = deactivateFront;
		deactivateFront = newDeactivateFront;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT, oldDeactivateFront, deactivateFront));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetConfiguration((Configuration)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE:
				return ((InternalEList<?>)getSourceDomainNodesByTGGRule()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE:
				return ((InternalEList<?>)getCorrespondenceDomainNodesByTGGRule()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE:
				return ((InternalEList<?>)getTargetDomainNodeByTGGRule()).basicRemove(otherEnd, msgs);
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				return basicSetConfiguration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				return eInternalContainer().eInverseRemove(this, InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO, Configuration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL:
				return getSourceDomainModel();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL:
				return getTargetDomainModel();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL:
				return getCorrespondenceModel();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME:
				return getName();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE:
				return getMode();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT:
				return getSourceDomainObject();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT:
				return getCorrespondenceDomainObject();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT:
				return getTargetDomainObject();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE:
				return getSourceDomainNode();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE:
				return getCorrespondenceDomainNode();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODES:
				return getTargetDomainNodes();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE:
				if (coreType) return getSourceDomainNodesByTGGRule();
				else return getSourceDomainNodesByTGGRule().map();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE:
				if (coreType) return getCorrespondenceDomainNodesByTGGRule();
				else return getCorrespondenceDomainNodesByTGGRule().map();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE:
				if (coreType) return getTargetDomainNodeByTGGRule();
				else return getTargetDomainNodeByTGGRule().map();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				return getConfiguration();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT:
				return isDeactivateFront();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL:
				getSourceDomainModel().clear();
				getSourceDomainModel().addAll((Collection<? extends DomainModel>)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL:
				getTargetDomainModel().clear();
				getTargetDomainModel().addAll((Collection<? extends DomainModel>)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL:
				getCorrespondenceModel().clear();
				getCorrespondenceModel().addAll((Collection<? extends DomainModel>)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME:
				setName((String)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE:
				setMode((ApplicationMode)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				setConfiguration((Configuration)newValue);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT:
				setDeactivateFront((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL:
				getSourceDomainModel().clear();
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL:
				getTargetDomainModel().clear();
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL:
				getCorrespondenceModel().clear();
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME:
				setName(NAME_EDEFAULT);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE:
				setMode(MODE_EDEFAULT);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				setConfiguration((Configuration)null);
				return;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT:
				setDeactivateFront(DEACTIVATE_FRONT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL:
				return sourceDomainModel != null && !sourceDomainModel.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL:
				return targetDomainModel != null && !targetDomainModel.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL:
				return correspondenceModel != null && !correspondenceModel.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE:
				return mode != MODE_EDEFAULT;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT:
				return !getSourceDomainObject().isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT:
				return !getCorrespondenceDomainObject().isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT:
				return !getTargetDomainObject().isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE:
				return sourceDomainNode != null && !sourceDomainNode.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE:
				return correspondenceDomainNode != null && !correspondenceDomainNode.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODES:
				return targetDomainNodes != null && !targetDomainNodes.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE:
				return sourceDomainNodesByTGGRule != null && !sourceDomainNodesByTGGRule.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE:
				return correspondenceDomainNodesByTGGRule != null && !correspondenceDomainNodesByTGGRule.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE:
				return targetDomainNodeByTGGRule != null && !targetDomainNodeByTGGRule.isEmpty();
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CONFIGURATION:
				return getConfiguration() != null;
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT:
				return deactivateFront != DEACTIVATE_FRONT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", mode: ");
		result.append(mode);
		result.append(", deactivateFront: ");
		result.append(deactivateFront);
		result.append(')');
		return result.toString();
	}

} //ApplicationScenarioImpl
