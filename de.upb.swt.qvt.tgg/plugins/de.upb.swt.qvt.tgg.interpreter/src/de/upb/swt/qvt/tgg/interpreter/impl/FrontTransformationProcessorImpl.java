/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.query.conditions.Condition;
import org.eclipse.emf.query.conditions.eobjects.EObjectTypeRelationCondition;
import org.eclipse.emf.query.conditions.eobjects.TypeRelation;

import de.upb.swt.qvt.qvtbase.Rule;
import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.FrontManager;
import de.upb.swt.qvt.tgg.interpreter.FrontTransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.DummyTupleGenerator;
import de.upb.swt.qvt.tgg.interpreter.util.InitialNodeBindingGenerator;
import de.upb.swt.qvt.tgg.interpreter.util.InterpreterStatus;
import de.upb.swt.qvt.tgg.interpreter.util.TransformationProcessorUtil;
import de.upb.swt.qvt.tgg.interpreter.util.TupleGenerator;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Front Transformation Processor</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl#getFrontManager <em>Front Manager</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl#getSourceDomainObjects <em>Source Domain Objects</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl#getCurrentTGGRule <em>Current TGG Rule</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.FrontTransformationProcessorImpl#getUnmatchedSourceDomainObjects <em>Unmatched Source Domain Objects</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FrontTransformationProcessorImpl extends TransformationProcessorImpl implements
		FrontTransformationProcessor {

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(
			FrontTransformationProcessorImpl.class.getName());

	/**
	 * The cached value of the '{@link #getFrontManager() <em>Front Manager</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrontManager()
	 * @generated
	 * @ordered
	 */
	protected FrontManager frontManager;
	/**
	 * The cached value of the '{@link #getSourceDomainObjects() <em>Source Domain Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDomainObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> sourceDomainObjects;

	protected IProgressMonitor newProgressMonitor;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected FrontTransformationProcessorImpl() {
		super();

		// set up the front manager
		setFrontManager(InterpreterFactory.eINSTANCE.createFrontManager());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void setProgressMonitor(IProgressMonitor newProgressMonitor) {
		super.setProgressMonitor(newProgressMonitor);
		progressMonitor.beginTask("Transformation", getSourceDomainObjects().size());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public boolean rulesApplicable() {
		if (getGlobalNodeBinding().isEmpty()) {
			// No global node bindings exist yet => can we generate another
			// candidate axiom node binding?
			return getAxiomBindingGenerator().hasNext();
		} else {
			// Can we generate another candidate node binding?
			return getInitialNodeBindingGenerator().hasNext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public RuleBinding createInitialRuleBinding() {

		RuleBinding initialRuleBinding = InterpreterconfigurationFactory.eINSTANCE.createRuleBinding();

		if (getGlobalNodeBinding().isEmpty()) {
			/*
			 *  No global node bindings exist yet ==> apply axiom.
			 *  
			 *  (1) If the configuration contains an initial AxiomBinding, use it. To do this,
			 *      it is necessary to take the initial node bindings and push them on the node 
			 *      binding stack. Then this rule binding is returned.
			 *  
			 *  (2) Otherwise create a candidate start binding for the axiom via
			 *      getAxiomBindingGenerator().getNextTuple() 
			 */
			if (getInterpreter().getConfiguration().getRuleBindingContainer() != null 
					&& getInterpreter().getConfiguration().getRuleBindingContainer().getInitialAxiomBinding() != null
					&& getInterpreter().getConfiguration().getRuleBindingContainer().getInitialAxiomBinding().getGraphElementStack().isEmpty()){
				initialRuleBinding = getInterpreter().getConfiguration().getRuleBindingContainer().getInitialAxiomBinding();
				if (initialRuleBinding.getTggRule() == null && !((AxiomBinding) initialRuleBinding).getInitialAxiomNodeBinding().isEmpty()){
					initialRuleBinding.setTggRule((TripleGraphGrammarRule) ((AxiomBinding) initialRuleBinding).getInitialAxiomNodeBinding().get(0).getNode().getGraph());
				}
				for (InitialAxiomNodeBinding initialAxiomNodeBinding : ((AxiomBinding) initialRuleBinding).getInitialAxiomNodeBinding()) {
					initialRuleBinding.pushNodeBinding(initialAxiomNodeBinding.getNode(), initialAxiomNodeBinding.getEObject());
				
					//--DEBUG--//
					DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
					if(ds!=null)((DebugSupportImpl)ds).debugSupportForMatchedNode(initialAxiomNodeBinding.getNode(), initialAxiomNodeBinding.getEObject(), initialRuleBinding, true, true, null);
					
				}
				logger.debug("Node bindings specified for the initial AXIOM rule binding: " + initialRuleBinding);
			} else {
				Object[] nextAxiomTuple = getAxiomBindingGenerator().getNextTuple();
				initialRuleBinding.pushNodeBinding((Node) nextAxiomTuple[0], (EObject) nextAxiomTuple[1]);
				
				//--DEBUG--//
				DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
				if(ds!=null)((DebugSupportImpl)ds).debugSupportForMatchedNode((Node) nextAxiomTuple[0], (EObject) nextAxiomTuple[1], initialRuleBinding, true, true, null);
				
				initialRuleBinding.setTggRule((TripleGraphGrammarRule) ((Node) nextAxiomTuple[0]).getGraph());
				logger.debug("Creating an initial rule binding for the AXIOM: " + initialRuleBinding);
			}
		} else {
			Object[] nextTuple = getInitialNodeBindingGenerator().getNextTuple();
			initialRuleBinding.pushNodeBinding((Node) nextTuple[2], (EObject) nextTuple[1]);
			
			//--DEBUG--//
			DebugSupport ds = this.getRuleProcessor().getTransformationProcessor().getInterpreter().getDebugSupport();
			if(ds!=null)((DebugSupportImpl)ds).debugSupportForMatchedNode((Node) nextTuple[2], (EObject) nextTuple[1], initialRuleBinding, true, true, null);
			
			initialRuleBinding.setTggRule((TripleGraphGrammarRule) nextTuple[0]);
			if (logger.isDebugEnabled())
				logger.debug("Created an initial rule binding: " + initialRuleBinding);
		}
		return initialRuleBinding;
	}

	Map<EList<?>, EList<EObject>> eClassToRelevantSourceObjectsMap = new HashMap<EList<?>, EList<EObject>>();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule getCurrentTGGRule() {
		if (currentTGGRule != null && currentTGGRule.eIsProxy()) {
			InternalEObject oldCurrentTGGRule = (InternalEObject)currentTGGRule;
			currentTGGRule = (TripleGraphGrammarRule)eResolveProxy(oldCurrentTGGRule);
			if (currentTGGRule != oldCurrentTGGRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE, oldCurrentTGGRule, currentTGGRule));
			}
		}
		return currentTGGRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TripleGraphGrammarRule basicGetCurrentTGGRule() {
		return currentTGGRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentTGGRule(TripleGraphGrammarRule newCurrentTGGRule) {
		TripleGraphGrammarRule oldCurrentTGGRule = currentTGGRule;
		currentTGGRule = newCurrentTGGRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE, oldCurrentTGGRule, currentTGGRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<EObject> getUnmatchedSourceDomainObjects() {
		if (unmatchedSourceDomainObjects == null) {
			unmatchedSourceDomainObjects = new UniqueEList<EObject>();
			for (EObject eObject : getSourceDomainObjects()) {
				if (getGlobalNodeBinding().get(eObject) == null) {
					unmatchedSourceDomainObjects.add(eObject);
				}
			}
		}
		return unmatchedSourceDomainObjects;
	}

	protected TupleGenerator axiomNodeBindingGenerator;

	protected TupleGenerator getAxiomBindingGenerator() {

		if (axiomNodeBindingGenerator == null) {

			Object[][] axiomNodeBindingTupleGeneratorInputArrays = new Object[2][];

			// first array: all axiom nodes of source domain(s)
			List<EObject> axiomSourceNodesList = new ArrayList<EObject>();
			for (DomainModel domainModel : getInterpreter().getConfiguration().getActiveApplicationScenario()
					.getSourceDomainModel()) {
				axiomSourceNodesList.addAll(getInterpreter().getConfiguration().getTripleGraphGrammar().getAxiom()
						.getDomainsNodesMap().get(domainModel.getTypedModel()));
			}
			axiomNodeBindingTupleGeneratorInputArrays[0] = axiomSourceNodesList.toArray();

			// second array: all source domain objects:
			axiomNodeBindingTupleGeneratorInputArrays[1] = getInterpreter().getConfiguration()
					.getActiveApplicationScenario().getSourceDomainObject().toArray();

			Condition axiomTupleCondition = new Condition() {

				@Override
				public boolean isSatisfied(Object object) {
					Object[] tuple = (Object[]) object;
					Node axiomNode = (Node) tuple[0];
					EObject domainObject = (EObject) tuple[1];
					if (axiomNode.isMatchSubtype()) {
						return (new EObjectTypeRelationCondition(axiomNode.getEType(),
								TypeRelation.SAMETYPE_OR_SUBTYPE_LITERAL)).isSatisfied(domainObject);
					} else {
						return (axiomNode.getEType() == domainObject.eClass());
					}
				}
			};
			axiomNodeBindingGenerator = new TupleGenerator(axiomNodeBindingTupleGeneratorInputArrays,
					axiomTupleCondition);
		}
		return axiomNodeBindingGenerator;
	}

	//
	// if (axiomNodeBindingGenerator == null) {
	// axiomNodeBindingGenerator =
	// InterpreterFactory.eINSTANCE.createAxiomNodeBindingGenerator();
	// // first list: all axiom nodes of source domain(s)
	// EList<EObject> axiomSourceNodesList = new UniqueEList<EObject>();
	// for (DomainModel domainModel :
	// getInterpreter().getConfiguration().getActiveApplicationScenario()
	// .getSourceDomainModel()) {
	// axiomSourceNodesList.addAll(getInterpreter().getConfiguration().getTripleGraphGrammar().getAxiom()
	// .getDomainsNodesMap().get(domainModel.getTypedModel()));
	// }
	// axiomNodeBindingGenerator.getInputLists().add(axiomSourceNodesList);
	// axiomNodeBindingGenerator.getInputLists().add(
	// getInterpreter().getConfiguration().getActiveApplicationScenario().getSourceDomainObject());
	// axiomNodeBindingGenerator.setTupelConstraint(new Condition() {
	//
	// @SuppressWarnings("unchecked")
	// @Override
	// public boolean isSatisfied(Object object) {
	// Node axiomNode = (Node) ((EList<EObject>) object).get(0);
	// EObject domainObject = ((EList<EObject>) object).get(1);
	// if (axiomNode.isMatchSubtype()) {
	// return (new EObjectTypeRelationCondition(axiomNode.getEType(),
	// TypeRelation.SAMETYPE_OR_SUBTYPE_LITERAL)).isSatisfied(domainObject);
	// } else {
	// return (axiomNode.getEType() == domainObject.eClass());
	// }
	// }
	// });
	// }
	// if (axiomNodeBindingGenerator != null &&
	// axiomNodeBindingGenerator.eIsProxy()) {
	// InternalEObject oldAxiomBindingGenerator = (InternalEObject)
	// axiomNodeBindingGenerator;
	// axiomNodeBindingGenerator = (AxiomNodeBindingGenerator)
	// eResolveProxy(oldAxiomBindingGenerator);
	// if (axiomNodeBindingGenerator != oldAxiomBindingGenerator) {
	// if (eNotificationRequired())
	// eNotify(new ENotificationImpl(this, Notification.RESOLVE,
	// InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__AXIOM_NODE_BINDING_GENERATOR,
	// oldAxiomBindingGenerator, axiomNodeBindingGenerator));
	// }
	// }
	// return axiomNodeBindingGenerator;
	// }

	protected InitialNodeBindingGenerator initialNodeBindingGenerator;

	protected void resetInitialNodeBindingGenerator() {
		initialNodeBindingGenerator = null;
	}

	protected TupleGenerator getInitialNodeBindingGenerator() {
		if (initialNodeBindingGenerator == null) {

			Object[][] initialNodeBindingInputArrays = new Object[3][];

//			if (tggRuleList == null){
//				// a) list: all TGG rules (without Axiom)
//				tggRuleList = getHelper().getSortedTGGRuleList(); 
//			}

			
//			String ruleNames = "";
//			for (TripleGraphGrammarRule tggRule : tggRuleList) {
//				ruleNames += tggRule.getName() + ", ";
//			}
//			System.out.println(" rule list: " + ruleNames);
			
			if (getHelper().getSortedTGGRuleList().isEmpty()) {
				// this should not happen, but it's possible when developing a
				// transformation, that only a tgg with one axiom and no rules
				// is tested. Then we return "null".
				return new DummyTupleGenerator();
			}
			
			
			initialNodeBindingInputArrays[0] = getHelper().getSortedTGGRuleList().toArray();

			// b) list: all source domain objects
			initialNodeBindingInputArrays[1] = getBindingCandidateEObjectList().toArray();

			// c) list: all context TGG rule nodes (which potentially match
			// at existing node bindings)
			// -> Initially set the node list of the first tgg rule handed over
			// -- The rest (replacing the list of nodes) is handled by the
			// InitialNodeBindingGenerator

			initialNodeBindingInputArrays[2] = InitialNodeBindingGenerator.getSortedContextNodeArray(getInterpreter().getConfiguration().getActiveApplicationScenario(), ((TripleGraphGrammarRule) getHelper().getSortedTGGRuleList().get(0))); 

			// condition: a) type has to match b) consider objects that are in
			// front node bindings only
			// when the rule's source pattern contains at least one produced
			// node
			Condition tupleConstraint = new Condition() {

				@Override
				public boolean isSatisfied(Object object) {
					Object[] tuple = (Object[]) object;
					TripleGraphGrammarRule tggRule = (TripleGraphGrammarRule) tuple[0];

					EObject domainObject = (EObject) tuple[1];
					Node ruleNode = (Node) tuple[2];

					// a) check whether type matches
					if (!TransformationProcessorUtil.nodeTypeMatches(ruleNode, domainObject)) 
						return false;

					if (!getInterpreter().getConfiguration().getActiveApplicationScenario().isDeactivateFront()){
						// b) consider the object only when it is in the front
						// AND consider the node only when it has non-context
						// incoming or outgoing edges
						if (getFrontManager().getNodeBinding().keySet().contains(domainObject)) {
							return true;
						} else {
							// when it isn't in the front, only consider it when the
							// rule does not contain any produced nodes in its
							// source domain.
							if (getHelper().hasProducedSourceDomainNodes(tggRule))
								return false;
						}
					}
					return true;
				}
			};

			initialNodeBindingGenerator = new InitialNodeBindingGenerator(initialNodeBindingInputArrays,
					tupleConstraint);

			initialNodeBindingGenerator.setApplicationScenario(getInterpreter().getConfiguration().getActiveApplicationScenario());
		}

		initialNodeBindingGenerator.setArrayOffset(1, bindingCandidateEObjectListIndexOfLastSuccessfulRuleApplication);
		initialNodeBindingGenerator.setArrayOffset(0, tggRuleListIndexOfLastSuccessfulRuleApplication);
		return initialNodeBindingGenerator;
	}

	/**
	 * The initial bindings are generated by building tuples of rule nodes and
	 * objects from the bindingCandidateObjectList. This attribute stores the
	 * index of the TGG rule that the last rule successfully applied; it may be
	 * set to a value other than zero in
	 * {@link #ruleApplicationPostProcessing(RuleBinding)}. The value must not
	 * exceed the size of tgg rules in the current tgg.
	 */
	protected int tggRuleListIndexOfLastSuccessfulRuleApplication = 0;

	/**
	 * The initial bindings are generated by building tuples of rule nodes and
	 * objects from the bindingCandidateObjectList, see
	 * {@link #bindingCandidateEObjectList},
	 * {@link #getBindingCandidateEObjectList()} This attribute stores the index
	 * of the candidate object that was in the initial binding of the last rule
	 * that was successfully applied; it may be set to a value other than zero
	 * in {@link #ruleApplicationPostProcessing(RuleBinding)}. The value must
	 * not exceed the size of {@link #bindingCandidateEObjectList}
	 */
	protected int bindingCandidateEObjectListIndexOfLastSuccessfulRuleApplication = 0;

	/**
	 * The initial bindings are generated by building tuples of rule nodes and
	 * objects from this list. Use {@link #getBindingCandidateEObjectList()} to
	 * access and initialize this list.
	 */
	protected EList<EObject> bindingCandidateEObjectList;
	
	/**
	 * The initial bindings are generated by building tuples of rule nodes and
	 * objects from the bindingCandidateObjectList. This method initializes the
	 * {@link #bindingCandidateEObjectList bindingCandidateEObjectList} if it is
	 * null.
	 * 
	 * @return binding candidate object list, possibly sorted
	 */
	protected EList<EObject> getBindingCandidateEObjectList() {
		if (bindingCandidateEObjectList == null) {
			bindingCandidateEObjectList = getInterpreter().getConfiguration().getActiveApplicationScenario()
					.getSourceDomainObject();
		}
		return bindingCandidateEObjectList;
	}

	/**
	 * The cached value of the '{@link #getCurrentTGGRule() <em>Current TGG Rule</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCurrentTGGRule()
	 * @generated
	 * @ordered
	 */
	protected TripleGraphGrammarRule currentTGGRule;

	/**
	 * The cached value of the '{@link #getUnmatchedSourceDomainObjects()
	 * <em>Unmatched Source Domain Objects</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUnmatchedSourceDomainObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> unmatchedSourceDomainObjects;

	/**
	 * This adds unsuccessfully matched rules unsuccessfullyAppliedRulesList, which is utilized 
	 * during the creation of initial node bindings such that no rules are matched before not all 
	 * non-abstract refining rules were tried
	 */
	@Override
	public void ruleApplicationUnsuccessfulPostProcessing(RuleBinding ruleBinding) {
		super.ruleApplicationUnsuccessfulPostProcessing(ruleBinding);
		// TODO: something else?
	}

	/**
	 * The given ruleBinding is not valid any more due to changes in the model elements covered by
	 * this rule application. Repair has also failed, so it must be revoked by marking its produced 
	 * target graph elements as deleted (actual deletion will be done at the end of the 
	 * transformation). 
	 */
	@Override
	public void ruleRepairUnsuccessful(RuleBinding ruleBinding) {
		// Revoke the rule application by marking its produced target graph elements as deleted
		//    (actual deletion will be done at the end of the transformation).
		
		for (Map.Entry <Node, EObject> nodeBinding: ruleBinding.getNodeToEObjectMap()) {
			if (!nodeBinding.getKey().isLeft())
				getDeletedNodeBindings().put(nodeBinding.getValue(), nodeBinding.getKey());
		}
		for (Map.Entry <Edge, EList<EObject>> edgeBinding: ruleBinding.getEdgeToVirtualModelEdgeMap()) {
			if (!edgeBinding.getKey().isLeft())
				getDeletedEdgeBindings().put(edgeBinding.getValue(), edgeBinding.getKey());
		}
	}	
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void ruleApplicationPostProcessing(RuleBinding ruleBinding) {

		if(logger.isDebugEnabled())
			logger.debug("Starting rule application post processing.");
		
		// copy element bindings from the rule binding to the global bindings
		// copy node bindings to the front manager as well
		for (Entry<Node, EObject> nodeBinding : ruleBinding.getNodeToEObjectMap().entrySet()) {
			if (nodeBinding.getKey() instanceof Node) {
				Node node = (Node) nodeBinding.getKey();
				EObject object = nodeBinding.getValue();
				/*
				 * each object that is not contained in another object or
				 * resource is added to the list of root objects of its
				 * corresponding domain model.
				 */
				if (object.eContainer() == null 
						/* jgreen: I don't know why this condition was in here.
						 * the problem is that with this condition, the root objects
						 * are not assigned to the domain model. This reference is
						 * needed in order to be able to iterate over the models 
						 * for checking the global constraints.
						 * && nodeBinding.getValue().eResource() == null*/) {
					TypedModel typedModel = ((TripleGraphGrammarRule) node.getGraph()).getNodesDomainGraphPattern(node)
							.getTypedModel();
					DomainModel domainModel = getInterpreter().getConfiguration().getDomainModelByTypedModel().get(
							typedModel);

					/* adding root objects. This reference is
					 * needed in order to be able to iterate over the models 
					 * for checking the global constraints */
					domainModel.getRootObject().add(object);

					if (object.eResource() == null && domainModel.getResource().size() > 0)
						domainModel.getResource().get(0).getContents().add(object);

					// add RelevantReferencesCrossReferenceAdapter to new root
					// objects
					object.eAdapters().add(getCrossReferenceAdapter());
				}
				if (getGlobalNodeBinding().get(nodeBinding.getValue()) == null) {
					getGlobalNodeBinding().put(nodeBinding.getValue(), new UniqueEList<Node>());
				}
				getGlobalNodeBinding().get(nodeBinding.getValue()).add(node);

				if (!nodeBinding.getKey().isContext()) {
					int i = getUnmatchedSourceDomainObjects().indexOf(nodeBinding.getValue());
					if (i >= 0) {
						getUnmatchedSourceDomainObjects().remove(i);
						// this measures the transformation progress:
						if (getProgressMonitor() != null)
						getProgressMonitor().worked(1);
					}
				}

				// only if the front is said to be active in the active application scenario
				if (!getInterpreter().getConfiguration().getActiveApplicationScenario().isDeactivateFront()){
					// copy SOURCE node bindings to the front manager
					if (getInterpreter().getConfiguration()
							.getActiveApplicationScenario().getSourceDomainNode().contains(node)){
						if (getFrontManager().getNodeBinding().get(nodeBinding.getValue()) == null){
							getFrontManager().getNodeBinding().put(nodeBinding.getValue(), new UniqueEList<Node>());
						}
						getFrontManager().getNodeBinding().get(nodeBinding.getValue()).add(node);
						
					}
				}
			}
		}

		for (Entry<EList<EObject>, EList<Edge>> edgeBinding : ruleBinding.getEdgeBinding().entrySet()) {
			if (getGlobalEdgeBinding().get(edgeBinding.getKey()) == null){
				getGlobalEdgeBinding().put(edgeBinding.getKey(), edgeBinding.getValue());
			}
		}

		getInterpreter().getConfiguration().getRuleBindingContainer().getRuleBinding().add(ruleBinding);

		if (!getInterpreter().getConfiguration().getActiveApplicationScenario().isDeactivateFront()){
			getFrontManager().update();
		}

		// calculate the
		// positionOfLastSuccessfulRuleApplicationInBindingCandidateEObjectList;
		if (bindingCandidateEObjectList != null) {
			bindingCandidateEObjectListIndexOfLastSuccessfulRuleApplication = bindingCandidateEObjectList
					.indexOf(ruleBinding.getNodeToEObjectMap().get(((Node) ruleBinding.getGraphElementStack().get(0))));
		}

		
		if (helper.getSortedConcreteRuleList().contains(ruleBinding.getTggRule())) { // false if rule is the AXIOM
			tggRuleListIndexOfLastSuccessfulRuleApplication = helper.getAdmissibleStartPosition(helper
					.getSortedConcreteRuleList().indexOf(ruleBinding.getTggRule()));
			if (helper.getSortedConcreteRuleList().indexOf(ruleBinding.getTggRule())
					- tggRuleListIndexOfLastSuccessfulRuleApplication > 3 || !ruleBinding.getTggRule().getRulesAdvisedToApplyNext().isEmpty()) {
				// So far I don't see that doing this speeds up the transformation at all. Therefore there is the
				// constant 3 in the above if condition such that this code is rarely visited. (Only sometimes if there are 
				// generalization hierarchies with > 3 levels of concrete TGG rules.)
				getHelper().sortTGGRuleList(ruleBinding.getTggRule());
			}
		}
		
		//System.out.println(ruleBinding.getTggRule().getName());

		// a new InitialNodeBindingGenerator must be created because the sets of matched/unmatched objects changed.
		resetInitialNodeBindingGenerator();
		
		super.ruleApplicationPostProcessing(ruleBinding);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public IStatus transformationSuccessful() {
		// delete all removed and not reused model objects on the target side
		for (Map.Entry <EObject, Node> nodeBinding: getDeletedNodeBindings().entrySet()) {
			if (nodeBinding.getKey() != null && !getGlobalNodeBinding().containsKey(nodeBinding.getKey())) {
				//delete the object only if it's on the target or correspondence side
				if ((getInterpreter().getConfiguration().getActiveApplicationScenario().getTargetDomainNodes().contains(nodeBinding.getValue()) 
						|| getInterpreter().getConfiguration().getActiveApplicationScenario().getCorrespondenceDomainNode().contains(nodeBinding.getValue()))
					&& (nodeBinding.getKey().eContainer() != null)) { //this could be a user-removed object

					//Call all registered NodeProcessingPolicyies' removeNode()
					for (INodeEnforcementPolicy nodeEnforcementPolicy : getRuleProcessor().getNodeEnforcementPolicy()) {
						nodeEnforcementPolicy.removeNode(nodeBinding.getValue(), nodeBinding.getKey());
					}

					EcoreUtil.remove(nodeBinding.getKey());
				}
			}
		}

		// delete all removed and not reused links on the target side
		for (Map.Entry<EList<EObject>, Edge> edgeBinding : getDeletedEdgeBindings().entrySet()) {
			if (getInterpreter().getConfiguration().getActiveApplicationScenario().getSourceDomainNode().contains(edgeBinding.getValue().getSourceNode()))
				continue; //delete the link only if it's on the target or correspondence side
			if (getGlobalEdgeBinding().containsKey(edgeBinding.getKey()))
				continue; //if this link is in the global edge binding, do not delete it (it is a reusable edge)
			EObject source = edgeBinding.getKey().get(0);
			if (source == null || source.eIsProxy())
				continue; //do not modify unresolvable proxies
			EObject target = edgeBinding.getKey().get(1);
			EReference ref = (EReference) edgeBinding.getKey().get(2);
			
			if (ref.isMany()) {
				Object list = source.eGet(ref);
				((EList<?>) list).remove(target);
			} else {
				source.eUnset(ref);
			}
				
		}
		
		EList<EObject> sourceObjectList = getSourceDomainObjects();
		sourceObjectList.removeAll(getGlobalNodeBinding().keySet());
		if (sourceObjectList.isEmpty()) {
			return new InterpreterStatus(
					InterpreterPlugin.getDefault().getBundle()
							.getSymbolicName(),
					Status.OK,
					"Transformation finished. All source objects have been transformed.",
					getInterpreter().getConfiguration(), null);
		} else {
			InterpreterStatus interpreterStatus = new InterpreterStatus(
					InterpreterPlugin.getDefault().getBundle()
							.getSymbolicName(),
					Status.INFO,
					"Transformation finished. Not all elements in the source model were transformed. "
					+ "(See the list of unbound objects)",
					getInterpreter().getConfiguration(), null);
			interpreterStatus.setUnboundSourceObjects(sourceObjectList);
			return interpreterStatus;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.FRONT_TRANSFORMATION_PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FrontManager getFrontManager() {
		return frontManager;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrontManager(FrontManager newFrontManager, NotificationChain msgs) {
		FrontManager oldFrontManager = frontManager;
		frontManager = newFrontManager;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER, oldFrontManager, newFrontManager);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrontManager(FrontManager newFrontManager) {
		if (newFrontManager != frontManager) {
			NotificationChain msgs = null;
			if (frontManager != null)
				msgs = ((InternalEObject)frontManager).eInverseRemove(this, InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR, FrontManager.class, msgs);
			if (newFrontManager != null)
				msgs = ((InternalEObject)newFrontManager).eInverseAdd(this, InterpreterPackage.FRONT_MANAGER__FRONT_TRANSFORMATION_PROCESSOR, FrontManager.class, msgs);
			msgs = basicSetFrontManager(newFrontManager, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER, newFrontManager, newFrontManager));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<EObject> getSourceDomainObjects() {
		if (sourceDomainObjects == null) {
			sourceDomainObjects = new UniqueEList<EObject>();

			EList<Node> nodeList = new UniqueEList<Node>();
			for (Rule tggRule : getInterpreter().getConfiguration().getTripleGraphGrammar().getAllRules()) {
				for (DomainModel sourceDomainModel : getInterpreter().getConfiguration().getActiveApplicationScenario()
						.getSourceDomainModel()) {
					nodeList.addAll(((TripleGraphGrammarRule) tggRule).getDomainsNodesMap().get(
							sourceDomainModel.getTypedModel()));
				}
			}
			sourceDomainObjects = TransformationProcessorUtil.getSourceDomainObjectsMatchableByNodes(nodeList,
					getInterpreter().getConfiguration().getActiveApplicationScenario());
		}
		return sourceDomainObjects;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				if (frontManager != null)
					msgs = ((InternalEObject)frontManager).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER, null, msgs);
				return basicSetFrontManager((FrontManager)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				return basicSetFrontManager(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				return getFrontManager();
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__SOURCE_DOMAIN_OBJECTS:
				return getSourceDomainObjects();
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE:
				if (resolve) return getCurrentTGGRule();
				return basicGetCurrentTGGRule();
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__UNMATCHED_SOURCE_DOMAIN_OBJECTS:
				return getUnmatchedSourceDomainObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				setFrontManager((FrontManager)newValue);
				return;
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE:
				setCurrentTGGRule((TripleGraphGrammarRule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				setFrontManager((FrontManager)null);
				return;
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE:
				setCurrentTGGRule((TripleGraphGrammarRule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__FRONT_MANAGER:
				return frontManager != null;
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__SOURCE_DOMAIN_OBJECTS:
				return sourceDomainObjects != null && !sourceDomainObjects.isEmpty();
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__CURRENT_TGG_RULE:
				return currentTGGRule != null;
			case InterpreterPackage.FRONT_TRANSFORMATION_PROCESSOR__UNMATCHED_SOURCE_DOMAIN_OBJECTS:
				return unmatchedSourceDomainObjects != null && !unmatchedSourceDomainObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public IStatus performTransformation(IProgressMonitor monitor) {
		//If the transformation direction changed since the last run,
		// we need to re-populate the binding candidate list.
		resetInitialNodeBindingGenerator();
		bindingCandidateEObjectList = null;
		return super.performTransformation(monitor);
	}

} // FrontTransformationProcessorImpl
