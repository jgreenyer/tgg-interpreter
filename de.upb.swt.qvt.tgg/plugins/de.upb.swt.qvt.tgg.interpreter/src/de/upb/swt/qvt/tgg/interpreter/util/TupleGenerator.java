package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.query.conditions.Condition;

public class TupleGenerator {

	/**
	 * Temporarily holds the value of the next tuple in case that hasNext() was
	 * called.
	 */
	private Object[] nextTuple;
	private boolean hasNextCalled = false;
	private boolean tupleLeftToTraverse = true;
	protected int[] arrayElementPointer;

	protected Object[][] inputArrays;

	private Condition tupleConstraint;
	
	protected int[] arrayOffset;
	
	
	public TupleGenerator(Object[][] inputArrays, Condition tupleConstraint) {
		this(inputArrays);
		this.tupleConstraint = tupleConstraint;
	}

	public TupleGenerator(Object[][] inputArrays) {
		this.inputArrays = inputArrays;
		arrayOffset = new int[inputArrays.length];
		arrayElementPointer = new int[inputArrays.length];
	}
	
	public Object[] getNextTuple() {
		// the next tuple may have already been calculated when the hasNext()
		// method was called
		if (hasNextCalled) {
			// then, return that tuple and set hasNextCalled to false.
			hasNextCalled = false;
			return nextTuple;
		}
		// it may be that at a previous call to this method, we already
		// determined that there are
		// no tuples left to be traversed. Then, return an empty list.
		if (!tupleLeftToTraverse)
			return new Object[0];
		
		// initialize the result array with a new array. The length is the
		// number of input arrays.
		Object[] resultArray = new Object[inputArrays.length];
		
		boolean returnListValid = false;
		/*
		 * because some results will not satisfy the tuple constraints, we
		 * iterate in this loop as long as we find a result satisfying the
		 * constraint (the we return this tuple) or until there are no tuples
		 * left to traverse (then, we return an empty list).
		 */
		do {
			// resultArray.clear();
			for (int i = 0; i < inputArrays.length; i++) {
				if (inputArrays.length <= i || inputArrays[i].length <= arrayElementPointer[i]) {
					resultArray[i] = EcoreFactory.eINSTANCE.createEObject();
				} else {
					resultArray[i] = getArrayElement(i, arrayElementPointer[i]);
				}
			}
			// determine whether there is another list that we can
			tupleLeftToTraverse = increaseInputArrayPointer();
			returnListValid = isTupleValid(resultArray);
			if (returnListValid) {
				return resultArray;
			}
		} while (!returnListValid && tupleLeftToTraverse);

		return new Object[0];
	}
	
	protected Object getArrayElement(int inputArrayIndex, int position){
		if (arrayOffset[inputArrayIndex] != 0){
			int shiftedIndex = position+arrayOffset[inputArrayIndex];
			if (shiftedIndex < inputArrays[inputArrayIndex].length){
				return inputArrays[inputArrayIndex][shiftedIndex];
			}
			else
				return inputArrays[inputArrayIndex][shiftedIndex-inputArrays[inputArrayIndex].length];

		}else
			return inputArrays[inputArrayIndex][position];
	}
	
	public void setArrayOffset(int inputArrayIndex, int offset){
		if (offset >= inputArrays[inputArrayIndex].length)
			throw new IndexOutOfBoundsException("The offset must not be greater than the size of the input list at this position.");
		arrayOffset[inputArrayIndex] = offset;
	}

	private boolean isTupleValid(Object[] tuple) {
		return (tupleConstraint == null || tupleConstraint.isSatisfied(tuple));
	}

	protected void replaceInputArray(int position, Object[] inputArray) {
		inputArrays[position] = inputArray;
		for (int i = position; i < arrayElementPointer.length; i++) {
			arrayElementPointer[i] = 0;
		}
		hasNextCalled = false;
	}

	public boolean hasNext() {
		if (hasNextCalled) {
			return nextTuple.length != 0;
		} else {
			nextTuple = getNextTuple();
			hasNextCalled = true;
			return nextTuple.length != 0;
		}
	}

	protected boolean increaseInputArrayPointer() {
		int inputListPointer = arrayElementPointer.length - 1;
		boolean increaseInputListPointer = true;
		while (increaseInputListPointer) {
			arrayElementPointer[inputListPointer]++;
			increaseInputListPointer = false;
			if (arrayElementPointer[inputListPointer] >= inputArrays[inputListPointer].length) {
				arrayElementPointer[inputListPointer] = 0;
				increaseInputListPointer = true;
				inputListPointer--;
				if (inputListPointer < 0)
					return false;
			}
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inputLists: ");
		result.append(inputArrays);
		result.append(", tupleConstraint: ");
		result.append(tupleConstraint);
		result.append(')');
		return result.toString();
	}
}
