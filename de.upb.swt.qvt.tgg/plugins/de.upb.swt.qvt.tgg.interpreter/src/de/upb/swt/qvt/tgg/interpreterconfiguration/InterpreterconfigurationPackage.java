/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface InterpreterconfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "interpreterconfiguration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.upb.swt.qvt.tgg.interpreter.interpreterconfiguration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.upb.swt.qvt.tgg.interpreter.interpreterconfiguration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InterpreterconfigurationPackage eINSTANCE = de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Triple Graph Grammar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__TRIPLE_GRAPH_GRAMMAR = 0;

	/**
	 * The feature id for the '<em><b>Record Rule Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__RECORD_RULE_BINDINGS = 1;

	/**
	 * The feature id for the '<em><b>Application Scenario</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__APPLICATION_SCENARIO = 2;

	/**
	 * The feature id for the '<em><b>Domain Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__DOMAIN_MODEL = 3;

	/**
	 * The feature id for the '<em><b>Rule Binding Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__RULE_BINDING_CONTAINER = 4;

	/**
	 * The feature id for the '<em><b>Active Application Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__ACTIVE_APPLICATION_SCENARIO = 5;

	/**
	 * The feature id for the '<em><b>Domain Model By Typed Model</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL = 6;

	/**
	 * The feature id for the '<em><b>Domain Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__DOMAIN_ROOT_OBJECTS = 7;

	/**
	 * The feature id for the '<em><b>No Package Uri Redirection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NO_PACKAGE_URI_REDIRECTION = 8;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl <em>Domain Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getDomainModel()
	 * @generated
	 */
	int DOMAIN_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Typed Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__TYPED_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Root Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__ROOT_OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__NAME = 2;

	/**
	 * The feature id for the '<em><b>Resource URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__RESOURCE_URI = 3;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__RESOURCE = 4;

	/**
	 * The feature id for the '<em><b>Domain Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__DOMAIN_OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Show Domain Model Diagnostic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC = 6;

	/**
	 * The number of structural features of the '<em>Domain Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_MODEL_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl <em>EObject To Node Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getEObjectToNodeMapEntry()
	 * @generated
	 */
	int EOBJECT_TO_NODE_MAP_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_NODE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_NODE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EObject To Node Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_NODE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.VirtualModelEdgeToEdgeMapEntryImpl <em>Virtual Model Edge To Edge Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.VirtualModelEdgeToEdgeMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getVirtualModelEdgeToEdgeMapEntry()
	 * @generated
	 */
	int VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Virtual Model Edge To Edge Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl <em>Rule Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getRuleBinding()
	 * @generated
	 */
	int RULE_BINDING = 4;

	/**
	 * The feature id for the '<em><b>Node To EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__NODE_TO_EOBJECT_MAP = 0;

	/**
	 * The feature id for the '<em><b>Edge Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__EDGE_BINDING = 1;

	/**
	 * The feature id for the '<em><b>Node Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__NODE_BINDING = 2;

	/**
	 * The feature id for the '<em><b>Rule Binding Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__RULE_BINDING_CONTAINER = 3;

	/**
	 * The feature id for the '<em><b>Edge To Virtual Model Edge Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP = 4;

	/**
	 * The feature id for the '<em><b>Graph Element Stack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__GRAPH_ELEMENT_STACK = 5;

	/**
	 * The feature id for the '<em><b>Tgg Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING__TGG_RULE = 6;

	/**
	 * The number of structural features of the '<em>Rule Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl <em>Application Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getApplicationScenario()
	 * @generated
	 */
	int APPLICATION_SCENARIO = 5;

	/**
	 * The feature id for the '<em><b>Source Domain Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Target Domain Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Correspondence Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__CORRESPONDENCE_MODEL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__NAME = 3;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__MODE = 4;

	/**
	 * The feature id for the '<em><b>Source Domain Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Correspondence Domain Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT = 6;

	/**
	 * The feature id for the '<em><b>Target Domain Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT = 7;

	/**
	 * The feature id for the '<em><b>Source Domain Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE = 8;

	/**
	 * The feature id for the '<em><b>Correspondence Domain Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE = 9;

	/**
	 * The feature id for the '<em><b>Target Domain Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__TARGET_DOMAIN_NODES = 10;

	/**
	 * The feature id for the '<em><b>Source Domain Nodes By TGG Rule</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE = 11;

	/**
	 * The feature id for the '<em><b>Correspondence Domain Nodes By TGG Rule</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE = 12;

	/**
	 * The feature id for the '<em><b>Target Domain Node By TGG Rule</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE = 13;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__CONFIGURATION = 14;

	/**
	 * The feature id for the '<em><b>Deactivate Front</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO__DEACTIVATE_FRONT = 15;

	/**
	 * The number of structural features of the '<em>Application Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_SCENARIO_FEATURE_COUNT = 16;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl <em>Rule Binding Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getRuleBindingContainer()
	 * @generated
	 */
	int RULE_BINDING_CONTAINER = 6;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING_CONTAINER__RULE_BINDING = 0;

	/**
	 * The feature id for the '<em><b>Initial Axiom Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING = 1;

	/**
	 * The number of structural features of the '<em>Rule Binding Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_BINDING_CONTAINER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TypedModelToDomainModelMapEntryImpl <em>Typed Model To Domain Model Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TypedModelToDomainModelMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getTypedModelToDomainModelMapEntry()
	 * @generated
	 */
	int TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY = 7;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Typed Model To Domain Model Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TGGRuleToNodesMapEntryImpl <em>TGG Rule To Nodes Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TGGRuleToNodesMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getTGGRuleToNodesMapEntry()
	 * @generated
	 */
	int TGG_RULE_TO_NODES_MAP_ENTRY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_TO_NODES_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_TO_NODES_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>TGG Rule To Nodes Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_TO_NODES_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.NodeToEObjectMapEntryImpl <em>Node To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.NodeToEObjectMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getNodeToEObjectMapEntry()
	 * @generated
	 */
	int NODE_TO_EOBJECT_MAP_ENTRY = 9;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Node To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EdgeToVirtualModelEdgeMapEntryImpl <em>Edge To Virtual Model Edge Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EdgeToVirtualModelEdgeMapEntryImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getEdgeToVirtualModelEdgeMapEntry()
	 * @generated
	 */
	int EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY = 10;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Edge To Virtual Model Edge Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.AxiomBindingImpl <em>Axiom Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.AxiomBindingImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getAxiomBinding()
	 * @generated
	 */
	int AXIOM_BINDING = 11;

	/**
	 * The feature id for the '<em><b>Node To EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__NODE_TO_EOBJECT_MAP = RULE_BINDING__NODE_TO_EOBJECT_MAP;

	/**
	 * The feature id for the '<em><b>Edge Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__EDGE_BINDING = RULE_BINDING__EDGE_BINDING;

	/**
	 * The feature id for the '<em><b>Node Binding</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__NODE_BINDING = RULE_BINDING__NODE_BINDING;

	/**
	 * The feature id for the '<em><b>Rule Binding Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__RULE_BINDING_CONTAINER = RULE_BINDING__RULE_BINDING_CONTAINER;

	/**
	 * The feature id for the '<em><b>Edge To Virtual Model Edge Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP = RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP;

	/**
	 * The feature id for the '<em><b>Graph Element Stack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__GRAPH_ELEMENT_STACK = RULE_BINDING__GRAPH_ELEMENT_STACK;

	/**
	 * The feature id for the '<em><b>Tgg Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__TGG_RULE = RULE_BINDING__TGG_RULE;

	/**
	 * The feature id for the '<em><b>Initial Axiom Node Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING = RULE_BINDING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Axiom Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_BINDING_FEATURE_COUNT = RULE_BINDING_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InitialAxiomNodeBindingImpl <em>Initial Axiom Node Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InitialAxiomNodeBindingImpl
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getInitialAxiomNodeBinding()
	 * @generated
	 */
	int INITIAL_AXIOM_NODE_BINDING = 12;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_AXIOM_NODE_BINDING__EOBJECT = 0;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_AXIOM_NODE_BINDING__NODE = 1;

	/**
	 * The number of structural features of the '<em>Initial Axiom Node Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_AXIOM_NODE_BINDING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode <em>Application Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getApplicationMode()
	 * @generated
	 */
	int APPLICATION_MODE = 13;


	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Severity <em>Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Severity
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getSeverity()
	 * @generated
	 */
	int SEVERITY = 14;


	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getTripleGraphGrammar <em>Triple Graph Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Triple Graph Grammar</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getTripleGraphGrammar()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_TripleGraphGrammar();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isRecordRuleBindings <em>Record Rule Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Record Rule Bindings</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isRecordRuleBindings()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_RecordRuleBindings();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getApplicationScenario <em>Application Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Application Scenario</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getApplicationScenario()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ApplicationScenario();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModel <em>Domain Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Domain Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModel()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_DomainModel();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getRuleBindingContainer <em>Rule Binding Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rule Binding Container</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getRuleBindingContainer()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_RuleBindingContainer();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getActiveApplicationScenario <em>Active Application Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Application Scenario</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getActiveApplicationScenario()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ActiveApplicationScenario();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainRootObjects <em>Domain Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Domain Root Objects</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainRootObjects()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_DomainRootObjects();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isNoPackageUriRedirection <em>No Package Uri Redirection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Package Uri Redirection</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#isNoPackageUriRedirection()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_NoPackageUriRedirection();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModelByTypedModel <em>Domain Model By Typed Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Domain Model By Typed Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration#getDomainModelByTypedModel()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_DomainModelByTypedModel();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel <em>Domain Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Domain Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel
	 * @generated
	 */
	EClass getDomainModel();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getTypedModel <em>Typed Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Typed Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getTypedModel()
	 * @see #getDomainModel()
	 * @generated
	 */
	EReference getDomainModel_TypedModel();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getRootObject <em>Root Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Root Object</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getRootObject()
	 * @see #getDomainModel()
	 * @generated
	 */
	EReference getDomainModel_RootObject();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getName()
	 * @see #getDomainModel()
	 * @generated
	 */
	EAttribute getDomainModel_Name();

	/**
	 * Returns the meta object for the attribute list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResourceURI <em>Resource URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Resource URI</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResourceURI()
	 * @see #getDomainModel()
	 * @generated
	 */
	EAttribute getDomainModel_ResourceURI();

	/**
	 * Returns the meta object for the attribute list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Resource</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResource()
	 * @see #getDomainModel()
	 * @generated
	 */
	EAttribute getDomainModel_Resource();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getDomainObject <em>Domain Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Domain Object</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getDomainObject()
	 * @see #getDomainModel()
	 * @generated
	 */
	EReference getDomainModel_DomainObject();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getShowDomainModelDiagnostic <em>Show Domain Model Diagnostic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Domain Model Diagnostic</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getShowDomainModelDiagnostic()
	 * @see #getDomainModel()
	 * @generated
	 */
	EAttribute getDomainModel_ShowDomainModelDiagnostic();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EObject To Node Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject To Node Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
	 *        valueType="de.upb.swt.qvt.tgg.Node" valueMany="true"
	 * @generated
	 */
	EClass getEObjectToNodeMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToNodeMapEntry()
	 * @generated
	 */
	EReference getEObjectToNodeMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToNodeMapEntry()
	 * @generated
	 */
	EReference getEObjectToNodeMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Virtual Model Edge To Edge Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Virtual Model Edge To Edge Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="de.upb.swt.qvt.tgg.Edge" valueMany="true"
	 *        keyDataType="org.eclipse.emf.ecore.EEList<org.eclipse.emf.ecore.EObject>" keyRequired="true" keyMany="false" keyTransient="true"
	 * @generated
	 */
	EClass getVirtualModelEdgeToEdgeMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVirtualModelEdgeToEdgeMapEntry()
	 * @generated
	 */
	EAttribute getVirtualModelEdgeToEdgeMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVirtualModelEdgeToEdgeMapEntry()
	 * @generated
	 */
	EReference getVirtualModelEdgeToEdgeMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding <em>Rule Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding
	 * @generated
	 */
	EClass getRuleBinding();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeToEObjectMap <em>Node To EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Node To EObject Map</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeToEObjectMap()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_NodeToEObjectMap();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeBinding <em>Edge Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Edge Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeBinding()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_EdgeBinding();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeBinding <em>Node Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Node Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getNodeBinding()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_NodeBinding();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer <em>Rule Binding Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Rule Binding Container</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getRuleBindingContainer()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_RuleBindingContainer();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeToVirtualModelEdgeMap <em>Edge To Virtual Model Edge Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Edge To Virtual Model Edge Map</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getEdgeToVirtualModelEdgeMap()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_EdgeToVirtualModelEdgeMap();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getGraphElementStack <em>Graph Element Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graph Element Stack</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getGraphElementStack()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_GraphElementStack();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getTggRule <em>Tgg Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgg Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding#getTggRule()
	 * @see #getRuleBinding()
	 * @generated
	 */
	EReference getRuleBinding_TggRule();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario <em>Application Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application Scenario</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario
	 * @generated
	 */
	EClass getApplicationScenario();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainModel <em>Source Domain Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source Domain Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainModel()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_SourceDomainModel();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainModel <em>Target Domain Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target Domain Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainModel()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_TargetDomainModel();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceModel <em>Correspondence Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Correspondence Model</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceModel()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_CorrespondenceModel();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getName()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EAttribute getApplicationScenario_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getMode()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EAttribute getApplicationScenario_Mode();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainObject <em>Source Domain Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source Domain Object</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainObject()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_SourceDomainObject();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainObject <em>Correspondence Domain Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Correspondence Domain Object</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainObject()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_CorrespondenceDomainObject();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainObject <em>Target Domain Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target Domain Object</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainObject()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_TargetDomainObject();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNode <em>Source Domain Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source Domain Node</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNode()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_SourceDomainNode();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNode <em>Correspondence Domain Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Correspondence Domain Node</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNode()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_CorrespondenceDomainNode();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodes <em>Target Domain Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target Domain Nodes</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodes()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_TargetDomainNodes();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNodesByTGGRule <em>Source Domain Nodes By TGG Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Source Domain Nodes By TGG Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getSourceDomainNodesByTGGRule()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_SourceDomainNodesByTGGRule();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNodesByTGGRule <em>Correspondence Domain Nodes By TGG Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Correspondence Domain Nodes By TGG Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getCorrespondenceDomainNodesByTGGRule()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_CorrespondenceDomainNodesByTGGRule();

	/**
	 * Returns the meta object for the map '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodeByTGGRule <em>Target Domain Node By TGG Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Target Domain Node By TGG Rule</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getTargetDomainNodeByTGGRule()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_TargetDomainNodeByTGGRule();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Configuration</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#getConfiguration()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EReference getApplicationScenario_Configuration();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#isDeactivateFront <em>Deactivate Front</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deactivate Front</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario#isDeactivateFront()
	 * @see #getApplicationScenario()
	 * @generated
	 */
	EAttribute getApplicationScenario_DeactivateFront();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer <em>Rule Binding Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Binding Container</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer
	 * @generated
	 */
	EClass getRuleBindingContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getRuleBinding <em>Rule Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getRuleBinding()
	 * @see #getRuleBindingContainer()
	 * @generated
	 */
	EReference getRuleBindingContainer_RuleBinding();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getInitialAxiomBinding <em>Initial Axiom Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial Axiom Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer#getInitialAxiomBinding()
	 * @see #getRuleBindingContainer()
	 * @generated
	 */
	EReference getRuleBindingContainer_InitialAxiomBinding();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Typed Model To Domain Model Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Model To Domain Model Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="de.upb.swt.qvt.qvtbase.TypedModel" keyRequired="true"
	 *        valueType="de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel" valueRequired="true"
	 * @generated
	 */
	EClass getTypedModelToDomainModelMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTypedModelToDomainModelMapEntry()
	 * @generated
	 */
	EReference getTypedModelToDomainModelMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTypedModelToDomainModelMapEntry()
	 * @generated
	 */
	EReference getTypedModelToDomainModelMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>TGG Rule To Nodes Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Rule To Nodes Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="de.upb.swt.qvt.tgg.TripleGraphGrammarRule" keyRequired="true"
	 *        valueType="de.upb.swt.qvt.tgg.Node" valueMany="true"
	 * @generated
	 */
	EClass getTGGRuleToNodesMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTGGRuleToNodesMapEntry()
	 * @generated
	 */
	EReference getTGGRuleToNodesMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTGGRuleToNodesMapEntry()
	 * @generated
	 */
	EReference getTGGRuleToNodesMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Node To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="de.upb.swt.qvt.tgg.Node" keyRequired="true"
	 *        valueType="org.eclipse.emf.ecore.EObject" valueRequired="true"
	 * @generated
	 */
	EClass getNodeToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNodeToEObjectMapEntry()
	 * @generated
	 */
	EReference getNodeToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNodeToEObjectMapEntry()
	 * @generated
	 */
	EReference getNodeToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Edge To Virtual Model Edge Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge To Virtual Model Edge Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="de.upb.swt.qvt.tgg.Edge" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EEList<org.eclipse.emf.ecore.EObject>" valueRequired="true" valueMany="false" valueTransient="true"
	 * @generated
	 */
	EClass getEdgeToVirtualModelEdgeMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEdgeToVirtualModelEdgeMapEntry()
	 * @generated
	 */
	EReference getEdgeToVirtualModelEdgeMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEdgeToVirtualModelEdgeMapEntry()
	 * @generated
	 */
	EAttribute getEdgeToVirtualModelEdgeMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding <em>Axiom Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Axiom Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding
	 * @generated
	 */
	EClass getAxiomBinding();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding#getInitialAxiomNodeBinding <em>Initial Axiom Node Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Initial Axiom Node Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding#getInitialAxiomNodeBinding()
	 * @see #getAxiomBinding()
	 * @generated
	 */
	EReference getAxiomBinding_InitialAxiomNodeBinding();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding <em>Initial Axiom Node Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial Axiom Node Binding</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding
	 * @generated
	 */
	EClass getInitialAxiomNodeBinding();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding#getEObject <em>EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding#getEObject()
	 * @see #getInitialAxiomNodeBinding()
	 * @generated
	 */
	EReference getInitialAxiomNodeBinding_EObject();

	/**
	 * Returns the meta object for the reference '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Node</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding#getNode()
	 * @see #getInitialAxiomNodeBinding()
	 * @generated
	 */
	EReference getInitialAxiomNodeBinding_Node();

	/**
	 * Returns the meta object for enum '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode <em>Application Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Application Mode</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode
	 * @generated
	 */
	EEnum getApplicationMode();

	/**
	 * Returns the meta object for enum '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Severity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Severity</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Severity
	 * @generated
	 */
	EEnum getSeverity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InterpreterconfigurationFactory getInterpreterconfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ConfigurationImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Triple Graph Grammar</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__TRIPLE_GRAPH_GRAMMAR = eINSTANCE.getConfiguration_TripleGraphGrammar();

		/**
		 * The meta object literal for the '<em><b>Record Rule Bindings</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__RECORD_RULE_BINDINGS = eINSTANCE.getConfiguration_RecordRuleBindings();

		/**
		 * The meta object literal for the '<em><b>Application Scenario</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__APPLICATION_SCENARIO = eINSTANCE.getConfiguration_ApplicationScenario();

		/**
		 * The meta object literal for the '<em><b>Domain Model</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__DOMAIN_MODEL = eINSTANCE.getConfiguration_DomainModel();

		/**
		 * The meta object literal for the '<em><b>Rule Binding Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__RULE_BINDING_CONTAINER = eINSTANCE.getConfiguration_RuleBindingContainer();

		/**
		 * The meta object literal for the '<em><b>Active Application Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__ACTIVE_APPLICATION_SCENARIO = eINSTANCE.getConfiguration_ActiveApplicationScenario();

		/**
		 * The meta object literal for the '<em><b>Domain Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__DOMAIN_ROOT_OBJECTS = eINSTANCE.getConfiguration_DomainRootObjects();

		/**
		 * The meta object literal for the '<em><b>No Package Uri Redirection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__NO_PACKAGE_URI_REDIRECTION = eINSTANCE.getConfiguration_NoPackageUriRedirection();

		/**
		 * The meta object literal for the '<em><b>Domain Model By Typed Model</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL = eINSTANCE.getConfiguration_DomainModelByTypedModel();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl <em>Domain Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.DomainModelImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getDomainModel()
		 * @generated
		 */
		EClass DOMAIN_MODEL = eINSTANCE.getDomainModel();

		/**
		 * The meta object literal for the '<em><b>Typed Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOMAIN_MODEL__TYPED_MODEL = eINSTANCE.getDomainModel_TypedModel();

		/**
		 * The meta object literal for the '<em><b>Root Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOMAIN_MODEL__ROOT_OBJECT = eINSTANCE.getDomainModel_RootObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOMAIN_MODEL__NAME = eINSTANCE.getDomainModel_Name();

		/**
		 * The meta object literal for the '<em><b>Resource URI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOMAIN_MODEL__RESOURCE_URI = eINSTANCE.getDomainModel_ResourceURI();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOMAIN_MODEL__RESOURCE = eINSTANCE.getDomainModel_Resource();

		/**
		 * The meta object literal for the '<em><b>Domain Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOMAIN_MODEL__DOMAIN_OBJECT = eINSTANCE.getDomainModel_DomainObject();

		/**
		 * The meta object literal for the '<em><b>Show Domain Model Diagnostic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOMAIN_MODEL__SHOW_DOMAIN_MODEL_DIAGNOSTIC = eINSTANCE.getDomainModel_ShowDomainModelDiagnostic();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl <em>EObject To Node Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getEObjectToNodeMapEntry()
		 * @generated
		 */
		EClass EOBJECT_TO_NODE_MAP_ENTRY = eINSTANCE.getEObjectToNodeMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_NODE_MAP_ENTRY__KEY = eINSTANCE.getEObjectToNodeMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_NODE_MAP_ENTRY__VALUE = eINSTANCE.getEObjectToNodeMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.VirtualModelEdgeToEdgeMapEntryImpl <em>Virtual Model Edge To Edge Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.VirtualModelEdgeToEdgeMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getVirtualModelEdgeToEdgeMapEntry()
		 * @generated
		 */
		EClass VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY = eINSTANCE.getVirtualModelEdgeToEdgeMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__KEY = eINSTANCE.getVirtualModelEdgeToEdgeMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY__VALUE = eINSTANCE.getVirtualModelEdgeToEdgeMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl <em>Rule Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getRuleBinding()
		 * @generated
		 */
		EClass RULE_BINDING = eINSTANCE.getRuleBinding();

		/**
		 * The meta object literal for the '<em><b>Node To EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__NODE_TO_EOBJECT_MAP = eINSTANCE.getRuleBinding_NodeToEObjectMap();

		/**
		 * The meta object literal for the '<em><b>Edge Binding</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__EDGE_BINDING = eINSTANCE.getRuleBinding_EdgeBinding();

		/**
		 * The meta object literal for the '<em><b>Node Binding</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__NODE_BINDING = eINSTANCE.getRuleBinding_NodeBinding();

		/**
		 * The meta object literal for the '<em><b>Rule Binding Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__RULE_BINDING_CONTAINER = eINSTANCE.getRuleBinding_RuleBindingContainer();

		/**
		 * The meta object literal for the '<em><b>Edge To Virtual Model Edge Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__EDGE_TO_VIRTUAL_MODEL_EDGE_MAP = eINSTANCE.getRuleBinding_EdgeToVirtualModelEdgeMap();

		/**
		 * The meta object literal for the '<em><b>Graph Element Stack</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__GRAPH_ELEMENT_STACK = eINSTANCE.getRuleBinding_GraphElementStack();

		/**
		 * The meta object literal for the '<em><b>Tgg Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING__TGG_RULE = eINSTANCE.getRuleBinding_TggRule();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl <em>Application Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.ApplicationScenarioImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getApplicationScenario()
		 * @generated
		 */
		EClass APPLICATION_SCENARIO = eINSTANCE.getApplicationScenario();

		/**
		 * The meta object literal for the '<em><b>Source Domain Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL = eINSTANCE.getApplicationScenario_SourceDomainModel();

		/**
		 * The meta object literal for the '<em><b>Target Domain Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL = eINSTANCE.getApplicationScenario_TargetDomainModel();

		/**
		 * The meta object literal for the '<em><b>Correspondence Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__CORRESPONDENCE_MODEL = eINSTANCE.getApplicationScenario_CorrespondenceModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION_SCENARIO__NAME = eINSTANCE.getApplicationScenario_Name();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION_SCENARIO__MODE = eINSTANCE.getApplicationScenario_Mode();

		/**
		 * The meta object literal for the '<em><b>Source Domain Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT = eINSTANCE.getApplicationScenario_SourceDomainObject();

		/**
		 * The meta object literal for the '<em><b>Correspondence Domain Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT = eINSTANCE.getApplicationScenario_CorrespondenceDomainObject();

		/**
		 * The meta object literal for the '<em><b>Target Domain Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT = eINSTANCE.getApplicationScenario_TargetDomainObject();

		/**
		 * The meta object literal for the '<em><b>Source Domain Node</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE = eINSTANCE.getApplicationScenario_SourceDomainNode();

		/**
		 * The meta object literal for the '<em><b>Correspondence Domain Node</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE = eINSTANCE.getApplicationScenario_CorrespondenceDomainNode();

		/**
		 * The meta object literal for the '<em><b>Target Domain Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__TARGET_DOMAIN_NODES = eINSTANCE.getApplicationScenario_TargetDomainNodes();

		/**
		 * The meta object literal for the '<em><b>Source Domain Nodes By TGG Rule</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE = eINSTANCE.getApplicationScenario_SourceDomainNodesByTGGRule();

		/**
		 * The meta object literal for the '<em><b>Correspondence Domain Nodes By TGG Rule</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE = eINSTANCE.getApplicationScenario_CorrespondenceDomainNodesByTGGRule();

		/**
		 * The meta object literal for the '<em><b>Target Domain Node By TGG Rule</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE = eINSTANCE.getApplicationScenario_TargetDomainNodeByTGGRule();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_SCENARIO__CONFIGURATION = eINSTANCE.getApplicationScenario_Configuration();

		/**
		 * The meta object literal for the '<em><b>Deactivate Front</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION_SCENARIO__DEACTIVATE_FRONT = eINSTANCE.getApplicationScenario_DeactivateFront();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl <em>Rule Binding Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.RuleBindingContainerImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getRuleBindingContainer()
		 * @generated
		 */
		EClass RULE_BINDING_CONTAINER = eINSTANCE.getRuleBindingContainer();

		/**
		 * The meta object literal for the '<em><b>Rule Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING_CONTAINER__RULE_BINDING = eINSTANCE.getRuleBindingContainer_RuleBinding();

		/**
		 * The meta object literal for the '<em><b>Initial Axiom Binding</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_BINDING_CONTAINER__INITIAL_AXIOM_BINDING = eINSTANCE.getRuleBindingContainer_InitialAxiomBinding();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TypedModelToDomainModelMapEntryImpl <em>Typed Model To Domain Model Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TypedModelToDomainModelMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getTypedModelToDomainModelMapEntry()
		 * @generated
		 */
		EClass TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY = eINSTANCE.getTypedModelToDomainModelMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__KEY = eINSTANCE.getTypedModelToDomainModelMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY__VALUE = eINSTANCE.getTypedModelToDomainModelMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TGGRuleToNodesMapEntryImpl <em>TGG Rule To Nodes Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.TGGRuleToNodesMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getTGGRuleToNodesMapEntry()
		 * @generated
		 */
		EClass TGG_RULE_TO_NODES_MAP_ENTRY = eINSTANCE.getTGGRuleToNodesMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_TO_NODES_MAP_ENTRY__KEY = eINSTANCE.getTGGRuleToNodesMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_TO_NODES_MAP_ENTRY__VALUE = eINSTANCE.getTGGRuleToNodesMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.NodeToEObjectMapEntryImpl <em>Node To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.NodeToEObjectMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getNodeToEObjectMapEntry()
		 * @generated
		 */
		EClass NODE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getNodeToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getNodeToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getNodeToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EdgeToVirtualModelEdgeMapEntryImpl <em>Edge To Virtual Model Edge Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EdgeToVirtualModelEdgeMapEntryImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getEdgeToVirtualModelEdgeMapEntry()
		 * @generated
		 */
		EClass EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY = eINSTANCE.getEdgeToVirtualModelEdgeMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__KEY = eINSTANCE.getEdgeToVirtualModelEdgeMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY__VALUE = eINSTANCE.getEdgeToVirtualModelEdgeMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.AxiomBindingImpl <em>Axiom Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.AxiomBindingImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getAxiomBinding()
		 * @generated
		 */
		EClass AXIOM_BINDING = eINSTANCE.getAxiomBinding();

		/**
		 * The meta object literal for the '<em><b>Initial Axiom Node Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING = eINSTANCE.getAxiomBinding_InitialAxiomNodeBinding();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InitialAxiomNodeBindingImpl <em>Initial Axiom Node Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InitialAxiomNodeBindingImpl
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getInitialAxiomNodeBinding()
		 * @generated
		 */
		EClass INITIAL_AXIOM_NODE_BINDING = eINSTANCE.getInitialAxiomNodeBinding();

		/**
		 * The meta object literal for the '<em><b>EObject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INITIAL_AXIOM_NODE_BINDING__EOBJECT = eINSTANCE.getInitialAxiomNodeBinding_EObject();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INITIAL_AXIOM_NODE_BINDING__NODE = eINSTANCE.getInitialAxiomNodeBinding_Node();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode <em>Application Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getApplicationMode()
		 * @generated
		 */
		EEnum APPLICATION_MODE = eINSTANCE.getApplicationMode();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.Severity <em>Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Severity
		 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.impl.InterpreterconfigurationPackageImpl#getSeverity()
		 * @generated
		 */
		EEnum SEVERITY = eINSTANCE.getSeverity();

	}

} //InterpreterconfigurationPackage
