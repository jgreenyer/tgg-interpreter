package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.ecore.EObject;

public class DummyTupleGenerator extends TupleGenerator {

	public DummyTupleGenerator() {
		super(new Object[0][0], null);
	}
	
	@Override
	public boolean hasNext() {
		return false;
	}
	
	@Override
	public EObject[] getNextTuple() {
		return null;
	}
	
}
