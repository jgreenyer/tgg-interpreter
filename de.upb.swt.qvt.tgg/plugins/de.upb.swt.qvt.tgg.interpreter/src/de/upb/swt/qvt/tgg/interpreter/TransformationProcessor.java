/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalEdgeBinding <em>Global Edge Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getGlobalNodeBinding <em>Global Node Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor <em>Rule Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter <em>Interpreter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getProgressMonitor <em>Progress Monitor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getCrossReferenceAdapter <em>Cross Reference Adapter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper <em>Helper</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesApplied <em>Rules Applied</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesRevoked <em>Rules Revoked</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesStillValid <em>Rules Still Valid</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedNodeBindings <em>Deleted Node Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedEdgeBindings <em>Deleted Edge Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getUnsuccessfulRuleApplicationAttempts <em>Unsuccessful Rule Application Attempts</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor()
 * @model abstract="true"
 * @generated
 */
public interface TransformationProcessor extends EObject {
	/**
	 * Returns the value of the '<em><b>Global Edge Binding</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject>},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Edge},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Edge Binding</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Edge Binding</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_GlobalEdgeBinding()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.VirtualModelEdgeToEdgeMapEntry<org.eclipse.emf.ecore.EEList<org.eclipse.emf.ecore.EObject>, de.upb.swt.qvt.tgg.Edge>" transient="true"
	 * @generated
	 */
	EMap<EList<EObject>, EList<Edge>> getGlobalEdgeBinding();

	/**
	 * Returns the value of the '<em><b>Global Node Binding</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link de.upb.swt.qvt.tgg.Node},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Node Binding</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Node Binding</em>' map.
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_GlobalNodeBinding()
	 * @model mapType="de.upb.swt.qvt.tgg.interpreterconfiguration.EObjectToNodeMapEntry<org.eclipse.emf.ecore.EObject, de.upb.swt.qvt.tgg.Node>" transient="true"
	 * @generated
	 */
	EMap<EObject, EList<Node>> getGlobalNodeBinding();

	/**
	 * Returns the value of the '<em><b>Rule Processor</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Processor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Processor</em>' containment reference.
	 * @see #setRuleProcessor(RuleProcessor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_RuleProcessor()
	 * @see de.upb.swt.qvt.tgg.interpreter.RuleProcessor#getTransformationProcessor
	 * @model opposite="transformationProcessor" containment="true" required="true"
	 * @generated
	 */
	RuleProcessor getRuleProcessor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRuleProcessor <em>Rule Processor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Processor</em>' containment reference.
	 * @see #getRuleProcessor()
	 * @generated
	 */
	void setRuleProcessor(RuleProcessor value);

	/**
	 * Returns the value of the '<em><b>Interpreter</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interpreter</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interpreter</em>' container reference.
	 * @see #setInterpreter(Interpreter)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_Interpreter()
	 * @see de.upb.swt.qvt.tgg.interpreter.Interpreter#getTransformationProcessor
	 * @model opposite="transformationProcessor" required="true" transient="false"
	 * @generated
	 */
	Interpreter getInterpreter();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getInterpreter <em>Interpreter</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpreter</em>' container reference.
	 * @see #getInterpreter()
	 * @generated
	 */
	void setInterpreter(Interpreter value);

	/**
	 * Returns the value of the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Progress Monitor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progress Monitor</em>' attribute.
	 * @see #setProgressMonitor(IProgressMonitor)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_ProgressMonitor()
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.IProgressMonitor" transient="true"
	 * @generated
	 */
	IProgressMonitor getProgressMonitor();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getProgressMonitor <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Progress Monitor</em>' attribute.
	 * @see #getProgressMonitor()
	 * @generated
	 */
	void setProgressMonitor(IProgressMonitor value);

	/**
	 * Returns the value of the '<em><b>Cross Reference Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cross Reference Adapter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cross Reference Adapter</em>' attribute.
	 * @see #setCrossReferenceAdapter(ECrossReferenceAdapter)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_CrossReferenceAdapter()
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.ECrossReferenceAdapter" transient="true"
	 * @generated
	 */
	ECrossReferenceAdapter getCrossReferenceAdapter();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getCrossReferenceAdapter <em>Cross Reference Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cross Reference Adapter</em>' attribute.
	 * @see #getCrossReferenceAdapter()
	 * @generated
	 */
	void setCrossReferenceAdapter(ECrossReferenceAdapter value);

	/**
	 * Returns the value of the '<em><b>Helper</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor <em>Transformation Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Helper</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Helper</em>' containment reference.
	 * @see #setHelper(TransformationProcessorHelper)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_Helper()
	 * @see de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper#getTransformationProcessor
	 * @model opposite="transformationProcessor" containment="true" required="true"
	 * @generated
	 */
	TransformationProcessorHelper getHelper();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getHelper <em>Helper</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Helper</em>' containment reference.
	 * @see #getHelper()
	 * @generated
	 */
	void setHelper(TransformationProcessorHelper value);

	/**
	 * Returns the value of the '<em><b>Rules Applied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules Applied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Applied</em>' attribute.
	 * @see #setRulesApplied(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_RulesApplied()
	 * @model transient="true"
	 * @generated
	 */
	Map<TripleGraphGrammarRule, Integer> getRulesApplied();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesApplied <em>Rules Applied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rules Applied</em>' attribute.
	 * @see #getRulesApplied()
	 * @generated
	 */
	void setRulesApplied(Map<TripleGraphGrammarRule, Integer> value);

	/**
	 * Returns the value of the '<em><b>Rules Revoked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules Revoked</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Revoked</em>' attribute.
	 * @see #setRulesRevoked(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_RulesRevoked()
	 * @model transient="true"
	 * @generated
	 */
	Map<TripleGraphGrammarRule, Integer> getRulesRevoked();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesRevoked <em>Rules Revoked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rules Revoked</em>' attribute.
	 * @see #getRulesRevoked()
	 * @generated
	 */
	void setRulesRevoked(Map<TripleGraphGrammarRule, Integer> value);

	/**
	 * Returns the value of the '<em><b>Rules Still Valid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules Still Valid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Still Valid</em>' attribute.
	 * @see #setRulesStillValid(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_RulesStillValid()
	 * @model transient="true"
	 * @generated
	 */
	Map<TripleGraphGrammarRule, Integer> getRulesStillValid();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getRulesStillValid <em>Rules Still Valid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rules Still Valid</em>' attribute.
	 * @see #getRulesStillValid()
	 * @generated
	 */
	void setRulesStillValid(Map<TripleGraphGrammarRule, Integer> value);

	/**
	 * Returns the value of the '<em><b>Deleted Node Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deleted Node Bindings</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deleted Node Bindings</em>' attribute.
	 * @see #setDeletedNodeBindings(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_DeletedNodeBindings()
	 * @model transient="true"
	 * @generated
	 */
	Map<EObject, Node> getDeletedNodeBindings();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedNodeBindings <em>Deleted Node Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deleted Node Bindings</em>' attribute.
	 * @see #getDeletedNodeBindings()
	 * @generated
	 */
	void setDeletedNodeBindings(Map<EObject, Node> value);

	/**
	 * Returns the value of the '<em><b>Deleted Edge Bindings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deleted Edge Bindings</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deleted Edge Bindings</em>' attribute.
	 * @see #setDeletedEdgeBindings(Map)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_DeletedEdgeBindings()
	 * @model transient="true"
	 * @generated
	 */
	Map<EList<EObject>, Edge> getDeletedEdgeBindings();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getDeletedEdgeBindings <em>Deleted Edge Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deleted Edge Bindings</em>' attribute.
	 * @see #getDeletedEdgeBindings()
	 * @generated
	 */
	void setDeletedEdgeBindings(Map<EList<EObject>, Edge> value);

	/**
	 * Returns the value of the '<em><b>Unsuccessful Rule Application Attempts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuccessful Rule Application Attempts</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuccessful Rule Application Attempts</em>' attribute.
	 * @see #setUnsuccessfulRuleApplicationAttempts(int)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_UnsuccessfulRuleApplicationAttempts()
	 * @model
	 * @generated
	 */
	int getUnsuccessfulRuleApplicationAttempts();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getUnsuccessfulRuleApplicationAttempts <em>Unsuccessful Rule Application Attempts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuccessful Rule Application Attempts</em>' attribute.
	 * @see #getUnsuccessfulRuleApplicationAttempts()
	 * @generated
	 */
	void setUnsuccessfulRuleApplicationAttempts(int value);

	/**
	 * Returns the value of the '<em><b>Edge Explore Operations Performed Leading To Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>' attribute.
	 * @see #setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(int)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications()
	 * @model
	 * @generated
	 */
	int getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>' attribute.
	 * @see #getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications()
	 * @generated
	 */
	void setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(int value);

	/**
	 * Returns the value of the '<em><b>Edge Explore Operations Performed Leading To Un Successful Rule Applications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>' attribute.
	 * @see #setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(int)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getTransformationProcessor_EdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications()
	 * @model
	 * @generated
	 */
	int getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.TransformationProcessor#getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>' attribute.
	 * @see #getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications()
	 * @generated
	 */
	void setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Start the transformation process after initializing the global node and
	 * edge binding hash manager.
	 * 
	 * The algorithm basically works as follows:
	 * 
	 * while(rulesApplicable()){
	 * 	ruleBinding = createInitialRuleBinding();
	 * 	if (ruleProcessor.applyRule(ruleBinding)){
	 * 		ruleSuccessfullyApplied(ruleBinding)
	 * 	}
	 * }
	 * return transformationSuccessful()
	 * <!-- end-model-doc -->
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.Status" monitorDataType="de.upb.swt.qvt.tgg.interpreter.IProgressMonitor"
	 * @generated
	 */
	IStatus performTransformation(IProgressMonitor monitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * this method is called after the successful application of a rule.
	 * It is responsible for updating the global node and edge bindings
	 * and storing the rule binding in the configuration (if required by
	 * the configuration).
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void ruleApplicationPostProcessing(RuleBinding ruleBinding);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * the transformation process continues as long as this operation
	 * returns TRUE.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean rulesApplicable();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * creates initial rule bindigs (with at least one candidate node binding)
	 * that is handed to the rule processor to process.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	RuleBinding createInitialRuleBinding();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.Status"
	 * @generated
	 */
	IStatus transformationSuccessful();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void ruleApplicationUnsuccessfulPostProcessing(RuleBinding ruleBinding);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Called when a rule application does not hold any more (due to changes in
	 * the models in an update or syncronization scenario).
	 * Usually the rule application should then be revoked or repaired.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void ruleRepairUnsuccessful(RuleBinding ruleBinding);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int countRuleApplication(Map<TripleGraphGrammarRule, Integer> ruleMap, TripleGraphGrammarRule rule, int count);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean usePartiallyReusablePatternSearch();

	/**
	 * @return List of EObjects of RuleBindings which didn't match anymore.
	 * @model dataType="de.upb.swt.qvt.tgg.interpreter.Status"
	 *        monitorDataType="de.upb.swt.qvt.tgg.interpreter.IProgressMonitor"
	 * @generated NOT
	 */
	IStatus calculatePossiblyChangedObjects();

} // TransformationProcessor
