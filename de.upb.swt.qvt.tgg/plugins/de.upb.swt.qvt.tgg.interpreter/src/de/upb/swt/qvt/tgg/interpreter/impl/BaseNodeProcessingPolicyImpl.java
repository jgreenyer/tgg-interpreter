/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.GraphMatcher;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl#getTransformationProcessor <em>Transformation Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl#getRuleProcessor <em>Rule Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl#getGraphMatcher <em>Graph Matcher</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BaseNodeProcessingPolicyImpl extends EObjectImpl implements BaseNodeProcessingPolicy {
	/**
	 * The cached value of the '{@link #getTransformationProcessor() <em>Transformation Processor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationProcessor()
	 * @generated
	 * @ordered
	 */
	protected TransformationProcessor transformationProcessor;

	/**
	 * The cached value of the '{@link #getRuleProcessor() <em>Rule Processor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleProcessor()
	 * @generated
	 * @ordered
	 */
	protected RuleProcessor ruleProcessor;

	/**
	 * The cached value of the '{@link #getGraphMatcher() <em>Graph Matcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphMatcher()
	 * @generated
	 * @ordered
	 */
	protected GraphMatcher graphMatcher;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseNodeProcessingPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.BASE_NODE_PROCESSING_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessor getTransformationProcessor() {
		if (transformationProcessor != null && transformationProcessor.eIsProxy()) {
			InternalEObject oldTransformationProcessor = (InternalEObject)transformationProcessor;
			transformationProcessor = (TransformationProcessor)eResolveProxy(oldTransformationProcessor);
			if (transformationProcessor != oldTransformationProcessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR, oldTransformationProcessor, transformationProcessor));
			}
		}
		return transformationProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessor basicGetTransformationProcessor() {
		return transformationProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationProcessor(TransformationProcessor newTransformationProcessor) {
		TransformationProcessor oldTransformationProcessor = transformationProcessor;
		transformationProcessor = newTransformationProcessor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR, oldTransformationProcessor, transformationProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher getGraphMatcher() {
		if (graphMatcher != null && graphMatcher.eIsProxy()) {
			InternalEObject oldGraphMatcher = (InternalEObject)graphMatcher;
			graphMatcher = (GraphMatcher)eResolveProxy(oldGraphMatcher);
			if (graphMatcher != oldGraphMatcher) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER, oldGraphMatcher, graphMatcher));
			}
		}
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphMatcher basicGetGraphMatcher() {
		return graphMatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphMatcher(GraphMatcher newGraphMatcher, NotificationChain msgs) {
		GraphMatcher oldGraphMatcher = graphMatcher;
		graphMatcher = newGraphMatcher;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER, oldGraphMatcher, newGraphMatcher);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphMatcher(GraphMatcher newGraphMatcher) {
		if (newGraphMatcher != graphMatcher) {
			NotificationChain msgs = null;
			if (graphMatcher != null)
				msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY, GraphMatcher.class, msgs);
			if (newGraphMatcher != null)
				msgs = ((InternalEObject)newGraphMatcher).eInverseAdd(this, InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY, GraphMatcher.class, msgs);
			msgs = basicSetGraphMatcher(newGraphMatcher, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER, newGraphMatcher, newGraphMatcher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleProcessor getRuleProcessor() {
		if (ruleProcessor != null && ruleProcessor.eIsProxy()) {
			InternalEObject oldRuleProcessor = (InternalEObject)ruleProcessor;
			ruleProcessor = (RuleProcessor)eResolveProxy(oldRuleProcessor);
			if (ruleProcessor != oldRuleProcessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR, oldRuleProcessor, ruleProcessor));
			}
		}
		return ruleProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleProcessor basicGetRuleProcessor() {
		return ruleProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleProcessor(RuleProcessor newRuleProcessor) {
		RuleProcessor oldRuleProcessor = ruleProcessor;
		ruleProcessor = newRuleProcessor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR, oldRuleProcessor, ruleProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void enforceNode(Node node, EObject eObject) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeNode(Node node, EObject eObject) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean nodeMatches(Node node, EObject candidateEObject) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getUnboundEdgeToMatchNext(Node node, EObject eObject) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getCandidateNeighborObjectsForEdge(Node node, EObject candidateObject, Edge edge) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				if (graphMatcher != null)
					msgs = ((InternalEObject)graphMatcher).eInverseRemove(this, InterpreterPackage.GRAPH_MATCHER__NODE_MATCHING_POLICY, GraphMatcher.class, msgs);
				return basicSetGraphMatcher((GraphMatcher)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				return basicSetGraphMatcher(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR:
				if (resolve) return getTransformationProcessor();
				return basicGetTransformationProcessor();
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR:
				if (resolve) return getRuleProcessor();
				return basicGetRuleProcessor();
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				if (resolve) return getGraphMatcher();
				return basicGetGraphMatcher();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)newValue);
				return;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)newValue);
				return;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR:
				setTransformationProcessor((TransformationProcessor)null);
				return;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)null);
				return;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				setGraphMatcher((GraphMatcher)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR:
				return transformationProcessor != null;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR:
				return ruleProcessor != null;
			case InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER:
				return graphMatcher != null;
		}
		return super.eIsSet(featureID);
	}

} //BaseNodeProcessingPolicyImpl
