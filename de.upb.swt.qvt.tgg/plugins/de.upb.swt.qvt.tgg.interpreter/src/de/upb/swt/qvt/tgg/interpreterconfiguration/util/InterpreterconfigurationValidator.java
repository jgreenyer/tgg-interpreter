/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EObjectValidator;

import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Severity;
import de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage
 * @generated
 */
public class InterpreterconfigurationValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final InterpreterconfigurationValidator INSTANCE = new InterpreterconfigurationValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "de.upb.swt.qvt.tgg.interpreterconfiguration";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterconfigurationValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return InterpreterconfigurationPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case InterpreterconfigurationPackage.CONFIGURATION:
				return validateConfiguration((Configuration)value, diagnostics, context);
			case InterpreterconfigurationPackage.DOMAIN_MODEL:
				return validateDomainModel((DomainModel)value, diagnostics, context);
			case InterpreterconfigurationPackage.EOBJECT_TO_NODE_MAP_ENTRY:
				return validateEObjectToNodeMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY:
				return validateVirtualModelEdgeToEdgeMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.RULE_BINDING:
				return validateRuleBinding((RuleBinding)value, diagnostics, context);
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO:
				return validateApplicationScenario((ApplicationScenario)value, diagnostics, context);
			case InterpreterconfigurationPackage.RULE_BINDING_CONTAINER:
				return validateRuleBindingContainer((RuleBindingContainer)value, diagnostics, context);
			case InterpreterconfigurationPackage.TYPED_MODEL_TO_DOMAIN_MODEL_MAP_ENTRY:
				return validateTypedModelToDomainModelMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.TGG_RULE_TO_NODES_MAP_ENTRY:
				return validateTGGRuleToNodesMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.NODE_TO_EOBJECT_MAP_ENTRY:
				return validateNodeToEObjectMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.EDGE_TO_VIRTUAL_MODEL_EDGE_MAP_ENTRY:
				return validateEdgeToVirtualModelEdgeMapEntry((Map.Entry<?, ?>)value, diagnostics, context);
			case InterpreterconfigurationPackage.AXIOM_BINDING:
				return validateAxiomBinding((AxiomBinding)value, diagnostics, context);
			case InterpreterconfigurationPackage.INITIAL_AXIOM_NODE_BINDING:
				return validateInitialAxiomNodeBinding((InitialAxiomNodeBinding)value, diagnostics, context);
			case InterpreterconfigurationPackage.APPLICATION_MODE:
				return validateApplicationMode((ApplicationMode)value, diagnostics, context);
			case InterpreterconfigurationPackage.SEVERITY:
				return validateSeverity((Severity)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConfiguration(Configuration configuration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(configuration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(configuration, diagnostics, context);
		if (result || diagnostics != null) result &= validateConfiguration_numberOfDomainModelsMatchesNumberOfTypedModels(configuration, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the numberOfDomainModelsMatchesNumberOfTypedModels constraint of '<em>Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CONFIGURATION__NUMBER_OF_DOMAIN_MODELS_MATCHES_NUMBER_OF_TYPED_MODELS__EEXPRESSION = "self.domainModel->size() = self.tripleGraphGrammar.modelParameter->size()";

	/**
	 * Validates the numberOfDomainModelsMatchesNumberOfTypedModels constraint of '<em>Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConfiguration_numberOfDomainModelsMatchesNumberOfTypedModels(Configuration configuration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(InterpreterconfigurationPackage.Literals.CONFIGURATION,
				 configuration,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "numberOfDomainModelsMatchesNumberOfTypedModels",
				 CONFIGURATION__NUMBER_OF_DOMAIN_MODELS_MATCHES_NUMBER_OF_TYPED_MODELS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDomainModel(DomainModel domainModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(domainModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateEObjectToNodeMapEntry(Map.Entry<?, ?> eObjectToNodeMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// return validate_EveryDefaultConstraint((EObject)
		// eObjectToNodeMapEntry, diagnostics, context);
		return true; // FIXME
	}
	
	
	@Override
	public boolean validate_EveryProxyResolves(EObject eObject,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (eObject instanceof EObjectToNodeMapEntryImpl) {
			return true; //quick fix for rule bindings to validation even if there are damaged rule bindings
		} else
			return super.validate_EveryProxyResolves(eObject, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVirtualModelEdgeToEdgeMapEntry(Map.Entry<?, ?> virtualModelEdgeToEdgeMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)virtualModelEdgeToEdgeMapEntry, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateRuleBinding(RuleBinding ruleBinding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// return validate_EveryDefaultConstraint(ruleBinding, diagnostics,
		// context);
		return true; // FIXME
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationScenario(ApplicationScenario applicationScenario, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(applicationScenario, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationScenario_eachDomainModelEitherSourceCorrespondenceOrTarget(applicationScenario, diagnostics, context);
		if (result || diagnostics != null) result &= validateApplicationScenario_eitherResourceURIOrRootObjectsSetForDomainModels(applicationScenario, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the eachDomainModelEitherSourceCorrespondenceOrTarget constraint of '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String APPLICATION_SCENARIO__EACH_DOMAIN_MODEL_EITHER_SOURCE_CORRESPONDENCE_OR_TARGET__EEXPRESSION = "self.sourceDomainModel->select(dm|self.correspondenceModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\n" +
		"and self.correspondenceModel->select(dm|self.sourceDomainModel->includes(dm) or self.targetDomainModel->includes(dm))->isEmpty()\r\n" +
		"and self.targetDomainModel->select(dm|self.sourceDomainModel->includes(dm) or self.correspondenceModel->includes(dm))->isEmpty()";

	/**
	 * Validates the eachDomainModelEitherSourceCorrespondenceOrTarget constraint of '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationScenario_eachDomainModelEitherSourceCorrespondenceOrTarget(ApplicationScenario applicationScenario, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO,
				 applicationScenario,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "eachDomainModelEitherSourceCorrespondenceOrTarget",
				 APPLICATION_SCENARIO__EACH_DOMAIN_MODEL_EITHER_SOURCE_CORRESPONDENCE_OR_TARGET__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the eitherResourceURIOrRootObjectsSetForDomainModels constraint of '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String APPLICATION_SCENARIO__EITHER_RESOURCE_URI_OR_ROOT_OBJECTS_SET_FOR_DOMAIN_MODELS__EEXPRESSION = "self.sourceDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\n" +
		"self.targetDomainModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty() and\r\n" +
		"self.correspondenceModel->select(dm:DomainModel| (dm.resourceURI->isEmpty()) and dm.rootObject->isEmpty())->isEmpty()";

	/**
	 * Validates the eitherResourceURIOrRootObjectsSetForDomainModels constraint of '<em>Application Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationScenario_eitherResourceURIOrRootObjectsSetForDomainModels(ApplicationScenario applicationScenario, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO,
				 applicationScenario,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "eitherResourceURIOrRootObjectsSetForDomainModels",
				 APPLICATION_SCENARIO__EITHER_RESOURCE_URI_OR_ROOT_OBJECTS_SET_FOR_DOMAIN_MODELS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRuleBindingContainer(RuleBindingContainer ruleBindingContainer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(ruleBindingContainer, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTypedModelToDomainModelMapEntry(Map.Entry<?, ?> typedModelToDomainModelMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)typedModelToDomainModelMapEntry, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTGGRuleToNodesMapEntry(Map.Entry<?, ?> tggRuleToNodesMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)tggRuleToNodesMapEntry, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateNodeToEObjectMapEntry(Map.Entry<?, ?> nodeToEObjectMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		//return validate_EveryDefaultConstraint((EObject)nodeToEObjectMapEntry, diagnostics, context);
		return true; // FIXME
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEdgeToVirtualModelEdgeMapEntry(Map.Entry<?, ?> edgeToVirtualModelEdgeMapEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)edgeToVirtualModelEdgeMapEntry, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAxiomBinding(AxiomBinding axiomBinding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(axiomBinding, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInitialAxiomNodeBinding(InitialAxiomNodeBinding initialAxiomNodeBinding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(initialAxiomNodeBinding, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationMode(ApplicationMode applicationMode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSeverity(Severity severity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //InterpreterconfigurationValidator
