/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage
 * @generated
 */
public interface InterpreterFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InterpreterFactory eINSTANCE = de.upb.swt.qvt.tgg.interpreter.impl.InterpreterFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Interpreter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interpreter</em>'.
	 * @generated
	 */
	Interpreter createInterpreter();

	/**
	 * Returns a new object of class '<em>Front Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Front Manager</em>'.
	 * @generated
	 */
	FrontManager createFrontManager();

	/**
	 * Returns a new object of class '<em>Rule Processor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Processor</em>'.
	 * @generated
	 */
	RuleProcessor createRuleProcessor();

	/**
	 * Returns a new object of class '<em>Front Transformation Processor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Front Transformation Processor</em>'.
	 * @generated
	 */
	FrontTransformationProcessor createFrontTransformationProcessor();

	/**
	 * Returns a new object of class '<em>Graph Matcher</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Graph Matcher</em>'.
	 * @generated
	 */
	GraphMatcher createGraphMatcher();

	/**
	 * Returns a new object of class '<em>Default Node Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Node Processing Policy</em>'.
	 * @generated
	 */
	DefaultNodeProcessingPolicy createDefaultNodeProcessingPolicy();

	/**
	 * Returns a new object of class '<em>Default Edge Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Edge Processing Policy</em>'.
	 * @generated
	 */
	DefaultEdgeProcessingPolicy createDefaultEdgeProcessingPolicy();

	/**
	 * Returns a new object of class '<em>Default OCL Constraint Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default OCL Constraint Processing Policy</em>'.
	 * @generated
	 */
	DefaultOCLConstraintProcessingPolicy createDefaultOCLConstraintProcessingPolicy();

	/**
	 * Returns a new object of class '<em>Transformation Processor Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transformation Processor Helper</em>'.
	 * @generated
	 */
	TransformationProcessorHelper createTransformationProcessorHelper();

	/**
	 * Returns a new object of class '<em>Debug Support</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Debug Support</em>'.
	 * @generated
	 */
	DebugSupport createDebugSupport();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	InterpreterPackage getInterpreterPackage();

} //InterpreterFactory
