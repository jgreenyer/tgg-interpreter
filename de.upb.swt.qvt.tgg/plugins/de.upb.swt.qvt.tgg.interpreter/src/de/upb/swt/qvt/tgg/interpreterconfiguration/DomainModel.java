/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.upb.swt.qvt.qvtbase.TypedModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * When loading a domain model, we first check whether
 * any root objects were assigned. If that is the case, we
 * apply the transformation to the models contained in
 * these root objects.
 * Any given resourceURI will be ignored.
 * 
 * In case that not root objects are given, we load the
 * resource specified by the resourceURI string. This string
 * must be either an absolute workspace path to a domain 
 * model resource or it must be a path relative to the 
 * configuration resource.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getTypedModel <em>Typed Model</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getRootObject <em>Root Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResourceURI <em>Resource URI</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getResource <em>Resource</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getDomainObject <em>Domain Object</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getShowDomainModelDiagnostic <em>Show Domain Model Diagnostic</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel()
 * @model
 * @generated
 */
public interface DomainModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Typed Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Model</em>' reference.
	 * @see #setTypedModel(TypedModel)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_TypedModel()
	 * @model required="true"
	 * @generated
	 */
	TypedModel getTypedModel();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getTypedModel <em>Typed Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Typed Model</em>' reference.
	 * @see #getTypedModel()
	 * @generated
	 */
	void setTypedModel(TypedModel value);

	/**
	 * Returns the value of the '<em><b>Root Object</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Object</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_RootObject()
	 * @model
	 * @generated
	 */
	EList<EObject> getRootObject();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Returns the value of the '<em><b>Resource URI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource URI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource URI</em>' attribute list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_ResourceURI()
	 * @model
	 * @generated
	 */
	EList<String> getResourceURI();

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.resource.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' attribute list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_Resource()
	 * @model transient="true"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * Returns the value of the '<em><b>Domain Object</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Object</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_DomainObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EObject> getDomainObject();

	/**
	 * Returns the value of the '<em><b>Show Domain Model Diagnostic</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.swt.qvt.tgg.interpreterconfiguration.Severity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * allows the user to define whether diagnostics above a certain severity-level shall be shown. The diagnostics refer to checking the domain model for validity against the meta-model or invariants formulated in an external OCL file.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Show Domain Model Diagnostic</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Severity
	 * @see #setShowDomainModelDiagnostic(Severity)
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage#getDomainModel_ShowDomainModelDiagnostic()
	 * @model
	 * @generated
	 */
	Severity getShowDomainModelDiagnostic();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel#getShowDomainModelDiagnostic <em>Show Domain Model Diagnostic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Domain Model Diagnostic</em>' attribute.
	 * @see de.upb.swt.qvt.tgg.interpreterconfiguration.Severity
	 * @see #getShowDomainModelDiagnostic()
	 * @generated
	 */
	void setShowDomainModelDiagnostic(Severity value);

} // DomainModel
