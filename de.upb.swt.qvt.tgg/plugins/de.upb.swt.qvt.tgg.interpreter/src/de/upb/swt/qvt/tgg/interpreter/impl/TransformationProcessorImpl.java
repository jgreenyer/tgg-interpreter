/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.impl;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.OCL;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.RuleProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessor;
import de.upb.swt.qvt.tgg.interpreter.TransformationProcessorHelper;
import de.upb.swt.qvt.tgg.interpreter.plugin.InterpreterPlugin;
import de.upb.swt.qvt.tgg.interpreter.util.InterpreterStatus;
import de.upb.swt.qvt.tgg.interpreter.util.RelevantReferencesCrossReferenceAdapter;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Severity;
import de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl;
import de.upb.swt.qvt.tgg.interpreterconfiguration.impl.VirtualModelEdgeToEdgeMapEntryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation Processor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getGlobalEdgeBinding <em>Global Edge Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getGlobalNodeBinding <em>Global Node Binding</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getRuleProcessor <em>Rule Processor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getInterpreter <em>Interpreter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getProgressMonitor <em>Progress Monitor</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getCrossReferenceAdapter <em>Cross Reference Adapter</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getHelper <em>Helper</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getRulesApplied <em>Rules Applied</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getRulesRevoked <em>Rules Revoked</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getRulesStillValid <em>Rules Still Valid</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getDeletedNodeBindings <em>Deleted Node Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getDeletedEdgeBindings <em>Deleted Edge Bindings</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getUnsuccessfulRuleApplicationAttempts <em>Unsuccessful Rule Application Attempts</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.impl.TransformationProcessorImpl#getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class TransformationProcessorImpl extends EObjectImpl implements TransformationProcessor {
	
	public class RuleCounter {
		TripleGraphGrammarRule rule;
		int count = 0;
		public RuleCounter(TripleGraphGrammarRule rule) {
			super();
			this.rule = rule;
		}
	}

	/**
	 * The cached value of the '{@link #getGlobalEdgeBinding() <em>Global Edge Binding</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalEdgeBinding()
	 * @generated
	 * @ordered
	 */
	protected EMap<EList<EObject>, EList<Edge>> globalEdgeBinding;

	/**
	 * The cached value of the '{@link #getGlobalNodeBinding() <em>Global Node Binding</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalNodeBinding()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<Node>> globalNodeBinding;

	private static Logger logger = InterpreterPlugin.getLogManager().getLogger(TransformationProcessorImpl.class.getName());

	/**
	 * The cached value of the '{@link #getRuleProcessor() <em>Rule Processor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleProcessor()
	 * @generated
	 * @ordered
	 */
	protected RuleProcessor ruleProcessor;

	/**
	 * The default value of the '{@link #getProgressMonitor() <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressMonitor()
	 * @generated
	 * @ordered
	 */
	protected static final IProgressMonitor PROGRESS_MONITOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProgressMonitor() <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressMonitor()
	 * @generated
	 * @ordered
	 */
	protected IProgressMonitor progressMonitor = PROGRESS_MONITOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getCrossReferenceAdapter() <em>Cross Reference Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCrossReferenceAdapter()
	 * @generated
	 * @ordered
	 */
	protected static final ECrossReferenceAdapter CROSS_REFERENCE_ADAPTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCrossReferenceAdapter() <em>Cross Reference Adapter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCrossReferenceAdapter()
	 * @generated
	 * @ordered
	 */
	protected ECrossReferenceAdapter crossReferenceAdapter = CROSS_REFERENCE_ADAPTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHelper() <em>Helper</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHelper()
	 * @generated
	 * @ordered
	 */
	protected TransformationProcessorHelper helper;

	/**
	 * The cached value of the '{@link #getRulesApplied() <em>Rules Applied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesApplied()
	 * @generated
	 * @ordered
	 */
	protected Map<TripleGraphGrammarRule, Integer> rulesApplied;

	/**
	 * The cached value of the '{@link #getRulesRevoked() <em>Rules Revoked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesRevoked()
	 * @generated
	 * @ordered
	 */
	protected Map<TripleGraphGrammarRule, Integer> rulesRevoked;

	/**
	 * The cached value of the '{@link #getRulesStillValid() <em>Rules Still Valid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesStillValid()
	 * @generated
	 * @ordered
	 */
	protected Map<TripleGraphGrammarRule, Integer> rulesStillValid;

	/**
	 * The cached value of the '{@link #getDeletedNodeBindings() <em>Deleted Node Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeletedNodeBindings()
	 * @generated
	 * @ordered
	 */
	protected Map<EObject, Node> deletedNodeBindings;

	/**
	 * The cached value of the '{@link #getDeletedEdgeBindings() <em>Deleted Edge Bindings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeletedEdgeBindings()
	 * @generated
	 * @ordered
	 */
	protected Map<EList<EObject>, Edge> deletedEdgeBindings;

	/**
	 * The default value of the '{@link #getUnsuccessfulRuleApplicationAttempts() <em>Unsuccessful Rule Application Attempts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuccessfulRuleApplicationAttempts()
	 * @generated
	 * @ordered
	 */
	protected static final int UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getUnsuccessfulRuleApplicationAttempts() <em>Unsuccessful Rule Application Attempts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuccessfulRuleApplicationAttempts()
	 * @generated
	 * @ordered
	 */
	protected int unsuccessfulRuleApplicationAttempts = UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications()
	 * @generated
	 * @ordered
	 */
	protected static final int EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() <em>Edge Explore Operations Performed Leading To Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications()
	 * @generated
	 * @ordered
	 */
	protected int edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications = EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications()
	 * @generated
	 * @ordered
	 */
	protected static final int EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() <em>Edge Explore Operations Performed Leading To Un Successful Rule Applications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications()
	 * @generated
	 * @ordered
	 */
	protected int edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications = EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected TransformationProcessorImpl() {
		super();
		
		setHelper(InterpreterFactory.eINSTANCE.createTransformationProcessorHelper());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeletedNodeBindings(Map<EObject, Node> newDeletedNodeBindings) {
		Map<EObject, Node> oldDeletedNodeBindings = deletedNodeBindings;
		deletedNodeBindings = newDeletedNodeBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS, oldDeletedNodeBindings, deletedNodeBindings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<EList<EObject>, Edge> getDeletedEdgeBindings() {
		return deletedEdgeBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeletedEdgeBindings(Map<EList<EObject>, Edge> newDeletedEdgeBindings) {
		Map<EList<EObject>, Edge> oldDeletedEdgeBindings = deletedEdgeBindings;
		deletedEdgeBindings = newDeletedEdgeBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS, oldDeletedEdgeBindings, deletedEdgeBindings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUnsuccessfulRuleApplicationAttempts() {
		return unsuccessfulRuleApplicationAttempts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuccessfulRuleApplicationAttempts(int newUnsuccessfulRuleApplicationAttempts) {
		int oldUnsuccessfulRuleApplicationAttempts = unsuccessfulRuleApplicationAttempts;
		unsuccessfulRuleApplicationAttempts = newUnsuccessfulRuleApplicationAttempts;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS, oldUnsuccessfulRuleApplicationAttempts, unsuccessfulRuleApplicationAttempts));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications() {
		return edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(int newEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications) {
		int oldEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications = edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
		edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications = newEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS, oldEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications, edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications() {
		return edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(int newEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications) {
		int oldEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications = edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;
		edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications = newEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS, oldEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications, edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.TRANSFORMATION_PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EList<EObject>, EList<Edge>> getGlobalEdgeBinding() {
		if (globalEdgeBinding == null) {
			globalEdgeBinding = new EcoreEMap<EList<EObject>,EList<Edge>>(InterpreterconfigurationPackage.Literals.VIRTUAL_MODEL_EDGE_TO_EDGE_MAP_ENTRY, VirtualModelEdgeToEdgeMapEntryImpl.class, this, InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING);
		}
		return globalEdgeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<Node>> getGlobalNodeBinding() {
		if (globalNodeBinding == null) {
			globalNodeBinding = new EcoreEMap<EObject,EList<Node>>(InterpreterconfigurationPackage.Literals.EOBJECT_TO_NODE_MAP_ENTRY, EObjectToNodeMapEntryImpl.class, this, InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING);
		}
		return globalNodeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleProcessor getRuleProcessor() {
		return ruleProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleProcessor(RuleProcessor newRuleProcessor, NotificationChain msgs) {
		RuleProcessor oldRuleProcessor = ruleProcessor;
		ruleProcessor = newRuleProcessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR, oldRuleProcessor, newRuleProcessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleProcessor(RuleProcessor newRuleProcessor) {
		if (newRuleProcessor != ruleProcessor) {
			NotificationChain msgs = null;
			if (ruleProcessor != null)
				msgs = ((InternalEObject)ruleProcessor).eInverseRemove(this, InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR, RuleProcessor.class, msgs);
			if (newRuleProcessor != null)
				msgs = ((InternalEObject)newRuleProcessor).eInverseAdd(this, InterpreterPackage.RULE_PROCESSOR__TRANSFORMATION_PROCESSOR, RuleProcessor.class, msgs);
			msgs = basicSetRuleProcessor(newRuleProcessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR, newRuleProcessor, newRuleProcessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter getInterpreter() {
		if (eContainerFeatureID() != InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER) return null;
		return (Interpreter)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterpreter(Interpreter newInterpreter, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newInterpreter, InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterpreter(Interpreter newInterpreter) {
		if (newInterpreter != eInternalContainer() || (eContainerFeatureID() != InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER && newInterpreter != null)) {
			if (EcoreUtil.isAncestor(this, newInterpreter))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newInterpreter != null)
				msgs = ((InternalEObject)newInterpreter).eInverseAdd(this, InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR, Interpreter.class, msgs);
			msgs = basicSetInterpreter(newInterpreter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER, newInterpreter, newInterpreter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IProgressMonitor getProgressMonitor() {
		return progressMonitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProgressMonitor(IProgressMonitor newProgressMonitor) {
		IProgressMonitor oldProgressMonitor = progressMonitor;
		progressMonitor = newProgressMonitor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR, oldProgressMonitor, progressMonitor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECrossReferenceAdapter getCrossReferenceAdapter() {
		return crossReferenceAdapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCrossReferenceAdapter(ECrossReferenceAdapter newCrossReferenceAdapter) {
		ECrossReferenceAdapter oldCrossReferenceAdapter = crossReferenceAdapter;
		crossReferenceAdapter = newCrossReferenceAdapter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER, oldCrossReferenceAdapter, crossReferenceAdapter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationProcessorHelper getHelper() {
		return helper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHelper(TransformationProcessorHelper newHelper, NotificationChain msgs) {
		TransformationProcessorHelper oldHelper = helper;
		helper = newHelper;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER, oldHelper, newHelper);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHelper(TransformationProcessorHelper newHelper) {
		if (newHelper != helper) {
			NotificationChain msgs = null;
			if (helper != null)
				msgs = ((InternalEObject)helper).eInverseRemove(this, InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR, TransformationProcessorHelper.class, msgs);
			if (newHelper != null)
				msgs = ((InternalEObject)newHelper).eInverseAdd(this, InterpreterPackage.TRANSFORMATION_PROCESSOR_HELPER__TRANSFORMATION_PROCESSOR, TransformationProcessorHelper.class, msgs);
			msgs = basicSetHelper(newHelper, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER, newHelper, newHelper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<TripleGraphGrammarRule, Integer> getRulesApplied() {
		return rulesApplied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRulesApplied(Map<TripleGraphGrammarRule, Integer> newRulesApplied) {
		Map<TripleGraphGrammarRule, Integer> oldRulesApplied = rulesApplied;
		rulesApplied = newRulesApplied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_APPLIED, oldRulesApplied, rulesApplied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<TripleGraphGrammarRule, Integer> getRulesRevoked() {
		return rulesRevoked;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRulesRevoked(Map<TripleGraphGrammarRule, Integer> newRulesRevoked) {
		Map<TripleGraphGrammarRule, Integer> oldRulesRevoked = rulesRevoked;
		rulesRevoked = newRulesRevoked;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_REVOKED, oldRulesRevoked, rulesRevoked));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<TripleGraphGrammarRule, Integer> getRulesStillValid() {
		return rulesStillValid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRulesStillValid(Map<TripleGraphGrammarRule, Integer> newRulesStillValid) {
		Map<TripleGraphGrammarRule, Integer> oldRulesStillValid = rulesStillValid;
		rulesStillValid = newRulesStillValid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_STILL_VALID, oldRulesStillValid, rulesStillValid));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<EObject, Node> getDeletedNodeBindings() {
		return deletedNodeBindings;
	}

	private class TGGRuleComparator implements Comparator<TripleGraphGrammarRule> {
		@Override
		public int compare(TripleGraphGrammarRule o1,
				TripleGraphGrammarRule o2) {
			String name1 = o1.getName();
			if (name1 == null)
				name1 = "";
			String name2 = o2.getName();
			if (name2 == null)
				name2 = "";
			return name1.compareTo(name2);
		}
	};

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IStatus performTransformation(IProgressMonitor monitor) {
		
		long time = System.currentTimeMillis();
		unsuccessfulRuleApplicationAttempts = 0;
		edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications = 0;
		edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications = 0;
		
		if (monitor == null)
			monitor = new NullProgressMonitor();
		setProgressMonitor(monitor);
		
		// create rule processor
		if (getRuleProcessor() == null) {
			setRuleProcessor(InterpreterFactory.eINSTANCE.createRuleProcessor());
		}
		
		// set up RelevantReferencesCrossReferenceAdapter
		setCrossReferenceAdapter(
				//new ECrossReferenceAdapter());
				new RelevantReferencesCrossReferenceAdapter(
						getInterpreter().getConfiguration().getTripleGraphGrammar().getRelevantReferences()));
		for (EObject domainRooObject : getInterpreter().getConfiguration().getDomainRootObjects()) {
			domainRooObject.eAdapters().add(getCrossReferenceAdapter());
		}
		
		TGGRuleComparator tggRuleComparator = new TGGRuleComparator();
		setRulesApplied(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));
		setRulesRevoked(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));
		setRulesStillValid(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));

		//Initialize arrays to store elements marked for deletion
		setDeletedEdgeBindings(new HashMap<EList<EObject>, Edge>());
		setDeletedNodeBindings(new HashMap<EObject, Node>());
		
		// update: check for changes that made rule applications inapplicable
		if (ApplicationMode.UPDATE.equals(getInterpreter().getConfiguration().getActiveApplicationScenario().getMode())) {

			RuleBindingContainer oldRuleBindingContainer = getInterpreter().getConfiguration().getRuleBindingContainer();

			// Sets the initial axiom binding to the new container, such that it
			// is not lost in the update.
			if (oldRuleBindingContainer.getInitialAxiomBinding() != null) {
				getInterpreter().getConfiguration().getRuleBindingContainer()
						.setInitialAxiomBinding(oldRuleBindingContainer.getInitialAxiomBinding());
			}
			
			//Before removing the old rule binding container from its resource, make sure that everything is resolved
			EcoreUtil.resolveAll(getInterpreter().getConfiguration().getRuleBindingContainer());
			//Throw away all old rule bindings. If we are updating incrementally, we later restore every binding that is still valid. 
			getInterpreter().getConfiguration().setRuleBindingContainer(InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer());
			getGlobalNodeBinding().clear();
			getGlobalEdgeBinding().clear();

			RuleBinding[] allOldRuleBindings = new RuleBinding[0];
			allOldRuleBindings = oldRuleBindingContainer.getRuleBinding().toArray(allOldRuleBindings);
			for (RuleBinding ruleBinding : allOldRuleBindings) {
				// Some fields are not (or cannot be) serialized, thus these have to be rebuild:
				// 0. remove invalid node bindings
				/*ArrayList<EObject> toBeDeleted = new ArrayList<EObject>();
				for (Map.Entry<EObject, EList<Node>> nodeBinding : ruleBinding.getNodeBinding()) {
					if (nodeBinding.getKey().eIsProxy())
						toBeDeleted.add(nodeBinding.getKey());
				}
				ruleBinding.getNodeBinding().removeAll(toBeDeleted);*/
				
				// 1. rebuild NodeToEObjectMap
				rebuildNodeToEObjectMap(ruleBinding);

				// 2. rebuild VirtualModelEdges and put them into EdgeBinding
				// and
				// VirtualModelEdgeMap
				rebuildVirtualModelEdgeMap(ruleBinding);
			}

			for (RuleBinding ruleBinding : allOldRuleBindings) {
				//check (in the order of applications) if rule application is still consistent
				if (logger.isDebugEnabled())
					logger.debug("Checking rule application for rule " + ruleBinding.getTggRule().getName() + ".");
				if (getRuleProcessor().processRule(ruleBinding)){
					//update front manager and global node binding.
					ruleApplicationPostProcessing(ruleBinding);  
					//count the rule application
					countRuleApplication(getRulesStillValid(), ruleBinding.getTggRule(), 1);
				}else{
					if (logger.isDebugEnabled())
						logger.debug("Trying to repair invalid rule application.");
					
					if (getRuleProcessor().repairRule(ruleBinding)) {
						//update front manager and global node binding.
						ruleApplicationPostProcessing(ruleBinding);  
						//count the rule application
						countRuleApplication(getRulesApplied(), ruleBinding.getTggRule(), 1);
					}
					else {
						if (logger.isDebugEnabled())
							logger.debug("Rule is invalid and not repairable. Removing...");
						ruleRepairUnsuccessful(ruleBinding);
						//count the unsuccessful rule repair
						countRuleApplication(getRulesRevoked(), ruleBinding.getTggRule(), 1);
						
						//now immediately try to apply another rule at that position
						//FIXME: Currently, the new rule can only use all or parts of the previously used elements
						//       This should be extended to be able to use also the source elements not covered by other rule applications.
						if (usePartiallyReusablePatternSearch()) {
							for (Map.Entry<Node, EObject> nodeBinding : ruleBinding.getNodeToEObjectMap()) {
								if (nodeBinding.getKey().isProduced())
									getRuleProcessor().getGraphMatcher().getElementsNotAvailableForProducedNodes().remove(nodeBinding.getValue());
							}
							for (Map.Entry<Edge, EList<EObject>> edgeBinding : ruleBinding.getEdgeToVirtualModelEdgeMap()) {
								if (edgeBinding.getKey().isProduced())
									getRuleProcessor().getGraphMatcher().getLinksNotAvailableForProducedEdges().remove(edgeBinding.getValue());
							}
							getRuleProcessor().getGraphMatcher().setRestrictProducedGraphMatching(true);
							while(rulesApplicable() && !monitor.isCanceled()){
								RuleBinding reappliedRuleBinding = createInitialRuleBinding();
								if (getRuleProcessor().processRule(reappliedRuleBinding)){
									ruleApplicationPostProcessing(reappliedRuleBinding);
									//count the rule application
									countRuleApplication(getRulesApplied(), reappliedRuleBinding.getTggRule(), 1);
									break; //one new application only
								}else{
									ruleApplicationUnsuccessfulPostProcessing(reappliedRuleBinding);
								}
							}
							getRuleProcessor().getGraphMatcher().setRestrictProducedGraphMatching(false);
						}
					}
				}
			}
		}

		getRuleProcessor().getGraphMatcher().getElementsNotAvailableForProducedNodes().clear();
		getRuleProcessor().getGraphMatcher().getLinksNotAvailableForProducedEdges().clear();

		postFirstRuleBindingCheck();
		
		// main transformation loop
		while(rulesApplicable() && !monitor.isCanceled()){
			RuleBinding ruleBinding = createInitialRuleBinding();
			if (getRuleProcessor().processRule(ruleBinding)){
				
				//--DEBUG--//
				DebugSupport ds = this.getInterpreter().getDebugSupport();
				if(ds!=null)((DebugSupportImpl)ds).debugSupportForCurrentRuleBinding(ruleBinding);
								
				ruleApplicationPostProcessing(ruleBinding);
				//count the rule application
				countRuleApplication(getRulesApplied(), ruleBinding.getTggRule(), 1);
			}else{
				ruleApplicationUnsuccessfulPostProcessing(ruleBinding);
			}
		}

		// check global constraints on domain models.
		OCL ocl = getInterpreter().getOcl();
		EList<Status> subStatusList = new BasicEList<Status>();
		for (TypedModel typedModel : getInterpreter().getConfiguration()
				.getTripleGraphGrammar().getAllModelParameters()) {
			
			// 1. Validate OCL invariants
			HashMap<EClassifier, Collection<Constraint>> eClassifierToConstraintsMap = new HashMap<EClassifier, Collection<Constraint>>();
			if (ocl != null) {
				for (Constraint constraint : ocl.getConstraints()) {

					/*
					 * Also the def expressions are constraints. For some
					 * reason, the invariant expressions can be identified by
					 * their name. In contrast to the def expressions, the name
					 * is not empty.
					 */
					if (constraint.getName() != null
							&& constraint.getName() != "") {
						EClassifier contextClassifier = constraint
								.getSpecification().getContextVariable()
								.getType();
						if (eClassifierToConstraintsMap.get(contextClassifier) == null) {
							eClassifierToConstraintsMap.put(contextClassifier,
									new BasicEList<Constraint>());
						}
						eClassifierToConstraintsMap.get(contextClassifier).add(
								constraint);
					}
				}

				DomainModel domainModel = getInterpreter().getConfiguration()
						.getDomainModelByTypedModel().get(typedModel);
				for (EObject rootObject : domainModel.getRootObject()) {
					Iterator<EObject> allObjectsIterator = rootObject
							.eAllContents();
					while (allObjectsIterator.hasNext()) {
						EObject eObject = (EObject) allObjectsIterator.next();
						if (eClassifierToConstraintsMap.get(eObject.eClass()) != null) {
							for (Constraint constraint : eClassifierToConstraintsMap
									.get(eObject.eClass())) {
								boolean valid = ocl.check(eObject, constraint);
								logger.debug("Domain model constraint: \""
										+ constraint + "\" on object "
										+ eObject + " is valid: " + valid);
								if (!valid && domainModel.getShowDomainModelDiagnostic() != Severity.NONE) {
									subStatusList.add(new Status(Status.ERROR,
											InterpreterPlugin.getDefault()
													.getBundle()
													.getSymbolicName(),
											"DOMAIN MODEL INVALID (additional OCL invariant): The constraint \""
													+ constraint.getName()
													+ "\" is violated on " + eObject));
								}
							}
						}
					}
					
					// 2. Validate against meta-model (and constraints that may be defined within it)
					Diagnostic diagnostic = Diagnostician.INSTANCE.validate(rootObject);
					if (diagnostic.getSeverity() != Diagnostic.OK){
						for (Diagnostic childDiagnostic : diagnostic.getChildren()) {
							if (childDiagnostic.getSeverity() == Diagnostic.INFO && domainModel.getShowDomainModelDiagnostic().getValue() <= Severity.INFO_VALUE
									|| childDiagnostic.getSeverity() == Diagnostic.WARNING && domainModel.getShowDomainModelDiagnostic().getValue() <= Severity.WARNING_VALUE
									|| childDiagnostic.getSeverity() == Diagnostic.ERROR && domainModel.getShowDomainModelDiagnostic().getValue() <= Severity.ERROR_VALUE)
							subStatusList.add(new Status(childDiagnostic.getSeverity(),
									InterpreterPlugin.getDefault()
											.getBundle()
											.getSymbolicName(),
									"DOMAIN MODEL INVALID (meta-model diagnostic): \""
											+ childDiagnostic.getMessage()));
						}
					}
					// end validate against meta-model
				}
			}
		}
		
		
		// add transformation time to the return status
		InterpreterStatus returnStatus = (InterpreterStatus) transformationSuccessful();
		returnStatus.setRulesApplied(getRulesApplied());
		returnStatus.setRulesRevoked(getRulesRevoked());
		returnStatus.setRulesStillValid(getRulesStillValid());
		returnStatus.setUnsuccessfulRuleApplicationAttempts(getUnsuccessfulRuleApplicationAttempts());
		returnStatus.setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications);
		returnStatus.setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications);
		if (!subStatusList.isEmpty()){
			returnStatus.setSeverity(Status.ERROR);
			returnStatus.setMessage("Transformation finished. Some global constraints are violated! "
							+ "You can save and inspect the created models. Change the (source) models or the "
							+ "transformation to solve the problem or suppress the errors/warnings "
							+ "for a specific domain in the interpreter configuration.");
			for (Status status : subStatusList) {
				returnStatus.add(status);
			}
		}
		long transformationTime = System.currentTimeMillis()-time;
		returnStatus.setTransformationTime(transformationTime);
		if (logger.isDebugEnabled())
			logger.debug("Transformation performed in " + transformationTime + " ms.");
		return returnStatus;
	}

	protected void postFirstRuleBindingCheck() {
		// TODO Auto-generated method stub

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void ruleApplicationPostProcessing(RuleBinding ruleBinding) {
		
		//System.out.println("edges were explored before matching succeeded for TGG rule " + getRuleProcessor().getGraphMatcher().getEdgeExploreOperationsPerformed() + "  " + ruleBinding.getTggRule().getName() + " " + ruleBinding.getGraphElementStack().get(0));

		
		edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications += getRuleProcessor().getGraphMatcher().getEdgeExploreOperationsPerformed();

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean rulesApplicable() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleBinding createInitialRuleBinding() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IStatus transformationSuccessful() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void ruleApplicationUnsuccessfulPostProcessing(RuleBinding ruleBinding) {
		unsuccessfulRuleApplicationAttempts++;
		
		//System.out.println("edges were explored before matching -failed-  for TGG rule " + getRuleProcessor().getGraphMatcher().getEdgeExploreOperationsPerformed() + "  " + ruleBinding.getTggRule().getName() + " " + ruleBinding.getGraphElementStack().get(0));
		
		edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications += getRuleProcessor().getGraphMatcher().getEdgeExploreOperationsPerformed();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void ruleRepairUnsuccessful(RuleBinding ruleBinding) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int countRuleApplication(Map<TripleGraphGrammarRule, Integer> ruleMap, TripleGraphGrammarRule rule, int count) {
		if (!ruleMap.containsKey(rule)) {
			ruleMap.put(rule, 0);
		}
		Integer counter = ruleMap.get(rule);
		counter = counter + count;
		ruleMap.put(rule, counter);
		return counter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean usePartiallyReusablePatternSearch() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				if (ruleProcessor != null)
					msgs = ((InternalEObject)ruleProcessor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR, null, msgs);
				return basicSetRuleProcessor((RuleProcessor)otherEnd, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetInterpreter((Interpreter)otherEnd, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				if (helper != null)
					msgs = ((InternalEObject)helper).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER, null, msgs);
				return basicSetHelper((TransformationProcessorHelper)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING:
				return ((InternalEList<?>)getGlobalEdgeBinding()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING:
				return ((InternalEList<?>)getGlobalNodeBinding()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				return basicSetRuleProcessor(null, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				return basicSetInterpreter(null, msgs);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				return basicSetHelper(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				return eInternalContainer().eInverseRemove(this, InterpreterPackage.INTERPRETER__TRANSFORMATION_PROCESSOR, Interpreter.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING:
				if (coreType) return getGlobalEdgeBinding();
				else return getGlobalEdgeBinding().map();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING:
				if (coreType) return getGlobalNodeBinding();
				else return getGlobalNodeBinding().map();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				return getRuleProcessor();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				return getInterpreter();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR:
				return getProgressMonitor();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER:
				return getCrossReferenceAdapter();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				return getHelper();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_APPLIED:
				return getRulesApplied();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_REVOKED:
				return getRulesRevoked();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_STILL_VALID:
				return getRulesStillValid();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS:
				return getDeletedNodeBindings();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS:
				return getDeletedEdgeBindings();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS:
				return getUnsuccessfulRuleApplicationAttempts();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS:
				return getEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS:
				return getEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING:
				((EStructuralFeature.Setting)getGlobalEdgeBinding()).set(newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING:
				((EStructuralFeature.Setting)getGlobalNodeBinding()).set(newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				setInterpreter((Interpreter)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR:
				setProgressMonitor((IProgressMonitor)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER:
				setCrossReferenceAdapter((ECrossReferenceAdapter)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				setHelper((TransformationProcessorHelper)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_APPLIED:
				setRulesApplied((Map<TripleGraphGrammarRule, Integer>)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_REVOKED:
				setRulesRevoked((Map<TripleGraphGrammarRule, Integer>)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_STILL_VALID:
				setRulesStillValid((Map<TripleGraphGrammarRule, Integer>)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS:
				setDeletedNodeBindings((Map<EObject, Node>)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS:
				setDeletedEdgeBindings((Map<EList<EObject>, Edge>)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS:
				setUnsuccessfulRuleApplicationAttempts((Integer)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS:
				setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications((Integer)newValue);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS:
				setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING:
				getGlobalEdgeBinding().clear();
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING:
				getGlobalNodeBinding().clear();
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				setRuleProcessor((RuleProcessor)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				setInterpreter((Interpreter)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR:
				setProgressMonitor(PROGRESS_MONITOR_EDEFAULT);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER:
				setCrossReferenceAdapter(CROSS_REFERENCE_ADAPTER_EDEFAULT);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				setHelper((TransformationProcessorHelper)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_APPLIED:
				setRulesApplied((Map<TripleGraphGrammarRule, Integer>)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_REVOKED:
				setRulesRevoked((Map<TripleGraphGrammarRule, Integer>)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_STILL_VALID:
				setRulesStillValid((Map<TripleGraphGrammarRule, Integer>)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS:
				setDeletedNodeBindings((Map<EObject, Node>)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS:
				setDeletedEdgeBindings((Map<EList<EObject>, Edge>)null);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS:
				setUnsuccessfulRuleApplicationAttempts(UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS_EDEFAULT);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS:
				setEdgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications(EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT);
				return;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS:
				setEdgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications(EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_EDGE_BINDING:
				return globalEdgeBinding != null && !globalEdgeBinding.isEmpty();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__GLOBAL_NODE_BINDING:
				return globalNodeBinding != null && !globalNodeBinding.isEmpty();
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULE_PROCESSOR:
				return ruleProcessor != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__INTERPRETER:
				return getInterpreter() != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__PROGRESS_MONITOR:
				return PROGRESS_MONITOR_EDEFAULT == null ? progressMonitor != null : !PROGRESS_MONITOR_EDEFAULT.equals(progressMonitor);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__CROSS_REFERENCE_ADAPTER:
				return CROSS_REFERENCE_ADAPTER_EDEFAULT == null ? crossReferenceAdapter != null : !CROSS_REFERENCE_ADAPTER_EDEFAULT.equals(crossReferenceAdapter);
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__HELPER:
				return helper != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_APPLIED:
				return rulesApplied != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_REVOKED:
				return rulesRevoked != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__RULES_STILL_VALID:
				return rulesStillValid != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_NODE_BINDINGS:
				return deletedNodeBindings != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__DELETED_EDGE_BINDINGS:
				return deletedEdgeBindings != null;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS:
				return unsuccessfulRuleApplicationAttempts != UNSUCCESSFUL_RULE_APPLICATION_ATTEMPTS_EDEFAULT;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS:
				return edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications != EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT;
			case InterpreterPackage.TRANSFORMATION_PROCESSOR__EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS:
				return edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications != EDGE_EXPLORE_OPERATIONS_PERFORMED_LEADING_TO_UN_SUCCESSFUL_RULE_APPLICATIONS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (progressMonitor: ");
		result.append(progressMonitor);
		result.append(", crossReferenceAdapter: ");
		result.append(crossReferenceAdapter);
		result.append(", rulesApplied: ");
		result.append(rulesApplied);
		result.append(", rulesRevoked: ");
		result.append(rulesRevoked);
		result.append(", rulesStillValid: ");
		result.append(rulesStillValid);
		result.append(", deletedNodeBindings: ");
		result.append(deletedNodeBindings);
		result.append(", deletedEdgeBindings: ");
		result.append(deletedEdgeBindings);
		result.append(", unsuccessfulRuleApplicationAttempts: ");
		result.append(unsuccessfulRuleApplicationAttempts);
		result.append(", edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications: ");
		result.append(edgeExploreOperationsPerformedLeadingToSuccessfulRuleApplications);
		result.append(", edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications: ");
		result.append(edgeExploreOperationsPerformedLeadingToUnSuccessfulRuleApplications);
		result.append(')');
		return result.toString();
	}

	/**
	 * Checks all rule bindings if they are still valid. Each rule binding which
	 * is not valid any more must contain a changed Objects. All these objects
	 * are returned.
	 */
	public InterpreterStatus calculatePossiblyChangedObjects() {
		// FIXME RuleBindingList is newly calculated and so it change the order
		// in the rule binding container, it may can be fixed by a sorted list.
		EList<EObject> changedObjects = new UniqueEList<EObject>();

		// create rule processor
		if (getRuleProcessor() == null) {
			setRuleProcessor(InterpreterFactory.eINSTANCE.createRuleProcessor());
		}

		// set up RelevantReferencesCrossReferenceAdapter
		setCrossReferenceAdapter(
		// new ECrossReferenceAdapter());
		new RelevantReferencesCrossReferenceAdapter(getInterpreter().getConfiguration().getTripleGraphGrammar().getRelevantReferences()));
		for (EObject domainRooObject : getInterpreter().getConfiguration().getDomainRootObjects()) {
			domainRooObject.eAdapters().add(getCrossReferenceAdapter());
		}

		TGGRuleComparator tggRuleComparator = new TGGRuleComparator();
		setRulesApplied(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));
		setRulesRevoked(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));
		setRulesStillValid(new TreeMap<TripleGraphGrammarRule, Integer>(tggRuleComparator));

		// Initialize arrays to store elements marked for deletion
		setDeletedEdgeBindings(new HashMap<EList<EObject>, Edge>());
		setDeletedNodeBindings(new HashMap<EObject, Node>());

		// Throw away all old rule bindings. If we are updating incrementally,
		// we later restore every binding that is still valid.
		RuleBindingContainer oldRuleBindingContainer = getInterpreter().getConfiguration().getRuleBindingContainer();
		getInterpreter().getConfiguration().setRuleBindingContainer(InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer());

		RuleBinding[] allOldRuleBindings = new RuleBinding[0];
		allOldRuleBindings = oldRuleBindingContainer.getRuleBinding().toArray(allOldRuleBindings);
		for (RuleBinding ruleBinding : allOldRuleBindings) {
			// Some fields are not (or cannot be) serialized, thus these have to
			// be rebuild:
			// 0. remove invalid node bindings
			/*
			 * ArrayList<EObject> toBeDeleted = new ArrayList<EObject>(); for
			 * (Map.Entry<EObject, EList<Node>> nodeBinding :
			 * ruleBinding.getNodeBinding()) { if
			 * (nodeBinding.getKey().eIsProxy())
			 * toBeDeleted.add(nodeBinding.getKey()); }
			 * ruleBinding.getNodeBinding().removeAll(toBeDeleted);
			 */

			// 1. rebuild NodeToEObjectMap
			rebuildNodeToEObjectMap(ruleBinding);

			// 2. rebuild VirtualModelEdges and put them into EdgeBinding
			// and
			// VirtualModelEdgeMap
			rebuildVirtualModelEdgeMap(ruleBinding);
		}

		for (RuleBinding ruleBinding : allOldRuleBindings) {
			// check (in the order of applications) if rule application is still
			// consistent
			if (logger.isDebugEnabled())
				logger.debug("Checking rule application for rule " + ruleBinding.getTggRule().getName() + ".");
			if (getRuleProcessor().processRule(ruleBinding)) {
				// update front manager and global node binding.
				ruleApplicationPostProcessing(ruleBinding);
				// count the rule application
				countRuleApplication(getRulesStillValid(), ruleBinding.getTggRule(), 1);
			} else {
				if (logger.isDebugEnabled())
					logger.debug("Rule " + ruleBinding.getTggRule().getName() + " not applicable for rule binding " + ruleBinding.toString() + ".");
				for (Entry<Node, EObject> mapping : ruleBinding.getNodeToEObjectMap()) {
					if (mapping.getKey().isProduced()) {
						changedObjects.add(mapping.getValue());
					}
				}
			}
		}
		InterpreterStatus returnStatus = (InterpreterStatus) transformationSuccessful();
		returnStatus.setRulesApplied(getRulesApplied());
		returnStatus.setRulesRevoked(getRulesRevoked());
		returnStatus.setRulesStillValid(getRulesStillValid());
		returnStatus.setChangedObjects(changedObjects);
		getInterpreter().getConfiguration().getRuleBindingContainer().getRuleBinding().addAll(oldRuleBindingContainer.getRuleBinding());
		return returnStatus;
	}

	/**
	 * Rebuild VirtualModelEdges and put them into EdgeBinding and
	 * VirtualModelEdgeMap for the specific ruleBinding. It is used to for
	 * updates ( performTransformation() ) and getPossibleChangedObjects().
	 * 
	 * @param ruleBinding
	 */

	protected void rebuildVirtualModelEdgeMap(RuleBinding ruleBinding) {
		for (Map.Entry<Node, EObject> nodeBinding : ruleBinding.getNodeToEObjectMap()) {
			for (Edge edge : nodeBinding.getKey().getAllOutgoingEdges()) {
				EList<EObject> virtualModelEdge = new BasicEList.FastCompare<EObject>(3);
				virtualModelEdge.add(nodeBinding.getValue());
				virtualModelEdge.add(ruleBinding.getNodeToEObjectMap().get(edge.getTargetNode()));
				virtualModelEdge.add(edge.getTypeReference());

				if (ruleBinding.getEdgeBinding().get(virtualModelEdge) == null) {
					ruleBinding.getEdgeBinding().put(virtualModelEdge, new UniqueEList<Edge>());
				}
				ruleBinding.getEdgeBinding().get(virtualModelEdge).add(edge);
				ruleBinding.getEdgeToVirtualModelEdgeMap().put(edge, virtualModelEdge);

				// Add every binding to a temporary global edge binding
				// list.
				// This is to prevent that a new rule application steals a
				// binding
				// from an already existing, but not yet checked rule
				// application.
				if (usePartiallyReusablePatternSearch())
					getRuleProcessor().getGraphMatcher().getLinksNotAvailableForProducedEdges().add(virtualModelEdge);
			}
		}
	}

	/**
	 * Rebuild NodeToEObjectMap for the specific ruleBinding. It is used to for
	 * updates ( performTransformation() ) and getPossibleChangedObjects().
	 * 
	 * @param ruleBinding
	 */
	protected void rebuildNodeToEObjectMap(RuleBinding ruleBinding) {
		for (Map.Entry<EObject, EList<Node>> nodeBinding : ruleBinding.getNodeBinding()) {
			for (Node node : nodeBinding.getValue()) {
				EObject key = nodeBinding.getKey();
				/*
				 * if (nodeBinding.getKey().eIsProxy()) { DomainModel dommod
				 * = getInterpreter().getConfiguration().
				 * getDomainModelByTypedModel
				 * ().get(node.getDomainGraphPattern().getTypedModel()); key
				 * = EcoreUtil.resolve(nodeBinding.getKey(),
				 * dommod.eResource()); }
				 */
				ruleBinding.getNodeToEObjectMap().put(node, key);

				// Add every binding to a temporary global node binding
				// list.
				// This is to prevent that a new rule application steals a
				// binding
				// from an already existing, but not yet checked rule
				// application.
				if (usePartiallyReusablePatternSearch())
					getRuleProcessor().getGraphMatcher().getElementsNotAvailableForProducedNodes().add(nodeBinding.getKey());
			}
		}
	}

} //TransformationProcessorImpl
