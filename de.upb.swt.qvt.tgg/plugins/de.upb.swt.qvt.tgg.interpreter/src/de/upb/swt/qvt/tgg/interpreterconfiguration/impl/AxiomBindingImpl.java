/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Axiom Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreterconfiguration.impl.AxiomBindingImpl#getInitialAxiomNodeBinding <em>Initial Axiom Node Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AxiomBindingImpl extends RuleBindingImpl implements AxiomBinding {
	/**
	 * The cached value of the '{@link #getInitialAxiomNodeBinding() <em>Initial Axiom Node Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialAxiomNodeBinding()
	 * @generated
	 * @ordered
	 */
	protected EList<InitialAxiomNodeBinding> initialAxiomNodeBinding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AxiomBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterconfigurationPackage.Literals.AXIOM_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InitialAxiomNodeBinding> getInitialAxiomNodeBinding() {
		if (initialAxiomNodeBinding == null) {
			initialAxiomNodeBinding = new EObjectContainmentEList<InitialAxiomNodeBinding>(InitialAxiomNodeBinding.class, this, InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING);
		}
		return initialAxiomNodeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING:
				return ((InternalEList<?>)getInitialAxiomNodeBinding()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING:
				return getInitialAxiomNodeBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING:
				getInitialAxiomNodeBinding().clear();
				getInitialAxiomNodeBinding().addAll((Collection<? extends InitialAxiomNodeBinding>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING:
				getInitialAxiomNodeBinding().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterconfigurationPackage.AXIOM_BINDING__INITIAL_AXIOM_NODE_BINDING:
				return initialAxiomNodeBinding != null && !initialAxiomNodeBinding.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AxiomBindingImpl
