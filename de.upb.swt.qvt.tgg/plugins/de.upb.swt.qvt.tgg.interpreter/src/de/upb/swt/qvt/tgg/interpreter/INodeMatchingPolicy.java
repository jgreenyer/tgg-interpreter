/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INode Matching Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getINodeMatchingPolicy()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface INodeMatchingPolicy extends IMatchingPolicy {
	/**
	 * Returns the value of the '<em><b>Graph Matcher</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getNodeMatchingPolicy <em>Node Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph Matcher</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph Matcher</em>' reference.
	 * @see #setGraphMatcher(GraphMatcher)
	 * @see de.upb.swt.qvt.tgg.interpreter.InterpreterPackage#getINodeMatchingPolicy_GraphMatcher()
	 * @see de.upb.swt.qvt.tgg.interpreter.GraphMatcher#getNodeMatchingPolicy
	 * @model opposite="nodeMatchingPolicy" required="true"
	 * @generated
	 */
	GraphMatcher getGraphMatcher();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy#getGraphMatcher <em>Graph Matcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Matcher</em>' reference.
	 * @see #getGraphMatcher()
	 * @generated
	 */
	void setGraphMatcher(GraphMatcher value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method is responsible for checking matching constraints,
	 * a) a context node matches only already bound objects.
	 * b) a produced node matches only already unbound objects.
	 * c) gray node may be matched to bound or unbound objects.
	 * 
	 * Subclasses may extend this behavior to consider and evaluate further
	 * constraints.
	 * 
	 * NOTE that the graph matcher already checks type conformance.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean nodeMatches(Node node, EObject candidateEObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * chooses an outgoing edge based on some strategy.
	 * <!-- end-model-doc -->
	 * @model many="false"
	 * @generated
	 */
	EList<Edge> getUnboundEdgeToMatchNext(Node node, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model many="false"
	 * @generated
	 */
	EList<EObject> getCandidateNeighborObjectsForEdge(Node node, EObject candidateObject, Edge edge);

} // INodeMatchingPolicy
