/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.provider;


import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ApplicationScenarioItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationScenarioItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourceDomainModelPropertyDescriptor(object);
			addTargetDomainModelPropertyDescriptor(object);
			addCorrespondenceModelPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addModePropertyDescriptor(object);
			addSourceDomainObjectPropertyDescriptor(object);
			addCorrespondenceDomainObjectPropertyDescriptor(object);
			addTargetDomainObjectPropertyDescriptor(object);
			addSourceDomainNodePropertyDescriptor(object);
			addCorrespondenceDomainNodePropertyDescriptor(object);
			addTargetDomainNodesPropertyDescriptor(object);
			addDeactivateFrontPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source Domain Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceDomainModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_sourceDomainModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_sourceDomainModel_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__SOURCE_DOMAIN_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Domain Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetDomainModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_targetDomainModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_targetDomainModel_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__TARGET_DOMAIN_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Correspondence Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrespondenceModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_correspondenceModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_correspondenceModel_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__CORRESPONDENCE_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_name_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_mode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_mode_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__MODE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Domain Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceDomainObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_sourceDomainObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_sourceDomainObject_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__SOURCE_DOMAIN_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Correspondence Domain Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrespondenceDomainObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_correspondenceDomainObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_correspondenceDomainObject_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Domain Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetDomainObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_targetDomainObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_targetDomainObject_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__TARGET_DOMAIN_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Domain Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceDomainNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_sourceDomainNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_sourceDomainNode_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODE,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Correspondence Domain Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrespondenceDomainNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_correspondenceDomainNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_correspondenceDomainNode_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODE,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Domain Nodes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetDomainNodesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_targetDomainNodes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_targetDomainNodes_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__TARGET_DOMAIN_NODES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deactivate Front feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeactivateFrontPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ApplicationScenario_deactivateFront_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ApplicationScenario_deactivateFront_feature", "_UI_ApplicationScenario_type"),
				 InterpreterconfigurationPackage.Literals.APPLICATION_SCENARIO__DEACTIVATE_FRONT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns ApplicationScenario.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ApplicationScenario"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ApplicationScenario)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ApplicationScenario_type") :
			getString("_UI_ApplicationScenario_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ApplicationScenario.class)) {
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__NAME:
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__MODE:
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__SOURCE_DOMAIN_NODES_BY_TGG_RULE:
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__CORRESPONDENCE_DOMAIN_NODES_BY_TGG_RULE:
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__TARGET_DOMAIN_NODE_BY_TGG_RULE:
			case InterpreterconfigurationPackage.APPLICATION_SCENARIO__DEACTIVATE_FRONT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return InterpreterconfigurationEditPlugin.INSTANCE;
	}

}
