/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.provider;


import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigurationItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTripleGraphGrammarPropertyDescriptor(object);
			addRecordRuleBindingsPropertyDescriptor(object);
			addActiveApplicationScenarioPropertyDescriptor(object);
			addDomainRootObjectsPropertyDescriptor(object);
			addNoPackageUriRedirectionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Triple Graph Grammar feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTripleGraphGrammarPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_tripleGraphGrammar_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Configuration_tripleGraphGrammar_feature", "_UI_Configuration_type"),
				 InterpreterconfigurationPackage.Literals.CONFIGURATION__TRIPLE_GRAPH_GRAMMAR,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Record Rule Bindings feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRecordRuleBindingsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_recordRuleBindings_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Configuration_recordRuleBindings_feature", "_UI_Configuration_type"),
				 InterpreterconfigurationPackage.Literals.CONFIGURATION__RECORD_RULE_BINDINGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Active Application Scenario feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addActiveApplicationScenarioPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_activeApplicationScenario_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Configuration_activeApplicationScenario_feature", "_UI_Configuration_type"),
				 InterpreterconfigurationPackage.Literals.CONFIGURATION__ACTIVE_APPLICATION_SCENARIO,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Domain Root Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDomainRootObjectsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_domainRootObjects_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Configuration_domainRootObjects_feature", "_UI_Configuration_type"),
				 InterpreterconfigurationPackage.Literals.CONFIGURATION__DOMAIN_ROOT_OBJECTS,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the No Package Uri Redirection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNoPackageUriRedirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_noPackageUriRedirection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Configuration_noPackageUriRedirection_feature", "_UI_Configuration_type"),
				 InterpreterconfigurationPackage.Literals.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(InterpreterconfigurationPackage.Literals.CONFIGURATION__APPLICATION_SCENARIO);
			childrenFeatures.add(InterpreterconfigurationPackage.Literals.CONFIGURATION__DOMAIN_MODEL);
			childrenFeatures.add(InterpreterconfigurationPackage.Literals.CONFIGURATION__RULE_BINDING_CONTAINER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Configuration.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Configuration"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Configuration configuration = (Configuration)object;
		return getString("_UI_Configuration_type") + " " + configuration.isRecordRuleBindings();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Configuration.class)) {
			case InterpreterconfigurationPackage.CONFIGURATION__RECORD_RULE_BINDINGS:
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL_BY_TYPED_MODEL:
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_ROOT_OBJECTS:
			case InterpreterconfigurationPackage.CONFIGURATION__NO_PACKAGE_URI_REDIRECTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case InterpreterconfigurationPackage.CONFIGURATION__APPLICATION_SCENARIO:
			case InterpreterconfigurationPackage.CONFIGURATION__DOMAIN_MODEL:
			case InterpreterconfigurationPackage.CONFIGURATION__RULE_BINDING_CONTAINER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(InterpreterconfigurationPackage.Literals.CONFIGURATION__APPLICATION_SCENARIO,
				 InterpreterconfigurationFactory.eINSTANCE.createApplicationScenario()));

		newChildDescriptors.add
			(createChildParameter
				(InterpreterconfigurationPackage.Literals.CONFIGURATION__DOMAIN_MODEL,
				 InterpreterconfigurationFactory.eINSTANCE.createDomainModel()));

		newChildDescriptors.add
			(createChildParameter
				(InterpreterconfigurationPackage.Literals.CONFIGURATION__RULE_BINDING_CONTAINER,
				 InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return InterpreterconfigurationEditPlugin.INSTANCE;
	}

}
