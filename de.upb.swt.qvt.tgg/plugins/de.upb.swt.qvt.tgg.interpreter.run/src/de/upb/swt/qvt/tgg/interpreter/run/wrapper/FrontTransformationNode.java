package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;

public class FrontTransformationNode {
	
	public Node node;
	public EObject eObject;
	public RuleBindingWrapper rulebinding; 
	public boolean isRunning; 
	public boolean isBreakpointFound;
	
	public FrontTransformationNode(){
	}
}
