package de.upb.swt.qvt.tgg.interpreter.run.model;

import java.util.Dictionary;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.emf.common.util.URI;
import org.osgi.framework.Bundle;

import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGProcess;
import de.upb.swt.qvt.tgg.diagram.debug.DebugEventsListener;
import de.upb.swt.qvt.tgg.interpreter.DebugSupport;
import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterFactory;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.run.TGGDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.ui.popup.actions.StartInterpreterAction;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

public class TGGInterpreterProcess extends AbstractTGGProcess implements Runnable {

	private Interpreter interpreter;
	private IStatus resultStatus;
	private String transformationName = null;
	private Configuration configuration;

	public TGGInterpreterProcess(ILaunch launch) {
		super(launch);
		 //create the interpreter instance as early as possible, so we already have the DebugSupport
		interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
		interpreter.setDebugSupport(InterpreterFactory.eINSTANCE.createDebugSupport());
		interpreter.getDebugSupport().setRunning(true);		
	}

	@Override
	public String getLabel() {
		Bundle bundle = Platform.getBundle("de.upb.swt.qvt.tgg.interpreter");
		if (bundle == null)
			return super.getLabel();
		Dictionary<?,?> dict = bundle.getHeaders();
		Object name = dict.get("Bundle-Name");
		if (name instanceof String)
			return name + " (" + bundle.toString() + ")";
		else
			return bundle.toString(); 
	}

	public void run() {
		try {
			final String mode = getLaunch().getLaunchMode();
			final Configuration tggconfig = InterpreterconfigurationUtil.loadInterpreterConfiguration(URI.createFileURI(getAttribute(AbstractTGGProcess.ATTR_CONFIG_FILE)));
			String fTGGName = tggconfig.getTripleGraphGrammar().getName();
			transformationName = tggconfig.getActiveApplicationScenario().getName() + " on " + tggconfig.getTripleGraphGrammar().getName();
			Job fMainTask = new Job("TGG Transformation: " + fTGGName) {

				protected IStatus run(IProgressMonitor monitor) {
					resultStatus = null;
//					interpreter = InterpreterPackage.eINSTANCE
//							.getInterpreterFactory().createInterpreter();

					interpreter.setConfiguration(tggconfig);
					interpreter.initializeConfiguration();
					IStatus returnStatus;
					if (mode.equals(ILaunchManager.DEBUG_MODE)) {
						interpreter.getDebugSupport().getDebugEventListener().add(new TGGDebugEventListener(getDebugTarget()));
						interpreter.getDebugSupport().setDebug(true);
						interpreter.getDebugSupport().isTransformationRunning(tggconfig, true);
						
						DebugProvider.getDefault().getDiagramListeners().add(new DebugEventsListener());
					}
					returnStatus = interpreter.performTransformation(monitor);

					postTransformationFinished(returnStatus, interpreter);
					DebugEvent launchChanged = new DebugEvent(getLaunch(),
							DebugEvent.TERMINATE);
					DebugEvent processChanged = new DebugEvent(getLaunch()
							.getProcesses()[0], DebugEvent.TERMINATE);
					if (mode.equals(ILaunchManager.DEBUG_MODE)) {
						DebugEvent debugTargetChanged = new DebugEvent(
								getDebugTarget(), DebugEvent.TERMINATE);
						DebugEvent threadChanged;
						try {
							threadChanged = new DebugEvent(getLaunch()
									.getDebugTarget().getThreads()[0],
									DebugEvent.TERMINATE);
							DebugPlugin.getDefault().fireDebugEventSet(
									new DebugEvent[] { launchChanged,
											processChanged, debugTargetChanged,
											threadChanged });
						} catch (DebugException e) {
						}
					} else {
						DebugPlugin.getDefault().fireDebugEventSet(
								new DebugEvent[] { launchChanged,
										processChanged });
					}
					return returnStatus;
				}

			};
			DebugEvent launchChanged = new DebugEvent(getLaunch(),
					DebugEvent.CHANGE);
			if (mode.equals(ILaunchManager.DEBUG_MODE)) {
				DebugEvent debugTargetChanged = new DebugEvent(
						getDebugTarget(), DebugEvent.CHANGE);
				DebugEvent threadNew = new DebugEvent(getDebugTarget()
						.getThreads()[0], DebugEvent.CREATE);
				DebugPlugin.getDefault().fireDebugEventSet(
						new DebugEvent[] { launchChanged, debugTargetChanged,
								threadNew });
			} else {
				DebugPlugin.getDefault().fireDebugEventSet(
						new DebugEvent[] { launchChanged });
			}
			fMainTask.setUser(true);
			fMainTask.schedule();
		} catch (Exception e) {
		}
	}
	
	public String getTransformationName() {
		return transformationName;
	}

	public void postTransformationFinished(final IStatus resultStatus,
			final Interpreter interpreter) {
		this.resultStatus = resultStatus;
		if (resultStatus.getCode() != Status.OK) {
			return;
		}
		StartInterpreterAction.postTransformationFinished(resultStatus,interpreter);
		interpreter.getDebugSupport().isTransformationRunning(configuration, false);
		clearDiagram();
	}

	@Override
	public int getExitValue() throws DebugException {
		if (resultStatus == null)
			throw new DebugException(new Status(IStatus.ERROR, DebugPlugin.getUniqueIdentifier(), "TGG Interpreter Process has not yet terminated."));
		return resultStatus.getCode();
	}

	@Override
	public boolean isTerminated() {
		return !interpreter.getDebugSupport().isRunning();
	}

	@Override
	public void terminate() throws DebugException {
		interpreter.getDebugSupport().setTerminate(true);
	}
	
	public DebugSupport getDebugSupport() {
		return interpreter.getDebugSupport();
	}
}
