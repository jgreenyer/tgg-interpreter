package de.upb.swt.qvt.tgg.interpreter.run;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.internal.ui.SWTFactory;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.StringVariableSelectionDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;


@SuppressWarnings("restriction")
public class LaunchConfigurationTabMain extends AbstractLaunchConfigurationTab {

	protected static final int WIDGET_ACTIVE_OWN = 2;
	protected static final int WIDGET_ACTIVE_CONFIG = 1;
	
    private Button fUseConfigFile;
    private Text fConfigFile;
    private Button fUseOwnInput;
    private Text fTGGRuleset;
    private Button fWorkspaceBrowse1;
    private Button fFileBrowse1;
    private Button fVariables1;
    private Button fWorkspaceBrowse2;
    private Button fFileBrowse2;
    private Button fVariables2;
	
	
	private ModifyListener fBasicModifyListener = new ModifyListener() {
		public void modifyText(ModifyEvent evt) {
			updateLaunchConfigurationDialog();
		}
	};
    
	public void createControl(Composite parent) {		
		Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(getControl(), IConstants.LAUNCH_CONFIGURATION_DIALOG_MAIN_TAB);
		comp.setLayout(new GridLayout(2, true));
		comp.setFont(parent.getFont());
		createTransformationInputComponent(comp);
	}

    private void createTransformationInputComponent(Composite parent) {
        Group group = SWTFactory.createGroup(parent, Messages.MainTab_TransformationInput, 5, 2, GridData.FILL_HORIZONTAL);
        Composite comp = SWTFactory.createComposite(group, 5, 5, GridData.FILL_BOTH);
        GridLayout ld = (GridLayout)comp.getLayout();
        ld.marginWidth = 1;
        ld.marginHeight = 1;
        fUseConfigFile = createRadioButton(comp, Messages.MainTab_ConfigFile); 
        GridData gd = new GridData(SWT.BEGINNING, SWT.NORMAL, false, false);
        fUseConfigFile.setLayoutData(gd);
        fUseConfigFile.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                setActiveStateOfWidges(WIDGET_ACTIVE_CONFIG);
                updateLaunchConfigurationDialog();
            }
        });        
        fConfigFile = SWTFactory.createSingleText(comp, 4);
        fConfigFile.getAccessible().addAccessibleListener(new AccessibleAdapter() {
        	public void getName(AccessibleEvent e) {
        		e.result = Messages.MainTab_Placeholder;
        	}
        });
        fConfigFile.addModifyListener(fBasicModifyListener);
        Composite bcomp = SWTFactory.createComposite(comp, 3, 5, GridData.HORIZONTAL_ALIGN_END);
        ld = (GridLayout)bcomp.getLayout();
        ld.marginHeight = 1;
        ld.marginWidth = 0;
        fWorkspaceBrowse1 = createPushButton(bcomp, Messages.MainTab_ButtonWorkspace, null); 
        fWorkspaceBrowse1.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(getShell(), new WorkbenchLabelProvider(), new WorkbenchContentProvider());
                dialog.setTitle(Messages.MainTab_DialogWorkspaceTitle); 
                dialog.setMessage(Messages.MainTab_DialogWorkspaceMessage); 
                dialog.setInput(ResourcesPlugin.getWorkspace().getRoot()); 
                dialog.setComparator(new ResourceComparator(ResourceComparator.NAME));
                if (dialog.open() == IDialogConstants.OK_ID) {
                    IResource resource = (IResource) dialog.getFirstResult();
                    String arg = resource.getFullPath().toString();
                    String fileLoc = VariablesPlugin.getDefault().getStringVariableManager().generateVariableExpression("workspace_loc", arg); //$NON-NLS-1$
                    fConfigFile.setText(fileLoc);
                }
            }
        });
        fFileBrowse1 = createPushButton(bcomp, Messages.MainTab_ButtonFileSystem, null);
        fFileBrowse1.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                String filePath = fTGGRuleset.getText();
                FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
                filePath = dialog.open();
                if (filePath != null) {
                	fConfigFile.setText(filePath);
                }
            }
        });
        fVariables1 = createPushButton(bcomp, Messages.MainTab_ButtonVariables, null); 
        fVariables1.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                StringVariableSelectionDialog dialog = new StringVariableSelectionDialog(getShell());
				dialog.open();
				String variable = dialog.getVariableExpression();
				if (variable != null) {
					fConfigFile.insert(variable);
				}
            }
            public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        
        fUseOwnInput = createRadioButton(comp, Messages.MainTab_OwnInput); 
        fUseOwnInput.setLayoutData(new GridData(SWT.BEGINNING, SWT.NORMAL, false, false));
        fUseOwnInput.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                setActiveStateOfWidges(WIDGET_ACTIVE_OWN);
                updateLaunchConfigurationDialog();
            }
        });
        
        fTGGRuleset = SWTFactory.createSingleText(comp, 4);
        fTGGRuleset.getAccessible().addAccessibleListener(new AccessibleAdapter() {
        	public void getName(AccessibleEvent e) {
        		e.result = Messages.MainTab_Placeholder;
        	}
        });
        fTGGRuleset.addModifyListener(fBasicModifyListener);
        
        bcomp = SWTFactory.createComposite(comp, 3, 5, GridData.HORIZONTAL_ALIGN_END);
        ld = (GridLayout)bcomp.getLayout();
        ld.marginHeight = 1;
        ld.marginWidth = 0;
        fWorkspaceBrowse2 = createPushButton(bcomp, Messages.MainTab_ButtonWorkspace, null); 
        fWorkspaceBrowse2.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(getShell(), new WorkbenchLabelProvider(), new WorkbenchContentProvider());
                dialog.setTitle(Messages.MainTab_DialogWorkspaceTitle); 
                dialog.setMessage(Messages.MainTab_DialogWorkspaceMessage); 
                dialog.setInput(ResourcesPlugin.getWorkspace().getRoot()); 
                dialog.setComparator(new ResourceComparator(ResourceComparator.NAME));
                if (dialog.open() == IDialogConstants.OK_ID) {
                    IResource resource = (IResource) dialog.getFirstResult();
                    String arg = resource.getFullPath().toString();
                    String fileLoc = VariablesPlugin.getDefault().getStringVariableManager().generateVariableExpression("workspace_loc", arg); //$NON-NLS-1$
                    fTGGRuleset.setText(fileLoc);
                }
            }
        });
        fFileBrowse2 = createPushButton(bcomp, Messages.MainTab_ButtonFileSystem, null);
        fFileBrowse2.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                String filePath = fTGGRuleset.getText();
                FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
                filePath = dialog.open();
                if (filePath != null) {
                    fTGGRuleset.setText(filePath);
                }
            }
        });
        fVariables2 = createPushButton(bcomp, Messages.MainTab_ButtonVariables, null); 
        fVariables2.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                StringVariableSelectionDialog dialog = new StringVariableSelectionDialog(getShell());
				dialog.open();
				String variable = dialog.getVariableExpression();
				if (variable != null) {
					fTGGRuleset.insert(variable);
				}
            }
            public void widgetDefaultSelected(SelectionEvent e) {}
        });
    }

    private void setActiveStateOfWidges(int enable) {
        fConfigFile.setEnabled(false);
        fFileBrowse1.setEnabled(false);
        fWorkspaceBrowse1.setEnabled(false);
        fVariables1.setEnabled(false);

        fTGGRuleset.setEnabled(false);
        fFileBrowse2.setEnabled(false);
        fWorkspaceBrowse2.setEnabled(false);
        fVariables2.setEnabled(false);

        switch (enable) {
		case 1:
			fConfigFile.setEnabled(true);
	        fFileBrowse1.setEnabled(true);
	        fWorkspaceBrowse1.setEnabled(true);
	        fVariables1.setEnabled(true);
			break;

		case 2:
	    	fTGGRuleset.setEnabled(true);
	        fFileBrowse2.setEnabled(true);
	        fWorkspaceBrowse2.setEnabled(true);
	        fVariables2.setEnabled(true);
			break;
		}
    }
	
	public void initializeFrom(ILaunchConfiguration configuration) {
		updateConfigurationData(configuration);
	}
	
    private void updateConfigurationData(ILaunchConfiguration configuration) {
        try {
            boolean useConfigFile = configuration.getAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_USE_CONFIG_FILE, true);
            String configFile = configuration.getAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_CONFIG_FILE, "");
            String tggFile = configuration.getAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_TGG_FILE, "");
            fUseConfigFile.setSelection(useConfigFile);
            fUseOwnInput.setSelection(!useConfigFile);
            setActiveStateOfWidges((useConfigFile)?WIDGET_ACTIVE_CONFIG:WIDGET_ACTIVE_OWN);
            fConfigFile.setText(configFile);
            fTGGRuleset.setText(tggFile);
        } catch (CoreException e) {
        }
    }

	public boolean isValid(ILaunchConfiguration config) {
		setMessage(null);
		setErrorMessage(null);
		
		return validateRedirectFile();
	}
	
    private boolean validateRedirectFile() {
        if(fUseOwnInput.getSelection()) {
            int len = fTGGRuleset.getText().trim().length();
            if (len == 0) {
                setErrorMessage(Messages.MainTab_Error_NoRuleset); 
                return false;
            }
        } else {
        	if(fUseConfigFile.getSelection()) {
        		int len = fConfigFile.getText().trim().length();
        		if (len == 0) {
        			setErrorMessage(Messages.MainTab_Error_NoConfig); 
        			return false;
        		}
        	}
        }

        return true;
    }
    
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
		config.setContainer(null);
	}

	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		if (fUseConfigFile.getSelection()) {
		    configuration.setAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_USE_CONFIG_FILE, true);
		} else {
		    configuration.setAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_USE_CONFIG_FILE, false);
		}
	    configuration.setAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_CONFIG_FILE, fConfigFile.getText());
	    configuration.setAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_TGG_FILE, fTGGRuleset.getText());	    
		configuration.setAttribute(DebugPlugin.ATTR_PROCESS_FACTORY_ID , "de.upb.swt.qvt.tgg.interpreter.run.TGGInterpreterProcessFactory");
	}

	public String getName() {
		return Messages.MainTab_Title; 
	}
	
	public String getId() {
		return TGGInterpreterRunPlugin.PLUGIN_ID + ".Main"; //$NON-NLS-1$
	}
	
	public boolean canSave() {
		return true;
	}

	public Image getImage() {
		return TGGInterpreterRunPlugin.getDefault().getImageRegistry().get(Images.IMG_MAIN_TAB);
	}
	
	public void activated(ILaunchConfigurationWorkingCopy workingCopy) {}

	public void deactivated(ILaunchConfigurationWorkingCopy workingCopy) {}

}
