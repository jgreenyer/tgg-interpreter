package de.upb.swt.qvt.tgg.interpreter.run.model;

import java.util.ArrayList;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGThread;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGValue;
import de.upb.swt.qvt.tgg.debug.manager.model.DebugConstants;
import de.upb.swt.qvt.tgg.debug.manager.model.TGGDebugElement;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.ConfigurationWrapper;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.RuleBindingWrapper;

public class TGGValue  extends AbstractTGGValue implements IValue {

	private String entry;
	private Object object;
	private AbstractTGGThread thread;
	private EObject eObject;
	private RuleBindingWrapper ruleBinding;
	private String ruleBindingObjectString;
	private ArrayList<TGGVariable> valueVariables = new ArrayList<TGGVariable>();
	
	public TGGValue(AbstractTGGThread thread, String entry, Object object){
		super(thread.getDebugTarget());
		this.entry = entry;
		this.object = object;
		this.thread = thread;
		makeNecessaryVariables(entry);
	}
	
	public TGGValue(AbstractTGGThread thread, String entry, RuleBindingWrapper ruleBinding){
		super(thread.getDebugTarget());
		this.entry = entry;
		this.ruleBinding = ruleBinding;
		this.thread = thread;
	}
	
	public TGGValue(AbstractTGGThread thread, String entry, String ruleBindingNodeString, String ruleBindingObjectString){
		super(thread.getDebugTarget());
		this.entry = entry;
		this.ruleBindingObjectString = ruleBindingObjectString;
	}
	
	public TGGValue(AbstractTGGThread thread, String entry, Node node, EObject eObject, RuleBindingWrapper ruleBinding){
		super(thread.getDebugTarget());
		this.entry = entry;
		this.eObject = eObject;
		this.thread = thread;
		this.ruleBinding = ruleBinding;
		makeNecessaryVariables(entry);
	}
	
	@Override
	public String getReferenceTypeName() throws DebugException {
		return "";
	}

	@Override
	public String getValueString() throws DebugException {
		if(entry.equals(DebugConstants.CONFIGURATION_VARIABLE)){
			return object.toString();
		} else if(entry.equals(DebugConstants.CANDIDATE_RULE)){
			return ((TripleGraphGrammarRule)object).getName();
		} else if(entry.equals(DebugConstants.NODE)){
			return this.eObject.toString();
		} else if(entry.equals(DebugConstants.APPLIED_RULES)){
			String appliedRulesCounter = ((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).getAppliedRulebindings().size() + "";
			return 	appliedRulesCounter;
		} else if(entry.equals(DebugConstants.ADD_APPLIED_RULE_TO_VALUE)){
			return "Nodes: " + this.ruleBinding.getNodeBinding().size() +" Edges: "+this.ruleBinding.getEdgeBinding().size();
		} else if(entry.equals(DebugConstants.RULEBINDINGNODE)){
			return this.ruleBindingObjectString;
		}else {
			return "";
		}
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		IVariable[] stackVariablesCopy = new IVariable[valueVariables.size()];
		for(int i = 0; i < valueVariables.toArray().length; i++){
			stackVariablesCopy[i] = (IVariable)valueVariables.toArray()[valueVariables.size()-1-i];
		}
		return stackVariablesCopy;
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return !valueVariables.isEmpty();
	}

	@Override
	public String getModelIdentifier() {
		return "";
	}
	
	@Override
	public boolean isAllocated() throws DebugException {
		return true;
	}
	
	private void makeNecessaryVariables(String identifer){
		if(identifer.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
			for(int i=0; i<((ConfigurationWrapper)object).getApplicationScenario().size(); i++){
				String location = ((ConfigurationWrapper)object).getApplicationScenario().get(i);
				valueVariables.add(new TGGVariable(thread, DebugConstants.CONFIGURATION_VARIABLE, new String(location), i));
			}
		} 
	}
	
	public void addAppliedRuleBinding(RuleBindingWrapper ruleBinding){
		valueVariables.add(new TGGVariable(thread, DebugConstants.ADD_APPLIED_RULE_TO_VALUE, ruleBinding));
	}
	
	public void addRuleBindingNode(String ruleBindingNodeString, String ruleBindingObjectString){
		valueVariables.add(new TGGVariable(thread, DebugConstants.RULEBINDINGNODE, ruleBindingNodeString, ruleBindingObjectString));
	}
	
	public void addNodeToValue(Node node, EObject eObject){
		TGGVariable deleteVariableItem = null;
		for(int i=0; i < valueVariables.size(); i++){
			Node node2 = valueVariables.get(i).getNode();
			XMLResource xmlResource= (XMLResource) node.eContainer().eContainer().eResource();
			XMLResource xmlResource2= (XMLResource) node2.eContainer().eContainer().eResource();																
																	
			if(xmlResource.getID(node).equals(xmlResource2.getID(node2))){
				deleteVariableItem = valueVariables.get(i);
				break;
			}			
		}
		if(deleteVariableItem != null){
			valueVariables.remove(deleteVariableItem);
			try {
				((TGGInterpreterThread)getDebugTarget().getThreads()[0]).decrementIndexOfNodes();
			} catch (DebugException e) {
			}
		}
		valueVariables.add(new TGGVariable(thread, DebugConstants.NODE, node, eObject));
	}
	
	public void addCandiateNodeToValue(Node node, EObject eObject){
		if(!valueVariables.isEmpty()){
			valueVariables.clear();
		}
		valueVariables.add(new TGGVariable(thread, DebugConstants.NODE, node, eObject));
	}
}
