package de.upb.swt.qvt.tgg.interpreter.run;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

public class LaunchConfigurationTabGroupMain extends
		AbstractLaunchConfigurationTabGroup {

	public LaunchConfigurationTabGroupMain() {
	}

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		ILaunchConfigurationTab[] tabs = 
			new ILaunchConfigurationTab[] {
				new LaunchConfigurationTabMain(),
				new CommonTab()
				};
		setTabs(tabs);
	}
}
