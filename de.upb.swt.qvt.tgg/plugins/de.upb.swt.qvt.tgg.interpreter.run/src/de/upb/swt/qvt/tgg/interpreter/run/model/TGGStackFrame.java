package de.upb.swt.qvt.tgg.interpreter.run.model;

import java.util.ArrayList;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGStackFrame;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGThread;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGVariable;
import de.upb.swt.qvt.tgg.debug.manager.model.DebugConstants;

public class TGGStackFrame extends AbstractTGGStackFrame {

	private AbstractTGGThread thread;
	private String entry;
	private Object object;
	private boolean isStepInto;
	private boolean isStepOver;
	private boolean isTerminated = false;
	private ArrayList<AbstractTGGVariable> stackVariables = new ArrayList<AbstractTGGVariable>();

	public TGGStackFrame(AbstractTGGThread thread, String entry, Object object){
		super(thread, entry, object);
		this.thread = thread;
		this.entry = entry;
		this.object = object;
		addStackVariables(entry);
	}
	
	@Override
	public IVariable[] getVariables() throws DebugException {
		IVariable[] stackVariablesCopy = new IVariable[stackVariables.size()];
		for(int i = 0; i < stackVariables.toArray().length; i++){
			stackVariablesCopy[i] = (IVariable)stackVariables.toArray()[stackVariables.size()-1-i];
		}
		return stackVariablesCopy;
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return !stackVariables.isEmpty();
	}
	
	@Override
	public String getName() throws DebugException {
		return entry;
	}
	
	private void addStackVariables(String identifer){
		if(identifer.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
			stackVariables.add(new TGGVariable(thread, entry, object));
		} else if(identifer.equals(DebugConstants.APPLY_RULE)){
			stackVariables.add(new TGGVariable(thread, DebugConstants.APPLIED_RULES, object));
			stackVariables.add(new TGGVariable(thread, DebugConstants.CANDIDATE_RULE, object));
		} else if(identifer.equals(DebugConstants.PATTERMATCHING)){
			stackVariables.add(new TGGVariable(thread, DebugConstants.CANDIDATE_NODE_BINDING, object));
			stackVariables.add(new TGGVariable(thread, DebugConstants.MATCHED_NODES, object));
		} else if(identifer.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
			stackVariables.add(new TGGVariable(thread, DebugConstants.ENFORCED_NODES, object));
		}
	}
	
	public TGGVariable getSpecificNodesVariable(String searchConstant){
		for(int i = 0 ;i < stackVariables.size() ;i++) {
			try {
				if(stackVariables.get(i).getName().equals(searchConstant)){
					return (TGGVariable)stackVariables.get(i);
				}
			} catch (DebugException e) {
			}
		}
		return null;
	}
	
	@Override
	public boolean canStepInto() {
		try {
			if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
				return (((TGGInterpreterThread)getThread()).getStackFrames().length == 1) ? true : false;		
			} else if(entry.equals(DebugConstants.APPLY_RULE)){
				return (((TGGInterpreterThread)getThread()).getStackFrames().length == 2) ? true : false;
			} else if(entry.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
				return (((TGGInterpreterThread)getThread()).getStackFrames().length == 3) ? true : false;
			} else if(entry.equals(DebugConstants.PATTERMATCHING)){
				return (((TGGInterpreterThread)getThread()).getStackFrames().length == 4) ? true : false;
			}
		} catch (DebugException e) {
		}
		return false;
	}

	@Override
	public boolean canStepOver() {
		return true;
	}

	@Override
	public boolean canStepReturn() {
		return false;
	}

	@Override
	public boolean isStepping() {
		return ((TGGInterpreterThread)getThread()).isStepping();
	}

	@Override
	public void stepInto() throws DebugException {
		((TGGInterpreterThread)getThread()).setStepping(true);
		isStepInto = true;
		if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.APPLY_RULE);
		} else if(entry.equals(DebugConstants.APPLY_RULE)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.MATCH_RULE_AND_EXECUTE);
		} else if(entry.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.NODE);
		} else if(entry.equals(DebugConstants.PATTERMATCHING)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.NODE);
		}
		((TGGInterpreterThread)getThread()).setClassInstanceNotify(TGGInterpreterThread.classInstanceWhichWaitsCurrently);
		getThread().stepInto();
	}

	@Override
	public void stepOver() throws DebugException {
		((TGGInterpreterThread)getThread()).setStepping(true);
		isStepOver = true;
		if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
			getThread().resume();
		} else if(entry.equals(DebugConstants.APPLY_RULE)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.APPLY_RULE);
		} else if(entry.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.MATCH_RULE_AND_EXECUTE);
		} else if(entry.equals(DebugConstants.PATTERMATCHING)){
			((TGGInterpreterThread)getThread()).setHasToStepOverPatternMatching(true);
			((TGGInterpreterThread)getThread()).setWaitingFor(DebugConstants.PATTERMATCHING);
		}
		((TGGInterpreterThread)getThread()).setClassInstanceNotify(TGGInterpreterThread.classInstanceWhichWaitsCurrently);
		getThread().stepOver();
	}

	@Override
	public void stepReturn() throws DebugException {
	}

	@Override
	public boolean canResume() {
		return true;
	}

	@Override
	public boolean canSuspend() {
		return false;
	}

	@Override
	public boolean isSuspended() {
		return false;
	}

	@Override
	public void resume() throws DebugException {
		((TGGInterpreterThread)getThread()).setStepping(false);
		getThread().resume();
	}

	@Override
	public void suspend() throws DebugException {
	}

	@Override
	public boolean canTerminate() {
		if(entry.equals(DebugConstants.PATTERMATCHING)){
			return false;
		} else {
			return true;
		}	
	}

	@Override
	public boolean isTerminated() {
		return isTerminated;
	}

	@Override
	public void terminate() throws DebugException {
		isTerminated = true;
		((TGGInterpreterThread)getThread()).terminate();
	}

	public void setStepInto(boolean isStepInto) {
		this.isStepInto = isStepInto;
	}

	public void setStepOver(boolean isStepOver) {
		this.isStepOver = isStepOver;
	}

	public boolean isStepInto() {
		return isStepInto;
	}

	public boolean isStepOver() {
		return isStepOver;
	}
}
