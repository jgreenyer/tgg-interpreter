package de.upb.swt.qvt.tgg.interpreter.run;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.ISourceLocator;

public class TGGLaunch extends Launch {

	public TGGLaunch(ILaunchConfiguration launchConfiguration, String mode, ISourceLocator locator) {
		super(launchConfiguration, mode, locator);
	}

	@Override
	public boolean isTerminated() {
		return super.isTerminated();
	}
}
