package de.upb.swt.qvt.tgg.interpreter.run.model;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGThread;
import de.upb.swt.qvt.tgg.debug.manager.model.DebugConstants;
import de.upb.swt.qvt.tgg.debug.manager.model.TGGNodeBreakpoint;
import de.upb.swt.qvt.tgg.debug.manager.model.TGGRuleBreakpoint;
import de.upb.swt.qvt.tgg.diagram.edit.parts.DomainGraphPatternEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ITGGNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditor;
import de.upb.swt.qvt.tgg.impl.TggFactoryImpl;
import de.upb.swt.qvt.tgg.impl.TripleGraphGrammarRuleImpl;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.FrontTransformationNode;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.RuleBindingWrapper;

public class TGGInterpreterThread extends AbstractTGGThread implements IThread {
	
	private ArrayList<RuleBindingWrapper> appliedRulebindings = new ArrayList<RuleBindingWrapper>();
	
	private static ShapeNodeEditPart lastNodePart;
	private static ShapeNodeEditPart currentNodePart;

	private FrontTransformationNode frontTransformationNode= null;
	private RuleBindingWrapper rulebindingwrapper;
	
	public TGGInterpreterThread(IDebugTarget target) {
		super(target);
	}

	public ArrayList<RuleBindingWrapper> getAppliedRulebindings() {
		return appliedRulebindings;
	}

	@Override
	public IBreakpoint[] getBreakpoints() {
		IBreakpoint[] breakpoints = DebugProvider.getDefault().getBreakpointManager().getBreakpoints();
		for(int i = 0; i < breakpoints.length; i++){
			if(breakpoints[i] instanceof TGGNodeBreakpoint || breakpoints[i] instanceof TGGRuleBreakpoint){
				breakpointList.add(breakpoints[i]);
			}
		}
		IBreakpoint[] breakpointscopy = new IBreakpoint[breakpointList.size()];
		for(int i = 0; i < breakpointList.toArray().length; i++){
			breakpointscopy[i] = (IBreakpoint)breakpointList.toArray()[breakpointList.size()-1-i];
		}
		return breakpointscopy;	
	}
	
	public void addEnforceNode(Node node, EObject eObject, RuleBindingWrapper rulebinding, Object classInstance, boolean isRunning, boolean isBreakpointFound) {
		try {
			boolean goOut = false;
			boolean isTGGDiagrammEditorFound = false;
			if(rulebindingwrapper != null){				
				if(rulebindingwrapper.getUID().equals(rulebinding.getUID())){
					indexOfNodes++;
				} else {
					indexOfNodes = 0;
				}
			} 
			TripleGraphGrammarRule ruleOfOpenEditor;
			String nameOfRuleOfOpenEditor;
			for(int i = 0 ;i < stackList.size() ;i++) {
					if(stackList.get(i).getName().equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
						((TGGValue)stackList.get(i).getSpecificNodesVariable(DebugConstants.ENFORCED_NODES).getValue()).addNodeToValue(node, eObject);
						changeDebugEvent(stackList.get(i));
						if(isStepping || isBreakpointFound){
							IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
							if(windows.length < 1){
								showErrorInConsole();
							}
							for(int u=0; u < windows.length; u++){
								if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
									isTGGDiagrammEditorFound = true;
									TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
									target.getRootEditpart();
									for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
										EditPart part = (EditPart)j.next();
										if(part instanceof TripleGraphGrammarRuleEditPart){
											DiagramImpl diagramImpl = (DiagramImpl)((TripleGraphGrammarRuleEditPart)part).getModel();
											ruleOfOpenEditor = (TripleGraphGrammarRuleImpl)diagramImpl.getElement();
											nameOfRuleOfOpenEditor = ruleOfOpenEditor.getName();
											TGGStackFrame tggApplyRuleStackframe = null;
											for(int a = 0; a < stackList.size(); a++){
												if(stackList.get(a).getName().equals(DebugConstants.APPLY_RULE)){
													tggApplyRuleStackframe = (TGGStackFrame) stackList.get(a);
													break;
												}
											}
											String currentRuleOfDebugger = ((TripleGraphGrammarRule)tggApplyRuleStackframe.getObject()).getName();
											if(currentRuleOfDebugger.equals(nameOfRuleOfOpenEditor)){
												TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
												Iterator k =rulepart.getChildren().iterator();
												while(k.hasNext() && !goOut){
													EditPart partOfRule = (EditPart)k.next();
													if(partOfRule instanceof ITGGNodeEditPart){
														ShapeNodeEditPart nodepart = (ShapeNodeEditPart)partOfRule;													
														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
																																			
														XMLResource xmlResource= (XMLResource) tggNodeOfGraph.eContainer().eContainer().eResource();
														XMLResource xmlResource2= (XMLResource) node.eContainer().eContainer().eResource();																
																												
														if(xmlResource.getID(tggNodeOfGraph).equals(xmlResource2.getID(node))){
															if(lastNodePart != null){															
																DebugProvider.getDefault().changeFigure(TGGInterpreterThread.lastNodePart, false, -1);
															}
															currentNodePart = nodepart;//															
															DebugProvider.getDefault().changeFigure(nodepart, true, indexOfNodes);
															TGGInterpreterThread.lastNodePart = nodepart;
															goOut = true;																														
														}
													} else if(partOfRule instanceof DomainGraphPatternEditPart){
													}
												}
											} else {
												showOpenTheRightDiagram(currentRuleOfDebugger);
											}
										}
									}
								} 
								if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
									setClassInstanceWait(classInstance);
								} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
									if(DebugConstants.NODE.equals(getWaitingFor()) && classInstance != null){
										setClassInstanceWait(classInstance);
										refreshStackFrames(); 
									} 
								} 
							}
							if(!isTGGDiagrammEditorFound){					
								showErrorInConsole();
							}
						}
					}
			}
		} catch (DebugException e) {
		}
		if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
			setClassInstanceWait(classInstance);
		} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
			if(DebugConstants.MATCH_RULE_AND_EXECUTE.equals(getWaitingFor()) && classInstance != null && !hasToStepOverPatternMatching){
				setClassInstanceWait(classInstance);
				refreshStackFrames(); 
			}
		} 
	}
	

	
	public void addMatchedNodeForPatternMatching(Node node, EObject eObject, RuleBindingWrapper rulebinding, boolean isFrontTransformationNode, Object classInstance, boolean isRunning, boolean isBreakpointFound) {
		boolean goOut = false;
		boolean isTGGDiagrammEditorFound = false;
		TGGInterpreterThread.classInstanceForFrontTransformationNodeToWAIT = classInstance;
		if(isFrontTransformationNode){
			frontTransformationNode = new FrontTransformationNode();
			frontTransformationNode.eObject = eObject;
			frontTransformationNode.isBreakpointFound = isBreakpointFound;
			frontTransformationNode.isRunning = isRunning;
			frontTransformationNode.node = node;
			frontTransformationNode.rulebinding = rulebinding;
		} else {
			if(frontTransformationNode != null){
				Node copynode = frontTransformationNode.node;
				EObject copyeObject= frontTransformationNode.eObject; 
				RuleBindingWrapper copyrulebinding = frontTransformationNode.rulebinding; 
				Object copyclassInstance = classInstance; 
				boolean copyisRunning = frontTransformationNode.isRunning;
				boolean copyisBreakpointFound = isNodeABreakpoint(copynode);
				frontTransformationNode = null;
				addMatchedNodeForPatternMatching(copynode, copyeObject, copyrulebinding, false, copyclassInstance, copyisRunning, copyisBreakpointFound);
			}
			if(rulebindingwrapper != null){
				if(rulebindingwrapper.getUID().equals(rulebinding.getUID())){
					indexOfNodes++;
				}
			} else {
				indexOfNodes = 0;
			}
			TripleGraphGrammarRule ruleOfOpenEditor;
			String nameOfRuleOfOpenEditor;
			for(int i = 0 ;i < stackList.size() ;i++) {
				try {
					if(stackList.get(i).getName().equals(DebugConstants.PATTERMATCHING)){
						((TGGValue)stackList.get(i).getSpecificNodesVariable(DebugConstants.MATCHED_NODES).getValue()).addNodeToValue(node, eObject);
						changeDebugEvent(stackList.get(i));
						if(isStepping || isBreakpointFound){
							IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
							if(windows.length < 1){
								showErrorInConsole();
							}
							for(int u=0; u < windows.length; u++){
								if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
									isTGGDiagrammEditorFound = true;
									TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
									target.getRootEditpart();
									for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
										EditPart part = (EditPart)j.next();
										if(part instanceof TripleGraphGrammarRuleEditPart){
											DiagramImpl diagramImpl = (DiagramImpl)((TripleGraphGrammarRuleEditPart)part).getModel();
											ruleOfOpenEditor = (TripleGraphGrammarRuleImpl)diagramImpl.getElement();
											nameOfRuleOfOpenEditor = ruleOfOpenEditor.getName();
											TGGStackFrame tggApplyRuleStackframe = null;
											for(int a = 0; a < stackList.size(); a++){
												if(stackList.get(a).getName().equals(DebugConstants.APPLY_RULE)){
													tggApplyRuleStackframe = (TGGStackFrame) stackList.get(a);
													break;
												}
											}
											String currentRuleOfDebugger = ((TripleGraphGrammarRule)tggApplyRuleStackframe.getObject()).getName();
											if(currentRuleOfDebugger.equals(nameOfRuleOfOpenEditor)){
												TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
												Iterator k =rulepart.getChildren().iterator();
												while(k.hasNext() && !goOut){
													EditPart partOfRule = (EditPart)k.next();
													if(partOfRule instanceof ITGGNodeEditPart){
														ShapeNodeEditPart nodepart = (ShapeNodeEditPart)partOfRule;
														NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
														Node tggNodeOfGraph = (Node)nodeOfGraph.getElement();
						
														XMLResource xmlResource= (XMLResource) tggNodeOfGraph.eContainer().eContainer().eResource();
														XMLResource xmlResource2= (XMLResource) node.eContainer().eContainer().eResource();																
																												
														if(xmlResource.getID(tggNodeOfGraph).equals(xmlResource2.getID(node))){
															if(lastNodePart != null){															
																DebugProvider.getDefault().changeFigure(TGGInterpreterThread.lastNodePart, false, -1);
															}
															currentNodePart = nodepart;//															
															DebugProvider.getDefault().changeFigure(nodepart, true, indexOfNodes);
															TGGInterpreterThread.lastNodePart = nodepart;
															goOut = true;				
														}
													} else if(partOfRule instanceof DomainGraphPatternEditPart){
													}
												}
											} else {
												showOpenTheRightDiagram(currentRuleOfDebugger);
											}
										}
									}
								} 
								if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING)){// && !resume){
									setClassInstanceWait(classInstance);
								} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
									if(DebugConstants.NODE.equals(getWaitingFor()) && classInstance != null){
										setClassInstanceWait(classInstance);
										refreshStackFrames(); 
									} 
								} 
							}
							if(!isTGGDiagrammEditorFound){					
								showErrorInConsole();
							}
						}
					}
				} catch (DebugException e) {
				}
			}
			if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING) && !resume){
				setClassInstanceWait(classInstance);
			} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
				if(DebugConstants.PATTERMATCHING.equals(getWaitingFor()) && classInstance != null && !hasToStepOverPatternMatching){
					setClassInstanceWait(classInstance);
					refreshStackFrames(); 
				}
			} 
		}
		this.rulebindingwrapper = rulebinding;
	}
	
	

	
	public void addOrDeleteTGGInterpreterStackFrame(String entry, Object object, boolean isRunning, Object classInstance, boolean isBreakpointFound) {
		if (currentNodePart!=null)
			DebugProvider.getDefault().changeFigure(currentNodePart, false, -1);
			
		if(isRunning){
			DebugProvider.getDefault().clearDiagram();
			if(object instanceof TripleGraphGrammarRule){
				this.tripleGraphGrammarRule = (TripleGraphGrammarRule)object;
				indexOfNodes = 0;
			}
			if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
				isTransformationRunning = true;
			}
			TGGStackFrame newStackFrame = new TGGStackFrame(this, entry, object);
			stackList.add(newStackFrame);
			if(entry.equals(DebugConstants.APPLY_RULE)){
				for(int i = 0 ;i < appliedRulebindings.size() ;i++) {
					try {
						((TGGValue)newStackFrame.getSpecificNodesVariable(DebugConstants.APPLIED_RULES).getValue()).addAppliedRuleBinding(appliedRulebindings.get(i));
					} catch (DebugException e) {
					}
				}
			}
			changeDebugEvent(newStackFrame);
			if(isBreakpointFound && classInstance != null && getWaitingFor().equals(DebugConstants.NOTHING)){
				setClassInstanceWait(classInstance);
			} else if(!getWaitingFor().equals(DebugConstants.NOTHING)){
				if(entry.equals(getWaitingFor()) && classInstance != null){
					setClassInstanceWait(classInstance);
					refreshStackFrames(); 
				}
			} 
		} else if(deleteStackFrame(entry, object) != null) {
			if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
				isTransformationRunning = false;
			}
			if(entry.equals(DebugConstants.MATCH_RULE_AND_EXECUTE)){
				if(getWaitingFor().equals(DebugConstants.NODE)){
					setWaitingFor(DebugConstants.PATTERMATCHING);
				}
			}
			if(hasToStepOverPatternMatching){
				if(entry.equals(DebugConstants.PATTERMATCHING)){
					isTransformationRunning = false;
				}
			}
			if(entry.equals(DebugConstants.PATTERMATCHING) && frontTransformationNode != null){
				Node copynode = frontTransformationNode.node;
				EObject copyeObject= frontTransformationNode.eObject; 
				RuleBindingWrapper wrapper = frontTransformationNode.rulebinding;
				Object copyclassInstance = classInstance; 
				boolean copyisRunning = frontTransformationNode.isRunning;
				boolean copyisBreakpointFound = isNodeABreakpoint(copynode);
				frontTransformationNode = null;
				addMatchedNodeForPatternMatching(copynode, copyeObject, wrapper, false, copyclassInstance, copyisRunning, copyisBreakpointFound);
			}
			TGGStackFrame deleteStack = (TGGStackFrame) deleteStackFrame(entry, object);
			stackList.remove(deleteStack);
			changeDebugEvent(deleteStack);
		}
	}
	
	
	public void addAppliedRuleBinding(RuleBindingWrapper ruleBinding){
		appliedRulebindings.add(ruleBinding);
	}

}
