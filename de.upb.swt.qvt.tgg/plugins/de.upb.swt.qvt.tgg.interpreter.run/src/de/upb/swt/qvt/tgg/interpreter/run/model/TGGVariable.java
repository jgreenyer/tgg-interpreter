package de.upb.swt.qvt.tgg.interpreter.run.model;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGThread;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGVariable;
import de.upb.swt.qvt.tgg.debug.manager.model.DebugConstants;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.RuleBindingWrapper;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.RuleBindingWrapperEntry;

public class TGGVariable extends AbstractTGGVariable {

	private String entry;
	private TGGValue value = null;
	private int index;
	private Node node;
	private RuleBindingWrapper ruleBinding;
	private String ruleBindingNodeString;
	
	public TGGVariable(AbstractTGGThread thread, String entry, Object object){
		super(thread);
		this.entry = entry;
		value = new TGGValue(thread, entry, object);
	}
	
	public TGGVariable(AbstractTGGThread thread, String entry, String ruleBindingNodeString, String ruleBindingObjectString){
		super(thread);
		this.entry = entry;
		this.ruleBindingNodeString = ruleBindingNodeString;
		value = new TGGValue(thread, entry, ruleBindingNodeString, ruleBindingObjectString);
	}
	
	public TGGVariable(AbstractTGGThread thread, String entry, RuleBindingWrapper ruleBinding){
		super(thread);
		this.entry = entry;
		this.ruleBinding = ruleBinding;
		value = new TGGValue(thread, entry, ruleBinding);
		ArrayList<RuleBindingWrapperEntry> arrayList = ruleBinding.getNodeBinding();
		Iterator i = arrayList.iterator();
		while(i.hasNext()){
			RuleBindingWrapperEntry entryWrapper = (RuleBindingWrapperEntry)i.next();
			value.addRuleBindingNode(entryWrapper.getNodeLeftSide(), entryWrapper.getObjectRightSide());
		}
	}
	
	public TGGVariable(AbstractTGGThread thread, String entry, Node node, EObject eObject){
		super(thread);
		this.node = node;
		this.entry = entry;
		value = new TGGValue(thread, entry, node, eObject, ruleBinding);
	}
	
	public TGGVariable(AbstractTGGThread thread, String entry, Object object, int index){
		super(thread);
		this.entry = entry;
		this.index = index;
		value = new TGGValue(thread, entry, object);
	}
	
	@Override
	public String getName() throws DebugException {
		if(entry.equals(DebugConstants.EXECUTE_TRANSFORMATION)){
			return DebugConstants.TRANSFORMATION_PROPERTIES;
		} else if(entry.equals(DebugConstants.CONFIGURATION_VARIABLE)){
			if(index == 0){
				return "Source Model";
			} else if(index == 1){
				return "Correspondence Model";
			} else {
				return "Target Model";
			}
		} else if(entry.equals(DebugConstants.NODE)){
			if(node != null){
				if(node.getName() != null){
					return node.getName()+":"+node.getTypedName();
				} else {
					return ":"+node.getTypedName();
				}
			} else {
				return "";
			}
		} else if(entry.equals(DebugConstants.ADD_APPLIED_RULE_TO_VALUE)){
			return this.ruleBinding.getTggRule().getName();
		} else if(entry.equals(DebugConstants.RULEBINDINGNODE)){
			return this.ruleBindingNodeString;
		}else {
			return entry;
		}
	}
	
	@Override
	public IValue getValue() throws DebugException {
		return value;
	}

	public Node getNode() {
		return node;
	}
}
