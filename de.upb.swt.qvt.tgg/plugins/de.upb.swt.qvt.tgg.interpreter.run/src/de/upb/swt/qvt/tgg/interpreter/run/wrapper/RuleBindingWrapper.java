package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.impl.NodeImpl;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.impl.EObjectToNodeMapEntryImpl;

final public class RuleBindingWrapper implements IRuleBindingProvider {

	private RuleBinding ruleBinding;
	
	public RuleBindingWrapper(RuleBinding ruleBinding){
		this.ruleBinding = ruleBinding;
	}
	
	public RuleBindingWrapper(Object ruleBinding){
		if(ruleBinding instanceof RuleBinding){
			this.ruleBinding = (RuleBinding)ruleBinding;
		}
	}

	@Override
	public EMap<EList<EObject>, EList<Edge>> getEdgeBinding() {
		return ruleBinding.getEdgeBinding();
	}

	@Override
	public ArrayList<RuleBindingWrapperEntry> getNodeBinding() {
		String entryNode ="";
		EMap<EObject, EList<Node>> nodemap = ruleBinding.getNodeBinding();
		ArrayList<RuleBindingWrapperEntry> nodeArray = new ArrayList<RuleBindingWrapperEntry>();
		Iterator i = nodemap.iterator();
		while(i.hasNext()){
			Object key = i.next();
			if(key instanceof EObjectToNodeMapEntryImpl){
				EObjectToNodeMapEntryImpl entry = (EObjectToNodeMapEntryImpl)key;
				EObject eObject = entry.getKey();
				Iterator j =entry.getTypedValue().listIterator();
				while(j.hasNext()){
					Object keylist = j.next();
					if(keylist instanceof NodeImpl){
						NodeImpl nodeimpl = (NodeImpl)keylist;
						if(nodeimpl.getName() != null){
							entryNode = nodeimpl.getName()+":"+nodeimpl.getTypedName();
							nodeArray.add(new RuleBindingWrapperEntry(entryNode, eObject.toString()));
						} else {
							entryNode = ":"+nodeimpl.getTypedName();
							nodeArray.add(new RuleBindingWrapperEntry(entryNode, eObject.toString()));
						}
					}
				}
			}	
		}
		return nodeArray;
	}

	@Override
	public TripleGraphGrammarRule getTggRule() {
		return ruleBinding.getTggRule();
	}
	
	public boolean equals(RuleBindingWrapper wrapper){
		if(ruleBinding.equals(wrapper.getRuleBinding())){
			return true;
		}
		return false;
	}
	
	private RuleBinding getRuleBinding(){
		return ruleBinding;
	}
	
	public String getRuleBindingString(){
		return ruleBinding.getTggRule().getName();
	}

	@Override
	public String getUID() {
		return EcoreUtil.getIdentification(ruleBinding);
	}
}
