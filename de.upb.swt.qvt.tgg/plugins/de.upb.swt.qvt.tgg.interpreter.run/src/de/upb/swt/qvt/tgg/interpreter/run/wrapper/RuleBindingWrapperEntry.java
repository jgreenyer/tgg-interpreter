package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

public class RuleBindingWrapperEntry {

	private String nodeLeftSide;
	private String objectRightSide;
	
	public RuleBindingWrapperEntry(String nodeLeftSide, String objectRightSide){
		this.nodeLeftSide = nodeLeftSide;
		this.objectRightSide = objectRightSide;
	}

	public String getNodeLeftSide() {
		return nodeLeftSide;
	}

	public String getObjectRightSide() {
		return objectRightSide;
	}
}
