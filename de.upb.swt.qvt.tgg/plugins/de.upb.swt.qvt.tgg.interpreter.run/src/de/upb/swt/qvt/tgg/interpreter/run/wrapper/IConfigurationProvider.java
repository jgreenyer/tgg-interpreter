package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

import java.util.ArrayList;

public interface IConfigurationProvider {
	
	public ArrayList<String> getApplicationScenario();

}
