package de.upb.swt.qvt.tgg.interpreter.run;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.debug.manager.model.DebugConstants;
import de.upb.swt.qvt.tgg.interpreter.IDebugEventListener;
import de.upb.swt.qvt.tgg.interpreter.run.model.TGGInterpreterDebugTarget;
import de.upb.swt.qvt.tgg.interpreter.run.model.TGGInterpreterThread;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.ConfigurationWrapper;
import de.upb.swt.qvt.tgg.interpreter.run.wrapper.RuleBindingWrapper;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBinding;

public class TGGDebugEventListener implements IDebugEventListener {

	IDebugTarget target;
	
	public TGGDebugEventListener(IDebugTarget target) {
		this.target = target;
	}

	@Override
	public EClass eClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resource eResource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EObject eContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EStructuralFeature eContainingFeature() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EReference eContainmentFeature() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EList<EObject> eContents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeIterator<EObject> eAllContents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eIsProxy() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public EList<EObject> eCrossReferences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eGet(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eGet(EStructuralFeature feature, boolean resolve) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eSet(EStructuralFeature feature, Object newValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean eIsSet(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void eUnset(EStructuralFeature feature) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object eInvoke(EOperation operation, EList<?> arguments)
			throws InvocationTargetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EList<Adapter> eAdapters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eDeliver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void eSetDeliver(boolean deliver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eNotify(Notification notification) {
		// TODO Auto-generated method stub
		
	}

	
	//################################
	//# START Debug Listener Methods #
	//################################

//	/**
//	 * Checks whether a breakpoint was hit, then pausing the thread.
//	 */
//	private void checkForBreakpoints() {
//		boolean breakpointHit = false;
//		while (breakpointHit) {
//			try {
//				wait();
//			} catch (InterruptedException e) {
//			}
//		}
//	}
//
//	@Override
//	public void matchingCandidateObjectToNode(Node node, EObject object, RuleBinding ruleBinding, boolean successful) {
//		if(DebugProvider.getDefault().nodeContainsBreakpoint(node, ruleBinding.getTggRule()))
//		{			
//			try {
//				for(IThread thread : target.getThreads()){
//					((TGGInterpreterThread) thread).addMatchedNodeForPatternMatching(node, object, null, false, thread, true, true);
//				}
//			} catch (DebugException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	@Override
//	public void finishedMatchingRule(RuleBinding ruleBinding, boolean successful) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void startMatchingRule(RuleBinding ruleBinding) {
//		if(DebugProvider.getDefault().ruleContainsBreakpoint(ruleBinding.getTggRule()))
//			System.out.println("rule "+ruleBinding.getTggRule().getName());
//		
//	}
//
	@Override
	public void startTransformation(Configuration configuration) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedTransformation(Configuration configuration,
			boolean successful) {
		// TODO Auto-generated method stub
		
	}

	//################################
	//#  END Debug Listener Methods  #
	//################################
	
	@Override
	public void isApplyRuleRunning(TripleGraphGrammarRule tripleGraphGrammarRule, boolean isRunning, Object classInstance) {
		
		boolean isBreakpointFound = DebugProvider.getDefault().ruleContainsBreakpoint(tripleGraphGrammarRule);		
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addOrDeleteTGGInterpreterStackFrame(DebugConstants.APPLY_RULE, tripleGraphGrammarRule, isRunning, classInstance, isBreakpointFound);
	}
	
	@Override
	public void isMatchingRuleRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance) {
		RuleBindingWrapper ruleBindingWrapper = new RuleBindingWrapper(ruleBinding);		
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addOrDeleteTGGInterpreterStackFrame(DebugConstants.MATCH_RULE_AND_EXECUTE, ruleBindingWrapper, isRunning, classInstance, false);
	}
	
	@Override
	public void isPatternMatchingRunning(RuleBinding ruleBinding, boolean isRunning, Object classInstance) {
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addOrDeleteTGGInterpreterStackFrame(DebugConstants.PATTERMATCHING, ruleBinding, isRunning,classInstance, false);
	}

	@Override
	public void matchedNode(Node node, EObject eObject, RuleBinding ruleBinding, boolean isFrontTransformationNode, boolean isRunning, Object classInstance) {
		
		boolean isBreakpointFound = DebugProvider.getDefault().nodeContainsBreakpoint(node, ruleBinding.getTggRule());
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addMatchedNodeForPatternMatching(node, eObject, new RuleBindingWrapper(ruleBinding), isFrontTransformationNode, classInstance, isRunning, isBreakpointFound);
	}
	
	@Override
	public void candidateNode(Node node, EObject eObject, Object classInstance) {		
		DebugProvider.getDefault().getDebugTarget().getThread().addCandidateNodeForPatternMatching(node, eObject, classInstance);
	}

	@Override
	public void enforcedNode(Node node, EObject eObject, RuleBinding ruleBinding, Object classInstance, boolean isRunning) {
		
		boolean isBreakpointFound = DebugProvider.getDefault().nodeContainsBreakpoint(node, ruleBinding.getTggRule());
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addEnforceNode(node, eObject, new RuleBindingWrapper(ruleBinding), classInstance, isRunning, isBreakpointFound);		
	}
	
	@Override
	public void currentRuleBinding(RuleBinding ruleBinding){
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addAppliedRuleBinding(new RuleBindingWrapper(ruleBinding));
	}
	
	@Override
	public IDebugTarget getDebugTarget(ILaunch launch, IProcess process) {
		TGGInterpreterDebugTarget debugTarget = new TGGInterpreterDebugTarget(launch, process);
		DebugProvider.getDefault().setDebugTarget(debugTarget);
		return debugTarget;
	}
	
	@Override
	public void isTransformationRunning(Configuration configuration, boolean isRunning) {
		((TGGInterpreterThread) DebugProvider.getDefault().getDebugTarget().getThread()).addOrDeleteTGGInterpreterStackFrame(DebugConstants.EXECUTE_TRANSFORMATION, new ConfigurationWrapper(configuration), isRunning, null, false);
	}

}
