package de.upb.swt.qvt.tgg.interpreter.run;


public interface IConstants {
	
	public static final String PREFIX = TGGInterpreterRunPlugin.PLUGIN_ID + "."; 
	
	public static final String LAUNCH_CONFIGURATION_DIALOG_MAIN_TAB = PREFIX + "launch_configuration_dialog_main_tab"; 

	public static final String LAUNCH_CONFIGURATION_ATTR_USE_CONFIG_FILE = PREFIX + "ATTR_USE_CONFIG_FILE"; 
	
	public static final String LAUNCH_CONFIGURATION_ATTR_CONFIG_FILE = PREFIX + "ATTR_CONFIG_FILE"; 
	
	public static final String LAUNCH_CONFIGURATION_ATTR_TGG_FILE = PREFIX + "ATTR_TGG_FILE"; 
	
	public static final String LAUNCH_CONFIGURATION_ATTR_DIRECTION = PREFIX + "ATTR_DIRECTION"; 
}

