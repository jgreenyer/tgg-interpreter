package de.upb.swt.qvt.tgg.interpreter.run;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;

import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGDebugTarget;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGProcess;
import de.upb.swt.qvt.tgg.interpreter.run.model.TGGInterpreterDebugTarget;
import de.upb.swt.qvt.tgg.interpreter.run.model.TGGInterpreterProcess;

public class TGGLaunchConfigurationDelegate extends LaunchConfigurationDelegate {

	public TGGLaunchConfigurationDelegate() {
	}
	
	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {
		String configFile = configuration.getAttribute(IConstants.LAUNCH_CONFIGURATION_ATTR_CONFIG_FILE, (String)null);
		String filePath = VariablesPlugin.getDefault().getStringVariableManager().performStringSubstitution(configFile, false);			
		TGGInterpreterProcess process = new TGGInterpreterProcess(launch); 
		process.setAttribute(AbstractTGGProcess.ATTR_CONFIG_FILE, filePath); 
		launch.addProcess(process);
		IDebugTarget debugTarget = null;
		if (mode.equals(ILaunchManager.DEBUG_MODE)) { 
			debugTarget = new TGGInterpreterDebugTarget(launch, process);
			launch.addDebugTarget(debugTarget);
			DebugProvider.getDefault().setDebugTarget((AbstractTGGDebugTarget) debugTarget);
		}
		process.run(); 
	}

	
	@Override
	public ILaunch getLaunch(ILaunchConfiguration configuration, String mode)
			throws CoreException {
		return new TGGLaunch(configuration, mode, null);
	}
}
