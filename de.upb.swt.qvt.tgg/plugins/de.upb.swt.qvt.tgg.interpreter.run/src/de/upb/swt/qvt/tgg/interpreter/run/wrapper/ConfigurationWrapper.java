package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

import java.util.ArrayList;
import java.util.Iterator;


import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;

public class ConfigurationWrapper implements IConfigurationProvider {

	private Configuration configuration;
	
	public ConfigurationWrapper(Configuration configuration){
		this.configuration = configuration;
	}
	
	@Override
	public ArrayList<String> getApplicationScenario() {
		ArrayList<String> list = new  ArrayList<String>();
		if(configuration != null){
			Iterator<DomainModel> iterator = configuration.getDomainModel().iterator();
			while ( iterator.hasNext()){
				DomainModel dom = iterator.next();
				list.add(dom.getTypedModel().getUsedPackage().toString());
			}
		}	
		return list;
	}
	
	public boolean equals(ConfigurationWrapper wrapper){
		if(this.configuration.equals(wrapper.getConfiguration())){
			return true;
		}
		return false;
	}
	
	private Configuration getConfiguration(){
		return configuration;
	}

}
