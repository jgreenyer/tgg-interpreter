package de.upb.swt.qvt.tgg.interpreter.run.wrapper;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;

public interface IRuleBindingProvider {

	public TripleGraphGrammarRule getTggRule();
	
	public ArrayList<RuleBindingWrapperEntry> getNodeBinding();
	
	public EMap<EList<EObject>, EList<Edge>> getEdgeBinding();
	
	public String getUID();
}
