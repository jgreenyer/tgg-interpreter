package de.upb.swt.qvt.tgg.interpreter.run;

import java.lang.reflect.Field;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

public class Images {
	public static final String ICONS_PATH = "icons/"; 
	
	public static final String IMG_MAIN_TAB = ICONS_PATH + "tgg_small.gif"; 
	public static final String IMG_TGG = ICONS_PATH + "tgg.gif"; 
	
	public Images(Bundle bundle) {
		String content = "";
		for (Field field : this.getClass().getDeclaredFields()) {
			if (!field.getName().equals("ICONS_PATH") && field.getType().equals(String.class)) {
				try {
					content = (String) field.get(content);
				} catch (IllegalArgumentException e) {
				} catch (IllegalAccessException e) {
				}
				getImageRegistry().put(content, createImageDescriptor(bundle, content, true));
			}
		}
	}

	public Image get(String key) {
		return getImageRegistry().get(key);
	}
	
	
	public ImageDescriptor getDescriptor(String key) {
		return getImageRegistry().getDescriptor(key);
	}
	
	ImageRegistry getImageRegistry() {
		return TGGInterpreterRunPlugin.getDefault().getImageRegistry();
	}

	
	public ImageDescriptor createImageDescriptor(Bundle bundle, String fileName, boolean useMissingImageDescriptor) {
		URL url = bundle.getEntry(fileName);
		if (url != null) {
			return ImageDescriptor.createFromURL(url);
		}
		if (useMissingImageDescriptor) {
			return ImageDescriptor.getMissingImageDescriptor();
		}
		return null;
	}
}
