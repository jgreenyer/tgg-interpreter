package de.upb.swt.qvt.tgg.interpreter.run;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	
	public static String MainTab_Error_NoRuleset = null;
	
	public static String MainTab_Error_NoConfig = null;
	
	public static String MainTab_OwnInput = null;
	
	public static String MainTab_Title = null;
	
	public static String MainTab_TransformationInput = null;
	
	public static String MainTab_ConfigFile = null;
	
	public static String MainTab_ButtonWorkspace = null;
	
	public static String MainTab_ButtonFileSystem = null;
	
	public static String MainTab_ButtonVariables = null;
	
	public static String MainTab_DialogWorkspaceTitle = null;
	
	public static String MainTab_DialogWorkspaceMessage = null;
	
	public static String MainTab_Placeholder = null;

	static {NLS.initializeMessages("messages", Messages.class);}
}
