package de.upb.swt.qvt.tgg.interpreter.run.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;

import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGDebugTarget;
import de.upb.swt.qvt.tgg.debug.manager.model.AbstractTGGThread;

public class TGGInterpreterDebugTarget extends AbstractTGGDebugTarget {
	@Override
	public String getName() throws DebugException {
		String name = process.getTransformationName();
		return (name!=null)?name:super.getName();
	}

	protected TGGInterpreterProcess process;
	protected ILaunch launch;
	private AbstractTGGThread thread;

	public TGGInterpreterDebugTarget(ILaunch launch, IProcess process) {
		super(launch, process);
		this.process = (TGGInterpreterProcess) process;
		this.thread = new TGGInterpreterThread(this);
		this.process.getDebugSupport().setDebug(true);
	}

	@Override
	public IThread[] getThreads() throws DebugException {
		return new IThread[] {thread};
	}

	public AbstractTGGThread getThread(){
		return thread;
	}
}
