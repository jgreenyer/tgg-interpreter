/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreterconfiguration.presentation;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.StringTokenizer;
import java.util.Vector;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.ui.dialogs.WorkspaceResourceDialog;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ISetSelectionTarget;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.provider.InterpreterconfigurationEditPlugin;
import de.upb.swt.qvt.tgg.presentation.TggModelWizard;


/**
 * This is a simple wizard for creating a new model file.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterconfigurationModelWizard extends Wizard implements INewWizard {
	/**
	 * The supported extensions for created files.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<String> FILE_EXTENSIONS =
		Collections.unmodifiableList(Arrays.asList(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationEditorFilenameExtensions").split("\\s*,\\s*")));

	/**
	 * A formatted list of supported file extensions, suitable for display.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String FORMATTED_FILE_EXTENSIONS =
		InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationEditorFilenameExtensions").replaceAll("\\s*,\\s*", ", ");

	/**
	 * This caches an instance of the model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterconfigurationPackage interpreterconfigurationPackage = InterpreterconfigurationPackage.eINSTANCE;

	/**
	 * This caches an instance of the model factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterconfigurationFactory interpreterconfigurationFactory = interpreterconfigurationPackage.getInterpreterconfigurationFactory();

	/**
	 * This is the file creation page.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterconfigurationModelWizardNewFileCreationPage newFileCreationPage;

	/**
	 * This is the initial object creation page.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterconfigurationModelWizardInitialObjectCreationPage initialObjectCreationPage;

	/**
	 * Remember the selection during initialization for populating the default container.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IStructuredSelection selection;

	/**
	 * Remember the workbench during initialization.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IWorkbench workbench;

	/**
	 * Caches the names of the types that can be created as the root object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected List<String> initialObjectNames;

	private InterpreterconfigurationModelWizard_TGGSelectionPage tggSelectionPagePage;

	private List<InterpreterconfigurationModelWizard_ModelResourceSelectionPage> modelResourceSelectionPages = new Vector<InterpreterconfigurationModelWizard_ModelResourceSelectionPage>();

	private InterpreterconfigurationModelWizard_ModelResourceSelectionPage dummyModelResourceSelectionPage;

	private InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage applicationScenarioCreationPage;

	/**
	 * This just records the information.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		setWindowTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_Wizard_label"));
		setDefaultPageImageDescriptor(ExtendedImageRegistry.INSTANCE.getImageDescriptor(InterpreterconfigurationEditorPlugin.INSTANCE.getImage("full/wizban/NewInterpreterconfiguration")));
	}

	/**
	 * Returns the names of the types that can be created as the root object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<String> getInitialObjectNames() {
		if (initialObjectNames == null) {
			initialObjectNames = new ArrayList<String>();
			for (EClassifier eClassifier : interpreterconfigurationPackage.getEClassifiers()) {
				if (eClassifier instanceof EClass) {
					EClass eClass = (EClass)eClassifier;
					if (!eClass.isAbstract()) {
						initialObjectNames.add(eClass.getName());
					}
				}
			}
			Collections.sort(initialObjectNames, CommonPlugin.INSTANCE.getComparator());
		}
		return initialObjectNames;
	}

	/**
	 * Create a new model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected EObject createInitialModel() {
		Configuration configuration = interpreterconfigurationFactory.createConfiguration();
		configuration.setTripleGraphGrammar(tggSelectionPagePage.getTripleGraphGrammar());

		ApplicationScenario appScenario = interpreterconfigurationFactory.createApplicationScenario();
		appScenario.setName(applicationScenarioCreationPage.getApplicationScenarioName());
		appScenario.setMode(ApplicationMode.get(applicationScenarioCreationPage.getMode()));
		
		for (int i = 0; i < modelResourceSelectionPages.size(); i++) {
			InterpreterconfigurationModelWizard_ModelResourceSelectionPage mrsp = modelResourceSelectionPages.get(i);
			DomainModel domainModel = interpreterconfigurationFactory.createDomainModel();
			domainModel.setTypedModel(mrsp.getTypedModel());
			if (mrsp.getFileName() != null)
				domainModel.getResourceURI().add(mrsp.getFileName());
			if (mrsp.getRootObject() != null)
				domainModel.getRootObject().add(mrsp.getRootObject());
			
			configuration.getDomainModel().add(domainModel);

			int assignment = applicationScenarioCreationPage.getDomainAssignment(i);
			switch (assignment) {
			case InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage.SELECTION_SOURCE:
				appScenario.getSourceDomainModel().add(domainModel);
				break;
			case InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage.SELECTION_TARGET:
				appScenario.getTargetDomainModel().add(domainModel);
				break;
			case InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage.SELECTION_CORRESPONDENCE:
				appScenario.getCorrespondenceModel().add(domainModel);
				break;

			default:
				break;
			}
		}

		configuration.getApplicationScenario().add(appScenario);
		configuration.setActiveApplicationScenario(appScenario);
		
		return configuration;
	}

	/**
	 * Do the work after everything is specified.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean performFinish() {
		try {
			// Remember the file.
			//
			final IFile modelFile = getModelFile();

			// Do the work within an operation.
			//
			WorkspaceModifyOperation operation =
				new WorkspaceModifyOperation() {
					@Override
					protected void execute(IProgressMonitor progressMonitor) {
						try {
							// Create a resource set
							//
							ResourceSet resourceSet = new ResourceSetImpl();

							// Get the URI of the model file.
							//
							URI fileURI = URI.createPlatformResourceURI(modelFile.getFullPath().toString(), true);

							// Create a resource for this file.
							//
							Resource resource = resourceSet.createResource(fileURI);

							// Add the initial model object to the contents.
							//
							EObject rootObject = createInitialModel();
							if (rootObject != null) {
								resource.getContents().add(rootObject);
							}

							// Save the contents of the resource to the file system.
							//
							Map<Object, Object> options = new HashMap<Object, Object>();
							options.put(XMLResource.OPTION_ENCODING, "UTF-8");
							resource.save(options);
						}
						catch (Exception exception) {
							InterpreterconfigurationEditorPlugin.INSTANCE.log(exception);
						}
						finally {
							progressMonitor.done();
						}
					}
				};

			getContainer().run(false, false, operation);

			// Select the new file resource in the current view.
			//
			IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
			IWorkbenchPage page = workbenchWindow.getActivePage();
			final IWorkbenchPart activePart = page.getActivePart();
			if (activePart instanceof ISetSelectionTarget) {
				final ISelection targetSelection = new StructuredSelection(modelFile);
				getShell().getDisplay().asyncExec
					(new Runnable() {
						 public void run() {
							 ((ISetSelectionTarget)activePart).selectReveal(targetSelection);
						 }
					 });
			}

			// Open an editor on the new file.
			//
			try {
				page.openEditor
					(new FileEditorInput(modelFile),
					 workbench.getEditorRegistry().getDefaultEditor(modelFile.getFullPath().toString()).getId());
			}
			catch (PartInitException exception) {
				MessageDialog.openError(workbenchWindow.getShell(), InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_OpenEditorError_label"), exception.getMessage());
				return false;
			}

			return true;
		}
		catch (Exception exception) {
			InterpreterconfigurationEditorPlugin.INSTANCE.log(exception);
			return false;
		}
	}

	/**
	 * This is the one page of the wizard.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public class InterpreterconfigurationModelWizardNewFileCreationPage extends WizardNewFileCreationPage {
		/**
		 * Pass in the selection.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public InterpreterconfigurationModelWizardNewFileCreationPage(String pageId, IStructuredSelection selection) {
			super(pageId, selection);
		}

		/**
		 * The framework calls this to see if the file is correct.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		@Override
		protected boolean validatePage() {
			if (super.validatePage()) {
				String extension = new Path(getFileName()).getFileExtension();
				if (extension == null || !FILE_EXTENSIONS.contains(extension)) {
					String key = FILE_EXTENSIONS.size() > 1 ? "_WARN_FilenameExtensions" : "_WARN_FilenameExtension";
					setErrorMessage(InterpreterconfigurationEditorPlugin.INSTANCE.getString(key, new Object [] { FORMATTED_FILE_EXTENSIONS }));
					return false;
				}
				return true;
			}
			return false;
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public IFile getModelFile() {
			return ResourcesPlugin.getWorkspace().getRoot().getFile(getContainerFullPath().append(getFileName()));
		}
	}

	/**
	 * This is the page where the type of object to create is selected.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public class InterpreterconfigurationModelWizardInitialObjectCreationPage extends WizardPage {
		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected Combo initialObjectField;

		/**
		 * @generated
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 */
		protected List<String> encodings;

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected Combo encodingField;

		/**
		 * Pass in the selection.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public InterpreterconfigurationModelWizardInitialObjectCreationPage(String pageId) {
			super(pageId);
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public void createControl(Composite parent) {
			Composite composite = new Composite(parent, SWT.NONE); {
				GridLayout layout = new GridLayout();
				layout.numColumns = 1;
				layout.verticalSpacing = 12;
				composite.setLayout(layout);

				GridData data = new GridData();
				data.verticalAlignment = GridData.FILL;
				data.grabExcessVerticalSpace = true;
				data.horizontalAlignment = GridData.FILL;
				composite.setLayoutData(data);
			}

			Label containerLabel = new Label(composite, SWT.LEFT);
			{
				containerLabel.setText(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_ModelObject"));

				GridData data = new GridData();
				data.horizontalAlignment = GridData.FILL;
				containerLabel.setLayoutData(data);
			}

			initialObjectField = new Combo(composite, SWT.BORDER);
			{
				GridData data = new GridData();
				data.horizontalAlignment = GridData.FILL;
				data.grabExcessHorizontalSpace = true;
				initialObjectField.setLayoutData(data);
			}

			for (String objectName : getInitialObjectNames()) {
				initialObjectField.add(getLabel(objectName));
			}

			if (initialObjectField.getItemCount() == 1) {
				initialObjectField.select(0);
			}
			initialObjectField.addModifyListener(validator);

			Label encodingLabel = new Label(composite, SWT.LEFT);
			{
				encodingLabel.setText(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_XMLEncoding"));

				GridData data = new GridData();
				data.horizontalAlignment = GridData.FILL;
				encodingLabel.setLayoutData(data);
			}
			encodingField = new Combo(composite, SWT.BORDER);
			{
				GridData data = new GridData();
				data.horizontalAlignment = GridData.FILL;
				data.grabExcessHorizontalSpace = true;
				encodingField.setLayoutData(data);
			}

			for (String encoding : getEncodings()) {
				encodingField.add(encoding);
			}

			encodingField.select(0);
			encodingField.addModifyListener(validator);

			setPageComplete(validatePage());
			setControl(composite);
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected ModifyListener validator =
			new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					setPageComplete(validatePage());
				}
			};

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected boolean validatePage() {
			return getInitialObjectName() != null && getEncodings().contains(encodingField.getText());
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		@Override
		public void setVisible(boolean visible) {
			super.setVisible(visible);
			if (visible) {
				if (initialObjectField.getItemCount() == 1) {
					initialObjectField.clearSelection();
					encodingField.setFocus();
				}
				else {
					encodingField.clearSelection();
					initialObjectField.setFocus();
				}
			}
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public String getInitialObjectName() {
			String label = initialObjectField.getText();

			for (String name : getInitialObjectNames()) {
				if (getLabel(name).equals(label)) {
					return name;
				}
			}
			return null;
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public String getEncoding() {
			return encodingField.getText();
		}

		/**
		 * Returns the label for the specified type name.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected String getLabel(String typeName) {
			try {
				return InterpreterconfigurationEditPlugin.INSTANCE.getString("_UI_" + typeName + "_type");
			}
			catch(MissingResourceException mre) {
				InterpreterconfigurationEditorPlugin.INSTANCE.log(mre);
			}
			return typeName;
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		protected Collection<String> getEncodings() {
			if (encodings == null) {
				encodings = new ArrayList<String>();
				for (StringTokenizer stringTokenizer = new StringTokenizer(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_XMLEncodingChoices")); stringTokenizer.hasMoreTokens(); ) {
					encodings.add(stringTokenizer.nextToken());
				}
			}
			return encodings;
		}
	}
	
	public class InterpreterconfigurationModelWizard_TGGSelectionPage extends WizardPage {
		
		private IFile tggFile;
		private TripleGraphGrammar tripleGraphGrammar;
		
		private Text fileText;
		private org.eclipse.swt.widgets.List infoList;

		protected InterpreterconfigurationModelWizard_TGGSelectionPage(String pageName) {
			super(pageName);
		}


		public void createControl(Composite parent) {
			Composite container = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			container.setLayout(layout);
			layout.numColumns = 3;
			layout.verticalSpacing = 9;
			Label label = new Label(container, SWT.NULL);
			label.setText("&Container:");

			fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			fileText.setLayoutData(gd);
			fileText.setEditable(false);
			fileText.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					dialogChanged();
				}
			});
			
			

			Button button = new Button(container, SWT.PUSH);
			button.setText("Browse...");
			button.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					handleBrowse();
				}
			});
			
			GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
			gd1.horizontalSpan = 3;
			Label infoLabel = new Label(container, SWT.NULL);
			infoLabel.setLayoutData(gd1);
			infoLabel.setText("Typed Models declared by the Triple Graph Grammar:");
			GridData gd2 = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
			gd2.horizontalSpan = 3;
			gd2.grabExcessHorizontalSpace = true;
			gd2.grabExcessVerticalSpace = true;
			infoList = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL);
			infoList.setLayoutData(gd2);

			setPageComplete(false);
			//dialogChanged();
			setControl(container);

		}

		protected String[] filterExtensions;

		protected String[] getFilterExtensions()
		{
			if (filterExtensions == null)
			{
				List<String> fileExtensions = TggModelWizard.FILE_EXTENSIONS;
				if (fileExtensions.isEmpty())
				{
					filterExtensions = new String []{ "*.*" };
				}
				else if (fileExtensions.size() == 1)
				{
					filterExtensions = new String[]{"*." + fileExtensions.get(0)};
				}
				else
				{
					StringBuffer allFilterExtensions = new StringBuffer();
					String[] extensions = new String [fileExtensions.size() + 1];
					for (int i = 1, lenght=extensions.length; i < lenght; i++)
					{
						extensions[i] = "*." + fileExtensions.get(i-1);
						allFilterExtensions.append(";").append(extensions[i]);
					}
					allFilterExtensions.deleteCharAt(0);
					extensions[0] = allFilterExtensions.toString();
					filterExtensions = extensions;
				}
			}
			return filterExtensions;
		}

		protected boolean isValidWorkspaceResource(IResource resource)
		{
			if (resource.getType() == IResource.FILE) 
			{
				String[] filterExtensions = getFilterExtensions();
				if (filterExtensions.length > 0)
				{
					for (int i = 0; i < filterExtensions.length; i++)
					{
						if (filterExtensions[i].endsWith(".*") || filterExtensions[i].endsWith("." + resource.getFileExtension()))
						{
							return true;
						}
					}
				}

			}
			return false;
		}

		
		/**
		 * Uses the standard container selection dialog to choose the new value for
		 * the container field.
		 */
		private void handleBrowse() {
			ViewerFilter extensionFilter = new ViewerFilter() {
				@Override
				public boolean select(Viewer viewer, Object parentElement, Object element)
				{
					return !(element instanceof IFile) || TggModelWizard.FILE_EXTENSIONS.contains(((IFile)element).getFileExtension());
				}
			};

			IFile[] files = WorkspaceResourceDialog.openFileSelection(getShell(), "Select TGG", "Select the TGG ruleset file:", false, null, extensionFilter == null ? null : Collections.singletonList(extensionFilter));
			if (files.length == 1)
			{
				if (isValidWorkspaceResource(files[0]))
				{
					fileText.setText(files[0].getFullPath().toString());
					tggFile = files[0];
				}
			}

			return;
		}

		private void updateStatus(String message) {
			setErrorMessage(message);
			setPageComplete(message == null);
		}


		/**
		 * Ensures that both text fields are set.
		 */

		private void dialogChanged() {
			
			String fileName = getFileName();

			if (fileName.length() == 0) {
				updateStatus("File must be specified");
				return;
			}

			try {
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource interpreterConfigurationResource = resourceSet.getResource(URI.createPlatformResourceURI(fileName, true), true);

				if (!(interpreterConfigurationResource.getContents().get(0) instanceof TripleGraphGrammar)){
					tggFile = null;
					tripleGraphGrammar = null;
					updateStatus("The selected resource does not contain a valid Triple Graph Grammar as its root element!");
					return;
				} else {
					tripleGraphGrammar = (TripleGraphGrammar) interpreterConfigurationResource.getContents().get(0);
					if (tripleGraphGrammar.getAllModelParameters().size() <= 0) {
						tggFile = null;
						tripleGraphGrammar = null;
						updateStatus("The selected Triple Graph Grammar does not contain any typed models!");
						return;
					} else { 
						infoList.removeAll();
						for (WizardPage page : modelResourceSelectionPages) {
							// as pages are not removed from the wizard's pages list, we have 
							// to ensure they are not preventing the wizards from finishing.
							page.setPageComplete(true);
						}
						modelResourceSelectionPages.clear();
						int typedModelCount = 0;
						Iterator<TypedModel> modelParameterIterator = tripleGraphGrammar.getModelParameter().iterator();
						while (modelParameterIterator.hasNext()) {
							TypedModel typedModel = modelParameterIterator.next();
							
							//information output for the user
							infoList.add(typedModel.getName() + " used packages: " + typedModel.getUsedPackage());

							//initialize the model selection page
							InterpreterconfigurationModelWizard_ModelResourceSelectionPage newPage = new InterpreterconfigurationModelWizard_ModelResourceSelectionPage(
									"ModelResourceSelectionPage " + typedModelCount, typedModel);
							newPage.setTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_ModelSelection_title", new Object[]{typedModel.getName()}));
							newPage.setDescription(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_ModelSelection_description", new Object[]{typedModel.getName()}));

							modelResourceSelectionPages.add(newPage);
							addPage(newPage);
							newPage.setWizard(InterpreterconfigurationModelWizard.this);
							typedModelCount++;
						}
						
						modelResourceSelectionPages.get(0).setPreviousPage(this);
						InterpreterconfigurationModelWizard_ModelResourceSelectionPage lastPage = modelResourceSelectionPages.get(typedModelCount-1);
						lastPage.setNextPage(applicationScenarioCreationPage);
						applicationScenarioCreationPage.setPreviousPage(lastPage);
						applicationScenarioCreationPage.updateRadioButtons();
					}
				}
			} catch (RuntimeException e) {
				updateStatus("Exception loading resource: " + e.getMessage());
				return;
			}
			
			updateStatus(null);
		}

		protected String getFileName(){
			return fileText.getText();
		}
		
		public IFile getTggFile() {
			return tggFile;
		}

		public void setTggFile(IFile tggFile) {
			this.tggFile = tggFile;
		}


		public TripleGraphGrammar getTripleGraphGrammar() {
			return tripleGraphGrammar;
		}


		@Override
		public IWizardPage getNextPage() {
			if (tripleGraphGrammar == null || modelResourceSelectionPages.size() == 0)
				return dummyModelResourceSelectionPage;
			else
				return modelResourceSelectionPages.get(0);
		}
	}
	


	public class InterpreterconfigurationModelWizard_ModelResourceSelectionPage extends WizardPage {

		private EObject rootObject;

		private WizardPage nextPage = null;

		private Button radioButtonResourceURI = null;
		private Button radioButtonObject = null;
		private Text textResourceURI = null;

		private Composite parent;

		private TypedModel typedModel;

		public TypedModel getTypedModel() {
			return typedModel;
		}

		private TreeViewer modelViewer;

		private boolean completionOverride;
		

		protected InterpreterconfigurationModelWizard_ModelResourceSelectionPage(String pageName, TypedModel typedModel) {
			super(pageName);
			setPageComplete(false);
			this.typedModel = typedModel;
		}

		@Override
		public IWizardPage getNextPage() {
			return (nextPage != null) ? nextPage : super.getNextPage();
		}

		public void setNextPage(
				WizardPage page) {
			nextPage = page;
		}

		public void createControl(Composite parent) {
			this.parent = parent;
			Composite container = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			container.setLayout(layout);
			layout.numColumns = 4;
			layout.verticalSpacing = 9;

			Label resourceLabel = new Label(container, SWT.NULL);
			resourceLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));
			resourceLabel.setText("Resource:");

			textResourceURI = new Text(container, SWT.BORDER);
			textResourceURI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
			textResourceURI.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					dialogChanged();
				}
			});
			textResourceURI.setText("");
			
			Button button = new Button(container, SWT.PUSH);
			button.setText("Browse...");
			button.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					handleResourceURIBrowse();
				}
			});
			button.setLayoutData(new GridData(SWT.END, SWT.FILL, false, false, 1, 1));
			
			Composite radios = new Composite(container, SWT.NULL);
			radios.setLayout(new GridLayout(2, true));
			radios.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1));
			
			radioButtonResourceURI = new Button(radios, SWT.RADIO);
			radioButtonResourceURI.setText("Use Resource as Root");
			radioButtonResourceURI.setSelection(true);
			radioButtonResourceURI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			radioButtonResourceURI.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (radioButtonResourceURI.getSelection()) {
						modelViewer.getControl().setEnabled(false);
						dialogChanged();
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
						
			radioButtonObject = new Button(radios, SWT.RADIO);
			radioButtonObject.setText("Use Specific Object as Root");
			radioButtonObject.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			radioButtonObject.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (radioButtonObject.getSelection()) {
						modelViewer.getControl().setEnabled(true);
						dialogChanged();
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			Label label = new Label(container, SWT.NULL);
			label.setText("Root Object:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 4, 1));
			
			modelViewer = new TreeViewer(container, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			GridData layoutData = new GridData(GridData.FILL_BOTH);
			layoutData.heightHint = 300;
			layoutData.widthHint = 300;
			layoutData.horizontalSpan = 4;
			modelViewer.getTree().setLayoutData(layoutData);
			modelViewer.addSelectionChangedListener(new ISelectionChangedListener() {
				public void selectionChanged(SelectionChangedEvent event) {
					updateSelection((IStructuredSelection) event.getSelection());
				}
			});
			modelViewer.getControl().setEnabled(false);

			// We need 2 item providers: for resources and for reflection
			List<AdapterFactory> factories = new ArrayList<AdapterFactory>();
			factories.add(new ResourceItemProviderAdapterFactory());
			factories.add(new ReflectiveItemProviderAdapterFactory());
			AdapterFactory adapterFactory = new ComposedAdapterFactory(factories);
			modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
			modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

			dialogChanged();
			setControl(container);
		}


		protected void updateSelection(IStructuredSelection selection) {
			if (selection.size() == 1 && selection.getFirstElement() instanceof EObject) {
				rootObject = (EObject) selection.getFirstElement();
				updateStatus(null);
			} else {
				updateStatus("No root element selected.");
			}
		}

		protected void handleResourceURIBrowse() {
			IFile[] files = WorkspaceResourceDialog.openFileSelection(parent.getShell(), "Select Resource", "Select resource for domain \"" + typedModel.getName() + "\":", false, null, null);
			if (files.length == 1)
			{
				textResourceURI.setText(files[0].getFullPath().toString());
			}

			return;
		}


		/**
		 * Sets the current status message, or if null, allow the page to complete
		 */
		private void updateStatus(String message) {
			setErrorMessage(message);
			setPageComplete(getCompletionOverride()?true:message == null);
		}


		private boolean getCompletionOverride() {
			return completionOverride;
		}

		public void setCompletionOverride(boolean completionOverride) {
			this.completionOverride = completionOverride;
		}

		/**
		 * Ensures that a file or object is selected as root 
		 */
		private void dialogChanged() {

			String fileName = textResourceURI.getText();
			rootObject = null;

			if (fileName.length() == 0) {
				updateStatus("File must be specified.");
				return;
			}

			if (!ResourcesPlugin.getWorkspace().getRoot().exists(Path.fromOSString(fileName))) {
				//if resource is used as root, it's ok to specify a non-existing file (will be created by the interpreter)
				if (radioButtonObject.getSelection())
					updateStatus("File does not exists.");
				else
					updateStatus(null);
				return;
			}

			try {
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource targetModelResource = resourceSet.getResource(URI.createPlatformResourceURI(fileName, true), true);
				if (!(targetModelResource.getContents().get(0) instanceof EObject)){
					updateStatus("The selected resource does not contain a valid EMF model.");
					return;
				}else{
					rootObject = (EObject) targetModelResource.getContents().get(0);
					if (rootObject != null) {
						modelViewer.setInput(targetModelResource);
						modelViewer.setSelection(new StructuredSelection(rootObject));
					}
				}
			} catch (RuntimeException e) {
				updateStatus("Exception loading resource: " + e.getMessage() + " " + e.getStackTrace()[0] + "...");
				return;
			}

			updateStatus(null);
		}

		public String getFileName(){
			return radioButtonObject.getSelection()?"":textResourceURI.getText();
		}

		public EObject getRootObject() {
			return radioButtonObject.getSelection()?rootObject:null;
		}

	}

	public class InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage extends WizardPage {

		public static final int SELECTION_SOURCE = 0;
		public static final int SELECTION_CORRESPONDENCE = 1;
		public static final int SELECTION_TARGET = 2;
		public static final int SELECTION_NA = -1;
		
		Composite container;
		private Text textApplicationScenarioName;
		
		private Button radioButtonsForDomains [][];
		private Label labelsForDomains [];
		private Composite compositesForDomains [];
		private Combo comboMode;

		protected InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage(
				String pageName) {
			super(pageName);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void createControl(Composite parent) {
			container = new Composite(parent, SWT.NULL);
			
			GridLayout layout = new GridLayout();
			container.setLayout(layout);
			layout.numColumns = 2;
			layout.verticalSpacing = 9;

			Label label = new Label(container, SWT.NULL);
			label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));
			label.setText("Resource:");

			textApplicationScenarioName = new Text(container, SWT.BORDER);
			textApplicationScenarioName.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			textApplicationScenarioName.setText("Default Application Scenario");
			
			label = new Label(container, SWT.NULL);
			label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));
			label.setText("Mode:");

			comboMode = new Combo (container, SWT.READ_ONLY);
			textApplicationScenarioName.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			comboMode.setItems (new String [] {"transform", "update", "synchronize"});
			comboMode.select(0);

			label = new Label(container, SWT.NULL);
			label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));
			label.setText("Assignment of domains:");
			
			updateRadioButtons();
			
			setControl(container);
			setPageComplete(true);
		}

		/**
		 * @param container
		 */
		public void updateRadioButtons() {
			if (compositesForDomains != null) {
				for (int i = 0; i < compositesForDomains.length; i++) {
					compositesForDomains[i].dispose();
				}
			}
			
			compositesForDomains = new Composite[modelResourceSelectionPages.size()];
			labelsForDomains = new Label[modelResourceSelectionPages.size()];
			radioButtonsForDomains = new Button[modelResourceSelectionPages.size()][3];
			
			for (int i = 0; i < modelResourceSelectionPages.size(); i++) {
				compositesForDomains[i] = new Composite(container, SWT.NULL);
				compositesForDomains[i].setLayout(new GridLayout(4, true));
				compositesForDomains[i].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
				
				labelsForDomains[i] = new Label(compositesForDomains[i], SWT.NULL);
				labelsForDomains[i].setText(modelResourceSelectionPages.get(i).getTypedModel().getName());
				                 
				radioButtonsForDomains[i][0] = new Button(compositesForDomains[i], SWT.RADIO);
				radioButtonsForDomains[i][0].setText("Source");
				radioButtonsForDomains[i][0].setSelection(true);
				radioButtonsForDomains[i][0].setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
				radioButtonsForDomains[i][1] = new Button(compositesForDomains[i], SWT.RADIO);
				radioButtonsForDomains[i][1].setText("Correspondence");
				radioButtonsForDomains[i][1].setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
				radioButtonsForDomains[i][2] = new Button(compositesForDomains[i], SWT.RADIO);
				radioButtonsForDomains[i][2].setText("Target");
				radioButtonsForDomains[i][2].setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			}
			container.layout();
		}

		@Override
		public IWizardPage getNextPage() {
			return null;
		}
		
		
		public int getMode(){
			return comboMode.getSelectionIndex();
		}

		public int getDomainAssignment(int domainIndex){
			return radioButtonsForDomains[domainIndex][0].getSelection()?SELECTION_SOURCE:
				   radioButtonsForDomains[domainIndex][1].getSelection()?SELECTION_CORRESPONDENCE:
				   radioButtonsForDomains[domainIndex][2].getSelection()?SELECTION_TARGET:
					   SELECTION_NA;
		}
		
		public String getApplicationScenarioName() {
			return textApplicationScenarioName.getText();
		}
	}
	
	/**
	 * The framework calls this to create the contents of the wizard.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
		@Override
	public void addPages() {
		// Create a page, set the title, and the initial model file name.
		//
		newFileCreationPage = new InterpreterconfigurationModelWizardNewFileCreationPage("NewFileCreationPage", selection);
		newFileCreationPage.setTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_label"));
		newFileCreationPage.setDescription(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_description"));
		newFileCreationPage.setFileName(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationEditorFilenameDefaultBase") + "." + FILE_EXTENSIONS.get(0));
		addPage(newFileCreationPage);

		// Try and get the resource selection to determine a current directory for the file dialog.
		//
		if (selection != null && !selection.isEmpty()) {
			// Get the resource...
			//
			Object selectedElement = selection.iterator().next();
			if (selectedElement instanceof IResource) {
				// Get the resource parent, if its a file.
				//
				IResource selectedResource = (IResource)selectedElement;
				if (selectedResource.getType() == IResource.FILE) {
					selectedResource = selectedResource.getParent();
				}

				// This gives us a directory...
				//
				if (selectedResource instanceof IFolder || selectedResource instanceof IProject) {
					// Set this for the container.
					//
					newFileCreationPage.setContainerFullPath(selectedResource.getFullPath());

					// Make up a unique new name here.
					//
					String defaultModelBaseFilename = InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationEditorFilenameDefaultBase");
					String defaultModelFilenameExtension = FILE_EXTENSIONS.get(0);
					String modelFilename = defaultModelBaseFilename + "." + defaultModelFilenameExtension;
					for (int i = 1; ((IContainer)selectedResource).findMember(modelFilename) != null; ++i) {
						modelFilename = defaultModelBaseFilename + i + "." + defaultModelFilenameExtension;
					}
					newFileCreationPage.setFileName(modelFilename);
				}
			}
		}
		
		tggSelectionPagePage = new InterpreterconfigurationModelWizard_TGGSelectionPage("TGGSelectionPage");
		tggSelectionPagePage.setTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_TGGSelection_title"));
		tggSelectionPagePage.setDescription(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_TGGSelection_description"));
		addPage(tggSelectionPagePage);
		
		dummyModelResourceSelectionPage = new InterpreterconfigurationModelWizard_ModelResourceSelectionPage(
				"Dummy Page", null);
		dummyModelResourceSelectionPage.setDescription("Dummy Page");
		addPage(dummyModelResourceSelectionPage);
		dummyModelResourceSelectionPage.setCompletionOverride(true);
		
		applicationScenarioCreationPage = new InterpreterconfigurationModelWizard_ApplicationScenarioCreationPage("ApplicationScenarioCreationPage");
		applicationScenarioCreationPage.setTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_ApplicationScenario_title"));
		applicationScenarioCreationPage.setDescription(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_ApplicationScenario_description"));
		addPage(applicationScenarioCreationPage);
		
	    /*ModelImporterDetailPage detailPage = new ModelImporterDetailPage(getModelImporter(), "EcoreModel");
	    detailPage.setTitle(EcoreImporterPlugin.INSTANCE.getString("_UI_EcoreImport_title"));
	    detailPage.setDescription(EcoreImporterPlugin.INSTANCE.getString(detailPage.showGenModel() ?
	      "_UI_EcoreImportNewProject_description" : "_UI_EcoreImportFile_description"));    
	    addPage(detailPage);*/

		/*initialObjectCreationPage = new InterpreterconfigurationModelWizardInitialObjectCreationPage("Whatever2");
		initialObjectCreationPage.setTitle(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_InterpreterconfigurationModelWizard_label"));
		initialObjectCreationPage.setDescription(InterpreterconfigurationEditorPlugin.INSTANCE.getString("_UI_Wizard_initial_object_description"));
		addPage(initialObjectCreationPage);*/
	}

	/**
	 * Get the file from the page.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IFile getModelFile() {
		return newFileCreationPage.getModelFile();
	}

}
