package de.upb.swt.qvt.tgg.diagram.debug.decorator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.core.service.IProviderChangeListener;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.CreateDecoratorsOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorProvider;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.diagram.debug.DiagramElements;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ITGGNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;

public class NodeDecoratorProvider implements IDecoratorProvider {

	@Override
	public void createDecorators(IDecoratorTarget decoratorTarget) {
		EditPart editPart = (EditPart) decoratorTarget.getAdapter(EditPart.class);
		if (editPart instanceof NodeEditPart || editPart instanceof Node2EditPart || editPart instanceof Node3EditPart) {
			Node node = null;
			if(editPart instanceof NodeEditPart){
				NodeEditPart nodepart = (NodeEditPart)editPart;
				NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
				node = (Node)nodeOfGraph.getElement();
			} else if(editPart instanceof Node2EditPart){
				Node2EditPart nodepart = (Node2EditPart)editPart;
				NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
				node = (Node)nodeOfGraph.getElement();
			} else if(editPart instanceof Node3EditPart){
				Node3EditPart nodepart = (Node3EditPart)editPart;
				NodeImpl nodeOfGraph = (NodeImpl)nodepart.getModel();
				node = (Node)nodeOfGraph.getElement();
			}
			TripleGraphGrammarRuleEditPart tggeditPart = (TripleGraphGrammarRuleEditPart)editPart.getParent();
			TripleGraphGrammarRule rule = (TripleGraphGrammarRule)((DiagramImpl)tggeditPart.getModel()).getElement();
			String ruleName = rule.getName();
			IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
			if(breakpoints.length > 0){
				for(int i=0; i < breakpoints.length; i++){					
					IMarker marker = breakpoints[i].getMarker();
					try {
						if(node.getName() != null){
							if(node.getName().equals(marker.getAttribute("nodename"))){
								if( marker.getAttribute("nodetype") != null){
									if(marker.getAttribute("nodetype").equals(node.getTypedName())){
										if(marker.getAttribute("rulename").equals(ruleName)){
											DiagramElements.makeOrDeleteBreakpointFigure((ITGGNodeEditPart) editPart,true);
										}
									}
								}
							}
						} else {
							if( marker.getAttribute("nodetype") != null){
								if(marker.getAttribute("nodetype").equals(node.getTypedName())){
									if(marker.getAttribute("rulename").equals(ruleName)){
										DiagramElements.makeOrDeleteBreakpointFigure((ITGGNodeEditPart) editPart,true);
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}			
			} 
		}
	}

	@Override
	public void addProviderChangeListener(IProviderChangeListener listener) {
	}

	@Override
	public boolean provides(IOperation operation) {
		if (!(operation instanceof CreateDecoratorsOperation)) {
			return false;
		}
		IDecoratorTarget decoratorTarget = ((CreateDecoratorsOperation) operation).getDecoratorTarget();
		View view = (View) decoratorTarget.getAdapter(View.class);
		return view != null && !(view instanceof Diagram); 
	}

	@Override
	public void removeProviderChangeListener(IProviderChangeListener listener) {
	}

}
