package de.upb.swt.qvt.tgg.diagram.debug.actions;

import org.eclipse.core.resources.IMarker;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ShowTGGBreakpoints implements IObjectActionDelegate {

	public ShowTGGBreakpoints() {
	}
	
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}
	@Override
	public void run(IAction action) {
		try {
			MessageConsole myConsole = findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			myConsole.clearConsole();
	 	    MessageConsoleStream out = myConsole.newMessageStream();
	 	    out.println("Current TGG Breakpoints:");
	 	    int counter = 0;
	 	    IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
	 	    for(int i=0; i < breakpoints.length; i++){
   	    	String outString ="";
   	    	IMarker marker = breakpoints[i].getMarker();
				if( marker.getAttribute("rulename") != null){
					counter++;
	     	    	outString = "#"+ counter +"  ";
	     	    	outString += marker.getResource().getName().substring(0, marker.getResource().getName().indexOf(marker.getResource().getFileExtension())-1);
					String ruleName2 = (String)marker.getAttribute("rulename");
					outString += " | "+ "Rule:" + " "+ ruleName2;
					if( marker.getAttribute("nodename") != null){
						String nodeName = (String)marker.getAttribute("nodename");
						outString += " | "+ "Nodename:" + " "+ nodeName;
						if(marker.getAttribute("nodetype") != null){
							String nodeType = (String)marker.getAttribute("nodetype");
							outString += " | "+ "Nodetype:" + " "+ nodeType;
						}
					}
					out.println(outString);
   	    	}	
   	    }
		} catch(Exception e){
		}
	}
	
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		
	}
	
	private MessageConsole findConsole(String name) {
	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();
	      IConsole[] existing = conMan.getConsoles();
	      for (int i = 0; i < existing.length; i++)
	         if (name.equals(existing[i].getName()))
	            return (MessageConsole) existing[i];
	      MessageConsole myConsole = new MessageConsole(name, null);
	      conMan.addConsoles(new IConsole[]{myConsole});
	      return myConsole;
	}

}
