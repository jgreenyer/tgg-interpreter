package de.upb.swt.qvt.tgg.diagram.debug;

import java.util.Iterator;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.upb.swt.qvt.tgg.diagram.edit.parts.ITGGNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditor;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditorPlugin;



public class DiagramElements {
	
	public static void makeOrDeleteBreakpointFigure(final ITGGNodeEditPart editPart, final boolean addFigure) {
		
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IFigure figure = editPart.getFigure();
				if(addFigure){
					editPart.unregisterVisuals();					
					Figure parent = (Figure) figure.getParent();
					int index = parent.getChildren().indexOf(figure);
					parent.remove(figure);
					IFigure figurecopy = figure;
					Label imageLabel = new Label();
					imageLabel.setIcon(new Image(Display.getDefault(),TggDiagramEditorPlugin.getInstance().getBundledImage("../de.upb.swt.qvt.tgg.diagram.debug/icons/debugpoint.gif").getImageData() ));
					figurecopy.add(imageLabel);
					figure = figurecopy;
					parent.add(figure, index);
					figure.setParent(parent);
				} else {
					Label deleteLabel = null;
					int childrenLength=  figure.getChildren().size();
					Object[] childrenArray = figure.getChildren().toArray();
					for(int i=0; i < childrenLength; i++) {
						if(childrenArray[i] instanceof Label){
							if(((Label)childrenArray[i]).getIcon() != null){
								deleteLabel = (Label)childrenArray[i];
								figure.remove(deleteLabel);
							}
						}
					}
				}
				editPart.registerVisuals();
				editPart.refreshVisuals();
		    }
		});
	}
	
	public static void clearDiagram(final ITGGNodeEditPart editPart) {		
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IFigure figure = editPart.getFigure();
				editPart.unregisterVisuals();
				Label deleteLabel = null;
				int childrenLength=  figure.getChildren().size();
				Object[] childrenArray = figure.getChildren().toArray();
				for(int i=0; i < childrenLength; i++) {
					if(childrenArray[i] instanceof Label){
						if(((Label)childrenArray[i]).getIcon() == null){
							deleteLabel = (Label)childrenArray[i];
							figure.remove(deleteLabel);
						}
					}
				}
				editPart.registerVisuals();
				editPart.refreshVisuals();
			}
		});
	}
	
	public static void changeFigure(final ITGGNodeEditPart editPart, final boolean isCurrentDebugNode, final int indexOfNodeMatching) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IFigure figure = editPart.getFigure();
				boolean needNewLabel = true;
				editPart.unregisterVisuals();
				Figure parent = (Figure) figure.getParent();
				int index = parent.getChildren().indexOf(figure);
				parent.remove(figure);
				IFigure figurecopy = figure;
				if(isCurrentDebugNode){
					LineBorder border = new LineBorder(ColorConstants.red);
					border.setWidth(4);
					figurecopy.setBorder(border);
					int childrenLength=  figure.getChildren().size();
					Object[] array = figure.getChildren().toArray();
					if(indexOfNodeMatching > -1){
						for(int i=0; i < childrenLength; i++) {
							if(array[i] instanceof Label){
								if(((Label)array[i]).getIcon() == null){
									((Label)array[i]).setText(indexOfNodeMatching+"");									
									needNewLabel = false;
									break;
								}
							}
						}
						if(needNewLabel){
							Label label = new Label(indexOfNodeMatching+"");							
							FontData defaultFont = new FontData("Arial", 15, SWT.BOLD);
							label.setFont(new Font(Display.getDefault(), defaultFont));
							label.setForegroundColor(ColorConstants.red);
							label.setLabelAlignment(PositionConstants.TOP);
							figurecopy.add(label);
						}
					}
				} else {
					LineBorder border = new LineBorder(ColorConstants.white);
					border.setWidth(0);
					figurecopy.setBorder(border);
				}
				figure = figurecopy;
				parent.add(figure, index);
				figure.setParent(parent);
				editPart.registerVisuals();
				editPart.refreshVisuals();
		    }
		});
	}
	
	public static void clearDiagram(){
		try {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			for(int u=0; u < windows.length; u++){
				if(windows[u].getActivePage().getActiveEditor() instanceof TggDiagramEditor){
					TggDiagramEditor target = (TggDiagramEditor)windows[u].getActivePage().getActiveEditor();
					target.getRootEditpart();
					for(Iterator j =target.getRootEditpart().getChildren().iterator(); j.hasNext();){
						EditPart part = (EditPart)j.next();
						if(part instanceof TripleGraphGrammarRuleEditPart){
							TripleGraphGrammarRuleEditPart rulepart = (TripleGraphGrammarRuleEditPart)part;
							Iterator k =rulepart.getChildren().iterator();
							while(k.hasNext()){														
//								nodepart.clearDiagram();
								try{
									clearDiagram((ITGGNodeEditPart) k.next());
								}catch (Exception e){
									
								}								
							}
						}
					}
				}
			}
		} catch (Exception e) {
		}
	}
}
