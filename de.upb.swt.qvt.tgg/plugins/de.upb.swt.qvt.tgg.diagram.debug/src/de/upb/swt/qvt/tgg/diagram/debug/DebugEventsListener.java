package de.upb.swt.qvt.tgg.diagram.debug;

import de.upb.swt.qvt.tgg.debug.manager.ITGGDebugEventListener;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ITGGNodeEditPart;

public class DebugEventsListener implements ITGGDebugEventListener {

	public DebugEventsListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void changeFigure(Object editPart, boolean isCurrentDebugNode, int indexOfNodeMatching) {		
		if(editPart instanceof ITGGNodeEditPart)
			DiagramElements.changeFigure((ITGGNodeEditPart) editPart, isCurrentDebugNode, indexOfNodeMatching);
	}

	@Override
	public void clearDiagram(Object editPart) {
		if(editPart instanceof ITGGNodeEditPart)
			DiagramElements.clearDiagram((ITGGNodeEditPart) editPart);
	}
	
	@Override
	public void clearDiagram() {
		DiagramElements.clearDiagram();
	}

//	@Override
//	public void isApplyRuleRunning(
//			TripleGraphGrammarRule tripleGraphGrammarRule, boolean isRunning,
//			Object classInstance, boolean isBreakpointFound) {
//		// TODO Auto-generated method stub
//
//	}

//	@Override
//	public void candidateNode(Node node, EObject eObject, Object classInstance) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void matchedNode(Node node, EObject eObject,
//			boolean isFrontTransformationNode, boolean isRunning,
//			Object classInstance, boolean isBreakpointFound) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void enforcedNode(Node node, EObject eObject, Object classInstance,
//			boolean isRunning, boolean isBreakpointFound) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void currentRuleBinding() {
//		// TODO Auto-generated method stub
//		
//	}
}
