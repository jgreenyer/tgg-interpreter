package de.upb.swt.qvt.tgg.diagram.debug.actions;

import java.util.Iterator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.debug.manager.DebugProvider;
import de.upb.swt.qvt.tgg.diagram.debug.DiagramElements;
import de.upb.swt.qvt.tgg.diagram.edit.parts.ITGGNodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node2EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.Node3EditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.NodeEditPart;
import de.upb.swt.qvt.tgg.diagram.edit.parts.TripleGraphGrammarRuleEditPart;
import de.upb.swt.qvt.tgg.diagram.part.TggDiagramEditor;

public class NewNodeBreakpoint implements IObjectActionDelegate{

	private Node selectedNode;
	private IResource resource;
	private GraphicalEditPart editpart;
	
	public NewNodeBreakpoint() {
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}
	
	@Override
	public void run(IAction action) {
		boolean addNodeAction = true;
		try {
			TripleGraphGrammarRuleEditPart tggeditPart = (TripleGraphGrammarRuleEditPart)editpart.getParent();
			TripleGraphGrammarRule rule = (TripleGraphGrammarRule)((DiagramImpl)tggeditPart.getModel()).getElement();
			String ruleName = rule.getName();
			IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
			if(breakpoints.length > 0){
				for(int i=0; i < breakpoints.length; i++){
					IMarker marker = breakpoints[i].getMarker();
					if( marker.getAttribute("nodetype") != null && selectedNode.getTypedName() != null){
						if(marker.getAttribute("nodetype").equals(selectedNode.getTypedName())){
							if(marker.getAttribute("rulename").equals(ruleName)){
								if(selectedNode.getName() != null){
									if(selectedNode.getName().equals(marker.getAttribute("nodename"))){
										addNodeAction = false;
										DebugPlugin.getDefault().getBreakpointManager().removeBreakpoint(breakpoints[i], true);
										DiagramElements.makeOrDeleteBreakpointFigure((ITGGNodeEditPart) editpart,false);
									}
								} else {
									addNodeAction = false;
									DebugPlugin.getDefault().getBreakpointManager().removeBreakpoint(breakpoints[i], true);										
									DiagramElements.makeOrDeleteBreakpointFigure((ITGGNodeEditPart)editpart,false);								
								}										
							}
						}
					} 
				}
			}
			if(addNodeAction){
				DiagramElements.makeOrDeleteBreakpointFigure((ITGGNodeEditPart)editpart,true);	
				DebugProvider dp = DebugProvider.getDefault();
				dp.addBreakpoint(resource, selectedNode, ruleName);
			}
			MessageConsole myConsole = findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			myConsole.clearConsole();
     	    MessageConsoleStream out = myConsole.newMessageStream();
     	    out.println("Current TGG Breakpoints:");
     	    int counter = 0;
     	    breakpoints = DebugPlugin.getDefault().getBreakpointManager().getBreakpoints();
     	    for(int i=0; i < breakpoints.length; i++){
   	    	String outString ="";
   	    	IMarker marker = breakpoints[i].getMarker();
				if( marker.getAttribute("rulename") != null){
					counter++;
	     	    	outString = "#"+ counter +"  ";
	     	    	outString += marker.getResource().getName().substring(0, marker.getResource().getName().indexOf(marker.getResource().getFileExtension())-1);
					String ruleName2 = (String)marker.getAttribute("rulename");
					outString += " | "+ "Rule:" + " "+ ruleName2;
					if( marker.getAttribute("nodename") != null){
						String nodeName = (String)marker.getAttribute("nodename");
						outString += " | "+ "Nodename:" + " "+ nodeName;
						if(marker.getAttribute("nodetype") != null){
							String nodeType = (String)marker.getAttribute("nodetype");
							outString += " | "+ "Nodetype:" + " "+ nodeType;
						}
					}
					out.println(outString);
   	    	}	
   	    }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private MessageConsole findConsole(String name) {
	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();
	      IConsole[] existing = conMan.getConsoles();
	      for (int i = 0; i < existing.length; i++)
	         if (name.equals(existing[i].getName()))
	            return (MessageConsole) existing[i];
	      MessageConsole myConsole = new MessageConsole(name, null);
	      conMan.addConsoles(new IConsole[]{myConsole});
	      return myConsole;
	}


	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		for (Iterator<?> selectionIterator = ((StructuredSelection)selection).iterator(); selectionIterator.hasNext();) {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			for(int i=0; i < windows.length; i++){
				IResource resource = (IResource)((TggDiagramEditor)windows[i].getActivePage().getActiveEditor()).getAdapter(IResource.class);
				this.resource = resource;
			}
			GraphicalEditPart graphicalEditPart = (GraphicalEditPart) selectionIterator.next();
			if(graphicalEditPart instanceof NodeEditPart || graphicalEditPart instanceof Node2EditPart || graphicalEditPart instanceof Node3EditPart){
				Object modelElement = ((org.eclipse.gmf.runtime.notation.Node)graphicalEditPart.getModel()).getElement();
				if (modelElement != null &&  modelElement instanceof Node){
					this.editpart = graphicalEditPart;
					this.selectedNode = (Node) modelElement;
				}
			}
		}
	}	
}
