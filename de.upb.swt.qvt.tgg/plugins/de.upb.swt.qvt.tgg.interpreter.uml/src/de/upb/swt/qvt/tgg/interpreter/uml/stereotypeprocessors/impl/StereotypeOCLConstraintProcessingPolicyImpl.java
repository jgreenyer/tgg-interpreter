/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.tgg.Constraint;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.OCLConstraint;
import de.upb.swt.qvt.tgg.TripleGraphGrammarRule;
import de.upb.swt.qvt.tgg.interpreter.impl.DefaultOCLConstraintProcessingPolicyImpl;
import de.upb.swt.qvt.tgg.interpreter.uml.Activator;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.util.Helper;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Stereotype OCL Constraint Processing Policy</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class StereotypeOCLConstraintProcessingPolicyImpl extends
		DefaultOCLConstraintProcessingPolicyImpl implements
		StereotypeOCLConstraintProcessingPolicy {

	private static Logger logger = Activator.getLogManager().getLogger(
			StereotypeOCLConstraintProcessingPolicyImpl.class.getName());

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StereotypeOCLConstraintProcessingPolicyImpl() {
		super();
	}

	private EList<Stereotype> stereotypesAppliedToNode;

	/**
	 * Used to store the UML Stereotype corresponding to the stereotype
	 * definition parent EClass of the constraint's slot attribute.
	 */
	private Stereotype slotAttributesParentStereotype;

	@Override
	public boolean canCheckConstraint(Node node, EObject eObject,
			Constraint constraint) {

		if (!(constraint instanceof OCLConstraint))
			return false;

		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		EAttribute slotAttribute = oclConstraint.getSlotAttribute();

		if (slotAttribute == null)
			return false;

		stereotypesAppliedToNode = Helper
				.getNodesAppliedStereotypes(oclConstraint.getSlotNode());

		// if the definition EClass of one stereotype applied to the
		// slot node is is a subtype of the parent class of the slot
		// attribute, return TRUE.
		for (Stereotype stereotype : stereotypesAppliedToNode) {
			if (stereotype.getDefinition() == slotAttribute
					.getEContainingClass()
					|| stereotype.getDefinition().getEAllSuperTypes()
							.contains(slotAttribute.getEContainingClass())) {
				slotAttributesParentStereotype = stereotype;
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean canEnforceConstraint(Node node, EObject eObject,
			Constraint constraint) {
		// jgreen: This was a bad idea: 
//		if (!(constraint instanceof OCLConstraint)) {
//			// Here we only do OCL constraints...
//			return false;
//		}
//		OCLConstraint oclConstraint = (OCLConstraint) constraint;
//		if (oclConstraint.getSlotAttribute() == null){
//			//there is no slot attribute to enforce
//			return false;
//		}
		// TODO: jgreen: check if this makes sense. 
		if (canCheckConstraint(node, eObject, constraint))
			return true;
		return false;
	}

	@Override
	public boolean checkConstraint(Node node, EObject eObject,
			Constraint constraint) throws ValueClashException,
			ConstraintUnprocessableException {

		if (!canCheckConstraint(node, eObject, constraint))
			return false;

		Object result = null;
		OCLConstraint oclConstraint = (OCLConstraint) constraint;

		initializeOCL();

		EAttribute slotAttribute = oclConstraint.getSlotAttribute();
		EObject contextObject = getGraphMatcher().getRuleBinding()
				.getNodeToEObjectMap().get(oclConstraint.getSlotNode());

		if (!(contextObject instanceof Element)) {
			// logger...
			return false;
		}
		Element contextUMLElement = (Element) contextObject;

		// The node may reference a number of stereotypes

		Stereotype relevantAppliedStereotype = null;
		for (Stereotype appliedStereotype : contextUMLElement
				.getAppliedStereotypes()) {
			if (appliedStereotype.getDefinition() == slotAttributesParentStereotype
					.getDefinition()
					|| appliedStereotype
							.getDefinition()
							.getEAllSuperTypes()
							.contains(
									slotAttributesParentStereotype
											.getDefinition())) {
				relevantAppliedStereotype = appliedStereotype;
				break;
			}

		}

		EObject stereotypeApplication = contextUMLElement
				.getStereotypeApplication(relevantAppliedStereotype);

		helper.setAttributeContext(stereotypeApplication.eClass(),
				slotAttribute);

		// if (oclConstraint.getExpression().startsWith("def:")) {
		// processDefExpression(node, eObject, oclConstraint);
		// return true;
		// }

		try {
			/*
			 * The node and object passed to this method are not necessarily the
			 * slot node and slot object of this constraint, because the
			 * constraint could be a previously unprocessable constraint
			 * attached to another node.
			 */
			// if (oclConstraint.getSlotNode() != node)
			// eObject =
			// getRuleBinding().getNodeToEObjectMap().get(oclConstraint.getSlotNode());
			//
			// if (eObject == null)
			// throw new ConstraintUnprocessableException("");

			OCLExpression<EClassifier> expr = helper.createQuery(oclConstraint
					.getExpression());
			result = ocl.evaluate(null, expr);
			
//			logger.debug("The OCL expression	 " + oclConstraint
//					.getExpression() + " evaluates to: " + result);

			Object value = contextUMLElement.getValue(
					relevantAppliedStereotype, slotAttribute.getName());

			
			boolean isSatisfied = (value != null && value.equals(result));

			/*
			 * Problem: If the attribute is typed over an Enumeration, the
			 * expression results in an instance of
			 * org.eclipse.emf.ecore.EEnumLiteral, but the value retrieved by
			 * the contextUMLElement.getValue() method returns an
			 * org.eclipse.uml2.uml.EnumerationLiteral.
			 */
			if (!isSatisfied && value instanceof EnumerationLiteral
					&& result instanceof EEnumLiteral) {
				String valueString = ((EnumerationLiteral) value).getName();
				String expressionResultString = ((EEnumLiteral) result)
						.getName();
				isSatisfied = valueString.equals(expressionResultString);
			}

			return isSatisfied;
		} catch (ParserException e) {
			if (e instanceof SemanticException
					&& e.getMessage().contains("Unrecognized variable")) {
				throw new ConstraintUnprocessableException(e.getMessage());
			}
			 logger.error("An exception occurred while parsing the ocl expression \""
			 + oclConstraint.getExpression() + "\"", e);
		}

		return true;

	}

	@Override
	public boolean enforceConstraint(Node node, EObject eObject,
			Constraint constraint) throws ValueClashException,
			ConstraintUnprocessableException {

		// TODO: This is (nearly completely) duplicate code, as it differs only
		// at
		// the stereotype checking from the original in
		// DefaultOCLConstraintProcessingPolicyImpl

		Object result = null;
		OCLConstraint oclConstraint = (OCLConstraint) constraint;
		// fetch currently processed TGG rule from the ruleBinding (see
		// FrontTransformationProcessor.createInitialRuleBinding())
		TripleGraphGrammarRule tggRule = getRuleBinding().getTggRule();
		Node refinedConstraintSlotNode = tggRule
				.getMostRefinedNodeOf(oclConstraint.getSlotNode());

		if (oclConstraint.isCheckonly())
			return checkConstraint(node, eObject, constraint);

		/*
		 * The node and object passed to this method are not necessarily the
		 * slot node and slot object of this constraint. If the passed node is
		 * the slot node and the passed object is the slot object, then the node
		 * cannot be bound. When the node is already bound, the object bound to
		 * the node is the slot object.
		 */
		EObject slotObject;
		if (getRuleBinding().getNodeToEObjectMap().get(
				refinedConstraintSlotNode) != null) {
			slotObject = getRuleBinding().getNodeToEObjectMap().get(
					refinedConstraintSlotNode);
		} else {
			slotObject = eObject;
		}

		// TODO this is a temporary modification. Instead of adding and
		// removing variables and their bindings wheneve a node binding is
		// added or removed from the rule binding, we initialize a new OCL
		// environment each time that we check or enforce a constraint.
		initializeOCL();

		// if (ocl == null) {
		// initializeOCL();
		// }

		helper.setAttributeContext(refinedConstraintSlotNode.getEType(),
				oclConstraint.getSlotAttribute());
		// jgreen: I don't know why, but it is necessary to enforce OCL constraints for applied stereotypes in target domains... 
		helper.setContext(oclConstraint.getSlotAttribute().getEType());
		try {
			OCLExpression<EClassifier> expr = helper.createQuery(oclConstraint
					.getExpression());
			result = ocl.evaluate(null, expr);
			if (slotObject instanceof NamedElement) {
				Stereotype stereotype = null;
				for (Stereotype st : ((NamedElement) slotObject)
						.getAppliedStereotypes()) {
					if (st.getName().equals(
							oclConstraint.getSlotAttribute()
									.getEContainingClass().getName())) {
						EList<Property> attribs = st.getAllAttributes();
						for (Property prop : attribs) {
							if (prop.getName().equals(
									oclConstraint.getSlotAttribute().getName())) { // FIXME:
																					// String
																					// compare
																					// is
																					// no
																					// substitute
																					// for
																					// stereotype
																					// equality!
								stereotype = (Stereotype) st;
								break;
							}
						}
					}
				}
				if (stereotype != null) {
					((NamedElement) slotObject).setValue(stereotype,
							oclConstraint.getSlotAttribute().getName(), result);
				} else {
					// throw new
					// ConstraintUnprocessableException("Value cannot be set because stereotype is not applied, yet.");
					getConstraintsForObjectAndCreateList((Element) slotObject)
							.add(new ConstraintContainer(constraint, this));
				}
			}
		} catch (ParserException e) {
			if (e instanceof SemanticException
					&& e.getMessage().contains("Unrecognized variable")) {
				throw new ConstraintUnprocessableException(e.getMessage());
			}
			else
			logger.error("An exception occurred while parsing the ocl expression \""
			+ oclConstraint.getExpression() + "\"", e);
			return false;
		} catch (ClassCastException e) {
			// jgreen: we want to log this in a meaningful way.
			logger.error("A class cast exception occurred while enforcing the evaluation result of \""
			+ oclConstraint.getExpression() + "\" " +
			"The query evaluated to \"" + result
			 + "\" and shall be assigned to the attribute " + "\"" +
			 oclConstraint.getSlotAttributeName()
			 + "\" of " + slotObject + ".", e);
			return false;
		}

		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StereotypeprocessorsPackage.Literals.STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY;
	}

	class ConstraintContainer {
		public ConstraintContainer(Constraint constraint,
				StereotypeOCLConstraintProcessingPolicy policy) {
			super();
			this.constraint = constraint;
			this.policy = policy;
		}

		Constraint constraint;
		StereotypeOCLConstraintProcessingPolicy policy;
	}

	private static Map<Element, List<ConstraintContainer>> objectsToConstraintsMap;

	static Map<Element, List<ConstraintContainer>> getObjectsToConstraintsMap() {
		if (objectsToConstraintsMap == null)
			objectsToConstraintsMap = new HashMap<Element, List<ConstraintContainer>>();
		return objectsToConstraintsMap;
	}

	private static List<ConstraintContainer> getConstraintsForObjectAndCreateList(
			Element eObject) {
		List<ConstraintContainer> result = getObjectsToConstraintsMap().get(
				eObject);
		if (result == null) {
			result = new Vector<ConstraintContainer>();
			getObjectsToConstraintsMap().put(eObject, result);
		}
		return result;
	}

	static List<ConstraintContainer> getConstraintsForObject(EObject eObject) {
		return getObjectsToConstraintsMap().get(eObject);
	}

} // StereotypeOCLConstraintProcessingPolicyImpl
