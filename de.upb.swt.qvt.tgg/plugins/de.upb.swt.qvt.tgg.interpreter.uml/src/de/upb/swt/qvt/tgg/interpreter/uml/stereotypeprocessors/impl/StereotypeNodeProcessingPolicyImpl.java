/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.interpreter.impl.BaseNodeProcessingPolicyImpl;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.util.Helper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stereotype Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StereotypeNodeProcessingPolicyImpl extends BaseNodeProcessingPolicyImpl implements StereotypeNodeProcessingPolicy {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StereotypeNodeProcessingPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StereotypeprocessorsPackage.Literals.STEREOTYPE_NODE_PROCESSING_POLICY;
	}
	
	@Override
	public boolean nodeMatches(Node node, EObject candidateEObject) {
		EList<Annotation> stereotypeAnnotations = getNodeStereotypeAnnotations(node);
		if (!stereotypeAnnotations.isEmpty()){
			for (Annotation annotation : stereotypeAnnotations) {
				for (EObject eObject : annotation.getValue()) {
					if (eObject instanceof Stereotype){
						Stereotype nodeStereotype = (Stereotype) eObject;
						
						boolean negative = Helper.negativeStereotypeString.equals(annotation.getKey());
						
						//if this is no stereotypeable element at all, return false in all cases.
						if (!(candidateEObject instanceof Element))
							return false;
						
						Element umlObject = (Element) candidateEObject;
						
						boolean stereotypeApplied = umlObject.isStereotypeApplied(nodeStereotype);

						// jgreen: Should sub-stereotypes be considered by default?
						stereotypeApplied = stereotypeApplied || !umlObject.getAppliedSubstereotypes(nodeStereotype).isEmpty();

						//consider the negativity
						if(negative == stereotypeApplied){
							return false;
						}
						

					}
				}
			}
		}
		
		return true;
	}
	
	private EList<Annotation> getNodeStereotypeAnnotations(Node node){
		EList<Annotation> nodeAnnotations = new BasicEList<Annotation>();

		Node refinedNode = node;
		while(true){
			if (!refinedNode.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()){
				nodeAnnotations.addAll(refinedNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations());
			}
			if (refinedNode.getRefinedNode() == null) 
				break;
			else
				refinedNode = refinedNode.getRefinedNode();
		}

		return nodeAnnotations;
	}
	
	
	@Override
	public EList<Edge> getUnboundEdgeToMatchNext(Node node, EObject eObject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enforceNode(Node node, EObject eObject) {
		if (!node.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()){
			EList<Annotation> stereotypeAnnotations = node.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations();
			for (Annotation annotation : stereotypeAnnotations) {
				for (EObject annotationValue : annotation.getValue()) {
					if (annotationValue instanceof Stereotype){
						Stereotype nodeStereotype = (Stereotype) annotationValue;
						
						boolean negative = Helper.negativeStereotypeString.equals(annotation.getKey());
						
						//if this is no stereotypeable element at all, throw an exception
						if (!(eObject instanceof Element))
							throw new UnsupportedOperationException("Cannot apply stereotype to a non-stereotypeable object!");
						
						if (negative)
							return; //nothing to do
						
						//((Element) eObject).applyStereotype(nodeStereotype);
						//delay stereotype application until the package link has been established
						getStereotypesForObjectAndCreateList((Element) eObject).add(nodeStereotype);
					}
				}
			}
		}
	}

	/**
	 * Removes all applied stereotypes from the element on its removal.
	 * Otherwise, the stereotype applications would stay in the resource, causing
	 * a DanglingHREFException during save. 
	 */
	@Override
	public void removeNode(Node node, EObject eObject) {
		if (eObject instanceof Element) {
			for (Stereotype stereotype :((Element)eObject).getAppliedStereotypes()) {
				((Element)eObject).unapplyStereotype(stereotype);
			}
		}
	}

	private static Map<Element, List<Stereotype>> objectsToStereotypesMap;
	static Map<Element, List<Stereotype>> getObjectsToStereotypesMap() {
		if (objectsToStereotypesMap == null)
			objectsToStereotypesMap = new HashMap<Element, List<Stereotype>>();
		return objectsToStereotypesMap;
	}
	private static List<Stereotype> getStereotypesForObjectAndCreateList(Element eObject) {
		List<Stereotype> result = getObjectsToStereotypesMap().get(eObject);
		if (result == null) {
			result = new Vector<Stereotype>();
			getObjectsToStereotypesMap().put(eObject, result);
		}
		return result;
	}
	static List<Stereotype> getStereotypesForObject(EObject eObject) {
		return getObjectsToStereotypesMap().get(eObject);
	}
	
	
} //StereotypeNodeProcessingPolicyImpl
