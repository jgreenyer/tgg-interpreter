/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsFactory;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StereotypeprocessorsFactoryImpl extends EFactoryImpl implements StereotypeprocessorsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StereotypeprocessorsFactory init() {
		try {
			StereotypeprocessorsFactory theStereotypeprocessorsFactory = (StereotypeprocessorsFactory)EPackage.Registry.INSTANCE.getEFactory("http://de.upb.swt.qvt.tgg.interpreter.uml/stereotypeprocessors/1.0"); 
			if (theStereotypeprocessorsFactory != null) {
				return theStereotypeprocessorsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StereotypeprocessorsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeprocessorsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StereotypeprocessorsPackage.STEREOTYPE_NODE_PROCESSING_POLICY: return createStereotypeNodeProcessingPolicy();
			case StereotypeprocessorsPackage.STEREOTYPE_EDGE_PROCESSING_POLICY: return createStereotypeEdgeProcessingPolicy();
			case StereotypeprocessorsPackage.STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY: return createStereotypeOCLConstraintProcessingPolicy();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeNodeProcessingPolicy createStereotypeNodeProcessingPolicy() {
		StereotypeNodeProcessingPolicyImpl stereotypeNodeProcessingPolicy = new StereotypeNodeProcessingPolicyImpl();
		return stereotypeNodeProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeEdgeProcessingPolicy createStereotypeEdgeProcessingPolicy() {
		StereotypeEdgeProcessingPolicyImpl stereotypeEdgeProcessingPolicy = new StereotypeEdgeProcessingPolicyImpl();
		return stereotypeEdgeProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeOCLConstraintProcessingPolicy createStereotypeOCLConstraintProcessingPolicy() {
		StereotypeOCLConstraintProcessingPolicyImpl stereotypeOCLConstraintProcessingPolicy = new StereotypeOCLConstraintProcessingPolicyImpl();
		return stereotypeOCLConstraintProcessingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeprocessorsPackage getStereotypeprocessorsPackage() {
		return (StereotypeprocessorsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StereotypeprocessorsPackage getPackage() {
		return StereotypeprocessorsPackage.eINSTANCE;
	}

} //StereotypeprocessorsFactoryImpl
