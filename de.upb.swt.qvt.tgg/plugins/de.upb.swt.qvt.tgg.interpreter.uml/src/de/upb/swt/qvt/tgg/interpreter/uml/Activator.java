package de.upb.swt.qvt.tgg.interpreter.uml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

import com.tools.logging.PluginLogManager;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends Plugin {

	
    // logger configuration
    private static final String LOG_PROPERTIES_FILE = "logger.properties";

    //log manager
    private PluginLogManager logManager;
	
	// The plug-in ID
	public static final String PLUGIN_ID = "de.upb.swt.qvt.tgg.interpreter.uml";

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		configure();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
        if(this.logManager != null) {
            logManager.shutdown();
            logManager = null;
        }
		super.stop(context);
	}
	
	/**
     * @return Returns the logManager.
     */
    public static PluginLogManager getLogManager() {
        return getDefault().logManager;
    }
    
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	
    private void configure() {
        URL url = getBundle().getEntry("/" + LOG_PROPERTIES_FILE);
        try {
            InputStream inputStream = url.openStream();
            if(inputStream != null) {
                Properties properties = new Properties();
                properties.load(inputStream);
                inputStream.close();
                logManager = new PluginLogManager(getDefault(), properties);
                //                logManager.hookPlugin(getDefault().getBundle()
                //                        .getSymbolicName(), getDefault().getLog());
            }
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    getDefault().getBundle().getSymbolicName(),
                    IStatus.ERROR,
                    "Error while initializing log properties." + e.getMessage(),
                    e);
            getDefault().getLog().log(status);
            throw new RuntimeException(
                    "Error while initializing log properties.", e);
        }
    }

}
