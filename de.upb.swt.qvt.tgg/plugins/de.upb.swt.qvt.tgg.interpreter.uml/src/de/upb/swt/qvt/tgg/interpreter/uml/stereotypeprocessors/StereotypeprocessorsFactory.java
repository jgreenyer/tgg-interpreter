/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage
 * @generated
 */
public interface StereotypeprocessorsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StereotypeprocessorsFactory eINSTANCE = de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Stereotype Node Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stereotype Node Processing Policy</em>'.
	 * @generated
	 */
	StereotypeNodeProcessingPolicy createStereotypeNodeProcessingPolicy();

	/**
	 * Returns a new object of class '<em>Stereotype Edge Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stereotype Edge Processing Policy</em>'.
	 * @generated
	 */
	StereotypeEdgeProcessingPolicy createStereotypeEdgeProcessingPolicy();

	/**
	 * Returns a new object of class '<em>Stereotype OCL Constraint Processing Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stereotype OCL Constraint Processing Policy</em>'.
	 * @generated
	 */
	StereotypeOCLConstraintProcessingPolicy createStereotypeOCLConstraintProcessingPolicy();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StereotypeprocessorsPackage getStereotypeprocessorsPackage();

} //StereotypeprocessorsFactory
