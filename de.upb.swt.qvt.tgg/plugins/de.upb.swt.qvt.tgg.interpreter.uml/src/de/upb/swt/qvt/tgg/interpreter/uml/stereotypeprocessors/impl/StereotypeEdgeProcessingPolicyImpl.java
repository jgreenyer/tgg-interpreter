/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.tgg.Edge;
import de.upb.swt.qvt.tgg.interpreter.impl.BaseEdgeProcessingPolicyImpl;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeOCLConstraintProcessingPolicyImpl.ConstraintContainer;
import de.upb.swt.qvt.tgg.interpreter.util.ConstraintUnprocessableException;
import de.upb.swt.qvt.tgg.interpreter.util.ValueClashException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stereotype Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StereotypeEdgeProcessingPolicyImpl extends BaseEdgeProcessingPolicyImpl implements StereotypeEdgeProcessingPolicy {
	@Override
	public void enforceEdge(Edge edge, EList<EObject> virtualModelEdge) {
		EObject target = virtualModelEdge.get(1);

		//apply all stereotypes that have been delayed from NodeProcessingPolicy
		List<Stereotype> stereotypes = StereotypeNodeProcessingPolicyImpl.getStereotypesForObject(target);
		if (stereotypes != null) {
			for (Stereotype stereotype : stereotypes) {
				if (((Element)target).isStereotypeApplicable(stereotype))
					((Element)target).applyStereotype(stereotype);
				else {
					((Element)target).getNearestPackage().applyProfile(stereotype.getProfile());
					if (((Element)target).isStereotypeApplicable(stereotype))
						((Element)target).applyStereotype(stereotype);
					else
						throw new UnsupportedOperationException("Cannot apply stereotype to object.");
				}
			}
			StereotypeNodeProcessingPolicyImpl.getStereotypesForObject(target).clear();
		}

		//enforce all constraints that have been delayed from ConstraintProcessingPolicy
		List<ConstraintContainer> constraints = StereotypeOCLConstraintProcessingPolicyImpl.getConstraintsForObject(target);
		if (constraints != null) {
			for (ConstraintContainer constraint : constraints.toArray(new ConstraintContainer[constraints.size()])) {
				try {
					constraint.policy.enforceConstraint(null, target, constraint.constraint);
				} catch (ValueClashException e) {
				} catch (ConstraintUnprocessableException e) {
				}
			}
			StereotypeOCLConstraintProcessingPolicyImpl.getConstraintsForObject(target).clear();
		}
	}

	@Override
	public boolean edgeMatches(Edge edge,
			EList<EObject> candidateVirtualModelEdge) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StereotypeEdgeProcessingPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StereotypeprocessorsPackage.Literals.STEREOTYPE_EDGE_PROCESSING_POLICY;
	}

} //StereotypeEdgeProcessingPolicyImpl
