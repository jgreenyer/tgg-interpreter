/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy;
import de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage
 * @generated
 */
public class StereotypeprocessorsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StereotypeprocessorsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeprocessorsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = StereotypeprocessorsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StereotypeprocessorsSwitch<Adapter> modelSwitch =
		new StereotypeprocessorsSwitch<Adapter>() {
			@Override
			public Adapter caseStereotypeNodeProcessingPolicy(StereotypeNodeProcessingPolicy object) {
				return createStereotypeNodeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseStereotypeEdgeProcessingPolicy(StereotypeEdgeProcessingPolicy object) {
				return createStereotypeEdgeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseStereotypeOCLConstraintProcessingPolicy(StereotypeOCLConstraintProcessingPolicy object) {
				return createStereotypeOCLConstraintProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseIMatchingPolicy(IMatchingPolicy object) {
				return createIMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseINodeMatchingPolicy(INodeMatchingPolicy object) {
				return createINodeMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseINodeEnforcementPolicy(INodeEnforcementPolicy object) {
				return createINodeEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseBaseNodeProcessingPolicy(BaseNodeProcessingPolicy object) {
				return createBaseNodeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseIEdgeEnforcementPolicy(IEdgeEnforcementPolicy object) {
				return createIEdgeEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseIEdgeMatchingPolicy(IEdgeMatchingPolicy object) {
				return createIEdgeMatchingPolicyAdapter();
			}
			@Override
			public Adapter caseBaseEdgeProcessingPolicy(BaseEdgeProcessingPolicy object) {
				return createBaseEdgeProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseIConstraintCheckingPolicy(IConstraintCheckingPolicy object) {
				return createIConstraintCheckingPolicyAdapter();
			}
			@Override
			public Adapter caseIConstraintEnforcementPolicy(IConstraintEnforcementPolicy object) {
				return createIConstraintEnforcementPolicyAdapter();
			}
			@Override
			public Adapter caseBaseConstraintProcessingPolicy(BaseConstraintProcessingPolicy object) {
				return createBaseConstraintProcessingPolicyAdapter();
			}
			@Override
			public Adapter caseDefaultOCLConstraintProcessingPolicy(DefaultOCLConstraintProcessingPolicy object) {
				return createDefaultOCLConstraintProcessingPolicyAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy <em>Stereotype Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy
	 * @generated
	 */
	public Adapter createStereotypeNodeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy <em>Stereotype Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy
	 * @generated
	 */
	public Adapter createStereotypeEdgeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy <em>Stereotype OCL Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy
	 * @generated
	 */
	public Adapter createStereotypeOCLConstraintProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy <em>IMatching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IMatchingPolicy
	 * @generated
	 */
	public Adapter createIMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy <em>INode Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeMatchingPolicy
	 * @generated
	 */
	public Adapter createINodeMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy <em>INode Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.INodeEnforcementPolicy
	 * @generated
	 */
	public Adapter createINodeEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy <em>Base Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseNodeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy <em>IEdge Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeEnforcementPolicy
	 * @generated
	 */
	public Adapter createIEdgeEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy <em>IEdge Matching Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IEdgeMatchingPolicy
	 * @generated
	 */
	public Adapter createIEdgeMatchingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy <em>Base Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseEdgeProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy <em>IConstraint Checking Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintCheckingPolicy
	 * @generated
	 */
	public Adapter createIConstraintCheckingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy <em>IConstraint Enforcement Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.IConstraintEnforcementPolicy
	 * @generated
	 */
	public Adapter createIConstraintEnforcementPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy <em>Base Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.BaseConstraintProcessingPolicy
	 * @generated
	 */
	public Adapter createBaseConstraintProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy <em>Default OCL Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy
	 * @generated
	 */
	public Adapter createDefaultOCLConstraintProcessingPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //StereotypeprocessorsAdapterFactory
