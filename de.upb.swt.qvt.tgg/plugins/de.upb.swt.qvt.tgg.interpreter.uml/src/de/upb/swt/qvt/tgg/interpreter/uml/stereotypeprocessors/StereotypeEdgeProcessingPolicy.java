/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors;

import de.upb.swt.qvt.tgg.interpreter.BaseEdgeProcessingPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stereotype Edge Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage#getStereotypeEdgeProcessingPolicy()
 * @model
 * @generated
 */
public interface StereotypeEdgeProcessingPolicy extends BaseEdgeProcessingPolicy {
} // StereotypeEdgeProcessingPolicy
