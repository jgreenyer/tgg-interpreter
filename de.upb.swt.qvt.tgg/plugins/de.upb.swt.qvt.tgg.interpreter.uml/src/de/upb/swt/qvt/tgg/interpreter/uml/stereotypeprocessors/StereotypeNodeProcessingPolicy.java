/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors;

import de.upb.swt.qvt.tgg.interpreter.BaseNodeProcessingPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stereotype Node Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage#getStereotypeNodeProcessingPolicy()
 * @model
 * @generated
 */
public interface StereotypeNodeProcessingPolicy extends BaseNodeProcessingPolicy {
} // StereotypeNodeProcessingPolicy
