package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.util;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Stereotype;

import de.upb.swt.qvt.qvtbase.Annotation;
import de.upb.swt.qvt.tgg.Node;


public final class Helper {
	
	public static final String negativeStereotypeString = "doNotMatch";
	public static final String positiveStereotypeString = "match";
	
	public static final String umlNsURI = "http://www.eclipse.org/uml2/3.0.0/UML";
	
	public static final String umlNsURIforProfiles = "http://www.eclipse.org/uml2/2.0.0/UML";
	
	public static EList<Stereotype> getNodesAppliedStereotypes(Node tggNode){
		EList<Stereotype> appliedStereotypesList = new UniqueEList<Stereotype>();
		if (!tggNode.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()
				&& !tggNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations().isEmpty()) {
			for (Annotation stereoTypeAnnotation : tggNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations()) {
				if (stereoTypeAnnotation.getKey().equals(Helper.positiveStereotypeString)){
					for (EObject referencedObject : stereoTypeAnnotation.getValue()) {
						if (referencedObject != null && referencedObject instanceof Stereotype)
							appliedStereotypesList.add((Stereotype) referencedObject);
					}
				}
			}
		}
		return appliedStereotypesList;
	}
	
	public static EList<Stereotype> getNodesAppliedNegativeStereotypes(Node tggNode){
		EList<Stereotype> appliedStereotypesList = new UniqueEList<Stereotype>();
		if (!tggNode.getAnnotationsByKeyString(Helper.umlNsURI).isEmpty()
				&& !tggNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations().isEmpty()) {
			for (Annotation stereoTypeAnnotation : tggNode.getAnnotationsByKeyString(Helper.umlNsURI).get(0).getAnnotations()) {
				if (stereoTypeAnnotation.getKey().equals(Helper.negativeStereotypeString)){
					for (EObject referencedObject : stereoTypeAnnotation.getValue()) {
						if (referencedObject != null && referencedObject instanceof Stereotype)
							appliedStereotypesList.add((Stereotype) referencedObject);
					}
				}
			}
		}
		return appliedStereotypesList;
	}

}
