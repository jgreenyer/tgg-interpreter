/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors;

import de.upb.swt.qvt.tgg.interpreter.DefaultOCLConstraintProcessingPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stereotype OCL Constraint Processing Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage#getStereotypeOCLConstraintProcessingPolicy()
 * @model
 * @generated
 */
public interface StereotypeOCLConstraintProcessingPolicy extends DefaultOCLConstraintProcessingPolicy {
} // StereotypeOCLConstraintProcessingPolicy
