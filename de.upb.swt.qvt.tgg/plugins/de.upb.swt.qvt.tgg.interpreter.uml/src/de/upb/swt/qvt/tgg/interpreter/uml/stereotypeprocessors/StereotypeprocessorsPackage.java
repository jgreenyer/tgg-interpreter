/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsFactory
 * @model kind="package"
 * @generated
 */
public interface StereotypeprocessorsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "stereotypeprocessors";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.upb.swt.qvt.tgg.interpreter.uml/stereotypeprocessors/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.upb.swt.qvt.tgg.interpreter.uml";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StereotypeprocessorsPackage eINSTANCE = de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeNodeProcessingPolicyImpl <em>Stereotype Node Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeNodeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeNodeProcessingPolicy()
	 * @generated
	 */
	int STEREOTYPE_NODE_PROCESSING_POLICY = 0;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = InterpreterPackage.BASE_NODE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_NODE_PROCESSING_POLICY__RULE_PROCESSOR = InterpreterPackage.BASE_NODE_PROCESSING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_NODE_PROCESSING_POLICY__GRAPH_MATCHER = InterpreterPackage.BASE_NODE_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The number of structural features of the '<em>Stereotype Node Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_NODE_PROCESSING_POLICY_FEATURE_COUNT = InterpreterPackage.BASE_NODE_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeEdgeProcessingPolicyImpl <em>Stereotype Edge Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeEdgeProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeEdgeProcessingPolicy()
	 * @generated
	 */
	int STEREOTYPE_EDGE_PROCESSING_POLICY = 1;

	/**
	 * The feature id for the '<em><b>Transformation Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_EDGE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR = InterpreterPackage.BASE_EDGE_PROCESSING_POLICY__TRANSFORMATION_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Rule Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_EDGE_PROCESSING_POLICY__RULE_PROCESSOR = InterpreterPackage.BASE_EDGE_PROCESSING_POLICY__RULE_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_EDGE_PROCESSING_POLICY__GRAPH_MATCHER = InterpreterPackage.BASE_EDGE_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The number of structural features of the '<em>Stereotype Edge Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_EDGE_PROCESSING_POLICY_FEATURE_COUNT = InterpreterPackage.BASE_EDGE_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeOCLConstraintProcessingPolicyImpl <em>Stereotype OCL Constraint Processing Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeOCLConstraintProcessingPolicyImpl
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeOCLConstraintProcessingPolicy()
	 * @generated
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY = 2;

	/**
	 * The feature id for the '<em><b>Graph Matcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__GRAPH_MATCHER;

	/**
	 * The feature id for the '<em><b>Unprocessable Constraints</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSABLE_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Unprocessed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__UNPROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Processed Constraints</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__PROCESSED_CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Rule Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY__RULE_BINDING;

	/**
	 * The number of structural features of the '<em>Stereotype OCL Constraint Processing Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY_FEATURE_COUNT = InterpreterPackage.DEFAULT_OCL_CONSTRAINT_PROCESSING_POLICY_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy <em>Stereotype Node Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stereotype Node Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy
	 * @generated
	 */
	EClass getStereotypeNodeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy <em>Stereotype Edge Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stereotype Edge Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy
	 * @generated
	 */
	EClass getStereotypeEdgeProcessingPolicy();

	/**
	 * Returns the meta object for class '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy <em>Stereotype OCL Constraint Processing Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stereotype OCL Constraint Processing Policy</em>'.
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy
	 * @generated
	 */
	EClass getStereotypeOCLConstraintProcessingPolicy();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StereotypeprocessorsFactory getStereotypeprocessorsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeNodeProcessingPolicyImpl <em>Stereotype Node Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeNodeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeNodeProcessingPolicy()
		 * @generated
		 */
		EClass STEREOTYPE_NODE_PROCESSING_POLICY = eINSTANCE.getStereotypeNodeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeEdgeProcessingPolicyImpl <em>Stereotype Edge Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeEdgeProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeEdgeProcessingPolicy()
		 * @generated
		 */
		EClass STEREOTYPE_EDGE_PROCESSING_POLICY = eINSTANCE.getStereotypeEdgeProcessingPolicy();

		/**
		 * The meta object literal for the '{@link de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeOCLConstraintProcessingPolicyImpl <em>Stereotype OCL Constraint Processing Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeOCLConstraintProcessingPolicyImpl
		 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl.StereotypeprocessorsPackageImpl#getStereotypeOCLConstraintProcessingPolicy()
		 * @generated
		 */
		EClass STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY = eINSTANCE.getStereotypeOCLConstraintProcessingPolicy();

	}

} //StereotypeprocessorsPackage
