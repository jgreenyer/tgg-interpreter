/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeEdgeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeNodeProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeOCLConstraintProcessingPolicy;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsFactory;
import de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StereotypeprocessorsPackageImpl extends EPackageImpl implements StereotypeprocessorsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stereotypeNodeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stereotypeEdgeProcessingPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stereotypeOCLConstraintProcessingPolicyEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.swt.qvt.tgg.interpreter.uml.stereotypeprocessors.StereotypeprocessorsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StereotypeprocessorsPackageImpl() {
		super(eNS_URI, StereotypeprocessorsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StereotypeprocessorsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StereotypeprocessorsPackage init() {
		if (isInited) return (StereotypeprocessorsPackage)EPackage.Registry.INSTANCE.getEPackage(StereotypeprocessorsPackage.eNS_URI);

		// Obtain or create and register package
		StereotypeprocessorsPackageImpl theStereotypeprocessorsPackage = (StereotypeprocessorsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StereotypeprocessorsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StereotypeprocessorsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		InterpreterPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theStereotypeprocessorsPackage.createPackageContents();

		// Initialize created meta-data
		theStereotypeprocessorsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStereotypeprocessorsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StereotypeprocessorsPackage.eNS_URI, theStereotypeprocessorsPackage);
		return theStereotypeprocessorsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStereotypeNodeProcessingPolicy() {
		return stereotypeNodeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStereotypeEdgeProcessingPolicy() {
		return stereotypeEdgeProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStereotypeOCLConstraintProcessingPolicy() {
		return stereotypeOCLConstraintProcessingPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StereotypeprocessorsFactory getStereotypeprocessorsFactory() {
		return (StereotypeprocessorsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		stereotypeNodeProcessingPolicyEClass = createEClass(STEREOTYPE_NODE_PROCESSING_POLICY);

		stereotypeEdgeProcessingPolicyEClass = createEClass(STEREOTYPE_EDGE_PROCESSING_POLICY);

		stereotypeOCLConstraintProcessingPolicyEClass = createEClass(STEREOTYPE_OCL_CONSTRAINT_PROCESSING_POLICY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InterpreterPackage theInterpreterPackage = (InterpreterPackage)EPackage.Registry.INSTANCE.getEPackage(InterpreterPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		stereotypeNodeProcessingPolicyEClass.getESuperTypes().add(theInterpreterPackage.getBaseNodeProcessingPolicy());
		stereotypeEdgeProcessingPolicyEClass.getESuperTypes().add(theInterpreterPackage.getBaseEdgeProcessingPolicy());
		stereotypeOCLConstraintProcessingPolicyEClass.getESuperTypes().add(theInterpreterPackage.getDefaultOCLConstraintProcessingPolicy());

		// Initialize classes and features; add operations and parameters
		initEClass(stereotypeNodeProcessingPolicyEClass, StereotypeNodeProcessingPolicy.class, "StereotypeNodeProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stereotypeEdgeProcessingPolicyEClass, StereotypeEdgeProcessingPolicy.class, "StereotypeEdgeProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stereotypeOCLConstraintProcessingPolicyEClass, StereotypeOCLConstraintProcessingPolicy.class, "StereotypeOCLConstraintProcessingPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //StereotypeprocessorsPackageImpl
