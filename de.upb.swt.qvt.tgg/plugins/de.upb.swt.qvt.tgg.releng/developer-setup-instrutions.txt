Check out this project
(https://dsd-serv.uni-paderborn.de/svn/m2m/trunk/de.upb.swt.qvt.tgg/plugins/de.upb.swt.qvt.tgg.releng)
and import projectSet_Interpreter-UI-editors.psf
(right-click on psf file-->Import Project Set... -- This checks out the
required plug-in projects from the repository)

Generate the following plug-ins using the respective genmodel files.
(Open the genmodel file with the genmodel tree editor, right-click on the root element and
choose to generate the edit/editor plug-ins)

de.upb.swt.qvt.edit                   (de.upb.swt.qvt\model\qvtbase.genmodel)
de.upb.swt.qvt.tgg.interpreter.edit   (de.upb.swt.qvt.tgg.interpreter\model\interpreterconfiguration.genmodel)

Switch to the plug-in developer perspective.

Configure an eclipse runtime-workspace.
(Run->Run Configurations...->select "Eclipse Application"->right-click->New)
Set the VM-Arguments to:
-Xms256m -Xmx256m -XX:PermSize=128m -XX:MaxPermSize=256m -ea
(Arguments-Tab, VM-Arguments text area.)
You can leave all other runtime workbench settings on default.