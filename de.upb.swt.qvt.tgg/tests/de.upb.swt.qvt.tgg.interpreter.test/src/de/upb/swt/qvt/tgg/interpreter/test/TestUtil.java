package de.upb.swt.qvt.tgg.interpreter.test;

import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.util.ModelUtils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

public class TestUtil {
	public static Configuration loadConfiguration(String path) {
		path = "de.upb.swt.qvt.tgg.interpreter.test/testcases/" + path;
		URI configFile = URI.createPlatformPluginURI(path, true);
		return InterpreterconfigurationUtil.loadInterpreterConfiguration(configFile);
	}
	
	public static class InterpreterResult {
		EList<EObject> rootObjects;
		Configuration config;
	}

	public static InterpreterResult runTransformation(Configuration configuration) {
		final Configuration config = configuration;
		
		/*final Job job = new Job("TGG Transformation: " + configuration.getTripleGraphGrammar().getName()){

			protected IStatus run(IProgressMonitor monitor) {*/
				Interpreter interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
				interpreter.setConfiguration(config);
				interpreter.initializeConfiguration();
				IStatus returnStatus = interpreter.performTransformation(null);

	        	postTransformationFinished(returnStatus, interpreter);

	        	InterpreterResult result = new InterpreterResult();
	        	result.rootObjects = interpreter.getConfiguration().getDomainRootObjects();
	        	result.config = interpreter.getConfiguration();
	        	
				return result;
			/*}
			
		};
					
	    job.setUser(true);
	    job.schedule();*/
	}

	public static void postTransformationFinished(final IStatus resultStatus, final Interpreter interpreter) {
		//interpreter.storeResult();
		//interpreter.storeConfiguration();
	}
	
	public static boolean checkFileEquality(EObject model1, String file2) {
		ResourceSet resourceSet2 = model1.eResource().getResourceSet();
		
		file2 = "de.upb.swt.qvt.tgg.interpreter.test/testcases/" + file2;
		URI modelURI2 = URI.createPlatformPluginURI(file2, true);


		// Loading models
		EObject model2;
		try {
			model2 = ModelUtils.load(modelURI2, resourceSet2);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		 
		// Matching model elements
		/*MatchModel match;
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(MatchOptions.OPTION_IGNORE_XMI_ID, Boolean.TRUE);
		options.put(MatchOptions.OPTION_DISTINCT_METAMODELS, Boolean.FALSE);
		try {
			match = MatchService.doMatch(model1, model2, options);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		// Computing differences
		DiffModel diff = DiffService.doDiff(match, false);
		if (diff.getOwnedElements().isEmpty())*/
		if (EcoreUtil.equals(model1, model2))
			return true;
		else
			return false;
		
		
		// Merges all differences from model1 to model2
		/*List<DiffElement> differences = new ArrayList<DiffElement>(diff.getOwnedElements());
		MergeService.merge(differences, true);*/
	}
}
