package de.upb.swt.qvt.tgg.interpreter.test;


import org.eclipse.emf.ecore.EObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.upb.swt.qvt.tgg.interpreter.test.TestUtil.InterpreterResult;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;

public class ProfileTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test_profile_batch() {
		Configuration config = TestUtil.loadConfiguration("profile/configuration/profile.interpreterconfiguration");
		InterpreterResult transformationResult = TestUtil.runTransformation(config);
		EObject target = transformationResult.rootObjects.get(6);

		org.junit.Assert.assertTrue(TestUtil.checkFileEquality(target, "profile/models/oracle/target.uml"));
	}

}
