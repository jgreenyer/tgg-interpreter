/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.test;

import org.eclipse.emf.ecore.EObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TggFactory;
import de.upb.swt.qvt.tgg.interpreter.util.PackageAccess;
import de.upb.swt.qvt.tgg.interpreter.util.TupleGenerator;

public class TupleGeneratorTest {

	protected TupleGenerator fixture = null;

	/**
	 * Sets the fixture for this Tuple Generator test case.
	 */
	protected void setFixture(TupleGenerator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Tuple Generator test case.
	 */
	protected TupleGenerator getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated NOT
	 */
	@Before
	protected void setUp() {

		Node node11 = TggFactory.eINSTANCE.createNode();
		node11.setName("1");
		Node node12 = TggFactory.eINSTANCE.createNode();
		node12.setName("2");
		Node node13 = TggFactory.eINSTANCE.createNode();
		node13.setName("3");
		Object[][] inputArrays = { { node11, node12, node13 }, { node11, node12, node13 }, { node11, node12, node13 } };

		setFixture(new TupleGenerator(inputArrays));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@After
	protected void tearDown() {
		setFixture(null);
	}

	private Object[] tuple;

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getNextTupel()
	 * <em>Get Next Tupel</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#getNextTupel()
	 * @generated NOT
	 */
	@Test
	public void testGetNextTupel() {
		tuple = getFixture().getNextTuple();
		int tupelValue = 0;
		while (tuple.length != 0) {
			int newTupelValue = parseTupelValue(tuple);
			org.junit.Assert.assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}
		org.junit.Assert.assertEquals(tupelValue, 333);
	}

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#replaceInputList(int, org.eclipse.emf.common.util.EList)
	 * <em>Replace Input List</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#replaceInputList(int,
	 *      org.eclipse.emf.common.util.EList)
	 * @generated NOT
	 */
	@Test
	public void testReplaceInputList__int_EList() {
		tuple = getFixture().getNextTuple();
		int tupelValue = 0;
		while (tuple.length != 0 && tupelValue != 222) {
			int newTupelValue = parseTupelValue(tuple);
			System.out.println(newTupelValue);
			org.junit.Assert.assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}

		Node node1 = TggFactory.eINSTANCE.createNode();
		node1.setName("1");
		Node node2 = TggFactory.eINSTANCE.createNode();
		node2.setName("2");
		Node node3 = TggFactory.eINSTANCE.createNode();
		node3.setName("3");
		Node node4 = TggFactory.eINSTANCE.createNode();
		node4.setName("4");

		EObject[] replaceArray = { node1, node2, node3, node4 };

		System.out.println("Replacing...");

		PackageAccess.replaceInputArray(getFixture(), 1, replaceArray);

		tuple = getFixture().getNextTuple();

		org.junit.Assert.assertEquals(211, parseTupelValue(tuple));

		System.out.println();

		tupelValue = 211;

		while (tuple.length != 0) {
			int newTupelValue = parseTupelValue(tuple);
			System.out.println(newTupelValue);
			org.junit.Assert.assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
			tuple = getFixture().getNextTuple();
		}
		org.junit.Assert.assertEquals(tupelValue, 343);
	}

	/**
	 * Tests the '
	 * {@link de.upb.swt.qvt.tgg.interpreter.TupelGenerator#hasNext()
	 * <em>Has Next</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.upb.swt.qvt.tgg.interpreter.TupelGenerator#hasNext()
	 * @generated NOT
	 */
	@Test
	public void testHasNext() {
		int tupelValue = 0;
		while (getFixture().hasNext()) {
			tuple = getFixture().getNextTuple();
			org.junit.Assert.assertTrue(tuple.length != 0);
			int newTupelValue = parseTupelValue(tuple);
			org.junit.Assert.assertTrue(tupelValue <= newTupelValue);
			tupelValue = newTupelValue;
		}
		org.junit.Assert.assertEquals(333, tupelValue);
	}

	private int parseTupelValue(Object[] tuple) {

		String tupelString = "";

		for (Object obj : tuple) {
			assert (obj != null);

			tupelString = tupelString.concat(((Node) obj).getName());
		}

		return Integer.parseInt(tupelString);

	}

} // TupelGeneratorTest
