/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.interpreter.util;

import org.eclipse.emf.ecore.EObject;

public class PackageAccess {

	public static void replaceInputArray(TupleGenerator fixture, int position, EObject[] replaceArray) {
		fixture.replaceInputArray(position, replaceArray);
	}
}
