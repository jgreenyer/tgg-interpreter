/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetAdapterFactory.java 1651 2006-07-07 12:47:00 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.util;

import beispiel.model.ctools2pnet.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage
 * @generated
 */
public class Ctools2pnetAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ctools2pnetPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctools2pnetAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Ctools2pnetPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch the delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ctools2pnetSwitch modelSwitch =
		new Ctools2pnetSwitch() {
			public Object caseTProjectToPetrinet(TProjectToPetrinet object) {
				return createTProjectToPetrinetAdapter();
			}
			public Object caseTTrackToPetrinet(TTrackToPetrinet object) {
				return createTTrackToPetrinetAdapter();
			}
			public Object caseTPortToPlace(TPortToPlace object) {
				return createTPortToPlaceAdapter();
			}
			public Object caseTPortToTransition(TPortToTransition object) {
				return createTPortToTransitionAdapter();
			}
			public Object caseTConnectionToArc(TConnectionToArc object) {
				return createTConnectionToArcAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link beispiel.model.ctools2pnet.TProjectToPetrinet <em>TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet
	 * @generated
	 */
	public Adapter createTProjectToPetrinetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link beispiel.model.ctools2pnet.TTrackToPetrinet <em>TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet
	 * @generated
	 */
	public Adapter createTTrackToPetrinetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link beispiel.model.ctools2pnet.TPortToPlace <em>TPort To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see beispiel.model.ctools2pnet.TPortToPlace
	 * @generated
	 */
	public Adapter createTPortToPlaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link beispiel.model.ctools2pnet.TPortToTransition <em>TPort To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see beispiel.model.ctools2pnet.TPortToTransition
	 * @generated
	 */
	public Adapter createTPortToTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link beispiel.model.ctools2pnet.TConnectionToArc <em>TConnection To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see beispiel.model.ctools2pnet.TConnectionToArc
	 * @generated
	 */
	public Adapter createTConnectionToArcAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Ctools2pnetAdapterFactory
