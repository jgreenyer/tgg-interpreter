/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetSwitch.java 1651 2006-07-07 12:47:00 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.util;

import beispiel.model.ctools2pnet.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage
 * @generated
 */
public class Ctools2pnetSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Ctools2pnetPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctools2pnetSwitch() {
		if (modelPackage == null) {
			modelPackage = Ctools2pnetPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET: {
				TProjectToPetrinet tProjectToPetrinet = (TProjectToPetrinet)theEObject;
				Object result = caseTProjectToPetrinet(tProjectToPetrinet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ctools2pnetPackage.TTRACK_TO_PETRINET: {
				TTrackToPetrinet tTrackToPetrinet = (TTrackToPetrinet)theEObject;
				Object result = caseTTrackToPetrinet(tTrackToPetrinet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ctools2pnetPackage.TPORT_TO_PLACE: {
				TPortToPlace tPortToPlace = (TPortToPlace)theEObject;
				Object result = caseTPortToPlace(tPortToPlace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ctools2pnetPackage.TPORT_TO_TRANSITION: {
				TPortToTransition tPortToTransition = (TPortToTransition)theEObject;
				Object result = caseTPortToTransition(tPortToTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Ctools2pnetPackage.TCONNECTION_TO_ARC: {
				TConnectionToArc tConnectionToArc = (TConnectionToArc)theEObject;
				Object result = caseTConnectionToArc(tConnectionToArc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>TProject To Petrinet</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>TProject To Petrinet</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTProjectToPetrinet(TProjectToPetrinet object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>TTrack To Petrinet</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>TTrack To Petrinet</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTTrackToPetrinet(TTrackToPetrinet object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>TPort To Place</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>TPort To Place</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTPortToPlace(TPortToPlace object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>TPort To Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>TPort To Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTPortToTransition(TPortToTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>TConnection To Arc</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>TConnection To Arc</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTConnectionToArc(TConnectionToArc object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //Ctools2pnetSwitch
