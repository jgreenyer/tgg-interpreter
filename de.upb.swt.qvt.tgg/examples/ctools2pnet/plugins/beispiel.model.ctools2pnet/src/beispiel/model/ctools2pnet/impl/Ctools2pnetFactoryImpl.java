/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetFactoryImpl.java 1651 2006-07-07 12:47:00 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools2pnet.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ctools2pnetFactoryImpl extends EFactoryImpl implements Ctools2pnetFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Ctools2pnetFactory init() {
		try {
			Ctools2pnetFactory theCtools2pnetFactory = (Ctools2pnetFactory)EPackage.Registry.INSTANCE.getEFactory("http://beispiel.model.ctools2pnet"); 
			if (theCtools2pnetFactory != null) {
				return theCtools2pnetFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Ctools2pnetFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctools2pnetFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET: return createTProjectToPetrinet();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET: return createTTrackToPetrinet();
			case Ctools2pnetPackage.TPORT_TO_PLACE: return createTPortToPlace();
			case Ctools2pnetPackage.TPORT_TO_TRANSITION: return createTPortToTransition();
			case Ctools2pnetPackage.TCONNECTION_TO_ARC: return createTConnectionToArc();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TProjectToPetrinet createTProjectToPetrinet() {
		TProjectToPetrinetImpl tProjectToPetrinet = new TProjectToPetrinetImpl();
		return tProjectToPetrinet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TTrackToPetrinet createTTrackToPetrinet() {
		TTrackToPetrinetImpl tTrackToPetrinet = new TTrackToPetrinetImpl();
		return tTrackToPetrinet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TPortToPlace createTPortToPlace() {
		TPortToPlaceImpl tPortToPlace = new TPortToPlaceImpl();
		return tPortToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TPortToTransition createTPortToTransition() {
		TPortToTransitionImpl tPortToTransition = new TPortToTransitionImpl();
		return tPortToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TConnectionToArc createTConnectionToArc() {
		TConnectionToArcImpl tConnectionToArc = new TConnectionToArcImpl();
		return tConnectionToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctools2pnetPackage getCtools2pnetPackage() {
		return (Ctools2pnetPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static Ctools2pnetPackage getPackage() {
		return Ctools2pnetPackage.eINSTANCE;
	}

} //Ctools2pnetFactoryImpl
