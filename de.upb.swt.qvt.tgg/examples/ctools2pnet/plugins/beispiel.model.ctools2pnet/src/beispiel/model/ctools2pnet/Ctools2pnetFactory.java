/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetFactory.java 1651 2006-07-07 12:47:00 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage
 * @generated
 */
public interface Ctools2pnetFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ctools2pnetFactory eINSTANCE = beispiel.model.ctools2pnet.impl.Ctools2pnetFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TProject To Petrinet</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TProject To Petrinet</em>'.
	 * @generated
	 */
	TProjectToPetrinet createTProjectToPetrinet();

	/**
	 * Returns a new object of class '<em>TTrack To Petrinet</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TTrack To Petrinet</em>'.
	 * @generated
	 */
	TTrackToPetrinet createTTrackToPetrinet();

	/**
	 * Returns a new object of class '<em>TPort To Place</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TPort To Place</em>'.
	 * @generated
	 */
	TPortToPlace createTPortToPlace();

	/**
	 * Returns a new object of class '<em>TPort To Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TPort To Transition</em>'.
	 * @generated
	 */
	TPortToTransition createTPortToTransition();

	/**
	 * Returns a new object of class '<em>TConnection To Arc</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TConnection To Arc</em>'.
	 * @generated
	 */
	TConnectionToArc createTConnectionToArc();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Ctools2pnetPackage getCtools2pnetPackage();

} //Ctools2pnetFactory
