/**
 * <copyright>
 * </copyright>
 *
 * $Id: TConnectionToArcImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.Connection;

import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TConnectionToArc;

import beispiel.model.ctools2pnet.TProjectToPetrinet;

import beispiel.model.pnet.Arc;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TConnection To Arc</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TConnectionToArcImpl#getConnectionToArcToConnection <em>Connection To Arc To Connection</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TConnectionToArcImpl#getConnectionToArcToArc <em>Connection To Arc To Arc</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TConnectionToArcImpl#getTConnectionToArcToTProjectToPetrinet <em>TConnection To Arc To TProject To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TConnectionToArcImpl extends EObjectImpl implements TConnectionToArc {
	/**
	 * The cached value of the '{@link #getConnectionToArcToConnection() <em>Connection To Arc To Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionToArcToConnection()
	 * @generated
	 * @ordered
	 */
	protected Connection connectionToArcToConnection = null;

	/**
	 * The cached value of the '{@link #getConnectionToArcToArc() <em>Connection To Arc To Arc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionToArcToArc()
	 * @generated
	 * @ordered
	 */
	protected Arc connectionToArcToArc = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TConnectionToArcImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Ctools2pnetPackage.Literals.TCONNECTION_TO_ARC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection getConnectionToArcToConnection() {
		if (connectionToArcToConnection != null && connectionToArcToConnection.eIsProxy()) {
			InternalEObject oldConnectionToArcToConnection = (InternalEObject)connectionToArcToConnection;
			connectionToArcToConnection = (Connection)eResolveProxy(oldConnectionToArcToConnection);
			if (connectionToArcToConnection != oldConnectionToArcToConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION, oldConnectionToArcToConnection, connectionToArcToConnection));
			}
		}
		return connectionToArcToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection basicGetConnectionToArcToConnection() {
		return connectionToArcToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionToArcToConnection(Connection newConnectionToArcToConnection) {
		Connection oldConnectionToArcToConnection = connectionToArcToConnection;
		connectionToArcToConnection = newConnectionToArcToConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION, oldConnectionToArcToConnection, connectionToArcToConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Arc getConnectionToArcToArc() {
		if (connectionToArcToArc != null && connectionToArcToArc.eIsProxy()) {
			InternalEObject oldConnectionToArcToArc = (InternalEObject)connectionToArcToArc;
			connectionToArcToArc = (Arc)eResolveProxy(oldConnectionToArcToArc);
			if (connectionToArcToArc != oldConnectionToArcToArc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC, oldConnectionToArcToArc, connectionToArcToArc));
			}
		}
		return connectionToArcToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Arc basicGetConnectionToArcToArc() {
		return connectionToArcToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionToArcToArc(Arc newConnectionToArcToArc) {
		Arc oldConnectionToArcToArc = connectionToArcToArc;
		connectionToArcToArc = newConnectionToArcToArc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC, oldConnectionToArcToArc, connectionToArcToArc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TProjectToPetrinet getTConnectionToArcToTProjectToPetrinet() {
		if (eContainerFeatureID != Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET) return null;
		return (TProjectToPetrinet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTConnectionToArcToTProjectToPetrinet(TProjectToPetrinet newTConnectionToArcToTProjectToPetrinet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTConnectionToArcToTProjectToPetrinet, Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTConnectionToArcToTProjectToPetrinet(TProjectToPetrinet newTConnectionToArcToTProjectToPetrinet) {
		if (newTConnectionToArcToTProjectToPetrinet != eInternalContainer() || (eContainerFeatureID != Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET && newTConnectionToArcToTProjectToPetrinet != null)) {
			if (EcoreUtil.isAncestor(this, newTConnectionToArcToTProjectToPetrinet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTConnectionToArcToTProjectToPetrinet != null)
				msgs = ((InternalEObject)newTConnectionToArcToTProjectToPetrinet).eInverseAdd(this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC, TProjectToPetrinet.class, msgs);
			msgs = basicSetTConnectionToArcToTProjectToPetrinet(newTConnectionToArcToTProjectToPetrinet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET, newTConnectionToArcToTProjectToPetrinet, newTConnectionToArcToTProjectToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTConnectionToArcToTProjectToPetrinet((TProjectToPetrinet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				return basicSetTConnectionToArcToTProjectToPetrinet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				return eInternalContainer().eInverseRemove(this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC, TProjectToPetrinet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION:
				if (resolve) return getConnectionToArcToConnection();
				return basicGetConnectionToArcToConnection();
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC:
				if (resolve) return getConnectionToArcToArc();
				return basicGetConnectionToArcToArc();
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				return getTConnectionToArcToTProjectToPetrinet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION:
				setConnectionToArcToConnection((Connection)newValue);
				return;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC:
				setConnectionToArcToArc((Arc)newValue);
				return;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				setTConnectionToArcToTProjectToPetrinet((TProjectToPetrinet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION:
				setConnectionToArcToConnection((Connection)null);
				return;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC:
				setConnectionToArcToArc((Arc)null);
				return;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				setTConnectionToArcToTProjectToPetrinet((TProjectToPetrinet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION:
				return connectionToArcToConnection != null;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC:
				return connectionToArcToArc != null;
			case Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET:
				return getTConnectionToArcToTProjectToPetrinet() != null;
		}
		return super.eIsSet(featureID);
	}

} //TConnectionToArcImpl