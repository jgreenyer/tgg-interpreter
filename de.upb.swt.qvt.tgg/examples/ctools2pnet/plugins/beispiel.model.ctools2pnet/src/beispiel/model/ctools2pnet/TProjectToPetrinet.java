/**
 * <copyright>
 * </copyright>
 *
 * $Id: TProjectToPetrinet.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import beispiel.model.ctools.Project;

import beispiel.model.pnet.Petrinet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TProject To Petrinet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToProject <em>TProject To Petrinet To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToPetrinet <em>TProject To Petrinet To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTTrackToPetrinet <em>TProject To Petrinet To TTrack To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTConnectionToArc <em>TProject To Petrinet To TConnection To Arc</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTProjectToPetrinet()
 * @model
 * @generated
 */
public interface TProjectToPetrinet extends EObject {
	/**
	 * Returns the value of the '<em><b>TProject To Petrinet To Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TProject To Petrinet To Project</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TProject To Petrinet To Project</em>' reference.
	 * @see #setTProjectToPetrinetToProject(Project)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTProjectToPetrinet_TProjectToPetrinetToProject()
	 * @model
	 * @generated
	 */
	Project getTProjectToPetrinetToProject();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToProject <em>TProject To Petrinet To Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TProject To Petrinet To Project</em>' reference.
	 * @see #getTProjectToPetrinetToProject()
	 * @generated
	 */
	void setTProjectToPetrinetToProject(Project value);

	/**
	 * Returns the value of the '<em><b>TProject To Petrinet To Petrinet</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TProject To Petrinet To Petrinet</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TProject To Petrinet To Petrinet</em>' reference.
	 * @see #setTProjectToPetrinetToPetrinet(Petrinet)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTProjectToPetrinet_TProjectToPetrinetToPetrinet()
	 * @model
	 * @generated
	 */
	Petrinet getTProjectToPetrinetToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToPetrinet <em>TProject To Petrinet To Petrinet</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TProject To Petrinet To Petrinet</em>' reference.
	 * @see #getTProjectToPetrinetToPetrinet()
	 * @generated
	 */
	void setTProjectToPetrinetToPetrinet(Petrinet value);

	/**
	 * Returns the value of the '<em><b>TProject To Petrinet To TTrack To Petrinet</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools2pnet.TTrackToPetrinet}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet <em>TTrack To Petrinet To TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TProject To Petrinet To TTrack To Petrinet</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TProject To Petrinet To TTrack To Petrinet</em>' containment reference list.
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet()
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet
	 * @model type="beispiel.model.ctools2pnet.TTrackToPetrinet" opposite="tTrackToPetrinetToTProjectToPetrinet" containment="true"
	 * @generated
	 */
	EList getTProjectToPetrinetToTTrackToPetrinet();

	/**
	 * Returns the value of the '<em><b>TProject To Petrinet To TConnection To Arc</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools2pnet.TConnectionToArc}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet <em>TConnection To Arc To TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TProject To Petrinet To TConnection To Arc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TProject To Petrinet To TConnection To Arc</em>' containment reference list.
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc()
	 * @see beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet
	 * @model type="beispiel.model.ctools2pnet.TConnectionToArc" opposite="tConnectionToArcToTProjectToPetrinet" containment="true"
	 * @generated
	 */
	EList getTProjectToPetrinetToTConnectionToArc();

} // TProjectToPetrinet