/**
 * <copyright>
 * </copyright>
 *
 * $Id: TPortToPlaceImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.Port;

import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TPortToPlace;

import beispiel.model.ctools2pnet.TTrackToPetrinet;

import beispiel.model.pnet.Place;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TPort To Place</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToPlaceImpl#getTPortToPlaceToPort <em>TPort To Place To Port</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToPlaceImpl#getTPortToPlaceToPlace <em>TPort To Place To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToPlaceImpl#getTPortToPlaceToTTrackToPetrinet <em>TPort To Place To TTrack To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TPortToPlaceImpl extends EObjectImpl implements TPortToPlace {
	/**
	 * The cached value of the '{@link #getTPortToPlaceToPort() <em>TPort To Place To Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPortToPlaceToPort()
	 * @generated
	 * @ordered
	 */
	protected Port tPortToPlaceToPort = null;

	/**
	 * The cached value of the '{@link #getTPortToPlaceToPlace() <em>TPort To Place To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPortToPlaceToPlace()
	 * @generated
	 * @ordered
	 */
	protected Place tPortToPlaceToPlace = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TPortToPlaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Ctools2pnetPackage.Literals.TPORT_TO_PLACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getTPortToPlaceToPort() {
		if (tPortToPlaceToPort != null && tPortToPlaceToPort.eIsProxy()) {
			InternalEObject oldTPortToPlaceToPort = (InternalEObject)tPortToPlaceToPort;
			tPortToPlaceToPort = (Port)eResolveProxy(oldTPortToPlaceToPort);
			if (tPortToPlaceToPort != oldTPortToPlaceToPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT, oldTPortToPlaceToPort, tPortToPlaceToPort));
			}
		}
		return tPortToPlaceToPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetTPortToPlaceToPort() {
		return tPortToPlaceToPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToPlaceToPort(Port newTPortToPlaceToPort) {
		Port oldTPortToPlaceToPort = tPortToPlaceToPort;
		tPortToPlaceToPort = newTPortToPlaceToPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT, oldTPortToPlaceToPort, tPortToPlaceToPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place getTPortToPlaceToPlace() {
		if (tPortToPlaceToPlace != null && tPortToPlaceToPlace.eIsProxy()) {
			InternalEObject oldTPortToPlaceToPlace = (InternalEObject)tPortToPlaceToPlace;
			tPortToPlaceToPlace = (Place)eResolveProxy(oldTPortToPlaceToPlace);
			if (tPortToPlaceToPlace != oldTPortToPlaceToPlace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE, oldTPortToPlaceToPlace, tPortToPlaceToPlace));
			}
		}
		return tPortToPlaceToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place basicGetTPortToPlaceToPlace() {
		return tPortToPlaceToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToPlaceToPlace(Place newTPortToPlaceToPlace) {
		Place oldTPortToPlaceToPlace = tPortToPlaceToPlace;
		tPortToPlaceToPlace = newTPortToPlaceToPlace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE, oldTPortToPlaceToPlace, tPortToPlaceToPlace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TTrackToPetrinet getTPortToPlaceToTTrackToPetrinet() {
		if (eContainerFeatureID != Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET) return null;
		return (TTrackToPetrinet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTPortToPlaceToTTrackToPetrinet(TTrackToPetrinet newTPortToPlaceToTTrackToPetrinet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTPortToPlaceToTTrackToPetrinet, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToPlaceToTTrackToPetrinet(TTrackToPetrinet newTPortToPlaceToTTrackToPetrinet) {
		if (newTPortToPlaceToTTrackToPetrinet != eInternalContainer() || (eContainerFeatureID != Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET && newTPortToPlaceToTTrackToPetrinet != null)) {
			if (EcoreUtil.isAncestor(this, newTPortToPlaceToTTrackToPetrinet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTPortToPlaceToTTrackToPetrinet != null)
				msgs = ((InternalEObject)newTPortToPlaceToTTrackToPetrinet).eInverseAdd(this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE, TTrackToPetrinet.class, msgs);
			msgs = basicSetTPortToPlaceToTTrackToPetrinet(newTPortToPlaceToTTrackToPetrinet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET, newTPortToPlaceToTTrackToPetrinet, newTPortToPlaceToTTrackToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTPortToPlaceToTTrackToPetrinet((TTrackToPetrinet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				return basicSetTPortToPlaceToTTrackToPetrinet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				return eInternalContainer().eInverseRemove(this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE, TTrackToPetrinet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT:
				if (resolve) return getTPortToPlaceToPort();
				return basicGetTPortToPlaceToPort();
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE:
				if (resolve) return getTPortToPlaceToPlace();
				return basicGetTPortToPlaceToPlace();
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				return getTPortToPlaceToTTrackToPetrinet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT:
				setTPortToPlaceToPort((Port)newValue);
				return;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE:
				setTPortToPlaceToPlace((Place)newValue);
				return;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				setTPortToPlaceToTTrackToPetrinet((TTrackToPetrinet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT:
				setTPortToPlaceToPort((Port)null);
				return;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE:
				setTPortToPlaceToPlace((Place)null);
				return;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				setTPortToPlaceToTTrackToPetrinet((TTrackToPetrinet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT:
				return tPortToPlaceToPort != null;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE:
				return tPortToPlaceToPlace != null;
			case Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET:
				return getTPortToPlaceToTTrackToPetrinet() != null;
		}
		return super.eIsSet(featureID);
	}

} //TPortToPlaceImpl