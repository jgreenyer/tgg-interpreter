/**
 * <copyright>
 * </copyright>
 *
 * $Id: TConnectionToArc.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import beispiel.model.ctools.Connection;

import beispiel.model.pnet.Arc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TConnection To Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToConnection <em>Connection To Arc To Connection</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToArc <em>Connection To Arc To Arc</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet <em>TConnection To Arc To TProject To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTConnectionToArc()
 * @model
 * @generated
 */
public interface TConnectionToArc extends EObject {
	/**
	 * Returns the value of the '<em><b>Connection To Arc To Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection To Arc To Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection To Arc To Connection</em>' reference.
	 * @see #setConnectionToArcToConnection(Connection)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTConnectionToArc_ConnectionToArcToConnection()
	 * @model
	 * @generated
	 */
	Connection getConnectionToArcToConnection();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToConnection <em>Connection To Arc To Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection To Arc To Connection</em>' reference.
	 * @see #getConnectionToArcToConnection()
	 * @generated
	 */
	void setConnectionToArcToConnection(Connection value);

	/**
	 * Returns the value of the '<em><b>Connection To Arc To Arc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection To Arc To Arc</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection To Arc To Arc</em>' reference.
	 * @see #setConnectionToArcToArc(Arc)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTConnectionToArc_ConnectionToArcToArc()
	 * @model
	 * @generated
	 */
	Arc getConnectionToArcToArc();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToArc <em>Connection To Arc To Arc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection To Arc To Arc</em>' reference.
	 * @see #getConnectionToArcToArc()
	 * @generated
	 */
	void setConnectionToArcToArc(Arc value);

	/**
	 * Returns the value of the '<em><b>TConnection To Arc To TProject To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTConnectionToArc <em>TProject To Petrinet To TConnection To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TConnection To Arc To TProject To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TConnection To Arc To TProject To Petrinet</em>' container reference.
	 * @see #setTConnectionToArcToTProjectToPetrinet(TProjectToPetrinet)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTConnectionToArc_TConnectionToArcToTProjectToPetrinet()
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTConnectionToArc
	 * @model opposite="tProjectToPetrinetToTConnectionToArc"
	 * @generated
	 */
	TProjectToPetrinet getTConnectionToArcToTProjectToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet <em>TConnection To Arc To TProject To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TConnection To Arc To TProject To Petrinet</em>' container reference.
	 * @see #getTConnectionToArcToTProjectToPetrinet()
	 * @generated
	 */
	void setTConnectionToArcToTProjectToPetrinet(TProjectToPetrinet value);

} // TConnectionToArc