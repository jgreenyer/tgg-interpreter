/**
 * <copyright>
 * </copyright>
 *
 * $Id: TPortToTransitionImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.Port;

import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TPortToTransition;

import beispiel.model.ctools2pnet.TTrackToPetrinet;

import beispiel.model.pnet.Transition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TPort To Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToTransitionImpl#getTPortToTransitionToTransition <em>TPort To Transition To Transition</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToTransitionImpl#getTPortToTransitionToPort <em>TPort To Transition To Port</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TPortToTransitionImpl#getTPortToTransitionToTTrackToPetrinet <em>TPort To Transition To TTrack To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TPortToTransitionImpl extends EObjectImpl implements TPortToTransition {
	/**
	 * The cached value of the '{@link #getTPortToTransitionToTransition() <em>TPort To Transition To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPortToTransitionToTransition()
	 * @generated
	 * @ordered
	 */
	protected Transition tPortToTransitionToTransition = null;

	/**
	 * The cached value of the '{@link #getTPortToTransitionToPort() <em>TPort To Transition To Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPortToTransitionToPort()
	 * @generated
	 * @ordered
	 */
	protected Port tPortToTransitionToPort = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TPortToTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Ctools2pnetPackage.Literals.TPORT_TO_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getTPortToTransitionToTransition() {
		if (tPortToTransitionToTransition != null && tPortToTransitionToTransition.eIsProxy()) {
			InternalEObject oldTPortToTransitionToTransition = (InternalEObject)tPortToTransitionToTransition;
			tPortToTransitionToTransition = (Transition)eResolveProxy(oldTPortToTransitionToTransition);
			if (tPortToTransitionToTransition != oldTPortToTransitionToTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION, oldTPortToTransitionToTransition, tPortToTransitionToTransition));
			}
		}
		return tPortToTransitionToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetTPortToTransitionToTransition() {
		return tPortToTransitionToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToTransitionToTransition(Transition newTPortToTransitionToTransition) {
		Transition oldTPortToTransitionToTransition = tPortToTransitionToTransition;
		tPortToTransitionToTransition = newTPortToTransitionToTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION, oldTPortToTransitionToTransition, tPortToTransitionToTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getTPortToTransitionToPort() {
		if (tPortToTransitionToPort != null && tPortToTransitionToPort.eIsProxy()) {
			InternalEObject oldTPortToTransitionToPort = (InternalEObject)tPortToTransitionToPort;
			tPortToTransitionToPort = (Port)eResolveProxy(oldTPortToTransitionToPort);
			if (tPortToTransitionToPort != oldTPortToTransitionToPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT, oldTPortToTransitionToPort, tPortToTransitionToPort));
			}
		}
		return tPortToTransitionToPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetTPortToTransitionToPort() {
		return tPortToTransitionToPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToTransitionToPort(Port newTPortToTransitionToPort) {
		Port oldTPortToTransitionToPort = tPortToTransitionToPort;
		tPortToTransitionToPort = newTPortToTransitionToPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT, oldTPortToTransitionToPort, tPortToTransitionToPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TTrackToPetrinet getTPortToTransitionToTTrackToPetrinet() {
		if (eContainerFeatureID != Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET) return null;
		return (TTrackToPetrinet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTPortToTransitionToTTrackToPetrinet(TTrackToPetrinet newTPortToTransitionToTTrackToPetrinet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTPortToTransitionToTTrackToPetrinet, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPortToTransitionToTTrackToPetrinet(TTrackToPetrinet newTPortToTransitionToTTrackToPetrinet) {
		if (newTPortToTransitionToTTrackToPetrinet != eInternalContainer() || (eContainerFeatureID != Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET && newTPortToTransitionToTTrackToPetrinet != null)) {
			if (EcoreUtil.isAncestor(this, newTPortToTransitionToTTrackToPetrinet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTPortToTransitionToTTrackToPetrinet != null)
				msgs = ((InternalEObject)newTPortToTransitionToTTrackToPetrinet).eInverseAdd(this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION, TTrackToPetrinet.class, msgs);
			msgs = basicSetTPortToTransitionToTTrackToPetrinet(newTPortToTransitionToTTrackToPetrinet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET, newTPortToTransitionToTTrackToPetrinet, newTPortToTransitionToTTrackToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTPortToTransitionToTTrackToPetrinet((TTrackToPetrinet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				return basicSetTPortToTransitionToTTrackToPetrinet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				return eInternalContainer().eInverseRemove(this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION, TTrackToPetrinet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION:
				if (resolve) return getTPortToTransitionToTransition();
				return basicGetTPortToTransitionToTransition();
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT:
				if (resolve) return getTPortToTransitionToPort();
				return basicGetTPortToTransitionToPort();
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				return getTPortToTransitionToTTrackToPetrinet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION:
				setTPortToTransitionToTransition((Transition)newValue);
				return;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT:
				setTPortToTransitionToPort((Port)newValue);
				return;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				setTPortToTransitionToTTrackToPetrinet((TTrackToPetrinet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION:
				setTPortToTransitionToTransition((Transition)null);
				return;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT:
				setTPortToTransitionToPort((Port)null);
				return;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				setTPortToTransitionToTTrackToPetrinet((TTrackToPetrinet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION:
				return tPortToTransitionToTransition != null;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT:
				return tPortToTransitionToPort != null;
			case Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET:
				return getTPortToTransitionToTTrackToPetrinet() != null;
		}
		return super.eIsSet(featureID);
	}

} //TPortToTransitionImpl