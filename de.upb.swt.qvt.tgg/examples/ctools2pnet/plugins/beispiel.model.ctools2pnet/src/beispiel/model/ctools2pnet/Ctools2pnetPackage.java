/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetPackage.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see beispiel.model.ctools2pnet.Ctools2pnetFactory
 * @model kind="package"
 * @generated
 */
public interface Ctools2pnetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ctools2pnet";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://beispiel.model.ctools2pnet";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "beispiel.model.ctools2pnet";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Ctools2pnetPackage eINSTANCE = beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl.init();

	/**
	 * The meta object id for the '{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl <em>TProject To Petrinet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl
	 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTProjectToPetrinet()
	 * @generated
	 */
	int TPROJECT_TO_PETRINET = 0;

	/**
	 * The feature id for the '<em><b>TProject To Petrinet To Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>TProject To Petrinet To Petrinet</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET = 1;

	/**
	 * The feature id for the '<em><b>TProject To Petrinet To TTrack To Petrinet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET = 2;

	/**
	 * The feature id for the '<em><b>TProject To Petrinet To TConnection To Arc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC = 3;

	/**
	 * The number of structural features of the '<em>TProject To Petrinet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPROJECT_TO_PETRINET_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl <em>TTrack To Petrinet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl
	 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTTrackToPetrinet()
	 * @generated
	 */
	int TTRACK_TO_PETRINET = 1;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Track</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK = 0;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Port Out</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT = 1;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Port In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN = 2;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Arc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC = 3;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE = 4;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION = 5;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To TProject To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET = 6;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To TPort To Place</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE = 7;

	/**
	 * The feature id for the '<em><b>TTrack To Petrinet To TPort To Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION = 8;

	/**
	 * The number of structural features of the '<em>TTrack To Petrinet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TTRACK_TO_PETRINET_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link beispiel.model.ctools2pnet.impl.TPortToPlaceImpl <em>TPort To Place</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.ctools2pnet.impl.TPortToPlaceImpl
	 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTPortToPlace()
	 * @generated
	 */
	int TPORT_TO_PLACE = 2;

	/**
	 * The feature id for the '<em><b>TPort To Place To Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT = 0;

	/**
	 * The feature id for the '<em><b>TPort To Place To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE = 1;

	/**
	 * The feature id for the '<em><b>TPort To Place To TTrack To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET = 2;

	/**
	 * The number of structural features of the '<em>TPort To Place</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_PLACE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link beispiel.model.ctools2pnet.impl.TPortToTransitionImpl <em>TPort To Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.ctools2pnet.impl.TPortToTransitionImpl
	 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTPortToTransition()
	 * @generated
	 */
	int TPORT_TO_TRANSITION = 3;

	/**
	 * The feature id for the '<em><b>TPort To Transition To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION = 0;

	/**
	 * The feature id for the '<em><b>TPort To Transition To Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT = 1;

	/**
	 * The feature id for the '<em><b>TPort To Transition To TTrack To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET = 2;

	/**
	 * The number of structural features of the '<em>TPort To Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPORT_TO_TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link beispiel.model.ctools2pnet.impl.TConnectionToArcImpl <em>TConnection To Arc</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.ctools2pnet.impl.TConnectionToArcImpl
	 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTConnectionToArc()
	 * @generated
	 */
	int TCONNECTION_TO_ARC = 4;

	/**
	 * The feature id for the '<em><b>Connection To Arc To Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION = 0;

	/**
	 * The feature id for the '<em><b>Connection To Arc To Arc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC = 1;

	/**
	 * The feature id for the '<em><b>TConnection To Arc To TProject To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET = 2;

	/**
	 * The number of structural features of the '<em>TConnection To Arc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCONNECTION_TO_ARC_FEATURE_COUNT = 3;


	/**
	 * Returns the meta object for class '{@link beispiel.model.ctools2pnet.TProjectToPetrinet <em>TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TProject To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet
	 * @generated
	 */
	EClass getTProjectToPetrinet();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToProject <em>TProject To Petrinet To Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TProject To Petrinet To Project</em>'.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToProject()
	 * @see #getTProjectToPetrinet()
	 * @generated
	 */
	EReference getTProjectToPetrinet_TProjectToPetrinetToProject();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToPetrinet <em>TProject To Petrinet To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TProject To Petrinet To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToPetrinet()
	 * @see #getTProjectToPetrinet()
	 * @generated
	 */
	EReference getTProjectToPetrinet_TProjectToPetrinetToPetrinet();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTTrackToPetrinet <em>TProject To Petrinet To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>TProject To Petrinet To TTrack To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTTrackToPetrinet()
	 * @see #getTProjectToPetrinet()
	 * @generated
	 */
	EReference getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTConnectionToArc <em>TProject To Petrinet To TConnection To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>TProject To Petrinet To TConnection To Arc</em>'.
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTConnectionToArc()
	 * @see #getTProjectToPetrinet()
	 * @generated
	 */
	EReference getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc();

	/**
	 * Returns the meta object for class '{@link beispiel.model.ctools2pnet.TTrackToPetrinet <em>TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TTrack To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet
	 * @generated
	 */
	EClass getTTrackToPetrinet();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTrack <em>TTrack To Petrinet To Track</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Track</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTrack()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToTrack();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortOut <em>TTrack To Petrinet To Port Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Port Out</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortOut()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToPortOut();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortIn <em>TTrack To Petrinet To Port In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Port In</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortIn()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToPortIn();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToArc <em>TTrack To Petrinet To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Arc</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToArc()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToArc();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPlace <em>TTrack To Petrinet To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Place</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPlace()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToPlace();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTransition <em>TTrack To Petrinet To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TTrack To Petrinet To Transition</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTransition()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToTransition();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet <em>TTrack To Petrinet To TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>TTrack To Petrinet To TProject To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToPlace <em>TTrack To Petrinet To TPort To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>TTrack To Petrinet To TPort To Place</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToPlace()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToTransition <em>TTrack To Petrinet To TPort To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>TTrack To Petrinet To TPort To Transition</em>'.
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToTransition()
	 * @see #getTTrackToPetrinet()
	 * @generated
	 */
	EReference getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition();

	/**
	 * Returns the meta object for class '{@link beispiel.model.ctools2pnet.TPortToPlace <em>TPort To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TPort To Place</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToPlace
	 * @generated
	 */
	EClass getTPortToPlace();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPort <em>TPort To Place To Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TPort To Place To Port</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPort()
	 * @see #getTPortToPlace()
	 * @generated
	 */
	EReference getTPortToPlace_TPortToPlaceToPort();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPlace <em>TPort To Place To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TPort To Place To Place</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPlace()
	 * @see #getTPortToPlace()
	 * @generated
	 */
	EReference getTPortToPlace_TPortToPlaceToPlace();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet <em>TPort To Place To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>TPort To Place To TTrack To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet()
	 * @see #getTPortToPlace()
	 * @generated
	 */
	EReference getTPortToPlace_TPortToPlaceToTTrackToPetrinet();

	/**
	 * Returns the meta object for class '{@link beispiel.model.ctools2pnet.TPortToTransition <em>TPort To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TPort To Transition</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToTransition
	 * @generated
	 */
	EClass getTPortToTransition();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTransition <em>TPort To Transition To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TPort To Transition To Transition</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTransition()
	 * @see #getTPortToTransition()
	 * @generated
	 */
	EReference getTPortToTransition_TPortToTransitionToTransition();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToPort <em>TPort To Transition To Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TPort To Transition To Port</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToPort()
	 * @see #getTPortToTransition()
	 * @generated
	 */
	EReference getTPortToTransition_TPortToTransitionToPort();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet <em>TPort To Transition To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>TPort To Transition To TTrack To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet()
	 * @see #getTPortToTransition()
	 * @generated
	 */
	EReference getTPortToTransition_TPortToTransitionToTTrackToPetrinet();

	/**
	 * Returns the meta object for class '{@link beispiel.model.ctools2pnet.TConnectionToArc <em>TConnection To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TConnection To Arc</em>'.
	 * @see beispiel.model.ctools2pnet.TConnectionToArc
	 * @generated
	 */
	EClass getTConnectionToArc();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToConnection <em>Connection To Arc To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection To Arc To Connection</em>'.
	 * @see beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToConnection()
	 * @see #getTConnectionToArc()
	 * @generated
	 */
	EReference getTConnectionToArc_ConnectionToArcToConnection();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToArc <em>Connection To Arc To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection To Arc To Arc</em>'.
	 * @see beispiel.model.ctools2pnet.TConnectionToArc#getConnectionToArcToArc()
	 * @see #getTConnectionToArc()
	 * @generated
	 */
	EReference getTConnectionToArc_ConnectionToArcToArc();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet <em>TConnection To Arc To TProject To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>TConnection To Arc To TProject To Petrinet</em>'.
	 * @see beispiel.model.ctools2pnet.TConnectionToArc#getTConnectionToArcToTProjectToPetrinet()
	 * @see #getTConnectionToArc()
	 * @generated
	 */
	EReference getTConnectionToArc_TConnectionToArcToTProjectToPetrinet();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Ctools2pnetFactory getCtools2pnetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals  {
		/**
		 * The meta object literal for the '{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl <em>TProject To Petrinet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl
		 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTProjectToPetrinet()
		 * @generated
		 */
		EClass TPROJECT_TO_PETRINET = eINSTANCE.getTProjectToPetrinet();

		/**
		 * The meta object literal for the '<em><b>TProject To Petrinet To Project</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT = eINSTANCE.getTProjectToPetrinet_TProjectToPetrinetToProject();

		/**
		 * The meta object literal for the '<em><b>TProject To Petrinet To Petrinet</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET = eINSTANCE.getTProjectToPetrinet_TProjectToPetrinetToPetrinet();

		/**
		 * The meta object literal for the '<em><b>TProject To Petrinet To TTrack To Petrinet</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET = eINSTANCE.getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet();

		/**
		 * The meta object literal for the '<em><b>TProject To Petrinet To TConnection To Arc</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC = eINSTANCE.getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc();

		/**
		 * The meta object literal for the '{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl <em>TTrack To Petrinet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl
		 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTTrackToPetrinet()
		 * @generated
		 */
		EClass TTRACK_TO_PETRINET = eINSTANCE.getTTrackToPetrinet();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Track</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToTrack();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Port Out</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToPortOut();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Port In</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToPortIn();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Arc</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToArc();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Place</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToPlace();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToTransition();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To TProject To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To TPort To Place</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace();

		/**
		 * The meta object literal for the '<em><b>TTrack To Petrinet To TPort To Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION = eINSTANCE.getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition();

		/**
		 * The meta object literal for the '{@link beispiel.model.ctools2pnet.impl.TPortToPlaceImpl <em>TPort To Place</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.ctools2pnet.impl.TPortToPlaceImpl
		 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTPortToPlace()
		 * @generated
		 */
		EClass TPORT_TO_PLACE = eINSTANCE.getTPortToPlace();

		/**
		 * The meta object literal for the '<em><b>TPort To Place To Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT = eINSTANCE.getTPortToPlace_TPortToPlaceToPort();

		/**
		 * The meta object literal for the '<em><b>TPort To Place To Place</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE = eINSTANCE.getTPortToPlace_TPortToPlaceToPlace();

		/**
		 * The meta object literal for the '<em><b>TPort To Place To TTrack To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET = eINSTANCE.getTPortToPlace_TPortToPlaceToTTrackToPetrinet();

		/**
		 * The meta object literal for the '{@link beispiel.model.ctools2pnet.impl.TPortToTransitionImpl <em>TPort To Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.ctools2pnet.impl.TPortToTransitionImpl
		 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTPortToTransition()
		 * @generated
		 */
		EClass TPORT_TO_TRANSITION = eINSTANCE.getTPortToTransition();

		/**
		 * The meta object literal for the '<em><b>TPort To Transition To Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION = eINSTANCE.getTPortToTransition_TPortToTransitionToTransition();

		/**
		 * The meta object literal for the '<em><b>TPort To Transition To Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT = eINSTANCE.getTPortToTransition_TPortToTransitionToPort();

		/**
		 * The meta object literal for the '<em><b>TPort To Transition To TTrack To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET = eINSTANCE.getTPortToTransition_TPortToTransitionToTTrackToPetrinet();

		/**
		 * The meta object literal for the '{@link beispiel.model.ctools2pnet.impl.TConnectionToArcImpl <em>TConnection To Arc</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.ctools2pnet.impl.TConnectionToArcImpl
		 * @see beispiel.model.ctools2pnet.impl.Ctools2pnetPackageImpl#getTConnectionToArc()
		 * @generated
		 */
		EClass TCONNECTION_TO_ARC = eINSTANCE.getTConnectionToArc();

		/**
		 * The meta object literal for the '<em><b>Connection To Arc To Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION = eINSTANCE.getTConnectionToArc_ConnectionToArcToConnection();

		/**
		 * The meta object literal for the '<em><b>Connection To Arc To Arc</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC = eINSTANCE.getTConnectionToArc_ConnectionToArcToArc();

			/**
		 * The meta object literal for the '<em><b>TConnection To Arc To TProject To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET = eINSTANCE.getTConnectionToArc_TConnectionToArcToTProjectToPetrinet();

	}

} //Ctools2pnetPackage
