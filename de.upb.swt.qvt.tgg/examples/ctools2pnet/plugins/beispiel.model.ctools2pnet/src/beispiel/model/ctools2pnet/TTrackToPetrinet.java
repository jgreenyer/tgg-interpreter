/**
 * <copyright>
 * </copyright>
 *
 * $Id: TTrackToPetrinet.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import beispiel.model.ctools.Port;
import beispiel.model.ctools.Track;

import beispiel.model.pnet.Arc;
import beispiel.model.pnet.Place;
import beispiel.model.pnet.Transition;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TTrack To Petrinet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTrack <em>TTrack To Petrinet To Track</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortOut <em>TTrack To Petrinet To Port Out</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortIn <em>TTrack To Petrinet To Port In</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToArc <em>TTrack To Petrinet To Arc</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPlace <em>TTrack To Petrinet To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTransition <em>TTrack To Petrinet To Transition</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet <em>TTrack To Petrinet To TProject To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToPlace <em>TTrack To Petrinet To TPort To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToTransition <em>TTrack To Petrinet To TPort To Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet()
 * @model
 * @generated
 */
public interface TTrackToPetrinet extends EObject {
	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Track</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Track</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Track</em>' reference.
	 * @see #setTTrackToPetrinetToTrack(Track)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToTrack()
	 * @model
	 * @generated
	 */
	Track getTTrackToPetrinetToTrack();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTrack <em>TTrack To Petrinet To Track</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Track</em>' reference.
	 * @see #getTTrackToPetrinetToTrack()
	 * @generated
	 */
	void setTTrackToPetrinetToTrack(Track value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Port Out</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Port Out</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Port Out</em>' reference.
	 * @see #setTTrackToPetrinetToPortOut(Port)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToPortOut()
	 * @model
	 * @generated
	 */
	Port getTTrackToPetrinetToPortOut();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortOut <em>TTrack To Petrinet To Port Out</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Port Out</em>' reference.
	 * @see #getTTrackToPetrinetToPortOut()
	 * @generated
	 */
	void setTTrackToPetrinetToPortOut(Port value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Port In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Port In</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Port In</em>' reference.
	 * @see #setTTrackToPetrinetToPortIn(Port)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToPortIn()
	 * @model
	 * @generated
	 */
	Port getTTrackToPetrinetToPortIn();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPortIn <em>TTrack To Petrinet To Port In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Port In</em>' reference.
	 * @see #getTTrackToPetrinetToPortIn()
	 * @generated
	 */
	void setTTrackToPetrinetToPortIn(Port value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Arc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Arc</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Arc</em>' reference.
	 * @see #setTTrackToPetrinetToArc(Arc)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToArc()
	 * @model
	 * @generated
	 */
	Arc getTTrackToPetrinetToArc();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToArc <em>TTrack To Petrinet To Arc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Arc</em>' reference.
	 * @see #getTTrackToPetrinetToArc()
	 * @generated
	 */
	void setTTrackToPetrinetToArc(Arc value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Place</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Place</em>' reference.
	 * @see #setTTrackToPetrinetToPlace(Place)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToPlace()
	 * @model
	 * @generated
	 */
	Place getTTrackToPetrinetToPlace();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToPlace <em>TTrack To Petrinet To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Place</em>' reference.
	 * @see #getTTrackToPetrinetToPlace()
	 * @generated
	 */
	void setTTrackToPetrinetToPlace(Place value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To Transition</em>' reference.
	 * @see #setTTrackToPetrinetToTransition(Transition)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToTransition()
	 * @model
	 * @generated
	 */
	Transition getTTrackToPetrinetToTransition();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTransition <em>TTrack To Petrinet To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To Transition</em>' reference.
	 * @see #getTTrackToPetrinetToTransition()
	 * @generated
	 */
	void setTTrackToPetrinetToTransition(Transition value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To TProject To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTTrackToPetrinet <em>TProject To Petrinet To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To TProject To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To TProject To Petrinet</em>' container reference.
	 * @see #setTTrackToPetrinetToTProjectToPetrinet(TProjectToPetrinet)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet()
	 * @see beispiel.model.ctools2pnet.TProjectToPetrinet#getTProjectToPetrinetToTTrackToPetrinet
	 * @model opposite="tProjectToPetrinetToTTrackToPetrinet"
	 * @generated
	 */
	TProjectToPetrinet getTTrackToPetrinetToTProjectToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTProjectToPetrinet <em>TTrack To Petrinet To TProject To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TTrack To Petrinet To TProject To Petrinet</em>' container reference.
	 * @see #getTTrackToPetrinetToTProjectToPetrinet()
	 * @generated
	 */
	void setTTrackToPetrinetToTProjectToPetrinet(TProjectToPetrinet value);

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To TPort To Place</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools2pnet.TPortToPlace}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet <em>TPort To Place To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To TPort To Place</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To TPort To Place</em>' containment reference list.
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace()
	 * @see beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet
	 * @model type="beispiel.model.ctools2pnet.TPortToPlace" opposite="tPortToPlaceToTTrackToPetrinet" containment="true"
	 * @generated
	 */
	EList getTTrackToPetrinetToTPortToPlace();

	/**
	 * Returns the value of the '<em><b>TTrack To Petrinet To TPort To Transition</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools2pnet.TPortToTransition}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet <em>TPort To Transition To TTrack To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TTrack To Petrinet To TPort To Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TTrack To Petrinet To TPort To Transition</em>' containment reference list.
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition()
	 * @see beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet
	 * @model type="beispiel.model.ctools2pnet.TPortToTransition" opposite="tPortToTransitionToTTrackToPetrinet" containment="true"
	 * @generated
	 */
	EList getTTrackToPetrinetToTPortToTransition();

} // TTrackToPetrinet