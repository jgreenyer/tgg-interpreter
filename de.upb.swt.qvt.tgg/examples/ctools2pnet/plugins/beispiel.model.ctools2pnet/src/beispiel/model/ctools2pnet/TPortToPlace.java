/**
 * <copyright>
 * </copyright>
 *
 * $Id: TPortToPlace.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import beispiel.model.ctools.Port;

import beispiel.model.pnet.Place;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPort To Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPort <em>TPort To Place To Port</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPlace <em>TPort To Place To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet <em>TPort To Place To TTrack To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToPlace()
 * @model
 * @generated
 */
public interface TPortToPlace extends EObject {
	/**
	 * Returns the value of the '<em><b>TPort To Place To Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Place To Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Place To Port</em>' reference.
	 * @see #setTPortToPlaceToPort(Port)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToPlace_TPortToPlaceToPort()
	 * @model
	 * @generated
	 */
	Port getTPortToPlaceToPort();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPort <em>TPort To Place To Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Place To Port</em>' reference.
	 * @see #getTPortToPlaceToPort()
	 * @generated
	 */
	void setTPortToPlaceToPort(Port value);

	/**
	 * Returns the value of the '<em><b>TPort To Place To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Place To Place</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Place To Place</em>' reference.
	 * @see #setTPortToPlaceToPlace(Place)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToPlace_TPortToPlaceToPlace()
	 * @model
	 * @generated
	 */
	Place getTPortToPlaceToPlace();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToPlace <em>TPort To Place To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Place To Place</em>' reference.
	 * @see #getTPortToPlaceToPlace()
	 * @generated
	 */
	void setTPortToPlaceToPlace(Place value);

	/**
	 * Returns the value of the '<em><b>TPort To Place To TTrack To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToPlace <em>TTrack To Petrinet To TPort To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Place To TTrack To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Place To TTrack To Petrinet</em>' container reference.
	 * @see #setTPortToPlaceToTTrackToPetrinet(TTrackToPetrinet)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToPlace_TPortToPlaceToTTrackToPetrinet()
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToPlace
	 * @model opposite="tTrackToPetrinetToTPortToPlace"
	 * @generated
	 */
	TTrackToPetrinet getTPortToPlaceToTTrackToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToPlace#getTPortToPlaceToTTrackToPetrinet <em>TPort To Place To TTrack To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Place To TTrack To Petrinet</em>' container reference.
	 * @see #getTPortToPlaceToTTrackToPetrinet()
	 * @generated
	 */
	void setTPortToPlaceToTTrackToPetrinet(TTrackToPetrinet value);

} // TPortToPlace