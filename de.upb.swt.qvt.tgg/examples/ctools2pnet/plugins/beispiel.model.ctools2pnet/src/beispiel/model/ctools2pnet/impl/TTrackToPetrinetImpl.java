/**
 * <copyright>
 * </copyright>
 *
 * $Id: TTrackToPetrinetImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.Port;
import beispiel.model.ctools.Track;

import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TPortToPlace;
import beispiel.model.ctools2pnet.TPortToTransition;
import beispiel.model.ctools2pnet.TProjectToPetrinet;
import beispiel.model.ctools2pnet.TTrackToPetrinet;

import beispiel.model.pnet.Arc;
import beispiel.model.pnet.Place;
import beispiel.model.pnet.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TTrack To Petrinet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToTrack <em>TTrack To Petrinet To Track</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToPortOut <em>TTrack To Petrinet To Port Out</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToPortIn <em>TTrack To Petrinet To Port In</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToArc <em>TTrack To Petrinet To Arc</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToPlace <em>TTrack To Petrinet To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToTransition <em>TTrack To Petrinet To Transition</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToTProjectToPetrinet <em>TTrack To Petrinet To TProject To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToTPortToPlace <em>TTrack To Petrinet To TPort To Place</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TTrackToPetrinetImpl#getTTrackToPetrinetToTPortToTransition <em>TTrack To Petrinet To TPort To Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TTrackToPetrinetImpl extends EObjectImpl implements TTrackToPetrinet {
	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToTrack() <em>TTrack To Petrinet To Track</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToTrack()
	 * @generated
	 * @ordered
	 */
	protected Track tTrackToPetrinetToTrack = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToPortOut() <em>TTrack To Petrinet To Port Out</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToPortOut()
	 * @generated
	 * @ordered
	 */
	protected Port tTrackToPetrinetToPortOut = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToPortIn() <em>TTrack To Petrinet To Port In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToPortIn()
	 * @generated
	 * @ordered
	 */
	protected Port tTrackToPetrinetToPortIn = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToArc() <em>TTrack To Petrinet To Arc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToArc()
	 * @generated
	 * @ordered
	 */
	protected Arc tTrackToPetrinetToArc = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToPlace() <em>TTrack To Petrinet To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToPlace()
	 * @generated
	 * @ordered
	 */
	protected Place tTrackToPetrinetToPlace = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToTransition() <em>TTrack To Petrinet To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToTransition()
	 * @generated
	 * @ordered
	 */
	protected Transition tTrackToPetrinetToTransition = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToTPortToPlace() <em>TTrack To Petrinet To TPort To Place</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToTPortToPlace()
	 * @generated
	 * @ordered
	 */
	protected EList tTrackToPetrinetToTPortToPlace = null;

	/**
	 * The cached value of the '{@link #getTTrackToPetrinetToTPortToTransition() <em>TTrack To Petrinet To TPort To Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTTrackToPetrinetToTPortToTransition()
	 * @generated
	 * @ordered
	 */
	protected EList tTrackToPetrinetToTPortToTransition = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TTrackToPetrinetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Ctools2pnetPackage.Literals.TTRACK_TO_PETRINET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Track getTTrackToPetrinetToTrack() {
		if (tTrackToPetrinetToTrack != null && tTrackToPetrinetToTrack.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToTrack = (InternalEObject)tTrackToPetrinetToTrack;
			tTrackToPetrinetToTrack = (Track)eResolveProxy(oldTTrackToPetrinetToTrack);
			if (tTrackToPetrinetToTrack != oldTTrackToPetrinetToTrack) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK, oldTTrackToPetrinetToTrack, tTrackToPetrinetToTrack));
			}
		}
		return tTrackToPetrinetToTrack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Track basicGetTTrackToPetrinetToTrack() {
		return tTrackToPetrinetToTrack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToTrack(Track newTTrackToPetrinetToTrack) {
		Track oldTTrackToPetrinetToTrack = tTrackToPetrinetToTrack;
		tTrackToPetrinetToTrack = newTTrackToPetrinetToTrack;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK, oldTTrackToPetrinetToTrack, tTrackToPetrinetToTrack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getTTrackToPetrinetToPortOut() {
		if (tTrackToPetrinetToPortOut != null && tTrackToPetrinetToPortOut.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToPortOut = (InternalEObject)tTrackToPetrinetToPortOut;
			tTrackToPetrinetToPortOut = (Port)eResolveProxy(oldTTrackToPetrinetToPortOut);
			if (tTrackToPetrinetToPortOut != oldTTrackToPetrinetToPortOut) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT, oldTTrackToPetrinetToPortOut, tTrackToPetrinetToPortOut));
			}
		}
		return tTrackToPetrinetToPortOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetTTrackToPetrinetToPortOut() {
		return tTrackToPetrinetToPortOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToPortOut(Port newTTrackToPetrinetToPortOut) {
		Port oldTTrackToPetrinetToPortOut = tTrackToPetrinetToPortOut;
		tTrackToPetrinetToPortOut = newTTrackToPetrinetToPortOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT, oldTTrackToPetrinetToPortOut, tTrackToPetrinetToPortOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getTTrackToPetrinetToPortIn() {
		if (tTrackToPetrinetToPortIn != null && tTrackToPetrinetToPortIn.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToPortIn = (InternalEObject)tTrackToPetrinetToPortIn;
			tTrackToPetrinetToPortIn = (Port)eResolveProxy(oldTTrackToPetrinetToPortIn);
			if (tTrackToPetrinetToPortIn != oldTTrackToPetrinetToPortIn) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN, oldTTrackToPetrinetToPortIn, tTrackToPetrinetToPortIn));
			}
		}
		return tTrackToPetrinetToPortIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetTTrackToPetrinetToPortIn() {
		return tTrackToPetrinetToPortIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToPortIn(Port newTTrackToPetrinetToPortIn) {
		Port oldTTrackToPetrinetToPortIn = tTrackToPetrinetToPortIn;
		tTrackToPetrinetToPortIn = newTTrackToPetrinetToPortIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN, oldTTrackToPetrinetToPortIn, tTrackToPetrinetToPortIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Arc getTTrackToPetrinetToArc() {
		if (tTrackToPetrinetToArc != null && tTrackToPetrinetToArc.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToArc = (InternalEObject)tTrackToPetrinetToArc;
			tTrackToPetrinetToArc = (Arc)eResolveProxy(oldTTrackToPetrinetToArc);
			if (tTrackToPetrinetToArc != oldTTrackToPetrinetToArc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC, oldTTrackToPetrinetToArc, tTrackToPetrinetToArc));
			}
		}
		return tTrackToPetrinetToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Arc basicGetTTrackToPetrinetToArc() {
		return tTrackToPetrinetToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToArc(Arc newTTrackToPetrinetToArc) {
		Arc oldTTrackToPetrinetToArc = tTrackToPetrinetToArc;
		tTrackToPetrinetToArc = newTTrackToPetrinetToArc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC, oldTTrackToPetrinetToArc, tTrackToPetrinetToArc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place getTTrackToPetrinetToPlace() {
		if (tTrackToPetrinetToPlace != null && tTrackToPetrinetToPlace.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToPlace = (InternalEObject)tTrackToPetrinetToPlace;
			tTrackToPetrinetToPlace = (Place)eResolveProxy(oldTTrackToPetrinetToPlace);
			if (tTrackToPetrinetToPlace != oldTTrackToPetrinetToPlace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE, oldTTrackToPetrinetToPlace, tTrackToPetrinetToPlace));
			}
		}
		return tTrackToPetrinetToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place basicGetTTrackToPetrinetToPlace() {
		return tTrackToPetrinetToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToPlace(Place newTTrackToPetrinetToPlace) {
		Place oldTTrackToPetrinetToPlace = tTrackToPetrinetToPlace;
		tTrackToPetrinetToPlace = newTTrackToPetrinetToPlace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE, oldTTrackToPetrinetToPlace, tTrackToPetrinetToPlace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getTTrackToPetrinetToTransition() {
		if (tTrackToPetrinetToTransition != null && tTrackToPetrinetToTransition.eIsProxy()) {
			InternalEObject oldTTrackToPetrinetToTransition = (InternalEObject)tTrackToPetrinetToTransition;
			tTrackToPetrinetToTransition = (Transition)eResolveProxy(oldTTrackToPetrinetToTransition);
			if (tTrackToPetrinetToTransition != oldTTrackToPetrinetToTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION, oldTTrackToPetrinetToTransition, tTrackToPetrinetToTransition));
			}
		}
		return tTrackToPetrinetToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetTTrackToPetrinetToTransition() {
		return tTrackToPetrinetToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToTransition(Transition newTTrackToPetrinetToTransition) {
		Transition oldTTrackToPetrinetToTransition = tTrackToPetrinetToTransition;
		tTrackToPetrinetToTransition = newTTrackToPetrinetToTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION, oldTTrackToPetrinetToTransition, tTrackToPetrinetToTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TProjectToPetrinet getTTrackToPetrinetToTProjectToPetrinet() {
		if (eContainerFeatureID != Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET) return null;
		return (TProjectToPetrinet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTTrackToPetrinetToTProjectToPetrinet(TProjectToPetrinet newTTrackToPetrinetToTProjectToPetrinet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTTrackToPetrinetToTProjectToPetrinet, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTTrackToPetrinetToTProjectToPetrinet(TProjectToPetrinet newTTrackToPetrinetToTProjectToPetrinet) {
		if (newTTrackToPetrinetToTProjectToPetrinet != eInternalContainer() || (eContainerFeatureID != Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET && newTTrackToPetrinetToTProjectToPetrinet != null)) {
			if (EcoreUtil.isAncestor(this, newTTrackToPetrinetToTProjectToPetrinet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTTrackToPetrinetToTProjectToPetrinet != null)
				msgs = ((InternalEObject)newTTrackToPetrinetToTProjectToPetrinet).eInverseAdd(this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET, TProjectToPetrinet.class, msgs);
			msgs = basicSetTTrackToPetrinetToTProjectToPetrinet(newTTrackToPetrinetToTProjectToPetrinet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET, newTTrackToPetrinetToTProjectToPetrinet, newTTrackToPetrinetToTProjectToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTTrackToPetrinetToTPortToPlace() {
		if (tTrackToPetrinetToTPortToPlace == null) {
			tTrackToPetrinetToTPortToPlace = new EObjectContainmentWithInverseEList(TPortToPlace.class, this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE, Ctools2pnetPackage.TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET);
		}
		return tTrackToPetrinetToTPortToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTTrackToPetrinetToTPortToTransition() {
		if (tTrackToPetrinetToTPortToTransition == null) {
			tTrackToPetrinetToTPortToTransition = new EObjectContainmentWithInverseEList(TPortToTransition.class, this, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION, Ctools2pnetPackage.TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET);
		}
		return tTrackToPetrinetToTPortToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTTrackToPetrinetToTProjectToPetrinet((TProjectToPetrinet)otherEnd, msgs);
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				return ((InternalEList)getTTrackToPetrinetToTPortToPlace()).basicAdd(otherEnd, msgs);
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				return ((InternalEList)getTTrackToPetrinetToTPortToTransition()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				return basicSetTTrackToPetrinetToTProjectToPetrinet(null, msgs);
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				return ((InternalEList)getTTrackToPetrinetToTPortToPlace()).basicRemove(otherEnd, msgs);
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				return ((InternalEList)getTTrackToPetrinetToTPortToTransition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				return eInternalContainer().eInverseRemove(this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET, TProjectToPetrinet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK:
				if (resolve) return getTTrackToPetrinetToTrack();
				return basicGetTTrackToPetrinetToTrack();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT:
				if (resolve) return getTTrackToPetrinetToPortOut();
				return basicGetTTrackToPetrinetToPortOut();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN:
				if (resolve) return getTTrackToPetrinetToPortIn();
				return basicGetTTrackToPetrinetToPortIn();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC:
				if (resolve) return getTTrackToPetrinetToArc();
				return basicGetTTrackToPetrinetToArc();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE:
				if (resolve) return getTTrackToPetrinetToPlace();
				return basicGetTTrackToPetrinetToPlace();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION:
				if (resolve) return getTTrackToPetrinetToTransition();
				return basicGetTTrackToPetrinetToTransition();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				return getTTrackToPetrinetToTProjectToPetrinet();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				return getTTrackToPetrinetToTPortToPlace();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				return getTTrackToPetrinetToTPortToTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK:
				setTTrackToPetrinetToTrack((Track)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT:
				setTTrackToPetrinetToPortOut((Port)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN:
				setTTrackToPetrinetToPortIn((Port)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC:
				setTTrackToPetrinetToArc((Arc)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE:
				setTTrackToPetrinetToPlace((Place)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION:
				setTTrackToPetrinetToTransition((Transition)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				setTTrackToPetrinetToTProjectToPetrinet((TProjectToPetrinet)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				getTTrackToPetrinetToTPortToPlace().clear();
				getTTrackToPetrinetToTPortToPlace().addAll((Collection)newValue);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				getTTrackToPetrinetToTPortToTransition().clear();
				getTTrackToPetrinetToTPortToTransition().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK:
				setTTrackToPetrinetToTrack((Track)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT:
				setTTrackToPetrinetToPortOut((Port)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN:
				setTTrackToPetrinetToPortIn((Port)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC:
				setTTrackToPetrinetToArc((Arc)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE:
				setTTrackToPetrinetToPlace((Place)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION:
				setTTrackToPetrinetToTransition((Transition)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				setTTrackToPetrinetToTProjectToPetrinet((TProjectToPetrinet)null);
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				getTTrackToPetrinetToTPortToPlace().clear();
				return;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				getTTrackToPetrinetToTPortToTransition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK:
				return tTrackToPetrinetToTrack != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT:
				return tTrackToPetrinetToPortOut != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN:
				return tTrackToPetrinetToPortIn != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC:
				return tTrackToPetrinetToArc != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE:
				return tTrackToPetrinetToPlace != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION:
				return tTrackToPetrinetToTransition != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET:
				return getTTrackToPetrinetToTProjectToPetrinet() != null;
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE:
				return tTrackToPetrinetToTPortToPlace != null && !tTrackToPetrinetToTPortToPlace.isEmpty();
			case Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION:
				return tTrackToPetrinetToTPortToTransition != null && !tTrackToPetrinetToTPortToTransition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TTrackToPetrinetImpl