/**
 * <copyright>
 * </copyright>
 *
 * $Id: TProjectToPetrinetImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.Project;

import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TConnectionToArc;
import beispiel.model.ctools2pnet.TProjectToPetrinet;

import beispiel.model.ctools2pnet.TTrackToPetrinet;

import beispiel.model.pnet.Petrinet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TProject To Petrinet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl#getTProjectToPetrinetToProject <em>TProject To Petrinet To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl#getTProjectToPetrinetToPetrinet <em>TProject To Petrinet To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl#getTProjectToPetrinetToTTrackToPetrinet <em>TProject To Petrinet To TTrack To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.impl.TProjectToPetrinetImpl#getTProjectToPetrinetToTConnectionToArc <em>TProject To Petrinet To TConnection To Arc</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TProjectToPetrinetImpl extends EObjectImpl implements TProjectToPetrinet {
	/**
	 * The cached value of the '{@link #getTProjectToPetrinetToProject() <em>TProject To Petrinet To Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTProjectToPetrinetToProject()
	 * @generated
	 * @ordered
	 */
	protected Project tProjectToPetrinetToProject = null;

	/**
	 * The cached value of the '{@link #getTProjectToPetrinetToPetrinet() <em>TProject To Petrinet To Petrinet</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTProjectToPetrinetToPetrinet()
	 * @generated
	 * @ordered
	 */
	protected Petrinet tProjectToPetrinetToPetrinet = null;

	/**
	 * The cached value of the '{@link #getTProjectToPetrinetToTTrackToPetrinet() <em>TProject To Petrinet To TTrack To Petrinet</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTProjectToPetrinetToTTrackToPetrinet()
	 * @generated
	 * @ordered
	 */
	protected EList tProjectToPetrinetToTTrackToPetrinet = null;

	/**
	 * The cached value of the '{@link #getTProjectToPetrinetToTConnectionToArc() <em>TProject To Petrinet To TConnection To Arc</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTProjectToPetrinetToTConnectionToArc()
	 * @generated
	 * @ordered
	 */
	protected EList tProjectToPetrinetToTConnectionToArc = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TProjectToPetrinetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Ctools2pnetPackage.Literals.TPROJECT_TO_PETRINET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project getTProjectToPetrinetToProject() {
		if (tProjectToPetrinetToProject != null && tProjectToPetrinetToProject.eIsProxy()) {
			InternalEObject oldTProjectToPetrinetToProject = (InternalEObject)tProjectToPetrinetToProject;
			tProjectToPetrinetToProject = (Project)eResolveProxy(oldTProjectToPetrinetToProject);
			if (tProjectToPetrinetToProject != oldTProjectToPetrinetToProject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT, oldTProjectToPetrinetToProject, tProjectToPetrinetToProject));
			}
		}
		return tProjectToPetrinetToProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project basicGetTProjectToPetrinetToProject() {
		return tProjectToPetrinetToProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTProjectToPetrinetToProject(Project newTProjectToPetrinetToProject) {
		Project oldTProjectToPetrinetToProject = tProjectToPetrinetToProject;
		tProjectToPetrinetToProject = newTProjectToPetrinetToProject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT, oldTProjectToPetrinetToProject, tProjectToPetrinetToProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Petrinet getTProjectToPetrinetToPetrinet() {
		if (tProjectToPetrinetToPetrinet != null && tProjectToPetrinetToPetrinet.eIsProxy()) {
			InternalEObject oldTProjectToPetrinetToPetrinet = (InternalEObject)tProjectToPetrinetToPetrinet;
			tProjectToPetrinetToPetrinet = (Petrinet)eResolveProxy(oldTProjectToPetrinetToPetrinet);
			if (tProjectToPetrinetToPetrinet != oldTProjectToPetrinetToPetrinet) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET, oldTProjectToPetrinetToPetrinet, tProjectToPetrinetToPetrinet));
			}
		}
		return tProjectToPetrinetToPetrinet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Petrinet basicGetTProjectToPetrinetToPetrinet() {
		return tProjectToPetrinetToPetrinet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTProjectToPetrinetToPetrinet(Petrinet newTProjectToPetrinetToPetrinet) {
		Petrinet oldTProjectToPetrinetToPetrinet = tProjectToPetrinetToPetrinet;
		tProjectToPetrinetToPetrinet = newTProjectToPetrinetToPetrinet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET, oldTProjectToPetrinetToPetrinet, tProjectToPetrinetToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTProjectToPetrinetToTTrackToPetrinet() {
		if (tProjectToPetrinetToTTrackToPetrinet == null) {
			tProjectToPetrinetToTTrackToPetrinet = new EObjectContainmentWithInverseEList(TTrackToPetrinet.class, this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET, Ctools2pnetPackage.TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET);
		}
		return tProjectToPetrinetToTTrackToPetrinet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTProjectToPetrinetToTConnectionToArc() {
		if (tProjectToPetrinetToTConnectionToArc == null) {
			tProjectToPetrinetToTConnectionToArc = new EObjectContainmentWithInverseEList(TConnectionToArc.class, this, Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC, Ctools2pnetPackage.TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET);
		}
		return tProjectToPetrinetToTConnectionToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				return ((InternalEList)getTProjectToPetrinetToTTrackToPetrinet()).basicAdd(otherEnd, msgs);
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				return ((InternalEList)getTProjectToPetrinetToTConnectionToArc()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				return ((InternalEList)getTProjectToPetrinetToTTrackToPetrinet()).basicRemove(otherEnd, msgs);
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				return ((InternalEList)getTProjectToPetrinetToTConnectionToArc()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT:
				if (resolve) return getTProjectToPetrinetToProject();
				return basicGetTProjectToPetrinetToProject();
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET:
				if (resolve) return getTProjectToPetrinetToPetrinet();
				return basicGetTProjectToPetrinetToPetrinet();
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				return getTProjectToPetrinetToTTrackToPetrinet();
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				return getTProjectToPetrinetToTConnectionToArc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT:
				setTProjectToPetrinetToProject((Project)newValue);
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET:
				setTProjectToPetrinetToPetrinet((Petrinet)newValue);
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				getTProjectToPetrinetToTTrackToPetrinet().clear();
				getTProjectToPetrinetToTTrackToPetrinet().addAll((Collection)newValue);
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				getTProjectToPetrinetToTConnectionToArc().clear();
				getTProjectToPetrinetToTConnectionToArc().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT:
				setTProjectToPetrinetToProject((Project)null);
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET:
				setTProjectToPetrinetToPetrinet((Petrinet)null);
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				getTProjectToPetrinetToTTrackToPetrinet().clear();
				return;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				getTProjectToPetrinetToTConnectionToArc().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT:
				return tProjectToPetrinetToProject != null;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET:
				return tProjectToPetrinetToPetrinet != null;
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET:
				return tProjectToPetrinetToTTrackToPetrinet != null && !tProjectToPetrinetToTTrackToPetrinet.isEmpty();
			case Ctools2pnetPackage.TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC:
				return tProjectToPetrinetToTConnectionToArc != null && !tProjectToPetrinetToTConnectionToArc.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TProjectToPetrinetImpl