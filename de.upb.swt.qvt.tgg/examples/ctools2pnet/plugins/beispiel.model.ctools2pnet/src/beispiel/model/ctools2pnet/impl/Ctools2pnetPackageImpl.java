/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ctools2pnetPackageImpl.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet.impl;

import beispiel.model.ctools.CtoolsPackage;

import beispiel.model.ctools2pnet.Ctools2pnetFactory;
import beispiel.model.ctools2pnet.Ctools2pnetPackage;
import beispiel.model.ctools2pnet.TConnectionToArc;
import beispiel.model.ctools2pnet.TPortToPlace;
import beispiel.model.ctools2pnet.TPortToTransition;
import beispiel.model.ctools2pnet.TProjectToPetrinet;
import beispiel.model.ctools2pnet.TTrackToPetrinet;

import beispiel.model.pnet.PnetPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Ctools2pnetPackageImpl extends EPackageImpl implements Ctools2pnetPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tProjectToPetrinetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tTrackToPetrinetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tPortToPlaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tPortToTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tConnectionToArcEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Ctools2pnetPackageImpl() {
		super(eNS_URI, Ctools2pnetFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Ctools2pnetPackage init() {
		if (isInited) return (Ctools2pnetPackage)EPackage.Registry.INSTANCE.getEPackage(Ctools2pnetPackage.eNS_URI);

		// Obtain or create and register package
		Ctools2pnetPackageImpl theCtools2pnetPackage = (Ctools2pnetPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof Ctools2pnetPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new Ctools2pnetPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CtoolsPackage.eINSTANCE.eClass();
		PnetPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theCtools2pnetPackage.createPackageContents();

		// Initialize created meta-data
		theCtools2pnetPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCtools2pnetPackage.freeze();

		return theCtools2pnetPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTProjectToPetrinet() {
		return tProjectToPetrinetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTProjectToPetrinet_TProjectToPetrinetToProject() {
		return (EReference)tProjectToPetrinetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTProjectToPetrinet_TProjectToPetrinetToPetrinet() {
		return (EReference)tProjectToPetrinetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet() {
		return (EReference)tProjectToPetrinetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc() {
		return (EReference)tProjectToPetrinetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTTrackToPetrinet() {
		return tTrackToPetrinetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToTrack() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToPortOut() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToPortIn() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToArc() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToPlace() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToTransition() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition() {
		return (EReference)tTrackToPetrinetEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTPortToPlace() {
		return tPortToPlaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToPlace_TPortToPlaceToPort() {
		return (EReference)tPortToPlaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToPlace_TPortToPlaceToPlace() {
		return (EReference)tPortToPlaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToPlace_TPortToPlaceToTTrackToPetrinet() {
		return (EReference)tPortToPlaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTPortToTransition() {
		return tPortToTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToTransition_TPortToTransitionToTransition() {
		return (EReference)tPortToTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToTransition_TPortToTransitionToPort() {
		return (EReference)tPortToTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTPortToTransition_TPortToTransitionToTTrackToPetrinet() {
		return (EReference)tPortToTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTConnectionToArc() {
		return tConnectionToArcEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTConnectionToArc_ConnectionToArcToConnection() {
		return (EReference)tConnectionToArcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTConnectionToArc_ConnectionToArcToArc() {
		return (EReference)tConnectionToArcEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTConnectionToArc_TConnectionToArcToTProjectToPetrinet() {
		return (EReference)tConnectionToArcEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctools2pnetFactory getCtools2pnetFactory() {
		return (Ctools2pnetFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tProjectToPetrinetEClass = createEClass(TPROJECT_TO_PETRINET);
		createEReference(tProjectToPetrinetEClass, TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PROJECT);
		createEReference(tProjectToPetrinetEClass, TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_PETRINET);
		createEReference(tProjectToPetrinetEClass, TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TTRACK_TO_PETRINET);
		createEReference(tProjectToPetrinetEClass, TPROJECT_TO_PETRINET__TPROJECT_TO_PETRINET_TO_TCONNECTION_TO_ARC);

		tTrackToPetrinetEClass = createEClass(TTRACK_TO_PETRINET);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRACK);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_OUT);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PORT_IN);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_ARC);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_PLACE);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TRANSITION);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPROJECT_TO_PETRINET);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_PLACE);
		createEReference(tTrackToPetrinetEClass, TTRACK_TO_PETRINET__TTRACK_TO_PETRINET_TO_TPORT_TO_TRANSITION);

		tPortToPlaceEClass = createEClass(TPORT_TO_PLACE);
		createEReference(tPortToPlaceEClass, TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PORT);
		createEReference(tPortToPlaceEClass, TPORT_TO_PLACE__TPORT_TO_PLACE_TO_PLACE);
		createEReference(tPortToPlaceEClass, TPORT_TO_PLACE__TPORT_TO_PLACE_TO_TTRACK_TO_PETRINET);

		tPortToTransitionEClass = createEClass(TPORT_TO_TRANSITION);
		createEReference(tPortToTransitionEClass, TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TRANSITION);
		createEReference(tPortToTransitionEClass, TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_PORT);
		createEReference(tPortToTransitionEClass, TPORT_TO_TRANSITION__TPORT_TO_TRANSITION_TO_TTRACK_TO_PETRINET);

		tConnectionToArcEClass = createEClass(TCONNECTION_TO_ARC);
		createEReference(tConnectionToArcEClass, TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_CONNECTION);
		createEReference(tConnectionToArcEClass, TCONNECTION_TO_ARC__CONNECTION_TO_ARC_TO_ARC);
		createEReference(tConnectionToArcEClass, TCONNECTION_TO_ARC__TCONNECTION_TO_ARC_TO_TPROJECT_TO_PETRINET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CtoolsPackage theCtoolsPackage = (CtoolsPackage)EPackage.Registry.INSTANCE.getEPackage(CtoolsPackage.eNS_URI);
		PnetPackage thePnetPackage = (PnetPackage)EPackage.Registry.INSTANCE.getEPackage(PnetPackage.eNS_URI);

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(tProjectToPetrinetEClass, TProjectToPetrinet.class, "TProjectToPetrinet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTProjectToPetrinet_TProjectToPetrinetToProject(), theCtoolsPackage.getProject(), null, "tProjectToPetrinetToProject", null, 0, 1, TProjectToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTProjectToPetrinet_TProjectToPetrinetToPetrinet(), thePnetPackage.getPetrinet(), null, "tProjectToPetrinetToPetrinet", null, 0, 1, TProjectToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet(), this.getTTrackToPetrinet(), this.getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet(), "tProjectToPetrinetToTTrackToPetrinet", null, 0, -1, TProjectToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc(), this.getTConnectionToArc(), this.getTConnectionToArc_TConnectionToArcToTProjectToPetrinet(), "tProjectToPetrinetToTConnectionToArc", null, 0, -1, TProjectToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tTrackToPetrinetEClass, TTrackToPetrinet.class, "TTrackToPetrinet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToTrack(), theCtoolsPackage.getTrack(), null, "tTrackToPetrinetToTrack", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToPortOut(), theCtoolsPackage.getPort(), null, "tTrackToPetrinetToPortOut", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToPortIn(), theCtoolsPackage.getPort(), null, "tTrackToPetrinetToPortIn", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToArc(), thePnetPackage.getArc(), null, "tTrackToPetrinetToArc", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToPlace(), thePnetPackage.getPlace(), null, "tTrackToPetrinetToPlace", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToTransition(), thePnetPackage.getTransition(), null, "tTrackToPetrinetToTransition", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToTProjectToPetrinet(), this.getTProjectToPetrinet(), this.getTProjectToPetrinet_TProjectToPetrinetToTTrackToPetrinet(), "tTrackToPetrinetToTProjectToPetrinet", null, 0, 1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace(), this.getTPortToPlace(), this.getTPortToPlace_TPortToPlaceToTTrackToPetrinet(), "tTrackToPetrinetToTPortToPlace", null, 0, -1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition(), this.getTPortToTransition(), this.getTPortToTransition_TPortToTransitionToTTrackToPetrinet(), "tTrackToPetrinetToTPortToTransition", null, 0, -1, TTrackToPetrinet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tPortToPlaceEClass, TPortToPlace.class, "TPortToPlace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTPortToPlace_TPortToPlaceToPort(), theCtoolsPackage.getPort(), null, "tPortToPlaceToPort", null, 0, 1, TPortToPlace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTPortToPlace_TPortToPlaceToPlace(), thePnetPackage.getPlace(), null, "tPortToPlaceToPlace", null, 0, 1, TPortToPlace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTPortToPlace_TPortToPlaceToTTrackToPetrinet(), this.getTTrackToPetrinet(), this.getTTrackToPetrinet_TTrackToPetrinetToTPortToPlace(), "tPortToPlaceToTTrackToPetrinet", null, 0, 1, TPortToPlace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tPortToTransitionEClass, TPortToTransition.class, "TPortToTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTPortToTransition_TPortToTransitionToTransition(), thePnetPackage.getTransition(), null, "TPortToTransitionToTransition", null, 0, 1, TPortToTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTPortToTransition_TPortToTransitionToPort(), theCtoolsPackage.getPort(), null, "TPortToTransitionToPort", null, 0, 1, TPortToTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTPortToTransition_TPortToTransitionToTTrackToPetrinet(), this.getTTrackToPetrinet(), this.getTTrackToPetrinet_TTrackToPetrinetToTPortToTransition(), "tPortToTransitionToTTrackToPetrinet", null, 0, 1, TPortToTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tConnectionToArcEClass, TConnectionToArc.class, "TConnectionToArc", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTConnectionToArc_ConnectionToArcToConnection(), theCtoolsPackage.getConnection(), null, "ConnectionToArcToConnection", null, 0, 1, TConnectionToArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTConnectionToArc_ConnectionToArcToArc(), thePnetPackage.getArc(), null, "ConnectionToArcToArc", null, 0, 1, TConnectionToArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTConnectionToArc_TConnectionToArcToTProjectToPetrinet(), this.getTProjectToPetrinet(), this.getTProjectToPetrinet_TProjectToPetrinetToTConnectionToArc(), "tConnectionToArcToTProjectToPetrinet", null, 0, 1, TConnectionToArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Ctools2pnetPackageImpl
