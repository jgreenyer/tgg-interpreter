/**
 * <copyright>
 * </copyright>
 *
 * $Id: TPortToTransition.java 1652 2006-07-07 12:54:01 +0000 (Fr, 07 Jul 2006) jgreen $
 */
package beispiel.model.ctools2pnet;

import beispiel.model.ctools.Port;

import beispiel.model.pnet.Transition;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPort To Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTransition <em>TPort To Transition To Transition</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToPort <em>TPort To Transition To Port</em>}</li>
 *   <li>{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet <em>TPort To Transition To TTrack To Petrinet</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToTransition()
 * @model
 * @generated
 */
public interface TPortToTransition extends EObject {
	/**
	 * Returns the value of the '<em><b>TPort To Transition To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Transition To Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Transition To Transition</em>' reference.
	 * @see #setTPortToTransitionToTransition(Transition)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToTransition_TPortToTransitionToTransition()
	 * @model
	 * @generated
	 */
	Transition getTPortToTransitionToTransition();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTransition <em>TPort To Transition To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Transition To Transition</em>' reference.
	 * @see #getTPortToTransitionToTransition()
	 * @generated
	 */
	void setTPortToTransitionToTransition(Transition value);

	/**
	 * Returns the value of the '<em><b>TPort To Transition To Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Transition To Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Transition To Port</em>' reference.
	 * @see #setTPortToTransitionToPort(Port)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToTransition_TPortToTransitionToPort()
	 * @model
	 * @generated
	 */
	Port getTPortToTransitionToPort();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToPort <em>TPort To Transition To Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Transition To Port</em>' reference.
	 * @see #getTPortToTransitionToPort()
	 * @generated
	 */
	void setTPortToTransitionToPort(Port value);

	/**
	 * Returns the value of the '<em><b>TPort To Transition To TTrack To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToTransition <em>TTrack To Petrinet To TPort To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPort To Transition To TTrack To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPort To Transition To TTrack To Petrinet</em>' container reference.
	 * @see #setTPortToTransitionToTTrackToPetrinet(TTrackToPetrinet)
	 * @see beispiel.model.ctools2pnet.Ctools2pnetPackage#getTPortToTransition_TPortToTransitionToTTrackToPetrinet()
	 * @see beispiel.model.ctools2pnet.TTrackToPetrinet#getTTrackToPetrinetToTPortToTransition
	 * @model opposite="tTrackToPetrinetToTPortToTransition"
	 * @generated
	 */
	TTrackToPetrinet getTPortToTransitionToTTrackToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools2pnet.TPortToTransition#getTPortToTransitionToTTrackToPetrinet <em>TPort To Transition To TTrack To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPort To Transition To TTrack To Petrinet</em>' container reference.
	 * @see #getTPortToTransitionToTTrackToPetrinet()
	 * @generated
	 */
	void setTPortToTransitionToTTrackToPetrinet(TTrackToPetrinet value);

} // TPortToTransition