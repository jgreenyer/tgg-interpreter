/**
 * <copyright>
 * </copyright>
 *
 * $Id: Transition.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.pnet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.pnet.Transition#getTransitionToPetrinet <em>Transition To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.pnet.Transition#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.pnet.PnetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Transition To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.pnet.Petrinet#getPetrinetToTransition <em>Petrinet To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition To Petrinet</em>' container reference.
	 * @see #setTransitionToPetrinet(Petrinet)
	 * @see beispiel.model.pnet.PnetPackage#getTransition_TransitionToPetrinet()
	 * @see beispiel.model.pnet.Petrinet#getPetrinetToTransition
	 * @model opposite="petrinetToTransition" transient="false"
	 * @generated
	 */
	Petrinet getTransitionToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.pnet.Transition#getTransitionToPetrinet <em>Transition To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition To Petrinet</em>' container reference.
	 * @see #getTransitionToPetrinet()
	 * @generated
	 */
	void setTransitionToPetrinet(Petrinet value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see beispiel.model.pnet.PnetPackage#getTransition_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link beispiel.model.pnet.Transition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Transition