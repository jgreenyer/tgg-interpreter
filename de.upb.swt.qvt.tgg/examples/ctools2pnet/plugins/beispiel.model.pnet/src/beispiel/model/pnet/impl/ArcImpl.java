/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArcImpl.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.pnet.impl;

import beispiel.model.pnet.Arc;
import beispiel.model.pnet.Petrinet;
import beispiel.model.pnet.Place;
import beispiel.model.pnet.PnetPackage;
import beispiel.model.pnet.Transition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.pnet.impl.ArcImpl#getArcToPetrinet <em>Arc To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.pnet.impl.ArcImpl#getArcToTransition <em>Arc To Transition</em>}</li>
 *   <li>{@link beispiel.model.pnet.impl.ArcImpl#getArcToPlace <em>Arc To Place</em>}</li>
 *   <li>{@link beispiel.model.pnet.impl.ArcImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArcImpl extends EObjectImpl implements Arc {
	/**
	 * The cached value of the '{@link #getArcToTransition() <em>Arc To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArcToTransition()
	 * @generated
	 * @ordered
	 */
	protected Transition arcToTransition;

	/**
	 * The cached value of the '{@link #getArcToPlace() <em>Arc To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArcToPlace()
	 * @generated
	 * @ordered
	 */
	protected Place arcToPlace;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArcImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return PnetPackage.Literals.ARC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Petrinet getArcToPetrinet() {
		if (eContainerFeatureID != PnetPackage.ARC__ARC_TO_PETRINET) return null;
		return (Petrinet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArcToPetrinet(Petrinet newArcToPetrinet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newArcToPetrinet, PnetPackage.ARC__ARC_TO_PETRINET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArcToPetrinet(Petrinet newArcToPetrinet) {
		if (newArcToPetrinet != eInternalContainer() || (eContainerFeatureID != PnetPackage.ARC__ARC_TO_PETRINET && newArcToPetrinet != null)) {
			if (EcoreUtil.isAncestor(this, newArcToPetrinet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newArcToPetrinet != null)
				msgs = ((InternalEObject)newArcToPetrinet).eInverseAdd(this, PnetPackage.PETRINET__PERTINET_TO_ARC, Petrinet.class, msgs);
			msgs = basicSetArcToPetrinet(newArcToPetrinet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PnetPackage.ARC__ARC_TO_PETRINET, newArcToPetrinet, newArcToPetrinet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getArcToTransition() {
		if (arcToTransition != null && arcToTransition.eIsProxy()) {
			InternalEObject oldArcToTransition = (InternalEObject)arcToTransition;
			arcToTransition = (Transition)eResolveProxy(oldArcToTransition);
			if (arcToTransition != oldArcToTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PnetPackage.ARC__ARC_TO_TRANSITION, oldArcToTransition, arcToTransition));
			}
		}
		return arcToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetArcToTransition() {
		return arcToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArcToTransition(Transition newArcToTransition) {
		Transition oldArcToTransition = arcToTransition;
		arcToTransition = newArcToTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PnetPackage.ARC__ARC_TO_TRANSITION, oldArcToTransition, arcToTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place getArcToPlace() {
		if (arcToPlace != null && arcToPlace.eIsProxy()) {
			InternalEObject oldArcToPlace = (InternalEObject)arcToPlace;
			arcToPlace = (Place)eResolveProxy(oldArcToPlace);
			if (arcToPlace != oldArcToPlace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PnetPackage.ARC__ARC_TO_PLACE, oldArcToPlace, arcToPlace));
			}
		}
		return arcToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place basicGetArcToPlace() {
		return arcToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArcToPlace(Place newArcToPlace) {
		Place oldArcToPlace = arcToPlace;
		arcToPlace = newArcToPlace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PnetPackage.ARC__ARC_TO_PLACE, oldArcToPlace, arcToPlace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PnetPackage.ARC__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetArcToPetrinet((Petrinet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				return basicSetArcToPetrinet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				return eInternalContainer().eInverseRemove(this, PnetPackage.PETRINET__PERTINET_TO_ARC, Petrinet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				return getArcToPetrinet();
			case PnetPackage.ARC__ARC_TO_TRANSITION:
				if (resolve) return getArcToTransition();
				return basicGetArcToTransition();
			case PnetPackage.ARC__ARC_TO_PLACE:
				if (resolve) return getArcToPlace();
				return basicGetArcToPlace();
			case PnetPackage.ARC__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				setArcToPetrinet((Petrinet)newValue);
				return;
			case PnetPackage.ARC__ARC_TO_TRANSITION:
				setArcToTransition((Transition)newValue);
				return;
			case PnetPackage.ARC__ARC_TO_PLACE:
				setArcToPlace((Place)newValue);
				return;
			case PnetPackage.ARC__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				setArcToPetrinet((Petrinet)null);
				return;
			case PnetPackage.ARC__ARC_TO_TRANSITION:
				setArcToTransition((Transition)null);
				return;
			case PnetPackage.ARC__ARC_TO_PLACE:
				setArcToPlace((Place)null);
				return;
			case PnetPackage.ARC__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PnetPackage.ARC__ARC_TO_PETRINET:
				return getArcToPetrinet() != null;
			case PnetPackage.ARC__ARC_TO_TRANSITION:
				return arcToTransition != null;
			case PnetPackage.ARC__ARC_TO_PLACE:
				return arcToPlace != null;
			case PnetPackage.ARC__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ArcImpl