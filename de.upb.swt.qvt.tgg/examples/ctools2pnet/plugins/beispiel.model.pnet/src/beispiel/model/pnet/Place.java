/**
 * <copyright>
 * </copyright>
 *
 * $Id: Place.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.pnet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}</li>
 *   <li>{@link beispiel.model.pnet.Place#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.pnet.PnetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends EObject {
	/**
	 * Returns the value of the '<em><b>Place To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.pnet.Petrinet#getPetrinetToPlace <em>Petrinet To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Place To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Place To Petrinet</em>' container reference.
	 * @see #setPlaceToPetrinet(Petrinet)
	 * @see beispiel.model.pnet.PnetPackage#getPlace_PlaceToPetrinet()
	 * @see beispiel.model.pnet.Petrinet#getPetrinetToPlace
	 * @model opposite="petrinetToPlace" transient="false"
	 * @generated
	 */
	Petrinet getPlaceToPetrinet();

	/**
	 * Sets the value of the '{@link beispiel.model.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Place To Petrinet</em>' container reference.
	 * @see #getPlaceToPetrinet()
	 * @generated
	 */
	void setPlaceToPetrinet(Petrinet value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see beispiel.model.pnet.PnetPackage#getPlace_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link beispiel.model.pnet.Place#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Place