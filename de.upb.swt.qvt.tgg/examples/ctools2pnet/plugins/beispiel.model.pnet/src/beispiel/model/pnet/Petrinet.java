/**
 * <copyright>
 * </copyright>
 *
 * $Id: Petrinet.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.pnet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petrinet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.pnet.Petrinet#getName <em>Name</em>}</li>
 *   <li>{@link beispiel.model.pnet.Petrinet#getPetrinetToPlace <em>Petrinet To Place</em>}</li>
 *   <li>{@link beispiel.model.pnet.Petrinet#getPetrinetToTransition <em>Petrinet To Transition</em>}</li>
 *   <li>{@link beispiel.model.pnet.Petrinet#getPertinetToArc <em>Pertinet To Arc</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.pnet.PnetPackage#getPetrinet()
 * @model
 * @generated
 */
public interface Petrinet extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see beispiel.model.pnet.PnetPackage#getPetrinet_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link beispiel.model.pnet.Petrinet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Petrinet To Place</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.pnet.Place}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Petrinet To Place</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Petrinet To Place</em>' containment reference list.
	 * @see beispiel.model.pnet.PnetPackage#getPetrinet_PetrinetToPlace()
	 * @see beispiel.model.pnet.Place#getPlaceToPetrinet
	 * @model type="beispiel.model.pnet.Place" opposite="placeToPetrinet" containment="true"
	 * @generated
	 */
	EList getPetrinetToPlace();

	/**
	 * Returns the value of the '<em><b>Petrinet To Transition</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.pnet.Transition}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.pnet.Transition#getTransitionToPetrinet <em>Transition To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Petrinet To Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Petrinet To Transition</em>' containment reference list.
	 * @see beispiel.model.pnet.PnetPackage#getPetrinet_PetrinetToTransition()
	 * @see beispiel.model.pnet.Transition#getTransitionToPetrinet
	 * @model type="beispiel.model.pnet.Transition" opposite="transitionToPetrinet" containment="true"
	 * @generated
	 */
	EList getPetrinetToTransition();

	/**
	 * Returns the value of the '<em><b>Pertinet To Arc</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.pnet.Arc}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.pnet.Arc#getArcToPetrinet <em>Arc To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pertinet To Arc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pertinet To Arc</em>' containment reference list.
	 * @see beispiel.model.pnet.PnetPackage#getPetrinet_PertinetToArc()
	 * @see beispiel.model.pnet.Arc#getArcToPetrinet
	 * @model type="beispiel.model.pnet.Arc" opposite="arcToPetrinet" containment="true"
	 * @generated
	 */
	EList getPertinetToArc();

} // Petrinet