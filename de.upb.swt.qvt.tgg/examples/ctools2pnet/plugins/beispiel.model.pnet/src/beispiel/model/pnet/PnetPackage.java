/**
 * <copyright>
 * </copyright>
 *
 * $Id: PnetPackage.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.pnet;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see beispiel.model.pnet.PnetFactory
 * @model kind="package"
 * @generated
 */
public interface PnetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "pnet";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://beispiel.model.pnet";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "beispiel.model.pnet";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PnetPackage eINSTANCE = beispiel.model.pnet.impl.PnetPackageImpl.init();

	/**
	 * The meta object id for the '{@link beispiel.model.pnet.impl.PetrinetImpl <em>Petrinet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.pnet.impl.PetrinetImpl
	 * @see beispiel.model.pnet.impl.PnetPackageImpl#getPetrinet()
	 * @generated
	 */
	int PETRINET = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRINET__NAME = 0;

	/**
	 * The feature id for the '<em><b>Petrinet To Place</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRINET__PETRINET_TO_PLACE = 1;

	/**
	 * The feature id for the '<em><b>Petrinet To Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRINET__PETRINET_TO_TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Pertinet To Arc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRINET__PERTINET_TO_ARC = 3;

	/**
	 * The number of structural features of the '<em>Petrinet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRINET_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link beispiel.model.pnet.impl.PlaceImpl <em>Place</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.pnet.impl.PlaceImpl
	 * @see beispiel.model.pnet.impl.PnetPackageImpl#getPlace()
	 * @generated
	 */
	int PLACE = 1;

	/**
	 * The feature id for the '<em><b>Place To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__PLACE_TO_PETRINET = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Place</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link beispiel.model.pnet.impl.ArcImpl <em>Arc</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.pnet.impl.ArcImpl
	 * @see beispiel.model.pnet.impl.PnetPackageImpl#getArc()
	 * @generated
	 */
	int ARC = 2;

	/**
	 * The feature id for the '<em><b>Arc To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__ARC_TO_PETRINET = 0;

	/**
	 * The feature id for the '<em><b>Arc To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__ARC_TO_TRANSITION = 1;

	/**
	 * The feature id for the '<em><b>Arc To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__ARC_TO_PLACE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__NAME = 3;

	/**
	 * The number of structural features of the '<em>Arc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link beispiel.model.pnet.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see beispiel.model.pnet.impl.TransitionImpl
	 * @see beispiel.model.pnet.impl.PnetPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 3;

	/**
	 * The feature id for the '<em><b>Transition To Petrinet</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRANSITION_TO_PETRINET = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = 1;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link beispiel.model.pnet.Petrinet <em>Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Petrinet</em>'.
	 * @see beispiel.model.pnet.Petrinet
	 * @generated
	 */
	EClass getPetrinet();

	/**
	 * Returns the meta object for the attribute '{@link beispiel.model.pnet.Petrinet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see beispiel.model.pnet.Petrinet#getName()
	 * @see #getPetrinet()
	 * @generated
	 */
	EAttribute getPetrinet_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.pnet.Petrinet#getPetrinetToPlace <em>Petrinet To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Petrinet To Place</em>'.
	 * @see beispiel.model.pnet.Petrinet#getPetrinetToPlace()
	 * @see #getPetrinet()
	 * @generated
	 */
	EReference getPetrinet_PetrinetToPlace();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.pnet.Petrinet#getPetrinetToTransition <em>Petrinet To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Petrinet To Transition</em>'.
	 * @see beispiel.model.pnet.Petrinet#getPetrinetToTransition()
	 * @see #getPetrinet()
	 * @generated
	 */
	EReference getPetrinet_PetrinetToTransition();

	/**
	 * Returns the meta object for the containment reference list '{@link beispiel.model.pnet.Petrinet#getPertinetToArc <em>Pertinet To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pertinet To Arc</em>'.
	 * @see beispiel.model.pnet.Petrinet#getPertinetToArc()
	 * @see #getPetrinet()
	 * @generated
	 */
	EReference getPetrinet_PertinetToArc();

	/**
	 * Returns the meta object for class '{@link beispiel.model.pnet.Place <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Place</em>'.
	 * @see beispiel.model.pnet.Place
	 * @generated
	 */
	EClass getPlace();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Place To Petrinet</em>'.
	 * @see beispiel.model.pnet.Place#getPlaceToPetrinet()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_PlaceToPetrinet();

	/**
	 * Returns the meta object for the attribute '{@link beispiel.model.pnet.Place#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see beispiel.model.pnet.Place#getName()
	 * @see #getPlace()
	 * @generated
	 */
	EAttribute getPlace_Name();

	/**
	 * Returns the meta object for class '{@link beispiel.model.pnet.Arc <em>Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arc</em>'.
	 * @see beispiel.model.pnet.Arc
	 * @generated
	 */
	EClass getArc();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.pnet.Arc#getArcToPetrinet <em>Arc To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Arc To Petrinet</em>'.
	 * @see beispiel.model.pnet.Arc#getArcToPetrinet()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_ArcToPetrinet();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.pnet.Arc#getArcToTransition <em>Arc To Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Arc To Transition</em>'.
	 * @see beispiel.model.pnet.Arc#getArcToTransition()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_ArcToTransition();

	/**
	 * Returns the meta object for the reference '{@link beispiel.model.pnet.Arc#getArcToPlace <em>Arc To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Arc To Place</em>'.
	 * @see beispiel.model.pnet.Arc#getArcToPlace()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_ArcToPlace();

	/**
	 * Returns the meta object for the attribute '{@link beispiel.model.pnet.Arc#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see beispiel.model.pnet.Arc#getName()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_Name();

	/**
	 * Returns the meta object for class '{@link beispiel.model.pnet.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see beispiel.model.pnet.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the container reference '{@link beispiel.model.pnet.Transition#getTransitionToPetrinet <em>Transition To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Transition To Petrinet</em>'.
	 * @see beispiel.model.pnet.Transition#getTransitionToPetrinet()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TransitionToPetrinet();

	/**
	 * Returns the meta object for the attribute '{@link beispiel.model.pnet.Transition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see beispiel.model.pnet.Transition#getName()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PnetFactory getPnetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link beispiel.model.pnet.impl.PetrinetImpl <em>Petrinet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.pnet.impl.PetrinetImpl
		 * @see beispiel.model.pnet.impl.PnetPackageImpl#getPetrinet()
		 * @generated
		 */
		EClass PETRINET = eINSTANCE.getPetrinet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PETRINET__NAME = eINSTANCE.getPetrinet_Name();

		/**
		 * The meta object literal for the '<em><b>Petrinet To Place</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PETRINET__PETRINET_TO_PLACE = eINSTANCE.getPetrinet_PetrinetToPlace();

		/**
		 * The meta object literal for the '<em><b>Petrinet To Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PETRINET__PETRINET_TO_TRANSITION = eINSTANCE.getPetrinet_PetrinetToTransition();

		/**
		 * The meta object literal for the '<em><b>Pertinet To Arc</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PETRINET__PERTINET_TO_ARC = eINSTANCE.getPetrinet_PertinetToArc();

		/**
		 * The meta object literal for the '{@link beispiel.model.pnet.impl.PlaceImpl <em>Place</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.pnet.impl.PlaceImpl
		 * @see beispiel.model.pnet.impl.PnetPackageImpl#getPlace()
		 * @generated
		 */
		EClass PLACE = eINSTANCE.getPlace();

		/**
		 * The meta object literal for the '<em><b>Place To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__PLACE_TO_PETRINET = eINSTANCE.getPlace_PlaceToPetrinet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLACE__NAME = eINSTANCE.getPlace_Name();

		/**
		 * The meta object literal for the '{@link beispiel.model.pnet.impl.ArcImpl <em>Arc</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.pnet.impl.ArcImpl
		 * @see beispiel.model.pnet.impl.PnetPackageImpl#getArc()
		 * @generated
		 */
		EClass ARC = eINSTANCE.getArc();

		/**
		 * The meta object literal for the '<em><b>Arc To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARC__ARC_TO_PETRINET = eINSTANCE.getArc_ArcToPetrinet();

		/**
		 * The meta object literal for the '<em><b>Arc To Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARC__ARC_TO_TRANSITION = eINSTANCE.getArc_ArcToTransition();

		/**
		 * The meta object literal for the '<em><b>Arc To Place</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARC__ARC_TO_PLACE = eINSTANCE.getArc_ArcToPlace();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARC__NAME = eINSTANCE.getArc_Name();

		/**
		 * The meta object literal for the '{@link beispiel.model.pnet.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see beispiel.model.pnet.impl.TransitionImpl
		 * @see beispiel.model.pnet.impl.PnetPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Transition To Petrinet</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRANSITION_TO_PETRINET = eINSTANCE.getTransition_TransitionToPetrinet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__NAME = eINSTANCE.getTransition_Name();

	}

} //PnetPackage
