/**
 * <copyright>
 * </copyright>
 *
 * $Id: Connection.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools.Connection#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link beispiel.model.ctools.Connection#getTargetPort <em>Target Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools.CtoolsPackage#getConnection()
 * @model
 * @generated
 */
public interface Connection extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Connection To Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Project#getProjectToConnection <em>Project To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection To Project</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection To Project</em>' container reference.
	 * @see #setConnectionToProject(Project)
	 * @see beispiel.model.ctools.CtoolsPackage#getConnection_ConnectionToProject()
	 * @see beispiel.model.ctools.Project#getProjectToConnection
	 * @model opposite="projectToConnection" transient="false"
	 * @generated
	 */
	Project getConnectionToProject();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection To Project</em>' container reference.
	 * @see #getConnectionToProject()
	 * @generated
	 */
	void setConnectionToProject(Project value);

	/**
	 * Returns the value of the '<em><b>Source Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Port#getOutgoingConnection <em>Outgoing Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Port</em>' reference.
	 * @see #setSourcePort(Port)
	 * @see beispiel.model.ctools.CtoolsPackage#getConnection_SourcePort()
	 * @see beispiel.model.ctools.Port#getOutgoingConnection
	 * @model opposite="outgoingConnection" required="true"
	 * @generated
	 */
	Port getSourcePort();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Connection#getSourcePort <em>Source Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Port</em>' reference.
	 * @see #getSourcePort()
	 * @generated
	 */
	void setSourcePort(Port value);

	/**
	 * Returns the value of the '<em><b>Target Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Port#getIncomingConnection <em>Incoming Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Port</em>' reference.
	 * @see #setTargetPort(Port)
	 * @see beispiel.model.ctools.CtoolsPackage#getConnection_TargetPort()
	 * @see beispiel.model.ctools.Port#getIncomingConnection
	 * @model opposite="incomingConnection" required="true"
	 * @generated
	 */
	Port getTargetPort();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Connection#getTargetPort <em>Target Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Port</em>' reference.
	 * @see #getTargetPort()
	 * @generated
	 */
	void setTargetPort(Port value);

} // Connection