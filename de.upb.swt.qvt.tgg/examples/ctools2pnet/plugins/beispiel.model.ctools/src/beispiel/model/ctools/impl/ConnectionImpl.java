/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConnectionImpl.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools.impl;

import beispiel.model.ctools.Connection;
import beispiel.model.ctools.CtoolsPackage;
import beispiel.model.ctools.Port;
import beispiel.model.ctools.Project;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools.impl.ConnectionImpl#getConnectionToProject <em>Connection To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools.impl.ConnectionImpl#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link beispiel.model.ctools.impl.ConnectionImpl#getTargetPort <em>Target Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConnectionImpl extends NamedElementImpl implements Connection {
	/**
	 * The cached value of the '{@link #getSourcePort() <em>Source Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePort()
	 * @generated
	 * @ordered
	 */
	protected Port sourcePort;

	/**
	 * The cached value of the '{@link #getTargetPort() <em>Target Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPort()
	 * @generated
	 * @ordered
	 */
	protected Port targetPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CtoolsPackage.Literals.CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project getConnectionToProject() {
		if (eContainerFeatureID != CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT) return null;
		return (Project)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConnectionToProject(Project newConnectionToProject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newConnectionToProject, CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionToProject(Project newConnectionToProject) {
		if (newConnectionToProject != eInternalContainer() || (eContainerFeatureID != CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT && newConnectionToProject != null)) {
			if (EcoreUtil.isAncestor(this, newConnectionToProject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newConnectionToProject != null)
				msgs = ((InternalEObject)newConnectionToProject).eInverseAdd(this, CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION, Project.class, msgs);
			msgs = basicSetConnectionToProject(newConnectionToProject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT, newConnectionToProject, newConnectionToProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getSourcePort() {
		if (sourcePort != null && sourcePort.eIsProxy()) {
			InternalEObject oldSourcePort = (InternalEObject)sourcePort;
			sourcePort = (Port)eResolveProxy(oldSourcePort);
			if (sourcePort != oldSourcePort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CtoolsPackage.CONNECTION__SOURCE_PORT, oldSourcePort, sourcePort));
			}
		}
		return sourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetSourcePort() {
		return sourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourcePort(Port newSourcePort, NotificationChain msgs) {
		Port oldSourcePort = sourcePort;
		sourcePort = newSourcePort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CtoolsPackage.CONNECTION__SOURCE_PORT, oldSourcePort, newSourcePort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourcePort(Port newSourcePort) {
		if (newSourcePort != sourcePort) {
			NotificationChain msgs = null;
			if (sourcePort != null)
				msgs = ((InternalEObject)sourcePort).eInverseRemove(this, CtoolsPackage.PORT__OUTGOING_CONNECTION, Port.class, msgs);
			if (newSourcePort != null)
				msgs = ((InternalEObject)newSourcePort).eInverseAdd(this, CtoolsPackage.PORT__OUTGOING_CONNECTION, Port.class, msgs);
			msgs = basicSetSourcePort(newSourcePort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.CONNECTION__SOURCE_PORT, newSourcePort, newSourcePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getTargetPort() {
		if (targetPort != null && targetPort.eIsProxy()) {
			InternalEObject oldTargetPort = (InternalEObject)targetPort;
			targetPort = (Port)eResolveProxy(oldTargetPort);
			if (targetPort != oldTargetPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CtoolsPackage.CONNECTION__TARGET_PORT, oldTargetPort, targetPort));
			}
		}
		return targetPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetTargetPort() {
		return targetPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetPort(Port newTargetPort, NotificationChain msgs) {
		Port oldTargetPort = targetPort;
		targetPort = newTargetPort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CtoolsPackage.CONNECTION__TARGET_PORT, oldTargetPort, newTargetPort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetPort(Port newTargetPort) {
		if (newTargetPort != targetPort) {
			NotificationChain msgs = null;
			if (targetPort != null)
				msgs = ((InternalEObject)targetPort).eInverseRemove(this, CtoolsPackage.PORT__INCOMING_CONNECTION, Port.class, msgs);
			if (newTargetPort != null)
				msgs = ((InternalEObject)newTargetPort).eInverseAdd(this, CtoolsPackage.PORT__INCOMING_CONNECTION, Port.class, msgs);
			msgs = basicSetTargetPort(newTargetPort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.CONNECTION__TARGET_PORT, newTargetPort, newTargetPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetConnectionToProject((Project)otherEnd, msgs);
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				if (sourcePort != null)
					msgs = ((InternalEObject)sourcePort).eInverseRemove(this, CtoolsPackage.PORT__OUTGOING_CONNECTION, Port.class, msgs);
				return basicSetSourcePort((Port)otherEnd, msgs);
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				if (targetPort != null)
					msgs = ((InternalEObject)targetPort).eInverseRemove(this, CtoolsPackage.PORT__INCOMING_CONNECTION, Port.class, msgs);
				return basicSetTargetPort((Port)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				return basicSetConnectionToProject(null, msgs);
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				return basicSetSourcePort(null, msgs);
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				return basicSetTargetPort(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				return eInternalContainer().eInverseRemove(this, CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION, Project.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				return getConnectionToProject();
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				if (resolve) return getSourcePort();
				return basicGetSourcePort();
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				if (resolve) return getTargetPort();
				return basicGetTargetPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				setConnectionToProject((Project)newValue);
				return;
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				setSourcePort((Port)newValue);
				return;
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				setTargetPort((Port)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				setConnectionToProject((Project)null);
				return;
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				setSourcePort((Port)null);
				return;
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				setTargetPort((Port)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT:
				return getConnectionToProject() != null;
			case CtoolsPackage.CONNECTION__SOURCE_PORT:
				return sourcePort != null;
			case CtoolsPackage.CONNECTION__TARGET_PORT:
				return targetPort != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionImpl