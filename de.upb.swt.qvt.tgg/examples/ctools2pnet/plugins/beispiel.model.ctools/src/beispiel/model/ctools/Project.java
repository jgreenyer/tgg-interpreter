/**
 * <copyright>
 * </copyright>
 *
 * $Id: Project.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools.Project#getProjectToComponent <em>Project To Component</em>}</li>
 *   <li>{@link beispiel.model.ctools.Project#getProjectToConnection <em>Project To Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools.CtoolsPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Project To Component</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools.Component}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Component#getComponentToProject <em>Component To Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project To Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project To Component</em>' containment reference list.
	 * @see beispiel.model.ctools.CtoolsPackage#getProject_ProjectToComponent()
	 * @see beispiel.model.ctools.Component#getComponentToProject
	 * @model type="beispiel.model.ctools.Component" opposite="componentToProject" containment="true"
	 * @generated
	 */
	EList getProjectToComponent();

	/**
	 * Returns the value of the '<em><b>Project To Connection</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project To Connection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project To Connection</em>' containment reference list.
	 * @see beispiel.model.ctools.CtoolsPackage#getProject_ProjectToConnection()
	 * @see beispiel.model.ctools.Connection#getConnectionToProject
	 * @model type="beispiel.model.ctools.Connection" opposite="connectionToProject" containment="true"
	 * @generated
	 */
	EList getProjectToConnection();

} // Project