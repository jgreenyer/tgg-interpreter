/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProjectImpl.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools.impl;

import beispiel.model.ctools.Component;
import beispiel.model.ctools.Connection;
import beispiel.model.ctools.CtoolsPackage;
import beispiel.model.ctools.Project;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools.impl.ProjectImpl#getProjectToComponent <em>Project To Component</em>}</li>
 *   <li>{@link beispiel.model.ctools.impl.ProjectImpl#getProjectToConnection <em>Project To Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProjectImpl extends NamedElementImpl implements Project {
	/**
	 * The cached value of the '{@link #getProjectToComponent() <em>Project To Component</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectToComponent()
	 * @generated
	 * @ordered
	 */
	protected EList projectToComponent;

	/**
	 * The cached value of the '{@link #getProjectToConnection() <em>Project To Connection</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectToConnection()
	 * @generated
	 * @ordered
	 */
	protected EList projectToConnection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CtoolsPackage.Literals.PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getProjectToComponent() {
		if (projectToComponent == null) {
			projectToComponent = new EObjectContainmentWithInverseEList(Component.class, this, CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT, CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT);
		}
		return projectToComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getProjectToConnection() {
		if (projectToConnection == null) {
			projectToConnection = new EObjectContainmentWithInverseEList(Connection.class, this, CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION, CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT);
		}
		return projectToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return ((InternalEList)getProjectToComponent()).basicAdd(otherEnd, msgs);
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return ((InternalEList)getProjectToConnection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return ((InternalEList)getProjectToComponent()).basicRemove(otherEnd, msgs);
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return ((InternalEList)getProjectToConnection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return getProjectToComponent();
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return getProjectToConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				getProjectToComponent().clear();
				getProjectToComponent().addAll((Collection)newValue);
				return;
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				getProjectToConnection().clear();
				getProjectToConnection().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				getProjectToComponent().clear();
				return;
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				getProjectToConnection().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return projectToComponent != null && !projectToComponent.isEmpty();
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return projectToConnection != null && !projectToConnection.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProjectImpl