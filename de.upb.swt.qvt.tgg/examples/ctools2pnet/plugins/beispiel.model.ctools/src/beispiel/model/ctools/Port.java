/**
 * <copyright>
 * </copyright>
 *
 * $Id: Port.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools.Port#getType <em>Type</em>}</li>
 *   <li>{@link beispiel.model.ctools.Port#getPortToComponent <em>Port To Component</em>}</li>
 *   <li>{@link beispiel.model.ctools.Port#getOutgoingConnection <em>Outgoing Connection</em>}</li>
 *   <li>{@link beispiel.model.ctools.Port#getIncomingConnection <em>Incoming Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools.CtoolsPackage#getPort()
 * @model
 * @generated
 */
public interface Port extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see beispiel.model.ctools.CtoolsPackage#getPort_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Port#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Port To Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Component#getComponentToPort <em>Component To Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port To Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port To Component</em>' container reference.
	 * @see #setPortToComponent(Component)
	 * @see beispiel.model.ctools.CtoolsPackage#getPort_PortToComponent()
	 * @see beispiel.model.ctools.Component#getComponentToPort
	 * @model opposite="componentToPort" transient="false"
	 * @generated
	 */
	Component getPortToComponent();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Port#getPortToComponent <em>Port To Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port To Component</em>' container reference.
	 * @see #getPortToComponent()
	 * @generated
	 */
	void setPortToComponent(Component value);

	/**
	 * Returns the value of the '<em><b>Outgoing Connection</b></em>' reference list.
	 * The list contents are of type {@link beispiel.model.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Connection#getSourcePort <em>Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Connection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Connection</em>' reference list.
	 * @see beispiel.model.ctools.CtoolsPackage#getPort_OutgoingConnection()
	 * @see beispiel.model.ctools.Connection#getSourcePort
	 * @model type="beispiel.model.ctools.Connection" opposite="sourcePort"
	 * @generated
	 */
	EList getOutgoingConnection();

	/**
	 * Returns the value of the '<em><b>Incoming Connection</b></em>' reference list.
	 * The list contents are of type {@link beispiel.model.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Connection#getTargetPort <em>Target Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Connection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Connection</em>' reference list.
	 * @see beispiel.model.ctools.CtoolsPackage#getPort_IncomingConnection()
	 * @see beispiel.model.ctools.Connection#getTargetPort
	 * @model type="beispiel.model.ctools.Connection" opposite="targetPort"
	 * @generated
	 */
	EList getIncomingConnection();

} // Port