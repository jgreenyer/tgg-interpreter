/**
 * <copyright>
 * </copyright>
 *
 * $Id: Track.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Track</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see beispiel.model.ctools.CtoolsPackage#getTrack()
 * @model
 * @generated
 */
public interface Track extends Component {
} // Track