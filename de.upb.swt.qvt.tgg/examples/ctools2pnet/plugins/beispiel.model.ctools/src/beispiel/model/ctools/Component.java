/**
 * <copyright>
 * </copyright>
 *
 * $Id: Component.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link beispiel.model.ctools.Component#getComponentToProject <em>Component To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools.Component#getComponentToPort <em>Component To Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see beispiel.model.ctools.CtoolsPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Component To Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Project#getProjectToComponent <em>Project To Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component To Project</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component To Project</em>' container reference.
	 * @see #setComponentToProject(Project)
	 * @see beispiel.model.ctools.CtoolsPackage#getComponent_ComponentToProject()
	 * @see beispiel.model.ctools.Project#getProjectToComponent
	 * @model opposite="projectToComponent" transient="false"
	 * @generated
	 */
	Project getComponentToProject();

	/**
	 * Sets the value of the '{@link beispiel.model.ctools.Component#getComponentToProject <em>Component To Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component To Project</em>' container reference.
	 * @see #getComponentToProject()
	 * @generated
	 */
	void setComponentToProject(Project value);

	/**
	 * Returns the value of the '<em><b>Component To Port</b></em>' containment reference list.
	 * The list contents are of type {@link beispiel.model.ctools.Port}.
	 * It is bidirectional and its opposite is '{@link beispiel.model.ctools.Port#getPortToComponent <em>Port To Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component To Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component To Port</em>' containment reference list.
	 * @see beispiel.model.ctools.CtoolsPackage#getComponent_ComponentToPort()
	 * @see beispiel.model.ctools.Port#getPortToComponent
	 * @model type="beispiel.model.ctools.Port" opposite="portToComponent" containment="true"
	 * @generated
	 */
	EList getComponentToPort();

} // Component