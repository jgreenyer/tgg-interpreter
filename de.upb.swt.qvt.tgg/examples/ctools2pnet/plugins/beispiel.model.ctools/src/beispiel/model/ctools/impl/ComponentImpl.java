/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentImpl.java 1360 2006-03-27 12:26:26 +0000 (Mo, 27 Mrz 2006) jgreen $
 */
package beispiel.model.ctools.impl;

import beispiel.model.ctools.Component;
import beispiel.model.ctools.CtoolsPackage;
import beispiel.model.ctools.Port;
import beispiel.model.ctools.Project;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link beispiel.model.ctools.impl.ComponentImpl#getComponentToProject <em>Component To Project</em>}</li>
 *   <li>{@link beispiel.model.ctools.impl.ComponentImpl#getComponentToPort <em>Component To Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ComponentImpl extends NamedElementImpl implements Component {
	/**
	 * The cached value of the '{@link #getComponentToPort() <em>Component To Port</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentToPort()
	 * @generated
	 * @ordered
	 */
	protected EList componentToPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CtoolsPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project getComponentToProject() {
		if (eContainerFeatureID != CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT) return null;
		return (Project)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentToProject(Project newComponentToProject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newComponentToProject, CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentToProject(Project newComponentToProject) {
		if (newComponentToProject != eInternalContainer() || (eContainerFeatureID != CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT && newComponentToProject != null)) {
			if (EcoreUtil.isAncestor(this, newComponentToProject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newComponentToProject != null)
				msgs = ((InternalEObject)newComponentToProject).eInverseAdd(this, CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT, Project.class, msgs);
			msgs = basicSetComponentToProject(newComponentToProject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT, newComponentToProject, newComponentToProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getComponentToPort() {
		if (componentToPort == null) {
			componentToPort = new EObjectContainmentWithInverseEList(Port.class, this, CtoolsPackage.COMPONENT__COMPONENT_TO_PORT, CtoolsPackage.PORT__PORT_TO_COMPONENT);
		}
		return componentToPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetComponentToProject((Project)otherEnd, msgs);
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				return ((InternalEList)getComponentToPort()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				return basicSetComponentToProject(null, msgs);
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				return ((InternalEList)getComponentToPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				return eInternalContainer().eInverseRemove(this, CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT, Project.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				return getComponentToProject();
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				return getComponentToPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				setComponentToProject((Project)newValue);
				return;
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				getComponentToPort().clear();
				getComponentToPort().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				setComponentToProject((Project)null);
				return;
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				getComponentToPort().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT:
				return getComponentToProject() != null;
			case CtoolsPackage.COMPONENT__COMPONENT_TO_PORT:
				return componentToPort != null && !componentToPort.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentImpl