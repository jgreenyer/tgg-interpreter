/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.pnet.impl;

import de.upb.swt.qvt.tgg.example.pnet.Arc;
import de.upb.swt.qvt.tgg.example.pnet.Petrinet;
import de.upb.swt.qvt.tgg.example.pnet.Place;
import de.upb.swt.qvt.tgg.example.pnet.PnetPackage;
import de.upb.swt.qvt.tgg.example.pnet.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Petrinet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.impl.PetrinetImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.impl.PetrinetImpl#getPetrinetToPlace <em>Petrinet To Place</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.impl.PetrinetImpl#getPetrinetToTransition <em>Petrinet To Transition</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.impl.PetrinetImpl#getPertinetToArc <em>Pertinet To Arc</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PetrinetImpl extends EObjectImpl implements Petrinet {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPetrinetToPlace() <em>Petrinet To Place</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPetrinetToPlace()
	 * @generated
	 * @ordered
	 */
	protected EList<Place> petrinetToPlace;

	/**
	 * The cached value of the '{@link #getPetrinetToTransition() <em>Petrinet To Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPetrinetToTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> petrinetToTransition;

	/**
	 * The cached value of the '{@link #getPertinetToArc() <em>Pertinet To Arc</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPertinetToArc()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc> pertinetToArc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PetrinetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PnetPackage.Literals.PETRINET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PnetPackage.PETRINET__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Place> getPetrinetToPlace() {
		if (petrinetToPlace == null) {
			petrinetToPlace = new EObjectContainmentWithInverseEList<Place>(Place.class, this, PnetPackage.PETRINET__PETRINET_TO_PLACE, PnetPackage.PLACE__PLACE_TO_PETRINET);
		}
		return petrinetToPlace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getPetrinetToTransition() {
		if (petrinetToTransition == null) {
			petrinetToTransition = new EObjectContainmentWithInverseEList<Transition>(Transition.class, this, PnetPackage.PETRINET__PETRINET_TO_TRANSITION, PnetPackage.TRANSITION__TRANSITION_TO_PETRINET);
		}
		return petrinetToTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Arc> getPertinetToArc() {
		if (pertinetToArc == null) {
			pertinetToArc = new EObjectContainmentWithInverseEList<Arc>(Arc.class, this, PnetPackage.PETRINET__PERTINET_TO_ARC, PnetPackage.ARC__ARC_TO_PETRINET);
		}
		return pertinetToArc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPetrinetToPlace()).basicAdd(otherEnd, msgs);
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPetrinetToTransition()).basicAdd(otherEnd, msgs);
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPertinetToArc()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				return ((InternalEList<?>)getPetrinetToPlace()).basicRemove(otherEnd, msgs);
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				return ((InternalEList<?>)getPetrinetToTransition()).basicRemove(otherEnd, msgs);
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				return ((InternalEList<?>)getPertinetToArc()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PnetPackage.PETRINET__NAME:
				return getName();
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				return getPetrinetToPlace();
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				return getPetrinetToTransition();
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				return getPertinetToArc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PnetPackage.PETRINET__NAME:
				setName((String)newValue);
				return;
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				getPetrinetToPlace().clear();
				getPetrinetToPlace().addAll((Collection<? extends Place>)newValue);
				return;
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				getPetrinetToTransition().clear();
				getPetrinetToTransition().addAll((Collection<? extends Transition>)newValue);
				return;
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				getPertinetToArc().clear();
				getPertinetToArc().addAll((Collection<? extends Arc>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PnetPackage.PETRINET__NAME:
				setName(NAME_EDEFAULT);
				return;
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				getPetrinetToPlace().clear();
				return;
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				getPetrinetToTransition().clear();
				return;
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				getPertinetToArc().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PnetPackage.PETRINET__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case PnetPackage.PETRINET__PETRINET_TO_PLACE:
				return petrinetToPlace != null && !petrinetToPlace.isEmpty();
			case PnetPackage.PETRINET__PETRINET_TO_TRANSITION:
				return petrinetToTransition != null && !petrinetToTransition.isEmpty();
			case PnetPackage.PETRINET__PERTINET_TO_ARC:
				return pertinetToArc != null && !pertinetToArc.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PetrinetImpl
