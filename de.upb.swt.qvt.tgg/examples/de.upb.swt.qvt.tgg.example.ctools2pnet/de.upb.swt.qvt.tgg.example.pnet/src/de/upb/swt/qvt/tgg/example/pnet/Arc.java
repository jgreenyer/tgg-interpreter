/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.pnet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPetrinet <em>Arc To Petrinet</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToTransition <em>Arc To Transition</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPlace <em>Arc To Place</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getArc()
 * @model
 * @generated
 */
public interface Arc extends EObject {
	/**
	 * Returns the value of the '<em><b>Arc To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPertinetToArc <em>Pertinet To Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arc To Petrinet</em>' container reference.
	 * @see #setArcToPetrinet(Petrinet)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getArc_ArcToPetrinet()
	 * @see de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPertinetToArc
	 * @model opposite="pertinetToArc" transient="false"
	 * @generated
	 */
	Petrinet getArcToPetrinet();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPetrinet <em>Arc To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arc To Petrinet</em>' container reference.
	 * @see #getArcToPetrinet()
	 * @generated
	 */
	void setArcToPetrinet(Petrinet value);

	/**
	 * Returns the value of the '<em><b>Arc To Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc To Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arc To Transition</em>' reference.
	 * @see #setArcToTransition(Transition)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getArc_ArcToTransition()
	 * @model required="true"
	 * @generated
	 */
	Transition getArcToTransition();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToTransition <em>Arc To Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arc To Transition</em>' reference.
	 * @see #getArcToTransition()
	 * @generated
	 */
	void setArcToTransition(Transition value);

	/**
	 * Returns the value of the '<em><b>Arc To Place</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc To Place</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arc To Place</em>' reference.
	 * @see #setArcToPlace(Place)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getArc_ArcToPlace()
	 * @model required="true"
	 * @generated
	 */
	Place getArcToPlace();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPlace <em>Arc To Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arc To Place</em>' reference.
	 * @see #getArcToPlace()
	 * @generated
	 */
	void setArcToPlace(Place value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getArc_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Arc
