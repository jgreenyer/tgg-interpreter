/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.pnet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petrinet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPetrinetToPlace <em>Petrinet To Place</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPetrinetToTransition <em>Petrinet To Transition</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPertinetToArc <em>Pertinet To Arc</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPetrinet()
 * @model
 * @generated
 */
public interface Petrinet extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPetrinet_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Petrinet To Place</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.pnet.Place}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Petrinet To Place</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Petrinet To Place</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPetrinet_PetrinetToPlace()
	 * @see de.upb.swt.qvt.tgg.example.pnet.Place#getPlaceToPetrinet
	 * @model opposite="placeToPetrinet" containment="true"
	 * @generated
	 */
	EList<Place> getPetrinetToPlace();

	/**
	 * Returns the value of the '<em><b>Petrinet To Transition</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.pnet.Transition}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.pnet.Transition#getTransitionToPetrinet <em>Transition To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Petrinet To Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Petrinet To Transition</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPetrinet_PetrinetToTransition()
	 * @see de.upb.swt.qvt.tgg.example.pnet.Transition#getTransitionToPetrinet
	 * @model opposite="transitionToPetrinet" containment="true"
	 * @generated
	 */
	EList<Transition> getPetrinetToTransition();

	/**
	 * Returns the value of the '<em><b>Pertinet To Arc</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.pnet.Arc}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPetrinet <em>Arc To Petrinet</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pertinet To Arc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pertinet To Arc</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPetrinet_PertinetToArc()
	 * @see de.upb.swt.qvt.tgg.example.pnet.Arc#getArcToPetrinet
	 * @model opposite="arcToPetrinet" containment="true"
	 * @generated
	 */
	EList<Arc> getPertinetToArc();

} // Petrinet
