/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.pnet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.pnet.Place#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends EObject {
	/**
	 * Returns the value of the '<em><b>Place To Petrinet</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPetrinetToPlace <em>Petrinet To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Place To Petrinet</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Place To Petrinet</em>' container reference.
	 * @see #setPlaceToPetrinet(Petrinet)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPlace_PlaceToPetrinet()
	 * @see de.upb.swt.qvt.tgg.example.pnet.Petrinet#getPetrinetToPlace
	 * @model opposite="petrinetToPlace" transient="false"
	 * @generated
	 */
	Petrinet getPlaceToPetrinet();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Place#getPlaceToPetrinet <em>Place To Petrinet</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Place To Petrinet</em>' container reference.
	 * @see #getPlaceToPetrinet()
	 * @generated
	 */
	void setPlaceToPetrinet(Petrinet value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.upb.swt.qvt.tgg.example.pnet.PnetPackage#getPlace_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.pnet.Place#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Place
