/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToProject <em>Component To Project</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToPort <em>Component To Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Component To Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToComponent <em>Project To Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component To Project</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component To Project</em>' container reference.
	 * @see #setComponentToProject(Project)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getComponent_ComponentToProject()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToComponent
	 * @model opposite="projectToComponent" transient="false"
	 * @generated
	 */
	Project getComponentToProject();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToProject <em>Component To Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component To Project</em>' container reference.
	 * @see #getComponentToProject()
	 * @generated
	 */
	void setComponentToProject(Project value);

	/**
	 * Returns the value of the '<em><b>Component To Port</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.ctools.Port}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Port#getPortToComponent <em>Port To Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component To Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component To Port</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getComponent_ComponentToPort()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Port#getPortToComponent
	 * @model opposite="portToComponent" containment="true"
	 * @generated
	 */
	EList<Port> getComponentToPort();

} // Component
