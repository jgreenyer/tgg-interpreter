/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Port#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Port#getPortToComponent <em>Port To Component</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Port#getOutgoingConnection <em>Outgoing Connection</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Port#getIncomingConnection <em>Incoming Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getPort()
 * @model
 * @generated
 */
public interface Port extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getPort_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Port#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Port To Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToPort <em>Component To Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port To Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port To Component</em>' container reference.
	 * @see #setPortToComponent(Component)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getPort_PortToComponent()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToPort
	 * @model opposite="componentToPort" transient="false"
	 * @generated
	 */
	Component getPortToComponent();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Port#getPortToComponent <em>Port To Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port To Component</em>' container reference.
	 * @see #getPortToComponent()
	 * @generated
	 */
	void setPortToComponent(Component value);

	/**
	 * Returns the value of the '<em><b>Outgoing Connection</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getSourcePort <em>Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Connection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Connection</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getPort_OutgoingConnection()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Connection#getSourcePort
	 * @model opposite="sourcePort"
	 * @generated
	 */
	EList<Connection> getOutgoingConnection();

	/**
	 * Returns the value of the '<em><b>Incoming Connection</b></em>' reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getTargetPort <em>Target Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Connection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Connection</em>' reference list.
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getPort_IncomingConnection()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Connection#getTargetPort
	 * @model opposite="targetPort"
	 * @generated
	 */
	EList<Connection> getIncomingConnection();

} // Port
