/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToComponent <em>Project To Component</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToConnection <em>Project To Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Project To Component</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.ctools.Component}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToProject <em>Component To Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project To Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project To Component</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getProject_ProjectToComponent()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Component#getComponentToProject
	 * @model opposite="componentToProject" containment="true"
	 * @generated
	 */
	EList<Component> getProjectToComponent();

	/**
	 * Returns the value of the '<em><b>Project To Connection</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.swt.qvt.tgg.example.ctools.Connection}.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project To Connection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project To Connection</em>' containment reference list.
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getProject_ProjectToConnection()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Connection#getConnectionToProject
	 * @model opposite="connectionToProject" containment="true"
	 * @generated
	 */
	EList<Connection> getProjectToConnection();

} // Project
