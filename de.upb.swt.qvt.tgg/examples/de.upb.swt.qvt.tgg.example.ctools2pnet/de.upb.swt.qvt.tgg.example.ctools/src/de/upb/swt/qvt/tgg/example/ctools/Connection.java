/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getTargetPort <em>Target Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getConnection()
 * @model
 * @generated
 */
public interface Connection extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Connection To Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToConnection <em>Project To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection To Project</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection To Project</em>' container reference.
	 * @see #setConnectionToProject(Project)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getConnection_ConnectionToProject()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Project#getProjectToConnection
	 * @model opposite="projectToConnection" transient="false"
	 * @generated
	 */
	Project getConnectionToProject();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getConnectionToProject <em>Connection To Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection To Project</em>' container reference.
	 * @see #getConnectionToProject()
	 * @generated
	 */
	void setConnectionToProject(Project value);

	/**
	 * Returns the value of the '<em><b>Source Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Port#getOutgoingConnection <em>Outgoing Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Port</em>' reference.
	 * @see #setSourcePort(Port)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getConnection_SourcePort()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Port#getOutgoingConnection
	 * @model opposite="outgoingConnection" required="true"
	 * @generated
	 */
	Port getSourcePort();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getSourcePort <em>Source Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Port</em>' reference.
	 * @see #getSourcePort()
	 * @generated
	 */
	void setSourcePort(Port value);

	/**
	 * Returns the value of the '<em><b>Target Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.swt.qvt.tgg.example.ctools.Port#getIncomingConnection <em>Incoming Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Port</em>' reference.
	 * @see #setTargetPort(Port)
	 * @see de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage#getConnection_TargetPort()
	 * @see de.upb.swt.qvt.tgg.example.ctools.Port#getIncomingConnection
	 * @model opposite="incomingConnection" required="true"
	 * @generated
	 */
	Port getTargetPort();

	/**
	 * Sets the value of the '{@link de.upb.swt.qvt.tgg.example.ctools.Connection#getTargetPort <em>Target Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Port</em>' reference.
	 * @see #getTargetPort()
	 * @generated
	 */
	void setTargetPort(Port value);

} // Connection
