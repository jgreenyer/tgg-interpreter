/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools.impl;

import de.upb.swt.qvt.tgg.example.ctools.Component;
import de.upb.swt.qvt.tgg.example.ctools.Connection;
import de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage;
import de.upb.swt.qvt.tgg.example.ctools.Project;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.ProjectImpl#getProjectToComponent <em>Project To Component</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.ProjectImpl#getProjectToConnection <em>Project To Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProjectImpl extends NamedElementImpl implements Project {
	/**
	 * The cached value of the '{@link #getProjectToComponent() <em>Project To Component</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectToComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> projectToComponent;

	/**
	 * The cached value of the '{@link #getProjectToConnection() <em>Project To Connection</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectToConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<Connection> projectToConnection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtoolsPackage.Literals.PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getProjectToComponent() {
		if (projectToComponent == null) {
			projectToComponent = new EObjectContainmentWithInverseEList<Component>(Component.class, this, CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT, CtoolsPackage.COMPONENT__COMPONENT_TO_PROJECT);
		}
		return projectToComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connection> getProjectToConnection() {
		if (projectToConnection == null) {
			projectToConnection = new EObjectContainmentWithInverseEList<Connection>(Connection.class, this, CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION, CtoolsPackage.CONNECTION__CONNECTION_TO_PROJECT);
		}
		return projectToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getProjectToComponent()).basicAdd(otherEnd, msgs);
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getProjectToConnection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return ((InternalEList<?>)getProjectToComponent()).basicRemove(otherEnd, msgs);
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return ((InternalEList<?>)getProjectToConnection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return getProjectToComponent();
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return getProjectToConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				getProjectToComponent().clear();
				getProjectToComponent().addAll((Collection<? extends Component>)newValue);
				return;
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				getProjectToConnection().clear();
				getProjectToConnection().addAll((Collection<? extends Connection>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				getProjectToComponent().clear();
				return;
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				getProjectToConnection().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PROJECT__PROJECT_TO_COMPONENT:
				return projectToComponent != null && !projectToComponent.isEmpty();
			case CtoolsPackage.PROJECT__PROJECT_TO_CONNECTION:
				return projectToConnection != null && !projectToConnection.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProjectImpl
