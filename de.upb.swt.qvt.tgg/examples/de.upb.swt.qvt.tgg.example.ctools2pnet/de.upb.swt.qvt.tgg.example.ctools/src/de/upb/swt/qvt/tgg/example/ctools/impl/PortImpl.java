/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.swt.qvt.tgg.example.ctools.impl;

import de.upb.swt.qvt.tgg.example.ctools.Component;
import de.upb.swt.qvt.tgg.example.ctools.Connection;
import de.upb.swt.qvt.tgg.example.ctools.CtoolsPackage;
import de.upb.swt.qvt.tgg.example.ctools.Port;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.PortImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.PortImpl#getPortToComponent <em>Port To Component</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.PortImpl#getOutgoingConnection <em>Outgoing Connection</em>}</li>
 *   <li>{@link de.upb.swt.qvt.tgg.example.ctools.impl.PortImpl#getIncomingConnection <em>Incoming Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortImpl extends NamedElementImpl implements Port {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOutgoingConnection() <em>Outgoing Connection</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<Connection> outgoingConnection;

	/**
	 * The cached value of the '{@link #getIncomingConnection() <em>Incoming Connection</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<Connection> incomingConnection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtoolsPackage.Literals.PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.PORT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getPortToComponent() {
		if (eContainerFeatureID() != CtoolsPackage.PORT__PORT_TO_COMPONENT) return null;
		return (Component)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortToComponent(Component newPortToComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newPortToComponent, CtoolsPackage.PORT__PORT_TO_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortToComponent(Component newPortToComponent) {
		if (newPortToComponent != eInternalContainer() || (eContainerFeatureID() != CtoolsPackage.PORT__PORT_TO_COMPONENT && newPortToComponent != null)) {
			if (EcoreUtil.isAncestor(this, newPortToComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPortToComponent != null)
				msgs = ((InternalEObject)newPortToComponent).eInverseAdd(this, CtoolsPackage.COMPONENT__COMPONENT_TO_PORT, Component.class, msgs);
			msgs = basicSetPortToComponent(newPortToComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtoolsPackage.PORT__PORT_TO_COMPONENT, newPortToComponent, newPortToComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connection> getOutgoingConnection() {
		if (outgoingConnection == null) {
			outgoingConnection = new EObjectWithInverseResolvingEList<Connection>(Connection.class, this, CtoolsPackage.PORT__OUTGOING_CONNECTION, CtoolsPackage.CONNECTION__SOURCE_PORT);
		}
		return outgoingConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connection> getIncomingConnection() {
		if (incomingConnection == null) {
			incomingConnection = new EObjectWithInverseResolvingEList<Connection>(Connection.class, this, CtoolsPackage.PORT__INCOMING_CONNECTION, CtoolsPackage.CONNECTION__TARGET_PORT);
		}
		return incomingConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetPortToComponent((Component)otherEnd, msgs);
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoingConnection()).basicAdd(otherEnd, msgs);
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingConnection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				return basicSetPortToComponent(null, msgs);
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				return ((InternalEList<?>)getOutgoingConnection()).basicRemove(otherEnd, msgs);
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				return ((InternalEList<?>)getIncomingConnection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				return eInternalContainer().eInverseRemove(this, CtoolsPackage.COMPONENT__COMPONENT_TO_PORT, Component.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtoolsPackage.PORT__TYPE:
				return getType();
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				return getPortToComponent();
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				return getOutgoingConnection();
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				return getIncomingConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtoolsPackage.PORT__TYPE:
				setType((String)newValue);
				return;
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				setPortToComponent((Component)newValue);
				return;
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				getOutgoingConnection().clear();
				getOutgoingConnection().addAll((Collection<? extends Connection>)newValue);
				return;
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				getIncomingConnection().clear();
				getIncomingConnection().addAll((Collection<? extends Connection>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PORT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				setPortToComponent((Component)null);
				return;
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				getOutgoingConnection().clear();
				return;
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				getIncomingConnection().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtoolsPackage.PORT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case CtoolsPackage.PORT__PORT_TO_COMPONENT:
				return getPortToComponent() != null;
			case CtoolsPackage.PORT__OUTGOING_CONNECTION:
				return outgoingConnection != null && !outgoingConnection.isEmpty();
			case CtoolsPackage.PORT__INCOMING_CONNECTION:
				return incomingConnection != null && !incomingConnection.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //PortImpl
