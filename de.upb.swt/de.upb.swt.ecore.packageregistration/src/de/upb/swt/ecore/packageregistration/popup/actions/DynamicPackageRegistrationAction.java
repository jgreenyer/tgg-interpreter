package de.upb.swt.ecore.packageregistration.popup.actions;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageRegistryImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

public class DynamicPackageRegistrationAction implements IObjectActionDelegate {

	//private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public DynamicPackageRegistrationAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		//shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		Iterator<?> files = structuredSelection.iterator();
		while (files.hasNext()) {
			IFile file = (IFile) files.next();
			URI resourceURI = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource ecoreResource = resourceSet.getResource(resourceURI, true);
			EObject root = ecoreResource.getContents().get(0);
			if (root instanceof EPackage) {
				if ("de.upb.swt.ecore.packageregistration.register".equals(action.getId())) {
					EPackageRegistryImpl.INSTANCE.put(((EPackage) root).getNsURI(), root);
				} else if ("de.upb.swt.ecore.packageregistration.unregister".equals(action.getId())) {
					EPackageRegistryImpl.INSTANCE.remove(((EPackage) root).getNsURI());
				}
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
